//
//  HomePageOptionCell.h
//  moffice
//
//  Created by CA on 13-11-3.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomePageOptionCell : UITableViewCell {
@private
    BOOL _isSelected;
    UIImageView *_iconView;
    UILabel *_iconLabel;
    UIImageView *_iconSelectedView;
}

@property (nonatomic, retain) UIImageView *iconView;
@property (nonatomic, retain) UILabel *iconLabel;
@property (nonatomic, retain) UIImageView *iconSelectedView;

@end
