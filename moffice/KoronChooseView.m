//
//  KoronChooseView.m
//  KoronMoffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronChooseView.h"

@implementation KoronChooseView

//是否自动登陆
@synthesize isChoose = _isChoose;

- (id)initWithFrame:(CGRect)frame type:(NSString *)type title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _manager = [KoronManager sharedManager];
        
        
        _type = type;
        
        
        
        _chooseView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 8, 15, 15)];
        _chooseView.userInteractionEnabled = YES;
        _label = [[UILabel alloc]initWithFrame:CGRectMake(40, 5, 80, 20)];
        _label.font = [UIFont systemFontOfSize:14];
        _label.userInteractionEnabled = YES;
        
        _label.backgroundColor = [UIColor clearColor];
        _label.textAlignment = NSTextAlignmentLeft;
        _label.text = title;
        [self addSubview:_label];
        
        [self setStatus:[[_manager getObjectForKey:type]isEqualToString:@"YES"]];
        
//        if ([[_manager getObjectForKey:type]isEqualToString:@"YES"]) {
//            _isChoose = YES;
//            [_chooseView setImage:[UIImage imageNamed:@"checkbox_chk.png"]];
//        }else {
//            _isChoose = NO;
//            [_chooseView setImage:[UIImage imageNamed:@"checkbox_nor.png"]];
//        }
        [self addSubview:_chooseView];
        
        
        self.userInteractionEnabled = YES;

//        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gesture)];
//        [self addGestureRecognizer:gesture];

    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

//- (void)gesture {
//    _isChoose = !_isChoose;
//    if (_isChoose) {
//        _isChoose = YES;
//        [_chooseView setImage:[UIImage imageNamed:@"checkbox_chk.png"]];
//        [_manager saveObject:@"YES" forKey:_type];
//    }else {
//        _isChoose = NO;
//        [_chooseView setImage:[UIImage imageNamed:@"checkbox_nor.png"]];
//        [_manager saveObject:@"NO" forKey:_type];
//    }
//}

-(void)setStatus:(BOOL)status {
    _isChoose = status;
    
    if (_isChoose) {
        _isChoose = YES;
        [_chooseView setImage:[UIImage imageNamed:@"checkbox_chk.png"]];
        [_manager saveObject:@"YES" forKey:_type];
    }else {
        _isChoose = NO;
        [_chooseView setImage:[UIImage imageNamed:@"checkbox_nor.png"]];
        [_manager saveObject:@"NO" forKey:_type];
    }
}


@end
