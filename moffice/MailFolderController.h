//
//  MailFolderController.h
//  moffice
//
//  Created by yangxi zou on 12-1-17.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CommonConst.h"
#import "JsonService.h"
#import "AbstractViewController.h"
#import "SMUtils.h"
#import "MailFolderController.h"
#import "MailCell.h"
#import "MailDetailsController.h"
#import "UINavigationBar+Helper.h"
#import "NewMailController.h"
#import "KoronEndRefreshView.h"


@interface MailFolderController : AbstractViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSString *boxid;
    NSString *folderid;
    BOOL isintranet;
    NSMutableDictionary *deleteDic;
    
    NSString *accounts;
    //是否能回复邮件(邮件为草稿箱,已发送邮件时不能回复自己的邮件)
    BOOL isReply;
    
    //上拉刷新
    KoronEndRefreshView *_endView;
    
    //是否显示上拉刷新
    BOOL isShowEndRefresh;
}
@property (nonatomic, retain) NSMutableDictionary *deleteDic;

@property (nonatomic,retain)NSString *boxid;
@property (nonatomic,retain)NSString *folderid;
@property (nonatomic,retain)NSString *accounts;
@property (nonatomic)BOOL isintranet;
@property (nonatomic,assign)BOOL isReply;

@end
