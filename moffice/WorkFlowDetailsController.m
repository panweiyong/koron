//
//  WorkFlowDetailsController.m
//  moffice
//
//  Created by yangxi zou on 12-1-14.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "WorkFlowDetailsController.h"
#import "FlowDetailCell.h"

@implementation TABLEHeader

    
@synthesize creator,createDate,type;


-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    UIFont *ff=[UIFont systemFontOfSize:15];
    [[UIColor colorWithWhite:0 alpha:1] set];
    
    [[NSString stringWithFormat:@"审批类型:%@",type] drawAtPoint:CGPointMake(15,10 ) withFont:ff ];
    [[NSString stringWithFormat:@"提  交  人:%@",creator] drawAtPoint:CGPointMake(15,40 ) withFont:ff ];
    [[NSString stringWithFormat:@"提交时间:%@",createDate ] drawAtPoint:CGPointMake(15,70 ) withFont:ff ];
}

-(CGFloat)getTotalHeight
{
    return  [self viewWithTag:1].frame.size.height+100;
}

@end

@implementation WorkFlowDetailsController

@synthesize oid,tipsid;

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    [footerView release];
    //[contentViewCache release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=NSLocalizedString(@"Workflow",@"Workflow") ; 
    }
    return self;
}

-(void)handleTapFrom:(UITapGestureRecognizer *)recognizer{ 
    UIImageView *iv =(UIImageView *) [recognizer view];
    
    [self setHidesBottomBarWhenPushed:YES ];
    ImageViewController *imgc=[[ImageViewController alloc] initWithNibName:nil bundle:nil];
    [imgc setDisplayImage: [iv image] ];
    [self.navigationController pushViewController:imgc animated:YES];
    [imgc release];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
     [[JsonService sharedManager] cancelAllRequest];
    [self setHidesBottomBarWhenPushed:NO]; 
    [super viewDidDisappear:animated]; 
} 

-(void)viewWillAppear:(BOOL)animated
{
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice getWorkflowDetails:oid];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStartAnimation" object:nil];
} 

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//	switch (section) {
//		case 0: 
//            return @"基本信息";
//			break;
//		case 1: 
//            return @"审批记录";
//			break;
//		case 2: 
//            if([tableData objectForKey:@"actions"]&&[[tableData objectForKey:@"actions"] count]>0)return @"审批操作";
//            return @"";
//			break;
//		case 3: 
//            if([ tableData objectForKey:@"cancelactions"]&&[[tableData objectForKey:@"cancelactions"] count]>0)return @"回退操作";
//            return @"回退操作";
//			break;
//		default:
//			return @"";
//			break;
//	}		 
//	return @"";
//}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if (tableData==nil) {
        return 0;
    }
    
    NSInteger sgcount=2;
    
    if([ tableData objectForKey:@"actions"]&&[[ tableData objectForKey:@"actions"] count]>0)sgcount++;
    if([ tableData objectForKey:@"cancelactions"]&&[[ tableData objectForKey:@"cancelactions"] count]>0)sgcount++;
    
    //if ([[ tableData objectForKey:@"cancelEnabled"] boolValue]==YES) sgcount++; 
	
    return sgcount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableData==nil) {
        return  0;
    }
    if (section==0) {
        return  [[  tableData objectForKey:@"attrs"]  count];
    }else if(section==1){
        return  [[  tableData objectForKey:@"nodes"]   count];
    }else if(section==2){
         return  [[  tableData objectForKey:@"actions"]   count];
    }else if(section==3){
        return  [[  tableData objectForKey:@"cancelactions"]   count];
    }
	return 0;
} 

-(NSString*)parseName:(NSString*)n
{
    //NSAssert(n!=nil, @"Invalid Field Value");
    if(n==nil)return @"unkonw";
    return [n substringToIndex:[ n rangeOfString:@"|"] .location  ];
}
/*
- (DTAttributedTextContentView *)contentViewForIndexPath:(NSIndexPath *)indexPath
{
	if (!contentViewCache)
	{
		//contentViewCache = [[NSMutableDictionary alloc] init];
	}
	
	DTAttributedTextContentView *contentView =nil;// (id)[contentViewCache objectForKey:indexPath];
	
	if (!contentView)
	{
		NSDictionary *snippet = [[ tableData objectForKey:@"attrs"] objectAtIndex:indexPath.row];
		
		NSString *title = [self parseName:  [snippet objectForKey:@"key"]];
		NSString *description = [snippet objectForKey:@"value"];
		
		NSString *html = [NSString stringWithFormat:@"<h3>%@</h3><p><font color=\"gray\">%@</font></p>", title, description];
		NSData *data = [html dataUsingEncoding:NSUTF8StringEncoding];
		NSAttributedString *string = [[[NSAttributedString alloc] initWithHTML:data documentAttributes:NULL] autorelease];
		
		// set width, height is calculated later from text
		CGFloat width = self.view.frame.size.width;
		[DTAttributedTextContentView setLayerClass:nil];//[CATiledLayer class]
		contentView = [[[DTAttributedTextContentView alloc] initWithAttributedString:string width:width - 20.0] autorelease];
		
		contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		contentView.edgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        
		//[contentViewCache setObject:contentView forKey:indexPath];
        contentView.backgroundColor= [UIColor clearColor];
	}
	
	return contentView;
}*/
UITableViewCell *dcell;
- (UITableViewCell *)tableView:(UITableView *)tableViewz cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    //基本信息 
    if (indexPath.section==0) {
    UITableViewCellStyle style = UITableViewCellStyleValue1; 
	NSString *cellid= @"mid" ;
	FlowDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        if (!cell) 
        {
          //cell = [[[UITableViewCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
         } else{
            //[[cell.contentView viewWithTag:100] removeFromSuperview];
        }
        cell = [[[FlowDetailCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
        NSDictionary *dd=[[ tableData objectForKey:@"attrs"]  objectAtIndex:indexPath.row];
        NSString *fkey=  [dd objectForKey:@"key"];// [self parseName: [dd objectForKey:@"key"]];
        
        
        [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        /*
         111
        DTAttributedTextContentView *contentView = [self contentViewForIndexPath:indexPath];
        contentView.frame = cell.contentView.bounds;
        //contentView.shouldDrawImages=YES;  
        contentView.delegate = self;
        contentView.tag = 1;
        //contentView.drawDebugFrames=YES;
        [cell.contentView addSubview:contentView];
        cell.backgroundColor = [UIColor whiteColor];
        if([@"html" isEqualToString: fkey] )
        {
            dcell =cell;
        }
        */
     
       
        UITextAlignment textf=UITextAlignmentLeft;
        
        //if([fkey hasSuffix:@"html"])textf=UITextAlignmentCenter;
        
        DisplayControl *dobj= [JsonUtils ParseControlWithTitle:fkey data:[dd objectForKey:@"value"] delegate:self textAlignment:textf];
        dobj.tag=100;
        cell.textLabel.text= @" ";// dobj.title;
        //cell.textLabel.font = [UIFont fontWithName:@"Arial" size:15];
        cell.textLabel.backgroundColor=[UIColor clearColor];
        [cell.contentView addSubview:dobj];
        /* */
                  
        //cell.detailTextLabel.text =  [[[[tableData objectAtIndex:indexPath.row] objectForKey:@"createTime"]  dateFromISO8601]stringWithFormat:@"HH:mm"];
        //cell.textLabel.backgroundColor  = [UIColor clearColor];
        //cell.detailTextLabel.backgroundColor  = [UIColor clearColor];
        //cell.imageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"tips_mail.png" ]  ];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
        
        
//        if (section==0) {
//            return  [[  tableData objectForKey:@"attrs"]  count];
//        }else if(section==1){
//            return  [[  tableData objectForKey:@"nodes"]   count];
//        }else if(section==2){
//            return  [[  tableData objectForKey:@"actions"]   count];
//        }else if(section==3){
//            return  [[  tableData objectForKey:@"cancelactions"]   count];
        
        [self cellBackgroundImage:cell withIndexPath:indexPath andKey:@"attrs"];
        
        return cell;
     }
    //审批记录
    if (indexPath.section==1) {
        static NSString *cellbid=@"cella";
        UITableViewCellStyle style = UITableViewCellStyleSubtitle; 
        FlowDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellbid];
        if (!cell) cell = [[[FlowDetailCell alloc] initWithStyle:style reuseIdentifier:cellbid] autorelease];
        
        cell.textLabel.text = [[[ tableData objectForKey:@"nodes"] objectAtIndex:indexPath.row] objectForKey:@"memo"];
        cell.detailTextLabel.text = [[[[[ tableData objectForKey:@"nodes"] objectAtIndex:indexPath.row] objectForKey:@"approvalTime"] dateFromISO8601]toShortString];
        
        cell.textLabel.font = [UIFont fontWithName:@"Arial" size:15];
        cell.detailTextLabel.font = [UIFont fontWithName:@"Arial" size:12];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode=UILineBreakModeCharacterWrap;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
        
        [self cellBackgroundImage:cell withIndexPath:indexPath andKey:@"nodes"];
        
        //[[cell.textLabel layer] setBorderWidth:2.0f];
        return cell;
    }
    //审批操作
    if (indexPath.section==2) {
        
        static NSString *cellbid=@"cellb";
        UITableViewCellStyle style = UITableViewCellStyleDefault; 
        FlowDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellbid];
        if (!cell){
            cell = [[[FlowDetailCell alloc] initWithStyle:style reuseIdentifier:cellbid] autorelease];
            

        }
        cell.textLabel.text = [[[ tableData objectForKey:@"actions"] objectAtIndex:indexPath.row] objectForKey:@"name"];
        //cell.detailTextLabel.text = @"";
        cell.textLabel.tag=290;
        //&&[[ tableData objectForKey:@"cancelEnabled"] boolValue]==NO
        //cell.detailTextLabel.text = [[[[[ tableData objectForKey:@"actions"] objectAtIndex:indexPath.row] objectForKey:@"approvalTime"] dateFromISO8601]toShortString];
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue ;
//        cell.backgroundColor = [UIColor orangeColor];
        
        [self cellBackgroundImage:cell withIndexPath:indexPath andKey:@"actions"];
        
        return cell;
    }if (indexPath.section==3) {
        static NSString *cellbid=@"celld";
        UITableViewCellStyle style = UITableViewCellStyleDefault; 
        FlowDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellbid];
        if (!cell) cell = [[[FlowDetailCell alloc] initWithStyle:style reuseIdentifier:cellbid] autorelease];
        cell.textLabel.text = [[[ tableData objectForKey:@"cancelactions"] objectAtIndex:indexPath.row] objectForKey:@"name"];
        //cell.detailTextLabel.text = @"cancel";//[[[[[ tableData objectForKey:@"actions"] objectAtIndex:indexPath.row] objectForKey:@"approvalTime"] dateFromISO8601]toShortString];
        cell.textLabel.tag=220;
        cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue ;
        
        [self cellBackgroundImage:cell withIndexPath:indexPath andKey:@"cancelactions"];
        
        return cell;
    }
    return nil;
}

- (void)cellBackgroundImage:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath andKey:(NSString *)key {
    if (IOS_VERSIONS > 6.9) {
        
        cell.backgroundColor = [UIColor clearColor];
    }
    if ([[  tableData objectForKey:key]  count] == 1 &&IOS_VERSIONS > 6.9) {
        UIImage *image =[[UIImage imageNamed:@"list_bg_group_single"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
        imageView.image = image;
        cell.backgroundView = imageView;
    }else {
        if ([[  tableData objectForKey:key]  count] > 1 &&IOS_VERSIONS > 6.9) {
            if (indexPath.row == 0) {
                UIImage *image =[[UIImage imageNamed:@"list_bg_group_top"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                imageView.image = image;
                cell.backgroundView = imageView;
            }else if (indexPath.row == [[  tableData objectForKey:key]  count]-1) {
                UIImage *image =[[UIImage imageNamed:@"list_bg_group_bottom"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                imageView.image = image;
                cell.backgroundView = imageView;
            }else {
                UIImage *image =[[UIImage imageNamed:@"list_bg_group_middle"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                imageView.image = image;
                cell.backgroundView = imageView;
            }
        }
    }
}

-(void)tableView:(UITableView *)tableViewz accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    [self tableView:tableViewz didSelectRowAtIndexPath:indexPath];
}  
- (CGFloat)tableView:(UITableView *)tableViewz heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ 	
    UITableViewCell *cell = (UITableViewCell *)[self tableView:tableViewz cellForRowAtIndexPath:indexPath];
    if(indexPath.section==0){
        if(cell!=nil&&[[cell.contentView subviews ] count]>1){
        UIView *c=(UIView *)[[cell.contentView subviews ] objectAtIndex:1];
        //CCLOG(@"count:%i",[[cell.contentView subviews ] count]);
        if(c)return c.frame.size.height+10.0f;
        }else{
        return 35;
        }
        //return 35;
        /*
        DTAttributedTextContentView *contentView = [self contentViewForIndexPath:indexPath];
        
        return contentView.bounds.size.height+1;//+5; // for cell seperator
         */
    }
    if(indexPath.section==1)
    {
        if(cell!=nil){
            //CCLOG(@"line:%i",cell.textLabel.numberOfLines);
            NSString *cellText = cell.textLabel.text;
            UIFont *cellFont = [UIFont fontWithName:@"Arial" size:15];
            CGSize constraintSize = CGSizeMake(280.0f, 1000);
            CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
            
            return labelSize.height + 30;
        }else{
            return 40;
        }
        return 40;
    }
        
        
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableViewc heightForFooterInSection:(NSInteger)section {
    if ([[ tableData objectForKey:@"cancelEnabled"] boolValue]==YES) {
        
        if([self numberOfSectionsInTableView:tableView]-1== section )
        {
            return 70;
        }else{
            return 0;
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    //if(headerView==nil)return 0.0f;
    if(section==0)return 100;// [headerView getTotalHeight];
    return  40;
}
-(UIView*)tableView:(UITableView *)tableViewx viewForHeaderInSection:(NSInteger)section
{
//    if(section!=0)return nil;
    if(headerView == nil) {
        headerView  = [[TABLEHeader alloc] init];
        headerView.backgroundColor=[UIColor clearColor];
        [headerView setAutoresizingMask: UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth];
        
        
        if(tableData!=nil)
        {
            headerView.creator = [tableData objectForKey:@"creator"];
            headerView.createDate = [[[tableData objectForKey:@"createTime"] dateFromISO8601] stringWithFormat:@"yyyy-MM-dd HH:mm"];
            headerView.type = [tableData objectForKey:@"type"];
        
        }
        
	}
    
    if (section >=1) {
        UIView *headerViewBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 30)];
        headerViewBg.backgroundColor = [UIColor clearColor];
        
        UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, 280, 30)];
        headerLabel.backgroundColor = [UIColor clearColor];
        NSString *text = nil;
        switch (section) {
            case 0:
                text = @"基本信息";
                break;
            case 1:
                text = @"审批记录";
                break;
            case 2:
                if([tableData objectForKey:@"actions"]&&[[tableData objectForKey:@"actions"] count]>0)text = @"审批操作";
                text = @"审批操作";
                break;
            case 3:
                if([ tableData objectForKey:@"cancelactions"]&&[[tableData objectForKey:@"cancelactions"] count]>0)text = @"回退操作";
                text = @"回退操作";
                break;
            default:
                text = @"";
                break;
        }
        headerLabel.text = text;
        headerLabel.font = [UIFont boldSystemFontOfSize:16];
        [headerViewBg addSubview:headerLabel];
        [headerLabel release];
        return headerViewBg ;
    }
    
	return headerView ;

}



- (UIView *)tableView:(UITableView *)tableViewx viewForFooterInSection:(NSInteger)section {
	
    if(![[ tableData objectForKey:@"cancelEnabled"] boolValue])return nil;
	if([self numberOfSectionsInTableView:tableView]-1 != section )return nil;
    if(footerView == nil) {
			footerView  = [[UIView alloc] init];
			//UIImage *image = [[UIImage imageNamed:@"button_red.png"] stretchableImageWithLeftCapWidth:8 topCapHeight:8];
			//footerView.backgroundColor=[UIColor redColor];
			//[footerView setAutoresizingMask: UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth];
			//create the button
			UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
			//[button setBackgroundImage:image forState:UIControlStateNormal];
			[button setFrame:CGRectMake(10, 3, 300, 40)];	
			//set title, font size and font color
			[button setTitle:NSLocalizedString(@"Cancel Workflow",@"Cancel Workflow")  forState:UIControlStateNormal];
			[button.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
			//[button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal]; 
			//[button setAutoresizingMask: UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth];
			//set action of the button
			[button addTarget:self action:@selector(doCancelWF:) forControlEvents:UIControlEventTouchUpInside];
			button.center =  CGPointMake(   tableView.center.x,button.frame.origin.y + 30 );// self.tableView.center;
			[footerView addSubview:button];
	}
	return footerView;
}
- (void)tableView:(UITableView *)tableViewx didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section<2)return;
    [tableViewx deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *da;
    UITableViewCell *cell = (UITableViewCell *)[self tableView:tableViewx cellForRowAtIndexPath:indexPath];
    
    if (cell.textLabel.tag==290) {
       da = [ tableData objectForKey:@"actions"];
    }else{
       da = [ tableData objectForKey:@"cancelactions"];
    }
    
    WorkFlowActionController *tdc=[[WorkFlowActionController alloc] initWithNibName:nil bundle:nil];
    [tdc setAid: [[da objectAtIndex:indexPath.row] objectForKey:@"id"]];
    [tdc setWid: oid];
    [tdc setFixed: [ [ [da objectAtIndex:indexPath.row] objectForKey:@"fixedApp"] boolValue ]   ];
    [tdc setActs:[[[da objectAtIndex:indexPath.row] objectForKey:@"to"] componentsSeparatedByString:@","]];
    [tdc setIdeaRequired:[  [[da objectAtIndex:indexPath.row] objectForKey:@"ideaRequired"] boolValue ]   ];
    if (cell.textLabel.tag==290) {
        tdc.isCancel=NO;
    }else{
        tdc.isCancel=YES;
    }
    [self.navigationController pushViewController:tdc animated:YES];
    [tdc release];
}
//撤回审批
-(void)doCancelWF:(id)sender
{
    if([ModalAlert confirm:NSLocalizedString(@"Confirm Cancel Workflow",@"Confirm Cancel Workflow")])
    {
        JsonService *jservice=[JsonService sharedManager];
        [jservice setDelegate:self];
        [jservice cancelWorkflow:oid];
    }
}
//上下翻页
-(void)goworkflow:(id)sender
{
    
}


/*
 - (void)tableView:(UITableView *)tableViewx willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 }*/
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadNavigationItem];

    tableView = [[UITableView alloc]  initWithFrame:CGRectMake(0,0, self.view.bounds.size.width ,self.view.bounds.size.height  - self.tabBarController.tabBar.frame.size.height- self.navigationController.navigationBar.frame.size.height  ) style: UITableViewStyleGrouped];
    
    [tableView setDelegate:self];
    [tableView setDataSource:self]; 
    // This should be set to work with the image height 
    [tableView setRowHeight:45];
    tableView.showsHorizontalScrollIndicator= NO;
    tableView.showsVerticalScrollIndicator=YES;
    tableView.scrollEnabled=YES;
    tableView.indicatorStyle =UIScrollViewIndicatorStyleDefault ;
//    [tableView setBackgroundColor: [UIColor colorWithWhite:0.9 alpha:1]]; //DETAILPAGE_BG
    [tableView setBackgroundColor: [UIColor colorWithWhite:0.90 alpha:1]]; //DETAILPAGE_BG
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [tableView setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    //[tableView.layer setShadowOpacity:0.5];
    //[tableView.layer setShadowOffset:CGSizeMake(1, 1)];
    [self.view addSubview:tableView];
    // register notifications
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lazyImageDidFinishLoading:) name:@"DTLazyImageViewDidFinishLoading" object:nil];
        
    //加入列表按钮组
    /*
	UISegmentedControl *listControl = [[UISegmentedControl alloc] initWithItems:nil];
	[listControl insertSegmentWithImage:[UIImage imageNamed:@"up.png"] atIndex:0 animated:YES];
	[listControl insertSegmentWithImage:[UIImage imageNamed:@"down.png"] atIndex:1 animated:YES];
	listControl.segmentedControlStyle = 2;// UISegmentedControlStyleBar;
	//listControl.frame = CGRectMake(600 , 8, 90, 30);	
	//if( UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)){
	//	listControl.frame = CGRectMake(( 1024-158 ) , 8, 90, 30);	
	//}else{
    listControl.frame = CGRectMake(260, 8, 70, 30);	
	//}
	[listControl setMomentary:YES];
	//listControl.selectedSegmentIndex = 0;
	[listControl addTarget:self action:@selector(goworkflow:) forControlEvents:UIControlEventValueChanged];
	UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:listControl];//allocate rightBarButton
	//[self.navigationController.navigationBar  addSubview:listControl];
	self.navigationItem.rightBarButtonItem = rightBarButton;
	[rightBarButton release];//release rightBarButton
	[listControl release];
     */
    
             
    UIActivityIndicatorView *processAlert=[SMView showProcessing];
    processAlert.center = tableView.center;
    processAlert.tag=10001;
    [self.view addSubview:processAlert];
//    [processAlert release];
    
}
//异步接收到返回的数据
- (void)requestDataFinished:(id)jsondata;//request
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
    tableData=[jsondata retain];
    self.title= [tableData objectForKey:@"title"];
    [super requestDataFinished:jsondata];
    
    //更新标记未已读
    if([[tableData objectForKey:@"flag"] intValue]==0){
        JsonService *jservice=[JsonService sharedManager];
        [jservice setDelegate:self];
        [jservice UpdateFlag:@"approval" oid:oid flag:1 tipsid:self.tipsid];
        CCLOG(@"%@",self.tipsid);
    }
}
- (void)postFinished:(id)jsondata
{
    //CCLOG(@"postFinished");
    ProcessResult r = ProcessResultMake([[jsondata objectForKey:@"code"] intValue],[jsondata objectForKey:@"desc"] );
    if (r.code==0) {
        /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error")
                                                            message:r.desc
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"Op Error",@"Op Error")
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView release];*/
        [ModalAlert prompt:[NSString stringWithFormat:@"%@\r%@",NSLocalizedString(@"Op Error",@"Op Error") ,r.desc]   ];
    }else{
        [ModalAlert prompt:NSLocalizedString(@"Op Success",@"Op Success")];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)postFailed:(id)jsondata
{
    CCLOG(@"postFailed");
}


- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForAttachment:(DTTextAttachment *)attachment frame:(CGRect)frame
{
	if (attachment.contentType == DTTextAttachmentTypeImage)
	{
		// if the attachment has a hyperlinkURL then this is currently ignored
		DTLazyImageView *imageView = [[[DTLazyImageView alloc] initWithFrame:frame] autorelease];
		if (attachment.contents)
		{
			imageView.image = attachment.contents;
		}
		
		// url for deferred loading
		imageView.url = attachment.contentURL;
		
		return imageView;
	}
	
	return nil;
}

#pragma mark Notifications
- (void)lazyImageDidFinishLoading:(NSNotification *)notification
{
	NSDictionary *userInfo = [notification userInfo];
	NSURL *url = [userInfo objectForKey:@"ImageURL"];
	CGSize imageSize = [[userInfo objectForKey:@"ImageSize"] CGSizeValue];
	
	NSPredicate *pred = [NSPredicate predicateWithFormat:@"contentURL == %@", url];
    DTAttributedTextView *contentView=(DTAttributedTextView *) [dcell.contentView viewWithTag:1];
	// update all attachments that matchin this URL (possibly multiple images with same size)
	for (DTTextAttachment *oneAttachment in [contentView.contentView.layoutFrame textAttachmentsWithPredicate:pred])
	{
		oneAttachment.originalSize = imageSize;
		
		if (!CGSizeEqualToSize(imageSize, oneAttachment.displaySize))
		{
			oneAttachment.displaySize = imageSize;
		}
	}
	
	// redo layout
	// here we're layouting the entire string, might be more efficient to only relayout the paragraphs that contain these attachments
	[contentView.contentView relayoutText];
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:18.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}
 

@end


