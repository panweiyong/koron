//
//  KoronClentListCell.m
//  moffice
//
//  Created by Mac Mini on 13-11-11.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientListCell.h"

@implementation KoronClientListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self initSubviews];
        
    }
    return self;
}

- (void)initSubviews
{
    //公司
    _clientLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _clientLabel.font = [UIFont boldSystemFontOfSize:14];
    _clientLabel.text = self.model.clientName;
    _clientLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_clientLabel];
    
    //联系人
    _contactLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _contactLabel.font = [UIFont boldSystemFontOfSize:14];
    _contactLabel.text = self.model.userName;
    _contactLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_contactLabel];
    
    //联系电话
    _phoneLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _phoneLabel.font = [UIFont boldSystemFontOfSize:14];
    _phoneLabel.text = self.model.mobile;
    _phoneLabel.backgroundColor = [UIColor clearColor];
    _phoneLabel.textColor = [UIColor blueColor];
    _phoneLabel.userInteractionEnabled = YES;
    [self.contentView addSubview:_phoneLabel];
    
    UITapGestureRecognizer *phoneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onPhoneTap:)];
    [_phoneLabel addGestureRecognizer:phoneTap];
    [phoneTap release];
    
    //销售跟单
    _merchandiserView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _merchandiserView.image = [UIImage imageNamed:@"client_icon"];
    _merchandiserView.userInteractionEnabled = YES;
    [self.contentView addSubview:_merchandiserView];
    
    UITapGestureRecognizer *merchandiserTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMerchandiserTap:)];
    [_merchandiserView addGestureRecognizer:merchandiserTap];
    [merchandiserTap release];
    
}

- (void)onPhoneTap:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(didSelectedPhoneNum:)]) {
        [self.delegate didSelectedPhoneNum:_phoneLabel.text];
    }
}

- (void)onMerchandiserTap:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(merchandiserPushWithName: andCustomerID:)]) {
        [self.delegate merchandiserPushWithName:_clientLabel.text andCustomerID:_customerID];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _clientLabel.frame = CGRectMake(10, 5, 250, 25);
    
    _contactLabel.frame = CGRectMake(_clientLabel.left, _clientLabel.bottom+5, 100, 20);
    
    _phoneLabel.frame = CGRectMake(_contactLabel.right, _contactLabel.top, 120, _contactLabel.height);
    [_phoneLabel sizeToFit];
    _phoneLabel.frame = CGRectMake(0, 0, 120, _phoneLabel.height);
    _phoneLabel.center = CGPointMake(_contactLabel.right+_phoneLabel.width/2.0, _contactLabel.top+_contactLabel.height/2.0);
    
    _merchandiserView.frame = CGRectMake(kDeviceWidth-40, 15, 30, 30);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    [_clientLabel release], _clientLabel = nil;
    [_contactLabel release], _contactLabel = nil;
    [_phoneLabel release], _phoneLabel = nil;
    [_merchandiserView release], _merchandiserView = nil;
    
    [super dealloc];
}

@end
