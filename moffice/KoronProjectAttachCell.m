//
//  KoronProjectAttachCell.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-28.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectAttachCell.h"
#import "KoronProjectAttachView.h"

@implementation KoronProjectAttachCell
{
    UIImageView *_icon;
    UILabel *_attachLabel;
    UIView *_frameView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initSubviews
{
    _frameView = [[UIView alloc] initWithFrame:CGRectZero];
    _frameView.layer.borderWidth = 1;
    _frameView.layer.borderColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1].CGColor;
    [self.contentView addSubview:_frameView];
    
    
    for (NSInteger index = 0; index < 10; index++) {
        KoronProjectAttachView *attachView = [[KoronProjectAttachView alloc] initWithFrame:CGRectZero];
//        attachView.backgroundColor = [UIColor redColor];
        attachView.tag = 100 + index;
        attachView.icon.image = [UIImage imageNamed:[self getAttachPicture:_attach]];
        attachView.attachLabel.text = _attach;
        [self.contentView addSubview:attachView];
        [attachView release];
    }
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _frameView.frame = CGRectMake(0, 0, self.width, self.height);
    
    for (NSInteger index = 0; index < 10; index++) {
        KoronProjectAttachView *attachView = (KoronProjectAttachView *)[self.contentView viewWithTag:index + 100];
        attachView.frame = CGRectMake(10, 10 + index * 60, self.width-20, 40);
    }
}

- (NSString *)getAttachPicture:(NSString *)attachName {
    if ([attachName hasSuffix:@".zip"]) {
        return @"attach_file_icon_zip.png";
    }else if ([attachName hasSuffix:@".xls"]) {
        return @"attach_file_icon_xls.png";
    }else if ([attachName hasSuffix:@".wps"]) {
        return @"attach_file_icon_wps.png";
    }else if ([attachName hasSuffix:@".video"]) {
        return @"attach_file_icon_video.png";
    }else if ([attachName hasSuffix:@".txt"]) {
        return @"attach_file_icon_txt.png";
    }else if ([attachName hasSuffix:@".swf"]) {
        return @"attach_file_icon_swf.png";
    }else if ([attachName hasSuffix:@".psd"]) {
        return @"attach_file_icon_psd.png";
    }else if ([attachName hasSuffix:@".ppt"]) {
        return @"attach_file_icon_ppt.png";
    }else if ([attachName hasSuffix:@".pdf"]) {
        return @"attach_file_icon_pdf.png";
    }else if ([attachName hasSuffix:@".png"]) {
        return @"attach_file_icon_image.png";
    }else if ([attachName hasSuffix:@".html"]) {
        return @"attach_file_icon_html.png";
    }else if ([attachName hasSuffix:@".fla"]) {
        return @"attach_file_icon_fla.png";
    }else if ([attachName hasSuffix:@".doc"]||[attachName hasSuffix:@".docx"]) {
        return @"attach_file_icon_doc.png";
    }else if ([attachName hasSuffix:@".mp3"]) {
        return @"attach_file_icon_audio.png";
    }
    return @"attach_file_icon_default.png";
}

- (void)setFrame:(CGRect)frame
{
    
    frame.origin.x += 10;
    frame.size.width -= 20;
    [super setFrame:frame];
    
}


@end
