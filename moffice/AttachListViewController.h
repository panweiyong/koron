//
//  AttachListViewController.h
//  moffice
//
//  Created by Mac Mini on 13-10-29.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttachView.h"
#import "BaseNavigationController.h"
@interface AttachListViewController : UIViewController<didSelectedItem, UIDocumentInteractionControllerDelegate> {
    NSFileHandle * _fileHandle;
    NSURLConnection * _connection;
    NSString * _fileName;
    
    // 当前的下载进度
    unsigned long long _downLoadSize;
    unsigned long long _totalFileSize;
    // 缓存文件的字节数
    unsigned long long _startDownLoadSize;
    
    UIScrollView *_scrollView;
    BOOL _isDownload;
    NSInteger _indexOfAttachView;
}

@property (nonatomic, retain) NSArray *attachArray;
@property (nonatomic, copy) NSString *pathId;
@property (nonatomic, retain) UIDocumentInteractionController *documentInteractionController;

@end
