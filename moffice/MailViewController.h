//
//  MailViewController.h
//  moffice
//
//  Created by yangxi zou on 11-12-29.
//  Copyright (c) 2011年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CommonConst.h"
#import "JsonService.h"
#import "AbstractViewController.h"
#import "MailBoxCell.h"
#import "SMUtils.h"
#import "MailFolderController.h"

@interface MailViewController :  AbstractViewController<UITableViewDelegate,UITableViewDataSource >{
    NSMutableArray *_switchArray;
}
 

@end
