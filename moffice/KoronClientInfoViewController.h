//
//  KoronClientInfoViewController.h
//  moffice
//
//  Created by Mac Mini on 13-11-12.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"

@interface KoronClientInfoViewController : UIViewController<ASIHTTPRequestDelegate, UITableViewDataSource, UITableViewDelegate>
{
@private
    UIImageView *_selectedView;
    
    ASIFormDataRequest *_infoRequest;
    
    UITableView *_tableView;
    
    NSMutableArray *_infoArray;
    NSMutableArray *_contactArray;
    
    BOOL _isInfoSelected;
    
  
}

@property (nonatomic, copy) NSString *infoClientId;
@property (nonatomic, copy) NSString *infoModuleId;


@end
