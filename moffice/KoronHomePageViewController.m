//
//  KoronHomePageViewController.m
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

//  Tag值  200-300

#import "KoronHomePageViewController.h"

@interface KoronHomePageViewController ()

@end

@implementation KoronHomePageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeStatusBarWhite" object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _topView = [[KoronHomePageTopView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, (self.view.bounds.size.height - 44) * 0.25 )];
    [self.view addSubview:_topView];
    
    _homePageView = [[KoronHomePageView alloc]initWithFrame:CGRectMake(0, _topView.bounds.size.height, self.view.bounds.size.width, (self.view.bounds.size.height - 44) * 0.75 )];
    
    [self.view addSubview:_homePageView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(homePageChange) name:@"HOMEPAGE_VIEW_CHANGE" object:nil];
}

- (void)homePageChange {
    if ([_homePageView superview] != nil) {
        [_homePageView removeFromSuperview];
        
        _homePageView = [[KoronHomePageView alloc]initWithFrame:CGRectMake(0, _topView.bounds.size.height, self.view.bounds.size.width, (self.view.bounds.size.height - 44) * 0.75 )];
        
        [self.view addSubview:_homePageView];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
