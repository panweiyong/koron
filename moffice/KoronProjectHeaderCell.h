//
//  KoronProjectHeaderCell.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-26.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KoronProjectHeaderCell;

@protocol KoronProjectHeaderCellDelegate <NSObject>

- (void)KoronProjectHeaderCell:(KoronProjectHeaderCell *)cell DidChangeType:(NSString *)type;
- (void)KoronProjectHeaderCell:(KoronProjectHeaderCell *)cell DidChangeStatus:(NSString *)status;

@end

@interface KoronProjectHeaderCell : UITableViewCell

@property (nonatomic, assign) id <KoronProjectHeaderCellDelegate> delegate;
@property (nonatomic, retain)  UIImageView *typeSelectedView;
@property (nonatomic, retain)  UIImageView *statusSelectedView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withStatusIndex:(NSInteger)statusIndex andTypeIndex:(NSInteger)typeIndex;

@end
