//
//  KoronToDoListCell.h
//  TodoListDemo
//
//  Created by Mac Mini on 13-11-20.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KoronToDoListCell;

@protocol KoronToDoListCellDelegate <NSObject>

- (void)KoronToDoListCellDidCheck:(KoronToDoListCell *)cell;

@end

@interface KoronToDoListCell : UITableViewCell

@property (nonatomic, retain) UIImageView   *checkView;
@property (nonatomic, retain) UIView        *checkedLine;
@property (nonatomic, retain) UILabel       *toDoContentLabel;
@property (nonatomic, retain) UIImageView   *clockView;
@property (nonatomic, retain) UILabel       *timeLabel;
@property (nonatomic, retain) UILabel       *colorLabel;
@property (nonatomic, retain) UILabel       *defaultLabel;
@property (nonatomic, assign) id <KoronToDoListCellDelegate> delegate;

@end
