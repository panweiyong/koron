//
//  KoronDynamicModel.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-29.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronDynamicModel.h"

@implementation KoronDynamicModel

- (id)mapAttributes
{
    NSDictionary *mapDic = @{
                             @"commentId"         : @"commentId",
                             @"content"           : @"content",
                             @"createBy"          : @"createBy",
                             @"createTime"        : @"createTime",
                             @"nameId"            : @"id",
                             @"replyId"           : @"replyId",
                             };
    return mapDic;
    
}

- (void)dealloc
{
    self.commentId = nil;
    self.content = nil;
    self.createBy = nil;
    self.createTime = nil;
    self.nameId = nil;
    self.replyId = nil;
    [super dealloc];
}


@end
