//
//  KoronClientInfoCell.h
//  moffice
//
//  Created by Mac Mini on 13-11-12.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface KoronClientInfoCell : UITableViewCell
{
@private
    UILabel *_keyLabel;
    UILabel *_valLabel;
}

@property (nonatomic, retain, readonly) UILabel *keyLabel;
@property (nonatomic, retain, readonly) UILabel *valLabel;


@end
