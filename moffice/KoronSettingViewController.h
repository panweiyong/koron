//
//  KoronSettingViewController.h
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "KoronManager.h"

@interface KoronSettingViewController : UIViewController <UIAlertViewDelegate>
{
    UITableView *_tableView;
    
    KoronManager *_manager;
    
    BOOL _isPasswordOn;
    BOOL _isAutoLoginOn;

}

@end
