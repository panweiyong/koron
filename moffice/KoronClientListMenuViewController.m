//
//  KoronClientListMenuViewController.m
//  moffice
//
//  Created by Mac Mini on 13-11-6.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientListMenuViewController.h"
#import "KoronClientModuleModel.h"

@interface KoronClientListMenuViewController ()

@end

@implementation KoronClientListMenuViewController

- (id)initWithStyle:(UITableViewStyle)style withModuleArray:(NSArray *)moduleArray
{
    self = [super initWithStyle:style];
    if (self) {
        _menuArr = [[NSMutableArray alloc]init];
        for (int i = 0; i < [moduleArray count]; i++) {
            KoronMenuModel *model = nil;

            KoronClientModuleModel *moduleModel = moduleArray[i];
            model = [[KoronMenuModel alloc] initWithMenuTitle:[moduleModel moduleName] andMenuType:[moduleModel modelId]];
            if (i == 0) {
                model.isSelected = YES;
            }
            
            [_menuArr addObject:model];
        }

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.tableView setSeparatorColor:[UIColor grayColor]];
}

- (void)setTableViewFrame:(CGRect)rect {
    [self.view setFrame:rect];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_menuArr count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (nil == cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.font = [UIFont systemFontOfSize:18];
    
    KoronMenuModel *model = [_menuArr objectAtIndex:indexPath.row];
    cell.textLabel.text = [model menuTitle];
    if (model.isSelected) {
        cell.textLabel.textColor = [UIColor orangeColor];
    }else {
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    for (KoronMenuModel *model in _menuArr) {
        model.isSelected = NO;
    }
    KoronMenuModel *model = [_menuArr objectAtIndex:indexPath.row];
    model.isSelected = YES;
    [tableView reloadData];
    if (self.delegate && [self.delegate respondsToSelector:@selector(changeTheType: withTitle:)]) {
        [self.delegate changeTheType:model.menuType withTitle:model.menuTitle];
    }
}

@end
