//
//  KoronHomePageTopView.m
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronHomePageTopView.h"

@implementation KoronHomePageTopView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.userInteractionEnabled = YES;
        _manager = [KoronManager sharedManager];
        
        _backgroundView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        _backgroundView.userInteractionEnabled = YES;
        [_backgroundView setImage:[UIImage imageNamed:@"main_top_bg_0.png"]];
        [self addSubview:_backgroundView];
        
        _iconView = [[UIImageView alloc]initWithFrame:CGRectMake(20, self.bounds.size.height - 75, 60, 60)];
        _iconView.backgroundColor = [UIColor clearColor];
        _iconView.layer.masksToBounds = YES;
        _iconView.layer.cornerRadius = 8;
        _iconView.layer.borderWidth = 2;
        _iconView.layer.borderColor = [UIColor whiteColor].CGColor;
        NSString *headURL = [NSString stringWithFormat:@"http://%@/ReadFile?mobile=true&tempFile=&type=PIC&tableName=tblEmployee&fileName=mobile\\%@.jpg",[_manager getObjectForKey:SERVER],[_manager getObjectForKey:SID]];
        headURL = [headURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [_iconView setImageWithURL:[NSURL URLWithString:headURL] placeholderImage:[UIImage imageNamed:@"default_head.jpg"]];
        NSLog(@"%@",headURL);
        [self addSubview:_iconView];
        
        KoronUser *user = [_manager getUserById:[_manager getObjectForKey:SID]];
        
        _userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(_iconView.frame.origin.x + _iconView.frame.size.width + 10, _iconView.frame.origin.y + 5, 170, 30)];
        _userNameLabel.backgroundColor = [UIColor clearColor];
        _userNameLabel.textColor = [UIColor whiteColor];
        _userNameLabel.font = [UIFont systemFontOfSize:24];
        _userNameLabel.textAlignment = NSTextAlignmentLeft;
        _userNameLabel.text = user.name;
        
        [self addSubview:_userNameLabel];
        
        _signatureLabel = [[UILabel alloc]initWithFrame:CGRectMake(_iconView.frame.origin.x + _iconView.frame.size.width + 10, _userNameLabel.frame.origin.y + _userNameLabel.frame.size.height , 170, 20)];
        _signatureLabel.backgroundColor = [UIColor clearColor];
        _signatureLabel.textColor = [UIColor whiteColor];
        _signatureLabel.font = [UIFont systemFontOfSize:14];
        _signatureLabel.textAlignment = NSTextAlignmentLeft;
        _signatureLabel.text = user.sign;
        if (user.sign.length == 0) {
            _signatureLabel.text = @"我正在构思一个伟大的签名!";
        }
        [self addSubview:_signatureLabel];
        
        _settingButton = [[UIButton alloc]initWithFrame:CGRectMake(_signatureLabel.frame.origin.x + _signatureLabel.frame.size.width + 5, _signatureLabel.frame.origin.y - 5, 30, 30)];
        [_settingButton setImage:[UIImage imageNamed:@"ico_set_nor.png"] forState:UIControlStateNormal];
        [_settingButton addTarget:self action:@selector(presentStting) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_settingButton];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)presentStting {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"presentStting" object:nil];
}


@end
