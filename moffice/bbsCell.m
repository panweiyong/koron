//
//  bbsCell.m
//  moffice
//
//  Created by koron on 13-6-5.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "bbsCell.h"

@implementation bbsCell
@synthesize logoImage,nameLabel,timeLabel,contentWeb;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawbbsCell];
    }
    return self;
}

-(void)drawbbsCell
{
    logoImage=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 30, 30)];
    [logoImage setBackgroundColor:[UIColor clearColor]];
    //[logoImage setImage:[UIImage imageNamed:@"org.png"]];
    [self addSubview:logoImage];
    
    nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(42, 15, 100, 10)];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textColor = [UIColor grayColor];
    nameLabel.textAlignment = UITextAlignmentLeft;
    [self addSubview:nameLabel];
    
    timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-125, 15, 120, 10)];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.textColor = [UIColor grayColor];
    timeLabel.textAlignment = UITextAlignmentLeft;
    [self addSubview:timeLabel];
    
    contentWeb=[[UIWebView alloc]init];
    CGRect webframe=CGRectMake(0,50, 320, 1);
    contentWeb.frame=webframe;
    [self addSubview:contentWeb];
    // [bbsWeb setUserInteractionEnabled:NO];
}

//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    CGRect frame = webView.frame;
//    webView.frame = frame;
//    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
//    frame.size = fittingSize;
//    webView.frame = frame;
//}

//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//
//}

-(void)dealloc
{
    [super dealloc];
    [logoImage release];
    [nameLabel release];
    [timeLabel release];
    [contentWeb release];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
