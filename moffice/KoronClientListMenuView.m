//
//  KoronClientListMenuView.m
//  moffice
//
//  Created by Mac Mini on 13-11-6.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientListMenuView.h"

@implementation KoronClientListMenuView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.bounds.size.width/2 - 10, 0, 60, 20)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont systemFontOfSize:20];
        _titleLabel.textColor = [UIColor grayColor];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_titleLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
