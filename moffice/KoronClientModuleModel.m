//
//  clientModuleModel.m
//  moffice
//
//  Created by Mac Mini on 13-11-14.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientModuleModel.h"

@implementation KoronClientModuleModel

- (id)mapAttributes
{
    NSDictionary *mapDic = @{
                             @"modelId"       : @"id",
                             @"moduleName"    : @"moduleName",
                            
                             };
    return mapDic;
}

- (void)dealloc
{
    
    self.modelId = nil;
    self.moduleName = nil;
    [super dealloc];
}

@end
