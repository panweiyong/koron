//
//  BaseModel.h
//  moffice
//
//  Created by Mac Mini on 13-11-11.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

- (id)initWithContent:(NSDictionary *)json;

@end
