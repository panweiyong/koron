//
//  ListViewController.h
//  moffice
//
//  Created by szsm on 12-2-3.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JsonService.h"
#import "DetailsViewController.h"

@interface CustomerCell : UITableViewCell
{
    NSString *refid;
    NSString *type;
}


@end


@interface ListViewController : UITableViewController
{
    int layout;
    NSString *moduleid;
    NSString *value1name;
    NSString *value2name;
    NSString *value3name;
    NSString *parameterKey;
    NSString *parameterKey2;
    //NSString *target;
    NSArray *tableData;
    
}

@property (nonatomic)int layout;
@property (nonatomic,retain)NSString *moduleid;
@property (nonatomic,retain)NSString *value1name;
@property (nonatomic,retain)NSString *value2name;
@property (nonatomic,retain)NSString *value3name;
@property (nonatomic,retain)NSString *parameterKey;
@property (nonatomic,retain)NSString *parameterKey2;
//@property (nonatomic,retain)NSString *target;

@property (nonatomic,retain)NSArray *tableData;


@end
