//
//  KoronRefrashCell.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-30.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronRefrashCell.h"

@implementation KoronRefrashCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame
{
    if (IOS_VERSIONS >= 7.0) {
        frame.origin.x += 5;
        frame.size.width -= 10;
    }
    [super setFrame:frame];
    
}

@end
