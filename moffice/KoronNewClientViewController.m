//
//  KoronNewClientViewController.m
//  moffice
//
//  Created by Mac Mini on 13-11-13.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronNewClientViewController.h"
#import "KoronNewClientCell.h"
#import  <MobileCoreServices/UTCoreTypes.h>
#import "merImageViewController.h"
#import "GTMBase64.h"



@interface KoronNewClientViewController ()

- (void)loadNavigationItem;

- (void)cellBackgroundImage:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath;

@end

@implementation KoronNewClientViewController
@synthesize _search;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        self.title = @"添加新客户";
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self loadNavigationItem];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-20-44) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.05];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _imageArray=[[NSMutableArray alloc]initWithCapacity:0];
    
    
    littleMap=[[BMKMapView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-130, 320, 120, 120)];
    [littleMap setShowsUserLocation:YES];
    littleMap.delegate=self;
    
    

}

#pragma mark - TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag == 10085) {
        return 1;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 10085) {
        return 3;
    }else {
    
        if (section == 0) {
            return 4;
        }
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 10085) {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        if(indexPath.row==0)
        {
            cell.textLabel.text=@"选择";
            cell.textLabel.textColor=[UIColor whiteColor];
            cell.textLabel.backgroundColor=[UIColor clearColor];
            UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
            backgrdView.backgroundColor = [UIColor blackColor];
            cell.backgroundView = backgrdView;
        }
        else if(indexPath.row==1)
        {
            cell.textLabel.text=@"相机拍摄";
        }
        else if(indexPath.row==2)
        {
            cell.textLabel.text=@"手机相册";
        }
        return cell;
    }else {
    
        KoronNewClientCell *cell = [[[KoronNewClientCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
        
        [self cellBackgroundImage:cell withIndexPath:indexPath];
        
        if (indexPath.section == 0 ) {
            if (indexPath.row == 0) {
                UILabel *clientNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 80, 30)];
                clientNameLabel.backgroundColor = [UIColor clearColor];
                clientNameLabel.text = @"客户名称:";
                clientNameLabel.font = [UIFont boldSystemFontOfSize:16];
                [cell.contentView addSubview:clientNameLabel];
                [clientNameLabel release];
                
                UIImageView *clientNameBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(clientNameLabel.right+20, 7, 180, 40)];
                clientNameBackgroundView.userInteractionEnabled = YES;
                UIImage *image = [[UIImage imageNamed:@"search_edit_bg.9.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
                clientNameBackgroundView.image = image;
                _clientNameField = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, 170, 30)];
                _clientNameField.clearButtonMode = YES;
                _clientNameField.delegate = self;
                _clientNameField.placeholder = @"请输入客户名称";
                _clientNameField.returnKeyType = UIReturnKeyDone;
                [clientNameBackgroundView addSubview:_clientNameField];
                
                [cell.contentView addSubview:clientNameBackgroundView];
                [clientNameBackgroundView release];
                
            }else if (indexPath.row == 1) {
                
                UILabel *contactNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 60, 30)];
                contactNameLabel.backgroundColor = [UIColor clearColor];
                contactNameLabel.text = @"联系人:";
                contactNameLabel.font = [UIFont boldSystemFontOfSize:16];
                [cell.contentView addSubview:contactNameLabel];
                [contactNameLabel release];
                
                UIImageView *contactBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(contactNameLabel.right+20, 7, 200, 40)];
                contactBackgroundView.userInteractionEnabled = YES;
                UIImage *image = [[UIImage imageNamed:@"search_edit_bg.9.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
                contactBackgroundView.image = image;
                
                _contactField = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, 200, 30)];
                _contactField.clearButtonMode = YES;
                _contactField.delegate = self;
                _contactField.placeholder = @"请输入联系人";
                _contactField.returnKeyType = UIReturnKeyDone;
                [contactBackgroundView addSubview:_contactField];
                
                [cell.contentView addSubview:contactBackgroundView];
                [contactBackgroundView release];
                
            }else if (indexPath.row == 2) {
                
                UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 60, 30)];
                phoneLabel.backgroundColor = [UIColor clearColor];
                phoneLabel.text = @"电话:";
                phoneLabel.font = [UIFont boldSystemFontOfSize:16];
                [cell.contentView addSubview:phoneLabel];
                [phoneLabel release];
                
                UIImageView *phoneBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(phoneLabel.right+20, 7, 200, 40)];
                phoneBackgroundView.userInteractionEnabled = YES;
                UIImage *image = [[UIImage imageNamed:@"search_edit_bg.9.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
                phoneBackgroundView.image = image;
                _phoneField = [[UITextField alloc] initWithFrame:CGRectMake(5, 5, 200, 30)];
                _phoneField.clearButtonMode = YES;
                _phoneField.delegate = self;
                _phoneField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                _phoneField.returnKeyType = UIReturnKeyDone;
                _phoneField.placeholder = @"请输入号码";
                [phoneBackgroundView addSubview:_phoneField];
                
                [cell.contentView addSubview:phoneBackgroundView];
                [phoneBackgroundView release];
                
            }else if (indexPath.row == 3) {
                UILabel *clientInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 100, 30)];
                clientInfoLabel.backgroundColor = [UIColor clearColor];
                clientInfoLabel.text = @"用户简介:";
                clientInfoLabel.font = [UIFont boldSystemFontOfSize:16];
                [cell.contentView addSubview:clientInfoLabel];
                [clientInfoLabel release];
                
                UIImageView *infoBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(clientInfoLabel.left, clientInfoLabel.bottom, 280, 95)];
                infoBackgroundView.userInteractionEnabled = YES;
                UIImage *image = [[UIImage imageNamed:@"search_edit_bg.9.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
                infoBackgroundView.image = image;
                
                _infoField = [[UITextView alloc] initWithFrame:CGRectMake(5, 5, 270, 85)];
                _infoField.backgroundColor = [UIColor clearColor];
                _infoField.font = [UIFont systemFontOfSize:16];
                _infoField.delegate = self;
                [infoBackgroundView addSubview:_infoField];
                
                [cell.contentView addSubview:infoBackgroundView];
                [infoBackgroundView release];
                
                
            }
        }else {
            
//            imageScrollview=[[UIScrollView alloc]initWithFrame:CGRectMake(7,5, 7+72*3, 80)];
            imageScrollview=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 7+72*3, 80)];
            [cell.contentView addSubview:imageScrollview];
                
            _addPhotoButton = [[UIButton alloc] initWithFrame:CGRectMake(7, 5, 70, 70)];
            [_addPhotoButton setBackgroundImage:[UIImage imageNamed:@"qz_icon_add_photo_normal.png"] forState:UIControlStateNormal];
            [_addPhotoButton setBackgroundImage:[UIImage imageNamed:@"qz_icon_add_photo_pressed.png"] forState:UIControlStateHighlighted];
            [_addPhotoButton addTarget:self action:@selector(addPhoto:) forControlEvents:UIControlEventTouchUpInside];
            [imageScrollview addSubview:_addPhotoButton];
            
            
            UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 55, 50, 10)];
            detailLabel.text = @"添加图片";
            detailLabel.font = [UIFont systemFontOfSize:10];
            detailLabel.textAlignment = NSTextAlignmentCenter;
            detailLabel.textColor = [UIColor colorWithRed:187/255.0 green:187/255.0 blue:187/255.0 alpha:1];
            [_addPhotoButton addSubview:detailLabel];
            [detailLabel release];
                
            
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _phoneField && _phoneField.text.length != 0) {
        if ([self CheckInput:_phoneField.text] == NO) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"M-office" message:@"电话号码必须为数字" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
            alertView.tag = 555;
            alertView.delegate = self;
            [alertView show];
            [alertView release];
        }
    }
}



- (BOOL)CheckInput:(NSString *)string {
    
    NSScanner* scan = [NSScanner scannerWithString:string];
    
    int val;
    
    return [scan scanInt:&val] && [scan isAtEnd];
    
}

#pragma mark - TextView Delegate
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{

    int offset = 216.0;//键盘高度216
    
    NSTimeInterval animationDuration = 0.30f;
    [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
    [UIView setAnimationDuration:animationDuration];
    
    //将视图的Y坐标向上移动offset个单位，以使下面腾出地方用于软键盘的显示
    if(offset > 0)
        _tableView.frame = CGRectMake(0.0f, 0, kDeviceWidth, kDeviceHeight-20-44-offset);
    [_tableView setContentOffset:CGPointMake(0, offset/2) animated:YES];
    [UIView commitAnimations];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
//    [_tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    
    _tableView.frame = CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-20-44);

}

#pragma mark - TableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == 10085) {
        return 0;
    }else {
    
        if (section == 0) {
            return 10;
        }
        return 5;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (tableView.tag == 10085) {
        return 0;
    }else {
    
        if (section == 0) {
            return 1;
        }
        return 40;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 10085) {
        
        return 60;
        
    }else {
    
        if (indexPath.section == 0 && indexPath.row == 3) {
            return 130;
        }else if (indexPath.section == 1 && indexPath.row == 0) {
            return 80;
        }
        
        return 50;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView* customView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)] autorelease];
    
//    UIImageView *locationView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 100, 35)];
//    locationView.userInteractionEnabled = YES;
//    UIImage *image = [[UIImage imageNamed:@"compose_locatebutton_background_locating.9.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 10, 1, 10)];
//    locationView.image = image;
//    [customView addSubview:locationView];
//    [locationView release];
//    
//    UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 25, 25)];
//    icon.image = [UIImage imageNamed:@"icon_lbs.png"];
//    [locationView addSubview:icon];
//    [icon release];
//    
//    UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(icon.right, icon.top, 60, 25)];
//    locationLabel.text = @"点击定位";
//    locationLabel.font = [UIFont systemFontOfSize:14];
//    locationLabel.textAlignment = NSTextAlignmentCenter;
//    locationLabel.textColor = [UIColor grayColor];
//    [locationView addSubview:locationLabel];
//    [locationLabel release];
//    
//    UITapGestureRecognizer *locationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getLocation:)];
//    [locationView addGestureRecognizer:locationTap];
//    [locationTap release];
    
    detailbutton = [[UIButton alloc] init];
    
    detailbutton.frame = CGRectMake(5, 5, 200, 25);
    
    [detailbutton setTitle:@"获取地址" forState:UIControlStateNormal];
    CALayer *demandbtnlayer=[detailbutton layer];
    //是否设置边框以及是否可见
    [demandbtnlayer setMasksToBounds:YES];
    detailbutton.titleLabel.backgroundColor=[UIColor clearColor];
    detailbutton.titleLabel.font = [UIFont systemFontOfSize: 12.0];
    detailbutton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    detailbutton.contentEdgeInsets=UIEdgeInsetsMake(0,5, 0, 0);
    [detailbutton setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    [detailbutton addTarget:self action:@selector(detailbuttonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [customView  addSubview:detailbutton];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"whereAmIAdd"]!=nil)
    {
//        [littleMap setShowsUserLocation:NO];
        whereAmI=[[NSUserDefaults standardUserDefaults]objectForKey:@"whereAmIAdd"];
        detailbutton.frame=CGRectMake(15, 5, 250, 25);
        [detailbutton setTitle:whereAmI forState:UIControlStateNormal];
        [detailbutton setImage:[UIImage imageNamed:@"icon_lbs_act.png"] forState:UIControlStateNormal];
        [detailbutton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-10,0.0,0.0)];
    }
    else
    {
        activity4=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0,0, 27, 27)];
        [activity4 setCenter:CGPointMake(20, detailbutton.top+12.5)];
        [activity4 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
        [self.view  addSubview:activity4];
//        [activity4 startAnimating];
        [detailbutton setTitle:@"获取地址" forState:UIControlStateNormal];
    }
    
    if (section == 1) {
        return customView;
    }
    
    return nil;
}

- (void)detailbuttonTouched:(UIButton *)button
{

}

#pragma mark -  实现 BMKMapViewDelegate 中的方法

- (void)mapViewWillStartLocatingUser:(BMKMapView *)mapView

{
	NSLog(@"start locate");
}

- (void)mapView:(BMKMapView *)mapView didUpdateUserLocation:(BMKUserLocation *)userLocation
{
	if (userLocation != nil)
    {
		NSLog(@"%f %f", userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
        [littleMap setShowsUserLocation:YES];
        [[NSUserDefaults standardUserDefaults] setFloat:userLocation.location.coordinate.latitude forKey:@"startPt_latitude"];
        [[NSUserDefaults standardUserDefaults] setFloat:userLocation.location.coordinate.longitude forKey:@"startPt_longitude"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // CLLocationCoordinate2D nowlocation;
        startPt.latitude = userLocation.location.coordinate.latitude;
        startPt.longitude = userLocation.location.coordinate.longitude;
        [self setMapRegionWithCoordinate:startPt];

	}
}

//传入经纬度,将baiduMapView 锁定到以当前经纬度为中心点的显示区域和合适的显示范围
- (void)setMapRegionWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    // coordinate.latitude=22.570662;
    //  coordinate.longitude=11.179366;
    BMKCoordinateRegion region;
    if (!isARetina1)//这里用一个变量判断一下,只在第一次锁定显示区域时 设置一下显示范围 Map Region
    {
        region = BMKCoordinateRegionMake(coordinate, BMKCoordinateSpanMake(0.002, 0.002));//越小地图显示越详细
        isARetina1 = YES;
        [littleMap setRegion:region animated:YES];//执行设定显示范围
        [self showWithlocation];
    }
    startPt= coordinate;
    [littleMap setCenterCoordinate:coordinate animated:YES];//根据提供的经纬度为中心原点 以动画的形式移动到该区域
}

//以后开始移动,当移动完成后,会执行以下委托
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    [littleMap.annotations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        BMKPointAnnotation *item = (BMKPointAnnotation *)obj;
        if (item.coordinate.latitude == startPt.latitude && item.coordinate.longitude == startPt.longitude )
        {
            [littleMap selectAnnotation:obj animated:YES];//执行之后,会让地图中的标注处于弹出气泡框状态
            *stop = YES;
        }
    }];
}


/**
 定位失败后，会调用此函数
 @param mapView 地图View
 @param error  错误号，参考CLError.h中定义的错误号
 */
- (void)mapView:(BMKMapView *)mapView didFailToLocateUserWithError:(NSError *)error

{
	if (error != nil)
		NSLog(@"locate failed: %@", [error localizedDescription]);
	else {
		NSLog(@"locate failed");
	}
}

//返回地址信息
- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error
{
    NSArray* array = [NSArray arrayWithArray:littleMap.annotations];
	[littleMap removeAnnotations:array];
	array = [NSArray arrayWithArray:littleMap.overlays];
	[littleMap removeOverlays:array];
	if (error == 0) {
		BMKPointAnnotation* item = [[BMKPointAnnotation alloc]init];
		item.coordinate = result.geoPt;
		item.title = result.strAddr;
        whereAmI=[NSString stringWithFormat:@"%@",result.strAddr];
        if(whereAmI==nil)
        {
            [detailbutton setTitle:@"获取地理位置信息失败" forState:UIControlStateNormal];
            
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:whereAmI forKey:@"whereAmI"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            detailbutton.frame=CGRectMake(15, 5, 250, 25);
            [detailbutton setTitle:whereAmI forState:UIControlStateNormal];
            [detailbutton setImage:[UIImage imageNamed:@"icon_lbs_act.png"] forState:UIControlStateNormal];
            [detailbutton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-10,0.0,0.0)];
        }
        [activity4 stopAnimating];
        [activity4 removeFromSuperview];
        [activity4 release];
		[littleMap addAnnotation:item];
		[item release];
	}
}

//将位置信息转化为地理信息
- (void)showWithlocation
{
    whereAmI=nil;
    _search = [[BMKSearch alloc]init];
    _search.delegate=self;
    NSLog(@"starPt=%f,%f",startPt.latitude,startPt.longitude);
    BOOL flag = [_search reverseGeocode:startPt];
    if (!flag)
    {
		NSLog(@"search failed!");
        [detailbutton setTitle:@"获取地理位置信息失败" forState:UIControlStateNormal];
        [detailbutton setImage:nil forState:UIControlStateNormal];
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.tag == 10085) {
        
        if(indexPath.row==1)
        {
            [self cameraPicture];
        }
        else if(indexPath.row==2)
        {
            [self takePicture];
        }
        [_alertTabView removeFromSuperview];
        [_alertTabView release];
        [_backgroundView removeFromSuperview];
        [_backgroundView release];
    }
}

BOOL isARetina1 = NO;
BOOL isACamer = NO;
- (void)cameraPicture
{
    
    //检查相机模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        NSLog(@"sorry, no camera or camera is unavailable!");
        return;
    }
    //获得相机模式下支持的媒体类型
    NSArray* availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    BOOL canTakePicture = NO;
    for (NSString* mediaType in availableMediaTypes) {
        if ([mediaType isEqualToString:(NSString*) kUTTypeImage]) {
            //支持拍照
            canTakePicture = YES;
            break;
        }
    }
    //检查是否支持拍照
    if (!canTakePicture) {
        NSLog(@"sorry, taking picture is not supported.");
        return;
    }
    isACamer = YES;
    //创建图像选取控制器
    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    //设置图像选取控制器的来源模式为相机模式
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    //设置图像选取控制器的类型为静态图像
    imagePickerController.mediaTypes = [[[NSArray alloc] initWithObjects:(NSString*)kUTTypeImage, nil] autorelease];
    //允许用户进行编辑
    imagePickerController.allowsEditing = YES;
    //设置委托对象
    imagePickerController.delegate = self;
    //以模视图控制器的形式显示
    [self presentModalViewController:imagePickerController animated:YES];
    [imagePickerController release];
     
}

//pragma mark - QBImagePickerControllerDelegate

- (void)imagePickerController:(id)imagePickerController didFinishPickingMediaWithInfo:(id)info
{
    if ([_imageArray count]>3||[_imageArray count]==3)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"最多添加三张照片" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    UIView *mainScreen=[[UIView alloc]initWithFrame:self.view.bounds];
    [mainScreen setBackgroundColor:[UIColor clearColor]];
    mainScreen.tag=5001;
    [self.view addSubview:mainScreen];
    
    UILabel *backLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-60, self.view.frame.size.height/2-30, 120, 60)];
    [backLabel setBackgroundColor:[UIColor grayColor]];
    backLabel.textAlignment=NSTextAlignmentRight;
    backLabel.textColor=[UIColor whiteColor];
    backLabel.layer.cornerRadius=5.0f;
    backLabel.tag=5002;
    [self.view addSubview:backLabel];
    
    UIActivityIndicatorView *acView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    acView.center=CGPointMake(27/2, self.view.frame.size.height/2);
    acView.tag=5003;
    [acView startAnimating];
    [self.view addSubview:acView];
    
    
    if([imagePickerController isKindOfClass:[QBImagePickerController class]])
    {
        QBImagePickerController* imagePickerControllers=(QBImagePickerController*)imagePickerController;
        if(imagePickerControllers.allowsMultipleSelection)
        {
            NSArray *mediaInfoArray = (NSArray *)info;
            //NSLog(@"Selected %d photos", mediaInfoArray.count);
            //打印出字典中的内容
            // NSLog(@"get the media info: %@", info);
            for(int i=0;i<[mediaInfoArray count];i++)
            {
                NSDictionary *imageDic=[mediaInfoArray objectAtIndex:i];
                UIImage *originImage = [imageDic valueForKey:UIImagePickerControllerOriginalImage];
                //获取图片路径
                NSString *imagerUrl=[imageDic objectForKey:UIImagePickerControllerReferenceURL];
                if(originImage!=nil&&imagerUrl!=nil)
                {
                    if(![_imageArray containsObject:originImage])
                    {
                        [_imageArray addObject:originImage];
                        
                    }
                }
            }
        }
    }
    else if([imagePickerController isKindOfClass:[UIImagePickerController class]])
    {
        NSDictionary *mediaInfo = (NSDictionary *)info;
        NSLog(@"Selected: %@", mediaInfo);
        UIImage *originImage = [mediaInfo valueForKey:UIImagePickerControllerOriginalImage];
        UIImageWriteToSavedPhotosAlbum(originImage, nil, nil, nil);
        if(originImage!=nil&&![_imageArray containsObject:originImage])
        {
            [_imageArray addObject:originImage];
        }
    }
    [self displayImage:_imageArray];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Cancelled");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSString *)descriptionForSelectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"选择所有照片";
}

- (NSString *)descriptionForDeselectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"所有照片的选择解除";
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos
{
    return [NSString stringWithFormat:@"照片%d张", numberOfPhotos];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"录像%d", numberOfVideos];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos numberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"照片%d张、录像%d", numberOfPhotos, numberOfVideos];
}

-(void)displayImage:(NSArray*)imageArr
{
    for(UIImageView* imageView in [imageScrollview subviews])
    {
        //UIImage *image=
        if([_imageArray containsObject:imageView.image])
        {
            [imageView removeFromSuperview];
        }
    }
    for(int i=0;i<[imageArr count];i++)
    {
        UIImageView *imageView=[[UIImageView alloc]init];
        CGRect viewFrame=CGRectMake(7+72*i, 5, 70, 70);
        imageView.frame=viewFrame;
        imageView.userInteractionEnabled=YES;
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressImageView:)];
        imageView.tag=i;
        [imageView addGestureRecognizer:longPress];
        
        UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapPressImageView:)];
        [imageView addGestureRecognizer:tapPress];
        
        [imageView setImage:[imageArr objectAtIndex:i]];
        [imageScrollview addSubview:imageView];
        [imageView release];
        [longPress release];
    }
    _addPhotoButton.frame=CGRectMake(7+72*[imageArr count], 5, 70, 70);
//    imageScrollview.contentSize=CGSizeMake(105+[imageArr count]*100,100);                                                                                                                       
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==5001||view.tag==5002||view.tag==5003)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}

-(void)longPressImageView:(UITapGestureRecognizer*)paramSender
{
    if(paramSender.state==UIGestureRecognizerStateEnded)
    {
        NSLog(@"long press!");
        CGPoint touchPoint = [paramSender locationInView:imageScrollview];
        NSLog(@"%f,%f",touchPoint.x,touchPoint.y);
        imageIndex=(touchPoint.x-7)/72;
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Confirm Delete",@"Confirm Delete") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel") otherButtonTitles:NSLocalizedString(@"OK",@"OK"), nil];
        [alert show];
        [alert release];
    }
}

-(void)tapPressImageView:(UILongPressGestureRecognizer*)paramSender
{
    NSLog(@"long press!");
    CGPoint touchPoint = [paramSender locationInView:imageScrollview];
    NSLog(@"%f,%f",touchPoint.x,touchPoint.y);
    imageIndex=(touchPoint.x-7)/72;
    merImageViewController *bigImageView=[[merImageViewController alloc]init];
    bigImageView.bigImage=[_imageArray objectAtIndex:imageIndex];
    bigImageView.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:bigImageView animated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 555) {
        [_phoneField becomeFirstResponder];
        return;
    }
    
    if(buttonIndex==1)
    {
        for(UIImageView* imageView in [imageScrollview subviews])
        {
            //UIImage *image=
            if([_imageArray containsObject:imageView.image])
            {
                [imageView removeFromSuperview];
            }
        }
        [_imageArray removeObjectAtIndex:imageIndex];
        //[imageUrlArray removeObjectAtIndex:imageIndex];
        NSLog(@"%d",[_imageArray count]);
        [self displayImage:_imageArray];

    }
}



-(void)takePicture
{
    
    QBImagePickerController *imagePickerController = [[QBImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.limitsMaximumNumberOfSelection=YES;
    imagePickerController.maximumNumberOfSelection=3-[_imageArray count];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
    [self presentViewController:navigationController animated:YES completion:NULL];
    [imagePickerController release];
    [navigationController release];
}

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize OriginImage:(UIImage *)images
{
    UIImage *sourceImage = images;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - Private
- (void)loadNavigationItem
{
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    UIImage *image = [[UIImage imageNamed:@"btn_big_gray"] stretchableImageWithLeftCapWidth:5 topCapHeight:10];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTitle:@"确认" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [button addTarget:self action:@selector(doConfirm:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [button release];
    self.navigationItem.rightBarButtonItem = rightItem;
    [rightItem release];
}

- (void)cellBackgroundImage:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    if (IOS_VERSIONS < 7.0) {
        return;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    if (indexPath.section == 1 && indexPath.row == 0) {
        UIImage *image =[[UIImage imageNamed:@"list_bg_group_single"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
        imageView.image = image;
        cell.backgroundView = imageView;
    }else {
        if (indexPath.row == 0) {
            UIImage *image =[[UIImage imageNamed:@"list_bg_group_top"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
            imageView.image = image;
            cell.backgroundView = imageView;
        }else if (indexPath.row == 3) {
            UIImage *image =[[UIImage imageNamed:@"list_bg_group_bottom"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
            imageView.image = image;
            cell.backgroundView = imageView;
        }else {
            UIImage *image =[[UIImage imageNamed:@"list_bg_group_middle"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
            imageView.image = image;
            cell.backgroundView = imageView;
        }
    }
    

}


#pragma mark - Target Action
- (void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:NO];
    [littleMap setShowsUserLocation:NO];
}

- (void)doConfirm:(UIButton *)btn
{
    if (nil != [[JsonService sharedManager] pushJson]) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"正在添加新用户.." delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
        [alertView release];
        return;
    }
    
    
    [[[JsonService sharedManager]MERRquest]clearDelegatesAndCancel];
    
    [self performSelector:@selector(setBtnbackColor:) withObject:btn afterDelay:1.0f];
    
    UIView *mainScreenView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    mainScreenView.backgroundColor=[UIColor clearColor];
    mainScreenView.tag=97;
    UIView *backView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height/2-40, 100, 80)];
    backView.tag=98;
    backView.backgroundColor=[UIColor grayColor];
    backView.layer.cornerRadius=10.0;
    [self.view addSubview:backView];
    
    activity3 = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 37, 37)];//指定进度轮的大小
    [activity3 setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];//指定进度轮中心点
    [activity3 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];//设置进度轮显示类型
    [self.view addSubview:activity3];
    [activity3 startAnimating];
    
    if(_clientNameField.text == nil || _clientNameField.text.length == 0)
    {
        [activity3 stopAnimating];
        promptStr=[NSString stringWithFormat:@"%@",@"客户不能为空"];
        for(UIView *view in [self.view subviews])
        {
            if(view.tag==97||view.tag==98)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
        [self displayLabel];
        return;
    }
    else if(_contactField.text == nil||[_contactField.text length]==0)
    {
        [activity3 stopAnimating];
        promptStr=[NSString stringWithFormat:@"%@",@"联系人不能为空"];
        for(UIView *view in [self.view subviews])
        {
            if(view.tag==97||view.tag==98)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
        [self displayLabel];
        return;
    }
    else if(_phoneField.text == nil||[_phoneField.text length]==0)
    {
        [activity3 stopAnimating];
        promptStr=[NSString stringWithFormat:@"%@",@"电话不能为空"];
        for(UIView *view in [self.view subviews])
        {
            if(view.tag==97||view.tag==98)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
        [self displayLabel];
        return;
    }
    else
    {
        [self.view addSubview:mainScreenView];
        JsonService *jservice=[JsonService sharedManager];
        [jservice setDelegate:self];
        jservice.pushDelegate=self;
        //        NSString *customerName=[[NSUserDefaults standardUserDefaults] wxq@koronsoft.com Koron168 objectForKey:@"customerFieldtext"];
        NSMutableDictionary *jsonDir=[[NSMutableDictionary alloc]init];
        [jsonDir setValue:_clientNameField.text forKey:@"ClientName"];
        [jsonDir setValue:_contactField.text forKey:@"UserName"];
        [jsonDir setValue:_phoneField.text forKey:@"Telephone"];
        [jsonDir setValue:_infoField.text forKey:@"ClientRemark"];
        NSMutableArray *imageStrArray=[[NSMutableArray alloc]initWithCapacity:0];
        for(int i=0;i<[_imageArray count];i++)
        {
            UIImage *image=[_imageArray objectAtIndex:i];
            
            NSData *imageData = UIImageJPEGRepresentation(image,1.0);
            for(;;)
            {
                if(imageData.length/3364>40)
                {
                    image=[self imageByScalingAndCroppingForSize:CGSizeMake(image.size.width*0.5, image.size.height*0.5) OriginImage:image];
                    imageData=UIImageJPEGRepresentation(image, 1.0);
                }
                else
                {
                    break;
                }
            }
            
            NSString* imageString = [[NSString alloc]initWithData:[GTMBase64 encodeData:imageData]encoding:NSUTF8StringEncoding];
            if(imageString!=nil&&![imageStrArray containsObject:imageString])
            {
                [imageStrArray addObject:imageString];
            }
        }
        [jsonDir setValue:imageStrArray forKey:@"locImage"];
        
#warning 地址
//        [jsonDir setValue:whereAmI forKey:@"Address"];
        NSString *jsonStr=[jsonDir JSONRepresentation];
        //   NSLog(@"%@",jsonStr);
        
        [jservice pushAddClient:jsonStr];
        jservice.pushJson = [jsonStr copy];
        
        
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }

}

- (void)setBtnbackColor:(UIButton *)btn
{
    
}

-(void) LabelremoveSubview:(UILabel *)label
{
    [label removeFromSuperview];
    [label release];
}

-(void) displayLabel
{
    UILabel *resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 310, 100, 40)];
    resultLabel.backgroundColor=[UIColor grayColor];
    resultLabel.textAlignment=NSTextAlignmentCenter;
    resultLabel.font=[UIFont systemFontOfSize:14.0];
    resultLabel.text=promptStr;
    resultLabel.textColor=[UIColor whiteColor];
    CALayer *labelLayer=[resultLabel layer];
    labelLayer.masksToBounds=YES;
    labelLayer.cornerRadius=5.0;
    [self.view addSubview:resultLabel];
    [self performSelector:@selector(LabelremoveSubview:) withObject:resultLabel afterDelay:1.0];
}

- (void)addPhoto:(UIButton *)button
{
    NSLog(@"添加图片");
    
    [_infoField resignFirstResponder];
    
    if([_imageArray count]>=3)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"最多只能添加三张照片" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    _backgroundView = [[UIView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:_backgroundView];
    _alertTabView = [[UITableView alloc]initWithFrame:CGRectMake(20, 120, self.view.frame.size.width-40, 180)];
    _alertTabView.tag=10085;
    _alertTabView.dataSource=self;
    _alertTabView.delegate=self;
    _alertTabView.scrollEnabled=NO;
    _alertTabView.layer.borderWidth = 1;
    _alertTabView.backgroundView.backgroundColor=[UIColor blackColor];
    _alertTabView.layer.borderColor = [[UIColor blackColor] CGColor];
    [self.view addSubview:_alertTabView];
    [self.view bringSubviewToFront:_alertTabView];
}

- (void)getLocation:(UITapGestureRecognizer *)tap
{
    NSLog(@"获取位置");
}

#pragma mark - Memory
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)dealloc
{
    [_addPhotoButton release], _addPhotoButton = nil;
    [_imageArray release], _imageArray = nil;
    [_infoField release], _infoField = nil;
    [_clientNameField release], _clientNameField = nil;
    [_contactField release], _contactField = nil;
    [_phoneField release], _phoneField = nil;
    [_tableView release], _tableView = nil;
    [super dealloc];
}

@end
