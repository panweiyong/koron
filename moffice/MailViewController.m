//
//  MailViewController.m
//  moffice
//
//  Created by yangxi zou on 11-12-29.
//  Copyright (c) 2011年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "MailViewController.h"

@implementation MailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title=NSLocalizedString(@"Mail",@"Mail") ;
        
    }
    return self;
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)dealloc
{
    [_switchArray release],_switchArray = nil;

    [super dealloc];
}

- (void) hideTabBar:(BOOL) hidden{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0];
    
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            if (hidden) {
                [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
            } else {
                [view setFrame:CGRectMake(view.frame.origin.x, 480-49, view.frame.size.width, view.frame.size.height)];
            }
        } 
        else 
        {
            if (hidden) {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
            } else {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480-49)];
            }
        }
    }
    
    [UIView commitAnimations];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [tableData count]; 
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([[_switchArray objectAtIndex:section] boolValue]) {
        
        return [[[tableData objectAtIndex:section]objectForKey:@"folders"] count];
    }
    else {
        
        return 0;
    }

} 
 
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//   return  [[tableData objectAtIndex:section] objectForKey:@"name"];
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    //设置标示图顶部视图
    UIButton *headerButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 45)];
    [headerButton addTarget:self action:@selector(pressHeaderBar:) forControlEvents:UIControlEventTouchUpInside];
    headerButton.backgroundColor = [UIColor whiteColor];
    headerButton.backgroundColor = [UIColor colorWithWhite:0.97 alpha:1];
    headerButton.tag = section;
    
    UIView *headerLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 1)];
    headerLineView.backgroundColor = [UIColor lightGrayColor];
    headerLineView.alpha = 0.5;
    [headerButton addSubview:headerLineView];
    
    UIView *footerLineView = [[UIView alloc] initWithFrame:CGRectMake(0, headerButton.bounds.size.height-1, kDeviceWidth, 1)];
    footerLineView.backgroundColor = [UIColor lightGrayColor];
    footerLineView.alpha = 0.5;
    [headerButton addSubview:footerLineView];
    
    UIImageView *smallMailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 35, 35)];
    smallMailImageView.contentMode = UIViewContentModeScaleAspectFit;
    smallMailImageView.image = [UIImage imageNamed:@"icon_mail_inner.png"];
    [headerButton addSubview:smallMailImageView];
    
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(smallMailImageView.frame.origin.x+smallMailImageView.bounds.size.width+5, smallMailImageView.frame.origin.y+5, 100, 20)];
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.text = [tableData[section] objectForKey:@"name"];
    headerLabel.font = [UIFont systemFontOfSize:14];
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerButton addSubview:headerLabel];
    
    NSArray *countArray = [tableData[section] objectForKey:@"folders"];
    for (NSDictionary *dic in countArray) {
        NSString *count = [[dic objectForKey:@"count"] stringValue];
        if ([count isEqualToString:@"0"] == NO) {
            UIImageView *newImageView = [[UIImageView alloc] initWithFrame:CGRectMake(headerLineView.frame.origin.x+headerLineView.bounds.size.width - 50, headerButton.bounds.size.height/2.0-9, 36, 18)];
            newImageView.image = [UIImage imageNamed:@"icon_new.png"];
            [headerButton addSubview:newImageView];
            
            break;
        }
    }

    return headerButton;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableViewz cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//dlog(@"%f",[[[UIDevice currentDevice] systemVersion] floatValue] );
	UITableViewCellStyle style = UITableViewCellStyleSubtitle; 
	static NSString *cellid= @"mid" ;
	MailBoxCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) 
    {
        cell = [[[ MailBoxCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleGray ;
        
        UIImageView *contentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(40, 6, 32, 32)];
        contentImageView.tag= 101;
        contentImageView.contentMode = UIViewContentModeScaleAspectFit;
        [cell.contentView addSubview:contentImageView];
        [contentImageView release];
        
        cell.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(contentImageView.frame.origin.x+contentImageView.bounds.size.width+5, contentImageView.frame.origin.y, 150, contentImageView.bounds.size.height)];
        cell.contentLabel.tag = 102;
        cell.contentLabel.font = [UIFont systemFontOfSize:14];
        [cell.contentView addSubview:cell.contentLabel];
        
        UIImageView *arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280, 10, 21, 21)];
        arrowView.tag = 103;
        [cell.contentView addSubview:arrowView];
        [arrowView release];
        
    }
    UIImageView *contentImageView = (UIImageView *)[cell.contentView viewWithTag:101];
    UILabel *contentLabel = (UILabel *)[cell.contentView viewWithTag:102];
    UIImageView *arrowView = (UIImageView *)[cell.contentView viewWithTag:103];
    arrowView.image = [UIImage imageNamed:@"blue_arrow"];
    
    NSArray *data=[ [tableData objectAtIndex:indexPath.section]  objectForKey:@"folders"];
    
    contentLabel.text=  [[data objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    cell.accoutid = [ [tableData objectAtIndex:indexPath.section]  objectForKey:@"id"];
    cell.folderid = [[ data objectAtIndex:indexPath.row] objectForKey:@"id"]   ;
    cell.isintranet =[[ [tableData objectAtIndex:indexPath.section]  objectForKey:@"intranet"] boolValue] ;
    cell.badgeColor=nil;
    cell.badgeString = nil;
    if([contentLabel.text isEqualToString:@"收件箱"]){

        cell.badgeString=  [[[data objectAtIndex:indexPath.row] objectForKey:@"count"] stringValue];
        contentImageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:   @"icon_mail_0.png" ]  ];
        cell.badgeColor=[UIColor redColor];
        if ([cell.badgeString isEqualToString:@"0"]) {
            cell.badgeString = nil;
        }
    }else if([contentLabel.text isEqualToString:@"草稿箱"]){
        contentImageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:   @"icon_mail_1.png" ]  ];
    }else if([contentLabel.text isEqualToString:@"已发送邮件箱"]){
        contentImageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:   @"icon_mail_2.png" ]  ];
    }else if([contentLabel.text isEqualToString:@"垃圾邮件箱"]){
        contentImageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:   @"icon_mail_3.png" ]  ];
    }else if([contentLabel.text isEqualToString:@"废件箱"]){
        contentImageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:   @"icon_mail_4.png" ]  ];
    }else{
        contentImageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:   @"icon_mail_0.png" ]  ];
    }
   
	return cell;
}
- (void)tableView:(UITableView *)tableViewx didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableViewx deselectRowAtIndexPath:indexPath animated:YES];
	[self setHidesBottomBarWhenPushed:NO ];
    MailBoxCell *cell = (MailBoxCell *)[self tableView:tableViewx cellForRowAtIndexPath:indexPath];
    MailFolderController *tdc=[[MailFolderController alloc] initWithNibName:nil bundle:nil];
    if ([cell.textLabel.text isEqualToString:@"收件箱"]) {
        tdc.isReply = YES;
    }
    
    [tdc setBoxid:cell.accoutid ];
    [tdc setFolderid: cell.folderid  ];
    [tdc setAccounts:[ [tableData objectAtIndex:indexPath.section]  objectForKey:@"accounts"] ];
//    NSLog(@"%@",[[tableData objectAtIndex:indexPath.section] objectAtIndex:@"accounts"]);
    [tdc setIsintranet: cell.isintranet ];
    [tdc setTitle:[NSString stringWithFormat:@"%@-%@",[[tableData objectAtIndex:indexPath.section] objectForKey:@"name"],cell.contentLabel.text] ];
    [self.navigationController pushViewController:tdc animated:YES];
    [tdc release];
    
}

#pragma mark - Private Method
- (void)pressHeaderBar:(UIButton *)myButton
{
    NSLog(@"点击段%d",myButton.tag);
    
    if ([[_switchArray objectAtIndex:myButton.tag] boolValue]) {
        
        //关上
        [_switchArray replaceObjectAtIndex:myButton.tag withObject:[NSNumber numberWithBool:NO]];
    }
    else {
        
        //打开
        [_switchArray replaceObjectAtIndex:myButton.tag withObject:[NSNumber numberWithBool:YES]];
    }
    
    //    [self.myTable reloadData];
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:myButton.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

//发起请求到服务器
-(void)requestJsonData
{
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice listMailBox];
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:  [SMFileUtils fullBundlePath:@"tipsbg.png" ]   ]];
    if (IOS_VERSIONS > 6.9) {
        tableView = [[UITableView alloc]  initWithFrame:CGRectMake(0,0, self.view.bounds.size.width ,self.view.bounds.size.height  - self.tabBarController.tabBar.frame.size.height- self.navigationController.navigationBar.frame.size.height-20  ) style: UITableViewStylePlain];
    }else {
        tableView = [[UITableView alloc]  initWithFrame:CGRectMake(0,0, self.view.bounds.size.width ,self.view.bounds.size.height  - self.tabBarController.tabBar.frame.size.height- self.navigationController.navigationBar.frame.size.height  ) style: UITableViewStylePlain];
    }
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    [tableView setDelegate:self];
    [tableView setDataSource:self];
    tableView.hidden = YES;
    // This should be set to work with the image height 
    [tableView setRowHeight:42];
    tableView.showsHorizontalScrollIndicator= NO;
    tableView.showsVerticalScrollIndicator=YES;
    tableView.scrollEnabled=YES;
    tableView.indicatorStyle =UIScrollViewIndicatorStyleDefault ;
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [tableView setIndicatorStyle:UIScrollViewIndicatorStyleBlack];
    [self.view addSubview:tableView];
    
    if (_refreshHeaderView == nil) {
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - tableView.bounds.size.height, self.view.frame.size.width, tableView.bounds.size.height)];
		view.delegate = self;
		[tableView addSubview:view];
		_refreshHeaderView = view;
		[view release];
	}
	//  update the last update date
	[_refreshHeaderView refreshLastUpdatedDate];
    
    //[self requestJsonData];
    /*
    UIBarButtonItem *rightBarButton = [[ UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(loadstockdata:)]; 
	self.navigationItem.rightBarButtonItem = rightBarButton;
    [rightBarButton release];
    */
    //self.tabBarItem.badgeValue = @"10" ;
}



- (void)requestDataFinished:(id)jsondata//request
{
    tableData=nil;
    tableData=[jsondata retain];
    [super requestDataFinished:jsondata];
#warning mail
//    NSLog(@"jsondata = %@",jsondata);
    
    //更新提示数字
    if(tableData&&[tableData count]>0){
        
        if (_switchArray == nil) {
            
            _switchArray = [[NSMutableArray alloc] initWithCapacity:[tableData count]];
            //设置开关
            for (NSInteger index = 0; index < [tableData count]; index++) {
                NSNumber *headSwitch = nil;
                if (index == 0) {
                    headSwitch = [NSNumber numberWithBool:YES];
                }else {
                    headSwitch = [NSNumber numberWithBool:NO];
                }
                [_switchArray addObject:headSwitch];
            }
        }
        tableView.hidden = NO;
        [tableView reloadData];
        
    }else{
        //self.tabBarItem.badgeValue =@"";
    }
}

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	[super egoRefreshTableHeaderDidTriggerRefresh:view];
	//[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    [self requestJsonData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:YES animated:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeStatusBarBlack" object:nil];
}



-(void)viewDidAppear:(BOOL)animated
{
    [self requestJsonData];
}

- (void)viewWillDisappear:(BOOL)animated { 
    [[JsonService sharedManager] cancelAllRequest];
    [self setHidesBottomBarWhenPushed:NO ];
    

} 

/*
 - (void)reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
 {
 animation =  UITableViewRowAnimationFade;
 }*/ 
@end
