//
//  KoronProjectDynamicCell.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-28.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectDynamicCell.h"

@interface dottedLine : UIView

@end

@implementation dottedLine

- (void)drawRect:(CGRect)rect
{
    CGContextRef context =UIGraphicsGetCurrentContext();
    CGContextBeginPath(context);
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    float lengths[] = {2,2};
    CGContextSetLineDash(context, 0, lengths,2);
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, 310.0,0);
    CGContextStrokePath(context);
//    CGContextClosePath(context);
}

@end

@implementation KoronProjectDynamicCell
{
    UIView *_frameView;
    UIButton *_dynamicButton;
    
    UIView *_leftLine;
    UIView *_rightLine;
    dottedLine *_dottedline;
    UIView *_dynamicBackgroundView;
    UILabel *_replyLabel;
    UILabel *_timeRightReply;
    BOOL _isRightOffset;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews
{
    _leftLine = [[UIView alloc] initWithFrame:CGRectZero];
    _leftLine.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
    [self.contentView addSubview:_leftLine];
    
    _rightLine = [[UIView alloc] initWithFrame:CGRectZero];
    _rightLine.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
    [self.contentView addSubview:_rightLine];
    
    _dynamicButton = [[UIButton alloc] initWithFrame:CGRectZero];
    _dynamicButton.layer.borderWidth = 1;
    _dynamicButton.layer.borderColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1].CGColor;
    [_dynamicButton setTitle:@"添加新动态" forState:UIControlStateNormal];
    [_dynamicButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [_dynamicButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    _dynamicButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_dynamicButton];
    
    _dottedline = [[dottedLine alloc] initWithFrame:CGRectMake(10, 0, 280, 1)];
    _dottedline.alpha = 0.3;
    [self.contentView addSubview:_dottedline];
    
    _dynamicBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    _dynamicBackgroundView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.8];
    _dynamicBackgroundView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:_dynamicBackgroundView];
    
    _iconView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _iconView.image = [UIImage imageNamed:@"default_head.jpg"];
    [_dynamicBackgroundView addSubview:_iconView];
    
    _replyContentLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _replyContentLabel.backgroundColor = [UIColor clearColor];
    _replyContentLabel.text = @"            回复              什么什么什么什么什么什么什么什么什么什么";
    _replyContentLabel.font = [UIFont systemFontOfSize:12];
    _replyContentLabel.numberOfLines = 0;
    [_dynamicBackgroundView addSubview:_replyContentLabel];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _nameLabel.textColor = [UIColor blueColor];
    _nameLabel.text = @"某某某:";
    _nameLabel.font = [UIFont systemFontOfSize:13];
    _nameLabel.backgroundColor = [UIColor clearColor];
    [_dynamicBackgroundView addSubview:_nameLabel];
    
    _replyLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _replyLabel.textColor = [UIColor blueColor];
    _replyLabel.text = @"某某某:";
    _replyLabel.backgroundColor = [UIColor clearColor];
    _replyLabel.font = [UIFont systemFontOfSize:13];
    [_dynamicBackgroundView addSubview:_replyLabel];
    
    _timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _timeLabel.text = @"11-23 17:56";
    _timeLabel.textColor = [UIColor grayColor];
    _timeLabel.font = [UIFont systemFontOfSize:12];
    _timeLabel.backgroundColor = [UIColor clearColor];
    [_dynamicBackgroundView addSubview:_timeLabel];
    
    _timeRightReply = [[UILabel alloc] initWithFrame:CGRectZero];
    _timeRightReply.text = @"回复";
    _timeRightReply.font = [UIFont systemFontOfSize:12];
    _timeRightReply.textColor = [UIColor blueColor];
    [_dynamicBackgroundView addSubview:_timeRightReply];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _leftLine.frame = CGRectMake(0, 0, 1, self.height);
    _rightLine.frame = CGRectMake(self.width-1, 0, 1, self.height);
    _dynamicButton.frame = CGRectMake(10, 10, self.width-20, self.height-20);
    _dynamicBackgroundView.frame = CGRectMake(20, 10, self.width - 40, self.height - 15);
    _iconView.frame = CGRectMake(0, 0, _dynamicBackgroundView.height, _dynamicBackgroundView.height);
    
    _nameLabel.frame = CGRectMake(_iconView.right+5, _iconView.top - 3, 45, _iconView.height/2.0);
    
    if (_isRightOffset == NO) {
        _replyContentLabel.frame = CGRectMake(_iconView.right + 5, _iconView.top, 230, _iconView.height/2.0);
        CGRect rect = CGRectMake(_iconView.right + 5, _iconView.top, 230, 10000);
        _replyContentLabel.frame = rect;
        [_replyContentLabel sizeToFit];
    } else {
        _replyContentLabel.frame = CGRectMake(_iconView.right + 5, _iconView.top, 160, _iconView.height/2.0);
        CGRect rect = CGRectMake(_iconView.right + 5, _iconView.top, 160, 10000);
        _replyContentLabel.frame = rect;
        [_replyContentLabel sizeToFit];
        
        _dynamicBackgroundView.frame = CGRectMake(70, 10, self.width - 40 - 50, self.height - 15);
    }
    
    
    _replyLabel.frame = CGRectMake(_nameLabel.right + 25, _nameLabel.top, 45, _iconView.height/2.0);

    _timeLabel.frame = CGRectMake(_iconView.right+5, _replyContentLabel.bottom, 100, _iconView.height/2.0);
    [_timeLabel sizeToFit];
    _timeLabel.center = CGPointMake(_iconView.right + 5 + _timeLabel.width / 2.0, _replyContentLabel.bottom + _timeLabel.height / 2.0 + 5);
    
    _timeRightReply.frame = CGRectMake(_timeLabel.right+5, _timeLabel.top, 30, _timeLabel.height);
}

- (void)changeDynamicFrame:(BOOL)isChange
{
    _isRightOffset = isChange;
}

- (void)setDottedLineHidden:(BOOL)isHidden
{
    if (isHidden == NO) {
        _dottedline.hidden = NO;
    } else {
        _dottedline.hidden = YES;
    }
}

- (void)setFrame:(CGRect)frame
{
    frame.origin.x += 10;
    frame.size.width -= 20;
    [super setFrame:frame];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (void)dealloc
{
    [_dynamicBackgroundView release], _dynamicBackgroundView = nil;
    [_leftLine release], _leftLine = nil;
    [_rightLine release], _rightLine = nil;
    [_dynamicButton release], _dynamicButton = nil;
    [_dottedline release], _dottedline = nil;
    [super dealloc];
}


@end
