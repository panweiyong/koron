//
//  KoronSuperclassViewController.h
//  Koron-mofffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//



//所有黑色状态栏的父类

#import <UIKit/UIKit.h>

@interface KoronSuperclassViewController : UIViewController {
    UIView *_statusBar;
    UIImageView *_topView;
    UILabel *_topLabel;
    UIButton *_dismissBtn;
}

@end
