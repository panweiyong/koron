//
//  BaseModel.m
//  moffice
//
//  Created by Mac Mini on 13-11-11.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

- (id)initWithContent:(NSDictionary *)json
{
    self = [super init];
    if (self) {
        
        [self setModelData:json];
        
    }
    return self;
}

- (SEL)setterMethod:(NSString *)key
{
    NSString *first = [[key substringToIndex:1] capitalizedString];
    NSString *end = [key substringFromIndex:1];
    NSString *setterName = [NSString stringWithFormat:@"set%@%@:",first, end];
    return NSSelectorFromString(setterName);
}

- (id)mapAttributes
{
    return nil;
}

- (void)setModelData:(NSDictionary *)json
{
    NSDictionary *dic = [self mapAttributes];
    
    for (id key in dic) {
        
        SEL sel = [self setterMethod:key];
        
        id jsonKey = [dic objectForKey:key];
        
        id jsonValue = [json objectForKey:jsonKey];
        
        [self performSelector:sel withObject:jsonValue];
        
    }
}

@end
