//
//  merchandiserViewController.h
//  moffice
//
//  Created by koron on 13-5-21.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import "BMapKit.h"
#import <QuartzCore/QuartzCore.h>
#import "GMGridView.h"
#import "GMGridViewLayoutStrategies.h"
#import "JsonService.h"
#import "QBImagePickerController.h"
#import "DropDown.h"
#import "PassvalueDelegate.h"
#import <CoreLocation/CoreLocation.h>

@interface merchandiserViewController : AbstractViewController<UITextFieldDelegate,UIScrollViewDelegate,UITextViewDelegate,BMKGeneralDelegate,BMKMapViewDelegate,BMKSearchDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,QBImagePickerControllerDelegate,pushDataDelegate,pushmerArrayDataDelegate>
{
   // id<PassValueDelegate>delegate;
    //UIScrollView *_merScrollView;
    UILabel *layLabel;
    UIImageView *logoView;
    UITextField *customerField;
    UITableView *customerTabview;
    UITextView *saysomeThing;
    BOOL textviewIsfirstedite;
    NSString *_customerID;
    NSMutableArray  *customerArray;
    UIButton *demandBtnp;
    
    UIButton *mapBtn;
    UIButton *detailbutton;
    CLLocationCoordinate2D startPt;
    CLLocation *whereLocation;
    NSString *whereAmI;
    UIView * backgroundView;
    UITableView *alertTabView;
    
    NSString *contentStr;
    NSString *customerStr;
    NSString *promptStr;
    NSString *netIsok;
    NSNumber *codeStr;
    
    UIScrollView *imageScrollview;
    UIImagePickerController *imagePick;
    NSMutableArray *imageArray;
    NSMutableArray *imageUrlArray;
    UIButton *imageAddbtn;
    int imageIndex;
    float keyboardHeigh;
    //数据交互,查询到数据以下拉框的形式显示
    
    NSString *customerLastID;
}
//@property(nonatomic,strong) id<PassValueDelegate>delegate;
//@property(nonatomic,retain) UIScrollView *merScrollView;
@property(nonatomic,retain) BMKMapView *littleMap;
@property(nonatomic,retain) BMKSearch *_search;
@property (retain, nonatomic)  NSString *contentStr;
@property (retain, nonatomic)  NSString *customerStr;
@property (retain, nonatomic)  NSString *promptStr;
@property (retain, nonatomic)  NSString *netIsok;
@property (retain, nonatomic)  NSNumber *codeStr; 
//@property (strong, nonatomic) CLLocationManager* locationManager;
@property(nonatomic,retain) UITableView *customerTabview;
@property (retain, nonatomic) UIActivityIndicatorView *activity1;
@property (retain, nonatomic) UIActivityIndicatorView *activity2;
@property (retain, nonatomic) UIActivityIndicatorView *activity3;
@property (retain, nonatomic) UIActivityIndicatorView *activity4;
@property (retain, nonatomic) NSString *customerLastID;
//@property (retain, nonatomic) ASIFormDataRequest *OARequest;

@property (nonatomic, assign) BOOL isNavigationPush;
@property (nonatomic, copy) NSString *customName;   //公司名字
@property (nonatomic, copy) NSString *customerID;

@end
