//
//  QueryViewController.h
//  moffice
//
//  Created by yangxi zou on 12-2-3.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JsonService.h"
#import "JsonUtils.h"
#import "CommonConst.h"
#import "ListViewController.h"
#import "QueryViewController.h"
#import "ModalAlert.h"

@interface QueryViewController : UITableViewController<UITextFieldDelegate,UIActionSheetDelegate>
{
    NSArray *tableData;
    NSString *target;
    UIView *footerView;
    NSString *moduleid;
}
@property (nonatomic,retain)NSArray *tableData;
@property (nonatomic,retain)NSString *target;
@property (nonatomic,retain)NSString *moduleid;

@end
