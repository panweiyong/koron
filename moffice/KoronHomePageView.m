//
//  KoronHomePageView.m
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronHomePageView.h"

@implementation KoronHomePageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        //屏蔽模块
       /*
        NSArray *homePageArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"HOMEPAGEARRAY"];
        NSArray *homePageImageArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"HOMEPAGEIMAGEARRAY"];
        NSArray *isOptionSelectedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"ISOPTIONSELECTEDARRAY"];
        
        if (homePageImageArray.count == 0 || homePageArray.count == 0 || isOptionSelectedArray.count == 0) {
            _homePageArray = [NSMutableArray arrayWithObjects:@"通讯录",@"工作计划",@"客户列表",@"销售跟单",@"客户移交",@"销售出库",@"发帖",@"工作审批",@"产品知识库",@"行程轨迹",@"待办",@"我的项目",@"编辑", nil];
            _homePageImageArray = [NSMutableArray arrayWithObjects:@"icon_contact.png",@"forum_test.png",@"icon_client.png",@"icon_sales_logistics.png",@"icon_transfer.png",@"icon_stock_removal.png",@"forum_test.png",@"icon_workflow.png",@"icon_information.png",@"icon_information.png",@"icon_information.png",@"icon_information.png",@"AddGroupMemberBtnHL",nil];
        }else {
            _homePageArray = [[NSMutableArray alloc] initWithCapacity:12];
            _homePageImageArray = [[NSMutableArray alloc] initWithCapacity:12];
            for (NSInteger index = 0; index < isOptionSelectedArray.count; index++) {
                if ([isOptionSelectedArray[index] boolValue]) {
                    [_homePageArray addObject:homePageArray[index]];
                    [_homePageImageArray addObject:homePageImageArray[index]];
                }
            }
            [_homePageArray addObject:@"编辑"];
            [_homePageImageArray addObject:@"AddGroupMemberBtnHL"];
        }
        */
        
        _homePageArray = [NSMutableArray arrayWithObjects:@"工作审批", @"销售跟单", nil];
        _homePageImageArray = [NSMutableArray arrayWithObjects:@"icon_workflow.png", @"icon_sales_logistics.png", nil];
        
        UIImage *image = [UIImage imageNamed:@"nearby_list_bg_img.png"];
        UIImage *backgroundImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        _backgroundImageView = [[UIImageView alloc]initWithImage:backgroundImage];
        _backgroundImageView.frame = self.bounds;
        [self addSubview:_backgroundImageView];
        
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(self.bounds.origin.x, self.bounds.origin.y + 20, self.bounds.size.width, self.bounds.size.height - 40)];
        if (_homePageArray.count > 9) {
            [_scrollView setContentSize:CGSizeMake(self.bounds.size.width * ([_homePageArray count] / 9 + 1), _scrollView.bounds.size.height)];
        }else {
            [_scrollView setContentSize:CGSizeMake(self.bounds.size.width, _scrollView.bounds.size.height)];
        }
        
        _scrollView.delegate = self;
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        _scrollView.pagingEnabled = YES;
        [self addSubview:_scrollView];
        
        NSInteger optionCount = 0;
        
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 3; j++) {
                if (optionCount >= [_homePageArray count]) {
                    break;
                }
                
                KoronHomePageOptionView *optionView = [[KoronHomePageOptionView alloc]initWithFrame:CGRectMake((j + (3 * (optionCount / 9))) * (_scrollView.frame.size.width / 3), (i % 3) * (_scrollView.frame.size.height / 3), self.frame.size.width / 3, 70)];
                
                optionView.optionLabel.text = [_homePageArray objectAtIndex:optionCount];
                [optionView.optionButton setImage:[UIImage imageNamed:[_homePageImageArray objectAtIndex:optionCount]] forState:UIControlStateNormal];
                [_scrollView addSubview:optionView];
                
                optionCount ++;
            }
        }
        
        if (_homePageArray.count > 9) {
            for (int i = 0;i < [_homePageArray count] / 9 + 1 ;i++ ) {
                UIImageView *pageImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
                if (i == 0) {
                    [pageImageView setImage:[UIImage imageNamed:@"page_pre.png"]];
                    [pageImageView setFrame:CGRectMake(self.frame.size.width / 2 - 8, _scrollView.frame.origin.y + _scrollView.frame.size.height, 12, 12)];
                }else {
                    [pageImageView setImage:[UIImage imageNamed:@"page_cur.png"]];
                    [pageImageView setFrame:CGRectMake(self.frame.size.width / 2 + 8, _scrollView.frame.origin.y + _scrollView.frame.size.height, 12, 12)];
                }
                pageImageView.tag = 200 + i;
                [self addSubview:pageImageView];
            }

        }
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    UIImageView *firstPageImageView = (UIImageView *)[self viewWithTag:200];
    UIImageView *secondImageView = (UIImageView *)[self viewWithTag:201];
    if (scrollView.contentOffset.x >= self.bounds.size.width) {
        [secondImageView setImage:[UIImage imageNamed:@"page_pre.png"]];
        [firstPageImageView setImage:[UIImage imageNamed:@"page_cur.png"]];
    }else {
        [secondImageView setImage:[UIImage imageNamed:@"page_cur.png"]];
        [firstPageImageView setImage:[UIImage imageNamed:@"page_pre.png"]];
    }
}

@end
