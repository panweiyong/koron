//
//  AppDelegate.h
//  moffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "KoronManager.h"
#import "KoronRootViewController.h"
#import "KoronLoginViewController.h"
#import "BMapKit.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,BMKGeneralDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) KoronManager *manager;
@property (strong, nonatomic)BMKMapManager *mapManager;

@end
