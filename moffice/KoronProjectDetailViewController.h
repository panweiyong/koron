//
//  KoronProjectDetailViewController.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-27.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronProjectModel.h"

@interface KoronProjectDetailViewController : UITableViewController

@property (nonatomic, retain) KoronProjectModel *detailItem;
@property (nonatomic, assign) NSInteger selectedIndex;

@end
