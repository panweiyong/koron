//
//  KoronProjectCell.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-25.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KoronProjectCell;

@protocol KoronProjectCellDelegate <NSObject>

- (void)KoronProjectCell:(KoronProjectCell *)cell didSelectItem:(NSString *)itemstring;
- (void)KoronProjectCellDidSelectUpView:(KoronProjectCell *)cell;

@end

@interface KoronProjectCell : UITableViewCell

@property (nonatomic, retain) UIImageView *statusPicture;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *executorLabel;
@property (nonatomic, retain) UILabel *remainingTimeLabel;
@property (nonatomic, retain) UILabel *introduceLabel;
@property (nonatomic, retain) UILabel *detailContentLabel;
@property (nonatomic, retain) UILabel *dynamicLabel;
@property (nonatomic, retain) UILabel *assignmentLabel;
@property (nonatomic, retain) UILabel *attachLabel;
@property (nonatomic, retain) UILabel *peopleLabel;


@property (nonatomic, assign) id <KoronProjectCellDelegate> delegate;

@end
