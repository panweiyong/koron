//
//  KoronHomePageViewSettingViewController.h
//  moffice
//
//  Created by CA on 13-11-3.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KoronHomePageViewSettingViewController : UITableViewController {
    NSMutableArray *_homePageArray;
    NSMutableArray *_homePageImageArray;
    NSMutableArray *_isOptionSelectedArray;
}

@end
