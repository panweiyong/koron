//
//  KoronClientListMenuViewController.h
//  moffice
//
//  Created by Mac Mini on 13-11-6.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronClientListMenuView.h"
#import "KoronMenuModel.h"

@protocol KoronClientListMenuViewControllerDelegate <NSObject>

- (void)changeTheType:(NSString *)type withTitle:(NSString *)title;

@end


@interface KoronClientListMenuViewController : UITableViewController {
    UIImageView *_backgroundImageView;
    NSMutableArray *_menuArr;
    NSMutableArray *_menuTypeArr;
    id<KoronClientListMenuViewControllerDelegate> _delegate;
}

@property (nonatomic,assign)id<KoronClientListMenuViewControllerDelegate> delegate;

-(void)setTableViewFrame:(CGRect)rect;

- (id)initWithStyle:(UITableViewStyle)style withModuleArray:(NSArray *)moduleArray;

@end
