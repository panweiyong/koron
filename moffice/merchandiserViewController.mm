//
//  merchandiserViewController.m
//  moffice
//
//  Created by koron on 13-5-21.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "merchandiserViewController.h"
//#import "MapViewController.h"
#import  <MobileCoreServices/UTCoreTypes.h>
#import "merImageViewController.h"
#import "GTMBase64.h"

BOOL isRetina1 = FALSE;
BOOL isCamer = NO;
@interface merchandiserViewController ()

@end

@implementation merchandiserViewController
//@synthesize delegate;
//@synthesize merScrollView=_merScrollView;
@synthesize activity1,activity2,activity3,activity4;
@synthesize customerTabview,littleMap,_search;
@synthesize contentStr,customerStr,promptStr,netIsok,codeStr,customerLastID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title=NSLocalizedString(@"The sales merchandiser",@"The sales merchandiser");
       // self.tabBarItem.image = [UIImage imageNamed:@"0"];
//        self.navigationItem.leftBarButtonItem.action=@selector(back:);
        [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(stopActivity2) name:@"stopActivity2" object:nil];
        [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(stopActivity3) name:@"stopActivity3" object:nil];
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_customName release], _customName = nil;
    [logoView release];
    [customerField release];
    [layLabel release];
    [saysomeThing release];
    [littleMap release];
    [imageScrollview release];
   // [self.merScrollView release];
    [activity1 release];
    [activity2 release];
    [activity3 release];
    [activity4 release];
    [customerTabview release];
    if (_search != nil) {
        [_search release];
        _search = nil;
    }
    if (littleMap) {
        littleMap.delegate = nil;
        [littleMap release];
        littleMap = nil;
    }
    
    [super dealloc];
}

-(void)viewWillAppear:(BOOL)animated
{
    //littleMap.showsUserLocation=YES;
    if (IOS_VERSIONS > 6.9) {
        
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    [self.navigationController setNavigationBarHidden:NO];
}



-(void)viewWillDisappear:(BOOL)animated
{
    //littleMap.showsUserLocation=NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadNavigationItem];
    
    
    customerArray=[[NSMutableArray alloc]initWithCapacity:0];
    [customerArray removeAllObjects];
    self.view.backgroundColor =  [UIColor whiteColor];
    layLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    layLabel.userInteractionEnabled=YES;
    [layLabel setBackgroundColor:[UIColor grayColor]];
    [self.view addSubview:layLabel];
    

    logoView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 7, 36, 36)];
    [logoView setImage:[UIImage imageNamed:@"org.png"]];
    [layLabel addSubview:logoView];

    customerField=[[UITextField alloc]initWithFrame:CGRectMake(50, 7, self.view.frame.size.width-100, 36)];
    customerField.tag=10020;
    customerField.userInteractionEnabled=YES;
    [customerField setBackgroundColor:[UIColor whiteColor]];
    [customerField setBorderStyle:UITextBorderStyleRoundedRect];
    customerField.placeholder=NSLocalizedString(@"Customer name", @"Customer name");
    customerField.textAlignment=NSTextAlignmentLeft;
    customerField.font = [UIFont fontWithName:@"helvetica" size:18.0];  //字体和大小设置
    customerField.adjustsFontSizeToFitWidth=YES;
    customerField.autoresizingMask=UIViewAutoresizingFlexibleHeight;
    customerField.enabled=YES;
//    customerField.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    customerField.keyboardAppearance = UIKeyboardAppearanceAlert ;
    customerField.returnKeyType = UIReturnKeyDone;
    customerField.delegate = self;
    [layLabel addSubview:customerField];
    
    
    if (_isNavigationPush) {
        customerField.text = _customName;
        customerStr = [_customName retain];
    }else {
        _customerID=nil;
    }
    
    demandBtnp=[UIButton buttonWithType:UIButtonTypeCustom];
    demandBtnp.frame=CGRectMake(self.view.frame.size.width-46, 7, 36, 36);
    //demandBtnp.backgroundColor=[UIColor colorWithRed:0.0/255.0 green:181.0/255.0 blue:244.0/255.0 alpha:1.0];
    [demandBtnp setBackgroundImage:[UIImage imageNamed:@"customer_search_bt.png"] forState:UIControlStateNormal];
    [demandBtnp setBackgroundImage:[UIImage imageNamed:@"customer_search_bt_pressed"] forState:UIControlStateHighlighted];
    [demandBtnp addTarget:self action:@selector(demandBtnTouched:) forControlEvents:UIControlEventTouchDown];
    [layLabel addSubview:demandBtnp];
    [layLabel bringSubviewToFront:demandBtnp];

    saysomeThing=[[UITextView alloc]initWithFrame:CGRectMake(10, 53, self.view.frame.size.width-20, self.view.frame.size.height-257)];
    saysomeThing.tag=10021;
    [saysomeThing setBackgroundColor:[UIColor whiteColor]];
    [saysomeThing setEditable:YES];
    saysomeThing.layer.backgroundColor = [[UIColor clearColor] CGColor];
    saysomeThing.layer.borderColor = [[UIColor blackColor] CGColor];
    saysomeThing.layer.borderWidth = 1.0;
    // saysomeThing.layer.cornerRadius = 8.0f;
    [saysomeThing.layer setMasksToBounds:YES];
    //    [saysomeThing setBorderStyle:UITextBorderStyleLine];
    saysomeThing.text=NSLocalizedString(@"Say something", @"Say something");
    saysomeThing.font = [UIFont fontWithName:@"Arial" size:18.0];
    saysomeThing.keyboardType=UIKeyboardTypeDefault;
    saysomeThing.returnKeyType = UIReturnKeyDone;//返回键的类型
    saysomeThing.autoresizingMask = UIViewAutoresizingFlexibleHeight;//自适应高度
    saysomeThing.delegate = self;
    textviewIsfirstedite=YES;
    [self.view addSubview:saysomeThing];
    
    UILabel *imageLabel=[[UILabel alloc]init];
    if (IOS_VERSIONS > 6.9) {
        imageLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,self.view.frame.size.height-157-44-20, self.view.frame.size.width-20,100)];
    }else {
        imageLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,self.view.frame.size.height-157-44, self.view.frame.size.width-20,100)];
    }

    imageLabel.userInteractionEnabled=YES;
    //设置layer
    CALayer *layer=[imageLabel layer];
    //是否设置边框以及是否可见
    [layer setMasksToBounds:YES];
    //设置边框圆角的弧度
    [layer setCornerRadius:10.0];
    //设置边框线的宽
    [layer setBorderWidth:1];
    //设置边框线的颜色
    [layer setBorderColor:[[UIColor blackColor] CGColor]];
    [self.view  addSubview:imageLabel];
    
    
    if (IOS_VERSIONS > 6.9) {
        imageScrollview=[[UIScrollView alloc]initWithFrame:CGRectMake(10,self.view.frame.size.height-157-44-20, self.view.frame.size.width-20, 100)];
    }else {
        imageScrollview=[[UIScrollView alloc]initWithFrame:CGRectMake(10,self.view.frame.size.height-157-44, self.view.frame.size.width-20, 100)];
    }
    imageScrollview.pagingEnabled=YES;
    imageScrollview.delegate=self;
    //self.merScrollView.indicatorStyle=UIScrollViewIndicatorStyleWhite;
    imageScrollview.showsVerticalScrollIndicator=YES;
    imageScrollview.scrollEnabled=YES;
    //imageScrollview.contentSize=CGSizeMake(self.view.frame.size.width,635);
    //    self.merScrollView.maximumZoomScale=2.0;
    //    self.merScrollView.minimumZoomScale=0.5;
    //    scrollview.decelerationRate=1;
    imageScrollview.delegate=self;
    //canCancelContentTouches:YES-移动手指足够长度触发滚动事件,NO-scrollView发送 tracking events 后，就算用户移动手指，scrollView也不会滚动。
    imageScrollview.canCancelContentTouches=NO;
    
    //当值是 YES的时候，用户触碰开始.要延迟一会，看看是否用户有意图滚动。假如滚动了，那么捕捉 touch-down事件，否则就不捕捉。假如值是NO，当用户触碰， scroll view会立即触发
    imageScrollview.delaysContentTouches=YES;
    [self.view  addSubview:imageScrollview];
    
    imageArray=[[NSMutableArray alloc]initWithCapacity:0];
    imageUrlArray = [[NSMutableArray alloc]initWithCapacity:0];
    
    
    imageAddbtn=[UIButton buttonWithType:UIButtonTypeCustom];
    imageAddbtn.frame=CGRectMake(5, 5, 90, 90);
    //[imageAddbtn setBackgroundColor:[UIColor clearColor]];
    [imageAddbtn setBackgroundImage:[UIImage imageNamed:@"qz_icon_add_photo_normal.png"] forState:UIControlStateNormal];
    [imageAddbtn setBackgroundImage:[UIImage imageNamed:@"qz_icon_add_photo_pressed.png"] forState:UIControlStateHighlighted];
    [imageAddbtn addTarget:self action:@selector(imageAddbtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [imageScrollview addSubview:imageAddbtn];
    
    
    mapBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    mapBtn.frame=CGRectMake(15, self.view.frame.size.height-84,self.view.frame.size.width-25,20);
    [self.view  addSubview:mapBtn];
    
    littleMap=[[BMKMapView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-130, 320, 120, 120)];
    [littleMap setShowsUserLocation:YES];
    littleMap.delegate=self;
    
    //NSLog(@"whereAmI=%@",whereAmI);
    
//    CLLocationCoordinate2D coor = [[littleMap.userLocation location] coordinate];
//    BMKCoordinateRegion viewRegion = BMKCoordinateRegionMake(coor, BMKCoordinateSpanMake(0.02f,0.02f));
//    BMKCoordinateRegion adjustedRegion = [littleMap regionThatFits:viewRegion];
//    [littleMap setRegion:adjustedRegion animated:YES];
    //[self.merScrollView addSubview:littleMap];
    //[self showWithlocation];
    
    
//    activity4=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0,0, 27, 27)];
//    [activity4 setCenter:CGPointMake(15, self.view.frame.size.height-194-44+27/2)];//指定进度轮中心点
//    [activity4 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
//    [self.view  addSubview:activity4];
//    [activity4 startAnimating];
    
    detailbutton = [[UIButton alloc] init];
    if (IOS_VERSIONS > 6.9) {
        detailbutton.frame=CGRectMake(25,self.view.frame.size.height-194-44-20,self.view.frame.size.width-55,27);
    }else {
        detailbutton.frame=CGRectMake(25,self.view.frame.size.height-194-44,self.view.frame.size.width-55,27);
    }
    
    // [detailbutton setBackgroundImage:[UIImage imageNamed:@"customer_search_bt.png"] forState:UIControlStateNormal];
    [detailbutton setTitle:@"获取地址" forState:UIControlStateNormal];
    CALayer *demandbtnlayer=[detailbutton layer];
    //是否设置边框以及是否可见
    [demandbtnlayer setMasksToBounds:YES];
    detailbutton.titleLabel.backgroundColor=[UIColor clearColor];
    detailbutton.titleLabel.font = [UIFont systemFontOfSize: 12.0];
    detailbutton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    detailbutton.contentEdgeInsets=UIEdgeInsetsMake(0,5, 0, 0);
    [detailbutton setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    [detailbutton addTarget:self action:@selector(detailbuttonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:detailbutton];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"whereAmI"]!=nil)
    {
         whereAmI=[[NSUserDefaults standardUserDefaults]objectForKey:@"whereAmI"];
        if (IOS_VERSIONS > 6.9) {
            detailbutton.frame=CGRectMake(10, self.view.frame.size.height-194-44-20, self.view.frame.size.width-55,27);
        }else {
            detailbutton.frame=CGRectMake(10, self.view.frame.size.height-194-44, self.view.frame.size.width-55,27);
        }
        [detailbutton setTitle:whereAmI forState:UIControlStateNormal];
        [detailbutton setImage:[UIImage imageNamed:@"icon_lbs_act.png"] forState:UIControlStateNormal];
        [detailbutton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-10,0.0,0.0)];
    }
    else
    {
        activity4=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0,0, 27, 27)];
        if (IOS_VERSIONS > 6.9) {
            [activity4 setCenter:CGPointMake(15, self.view.frame.size.height-194-44+27/2-20)];
        }else {
            [activity4 setCenter:CGPointMake(15, self.view.frame.size.height-194-44+27/2)];//指定进度轮中心点
        }
        [activity4 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
        [self.view  addSubview:activity4];
        [activity4 startAnimating];
        [detailbutton setTitle:@"获取地址" forState:UIControlStateNormal];
    }
        
    UIButton *okBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS > 6.9) {
       okBtn.frame=CGRectMake(15,self.view.frame.size.height-47-44-20,self.view.frame.size.width-30,45);
    }else {
        okBtn.frame=CGRectMake(15,self.view.frame.size.height-47-44,self.view.frame.size.width-30,45);
    }
    CALayer *btnlayer=[okBtn layer];
    [btnlayer setCornerRadius:5.0];
    [okBtn setBackgroundColor:[UIColor grayColor]];
    [okBtn setTitle:NSLocalizedString(@"To submit", @"To submit") forState:UIControlStateNormal];
    [okBtn addTarget:self action:@selector(okBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view  addSubview:okBtn];
    [imageLabel release];
	// Do any additional setup after loading the view.
}

- (void)merkeyboardWillShow:(NSNotification *)aNotification
{
    //获取键盘的高度

}

- (void)merkeyboardWillHide:(NSNotification *)aNotification
{
    //self.tabBarController.tabBar.frame=CGRectMake(0, 519, 320, 49);
}

-(void)layoutScrollview
{

}

#pragma mark -  实现 BMKMapViewDelegate 中的方法

/**
 在地图View将要启动定位时，会调用此函数
 @param mapView 地图View
 下面的这个方法，貌似并没有被启动啊？是否是可有可无的？
 */
- (void)mapViewWillStartLocatingUser:(BMKMapView *)mapView

{
	NSLog(@"start locate");
}

/**
 用户位置更新后，会调用此函数
 @param mapView 地图View
 @param userLocation 新的用户位置
 在实际使用中，只需要    [mapView setShowsUserLocation:YES];    mapView.delegate = self;   两句代码就可以启动下面的方法。
 疑问，为什么我的位置没有移动的情况下，这个方法循环被调用呢？
 */
- (void)mapView:(BMKMapView *)mapView didUpdateUserLocation:(BMKUserLocation *)userLocation
{
	if (userLocation != nil)
    {
		NSLog(@"%f %f", userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
        [littleMap setShowsUserLocation:YES];
        [[NSUserDefaults standardUserDefaults] setFloat:userLocation.location.coordinate.latitude forKey:@"startPt_latitude"];
        [[NSUserDefaults standardUserDefaults] setFloat:userLocation.location.coordinate.longitude forKey:@"startPt_longitude"];
        [[NSUserDefaults standardUserDefaults] synchronize];
       // CLLocationCoordinate2D nowlocation;
        startPt.latitude=userLocation.location.coordinate.latitude;
        startPt.longitude=userLocation.location.coordinate.longitude;
        [self setMapRegionWithCoordinate:startPt];
//        [littleMap setShowsUserLocation:NO];
	}
}

//传入经纬度,将baiduMapView 锁定到以当前经纬度为中心点的显示区域和合适的显示范围
- (void)setMapRegionWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    // coordinate.latitude=22.570662;
    //  coordinate.longitude=11.179366;
    BMKCoordinateRegion region;
    if (!isRetina1)//这里用一个变量判断一下,只在第一次锁定显示区域时 设置一下显示范围 Map Region
    {
        region = BMKCoordinateRegionMake(coordinate, BMKCoordinateSpanMake(0.002, 0.002));//越小地图显示越详细
        isRetina1 = YES;
        [littleMap setRegion:region animated:YES];//执行设定显示范围
        [self showWithlocation];
    }
    startPt= coordinate;
    [littleMap setCenterCoordinate:coordinate animated:YES];//根据提供的经纬度为中心原点 以动画的形式移动到该区域
}

//以后开始移动,当移动完成后,会执行以下委托
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    [littleMap.annotations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        BMKPointAnnotation *item = (BMKPointAnnotation *)obj;
        if (item.coordinate.latitude == startPt.latitude && item.coordinate.longitude == startPt.longitude )
        {
            [littleMap selectAnnotation:obj animated:YES];//执行之后,会让地图中的标注处于弹出气泡框状态
            *stop = YES;
        }
    }];
}


/**
 定位失败后，会调用此函数
 @param mapView 地图View
 @param error  错误号，参考CLError.h中定义的错误号
 */
- (void)mapView:(BMKMapView *)mapView didFailToLocateUserWithError:(NSError *)error

{
	if (error != nil)
		NSLog(@"locate failed: %@", [error localizedDescription]);
	else {
		NSLog(@"locate failed");
	}
}

//返回地址信息
- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error
{
    NSArray* array = [NSArray arrayWithArray:littleMap.annotations];
	[littleMap removeAnnotations:array];
	array = [NSArray arrayWithArray:littleMap.overlays];
	[littleMap removeOverlays:array];
	if (error == 0) {
		BMKPointAnnotation* item = [[BMKPointAnnotation alloc]init];
		item.coordinate = result.geoPt;
		item.title = result.strAddr;
        whereAmI=[NSString stringWithFormat:@"%@",result.strAddr];
        if(whereAmI==nil)
        {
            [detailbutton setTitle:@"获取地理位置信息失败" forState:UIControlStateNormal];
            
        } 
        else
        {
            [[NSUserDefaults standardUserDefaults] setObject:whereAmI forKey:@"whereAmI"];
            [[NSUserDefaults standardUserDefaults] synchronize];
             detailbutton.frame=CGRectMake(10, self.view.frame.size.height-194, self.view.frame.size.width-55,27);
            [detailbutton setTitle:whereAmI forState:UIControlStateNormal];
            [detailbutton setImage:[UIImage imageNamed:@"icon_lbs_act.png"] forState:UIControlStateNormal];
            [detailbutton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-10,0.0,0.0)];

        }
        [activity4 stopAnimating];
        [activity4 removeFromSuperview];
        [activity4 release];
		[littleMap addAnnotation:item];
		[item release];
	}
}

//将位置信息转化为地理信息
- (void)showWithlocation
{
      whereAmI=nil;
      _search = [[BMKSearch alloc]init];
      _search.delegate=self;
      NSLog(@"starPt=%f,%f",startPt.latitude,startPt.longitude);
	  BOOL flag = [_search reverseGeocode:startPt];
	  if (!flag)
      {
		NSLog(@"search failed!");
        [detailbutton setTitle:@"获取地理位置信息失败" forState:UIControlStateNormal];
        [detailbutton setImage:nil forState:UIControlStateNormal];
	 }
    //[_search release];
        //添加图标
//        UIImageView *maplogoView=[[UIImageView alloc]initWithFrame:CGRectMake(15, 370, 27, 27)];
//        [maplogoView setImage:[UIImage imageNamed:@"icon_lbs_act.png"]];
//        [self.merScrollView addSubview:maplogoView];
         //按钮
       // NSString *btnTitle=[NSString stringWithFormat:@"%@%@%@%@%@%@",area,subarea,city,subcity,street,substreet];
        //设置layer
}


//响应函数
#pragma mark - demandBtnTouched
-(void)demandBtnTouched:(id)sender
{
    [customerField resignFirstResponder];
    [customerArray removeAllObjects];
    
    UIView *mainBackView=[[UIView alloc]initWithFrame:self.view.bounds];
    [mainBackView setBackgroundColor:[UIColor clearColor]];
    mainBackView.tag=607;
    [self.view addSubview:mainBackView];
    
    UIView *backview=[[UIView alloc]initWithFrame:CGRectMake(110, self.view.frame.size.height/2-40, 100, 80)];
    [backview setBackgroundColor:[UIColor grayColor]];
    backview.layer.cornerRadius=5.0f;
    backview.tag=608;
    [self.view addSubview:backview];
    
    activity2 = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];//指定进度轮的大小
    [activity2 setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];//指定进度轮中心点
    [activity2 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];//设置进度轮显示类型
    [self.view addSubview:activity2];
    
    [activity2 startAnimating];
    
    JsonService *jservice=[JsonService sharedManager];
    
    
    [jservice setDelegate:self];
    jservice.MerArrayDelegate=self;
    [jservice findCustomer:customerStr];
    
}


-(void)addmoreCustomerInfo:(id)sender
{
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    if([customerArray count]!=0)
    {
        customerLastID=[[customerArray objectAtIndex:[customerArray count]-1] objectForKey:@"id"];
        UIView *mainBackView=[[UIView alloc]initWithFrame:self.view.bounds];
        [mainBackView setBackgroundColor:[UIColor clearColor]];
        mainBackView.tag=904;
        [self.view addSubview:mainBackView];
        
        UIView *backview=[[UIView alloc]initWithFrame:CGRectMake(110, self.view.frame.size.height/2-40, 100, 80)];
        [backview setBackgroundColor:[UIColor grayColor]];
        backview.layer.cornerRadius=5.0f;
        backview.tag=905;
        [self.view addSubview:backview];
        
        UIActivityIndicatorView *activity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activity.center=CGPointMake(160, self.view.frame.size.height/2);
        activity.tag=906;
        [self.view addSubview:activity];
        [activity startAnimating];
        [jservice findMoreCustomer:customerStr lastId:customerLastID];
    }
    else
    {
        [jservice findCustomer:customerStr];
    }
}

-(void)requestDataFinished:(id)jsondata
{
    if ([jsondata isKindOfClass:[NSDictionary class]])
    {
        NSArray *dataArray=[jsondata objectForKey:@"list"];
        if([dataArray count]==0)
        {
            UILabel *tellLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height-90, 100, 50)];
            tellLabel.text=@"数据加载已完成";
            tellLabel.adjustsFontSizeToFitWidth=YES;
            tellLabel.layer.cornerRadius=5.0f;
            tellLabel.backgroundColor=[UIColor grayColor];
            tellLabel.textColor=[UIColor whiteColor];
            [self.view addSubview:tellLabel];
            [self performSelector:@selector(removeLabel:) withObject:tellLabel afterDelay:1.0f];
        }
        [customerArray addObjectsFromArray:dataArray];
    }
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==904||view.tag==905||view.tag==906)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
    [customerTabview reloadData];
}

-(void)removeLabel:(UILabel*)label
{
    [label removeFromSuperview];
    [label release];
}

#pragma mark - stopActivity2
-(void)stopActivity2
{
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"netIsok"] isEqualToString:@"NO"])
    {
        [activity2 stopAnimating];
        return;
    }
    [activity2 stopAnimating];
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==608||view.tag==607)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"existCustomer"] isEqualToString:@"NO"])
    {
        UILabel *resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-40, 300, 80, 40)];
        resultLabel.backgroundColor=[UIColor grayColor];
        resultLabel.text=@"无查询结果";
        resultLabel.textAlignment=NSTextAlignmentCenter;
        resultLabel.font=[UIFont systemFontOfSize:12.0];
        resultLabel.textColor=[UIColor whiteColor];
        CALayer *labelLayer=[resultLabel layer];
        labelLayer.masksToBounds=YES;
        labelLayer.cornerRadius=5.0;
        [self.view addSubview:resultLabel];
        [self performSelector:@selector(LabelremoveSubview:) withObject:resultLabel afterDelay:1.0];
        for(UITableView *table in [self.view subviews])
        {
            if(table.tag==10086)
            {
                [table removeFromSuperview];
            }
        }
    }
    else
    {
        if (nil != customerTabview) {
            [customerTabview removeFromSuperview];
            [customerTabview setDelegate:nil];
            [customerTabview setDataSource:nil];
            [customerTabview release];
            customerTabview = nil;
        }
        customerTabview=[[UITableView alloc]init];
        customerTabview.frame=CGRectMake(0, 43, self.view.frame.size.width, self.view.frame.size.height-43);
        customerTabview.scrollEnabled=YES;
        customerTabview.tag=10086;
        customerTabview.delegate=self;
        customerTabview.dataSource=self;
        [self.view addSubview:customerTabview];
    }
}

-(void)dataCallBack:(NSString *)str1 other:(NSString *)str2 third:(NSNumber *)str3
{
    netIsok=[str1 retain];
    promptStr=[str2 retain];
    codeStr=[str3 retain];
}

#pragma mark - merarrayDataCallBack
-(void)merarrayDataCallBack:(NSArray *)arr
{
    if (customerArray != nil) {
        [customerArray release];
        customerArray = nil;
    }
    customerArray = [[NSMutableArray alloc] initWithArray:arr];
    NSLog(@"customerArray   ==   %@",arr);
    for(UITableView *table in [self.view subviews])
    {
        if(table.tag==10086)
        {
            [table reloadData];
            break;
        }
    }
    //[customerTabview reloadData];
    NSLog(@"customerArray count=%d",[customerArray count]);
}

-(void)stopActivity3
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==97||view.tag==98)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
    if([netIsok isEqualToString:@"NO"])
    {
        [activity3 stopAnimating];
        return;
    }
    [activity3 stopAnimating];
    if([codeStr intValue]>0||[codeStr intValue]==0)
    {
        customerField.text=nil;
        [_customerID release];
        _customerID=nil;
        [customerStr release];
        customerStr=nil;
        contentStr=nil;
        saysomeThing.text=nil;
        for(UIImageView* imageView in [imageScrollview subviews])
        {
            //UIImage *image=
            if([imageArray containsObject:imageView.image])
            {
                [imageView removeFromSuperview];
            }
        }
        [imageArray removeAllObjects];
        imageAddbtn.frame=CGRectMake(7, 5, 90, 90);
    }
    [self displayLabel];  
}

-(void) displayLabel
{
    UILabel *resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 310, 100, 40)];
    resultLabel.backgroundColor=[UIColor grayColor];
    resultLabel.textAlignment=NSTextAlignmentCenter;
    resultLabel.font=[UIFont systemFontOfSize:14.0];
    resultLabel.text=promptStr;
    resultLabel.textColor=[UIColor whiteColor];
    CALayer *labelLayer=[resultLabel layer];
    labelLayer.masksToBounds=YES;
    labelLayer.cornerRadius=5.0;
    [self.view addSubview:resultLabel];
    [self performSelector:@selector(LabelremoveSubview:) withObject:resultLabel afterDelay:1.0];
}


-(void) LabelremoveSubview:(UILabel *)label
{
    [label removeFromSuperview];
    [label release];
}

-(void)imageAddbtnTouched:(id)sender
{
    if([imageArray count]>=3)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"最多只能添加三张照片" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    backgroundView=[[UIView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:backgroundView];
    alertTabView=[[UITableView alloc]initWithFrame:CGRectMake(20, 120, self.view.frame.size.width-40, 180)];
    alertTabView.tag=10085;
    alertTabView.dataSource=self;
    alertTabView.delegate=self;
    alertTabView.scrollEnabled=NO;
    alertTabView.layer.borderWidth = 1;
    alertTabView.backgroundView.backgroundColor=[UIColor blackColor];
    alertTabView.layer.borderColor = [[UIColor blackColor] CGColor];
    [self.view addSubview:alertTabView];
    [self.view bringSubviewToFront:alertTabView];
}


-(void)detailbuttonTouched:(id)sender
{
//     MapViewController *mapView=[[MapViewController alloc]init];
//     [mapView setWhereAmI:whereAmI];
//     mapView.hidesBottomBarWhenPushed=YES;
//     [self.navigationController pushViewController:mapView animated:YES];
}

//textview委托方法
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];
    if(textviewIsfirstedite)
    {
        textView.text=nil;
        textviewIsfirstedite=NO;
    }

}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    contentStr=[textView.text retain];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

//委托方法
-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    [customerField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    customerStr=[textField.text retain];
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}


//按下Done按钮的调用方法，我们让键盘消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    //self.tabBarController.tabBar.frame=CGRectMake(0, 519, 320, 49);
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//UITableView 委托方法
- (NSInteger)tableView:(UITableView *)tableView1 numberOfRowsInSection:(NSInteger)section {
    if (tableView1.tag==10085)
    {
        return 3;
    }
    else if(tableView1.tag==10086)
    {
        return [customerArray count]+1;
    }else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if(tableView1.tag==10085)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
         if(indexPath.row==0)
         {
          cell.textLabel.text=@"选择";
          cell.textLabel.textColor=[UIColor whiteColor];
          cell.textLabel.backgroundColor=[UIColor clearColor];
          UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
          backgrdView.backgroundColor = [UIColor blackColor];
          cell.backgroundView = backgrdView;
         }
         else if(indexPath.row==1)
         {
            cell.textLabel.text=@"相机拍摄";
         }
         else if(indexPath.row==2)
         {
            cell.textLabel.text=@"手机相册";
         }
         return cell;
    }
    else if(tableView1.tag==10086)
    {
       if(indexPath.row==[customerArray count])
       {
           static NSString *CellIdentifier = @"mCell";
           UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
           if (cell == nil) {
               cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
           }
           for(UIButton *btn in [cell.contentView subviews])
           {
               [btn removeFromSuperview];
           }
           UIButton *moreBtn=[UIButton buttonWithType:UIButtonTypeCustom];
           moreBtn.frame=CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
           [moreBtn setBackgroundColor:[UIColor clearColor]];
           [moreBtn setTitle:@"点击加载更多" forState:UIControlStateNormal];
           [moreBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
           [moreBtn addTarget:self action:@selector(addmoreCustomerInfo:) forControlEvents:UIControlEventTouchUpInside];
           [cell.contentView addSubview:moreBtn];
           return cell;
       }
       else
       {
           static NSString *CellIdentifier = @"nomalCell";
           UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
           if (cell == nil) {
               cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
           }
           cell.selectionStyle=UITableViewCellSelectionStyleNone;
           NSDictionary *cellDic=[customerArray objectAtIndex:indexPath.row];
           cell.textLabel.text=[cellDic objectForKey:@"name"];
           return cell;
       }
    }
    return nil;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    return indexPath;
}

- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView1.tag==10085)
    {
        return 60;
    }
    else if(tableView1.tag==10086)
    {
        return 44;
    }
    return 44;
}

#pragma mark - tableview
- (void) tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPat
{
   if(tableView1.tag==10085)
   {
       if(indexPat.row==1)
       {
        [self cameraPicture];
       }
       else if(indexPat.row==2)
       {
        [self takePicture];
       }
       [alertTabView removeFromSuperview];
       [alertTabView release];
       [backgroundView removeFromSuperview];
       [backgroundView release];
   }
   else if(tableView1.tag==10086)
   {
       if(indexPat.row==[customerArray count])
       {
           return;
       }
       customerField.text=[[customerArray objectAtIndex:indexPat.row] objectForKey:@"name"];
       if (_customerID != nil) {
           [_customerID release];
           _customerID = nil;
       }
       _customerID = [[[customerArray objectAtIndex:indexPat.row]objectForKey:@"id"] copy];
       if (customerStr != nil) {
           [customerStr release];
           customerStr = nil;
       }
       customerStr=[[[customerArray objectAtIndex:indexPat.row] objectForKey:@"name"] copy];
       
       [tableView1 removeFromSuperview];
   }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITableView *table in [self.view subviews])
    {
        if(table.tag==10085)
        {
            UITouch *touch=[touches anyObject];
            CGPoint currentPoint=[touch locationInView:backgroundView];
            if(!CGRectContainsPoint(table.frame, currentPoint))
            {    
                [table removeFromSuperview];
                [table release];
                table=nil;
                [backgroundView removeFromSuperview];
                [backgroundView release];
                backgroundView=nil;
            }
        }
    }
}

-(void)takePicture
{
    
    QBImagePickerController *imagePickerController = [[QBImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.limitsMaximumNumberOfSelection=YES;
    imagePickerController.maximumNumberOfSelection=3-[imageArray count];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
    [self presentViewController:navigationController animated:YES completion:NULL];
    [imagePickerController release];
    [navigationController release];
}

-(void)cameraPicture
{
    //检查相机模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        NSLog(@"sorry, no camera or camera is unavailable!");
        return;
    }
    //获得相机模式下支持的媒体类型
    NSArray* availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    BOOL canTakePicture = NO;
    for (NSString* mediaType in availableMediaTypes) {
        if ([mediaType isEqualToString:(NSString*) kUTTypeImage]) {
            //支持拍照
            canTakePicture = YES;
            break;
        }
    }
    //检查是否支持拍照
    if (!canTakePicture) {
        NSLog(@"sorry, taking picture is not supported.");
        return;
    }
    isCamer=YES;
    //创建图像选取控制器
    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    //设置图像选取控制器的来源模式为相机模式
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    //设置图像选取控制器的类型为静态图像
    imagePickerController.mediaTypes = [[[NSArray alloc] initWithObjects:(NSString*)kUTTypeImage, nil] autorelease];
    //允许用户进行编辑
    imagePickerController.allowsEditing = YES;
    //设置委托对象
    imagePickerController.delegate = self;
    //以模视图控制器的形式显示
    [self presentModalViewController:imagePickerController animated:YES];
    [imagePickerController release];
    
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       

//pragma mark - QBImagePickerControllerDelegate

- (void)imagePickerController:(id)imagePickerController didFinishPickingMediaWithInfo:(id)info
{
    if ([imageArray count]>3||[imageArray count]==3)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"最多添加三张照片" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    UIView *mainScreen=[[UIView alloc]initWithFrame:self.view.bounds];
    [mainScreen setBackgroundColor:[UIColor clearColor]];
    mainScreen.tag=5001;
    [self.view addSubview:mainScreen];
    
    UILabel *backLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-60, self.view.frame.size.height/2-30, 120, 60)];
    [backLabel setBackgroundColor:[UIColor grayColor]];
    backLabel.textAlignment=NSTextAlignmentRight;
    backLabel.textColor=[UIColor whiteColor];
    backLabel.layer.cornerRadius=5.0f;
    backLabel.tag=5002;
    [self.view addSubview:backLabel];
    
    UIActivityIndicatorView *acView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    acView.center=CGPointMake(27/2, self.view.frame.size.height/2);
    acView.tag=5003;
    [acView startAnimating];
    [self.view addSubview:acView];
    
    
    if([imagePickerController isKindOfClass:[QBImagePickerController class]])
    {
     QBImagePickerController* imagePickerControllers=(QBImagePickerController*)imagePickerController;
     if(imagePickerControllers.allowsMultipleSelection)
     {
        NSArray *mediaInfoArray = (NSArray *)info;
        //NSLog(@"Selected %d photos", mediaInfoArray.count);
        //打印出字典中的内容
       // NSLog(@"get the media info: %@", info);
        for(int i=0;i<[mediaInfoArray count];i++)
        {
            NSDictionary *imageDic=[mediaInfoArray objectAtIndex:i];
            UIImage *originImage = [imageDic valueForKey:UIImagePickerControllerOriginalImage];
            //获取图片路径
            NSString *imagerUrl=[imageDic objectForKey:UIImagePickerControllerReferenceURL];
            if(originImage!=nil&&imagerUrl!=nil)
            {
                if(![imageArray containsObject:originImage])
                {
                    [imageArray addObject:originImage];
//                    [imageUrlArray addObject:imagerUrl];
//                    UIImage *newImage=[self imageByScalingAndCroppingForSize:CGSizeMake(originImage.size.width*0.5, originImage.size.height*0.5) OriginImage:originImage];
//                    NSData *imageData=UIImageJPEGRepresentation(newImage,1.0);
//                    NSString* imageString = [[NSString alloc]initWithData:[GTMBase64 encodeData:imageData]encoding:NSUTF8StringEncoding];
//                    //NSLog(@"imageString=%@",imageString);
//                    if(imageString!=nil&&![imageStrArray containsObject:imageString])
//                    {
//                        [imageStrArray addObject:imageString];
//                    }
                }
            }
         }
      }
    }
    else if([imagePickerController isKindOfClass:[UIImagePickerController class]])
    {
        NSDictionary *mediaInfo = (NSDictionary *)info;
        NSLog(@"Selected: %@", mediaInfo);
        UIImage *originImage = [mediaInfo valueForKey:UIImagePickerControllerOriginalImage];
        UIImageWriteToSavedPhotosAlbum(originImage, nil, nil, nil);
        if(originImage!=nil&&![imageArray containsObject:originImage])
        {
            [imageArray addObject:originImage];
//            UIImage *newImage=[self imageByScalingAndCroppingForSize:CGSizeMake(originImage.size.width*0.5, originImage.size.height*0.5) OriginImage:originImage];
//            NSData *imageData=UIImageJPEGRepresentation(newImage,1.0);
//            NSString* imageString = [[NSString alloc]initWithData:[GTMBase64 encodeData:imageData]encoding:NSUTF8StringEncoding];
//            //NSLog(@"imageString=%@",imageString);
//            if(imageString!=nil&&![imageStrArray containsObject:imageString])
//            {
//                [imageStrArray addObject:imageString];
//            }
        }
    }
    [self displayImage:imageArray];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Cancelled");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSString *)descriptionForSelectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"选择所有照片";
}

- (NSString *)descriptionForDeselectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"所有照片的选择解除";
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos
{
    return [NSString stringWithFormat:@"照片%d张", numberOfPhotos];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"录像%d", numberOfVideos];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos numberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"照片%d张、录像%d", numberOfPhotos, numberOfVideos];
}
//

-(void)displayImage:(NSArray*)imageArr
{
    for(UIImageView* imageView in [imageScrollview subviews])
    {
        //UIImage *image=
        if([imageArray containsObject:imageView.image])
        {
            [imageView removeFromSuperview];
        }
    }
    for(int i=0;i<[imageArr count];i++)
    {
        UIImageView *imageView=[[UIImageView alloc]init];
        CGRect viewFrame=CGRectMake(5+97*i, 5, 95, 90);
        imageView.frame=viewFrame;
        imageView.userInteractionEnabled=YES;
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressImageView:)];
        imageView.tag=i;
        [imageView addGestureRecognizer:longPress];
        
        UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapPressImageView:)];
        [imageView addGestureRecognizer:tapPress];
        
        [imageView setImage:[imageArr objectAtIndex:i]];
        [imageScrollview addSubview:imageView];
        [imageView release];
        [longPress release];
    }
    imageAddbtn.frame=CGRectMake(7+98*[imageArr count], 5, 95, 90);
    imageScrollview.contentSize=CGSizeMake(105+[imageArr count]*100,100);
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==5001||view.tag==5002||view.tag==5003)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}

-(void)longPressImageView:(UITapGestureRecognizer*)paramSender
{
    if(paramSender.state==UIGestureRecognizerStateEnded)
    {
      NSLog(@"long press!");
      CGPoint touchPoint = [paramSender locationInView:imageScrollview];
      NSLog(@"%f,%f",touchPoint.x,touchPoint.y);
      imageIndex=(touchPoint.x-5)/97;
      UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Confirm Delete",@"Confirm Delete") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel") otherButtonTitles:NSLocalizedString(@"OK",@"OK"), nil];
      [alert show];
      [alert release];
    }
}

-(void)tapPressImageView:(UILongPressGestureRecognizer*)paramSender
{
    NSLog(@"long press!");
    CGPoint touchPoint = [paramSender locationInView:imageScrollview];
    NSLog(@"%f,%f",touchPoint.x,touchPoint.y);
    imageIndex=(touchPoint.x-5)/97;
    merImageViewController *bigImageView=[[merImageViewController alloc]init];
    bigImageView.bigImage=[imageArray objectAtIndex:imageIndex];
    bigImageView.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:bigImageView animated:YES];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
      for(UIImageView* imageView in [imageScrollview subviews])
      {
          //UIImage *image=
          if([imageArray containsObject:imageView.image])
          {
            [imageView removeFromSuperview];
          }
      }
      [imageArray removeObjectAtIndex:imageIndex];
      //[imageUrlArray removeObjectAtIndex:imageIndex];
       NSLog(@"%d",[imageArray count]);
      [self displayImage:imageArray];
     // [imageAddbtn addSubview:imageScrollview];
    }
}

-(void)setBtnbackColor:(UIButton*)sender
{
    [sender setBackgroundColor:[UIColor grayColor]];
}

-(void)okBtnTouched:(id)sender
{
    if (nil != [[JsonService sharedManager] pushJson]) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"正在提交销售跟单.." delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
        [alertView release];
        return;
    }
    
    
    [[[JsonService sharedManager]MERRquest]clearDelegatesAndCancel];
    
    
    UIButton *btn=(UIButton*)sender;
    [btn setBackgroundColor:[UIColor blueColor]];
    [self performSelector:@selector(setBtnbackColor:) withObject:btn afterDelay:1.0f];
    
    UIView *mainScreenView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    mainScreenView.backgroundColor=[UIColor clearColor];
    mainScreenView.tag=97;
    UIView *backView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height/2-40, 100, 80)];
    backView.tag=98;
    backView.backgroundColor=[UIColor grayColor];
    backView.layer.cornerRadius=10.0;
    [self.view addSubview:backView];
    
    activity3 = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 37, 37)];//指定进度轮的大小
    [activity3 setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];//指定进度轮中心点
    [activity3 setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];//设置进度轮显示类型
    [self.view addSubview:activity3];
    [activity3 startAnimating];
    
     if(_customerID==nil)
    {
        [activity3 stopAnimating];
        promptStr=[NSString stringWithFormat:@"%@",@"客户不能为空"];
        for(UIView *view in [self.view subviews])
        {
            if(view.tag==97||view.tag==98)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
        [self displayLabel];
        return;
    }
    else if(contentStr==nil||[contentStr length]==0)
    {
        [activity3 stopAnimating];
        promptStr=[NSString stringWithFormat:@"%@",@"内容不能为空"];
        for(UIView *view in [self.view subviews])
        {
            if(view.tag==97||view.tag==98)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
        [self displayLabel];
        return;
    }
    else
    {
        [self.view addSubview:mainScreenView];
        JsonService *jservice=[JsonService sharedManager];
        [jservice setDelegate:self];
        jservice.pushDelegate=self;
//        NSString *customerName=[[NSUserDefaults standardUserDefaults] wxq@koronsoft.com Koron168 objectForKey:@"customerFieldtext"];
         NSMutableDictionary *jsonDir=[[NSMutableDictionary alloc]init];
        [jsonDir setValue:_customerID forKey:@"clientId"];
        [jsonDir setValue:contentStr forKey:@"content"];
        NSMutableArray *imageStrArray=[[NSMutableArray alloc]initWithCapacity:0];
        for(int i=0;i<[imageArray count];i++)
        {
            UIImage *image=[imageArray objectAtIndex:i];
           // NSLog(@"%f*%f",image.size.width,image.size.height);
           // NSLog(@"image.length=%f",image.scale);
           // UIImage *newImage=[self imageByScalingAndCroppingForSize:CGSizeMake(image.size.width*0.5, image.size.height*0.5) OriginImage:image];
           // UIImageWriteToSavedPhotosAlbum(newImage, nil, nil, nil);
           // NSLog(@"%f*%f",newImage.size.width,newImage.size.height);
           // NSLog(@"image.length=%f",newImage.scale);
            //UIImage *image=[UIImage imageNamed:@"Default.png"];    
           // NSData *imageData=UIImagePNGRepresentation(image);
            NSData *imageData = UIImageJPEGRepresentation(image,1.0);
            for(;;)
            {
                if(imageData.length/3364>40)
                {
                    image=[self imageByScalingAndCroppingForSize:CGSizeMake(image.size.width*0.5, image.size.height*0.5) OriginImage:image];
                    imageData=UIImageJPEGRepresentation(image, 1.0);
                }
                else
                {
                    break;
                }
            }
           // NSLog(@"newImage.memory＝%d",imageData.length/3364);
//            if(imageData.length/3364>40)
//            {
//                UIImage *fNewImage=[self imageByScalingAndCroppingForSize:CGSizeMake(image.size.width*0.5, image.size.height*0.5) OriginImage:newImage];
//                imageData = UIImageJPEGRepresentation(fNewImage,1.0);
//            }
            //NSData *imageData＝[[NSData alloc]initWithData:UIImagePNGRepresentation([imageArray objectAtIndex:i])];
           // imageData=[GTMBase64 encodeData:imageData];
          //NSString *imageString = [GTMBase64 stringByEncodingData:imageData];
            NSString* imageString = [[NSString alloc]initWithData:[GTMBase64 encodeData:imageData]encoding:NSUTF8StringEncoding];
            if(imageString!=nil&&![imageStrArray containsObject:imageString])
            {
                [imageStrArray addObject:imageString];
            }
        }
        [jsonDir setValue:imageStrArray forKey:@"locImage"];
        [jsonDir setValue:whereAmI forKey:@"location"];
         NSString *jsonStr=[jsonDir JSONRepresentation];
      //   NSLog(@"%@",jsonStr);
        
        [jservice pushDate:jsonStr];
        jservice.pushJson = [jsonStr copy];
        
        
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize OriginImage:(UIImage *)images
{
    UIImage *sourceImage = images;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}

- (void)backButtonPressed {
    
    if (_isNavigationPush) {
        [self.navigationController popViewControllerAnimated:YES];
    }else {
        [self.navigationController dismissModalViewControllerAnimated:YES];
    }
    
    [littleMap setShowsUserLocation:NO];
}
@end
