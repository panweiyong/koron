//
//  KoronProjectDownloadCell.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-28.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KoronProjectDownloadCell : UITableViewCell

- (void)startActivityIndicatorView;
- (void)stopActivityIndicatorView;

@end
