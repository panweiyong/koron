//
//  KoronRootViewController.h
//  moffice
//
//  Created by Mac Mini on 13-9-27.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronStartAnimationViewController.h"
#import "KoronManager.h"
#import "SVProgressHUD.h"
#import "ASIFormDataRequest.h"
#import "KoronRootTabBarContller.h"
#import "KoronHomePageViewController.h"
#import "KoronSettingViewController.h"
#import "KoronPrivateMessageViewController.h"
#import "KoronLocusViewController.h"
#import "KoronNotificationViewController.h"
#import "connectViewController.h"
#import "WorkPlanViewController.h"
//#import "KoronMailViewController.h"
#import "MailViewController.h"
#import "BaseNavigationController.h"

@interface KoronRootViewController : UIViewController <KoronRootTabBarContllerDelegate> {
    BOOL isLoginSucceed;
    
    KoronManager *_manager;
    
    KoronStartAnimationViewController *_startAnimationView;
    
    //自动登陆请求
    ASIFormDataRequest *_loginRequest;
    
    UIView *_customView;
    
    KoronRootTabBarContller *_tabBar;
    
    KoronHomePageViewController *_homePageCtl;
    
    KoronPrivateMessageViewController *_privateMessageCtl;
    
    KoronNotificationViewController *_notificationCtl;
    
    BaseNavigationController *_notificationNavigationController;
    
    MailViewController *_mailCtl;
    
    BaseNavigationController *_mailNavigationController;
    
    BOOL _isStatusBarBlack;
}

@end
