//
//  KoronSuperclassViewController.m
//  Koron-mofffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronSuperclassViewController.h"

@interface KoronSuperclassViewController ()

@end

@implementation KoronSuperclassViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if (IOS_VERSIONS >= 7.0) {
            [self setNeedsStatusBarAppearanceUpdate];
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (IOS_VERSIONS >= 7.0) {
        _statusBar  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
        _statusBar.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:_statusBar];
        [self.view bringSubviewToFront:_statusBar];
    }
    
    UIImage *topImage = [UIImage imageNamed:@"home_top.png"];
    UIImage *newTopImage = [topImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 1, 0)];
    if (IOS_VERSIONS < 7.0) {
        _topView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,45)];
    }else {
        _topView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20, self.view.bounds.size.width,45)];
    }
    
    _topView.userInteractionEnabled = YES;
    [_topView setImage:newTopImage];
    [self.view addSubview:_topView];
    _topLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 200, 34)];
    _topLabel.textAlignment = NSTextAlignmentCenter;
    _topLabel.textColor = [UIColor grayColor];
    _topLabel.backgroundColor = [UIColor clearColor];
    [_topView addSubview:_topLabel];
    
    _dismissBtn = [[UIButton alloc]init];
    [_dismissBtn setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [_dismissBtn setFrame:CGRectMake(5, 2, 40, 40)];
    [_dismissBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    _dismissBtn.hidden = YES;
    [_topView addSubview:_dismissBtn];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)dismissViewController {
 
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
