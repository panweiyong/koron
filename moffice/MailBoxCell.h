//
//  MailBoxCell.h
//  moffice
//
//  Created by yangxi zou on 12-1-15.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDBadgedCell.h"
#import <QuartzCore/QuartzCore.h>

@interface MailBoxCell : TDBadgedCell
{
NSString *accoutid;
NSString *folderid;
BOOL isintranet;
}


@property (nonatomic,retain) NSString *accoutid;
@property (nonatomic,retain) NSString *folderid;
@property (nonatomic) BOOL isintranet;

@property (nonatomic, retain) UILabel *contentLabel;

@end
