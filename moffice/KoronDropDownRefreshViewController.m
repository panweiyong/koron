//
//  KoronDropDownRefreshViewController.m
//  moffice
//
//  Created by Mac Mini on 13-10-9.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronDropDownRefreshViewController.h"

@interface KoronDropDownRefreshViewController ()

@end

@implementation KoronDropDownRefreshViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if (IOS_VERSIONS >= 7.0) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, _topView.frame.origin.y + _topView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - _topView.frame.size.height - 20 - 44) style:UITableViewStylePlain];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.view addSubview:_tableView];
    }else {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, _topView.frame.origin.y + _topView.frame.size.height, self.view.bounds.size.width, self.view.bounds.size.height - _topView.frame.size.height - 44) style:UITableViewStylePlain];
        [_tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.view addSubview:_tableView];
    }
    
    
    if (_refreshHeaderView == nil) {
        EGORefreshTableHeaderView * view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0, 0-_tableView.bounds.size.height, _tableView.frame.size.width, _tableView.bounds.size.height)];
        view.delegate = self;
        [_tableView addSubview:view];
        _refreshHeaderView = view;
    }
    [_refreshHeaderView refreshLastUpdatedDate];
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:_tableView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)reloadTableViewDataSource{
	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
	
}

- (void)doneLoadingTableViewData{
	
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_tableView];
	
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
	[_refreshHeaderView egoRefreshScrollViewDidScroll:_tableView];
    
    CGPoint contentOffsetPoint = _tableView.contentOffset;
    CGRect frame = _tableView.frame;
    //判断是否滚动到底部
    if (contentOffsetPoint.y == _tableView.contentSize.height - frame.size.height || _tableView.contentSize.height < frame.size.height)
    {
        [self scrollToTheEnd];
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
	
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	
	[self reloadTableViewDataSource];
    [self beginSendRequst];
	
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading; // should return if data source model is reloading
	
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


//让子类重写该方法,用来下拉刷新数据
- (void)beginSendRequst {
    
}

//让子类重写该方法,用来上拉刷新数据
- (void)scrollToTheEnd {
    
}


@end
