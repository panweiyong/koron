//
//  KoronHomePageViewController.h
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronHomePageTopView.h"
#import "KoronHomePageView.h"

@interface KoronHomePageViewController : UIViewController {

    KoronHomePageTopView *_topView;
    
    KoronHomePageView *_homePageView;
}

@end
