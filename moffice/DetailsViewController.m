//
//  DetailsViewController.m
//  moffice
//
//  Created by szsm on 12-2-3.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "DetailsViewController.h"


@implementation DetailsViewController

@synthesize tableData,moduleid;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)dealloc
{
    [moduleid release];
    [tableData release];
    [super dealloc];
}


-(void)handleTapFrom:(UITapGestureRecognizer *)recognizer{ 
    UIImageView *iv =(UIImageView *) [recognizer view];
    
    [self setHidesBottomBarWhenPushed:YES ];
    ImageViewController *imgc=[[ImageViewController alloc] initWithNibName:nil bundle:nil];
    [imgc setDisplayImage: [iv image] ];
    [self.navigationController pushViewController:imgc animated:YES];
    [imgc release];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setBackgroundColor:DETAILPAGE_BG]; 
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self setHidesBottomBarWhenPushed:NO]; 
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{ 
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ 
    // Return the number of rows in the section.
    return [tableData count] ;
}
+(NSString*)parseName:(NSString*)n
{
    //NSAssert(n!=nil, @"Invalid Field Value");
    if(n==nil)return @"unkonw";
    return [n substringToIndex:[ n rangeOfString:@"|"] .location  ];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
   
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    [[cell.contentView viewWithTag:100] removeFromSuperview];
    // CCLOG(@"MODE :%i",indexPath.row%2 );
    NSDictionary *dd=[tableData objectAtIndex:indexPath.row ] ;
    NSString *fkey= [ dd   objectForKey:@"key"] ;
     
    
    DisplayControl *dobj= [JsonUtils ParseControl:fkey data:[ dd  objectForKey:@"value"] delegate:self textAlignment:UITextAlignmentLeft];
    dobj.tag=100;
    //cell.textLabel.text= @" "; //.title;
    //cell.textLabel.text= [self.class parseName:fkey];
    cell.textLabel.text=  dobj.title;
    cell.backgroundColor=[UIColor whiteColor];
    [cell.contentView addSubview:dobj];
    CGRect  ffx = dobj.frame;
    ffx.origin.y =cell.textLabel.frame.origin.y+7 ;//ffx.origin.y + 5;
    dobj.frame = ffx;
    cell.textLabel.font=  [UIFont systemFontOfSize:15];
    //cell.detailTextLabel.text =  [[[[tableData objectAtIndex:indexPath.row] objectForKey:@"createTime"]  dateFromISO8601]stringWithFormat:@"HH:mm"];
    //cell.textLabel.backgroundColor  = [UIColor clearColor];
    //cell.detailTextLabel.backgroundColor  = [UIColor clearColor];
    //cell.imageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"tips_mail.png" ]  ];
 
     
     
        
        
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableViewz heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ 	
    //UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    UITableViewCell *cell = (UITableViewCell *)[self tableView:tableViewz cellForRowAtIndexPath:indexPath];
    
    if(cell!=nil&&[[cell.contentView subviews ] count]>1){
        
        UIView *c=(UIView *)[[cell.contentView subviews ] objectAtIndex:1];
        //CCLOG(@"count:%i",[[cell.contentView subviews ] count]);
        if(c)return c.frame.size.height+15.0f;
    }else{
        return 35;
    }
    return 35;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
