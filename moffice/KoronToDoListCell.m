//
//  KoronToDoListCell.m
//  TodoListDemo
//
//  Created by Mac Mini on 13-11-20.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronToDoListCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation KoronToDoListCell
{
    UIImageView   *_checkView;
    UIImageView   *_clockView;
    UILabel       *_toDoContentLabel;
    UILabel       *_timeLabel;
    UILabel       *_defaultLabel;
    UILabel       *_colorLabel;
    UIView        *_checkedLine;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews
{
    _checkView = [[UIImageView alloc] initWithFrame:CGRectZero];
    
    _checkView.userInteractionEnabled = YES;
    [self.contentView addSubview:_checkView];
    
    UITapGestureRecognizer *checkTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleCheckedView)];
    [_checkView addGestureRecognizer:checkTap];
    [checkTap release];
    
    _clockView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:_clockView];
    _clockView.image = [UIImage imageNamed:@"backlog_clock.png"];
    
    _toDoContentLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:_toDoContentLabel];
    _toDoContentLabel.font = [UIFont systemFontOfSize:18];
//    _toDoContentLabel.text = @"我的待办界面，标题很长很长标题很长很长标题很长很长";
    
    _checkedLine = [[UIView alloc] initWithFrame:CGRectZero];
    _checkedLine.backgroundColor = [UIColor redColor];
    [_toDoContentLabel addSubview:_checkedLine];
    
    _timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _timeLabel.textColor = [UIColor grayColor];
    _timeLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_timeLabel];
//    _timeLabel.text = @"9:30";

    
    _colorLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:_colorLabel];
    _colorLabel.backgroundColor = [UIColor colorWithRed:76/255.0 green:170/255.0 blue:250/255.0 alpha:1];
    _colorLabel.font = [UIFont boldSystemFontOfSize:13];
    _colorLabel.textColor = [UIColor whiteColor];
    _colorLabel.textAlignment = NSTextAlignmentCenter;
    
    _defaultLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.contentView addSubview:_defaultLabel];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
   

    _checkView.frame = CGRectMake(20, 20, 30, 30);
    _checkView.center = CGPointMake(20, self.height/2.0);
    _toDoContentLabel.frame = CGRectMake(_checkView.right + 5, _checkView.top - 10, 250, 25);
    _checkedLine.frame = CGRectMake(0, _toDoContentLabel.height/2.0, _toDoContentLabel.width, 1);
    _clockView.frame = CGRectMake(_checkView.right, _checkView.bottom  , 15, 15);
    _timeLabel.frame = CGRectMake(_clockView.right, _clockView.top, 50, _clockView.height);
    [_timeLabel sizeToFit];
    _timeLabel.center = CGPointMake(_clockView.right+_timeLabel.width/2.0+10, _clockView.top+_clockView.height/2.0);
    _colorLabel.frame = CGRectMake(_timeLabel.right+10 , _clockView.top+2.5, 60, 15);
//    [_colorLabel sizeToFit];
    _colorLabel.center = CGPointMake(_timeLabel.right+_colorLabel.width/2.0+5, _timeLabel.top+_timeLabel.height/2.0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)toggleCheckedView
{
    NSLog(@"check");
    if ([self.delegate respondsToSelector:@selector(KoronToDoListCellDidCheck:)]) {
        [self.delegate KoronToDoListCellDidCheck:self];
    }
}


- (void)dealloc
{
    
    self.checkView = nil;
    self.checkedLine = nil;
    self.toDoContentLabel = nil;
    self.clockView = nil;
    self.timeLabel = nil;
    self.colorLabel = nil;
    self.defaultLabel = nil;
    [super dealloc];
}

@end
