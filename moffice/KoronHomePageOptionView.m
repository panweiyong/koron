//
//  KoronHomePageOptionView.m
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronHomePageOptionView.h"

@implementation KoronHomePageOptionView
@synthesize optionButton = _optionButton;
@synthesize optionLabel = _optionLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        _optionButton = [[UIButton alloc]initWithFrame:CGRectMake((self.bounds.size.width - 60) / 2, 5, 60, 60)];
        [_optionButton addTarget:self action:@selector(pressOptionButton) forControlEvents:UIControlEventTouchUpInside];
        [_optionButton setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_optionButton];
        
        _optionLabel = [[UILabel alloc]initWithFrame:CGRectMake((self.bounds.size.width - 60) / 2, _optionButton.frame.origin.y + _optionButton.frame.size.height + 5, 60, 20)];
        _optionLabel.backgroundColor = [UIColor clearColor];
        _optionLabel.font = [UIFont systemFontOfSize:12];
        _optionLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_optionLabel];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)pressOptionButton {
    //通知 RootViewController  (切换至 通讯录,工作计划,客户列表等页面)
    [[NSNotificationCenter defaultCenter] postNotificationName:@"presentOption" object:_optionLabel.text];
}


@end
