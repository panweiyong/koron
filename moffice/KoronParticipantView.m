//
//  KoronParticipantView.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-28.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronParticipantView.h"
#import <QuartzCore/QuartzCore.h>

@implementation KoronParticipantView
{
    UIImageView *_headIcon;
    UILabel *_nameLabel;
    UILabel *_positionLabel;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews
{
    self.layer.cornerRadius = 10.0f;
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1].CGColor;
    
    _headIcon = [[UIImageView alloc] initWithFrame:CGRectZero];
    _headIcon.layer.cornerRadius = 10.0f;
    _headIcon.backgroundColor = [UIColor orangeColor];
    _headIcon.image = [UIImage imageNamed:@"default_head.jpg"];
    _headIcon.clipsToBounds = YES;
    [self addSubview:_headIcon];
    
    _nameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _nameLabel.textColor = [UIColor colorWithRed:47/255.0 green:146/255.0 blue:1 alpha:1];
    _nameLabel.backgroundColor = [UIColor clearColor];
    _nameLabel.text = @"某某某";
    _nameLabel.font = [UIFont boldSystemFontOfSize:15];
    [self addSubview:_nameLabel];
    
    _positionLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _positionLabel.backgroundColor = [UIColor clearColor];
    _positionLabel.text = @"职员";
    _positionLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:_positionLabel];
    
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _headIcon.frame = CGRectMake(5, 5, self.height - 10, self.height - 10);
    _nameLabel.frame = CGRectMake(_headIcon.right + 10, _headIcon.top, 70, _headIcon.height/2.0);
    _positionLabel.frame = CGRectMake(_nameLabel.left, _nameLabel.bottom, 70, _nameLabel.height);
}

- (void)dealloc
{
    [_headIcon release], _headIcon = nil;
    [_nameLabel release], _nameLabel = nil;
    [_positionLabel release], _positionLabel = nil;
    [super dealloc];
}

@end
