//
//  KoronUser.h
//  moffice
//
//  Created by 潘蔚勇 on 14-1-10.
//  Copyright (c) 2014年 Hsn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KoronUser : NSObject {
    
}

@property (nonatomic,retain)NSString *id;
@property (nonatomic,retain)NSString *empNumber;
@property (nonatomic,retain)NSString *name;
@property (nonatomic,retain)NSString *gender;
@property (nonatomic,retain)NSString *title;
@property (nonatomic,retain)NSString *sign;
@property (nonatomic,retain)NSString *department;
@property (nonatomic,retain)NSString *telephone;
@property (nonatomic,retain)NSString *mobile;
@property (nonatomic,retain)NSString *email;
@property (nonatomic,retain)NSString *icon;
@property (nonatomic,retain)NSString *lastUpdaeTime;
@property (nonatomic,retain)NSString *statusId;
@property (nonatomic,retain)NSString *alpha;
@property (nonatomic,retain)NSString *fullAlpha;
@property (nonatomic,retain)NSString *online;
@property (nonatomic,retain)NSString *onlineType;

+(KoronUser *) koronUserWithNSDictionary:(NSDictionary *)dictionary;

@end
