//
//  DropDown.m
//  Emenu
//
//  Created by iCON iOS developer on 13年3月12日.
//  Copyright (c) 2013年 iCON iOS developer. All rights reserved.
//

#import "DropDown.h"

@implementation DropDown
@synthesize tv,tableArray,textField;

-(id)initWithFrame:(CGRect)frame
{
    if (frame.size.height<200) {
        frameHeight = 200;
    }else{
        frameHeight = frame.size.height;
    }
   // tabheight = frameHeight-50;
    tabheight = frameHeight-50;
    frame.size.height = 50.0f;
    
    self=[super initWithFrame:frame];
    
    if(self){
        showList = NO; //默认不显示下拉框
        tv = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, frame.size.width-10, 0)];
        tv.delegate = self;
        tv.dataSource = self;
       // tv.backgroundColor = [UIColor grayColor];
       // tv.separatorColor = [UIColor lightGrayColor];
        tv.scrollEnabled=NO;
        tv.hidden = YES;
        [self addSubview:tv];
        
        textField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 50)];
       // textField.borderStyle=UITextBorderStyleRoundedRect;//设置文本框的边框风格
        textField.borderStyle=UITextBorderStyleNone;//设置文本框的边框风格
        textField.textAlignment=UITextAlignmentLeft;
        //[textField setEnabled:NO];
        [textField addTarget:self action:@selector(dropdown) forControlEvents:UIControlEventAllTouchEvents];
        [self addSubview:textField];
     }
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(changeTextfield)  name: @"changeTextfield" object: nil];
    return self;
}
-(void)dropdown{
    [textField resignFirstResponder];
    if (showList) {//如果下拉框已显示，什么都不做
        return;
    }else {//如果下拉框尚未显示，则进行显示
        
        CGRect sf = self.frame;
        sf.size.height = frameHeight;
        
        //把dropdownList放到前面，防止下拉框被别的控件遮住
        [self.superview bringSubviewToFront:self];
        tv.hidden = NO;
        showList = YES;//显示下拉
        CGRect frame = tv.frame;
        frame.size.height = 0;
        tv.frame = frame;
        frame.size.height = tabheight;
        [UIView beginAnimations:@"ResizeForKeyBoard" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        self.frame = sf;
        tv.frame = frame;
        [UIView commitAnimations];
    }
}

-(void)changeTextfield
{
    textField.text=[[NSUserDefaults standardUserDefaults] stringForKey:@"viewnameValue"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.text = [tableArray objectAtIndex:[indexPath row]];
    cell.textLabel.font = [UIFont systemFontOfSize:16.0f];
    cell.textLabel.textAlignment=UITextAlignmentCenter;
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    textField.text = [tableArray objectAtIndex:[indexPath row]];
    showList = NO;
    tv.hidden = YES;
    
    CGRect sf = self.frame;
    sf.size.height = 50;
    self.frame = sf;
    CGRect frame = tv.frame;
    frame.size.height = 0;
    tv.frame = frame;
    [[NSUserDefaults standardUserDefaults] setObject:textField.text forKey:@"viewnameValue"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeView" object:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//-(void)hiddenTv
//{
//    tv.hidden=YES;
//}

@end
