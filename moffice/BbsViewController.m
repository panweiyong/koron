//
//  BbsViewController.m
//  moffice
//
//  Created by koron on 13-6-3.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "BbsViewController.h"
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
float tableHeight=0;

@interface BbsViewController ()

@end

@implementation BbsViewController
@synthesize refid,tipsid;
@synthesize bbsScrollview;
@synthesize commentsArr;
@synthesize replayTabview;
@synthesize dataArr;
@synthesize activity;
@synthesize replayContent,topicId;
@synthesize replayBtn;
@synthesize replayNum,replayResult;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=NSLocalizedString(@"bbs",@"bbs") ;
    }
    return self;
}

-(void)dealloc
{
  [super dealloc];
  [timeLabel release];
  [bbsWeb release];
  [detailbbsScroll release];
  [replayTextview release];
  [replayActivity release];
  //[commentsArr release];
  //[bbsScrollview release];
}


- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
    [[JsonService sharedManager] cancelAllRequest];
    [self setHidesBottomBarWhenPushed:NO];
    [super viewDidDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    if (IOS_VERSIONS > 6.9) {
        
        [self setNeedsStatusBarAppearanceUpdate];
    }
   // replayTabHeight=0;
    flag=NO;
    firstPushBbs=NO;
    cellWebHeight=[[[NSMutableArray alloc]initWithCapacity:0] retain];
    [cellWebHeight removeAllObjects];
    activity=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 160, 160)];
    [activity setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
    [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:activity];
    [activity startAnimating];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStartAnimation" object:nil];
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
     NSLog(@"refid=%@",refid);
    [jservice getBbs:refid];
    replayContent=nil;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

//异步接收到返回的数据
- (void)requestDataFinished:(id)jsondata//request                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
    if(!flag)
    {
        flag=YES;
        if ([jsondata isKindOfClass:[NSDictionary class]])
        {
            
            topicName=[[jsondata objectForKey:@"topicName"] retain];
            userName=[[[jsondata objectForKey:@"bbsUser"] objectForKey:@"fullName"] retain];
            creatTime=[[jsondata objectForKey:@"createTime"] retain];
            
//            NSString *searchStr = @"<img alt=\"\" src=\"";
//            NSString *replaceStr = [NSString stringWithFormat:@"%@%@",searchStr,[[JsonService sharedManager] getHost]];
//
//            
//            NSString *content = [[jsondata objectForKey:@"topicContent"] stringByReplacingOccurrencesOfString:searchStr withString:replaceStr];
//

            
            webContent = [[jsondata objectForKey:@"topicContent"] retain];
//            webContent=[content retain];
            
            //commentsArr=[[[NSArray alloc]init] retain];
            commentsArr=[[jsondata objectForKey:@"replayList"] retain];
            topicId=[[jsondata objectForKey:@"id"] retain];
            //NSLog(@"%@",creatTime);
        }
        
        dataArr=[[NSMutableArray alloc]initWithCapacity:0];
        [dataArr removeAllObjects];
        
        
        for(UIView *view in [bbsScrollview subviews])
        {
            [view removeFromSuperview];
            //[view release];
        }
        [self layoutView];
    }
    else
    {
        flag=NO;
        NSLog(@"jsondata   == %@",jsondata);
        replayNum=[[jsondata objectForKey:@"code"] retain];
        replayResult=[[jsondata objectForKey:@"desc"] retain];
        [self displayResult:replayResult];
        if([replayNum intValue]>0)
        {
//            if ([commentsArr count]==0)
//            {
//                firstPushBbs=YES;
//                [self displayComments:bbsWeb.frame.origin.y+bbsWeb.frame.size.height+5];
//            }
            [cellWebHeight removeAllObjects];
            JsonService *jservice=[JsonService sharedManager];
            [jservice setDelegate:self];
            [jservice getBbs:refid];
            //[self changeLabeltext];
            [replayNum release];
            replayTextview.text=nil;
            replayContent=nil;
        }
    }
}



-(void)layoutView
{
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.text=topicName;
    [titleLabel setNumberOfLines:0];
    titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    [titleLabel setTextColor:[UIColor blackColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    //设置一个行高上限
    CGSize size = CGSizeMake(320,2000);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [topicName sizeWithFont:[UIFont systemFontOfSize:24] constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    [titleLabel setFrame:CGRectMake(30,0,self.view.frame.size.width-60, labelsize.height)];
    // [titleLabel sizeToFit];
    titleLabel.font=[UIFont systemFontOfSize:22];
    titleLabel.adjustsFontSizeToFitWidth=YES;
    titleLabel.backgroundColor=[UIColor clearColor];
    [bbsScrollview addSubview:titleLabel];
    [titleLabel release];
    
    UIImageView *logoView=[[UIImageView alloc]initWithFrame:CGRectMake(10, titleLabel.frame.size.height+5, 40, 40)];
    [logoView setImage:[UIImage imageNamed:@"org.png"]];
    [bbsScrollview addSubview:logoView];
    [logoView release];
    
    UILabel *userLabel=[[UILabel alloc]initWithFrame:CGRectMake(55, titleLabel.frame.size.height+5, 100, 15)];
    [userLabel setBackgroundColor:[UIColor clearColor]];
    [userLabel setTextColor:[UIColor blueColor]];
    userLabel.text=userName;
    userLabel.font=[UIFont systemFontOfSize:12.0];
    userLabel.textAlignment=UITextAlignmentLeft;
    [bbsScrollview addSubview:userLabel];
    [userLabel release];
    
    timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(55, logoView.frame.origin.y+30, 100, 10)];
    [timeLabel setBackgroundColor:[UIColor clearColor]];
    [timeLabel setTextColor:[UIColor grayColor]];
    timeLabel.text=creatTime;
    timeLabel.font=[UIFont systemFontOfSize:8.0];
    timeLabel.textAlignment=UITextAlignmentLeft;
    [bbsScrollview addSubview:timeLabel];
    //[timeLabel release];
    
    
    
    
    
    
    bbsWeb=[[UIWebView alloc]init];
    bbsWeb.tag=188;
    CGRect webframe=CGRectMake(0, timeLabel.frame.origin.y+15, 320, 20);
    bbsWeb.frame=webframe;
    bbsWeb.delegate=self;
//    NSString *makeStr=[NSString stringWithFormat:@"%@%@%@",@"<img src='",@"http://",[[NSUserDefaults standardUserDefaults]objectForKey:@"host"]];
//    NSString *replacewebContent=[webContent stringByReplacingOccurrencesOfString:@"<img src='" withString:makeStr];
    [bbsWeb loadHTMLString:webContent baseURL:nil];
    [bbsWeb setBackgroundColor:[UIColor clearColor]];
     bbsWeb.scalesPageToFit=NO;
    [bbsScrollview addSubview:bbsWeb];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
  if(webView.tag==188)
  {
    CGFloat webViewHeight=[webView.scrollView contentSize].height;
    CGFloat webViewWidth=[webView.scrollView contentSize].width;
    CGRect newFrame = webView.frame;
    newFrame.size.height = webViewHeight;
    webView.frame = newFrame;
    webView.scrollView.scrollEnabled=YES;
    webView.scrollView.contentSize=CGSizeMake(webViewWidth, newFrame.size.height);


    float nowHeight=webView.frame.origin.y+webView.frame.size.height+5;
    NSLog(@"%f",nowHeight);
    if([commentsArr count]==0)
    {
        [self displayReplay:nowHeight];
    }
    else
    {
//        webviewArray=[[[NSMutableArray alloc]initWithCapacity:0] retain];
//        [webviewArray removeAllObjects];
//        for (int i=0; i<[commentsArr count]; i++)
//        {
//            UIWebView *cellweb=[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 0)];
//            cellweb.delegate=nil;
//            [cellweb loadHTMLString:[[commentsArr objectAtIndex:i] objectForKey:@"content"] baseURL:nil];
//            [self.view addSubview:cellweb];
//            if(![webviewArray containsObject:cellweb])
//            {
//                [webviewArray addObject:cellweb];
//            }
//            [cellweb removeFromSuperview];
//            [cellweb release];
//        }
        [self displayComments:nowHeight];
     }
  }
  else
  {
      CGRect frame = webView.frame;
      frame.size.height = 1;
      webView.frame = frame;
      CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
      frame.size = fittingSize;
      webView.frame = frame;
      float height=frame.size.height;
      NSString *heightStr=[NSString stringWithFormat:@"%f",height];
      [cellWebHeight addObject:heightStr];
      tableHeight+=frame.size.height;
  }
}

-(void)displayComments:(float)height
{
    UILabel *comLabel=[[UILabel alloc]initWithFrame:CGRectMake(20,height, 100, 15)];
    [comLabel setBackgroundColor:[UIColor clearColor]];
    [comLabel setTextColor:[UIColor grayColor]];
    [comLabel setFont:[UIFont systemFontOfSize:10.0]];
    comLabel.tag=95;
    comLabel.text=[NSString stringWithFormat:@"评论:%d",[commentsArr count]];
    [bbsScrollview addSubview:comLabel];
    [comLabel release];
    
    UIView *lineView=[[UIView alloc]initWithFrame:CGRectMake(0, height+25, self.view.frame.size.width, 1.0f)];
    [lineView setBackgroundColor:[UIColor colorWithRed:193.0/255.0 green:196/255.0 blue:218/255.0 alpha:1.0]];
    [bbsScrollview addSubview:lineView];
    [lineView release];

    NSLog(@"lineView.frame.origin.y=%f",lineView.frame.origin.y);
    UIImageView *triImage=[[UIImageView alloc]initWithFrame:CGRectMake(30, lineView.frame.origin.y-10, 16, 11)];
    [triImage setBackgroundColor:[UIColor clearColor]];
    [triImage setImage:[UIImage imageNamed:@"little_triangle.png"]];
    [bbsScrollview addSubview:triImage];
    [triImage release];
    
    for(int i=0;i<[commentsArr count];i++)
    {
        UIWebView *cellWeb=[[UIWebView alloc]init];
        [cellWeb setScalesPageToFit:NO];
        cellWeb.delegate=self;
        [cellWeb loadHTMLString:[[commentsArr objectAtIndex:i] objectForKey:@"content"] baseURL:nil];
        [cellWeb release];
    }
    replayTabview=[[UITableView alloc]initWithFrame:CGRectMake(0,comLabel.frame.origin.y+comLabel.frame.size.height+15, self.view.frame.size.width,300)];
    replayTabview.delegate=self;
    replayTabview.dataSource=self;
// [replayTabview reloadData];
    [bbsScrollview addSubview:replayTabview];
    //[replayTabview reloadData];
    //[self drawTable];
    float heightNow=replayTabview.frame.origin.y+replayTabview.frame.size.height+10;
    if(!firstPushBbs)
    {
      [self displayReplay:heightNow];
    }
}

-(void)changeLabeltext
{
    for(UILabel *label in [bbsScrollview subviews])
    {
        if(label.tag==95)
        {
          label.text=[NSString stringWithFormat:@"评论:%d",[commentsArr count]];
        }
    }
}

-(void)drawTable
{
   CGFloat tableHeight=0;
   if(commentsArr!=nil&&[commentsArr count]!=0)
   {
       for(int i=0;i<[commentsArr count];i++)
       {
          NSIndexPath *indexPath=[[NSIndexPath alloc]initWithIndex:i];
          UITableViewCell *cell=[replayTabview cellForRowAtIndexPath:indexPath];
          CGFloat cellHeigh=cell.frame.size.height;
          tableHeight+=cellHeigh;
       }
       CGRect tableFrame=replayTabview.frame;
       tableFrame.size.height=tableHeight;
       replayTabview.frame=tableFrame;
   }
}


#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{   
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([commentsArr count]!=0)
    {
        return [commentsArr count];
    }
	return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"bbsliCell";
    bbsCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[bbsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.logoImage setImage:[UIImage imageNamed:@"org.png"]];
    cell.nameLabel.text=[[[commentsArr objectAtIndex:indexPath.row] objectForKey:@"bbsUser"] objectForKey:@"fullName"];
    cell.nameLabel.textColor=[UIColor grayColor];
    cell.nameLabel.font=[UIFont systemFontOfSize:8.0];
    cell.timeLabel.text=[[commentsArr objectAtIndex:indexPath.row] objectForKey:@"createTime"];
    cell.timeLabel.textColor=[UIColor grayColor];
    cell.timeLabel.font=[UIFont systemFontOfSize:8.0];
//    cell.contentWeb=[webviewArray objectAtIndex:indexPath.row];
    cell.contentWeb.delegate=self;
    cell.contentWeb.frame=CGRectMake(0,36, 280, 1200);
    [cell.contentWeb loadHTMLString:[[commentsArr objectAtIndex:indexPath.row] objectForKey:@"content"] baseURL:nil];
    UIFont *font = [UIFont fontWithName:@"Arial" size:12.0f];
    CGSize size = CGSizeMake(320,2000);
    CGSize labelsize = [[[commentsArr objectAtIndex:indexPath.row] objectForKey:@"content"] sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping];
    
    CGRect frame = [cell.contentWeb frame];
    [cell.contentWeb setBackgroundColor:[UIColor clearColor]];
    frame.size.height=labelsize.height+5;
    [cell.contentWeb setFrame:frame];
    
//    CGSize actualSize = [cell.contentWeb sizeThatFits:CGSizeZero];
//    CGRect newFrame = cell.contentWeb.frame;
//    newFrame.size.height = actualSize.height;
//    cell.contentWeb.frame = newFrame;
//    NSLog(@"newFrame.size.height=%f",newFrame.size.height);
//    CGRect frame = cell.contentWeb.frame;
//    frame.size.height = 1;
//    cell.contentWeb.frame = frame;
//    CGSize fittingSize = [cell.contentWeb sizeThatFits:CGSizeZero];
//    frame.size = fittingSize;
//    cell.contentWeb.frame = frame;
//    cell.frame=CGRectMake(0, 0, self.view.frame.size.width, 50+frame.size.height);
    return cell;
}


-(void)setreplayTabHeight:(float)height
{
    replayTabview.frame=CGRectMake(0,replayTabview.frame.origin.y,self.view.frame.size.width, height);
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPat
{
    
    return;

}

- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    bbsCell *cell=(bbsCell*)[self tableView:replayTabview cellForRowAtIndexPath:indexPath];
    CGFloat height=cell.contentWeb.frame.size.height+cell.contentWeb.frame.origin.y+35;
    return height;
}


-(void)displayReplay:(float)height
{
    UILabel *replayLabel=[[UILabel alloc]initWithFrame:CGRectMake(5, height, self.view.frame.size.width-10, 25)];
    [replayLabel setBackgroundColor:[UIColor colorWithRed:191.0/255.0 green:192.0/255.0 blue:191.0/255.0 alpha:1.0]];
    [replayLabel setTextColor:[UIColor blackColor]];
    [replayLabel setFont:[UIFont systemFontOfSize:10.0]];
    [replayLabel setText:@"  发表评论"];
    [bbsScrollview addSubview:replayLabel];
    [replayLabel release];
    
    UIImageView *triImage=[[UIImageView alloc]initWithFrame:CGRectMake(25, height+25-8, 12, 8)];
    [triImage setBackgroundColor:[UIColor clearColor]];
    [triImage setImage:[UIImage imageNamed:@"little_triangle.png"]];
    [bbsScrollview addSubview:triImage];
    [triImage release];
    
    //加入评论框
    replayTextview=[[UITextView alloc]initWithFrame:CGRectMake(5, replayLabel.frame.origin.y+replayLabel.frame.size.height+5,self.view.frame.size.width-10,120)];
    CALayer *textViewlayer=[replayTextview.layer retain];
    textViewlayer.borderWidth=1.0;
    textViewlayer.borderColor=[[UIColor blackColor] CGColor];
    textViewlayer.masksToBounds=YES;
    textViewlayer.cornerRadius=5.0;
    [replayTextview setEditable:YES];
    replayTextview.text=NSLocalizedString(@"write replay", @"write replay");
    replayTextview.textColor=[UIColor grayColor];
    replayTextview.font = [UIFont fontWithName:@"Arial" size:18.0];
    replayTextview.keyboardType=UIKeyboardTypeDefault;
    replayTextview.returnKeyType = UIReturnKeyDone;//返回键的类型
    replayTextview.autoresizingMask = UIViewAutoresizingFlexibleHeight;//自适应高度
    replayTextview.delegate=self;
    [replayTextview setBackgroundColor:[UIColor clearColor]];
    [bbsScrollview addSubview:replayTextview];

    
    //发表评论btn
    replayBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, replayTextview.frame.origin.y+replayTextview.frame.size.height+5, self.view.frame.size.width-20, 40)];
    [replayBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    CALayer *btnlayer=[replayBtn.layer retain];
    //btnlayer.borderWidth=1.0;
    btnlayer.borderColor=[[UIColor blackColor] CGColor];
    btnlayer.masksToBounds=YES;
    btnlayer.cornerRadius=5;
    [replayBtn setBackgroundColor:[UIColor colorWithRed:76.0/255.0 green:150.0/255.2 blue:215.0/255.0 alpha:1.0]];
    [replayBtn setTitle:@"发表评论" forState:UIControlStateNormal];
     replayBtn.titleLabel.textColor=[UIColor blackColor];
    [replayBtn addTarget:self action:@selector(replayBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [bbsScrollview addSubview:replayBtn];
    [replayBtn release];
    bbsScrollview.contentSize=CGSizeMake(0, replayBtn.frame.origin.y+replayBtn.frame.size.height+100);
    for (UIView *subView in [bbsWeb subviews])
    {
        if ([subView isKindOfClass:[UIScrollView class]]) {
            for (UIView *shadowView in [subView subviews]) {
                if ([shadowView isKindOfClass:[UIImageView class]]) {
                    shadowView.hidden = YES;
                }
            }
        }
    }
    //[replayTabview reloadData];
    [activity stopAnimating];
    //[activity release];
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];
    if(textviewIsfirstEdite)
    {
      textView.text=nil;
      textviewIsfirstEdite=NO;
    }
     [textView setTextColor:[UIColor blackColor]];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    replayContent=[[NSString stringWithFormat:@"%@",textView.text] retain];
    if(bbsScrollview.contentOffset.y==replayBtn.frame.origin.y-250)
    {
        [bbsScrollview setContentOffset:CGPointMake(0,replayBtn.frame.origin.y-300) animated:YES];
        bbsScrollview.scrollEnabled=YES;
    }
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }   
    return YES;   
}

-(void)replayBtnPressed:(UIButton*)sender
{
    //NSString *startStr=[NSString stringWithFormat:@"写评论..."];
    textviewIsfirstEdite=YES;
    if(replayContent==nil||[replayContent length]==0)
    {
       // [self displayResult:@"请输入内容"];
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"回复内容不能为空!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    {
        UIView *mainscreenView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        mainscreenView.tag=96;
        [self.view addSubview:mainscreenView];
        
        UIView *backView=[[UIView alloc]initWithFrame:CGRectMake(110,self.view.frame.size.height/2-40, 100, 80)];
        backView.tag=97;
        backView.layer.cornerRadius=5.0f;
        [backView setBackgroundColor:[UIColor grayColor]];
        [self.view addSubview:backView];
        
        replayActivity=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        [replayActivity setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
        [replayActivity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
        [self.view addSubview:replayActivity];
        [replayActivity startAnimating];
        NSLog(@"00000");
        NSLog(@"refid=%@",refid);
        NSLog(@"topicId=%@",topicId);
        NSLog(@"content=%@",replayContent);
        JsonService *jservicereplay=[JsonService sharedManager];
        [jservicereplay setDelegate:self];
        [jservicereplay postReplay:refid second:topicId third:replayContent];
       // [topicId release];
       // [replayContent release];
    }
}

-(void)displayResult:(NSString*)text
{
    UILabel *tellLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50,replayBtn.frame.origin.y-100, 100, 80)];
    tellLabel.layer.masksToBounds=YES;
    tellLabel.layer.cornerRadius=4.0;
    [tellLabel setBackgroundColor:[UIColor colorWithRed:7.0/255.0 green:16.0/255.0 blue:33.0/255.0 alpha:1.0]];
    [tellLabel setText:text];
    [tellLabel setFont:[UIFont systemFontOfSize:10.0]];
    [tellLabel setTextAlignment:NSTextAlignmentCenter];
    [tellLabel setTextColor:[UIColor whiteColor]];
    [bbsScrollview addSubview:tellLabel];
    //[replayResult release];
    [self performSelector:@selector(removetellLabel:) withObject:tellLabel afterDelay:1.0];
}

-(void)removetellLabel:(UILabel*)label
{
    [label removeFromSuperview];
    [label release];
    [replayActivity stopAnimating];
    [replayActivity removeFromSuperview];
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==96||view.tag==97)
        {
            [view removeFromSuperview];
            [view release];
        }
        
    }
}

- (void)requestReplayDataFinished:(id)jsondata
{
    NSLog(@"replayData=%@",jsondata);
} 

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadNavigationItem];
    textviewIsfirstEdite=YES;
    
    if ([self respondsToSelector:@selector(modalPresentationCapturesStatusBarAppearance)]) {
        self.modalPresentationCapturesStatusBarAppearance = NO;
    }
    
   // NSLog(@"timeLabel.frame.origin.y=%f",timeLabel.frame.origin.y);
    bbsScrollview=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
   // bbsScrollview.pagingEnabled=YES;
    bbsScrollview.delegate=self;
    //self.merScrollView.indicatorStyle=UIScrollViewIndicatorStyleWhite;
    bbsScrollview.showsVerticalScrollIndicator=YES;
    bbsScrollview.scrollEnabled=YES;
   // bbsScrollview.canCancelContentTouches=NO;
   // bbsScrollview.delaysContentTouches=YES;
    [self.view addSubview:bbsScrollview];
    [bbsScrollview release];
    
//    UIBarButtonItem *done =[[UIBarButtonItem alloc] initWithTitle:@"评论" style:UIBarButtonItemStyleBordered target:self action:@selector(replayShow:)];
//    self.navigationItem.rightBarButtonItem = done;
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    
//    if(IOS_VERSION<5.0)
//    {
//      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillHideNotification object:nil];
//    }
//    else
//    { 
//      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
//    }
}


-(void)keyboardWillShow:(NSNotification *)notification
{
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        NSInteger offset =self.view.frame.size.height-keyboardBounds.origin.y+64.0;
        CGRect listFrame = CGRectMake(0, -offset, self.view.frame.size.width,self.view.frame.size.height);
        NSLog(@"offset is %d",offset);
        [UIView beginAnimations:@"anim" context:NULL];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.3];
        //处理移动事件，将各视图设置最终要达到的状态
        self.view.frame=listFrame;
        [UIView commitAnimations];
    }
}

-(void)keyboardWillHide:(NSNotification*)notification
{
         NSTimeInterval animationDuration = 0.30f;
         [UIView beginAnimations:@"ResizeForKeyboard" context:nil];
         [UIView setAnimationDuration:animationDuration];
         CGRect rect = CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height);
         self.view.frame = rect;
         [UIView commitAnimations];
}


-(void)replayShow:(id)sender
{
    [bbsScrollview setContentOffset:CGPointMake(0,replayBtn.frame.origin.y-320) animated:YES];
   //  bbsScrollview.bouncesZoom = NO;
   // [replayTextview becomeFirstResponder];
    [replayTextview becomeFirstResponder];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}

- (void)backButtonPressed {
    if ([self.navigationController respondsToSelector:@selector(popViewControllerAnimated:)]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if ([self.navigationController respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self.navigationController dismissModalViewControllerAnimated:YES];
    }
    
    
    
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

@end
