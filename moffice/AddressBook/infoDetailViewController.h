//
//  infoDetailViewController.h
//  moffice
//
//  Created by lijinhua on 13-7-31.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "infodetailCell.h"
#import "unitCell.h"
#import "KoronSuperclassViewController.h"
@interface infoDetailViewController : KoronSuperclassViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *detailList;
    NSDictionary *infoDic;
    NSString *labelText;
}
@property(nonatomic,retain)  NSDictionary *infoDic;
@property(nonatomic,retain)  NSString *labelText;
@end
