//
//  connectViewController.h
//  moffice
//
//  Created by lijinhua on 13-7-23.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "DAReloadActivityButton.h"
#import "ChineseString.h"
#import "pinyin.h"
#import "personInfoCell.h"
#import "infoDetailViewController.h"
#import "SJABHelper.h"
#import "CHSectionSelectionView.h"
#import "DemoSectionItemSubclass.h"
#import "ASIFormDataRequest.h"
#import "KoronManager.h"
#import "SBJsonParser.h"
#import "KoronSuperclassViewController.h"

@interface connectViewController : KoronSuperclassViewController<UITableViewDelegate,UITableViewDataSource,CHSectionSelectionViewDataSource, CHSectionSelectionViewDelegate,ASIHTTPRequestDelegate>
{
    KoronManager *_manager;
    
    ASIFormDataRequest *_addressBookRequest;
    
    UITableView *personList;
    NSArray *listArray;
    NSMutableArray *firstStrArray;
    NSMutableArray *result;
    NSMutableArray *listContentArr;
    NSDictionary *selectCellDic;
    
    NSArray *sections;
    UIActivityIndicatorView *ac;
   // NSArray *cellData;
}
@property (nonatomic, strong) DAReloadActivityButton *navigationBarItem;
@property (nonatomic, strong) CHSectionSelectionView *selectionView;
@end
