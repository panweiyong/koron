//
//  DAReloadActivityButton.h
//  moffice
//
//  Created by lijinhua on 13-7-31.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAReloadActivityButton : UIButton
{
    BOOL animating;
}
@property (nonatomic) CGFloat animationDuration;

- (BOOL)isAnimating;
- (void)startAnimating;
- (void)stopAnimating;
- (void)spin;
@end
