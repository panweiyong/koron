//
//  unitCell.h
//  moffice
//
//  Created by lijinhua on 13-8-5.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface unitCell : UITableViewCell
{
    UILabel *unitLabel;
    UILabel *deLabel;
}
@property(nonatomic,retain) UILabel *unitLabel;
@property(nonatomic,retain) UILabel *deLabel;
@end
