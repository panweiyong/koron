//
//  infodetailCell.h
//  moffice
//
//  Created by lijinhua on 13-7-31.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface infodetailCell : UITableViewCell
{
    UILabel *nameLabel;
    UILabel *firstLabel;
    UILabel *infoLabel;
    UILabel *secondLabel;
}
@property(nonatomic,retain) UILabel *nameLabel;
@property(nonatomic,retain) UILabel *firstLabel;
@property(nonatomic,retain) UILabel *infoLabel;
@property(nonatomic,retain) UILabel *secondLabel;
@end
