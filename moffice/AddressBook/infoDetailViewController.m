//
//  infoDetailViewController.m
//  moffice
//
//  Created by lijinhua on 13-7-31.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "infoDetailViewController.h"

@interface infoDetailViewController ()

@end

@implementation infoDetailViewController
@synthesize infoDic,labelText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=@"详情";
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated
{
  [self setHidesBottomBarWhenPushed:YES];
  [super viewDidDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setHidesBottomBarWhenPushed:YES];
    [super viewWillAppear:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dismissBtn.hidden = NO;
    
    detailList=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    if (IOS_VERSIONS >= 7.0) {
        [detailList setFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    }else {
        [detailList setFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    }
    
    [detailList setBackgroundColor:[UIColor colorWithRed:226.0/255.0 green:226.0/255.0 blue:226.0/255.0 alpha:1.0]];
    [detailList setBackgroundView:nil];
    detailList.delegate=self;
    detailList.dataSource=self;
    [self.view addSubview:detailList];
	// Do any additional setup after loading the view.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag==61)
    {
        return 1;
    }
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //NSLog(@"title=%@",[[listArray objectAtIndex:section] objectForKey:@"name"]);
    if(tableView.tag==61)
    {
        return nil;
    }
    else
    {
       if(section==0)
       {
          return @"基本信息";
       }
       else if(section==1)
       {
          return @"单位信息";
       }
       else
       {
         return @"备注";
       }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(tableView.tag==61)
    {
        if(tableView.frame.size.height==176)
        {
            return 4;
        }
        else
        {
            return 2;
        }
    }
    else
    {
        if(section==0)
       {
          return 5;
       }
       else if(section==1)
       {
          return 6;
       }
       else
       {
          return 1;
       }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   if(tableView.tag==61)
   {
       static NSString *CellIdentifier = @"nomalCell";
       UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
       if (cell == nil) {
           cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
       }
       cell.selectionStyle=UITableViewCellSelectionStyleNone;
       if(tableView.frame.size.height==176)
       {
           switch (indexPath.row)
           {
               case 0:
               {
                   cell.textLabel.textColor=[UIColor whiteColor];
                   cell.textLabel.text=labelText;
                   cell.textLabel.backgroundColor=[UIColor clearColor];
                   UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
                   backgrdView.backgroundColor = [UIColor blackColor];
                   cell.backgroundView = backgrdView;
                   break;
               }
               case 1:
                   cell.textLabel.text=@"打电话";
                   break;
               case 2:
                   cell.textLabel.text=@"发短信";
                   break;
               case 3:
                   cell.textLabel.text=@"复制";
                   break;
               default:
                   break;
           }
       }
       else
       {
           switch (indexPath.row)
           {
               case 0:
                   cell.textLabel.textColor=[UIColor whiteColor];
                   cell.textLabel.text=labelText;
                   cell.textLabel.backgroundColor=[UIColor clearColor];
                   UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
                   backgrdView.backgroundColor = [UIColor blackColor];
                   [cell.backgroundView addSubview:backgrdView];
                   [backgrdView release];
                   break;
               case 1:
                   cell.textLabel.text=@"复制";
                   break;
               default:
                   break;
           }
       }
       return cell;
   }
   else
   {
    if (indexPath.section==0)
    {
        static NSString *CellIdentifier = @"infoDetailCell";
        infodetailCell *cell =(infodetailCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[infodetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        UITapGestureRecognizer *tapRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LabeladdTap:)];
        [cell.firstLabel addGestureRecognizer:tapRecognizer];
        UITapGestureRecognizer *stapRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LabeladdTap:)];
        [cell.secondLabel addGestureRecognizer:stapRecognizer];
        
        cell.nameLabel.font = [UIFont systemFontOfSize:14];
        cell.infoLabel.font = [UIFont systemFontOfSize:14];
        cell.firstLabel.font = [UIFont systemFontOfSize:14];
        cell.secondLabel.font = [UIFont systemFontOfSize:14];
        
        switch (indexPath.row)
        {
            case 0:
            {
                cell.nameLabel.text=@"姓名:";
                cell.infoLabel.text=@"部门:";
                cell.firstLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"name"]];
                CGSize fsize = [cell.firstLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(92.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect fnewFrame = cell.firstLabel.frame;
                fnewFrame.size.height = fsize.height;
                cell.firstLabel.frame = fnewFrame;
                [cell.firstLabel sizeToFit];
                
                cell.secondLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"deptFullName"]];
                CGSize size = [cell.secondLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(105.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.secondLabel.frame;
                newFrame.size.height = size.height;
                cell.secondLabel.frame = newFrame;
                [cell.secondLabel sizeToFit];
                break;
            }
            case 1:
            {
                cell.nameLabel.text=@"性别:";
                cell.infoLabel.text=@"生日:";
                cell.firstLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"sex"]];
                CGSize fsize = [cell.firstLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(92.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect fnewFrame = cell.firstLabel.frame;
                fnewFrame.size.height = fsize.height;
                cell.firstLabel.frame = fnewFrame;
                [cell.firstLabel sizeToFit];
                
                cell.secondLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"birthday"]];
                CGSize size = [cell.secondLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(105.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.secondLabel.frame;
                newFrame.size.height = size.height;
                cell.secondLabel.frame = newFrame;
                [cell.secondLabel sizeToFit];
                break;
            }
            case 2:
            {
                cell.nameLabel.text=@"手机:";
                cell.infoLabel.text=@"邮件:";
                cell.firstLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"phone"]];
                cell.firstLabel.tag=62;
                CGSize fsize = [cell.firstLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(92.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect fnewFrame = cell.firstLabel.frame;
                fnewFrame.size.height = fsize.height;
                cell.firstLabel.frame = fnewFrame;
                [cell.firstLabel sizeToFit];
                
                cell.secondLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"email"]];
                CGSize size = [cell.secondLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(105.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.secondLabel.frame;
                newFrame.size.height = size.height;
                cell.secondLabel.frame = newFrame;
                [cell.secondLabel sizeToFit];
                break;
            }
            case 3:
            {
                cell.nameLabel.text=@"Q Q:";
                cell.infoLabel.text=@"MSN:";
                cell.firstLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"qq"]];
                CGSize fsize = [cell.firstLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(92.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect fnewFrame = cell.firstLabel.frame;
                fnewFrame.size.height = fsize.height;
                cell.firstLabel.frame = fnewFrame;
                [cell.firstLabel sizeToFit];
                
                cell.secondLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"msn"]];
                CGSize size = [cell.secondLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(105.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.secondLabel.frame;
                newFrame.size.height = size.height;
                cell.secondLabel.frame = newFrame;
                [cell.secondLabel sizeToFit];
                break;
            }
            case 4:
            {
                cell.nameLabel.text=@"地址:";
                cell.firstLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"homeAddress"]];
                cell.firstLabel.numberOfLines = 0;
                cell.firstLabel.lineBreakMode = NSLineBreakByWordWrapping;
                CGSize size = [cell.firstLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(242.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.firstLabel.frame;
                newFrame.size.width=242;
                newFrame.size.height = size.height;
                cell.firstLabel.frame = newFrame;
                [cell.firstLabel sizeToFit];
                break;
            }
            default:
                break;
        }
        return cell;
    }
    else if(indexPath.section==1)
    {
        static NSString *CellIdentifier = @"unitDetailCell";
        unitCell *cell =(unitCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[unitCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        UITapGestureRecognizer *stapRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LabeladdTap:)];
        [cell.deLabel addGestureRecognizer:stapRecognizer];
        
        cell.unitLabel.font = [UIFont systemFontOfSize:14];
        cell.deLabel.font = [UIFont systemFontOfSize:14];
        
        
        
        switch (indexPath.row)
        {
            case 0:
            {
                cell.unitLabel.text=@"单位名称:";
                cell.deLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"unitName"]];
                CGSize size = [cell.textLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(207.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.deLabel.frame;
                newFrame.size.height = size.height;
                cell.deLabel.frame = newFrame;
                [cell.deLabel sizeToFit];
                break;
            }
            case 1:
            {
                cell.unitLabel.text=@"单位地址:";
                cell.deLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"unitAddress"]];
                CGSize size = [cell.textLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(207.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.deLabel.frame;
                newFrame.size.height = size.height;
                cell.deLabel.frame = newFrame;
                [cell.deLabel sizeToFit];
                break;
            }
            case 2:
            {
                cell.unitLabel.text=@"单位传真:";
                cell.deLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"unitFax"]];
                CGSize size = [cell.deLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(207.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.deLabel.frame;
                newFrame.size.height = size.height;
                cell.deLabel.frame = newFrame;
                [cell.deLabel sizeToFit];
                break;
            }
            case 3:
            {
                cell.unitLabel.text=@"单位邮编:";
                cell.deLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"unitPostNumber"]];
                CGSize size = [cell.textLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(207.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.deLabel.frame;
                newFrame.size.height = size.height;
                cell.deLabel.frame = newFrame;
                [cell.deLabel sizeToFit];
                break;
            }
            case 4:
            {
                cell.unitLabel.text=@"办公直连:";
                cell.deLabel.tag=63;
                cell.deLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"unitTel"]];
                CGSize size = [cell.deLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(207.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.deLabel.frame;
                newFrame.size.height = size.height;
                cell.deLabel.frame = newFrame;
                [cell.deLabel sizeToFit];
                break;
            }
            case 5:
            {
                cell.unitLabel.text=@"办公分机:";
                cell.deLabel.tag=64;
                cell.deLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"unitTelExtension"]];
                CGSize size = [cell.textLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(207.0f,1000) lineBreakMode:NSLineBreakByWordWrapping];
                CGRect newFrame = cell.deLabel.frame;
                newFrame.size.height = size.height;
                cell.deLabel.frame = newFrame;
                [cell.deLabel sizeToFit];
                break;
            }
            default:
                break;
        }
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"lastCell";
        unitCell *cell =(unitCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[unitCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.unitLabel.userInteractionEnabled=YES;
        UITapGestureRecognizer *stapRecognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(LabeladdTap:)];
        [cell.unitLabel addGestureRecognizer:stapRecognizer];
        cell.unitLabel.numberOfLines = 0;
        cell.unitLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.unitLabel.text=[NSString stringWithFormat:@"%@",[infoDic objectForKey:@"remark"]];
        CGSize size = [cell.unitLabel.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(290,1000)lineBreakMode:NSLineBreakByWordWrapping];
        CGRect newFrame = cell.unitLabel.frame;
        newFrame.size.height = size.height;
        newFrame.size.width=290;
        cell.unitLabel.frame = newFrame;
        [cell.unitLabel sizeToFit];
        return cell;
    }
   }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==61)
    {
        return 44;
    }
    else
    {
          if(indexPath.section==0)
          {
            infodetailCell *cell=(infodetailCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
            if(cell.secondLabel.frame.origin.y+cell.secondLabel.frame.size.height<44&&cell.firstLabel.frame.origin.y+cell.firstLabel.frame.size.height<44)
            {
               return 44;
            }
            if(cell.secondLabel.frame.origin.y+cell.secondLabel.frame.size.height>cell.firstLabel.frame.origin.y+cell.firstLabel.frame.size.height)
            {
               return cell.secondLabel.frame.origin.y+cell.secondLabel.frame.size.height;
            }
            else
            {
               return  cell.firstLabel.frame.origin.y+cell.firstLabel.frame.size.height;
             }
          }
          else if(indexPath.section==1)
          {
             unitCell *cell=(unitCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
             // NSLog(@"heighu= %f",cell.textLabel.frame.size.height);
             if(cell.deLabel.frame.origin.y+cell.deLabel.frame.size.height<44)
             {
                 return 44;
              }
              return cell.deLabel.frame.origin.y+cell.deLabel.frame.size.height;
           }
          else
          {
             unitCell *cell=(unitCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
             if(cell.unitLabel.frame.origin.y+cell.unitLabel.frame.size.height<44)
             {
                return 44;
              }
             return cell.deLabel.frame.origin.y+cell.unitLabel.frame.size.height;
           }
     }
}

-(void)LabeladdTap:(UITapGestureRecognizer*)sender
{
    UILabel *v = (UILabel *)[sender view];
    NSLog(@"%@",v.text);
    if(v.text==nil)
    {
        return;
    }
    labelText=v.text;
    
    UIView *mainScreen=[[UIView alloc]initWithFrame:self.view.bounds];
    [mainScreen setBackgroundColor:[UIColor colorWithRed:85.0/255.0 green:89.0/255.0 blue:92.0/255.0 alpha:0.3]];
    mainScreen.tag=60;
    [self.view addSubview:mainScreen];
    
    UITableView *outTable=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    if(v.tag==62||v.tag==63||v.tag==64)
    {
        outTable.frame=CGRectMake(20,60,280,176);
    }
    else
    { 
        outTable.frame=CGRectMake(20,104,280,88);
    }
    [outTable setBackgroundColor:[UIColor whiteColor]];
    [outTable setBackgroundView:nil];
    outTable.layer.cornerRadius=5.0f;
    outTable.tag=61;
    outTable.delegate=self;
    outTable.dataSource=self;
    [self.view addSubview:outTable];
    //NSLog(@"%f,%f",sender.,sender.accessibilityFrame.origin.y);
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITableView *table in [self.view subviews])
    {
        if(table.tag==61)
        {
            UITouch *touch=[touches anyObject];
            CGPoint currentPoint=[touch locationInView:self.view];
            if(!CGRectContainsPoint(table.frame, currentPoint))
            {
                [table removeFromSuperview];
                [table release];
                table=nil;
                for (UIView *view in [self.view subviews])
                {
                    if(view.tag==60)
                    {
                        [view removeFromSuperview];
                        [view release];
                    }
                }
            }
        }
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==61)
    {
        if(tableView.frame.size.height==176)
        {
            switch (indexPath.row)
            {
                case 0:
                   break;
                case 1:
                {
                    NSString *phoneNum=[NSString stringWithFormat:@"tel://%@",labelText];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNum]];
                    break;
                }
                case 2:
                {
                    NSString *phoneNum=[NSString stringWithFormat:@"sms://%@",labelText];
                    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:phoneNum]];
                    break;
                }
                case 3:
                {
                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                    [pasteboard setString:labelText];
                    break;
                }
                default:
                    break;
            }
        }
        else
        {
            switch (indexPath.row)
            {
                case 0:
                    break;
                case 1:
                {
                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                    [pasteboard setString:labelText];
                    break;
                }
                default:
                    break;
            }
        }

        for(UIView *view in [self.view subviews])
        {
            if(view.tag==61||view.tag==60)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
