//
//  personInfoCell.m
//  moffice
//
//  Created by lijinhua on 13-7-23.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "personInfoCell.h"

@implementation personInfoCell
@synthesize phonenumLabel,photoImage,nameLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawCell];
    }
    return self;
}

-(void)drawCell
{
    photoImage=[[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 50, 50)];
    [photoImage setBackgroundColor:[UIColor clearColor]];
    [photoImage setImage:[UIImage imageNamed:@"default_head.jpg"]];
    [self addSubview:photoImage];
    
    nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(62, 5, 200, 24)];
    [nameLabel setBackgroundColor:[UIColor clearColor]];
    nameLabel.textColor=[UIColor blackColor];
    nameLabel.textAlignment=UITextAlignmentLeft;
    [self addSubview:nameLabel];
    
    phonenumLabel=[[UILabel alloc]initWithFrame:CGRectMake(62, 31, 200, 24)];
    [phonenumLabel setBackgroundColor:[UIColor clearColor]];
    phonenumLabel.textColor=[UIColor blackColor];
    phonenumLabel.textAlignment=UITextAlignmentLeft;
    [self addSubview:phonenumLabel];
}

-(void)dealloc
{
    [photoImage release];
    [nameLabel release];
    [phonenumLabel release];
    [super dealloc];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
