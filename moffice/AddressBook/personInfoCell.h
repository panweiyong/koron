//
//  personInfoCell.h
//  moffice
//
//  Created by lijinhua on 13-7-23.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface personInfoCell : UITableViewCell
{
    UIImageView *photoImage;
    UILabel *nameLabel;
    UILabel *phonenumLabel;
}
@property(nonatomic,retain) UIImageView *photoImage;
@property(nonatomic,retain) UILabel *nameLabel;
@property(nonatomic,retain) UILabel *phonenumLabel;
@end
