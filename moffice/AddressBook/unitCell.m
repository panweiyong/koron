//
//  unitCell.m
//  moffice
//
//  Created by lijinhua on 13-8-5.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "unitCell.h"

@implementation unitCell
@synthesize unitLabel,deLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawCell];
    }
    return self;
}

-(void)drawCell
{
    unitLabel=[[UILabel alloc]initWithFrame:CGRectMake(18, 0, 80, 44)];
    unitLabel.backgroundColor=[UIColor clearColor];
    //firstLabel.adjustsFontSizeToFitWidth=YES;
    //[nameLabel setFont:[UIFont systemFontOfSize:22.0]];
    [self addSubview:unitLabel];
    
    deLabel=[[UILabel alloc]initWithFrame:CGRectMake(98, 12, 207, 44)];
    deLabel.backgroundColor=[UIColor clearColor];
    deLabel.lineBreakMode = UILineBreakModeWordWrap;
    deLabel.userInteractionEnabled=YES;
    deLabel.numberOfLines = 0;
    deLabel.userInteractionEnabled=YES;
    //firstLabel.adjustsFontSizeToFitWidth=YES;
    [self addSubview:deLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
