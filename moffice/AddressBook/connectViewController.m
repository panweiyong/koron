//
//  connectViewController.m
//  moffice
//
//  Created by lijinhua on 13-7-23.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "connectViewController.h"
#define kSectionSelectorWidth 40
@interface connectViewController ()

@end

@implementation connectViewController
@synthesize navigationBarItem;
@synthesize selectionView=_selectionView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _manager = [KoronManager sharedManager];
    }
    return self;
}

-(void)dealloc
{
    [navigationBarItem release];
    [listArray release];
    [firstStrArray release];
    [result release];
    [selectCellDic release];
    
    [_addressBookRequest clearDelegatesAndCancel];
    [_addressBookRequest release];
    
    [super dealloc];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)dataGet
{
    [self sendAddressBookReqeust];
}


- (void)sendAddressBookReqeust {
    if (_addressBookRequest) {
        [_addressBookRequest clearDelegatesAndCancel];
        [_addressBookRequest release];
    }
    
    _addressBookRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[_manager getObjectForKey:SERVERURL]]];
    [_addressBookRequest setDelegate:self];
    [_addressBookRequest setDidFinishSelector:@selector(requestFinished:)];
    [_addressBookRequest setDidFailSelector:@selector(requestFailed:)];
    [_addressBookRequest setPostValue:[_manager getObjectForKey:SID] forKey:SID];
    [_addressBookRequest setPostValue:@"syncContact" forKey:@"op"];
    [_addressBookRequest startAsynchronous];
}


//异步接收到返回的数据
- (void)requestFinished:(ASIFormDataRequest *)response
{

    SBJsonParser *parser = [[SBJsonParser alloc]init];
    NSDictionary *dictionary = [parser objectWithString:[response responseString]];
    [parser release];
    
    listArray=[NSArray arrayWithArray:[dictionary objectForKey:@"list"]];
    if([listArray count]!=0)
    {
         [self listArraySort:listArray];
    }
    
}

//按照拼音顺序对数组进行排序
-(void)listArraySort:(NSArray*)stringsToSort
{
    //Step1:获取字符串中文字的拼音首字母并与字符串共同存放
    [result removeAllObjects];
    NSMutableArray *chineseStringsArray=[NSMutableArray array];
    for(int i=0;i<[stringsToSort count];i++){
        ChineseString *chineseString=[[ChineseString alloc]init];

        chineseString.string=[NSString stringWithString:[[stringsToSort objectAtIndex:i] objectForKey:@"name"]];
        //NSLog(@"%@",[[stringsToSort objectAtIndex:i] objectForKey:@"deptFullName"]);
        if(chineseString.string==nil){
            chineseString.string=@"";
        }
        
        if(![chineseString.string isEqualToString:@""]){
            //NSString *pinYinResult=[NSString string];
            //for(int j=0;j<chineseString.string.length;j++){
                NSString *singlePinyinLetter=[[NSString stringWithFormat:@"%c",pinyinFirstLetter([chineseString.string characterAtIndex:0])]uppercaseString];
                
                //pinYinResult=[pinYinResult stringByAppendingString:singlePinyinLetter];
           // }
            chineseString.pinYin=singlePinyinLetter;
        }else{
            chineseString.pinYin=@"";
        }

        NSMutableDictionary *dic=[stringsToSort objectAtIndex:i];
        [dic setObject:chineseString.pinYin forKey:@"firstLetter"];
        [result addObject:dic];
        
        [chineseStringsArray addObject:chineseString];
    }
    
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"firstLetter" ascending:YES]];
    [result sortUsingDescriptors:sortDescriptors];
    
    
      for(int i=0;i<[result count];i++)
    {
        NSString* letter=[[result objectAtIndex:i] objectForKey:@"firstLetter"];
        if(![firstStrArray containsObject:letter])
        {
            [firstStrArray addObject:letter];
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:result forKey:@"connectInfo"];
    [[NSUserDefaults standardUserDefaults] setObject:firstStrArray forKey:@"Letter"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [ac stopAnimating];
    [personList reloadData];
    [_selectionView reloadSections];
    [self removeBackgroundView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _dismissBtn.hidden = NO;
    
    _topLabel.text = @"联系人";
    
    listArray=[[NSArray alloc]init];
    firstStrArray=[[NSMutableArray alloc]initWithCapacity:0];
    [firstStrArray removeAllObjects];
    
    result=[[NSMutableArray alloc]initWithCapacity:0];
    [result removeAllObjects];
    
    listContentArr=[[NSMutableArray alloc]initWithCapacity:0];
    
    selectCellDic=[[NSDictionary alloc]init];
    
    UIBarButtonItem *done =[[UIBarButtonItem alloc] initWithTitle:@"同步数据" style:UIBarButtonItemStyleBordered target:self action:@selector(getData)];
    self.navigationItem.rightBarButtonItem = done;
    
    personList=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    
    
    personList.tag=10;
    [personList setDelegate:self];
    [personList setDataSource:self];
    [personList setBackgroundView:nil];
    [personList setBackgroundColor:[UIColor whiteColor]];
    [personList setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [self.view addSubview:personList];
    
    sections = [NSArray arrayWithObjects:@"#",@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",nil];
    _selectionView = [[CHSectionSelectionView alloc] init];
    
    _selectionView.backgroundColor =[UIColor clearColor]; //[UIColor colorWithRed:248.0/255.0 green:248.0/255.0 blue:248.0/255.0 alpha:0.0];
    _selectionView.dataSource = self;
    _selectionView.delegate = self;
    _selectionView.showCallouts = YES; // the view should show a callout when an item is selected
    _selectionView.calloutDirection = SectionCalloutDirectionLeft; // Callouts should appear on the right side
    _selectionView.calloutPadding = 20;
    [self.view addSubview:_selectionView];
    
    
    
    if (IOS_VERSIONS >= 7.0) {
        [personList setFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
        _selectionView.frame = CGRectMake(self.view.bounds.size.width-kSectionSelectorWidth,64, kSectionSelectorWidth, self.view.bounds.size.height-64);
    }else {
        [personList setFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
        _selectionView.frame = CGRectMake(self.view.bounds.size.width-kSectionSelectorWidth,44, kSectionSelectorWidth, self.view.bounds.size.height-44);
    }
    
    
    
     NSArray* Connect=[[NSUserDefaults standardUserDefaults] objectForKey:@"connectInfo"];
     NSArray* Letter=[[NSUserDefaults standardUserDefaults] objectForKey:@"Letter"];
    if([Connect count]!=0&&[Letter count]!=0)
    {
        result=[[NSMutableArray arrayWithArray:Connect] retain];
        firstStrArray=[[NSMutableArray arrayWithArray:Letter] retain];
        [personList reloadData];
        [_selectionView reloadSections];
    }
    else
    {
      [self addBackgroundView];
      [self dataGet];
   }
}

-(void)addBackgroundView
{
    UIView *mainScreen=[[UIView alloc]initWithFrame:self.view.bounds];
    [mainScreen setBackgroundColor:[UIColor colorWithRed:85.0/255.0 green:89.0/255.0 blue:92.0/255.0 alpha:0.3]];
    mainScreen.tag=30;
    [self.view addSubview:mainScreen];
    
    UILabel *backLabel=[[UILabel alloc]initWithFrame:CGRectMake(110, self.view.frame.size.height/2-40, 100, 80)];
    [backLabel setBackgroundColor:[UIColor grayColor]];
    backLabel.tag=31;
    backLabel.layer.cornerRadius=5.0f;
    [self.view addSubview:backLabel];
    
    UIActivityIndicatorView *activity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activity.tag=32;
    [activity setCenter:CGPointMake(160, self.view.frame.size.height/2)];
    [activity startAnimating];
    [self.view addSubview:activity];
}

-(void)removeBackgroundView
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==30||view.tag==31||view.tag==32)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}


-(void)getData
{
    //[ac startAnimating];
    [self addBackgroundView];
    [self dataGet];
}

- (void)animate:(DAReloadActivityButton *)button
{

        if ([button isAnimating])
        {   
            [button stopAnimating];
            [_addressBookRequest clearDelegatesAndCancel];
        }
        else
        {  
            [button startAnimating];
            [self addBackgroundView];
            [self dataGet];
        }
}

//////////////////////////////////////////////////////////////////////////
#pragma mark - SectionSelectionView DataSource

// Tell the datasource how many sections we have - best is to forward to the tableviews datasource
-(NSInteger)numberOfSectionsInSectionSelectionView:(CHSectionSelectionView *)sectionSelectionView
{
    return [personList.dataSource numberOfSectionsInTableView:personList];
}

// Create a nice callout view so that you see whats selected when
// your finger covers the sectionSelectionView
-(UIView *)sectionSelectionView:(CHSectionSelectionView *)selectionView callOutViewForSelectedSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] init];
    
    label.frame = CGRectMake(0, 0, 80, 80); // you MUST set the size of the callout in this method
    
    // do some ui stuff
    
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor grayColor];
    label.font = [UIFont boldSystemFontOfSize:40];
    label.text = [personList.dataSource tableView:personList titleForHeaderInSection:section];
    label.textAlignment = UITextAlignmentCenter;
    
    // dont use that in your code cause layer shadows are
    // negatively affecting performance
    
    [label.layer setCornerRadius:label.frame.size.width/2];
    [label.layer setBorderColor:[UIColor clearColor].CGColor];
    [label.layer setBorderWidth:3.0f];
    [label.layer setShadowColor:[UIColor blackColor].CGColor];
    [label.layer setShadowOpacity:0.8];
    [label.layer setShadowRadius:5.0];
    [label.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    return label;
}


// Create the view that is displayed inside the sectionSelectionView
// This is basically our letter index. CHSectionSelectionItemView subclass
// should be used here. The frame of the view is set by the sectionSelectionView
// it takes its, width and height/numberOfSections for the height
-(CHSectionSelectionItemView *)sectionSelectionView:(CHSectionSelectionView *)selectionView sectionSelectionItemViewForSection:(NSInteger)section
{
    DemoSectionItemSubclass *selectionItem = [[DemoSectionItemSubclass alloc] init];
    
    selectionItem.titleLabel.text = [personList.dataSource tableView:personList titleForHeaderInSection:section];
    selectionItem.titleLabel.font = [UIFont systemFontOfSize:12];
    selectionItem.titleLabel.textColor = [UIColor darkGrayColor];
    selectionItem.titleLabel.highlightedTextColor = [UIColor orangeColor];
    selectionItem.titleLabel.shadowColor = [UIColor whiteColor];
    selectionItem.titleLabel.shadowOffset = CGSizeMake(0, 1);
    
    return selectionItem;
}

//////////////////////////////////////////////////////////////////////////
#pragma mark - SectionSelectionView Delegate

// Jump to the selected section in our tableview
-(void)sectionSelectionView:(CHSectionSelectionView *)sectionSelectionView didSelectSection:(NSInteger)section
{
    [personList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //[_selectionView reloadSections];
}


-(BOOL)shouldAutorotate
{
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag==10)
    {
        return [firstStrArray count];
    }
    else
    {
        return 1;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView.tag==10)
    {
        return [firstStrArray objectAtIndex:section];
    }
    else
    {
        return nil;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag==10)
    {
       int Num=0;
       for(int i=0;i<[result count];i++)
       {
           NSString *string1=[[result objectAtIndex:i] objectForKey:@"firstLetter"];
           NSString *string2=[firstStrArray objectAtIndex:section];
           if ([string1 isEqualToString:string2])
           {
              Num++;
           }
        }
        return Num;
    }
    else
    {
        return 5;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==10)
    {
      static NSString *CellIdentifier = @"personCell";
      personInfoCell *cell = (personInfoCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
      if (cell == nil) {
        cell = [[[personInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
      }
      cell.selectionStyle=UITableViewCellSelectionStyleNone;
      [listContentArr removeAllObjects];
      for(int i=0;i<[result count];i++)
      {
        NSString *string1=[[result objectAtIndex:i] objectForKey:@"firstLetter"];
        NSString *string2=[firstStrArray objectAtIndex:indexPath.section];
        if ([string1 isEqualToString:string2])
        {
            [listContentArr addObject:[result objectAtIndex:i]];
        }
      }
      if([listContentArr count]!=0)
      {
        cell.nameLabel.text=[[listContentArr objectAtIndex:indexPath.row] objectForKey:@"name"];
        if([[listContentArr objectAtIndex:indexPath.row] objectForKey:@"phone"]!=nil&&[[[listContentArr objectAtIndex:indexPath.row] objectForKey:@"phone"] length]!=0)
        {
            cell.phonenumLabel.text=[NSString stringWithFormat:@"手机:%@",[[listContentArr objectAtIndex:indexPath.row] objectForKey:@"phone"]];
        }
        else if([[listContentArr objectAtIndex:indexPath.row] objectForKey:@"unitTel"]!=nil&&[[[listContentArr objectAtIndex:indexPath.row] objectForKey:@"unitTel"] length]!=0)
        {
            cell.phonenumLabel.text=[NSString stringWithFormat:@"办公直连:%@",[[listContentArr objectAtIndex:indexPath.row] objectForKey:@"unitTel"]];
        }
        else if([[listContentArr objectAtIndex:indexPath.row] objectForKey:@"unitTelExtension"]!=nil&&[[[listContentArr objectAtIndex:indexPath.row] objectForKey:@"unitTelExtension"] length]!=0)
        {
            cell.phonenumLabel.text=[NSString stringWithFormat:@"办公分机:%@",[[listContentArr objectAtIndex:indexPath.row] objectForKey:@"unitTelExtension"]];
        }
        else
        {   
            cell.phonenumLabel.text=nil;
        }
      }
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"nomalCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        switch (indexPath.row)
        {
            case 0:
                cell.textLabel.textColor=[UIColor whiteColor];
                cell.textLabel.text=[selectCellDic objectForKey:@"name"];
                cell.textLabel.backgroundColor=[UIColor clearColor];
                UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
                backgrdView.backgroundColor = [UIColor blackColor];
                cell.backgroundView = backgrdView;
                break;
            case 1:
                cell.textLabel.text=@"打电话";
                break;
            case 2:
                cell.textLabel.text=@"发短信";
                break;
            case 3:
                cell.textLabel.text=@"查看详细";
                break;
            case 4:
                cell.textLabel.text=@"导入到手机";
                break;
            default:
                break;
        }
        return cell;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==10)
    {
      return 60;
    }
    else
    {
        return 44;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==10)
    {
        personInfoCell *cell=(personInfoCell*)[self tableView:tableView cellForRowAtIndexPath:indexPath];
        for(int i=0;i<[result count];i++)
        {
            NSString *string1=cell.nameLabel.text;
            //NSString *string2=[cell.phonenumLabel.text substringFromIndex:4];
            NSDictionary *dic=[result objectAtIndex:i];
            if ([[dic objectForKey:@"name"] isEqualToString:string1])
            {
                selectCellDic=dic;
                break;
            }
        }
        //NSLog(@"selectCellDic=%@",selectCellDic);
        [self addTableView];
    }
    else
    {
        for(UIView *view in [self.view subviews])
        {
            if(view.tag==20||view.tag==11)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
        switch(indexPath.row)
        {
            case 1:
            {
                NSString *phoneNum=[NSString stringWithFormat:@"tel://%@",[selectCellDic objectForKey:@"phone"]];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNum]];
                break;
            }
            case 2:
            {
                NSString *phoneNum=[NSString stringWithFormat:@"sms://%@",[selectCellDic objectForKey:@"phone"]];
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:phoneNum]];
                break;
            }
            case 3:
            {
                infoDetailViewController *infoDetail=[[infoDetailViewController alloc]initWithNibName:nil bundle:nil];
                [infoDetail setInfoDic:selectCellDic];
                infoDetail.hidesBottomBarWhenPushed=YES;
                [self presentViewController:infoDetail animated:YES completion:nil];
                [infoDetail release];
                break;
            }
            case 4:
            {
                SJABHelper *helper=[SJABHelper shareControl];
                switch([helper existPhone:[selectCellDic objectForKey:@"phone"]])
                {
                    case 0:
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"无法连接到电话簿!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alert show];
                        [alert release];
                        break;
                    }
                    case 1:
                    {
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"该号码已存在!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                        [alert show];
                        [alert release];
                        break;
                    }
                    case 2:
                    {
                        if([helper addContactName:[selectCellDic objectForKey:@"name"] phoneNum:[selectCellDic objectForKey:@"phone"] withLabel:nil])
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"导入成功!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                            [alert show];
                            [alert release];
                        }
                        else
                        {
                            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"导入失败!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                            [alert show];
                            [alert release];
                        }
                        break;
                    }
                }
                break;
            }
            default:
                break; 
        }
    }
}


-(void)addTableView
{
    UIView *mainScreen=[[UIView alloc]initWithFrame:self.view.bounds];
    [mainScreen setBackgroundColor:[UIColor colorWithRed:85.0/255.0 green:89.0/255.0 blue:92.0/255.0 alpha:0.3]];
    mainScreen.tag=20;
    [self.view addSubview:mainScreen];
    
    UITableView *outTable=[[UITableView alloc]initWithFrame:CGRectMake(10, self.view.frame.size.height /2 - 110, 300, 220) style:UITableViewStylePlain];
    [outTable setBackgroundColor:[UIColor whiteColor]];
    [outTable setBackgroundView:nil];
    outTable.layer.cornerRadius=5.0f;
    outTable.tag=11;
    outTable.delegate=self;
    outTable.dataSource=self;
    [self.view addSubview:outTable];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITableView *table in [self.view subviews])
    {
        if(table.tag==11)
        {
            UITouch *touch=[touches anyObject];
            CGPoint currentPoint=[touch locationInView:self.view];
            if(!CGRectContainsPoint(table.frame, currentPoint))
            {
                [table removeFromSuperview];
                [table release];
                table=nil;
                for (UIView *view in [self.view subviews])
                {
                    if(view.tag==20)
                    {
                        [view removeFromSuperview];
                        [view release];
                    }
                }
            }
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
