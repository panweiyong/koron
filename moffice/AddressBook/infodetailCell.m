//
//  infodetailCell.m
//  moffice
//
//  Created by lijinhua on 13-7-31.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "infodetailCell.h"

@implementation infodetailCell
@synthesize nameLabel,firstLabel,secondLabel,infoLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawCell];
    }
    return self;
}

-(void)drawCell
{
    nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(18, 0, 45, 44)];
    nameLabel.backgroundColor=[UIColor clearColor];
    //firstLabel.adjustsFontSizeToFitWidth=YES;
    //[nameLabel setFont:[UIFont systemFontOfSize:22.0]];
    [self addSubview:nameLabel];
    
    firstLabel=[[UILabel alloc]initWithFrame:CGRectMake(63, 12, 92, 44)];
    firstLabel.backgroundColor=[UIColor clearColor];
    firstLabel.lineBreakMode = UILineBreakModeWordWrap;
    firstLabel.numberOfLines = 0;
    firstLabel.userInteractionEnabled=YES;
    //firstLabel.adjustsFontSizeToFitWidth=YES;
    [self addSubview:firstLabel];
  
    infoLabel=[[UILabel alloc]initWithFrame:CGRectMake(155, 0, 45, 44)];
    infoLabel.backgroundColor=[UIColor clearColor];
    //firstLabel.adjustsFontSizeToFitWidth=YES;
    [self addSubview:infoLabel];
    
    secondLabel=[[UILabel alloc]initWithFrame:CGRectMake(200, 12, 105, 44)];
    secondLabel.lineBreakMode = UILineBreakModeWordWrap;
    secondLabel.numberOfLines = 0;
    secondLabel.userInteractionEnabled=YES;
    secondLabel.backgroundColor=[UIColor clearColor];
    [self addSubview:secondLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
