//
//  HomePageOptionCell.m
//  moffice
//
//  Created by CA on 13-11-3.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "HomePageOptionCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation HomePageOptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews {
    _iconLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//    _iconLabel.backgroundColor = [UIColor redColor];
    _iconLabel.font = [UIFont systemFontOfSize:20];
    [self addSubview:_iconLabel];
    
    _iconView = [[UIImageView alloc] initWithFrame:CGRectZero];
//    _iconView.backgroundColor = [UIColor orangeColor];
    _iconView.layer.cornerRadius = 10;
    [self addSubview:_iconView];

    _iconSelectedView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _iconSelectedView.contentMode = UIViewContentModeScaleToFill;
    [self.contentView addSubview:_iconSelectedView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    _iconView.frame = CGRectMake(10, 7.5, self.height-15, self.height-15);
    _iconLabel.frame = CGRectMake(_iconView.right+10, self.height/2.0-15, 100, 30);
    _iconSelectedView.frame = CGRectMake(self.width-40, self.height/2.0-12.5, 25, 25);
}

- (void)dealloc
{
    self.iconSelectedView = nil;
    self.iconView = nil;
    self.iconLabel = nil;
    [super dealloc];
}

@end
