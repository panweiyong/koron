//
//  clientModuleModel.h
//  moffice
//
//  Created by Mac Mini on 13-11-14.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "BaseModel.h"
/*
 "moduleList": [
 {
 "id": "1",
 "moduleName": "默认模版",
 */
@interface KoronClientModuleModel : BaseModel

@property (nonatomic, copy) NSString *modelId;
@property (nonatomic, copy) NSString *moduleName;

@end
