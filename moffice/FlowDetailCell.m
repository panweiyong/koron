//
//  FlowDetailCell.m
//  moffice
//
//  Created by Mac Mini on 13-11-7.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "FlowDetailCell.h"

@implementation FlowDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    if (IOS_VERSIONS > 6.9) {
        frame.origin.x += 15;
        frame.size.width -= 30;
        [super setFrame:frame];
    }
    
    [super setFrame:frame];
}

@end
