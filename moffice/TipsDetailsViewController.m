//
//  TipsDetailsViewController.m
//  moffice
//
//  Created by yangxi zou on 12-1-10.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "TipsDetailsViewController.h"

@implementation TipsDetailsViewController
@synthesize type;
@synthesize refid;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {		
        // Custom initialization
        self.title=NSLocalizedString(@"Tips",@"Tips") ; 
    }
    return self;
}
-(void)handleTapFrom:(UITapGestureRecognizer *)recognizer{ 
    UIImageView *iv =(UIImageView *) [recognizer view];
    
    [self setHidesBottomBarWhenPushed:YES ];
    ImageViewController *imgc=[[ImageViewController alloc] initWithNibName:nil bundle:nil];
    [imgc setDisplayImage: [iv image] ];
    [self.navigationController pushViewController:imgc animated:YES];
    [imgc release];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return  [[ tableData objectForKey:@"attrs"]   count];
} 

-(NSString*)parseName:(NSString*)n
{
    //NSAssert(n!=nil, @"Invalid Field Value");
    if(n==nil)return @"unkonw";
    return [n substringToIndex:[ n rangeOfString:@"|"] .location  ];
}
- (DTAttributedTextContentView *)contentViewForIndexPath:(NSIndexPath *)indexPath
{
	if (!contentViewCache)
	{
		contentViewCache = [[NSMutableDictionary alloc] init];
	}
	
	DTAttributedTextContentView *contentView =nil;// (id)[contentViewCache objectForKey:indexPath];
	
	if (!contentView)
	{
		NSDictionary *snippet = [[ tableData objectForKey:@"attrs"] objectAtIndex:indexPath.row];
		
		NSString *title = [self parseName:  [snippet objectForKey:@"key"]];
		NSString *description = [snippet objectForKey:@"value"];
		
		NSString *html = [NSString stringWithFormat:@"<h3>%@</h3><p><font color=\"gray\">%@</font></p>", title, description];
		NSData *data = [html dataUsingEncoding:NSUTF8StringEncoding];
		NSAttributedString *string = [[[NSAttributedString alloc] initWithHTML:data documentAttributes:NULL] autorelease];
		
		// set width, height is calculated later from text
		CGFloat width = self.view.frame.size.width;
		[DTAttributedTextContentView setLayerClass:nil];//[CATiledLayer class]
		contentView = [[[DTAttributedTextContentView alloc] initWithAttributedString:string width:width - 20.0] autorelease];
		
		contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		contentView.edgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        
		[contentViewCache setObject:contentView forKey:indexPath];
        contentView.backgroundColor= [UIColor clearColor];
	}
	
	return contentView;
}

- (UITableViewCell *)tableView:(UITableView *)tableViewz cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	UITableViewCellStyle style = UITableViewCellStyleValue1; 
	NSString *cellid= @"tid";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    }else{
       [[cell.contentView viewWithTag:100] removeFromSuperview];
    }
    
    /*
    NSDictionary *dd=[[ tableData objectForKey:@"attrs"] objectAtIndex:indexPath.row] ;
    NSString *fkey= [ dd   objectForKey:@"key"] ;
    DisplayControl *dobj= [JsonUtils ParseControl:fkey data:[ dd  objectForKey:@"value"] delegate:self textAlignment:UITextAlignmentCenter];
    dobj.tag=100;
    //cell.textLabel.text=  dobj.title;
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell.contentView addSubview:dobj];
     */
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    DTAttributedTextContentView *contentView = [self contentViewForIndexPath:indexPath];
    contentView.frame = cell.contentView.bounds;
    //contentView.shouldDrawImages=YES;  
    //contentView.delegate = self;
    contentView.tag = 1;
    //contentView.drawDebugFrames=YES;
    [cell.contentView addSubview:contentView];
    cell.backgroundColor = [UIColor whiteColor];
    
    //cell.detailTextLabel.text =  [[[[tableData objectAtIndex:indexPath.row] objectForKey:@"createTime"]  dateFromISO8601]stringWithFormat:@"HH:mm"];
    //cell.textLabel.backgroundColor  = [UIColor clearColor];
    //cell.detailTextLabel.backgroundColor  = [UIColor clearColor];
    //cell.imageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"tips_mail.png" ]  ];
   
	return cell;
}
- (CGFloat)tableView:(UITableView *)tableViewz heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ 	
    //UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    UITableViewCell *cell = (UITableViewCell *)[self tableView:tableViewz cellForRowAtIndexPath:indexPath];
    
    if(cell!=nil&&[[cell.contentView subviews ] count]>0){
        
        UIView *c=(UIView *)[[cell.contentView subviews ] objectAtIndex:0];
        //CCLOG(@"count:%i",[[cell.contentView subviews ] count]);
        if(c)return c.frame.size.height+15.0f;
    }else{
        return 30;
    }
    return 30;
}
-(void)viewWillAppear:(BOOL)animated
{
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice getTipDetails:refid t:type];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStartAnimation" object:nil];
}
- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
    [[JsonService sharedManager] cancelAllRequest];
    [self setHidesBottomBarWhenPushed:NO]; 
    [super viewDidDisappear:animated]; 
} 
/*
- (void)tableView:(UITableView *)tableViewx willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
}*/
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadNavigationItem];
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:  [SMFileUtils fullBundlePath:@"tipsbg.png" ] ]];
    tableView = [[UITableView alloc]  initWithFrame:CGRectMake(0,0, self.view.bounds.size.width ,self.view.bounds.size.height  - self.tabBarController.tabBar.frame.size.height- self.navigationController.navigationBar.frame.size.height  ) style: UITableViewStyleGrouped];
    [tableView setDelegate:self]; 
    [tableView setDataSource:self]; 
    // This should be set to work with the image height 
    [tableView setRowHeight:45];
    tableView.showsHorizontalScrollIndicator= NO;
    tableView.showsVerticalScrollIndicator=YES;
    tableView.scrollEnabled=YES;
    tableView.indicatorStyle =UIScrollViewIndicatorStyleDefault ;
    [tableView setBackgroundColor:DETAILPAGE_BG]; 
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; 
    [tableView setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    //[tableView.layer setShadowOpacity:0.5];
    //[tableView.layer setShadowOffset:CGSizeMake(1, 1)];
    [self.view addSubview:tableView];
    
    
    UIActivityIndicatorView *processAlert=[SMView showProcessing];
    processAlert.center = tableView.center;
    processAlert.tag=10001;
    [self.view addSubview:processAlert];
    [processAlert release];
    
}

//异步接收到返回的数据
- (void)requestDataFinished:(id)jsondata;//request
{
    tableData=[jsondata retain];
    [super requestDataFinished:jsondata];
    NSLog(@"%@",jsondata);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
    //更新标记未已读
   // JsonService *jservice=[JsonService sharedManager];
   // [jservice setDelegate:self];
   // [jservice UpdateFlag:type oid:refid flag:1 tipsid:self.refid];
    
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}

- (void)backButtonPressed {
    if ([self.navigationController respondsToSelector:@selector(popViewControllerAnimated:)]) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if ([self.navigationController respondsToSelector:@selector(dismissModalViewControllerAnimated:)]) {
        [self.navigationController dismissModalViewControllerAnimated:YES];
    }
    
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}  

@end
