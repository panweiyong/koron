//
//  PostingViewController.m
//  moffice
//
//  Created by lijinhua on 13-6-8.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "PostingViewController.h"
#import "categoryListViewController.h"
#import  <MobileCoreServices/UTCoreTypes.h>
#import "merImageViewController.h"
#import "BbsViewController.h"
//#import "MapViewController.h"

@interface PostingViewController ()

@end

@implementation PostingViewController
@synthesize backgroundView,alertTabView;
@synthesize littleMap,_search,activity,detailbutton;
@synthesize caDetailLabel,cateView;
@synthesize titleStr,topId,contentStr,whereAmI,netIsok,promptStr,codeStr;
@synthesize titleField,contentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=@"发帖";
    }
    return self;
}


-(void)dealloc 
{
    
//    [backgroundView release];
//    [alertTabView release];
//    [littleMap release];
//    [_search release];
//    [activity release];
//    [detailbutton release];
//    [caDetailLabel release];
//    [cateView release];
//    [titleStr release];
//    [topId release];
//    [content release];
//    [whereAmI release];
    littleMap.delegate = nil;
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    imageArray=[[[NSMutableArray alloc]initWithCapacity:0] retain];
    [imageArray removeAllObjects];
    isRetina1 = FALSE;
    //self.tabBarController.tabBar.hidden=YES;
    UIBarButtonItem *done =[[UIBarButtonItem alloc] initWithTitle:@"发送" style:UIBarButtonItemStyleBordered target:self action:@selector(postData:)];
    self.navigationItem.rightBarButtonItem = done;
    
    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0,self.view.frame.size.width, 40)];
    [titleLabel setUserInteractionEnabled:YES];
    [titleLabel setBackgroundColor:[UIColor colorWithRed:213.0/255.0 green:233.0/255.0 blue:255.0/255.0 alpha:1.0]];
    [titleLabel setFont:[UIFont systemFontOfSize:14.0]];
    titleLabel.text=@"标题 :";
    titleLabel.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:titleLabel];
    
    titleField=[[UITextField alloc]initWithFrame:CGRectMake(38, 5, self.view.frame.size.width-43, 30)];
    titleField.delegate=self;
//    titleField.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    [titleField setBackgroundColor:[UIColor whiteColor]];
    titleField.font=[UIFont systemFontOfSize:14.0];
    titleField.placeholder=[NSString stringWithFormat:@"%@",@"请输入标题"];
    titleField.autocorrectionType = UITextAutocorrectionTypeNo;
    titleField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    titleField.returnKeyType=UIReturnKeyDone;
    CALayer* fieldLayer=titleField.layer;
    fieldLayer.borderColor=[[UIColor colorWithRed:235.0/255.0 green:239.0/255.0 blue:241.0/255.0 alpha:1.0] CGColor];
    fieldLayer.borderWidth=1.0;
    fieldLayer.cornerRadius=2.0;
    fieldLayer.masksToBounds=YES;
    [titleLabel addSubview:titleField];
    
    UILabel *cagoryLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 40,self.view.frame.size.width, 40)];
    [cagoryLabel setUserInteractionEnabled:YES];
    [cagoryLabel setBackgroundColor:[UIColor colorWithRed:213.0/255.0 green:233.0/255.0 blue:255.0/255.0 alpha:1.0]];
    [cagoryLabel setFont:[UIFont systemFontOfSize:14.0]];
    cagoryLabel.text=@"分类 :";
    cagoryLabel.textAlignment=NSTextAlignmentLeft;
    [self.view addSubview:cagoryLabel];
    
    caDetailLabel=[[UILabel alloc]initWithFrame:CGRectMake(38, 45, self.view.frame.size.width-43, 30)];
    caDetailLabel.userInteractionEnabled=YES;
    caDetailLabel.backgroundColor=[UIColor whiteColor];
    caDetailLabel.font=[UIFont systemFontOfSize:14.0];
    caDetailLabel.text=[[NSUserDefaults standardUserDefaults] objectForKey:@"caDetailString"];
    //caDetailLabel.text=@"知识分享";
    //topId=[[NSString stringWithFormat:@"%@",@"b74a05c4_1206051706257961689"] retain];
    caDetailLabel.textAlignment=NSTextAlignmentLeft;
    
    UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapPressLabel:)];
    [caDetailLabel addGestureRecognizer:tapPress];
    CALayer* labelLayer=titleField.layer;
    labelLayer.borderColor=[[UIColor colorWithRed:235.0/255.0 green:239.0/255.0 blue:241.0/255.0 alpha:1.0] CGColor];
    labelLayer.borderWidth=1.0;
    labelLayer.cornerRadius=2.0;
    labelLayer.masksToBounds=YES;
    [self.view addSubview:caDetailLabel];
    
    UIImageView *detailImage=[[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width-25, 45, 20, 30)];
    [detailImage setImage:[UIImage imageNamed:@"celldetail.png"]];
    [self.view addSubview:detailImage];
    [detailImage release];
    
    textviewIsfirstEdite=YES;
    contentView=[[UITextView alloc]initWithFrame:CGRectMake(5, 85, self.view.frame.size.width-10, 180)];
    contentView.delegate=self;
    contentView.font=[UIFont systemFontOfSize:14.0];
    contentView.text=@"请输入内容";
    contentView.returnKeyType=UIReturnKeyDone;
    CALayer *textViewlayer=contentView.layer;
    textViewlayer.borderColor=[[UIColor colorWithRed:235.0/255.0 green:239.0/255.0 blue:241.0/255.0 alpha:1.0] CGColor];
    textViewlayer.borderWidth=1.0;
    textViewlayer.cornerRadius=2.0;
    textViewlayer.masksToBounds=YES;
    [self.view addSubview:contentView];
    
    UIButton *camerImageview=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-45, self.view.frame.size.height-80, 40, 30)];
    [camerImageview setBackgroundImage:[UIImage imageNamed:@"camerimage.png"] forState:UIControlStateNormal];
    UITapGestureRecognizer *camerTapPress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageAddbtnTouched:)];
    [camerImageview addGestureRecognizer:camerTapPress];
    [self.view addSubview:camerImageview];
    
    
    littleMap=[[BMKMapView alloc]initWithFrame:CGRectMake(0,300,10,10)];
    [littleMap setShowsUserLocation:YES];
    littleMap.delegate=self;
    //[self.view addSubview:littleMap];
    //[self showWithlocation];
    
    


    detailbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    detailbutton.frame=CGRectMake(10, self.view.frame.size.height-80, 270, 30);
    CALayer *demandbtnlayer=[detailbutton layer];
    [demandbtnlayer setMasksToBounds:YES];
    detailbutton.titleLabel.backgroundColor=[UIColor clearColor];
    detailbutton.titleLabel.font = [UIFont systemFontOfSize: 12.0];
    //[detailbutton setTitle:@"    获取地址" forState:UIControlStateNormal];
    detailbutton.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
    detailbutton.contentEdgeInsets=UIEdgeInsetsMake(0,5, 0, 0);
    [detailbutton setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    [detailbutton addTarget:self action:@selector(detailbuttonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:detailbutton];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"whereAmI"]!=nil)
    {
        whereAmI=[[NSUserDefaults standardUserDefaults]objectForKey:@"whereAmI"];
        detailbutton.frame=CGRectMake(10, self.view.frame.size.height-194, self.view.frame.size.width-55,27);
        [detailbutton setTitle:whereAmI forState:UIControlStateNormal];
        [detailbutton setImage:[UIImage imageNamed:@"icon_lbs_act.png"] forState:UIControlStateNormal];
        [detailbutton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-10,0.0,0.0)];
    }
    else
    {
        activity=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0,0, 27, 27)];
        [activity setCenter:CGPointMake(5+27/2, self.view.frame.size.height-65)];//指定进度轮中心点
        [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
        [self.view addSubview:activity];
        [activity startAnimating];
        [detailbutton setTitle:@"    获取地址" forState:UIControlStateNormal];
    }

}

-(void)detailbuttonTouched:(id)sender
{
//    MapViewController *mapView=[[MapViewController alloc]initWithNibName:nil bundle:nil];
//    [mapView setWhereAmI:whereAmI];
//    mapView.hidesBottomBarWhenPushed=YES;
//    [self.navigationController pushViewController:mapView animated:YES];
    //[mapView release];
}

//textview委托方法
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];
    if(textviewIsfirstEdite)
    {
        textView.text=nil;
        textviewIsfirstEdite=NO;
    }
}



- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    contentStr=[textView.text retain];
    NSLog(@"%@",contentStr);
    //self.tabBarController.tabBar.frame=CGRectMake(0, 519, 320, 49);
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        //content=textView.text;
       // NSLog(@"%@",content);
        return NO;
    }
    return YES;
}

//委托方法
//当开始点击textField会调用的方法
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [customerField becomeFirstResponder];
//    self.tabBarController.tabBar.frame=CGRectMake(0, 305, self.view.frame.size.width, 49);
//    //NSLog(@"000000");
//}

-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
    // self.tabBarController.tabBar.frame=CGRectMake(0, 215, 320, 49);

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // customerField = textField;
    //  self.tabBarController.tabBar.frame=CGRectMake(0, 519, 320, 49);
    [textField resignFirstResponder];
    titleStr=[textField.text retain];
    //NSLog(@"%@",titleStr);
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}


//按下Done按钮的调用方法，我们让键盘消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    //self.tabBarController.tabBar.frame=CGRectMake(0, 519, 320, 49);
    return YES;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITableView *table in [self.view subviews])
    {
        if(table.tag==10085)
        {
            UITouch *touch=[touches anyObject];
            CGPoint currentPoint=[touch locationInView:backgroundView];
            if(!CGRectContainsPoint(table.frame, currentPoint))
            {
                [table removeFromSuperview];
                [table release];
                table=nil;
                [backgroundView removeFromSuperview];
                [backgroundView release];
                backgroundView=nil;
            }
        }
    }
}


- (void)CallBack:(NSString *)str other:(NSString *)str2 
{
    caDetailLabel.text=str2;
    [[NSUserDefaults standardUserDefaults] setObject:str2 forKey:@"caDetailString"];
    topId = [str retain];
    [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"caDetailId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)postData:(id)sender
{
//    NSLog(@"titleStr=%@",titleStr);
//    NSLog(@"whereAmI=%@",whereAmI); 
//    NSLog(@"contentStr=%@",contentStr);
//    NSLog(@"topId=%@",topId);
    
    
    
    
    [titleField resignFirstResponder];
    [contentView resignFirstResponder];
    
    topId=[[NSUserDefaults standardUserDefaults] objectForKey:@"caDetailId"];
    if(titleStr==nil||[titleStr length]==0)
    {
        UIAlertView *titleAlert=[[UIAlertView alloc]initWithTitle:nil message:@"请输入标题" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [titleAlert show];
        [titleAlert release];
        return;
    }
    if(topId==nil||[topId length]==0)
    {
        UIAlertView *titleAlert=[[UIAlertView alloc]initWithTitle:nil message:@"请选择分类" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [titleAlert show];
        [titleAlert release];
        return;
    }
    if (contentStr==nil||[contentStr length]==0)
    {
        UIAlertView *titleAlert=[[UIAlertView alloc]initWithTitle:nil message:@"内容不能为空" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [titleAlert show];
        [titleAlert release];
        return;
    }
    JsonService *jservice=[JsonService sharedManager]; 
    [jservice setDelegate:self];
    //jservice.delegate=self;
    jservice.pushDelegate=self;
    NSMutableDictionary *jsonDir=[[NSMutableDictionary alloc]init];
    [jsonDir setValue:titleStr forKey:@"title"];
    [jsonDir setValue:contentStr forKey:@"content"];
    if(whereAmI==nil)
    {
      [jsonDir setValue:@" " forKey:@"address"];
    }
    else
    {
      [jsonDir setValue:whereAmI forKey:@"address"];
    }
    NSMutableArray *imageStrArray=[[NSMutableArray alloc]initWithCapacity:0];
    for(int i=0;i<[imageArray count];i++)
    {
        UIImage *image=[imageArray objectAtIndex:i];
        NSData *imageData=UIImageJPEGRepresentation(image, 1.0);
        //NSLog(@"%f*%f",image.size.width,image.size.height);
        //NSLog(@"image.length=%f",image.scale);
        for(;;)
        {
            if(imageData.length/3364>40)
            {
                image=[self imageByScalingAndCroppingForSize:CGSizeMake(image.size.width*0.5, image.size.height*0.5) OriginImage:image];
                imageData=UIImageJPEGRepresentation(image, 1.0);
            }
            else
            {
                break;
            }
        }
        
       // UIImage *newImage=[self imageByScalingAndCroppingForSize:CGSizeMake(image.size.width*0.5, image.size.height*0.5) OriginImage:image];
        
       // NSData *imageData=UIImageJPEGRepresentation(newImage, 1.0);
        
      //  NSLog(@"newImage.memory＝%d",imageData.length/3364);
//        if(imageData.length/3364>40)
//        {
//            UIImage *fNewImage=[self imageByScalingAndCroppingForSize:CGSizeMake(image.size.width*0.5, image.size.height*0.5) OriginImage:newImage];
//            imageData = UIImageJPEGRepresentation(fNewImage,1.0);
//        }
      //  UIImageWriteToSavedPhotosAlbum(newImage, nil, nil, nil);
        
      //  NSLog(@"%f*%f",newImage.size.width,newImage.size.height);
     //   NSLog(@"image.length=%f",newImage.scale);

        NSString* imageString = [[NSString alloc]initWithData:[GTMBase64 encodeData:imageData]encoding:NSUTF8StringEncoding];
        if(imageString!=nil&&![imageStrArray containsObject:imageString])
        {
            [imageStrArray addObject:imageString];
        }
    }
    [jsonDir setValue:imageStrArray forKey:@"photo"];
    NSString *jsonStr=[jsonDir JSONRepresentation];
    NSLog(@"%@",jsonStr);
    [jservice pushListData:jsonStr topStr:topId];
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(stopActivity3) name:@"stopActivity3" object:nil];
    
    UIView *mainScreen=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [mainScreen setBackgroundColor:[UIColor clearColor]];
    mainScreen.tag=606;
    [self.view addSubview:mainScreen];
    
    UIView *backView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height/2-40, 100, 80)];
    backView.tag=609;
    backView.backgroundColor=[UIColor grayColor];
    backView.layer.cornerRadius=10.0;
    [self.view addSubview:backView];
    
    
    activity = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];//指定进度轮的大小
    [activity setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];//指定进度轮中心点
    [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];//设置进度轮显示类型
    [self.view addSubview:activity];
    [activity startAnimating];
}

-(void)dataCallBack:(NSString *)str1 other:(NSString *)str2 third:(NSNumber *)str3
{
    netIsok=[str1 retain];
    promptStr=[str2 retain];
    codeStr=[str3 retain];
}

-(void)stopActivity3
{
    if([netIsok isEqualToString:@"NO"])
    {
        for(UIView *view in [self.view subviews])
        {
            if(view.tag==606||view.tag==609)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
        [activity stopAnimating];
        return;
    }
    if([codeStr intValue]==1)
    {
        titleField.text=nil;
        titleStr=nil;
        contentView.text=nil;
        contentStr=nil;
        for(UIImageView* imageView in [self.view subviews])
       {
            if(imageView.frame.origin.y==270)
            {
                [imageView removeFromSuperview];
            }
        }
        [imageArray removeAllObjects];
    }
    UILabel *resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, 320, 100, 50)];
    resultLabel.backgroundColor=[UIColor grayColor];
    if([codeStr intValue]==1)
    {
       resultLabel.text=@"发送成功";
    }
    else
    {
       resultLabel.text=@"发送失败";
    }
    resultLabel.textAlignment=NSTextAlignmentCenter;
    resultLabel.font=[UIFont systemFontOfSize:12.0];
    resultLabel.textColor=[UIColor whiteColor];
    CALayer *labelLayer=[resultLabel layer];
    labelLayer.masksToBounds=YES;
    labelLayer.cornerRadius=5.0;
    [self.view addSubview:resultLabel];
    [self performSelector:@selector(LabelremoveSubview:) withObject:resultLabel afterDelay:1.0];
}

-(void) LabelremoveSubview:(UILabel *)label
{
    [activity stopAnimating];
    [label removeFromSuperview];
    [label release];
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==606||view.tag==609)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
   // [self.navigationController popViewControllerAnimated:YES];
    if([codeStr intValue]>0)
    {
//        static int i=0;
//        if(i>0)
//        {
//            [self.navigationController popViewControllerAnimated:YES];
//        }
//        i++;
        UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
        temporaryBarButtonItem.title = @"返回";
        self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
        BbsViewController *bbsView=[[BbsViewController alloc]initWithNibName:nil bundle:nil];
        [bbsView setRefid:promptStr];
        [self.navigationController pushViewController:bbsView animated:YES];
        [bbsView release];
    }
    else
    {
        return;
    }
}



-(void)tapPressLabel:(id)sender
{
    UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
    temporaryBarButtonItem.title = @"返回";
    self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
    categoryListViewController *list=[[categoryListViewController alloc]initWithNibName:nil bundle:nil];
    self.cateView = list;
    list.delegate = self;
    [self.navigationController pushViewController:list animated:YES];
    [list release];
}


-(void)imageAddbtnTouched:(id)sender
{
    if([imageArray count]>=3)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"最多只能添加三张照片" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    backgroundView=[[UIView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:backgroundView];
    alertTabView=[[UITableView alloc]initWithFrame:CGRectMake(20, 120, self.view.frame.size.width-40, 180)];
    alertTabView.tag=10085;
    alertTabView.dataSource=self;
    alertTabView.delegate=self;
    alertTabView.scrollEnabled=NO;
    alertTabView.layer.borderWidth = 1;
    alertTabView.backgroundView.backgroundColor=[UIColor blackColor];
    alertTabView.layer.borderColor = [[UIColor blackColor] CGColor];
    [self.view addSubview:alertTabView];
    [self.view bringSubviewToFront:alertTabView];
}

//UITableView 委托方法
- (NSInteger)tableView:(UITableView *)tableView1 numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    if(indexPath.row==0)
    {
        cell.textLabel.text=@"选择";
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.backgroundColor=[UIColor clearColor];
        UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
        backgrdView.backgroundColor = [UIColor blackColor];
        cell.backgroundView = backgrdView;
    }
    else if(indexPath.row==1)
    {
        cell.textLabel.text=@"相机拍摄";
    }
    else if(indexPath.row==2)
    {
        cell.textLabel.text=@"手机相册";
    }
    return cell;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

        return 60;
}

- (void) tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPat
{
        if(indexPat.row==1)
        {
            [self cameraPicture];
        }
        else if(indexPat.row==2)
        {
            [self takePicture];
        }
        [alertTabView removeFromSuperview];
        [alertTabView release];
        [backgroundView removeFromSuperview];
        [backgroundView release];
}

-(void)takePicture
{
    QBImagePickerController *imagePickerController = [[QBImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.limitsMaximumNumberOfSelection=YES;
    imagePickerController.maximumNumberOfSelection=3-[imageArray count];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:imagePickerController];
    [self presentViewController:navigationController animated:YES completion:NULL];
    [imagePickerController release];
    [navigationController release];
}

-(void)cameraPicture
{
    //检查相机模式是否可用
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        NSLog(@"sorry, no camera or camera is unavailable!");
        return;
    }
    //获得相机模式下支持的媒体类型
    NSArray* availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
    BOOL canTakePicture = NO;
    for (NSString* mediaType in availableMediaTypes) {
        if ([mediaType isEqualToString:(NSString*) kUTTypeImage]) {
            //支持拍照
            canTakePicture = YES;
            break;
        }
    }
    //检查是否支持拍照
    if (!canTakePicture) {
        NSLog(@"sorry, taking picture is not supported.");
        return;
    }
    //创建图像选取控制器
    UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
    //设置图像选取控制器的来源模式为相机模式
    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    //设置图像选取控制器的类型为静态图像
    imagePickerController.mediaTypes = [[[NSArray alloc] initWithObjects:(NSString*)kUTTypeImage, nil] autorelease];
    //允许用户进行编辑
    imagePickerController.allowsEditing = YES;
    //设置委托对象
    imagePickerController.delegate = self;
    //以模视图控制器的形式显示
    [self presentModalViewController:imagePickerController animated:YES];
    [imagePickerController release];
}

//pragma mark - QBImagePickerControllerDelegate

- (void)imagePickerController:(id)imagePickerController didFinishPickingMediaWithInfo:(id)info
{
    if ([imageArray count]>=3)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"提示" message:@"最多添加三张照片" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    if([imagePickerController isKindOfClass:[QBImagePickerController class]])
    {
        QBImagePickerController* imagePickerControllers=(QBImagePickerController*)imagePickerController;
        if(imagePickerControllers.allowsMultipleSelection)
        {
            NSArray *mediaInfoArray = (NSArray *)info;
            NSLog(@"Selected %d photos", mediaInfoArray.count);
            //打印出字典中的内容
            NSLog(@"get the media info: %@", info);
            for(int i=0;i<[mediaInfoArray count];i++)
            {
                NSDictionary *imageDic=[mediaInfoArray objectAtIndex:i];
                UIImage *originImage = [imageDic valueForKey:UIImagePickerControllerOriginalImage];
                if(originImage!=nil&&![imageArray containsObject:originImage])
                {

                    [imageArray addObject:originImage];
                }
            }
        }
    }
    else if([imagePickerController isKindOfClass:[UIImagePickerController class]])
    {
        NSDictionary *mediaInfo = (NSDictionary *)info;
        NSLog(@"Selected: %@", mediaInfo);
        UIImage *originImage = [mediaInfo valueForKey:UIImagePickerControllerOriginalImage];
        if(originImage!=nil&&![imageArray containsObject:originImage])
        {
            [imageArray addObject:originImage];
        }
    }
    [self displayImage:imageArray];
    [self dismissViewControllerAnimated:YES completion:NULL];
}


- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize OriginImage:(UIImage *)images
{
    UIImage *sourceImage = images;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Cancelled");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (NSString *)descriptionForSelectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"选择所有照片";
}

- (NSString *)descriptionForDeselectingAllAssets:(QBImagePickerController *)imagePickerController
{
    return @"所有照片的选择解除";
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos
{
    return [NSString stringWithFormat:@"照片%d张", numberOfPhotos];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"录像%d", numberOfVideos];
}

- (NSString *)imagePickerController:(QBImagePickerController *)imagePickerController descriptionForNumberOfPhotos:(NSUInteger)numberOfPhotos numberOfVideos:(NSUInteger)numberOfVideos
{
    return [NSString stringWithFormat:@"照片%d张、录像%d", numberOfPhotos, numberOfVideos];
}
//

-(void)displayImage:(NSArray*)imageArr
{
    for(int i=0;i<[imageArr count];i++)
    {
        UIImageView *imageView=[[UIImageView alloc]init];
        CGRect viewFrame=CGRectMake(10+102*i,270, 95, 90);
        imageView.frame=viewFrame;
        imageView.userInteractionEnabled=YES;
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressImageView:)];
        imageView.tag=i;
        [imageView addGestureRecognizer:longPress];
        
        UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapPressImageView:)];
        [imageView addGestureRecognizer:tapPress];
        
        [imageView setImage:[imageArr objectAtIndex:i]];
        [self.view addSubview:imageView];
        [imageView release];
        [longPress release];
    }
}

-(void)longPressImageView:(UITapGestureRecognizer*)paramSender
{
    if(paramSender.state==UIGestureRecognizerStateEnded)
    {
        NSLog(@"long press!");
        CGPoint touchPoint = [paramSender locationInView:self.view];
        NSLog(@"%f,%f",touchPoint.x,touchPoint.y);
        imageIndex=(touchPoint.x-10)/102;
        NSLog(@"%d",imageIndex);
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"Confirm Delete",@"Confirm Delete") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",@"Cancel") otherButtonTitles:NSLocalizedString(@"OK",@"OK"), nil];
        alert.delegate=self;
        [alert show];
        [alert release];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        for(UIImageView* imageView in [self.view subviews])
        {
            if(imageView.frame.origin.y==270)
            {
                [imageView removeFromSuperview];
            }
        }
        [imageArray removeObjectAtIndex:imageIndex];
        //[imageUrlArray removeObjectAtIndex:imageIndex];
        NSLog(@"%d",[imageArray count]);
        [self displayImage:imageArray];
        // [imageAddbtn addSubview:imageScrollview];
    }
}

-(void)tapPressImageView:(UILongPressGestureRecognizer*)paramSender
{
    NSLog(@"long press!");
    CGPoint touchPoint = [paramSender locationInView:self.view];
    NSLog(@"%f,%f",touchPoint.x,touchPoint.y);
    imageIndex=(touchPoint.x-10)/102;
    merImageViewController *bigImageView=[[merImageViewController alloc]init];
    bigImageView.bigImage=[imageArray objectAtIndex:imageIndex];
    bigImageView.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:bigImageView animated:YES];
}


//百度地图相关委托方法
#pragma mark -  实现 BMKMapViewDelegate 中的方法
/**
 在地图View将要启动定位时，会调用此函数
 @param mapView 地图View
 下面的这个方法，貌似并没有被启动啊？是否是可有可无的？
 */
- (void)mapViewWillStartLocatingUser:(BMKMapView *)mapView

{
	NSLog(@"start locate");
}

/**
 用户位置更新后，会调用此函数
 @param mapView 地图View
 @param userLocation 新的用户位置
 在实际使用中，只需要    [mapView setShowsUserLocation:YES];    mapView.delegate = self;   两句代码就可以启动下面的方法。疑问，为什么我的位置没有移动的情况下，这个方法循环被调用呢？
 */
- (void)mapView:(BMKMapView *)mapView didUpdateUserLocation:(BMKUserLocation *)userLocation
{
	if (userLocation != nil)
    {
		NSLog(@"%f %f", userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude);
        [littleMap setShowsUserLocation:YES];
        [[NSUserDefaults standardUserDefaults] setFloat:userLocation.location.coordinate.latitude forKey:@"startPt_latitude"];
        [[NSUserDefaults standardUserDefaults] setFloat:userLocation.location.coordinate.longitude forKey:@"startPt_longitude"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // CLLocationCoordinate2D nowlocation;
        startPt.latitude=userLocation.location.coordinate.latitude;
        startPt.longitude=userLocation.location.coordinate.longitude;
        [self setMapRegionWithCoordinate:startPt];
        //[littleMap setShowsUserLocation:NO];
	}
}

//传入经纬度,将baiduMapView 锁定到以当前经纬度为中心点的显示区域和合适的显示范围
- (void)setMapRegionWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    // coordinate.latitude=22.570662;
    //  coordinate.longitude=11.179366;
    BMKCoordinateRegion region;
    if (!isRetina1)//这里用一个变量判断一下,只在第一次锁定显示区域时 设置一下显示范围 Map Region
    {
        region = BMKCoordinateRegionMake(coordinate, BMKCoordinateSpanMake(0.002, 0.002));//越小地图显示越详细
        isRetina1 = YES;
        [littleMap setRegion:region animated:YES];//执行设定显示范围
        [self showWithlocation];
    }
    startPt= coordinate;
    [littleMap setCenterCoordinate:coordinate animated:YES];//根据提供的经纬度为中心原点 以动画的形式移动到该区域
}

//以后开始移动,当移动完成后,会执行以下委托
- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    [littleMap.annotations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        BMKPointAnnotation *item = (BMKPointAnnotation *)obj;
        if (item.coordinate.latitude == startPt.latitude && item.coordinate.longitude == startPt.longitude )
        {
            [littleMap selectAnnotation:obj animated:YES];//执行之后,会让地图中的标注处于弹出气泡框状态
            *stop = YES;
        }
    }];
}


/**
 定位失败后，会调用此函数
 @param mapView 地图View
 @param error  错误号，参考CLError.h中定义的错误号
 */
- (void)mapView:(BMKMapView *)mapView didFailToLocateUserWithError:(NSError *)error

{
	if (error != nil)
		NSLog(@"locate failed: %@", [error localizedDescription]);
	else {
		NSLog(@"locate failed");
	}
}

//返回地址信息
- (void)onGetAddrResult:(BMKAddrInfo*)result errorCode:(int)error
{
    NSArray* array = [NSArray arrayWithArray:littleMap.annotations];
	[littleMap removeAnnotations:array];
	array = [NSArray arrayWithArray:littleMap.overlays];
	[littleMap removeOverlays:array];
	if (error == 0) {
		BMKPointAnnotation* item = [[BMKPointAnnotation alloc]init];
		item.coordinate = result.geoPt;
		item.title = result.strAddr;
        whereAmI=[NSString stringWithFormat:@"%@",result.strAddr];
        if(whereAmI==nil)
        {
            [detailbutton setTitle:@"获取地理位置信息失败" forState:UIControlStateNormal];
            //[detailbutton setImage:activity4 forState:UIControlStateNormal];
        }
        else
        {
            //detailbutton.frame=CGRectMake(10, self.view.frame.size.height-80, 200, 30);
            [[NSUserDefaults standardUserDefaults] setObject:whereAmI forKey:@"whereAmI"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [detailbutton setTitle:whereAmI forState:UIControlStateNormal];
            // NSLog(@"whereAmI:%@",whereAmI);
            [detailbutton setImage:[UIImage imageNamed:@"icon_lbs_act.png"] forState:UIControlStateNormal];
            [detailbutton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-10,0.0,0.0)];
        }
        [activity stopAnimating];
        [activity removeFromSuperview];
        [activity release];
		[littleMap addAnnotation:item];
		[item release];
	}
}

//将位置信息转化为地理信息
- (void)showWithlocation
{
    whereAmI=nil;
    _search = [[BMKSearch alloc]init];
    _search.delegate=self;
    NSLog(@"starPt=%f,%f",startPt.latitude,startPt.longitude);
    BOOL flag = [_search reverseGeocode:startPt];
    if (!flag) {
		NSLog(@"search failed!");
        [detailbutton setTitle:@"获取地理位置信息失败" forState:UIControlStateNormal];
        [detailbutton setImage:nil forState:UIControlStateNormal];
    }
}


//***
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
