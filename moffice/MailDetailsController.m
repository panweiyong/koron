//
//  MailDetailsController.m
//  moffice
//
//  Created by yangxi zou on 12-1-17.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "MailDetailsController.h"
#import "BaseNavigationController.h"
#import "AttachListViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface MailHeaderCell : UITableViewCell
@property (nonatomic,assign) BOOL isStrink;
@property (nonatomic,assign) BOOL isSubject;
@end

@implementation MailHeaderCell

- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CGRectMake(10,6,50,self.textLabel.frame.size.height);
    [self.textLabel setFont: [UIFont boldSystemFontOfSize:14]];
    //self.detailTextLabel.frame.size.width
    if (self.isStrink) {
        self.detailTextLabel.frame = CGRectMake(62,6,170,self.detailTextLabel.frame.size.height);
        [self.detailTextLabel setFont: [UIFont systemFontOfSize:14]];
    }else {
        if (self.isSubject) {
            self.detailTextLabel.frame = CGRectMake(10,6,300,self.detailTextLabel.frame.size.height);
            [self.detailTextLabel setFont: [UIFont boldSystemFontOfSize:18]];
        }else {
            self.detailTextLabel.frame = CGRectMake(62,6,219,self.detailTextLabel.frame.size.height);
            [self.detailTextLabel setFont: [UIFont systemFontOfSize:14]];
        }
        
        [self.detailTextLabel sizeToFit];
    }
    
    
    
   }
@end

//@interface UIView(IWantToGetRejected) ; + (void) setAnimationPosition:(CGPoint)p ; @end

@implementation MailDetailsController

@synthesize mid = _mid,allmids = _allmids,currentindex = _currentindex,accounts = _accounts,isintranet,tipsid,showTurePage;
@synthesize flag = _flag;
@synthesize isSystemMail;
@synthesize isReply;
 
-(void)dealloc
{
    [[JsonService sharedManager] cancelAllRequest];
    [tipsid release];
    [_accounts release];
    [_mid release];
    [_allmids release];
//    [_attachBackfroundView release],_attachBackfroundView = nil;
    [_attachView release],_attachView = nil;
    
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _isCollect = [[[NSUserDefaults standardUserDefaults] objectForKey:@"ISCOLLECT"] boolValue];
    }
    return self;
}
-(void)handleTapFrom:(UITapGestureRecognizer *)recognizer{ 
    UIImageView *iv =(UIImageView *) [recognizer view];
    [self setHidesBottomBarWhenPushed:YES ];
    ImageViewController *imgc=[[ImageViewController alloc] initWithNibName:nil bundle:nil];
    [imgc setDisplayImage: [iv image] ];
    [self.navigationController pushViewController:imgc animated:YES];
    [imgc release];
}

#pragma mark - View lifecycle



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return @"";
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableData==nil) {
        return  0;
    }
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableData==nil) {
        return  0;
    }
    if (section==0) {
        if (_isCollect) {
            return 2;
        }else if ([[tableData objectForKey:@"cc"] isEqualToString:@""]) {
            return 4;
        }
        return  5;
    }else if(section==1){
        return  [[  tableData objectForKey:@"attrs"]   count];
    }else if(section==2){
        return  1;
    }
	return 0;
} 
  
- (UITableViewCell *)tableView:(UITableView *)tableViewz cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (indexPath.section==0) {
        UITableViewCellStyle style = UITableViewCellStyleValue2; 
        NSString *cellid= @"tid0" ;
        MailHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        if (cell == nil)
        {
            cell = [[[MailHeaderCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
            UIButton *backgroundButton = [UIButton buttonWithType:UIButtonTypeCustom];
            backgroundButton.frame = CGRectMake(0, 3, kDeviceWidth, 20);
            backgroundButton.tag = 110;
            [backgroundButton addTarget:self action:@selector(onDetailButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:backgroundButton];

            UIButton *detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
            detailButton.tag = 111;
            detailButton.frame = CGRectMake(kDeviceWidth-50, 0, 40, 20);
            detailButton.titleLabel.font = [UIFont systemFontOfSize:12];
            if (_isCollect) {
                [detailButton setTitle:@"详情" forState:UIControlStateNormal];
                [detailButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                [detailButton setTitle:@"详情" forState:UIControlStateHighlighted];
                [detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
            }else {
                [detailButton setTitle:@"隐藏" forState:UIControlStateNormal];
                [detailButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                [detailButton setTitle:@"隐藏" forState:UIControlStateHighlighted];
                [detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
            }
            detailButton.alpha = 0.7;
            [detailButton addTarget:self action:@selector(onDetailButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [backgroundButton addSubview:detailButton];
            
            UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(detailButton.frame.origin.x-40-5, detailButton.frame.origin.y-1, 50, detailButton.bounds.size.height)];
            dateLabel.backgroundColor = [UIColor clearColor];
            dateLabel.tag = 112;
            dateLabel.text = [self loadFomatterDate:[tableData objectForKey:@"receivedTime"]];
            dateLabel.font = [UIFont systemFontOfSize:12];
            dateLabel.textAlignment = NSTextAlignmentCenter;
            dateLabel.textColor = [UIColor grayColor];
            [backgroundButton addSubview:dateLabel];
            

            
        }
            if(indexPath.row==0) {
            cell.isSubject = YES;
            cell.textLabel.text=  @"";
            cell.detailTextLabel.text =  [tableData objectForKey:@"id"];
            UIButton *backgroundButton = (UIButton *)[cell.contentView viewWithTag:110];
            backgroundButton.hidden = YES;

//                cell.detailTextLabel.text = @"1";
        }else if (indexPath.row==1) {
            cell.textLabel.text= NSLocalizedString(@"from:", @"from:") ; 
            cell.detailTextLabel.text =  [tableData objectForKey:@"from"];
//            cell.detailTextLabel.text = @"我们都十大搜；放假撒的恢复快来打时空裂缝撒酒疯客户为ufhssldfh"; //test
            cell.isStrink =_isCollect;
            
            UIButton *backgroundButton = (UIButton *)[cell.contentView viewWithTag:110];
            backgroundButton.hidden = NO;
            UIButton *detailButton = (UIButton *)[backgroundButton viewWithTag:111];
            UILabel *dateLabel = (UILabel *)[backgroundButton viewWithTag:112];
            if (_isCollect) {
                [detailButton setTitle:@"详情" forState:UIControlStateNormal];
                [detailButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                [detailButton setTitle:@"详情" forState:UIControlStateHighlighted];
                [detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
                
                dateLabel.hidden = NO;
            }else {
                [detailButton setTitle:@"隐藏" forState:UIControlStateNormal];
                [detailButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
                [detailButton setTitle:@"隐藏" forState:UIControlStateHighlighted];
                [detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
                
                dateLabel.hidden = YES;
            }
            
            
        }else if(indexPath.row==2) {
            UIButton *backgroundButton = (UIButton *)[cell.contentView viewWithTag:110];
            backgroundButton.hidden = YES;
            cell.textLabel.text=  NSLocalizedString(@"Time:", @"Time:");
            cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ %@",[tableData objectForKey:@"receivedTime"],[self getWeekdayFomatter:[tableData objectForKey:@"receivedTime"]]];
        }else if(indexPath.row==3) {
            cell.textLabel.text=  NSLocalizedString(@"to:", @"to:") ;
            cell.detailTextLabel.text =  [tableData objectForKey:@"to"];//[[[[tableData objectAtIndex:indexPath.row] objectForKey:@"createTime"]
            UIButton *backgroundButton = (UIButton *)[cell.contentView viewWithTag:110];
            backgroundButton.hidden = YES;
        }else if(indexPath.row==4) {
            cell.textLabel.text=  @"抄  送:";
            cell.detailTextLabel.text =  [tableData objectForKey:@"cc"];
            UIButton *backgroundButton = (UIButton *)[cell.contentView viewWithTag:110];
            backgroundButton.hidden = YES;
        }
        if (cell.isStrink && indexPath.row == 1) {
            cell.detailTextLabel.numberOfLines = 1;
        }else {
            cell.detailTextLabel.numberOfLines=0;
        }
//        cell.detailTextLabel.lineBreakMode=UILineBreakModeCharacterWrap;
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
        cell.textLabel.textColor = [UIColor grayColor];
//        cell.detailTextLabel.textColor = [UIColor grayColor];
        return cell;
    }

    if (indexPath.section==1) {
	UITableViewCellStyle style = UITableViewCellStyleValue1; 
	NSString *cellid= @"tid1";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
    } else{
        [[cell.contentView viewWithTag:100] removeFromSuperview];
    }
        
        NSDictionary *dd=[[ tableData objectForKey:@"attrs"]  objectAtIndex:indexPath.row];
        NSString *fkey= [dd objectForKey:@"key"];
        DisplayControl *dobj= [JsonUtils ParseControl:fkey data:[dd objectForKey:@"value"] delegate:self textAlignment:UITextAlignmentLeft];
        dobj.tag=100;
        cell.textLabel.text=  dobj.title;
        cell.textLabel.font = [UIFont fontWithName:@"Arial" size:15];
        cell.backgroundColor=[UIColor whiteColor];
        [cell.contentView addSubview:dobj];
        //cell.detailTextLabel.text =  [[[[tableData objectAtIndex:indexPath.row] objectForKey:@"createTime"]  dateFromISO8601]stringWithFormat:@"HH:mm"];
        //cell.textLabel.backgroundColor  = [UIColor clearColor];
        //cell.detailTextLabel.backgroundColor  = [UIColor clearColor];
        //cell.imageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"tips_mail.png" ]  ];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    return cell;
    }
    if (indexPath.section==2) {
        UITableViewCellStyle style = UITableViewCellStyleValue1; 
        NSString *cellid= @"tid2";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        if (!cell) 
        {
            cell = [[[UITableViewCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
        }else{
            [[cell.contentView viewWithTag:100] removeFromSuperview];
        }
        
        UIFont *font = [UIFont fontWithName:@"Arial" size:19.0f];
        CGSize size = CGSizeMake(280,2000);  
        CGSize labelsize = [[tableData objectForKey:@"content"] sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping];
        if (labelsize.height<250) {
            labelsize.height=250;
        }
        _webview=[[UIWebView alloc]initWithFrame:CGRectMake(10,5, 295, labelsize.height)];
        _webview.tag=100;
        _webview.scrollView.bounces = YES;
//        _webview.scrollView.userInteractionEnabled = NO;
        _webview.scrollView.showsHorizontalScrollIndicator = NO;
        _webview.delegate = nil;
//        webview.scalesPageToFit = YES;
//        webview.scrollView.scrollEnabled = NO;
        _webview.scrollView.delegate = self;
        _webview.backgroundColor = [UIColor whiteColor];
        [_webview loadHTMLString:[tableData objectForKey:@"content"] baseURL:nil];
        [cell.contentView addSubview:_webview];
        
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
        return cell;
    }
    return  nil;
}

//- (void)webViewDidFinishLoad:(UIWebView *)theWebView
//{
//    CGSize contentSize = theWebView.scrollView.contentSize;
//    CGSize viewSize = self.view.bounds.size;
//    
//    float rw = viewSize.width / contentSize.width;
//    
//    theWebView.scrollView.minimumZoomScale = rw;
//    theWebView.scrollView.maximumZoomScale = rw;
//    theWebView.scrollView.zoomScale = rw;
//}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
  
}


- (CGFloat)tableView:(UITableView *)tableViewz heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ 	
    
    UITableViewCell *cell = (UITableViewCell *)[self tableView:tableViewz cellForRowAtIndexPath:indexPath];
    
    if(indexPath.section==0)
    { 
        if (_isCollect && indexPath.row == 1) {
            return 30;
        }
        NSString *cellText = cell.detailTextLabel.text;
        UIFont *cellFont = nil;
        if (indexPath.row == 0) {
            cellFont = [UIFont systemFontOfSize:18];
        }else {
            cellFont = [UIFont systemFontOfSize:14];
        }
        
        CGSize constraintSize = CGSizeMake(200.0f, 1000);
        
//        CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeTailTruncation];

        CGSize labelSize = [cellText sizeWithFont:cellFont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
       
//        if (indexPath.row == 0) {
//            return labelSize.height + 4;
//        }
        return labelSize.height + 8;
    }
    
    if(indexPath.section==1){
        if(cell!=nil&&[[cell.contentView subviews ] count]>1){
            UIView *c=(UIView *)[[cell.contentView subviews ] objectAtIndex:1];
            //CCLOG(@"count:%i",[[cell.contentView subviews ] count]);
            if(c)return c.frame.size.height+6.0f;
        }else{
            return 36;
        }
        return 36;
    } 
    
   
    
    if(indexPath.section==2){ 
        UITableViewCell *cell = (UITableViewCell *)[self tableView:tableViewz cellForRowAtIndexPath:indexPath];
        if(cell!=nil&&[[cell.contentView subviews ] count]>0){
            UIView *c=(UIView *)[[cell.contentView subviews ] objectAtIndex:0];
            //CCLOG(@"count:%i",[[cell.contentView subviews ] count]);
            if(c)return c.frame.size.height+30.0f;
        }else{
            return 36;
        }
        return 36;
    }
    return 36;
}

- (void)webViewDidFinishLoad:(UIWebView *) webView
{
    CGSize actualSize = [webView sizeThatFits:CGSizeZero];
    
    CGRect newFrame = webView.frame;
    newFrame.size.height = actualSize.height;
    webView.frame = newFrame;
    //webviewHeight=actualSize.height;
    //[self tableView:tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    //[tableView reloadData]; 
    /*
    UITableViewCell *cell = (UITableViewCell *)[self tableView:tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    if(cell!=nil&&[[cell.contentView subviews ] count]>1){
        CGRect fra=cell.frame; 
        fra.size.height = actualSize.height+10;//
        [cell setFrame:fra];
    }*/
}


#pragma getDate Method
- (NSString *)loadFomatterDate:(NSString *)dateString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger result = [self computationTime:[formatter stringFromDate:date]];
    if (result == 0) {
        [formatter setDateFormat:@"今天"];
    }else if (result == 1) {
        [formatter setDateFormat:@"昨天"];
    }
    else if (result == 2) {
        [formatter setDateFormat:@"前天"];
    }
    else {
        [formatter setDateFormat:@"MM-dd"];
    }
    
    
    return [formatter stringFromDate:date];
    
}

- (NSInteger)computationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowadayTimeStr = [fromatter stringFromDate:[NSDate date]];
    
    
    if ([[nowadayTimeStr substringToIndex:3] integerValue] - [[timeStr substringToIndex:3] integerValue] == 0) {
        if ([[nowadayTimeStr substringWithRange:NSMakeRange(5, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(5, 2)] integerValue] == 0) {
            if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 0) {
                return 0;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
                return 1;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 2) {
                return 2;
            }else {
                return 4;
            }
        }else {
            return 4;
        }
        
    }else {
        return 4;
    }
    
}

- (NSString *)getWeekdayFomatter:(NSString *)receivedTime{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:receivedTime];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    comps = [calendar components:(NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSWeekdayOrdinalCalendarUnit)
                        fromDate:date];
    NSInteger weekday = [comps weekday];
    NSString *weekdayString = nil;
    switch (weekday) {
        case 1:
            weekdayString = @"星期一";
            break;
        case 2:
            weekdayString = @"星期二";
            break;
        case 3:
            weekdayString = @"星期三";
            break;
        case 4:
            weekdayString = @"星期四";
            break;
        case 5:
            weekdayString = @"星期五";
            break;
        case 6:
            weekdayString = @"星期六";
            break;
        case 7:
            weekdayString = @"星期日";
            break;
        default:
            break;
    }
    
    return weekdayString;
}

#pragma mark - Target Action 
- (void)onDetailButtonPressed:(id)sender {
    UIButton *button = (UIButton *)sender;
    
    if (_isCollect) {
        _isCollect = NO;
    }else {
        _isCollect = YES;
    }
    
//    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    [[NSUserDefaults standardUserDefaults] setBool:_isCollect forKey:@"ISCOLLECT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"ISCOLLECT"]);
    
    button.hidden = YES;
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)onAttachButton:(UIButton *)button {
    
    CATransition *myAnimation = [CATransition animation];
    myAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    myAnimation.duration = 0.4;
    myAnimation.type = kCATransitionMoveIn;
    myAnimation.subtype = kCATransitionFromRight;
    
    [self.navigationController.view.layer addAnimation:myAnimation forKey:nil];

    
    AttachListViewController *attachListViewCtl = [[AttachListViewController alloc] init];
    attachListViewCtl.attachArray = [tableData objectForKey:@"listAttach"];
    attachListViewCtl.pathId = self.pathId;
    [self.navigationController pushViewController:attachListViewCtl animated:NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BaseNavigationControllerBackgroundColorChangeWhite" object:nil];
    
    
    
    [attachListViewCtl release];
}


-(void)viewWillAppear:(BOOL)animated
{
//    [self .navigationController setToolbarHidden: NO];
//    [self.navigationController setToolbarItems:buttons];
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice getMail:_mid];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStartAnimation" object:nil];
    
    [tableView reloadData];
    
    
}
- (void)viewWillDisappear:(BOOL)animated {
    if (isSystemMail) {
        [self.navigationController setToolbarHidden: YES];
    }
    [[JsonService sharedManager] cancelAllRequest];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
     [super viewDidDisappear:animated];
    
    
} 

- (void)viewDidLoad
{
    [super viewDidLoad];
    [tableData[0] objectForKey:@"id"];
    NSLog(@"%@",[tableData[0] objectForKey:@"id"]);
    
    self.view.backgroundColor = [UIColor whiteColor]; // [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:  [SMFileUtils fullBundlePath:@"tipsbg.png" ] ]];
    tableView = [[UITableView alloc]  initWithFrame:CGRectMake(0,0, self.view.bounds.size.width ,kDeviceHeight-20-40-49 ) style: UITableViewStylePlain];
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
//    CGRectMake(0,0, self.view.bounds.size.width ,self.view.bounds.size.height  - self.tabBarController.tabBar.frame.size.height- self.navigationController.navigationBar.frame.size.height+30  ) 
    [tableView setDelegate:self];
    [tableView setDataSource:self]; 
    // This should be set to work with the image height 
    [tableView setRowHeight:45];
    tableView.showsHorizontalScrollIndicator= NO;
    tableView.showsVerticalScrollIndicator=YES;
    tableView.scrollEnabled=YES;
    tableView.indicatorStyle =UIScrollViewIndicatorStyleDefault ;
    //[tableView setBackgroundColor:MAILCELL_BACKGROUND]; 
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine]; 
    [tableView setIndicatorStyle:UIScrollViewIndicatorStyleDefault];
    //[tableView.layer setShadowOpacity:0.5];
    //[tableView.layer setShadowOffset:CGSizeMake(1, 1)];
    [self.view addSubview:tableView];
    
    UIActivityIndicatorView *processAlert=[SMView showProcessing];
    processAlert.center = tableView.center;
    processAlert.tag=10001;
    [self.view addSubview:processAlert];
	
	//设置按钮背景
    UIView *selectedButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    selectedButtonBackgroundView.userInteractionEnabled = YES;
    selectedButtonBackgroundView.clipsToBounds = YES;
    
    //垃圾桶
    UIButton *trashButton = [[UIButton alloc] initWithFrame:CGRectMake(0, -5, 72, 50)];
    [trashButton setImage:[UIImage imageNamed:@"icon_delete_nor.png"] forState:UIControlStateNormal];
    [trashButton setImage:[UIImage imageNamed:@"icon_delete_sel.png"] forState:UIControlStateHighlighted];
    [trashButton addTarget:self action:@selector(doDelete:) forControlEvents:UIControlEventTouchUpInside];
    [selectedButtonBackgroundView addSubview:trashButton];
    [trashButton release];
    
    //回复
    UIButton *replyButton = [[UIButton alloc] initWithFrame:CGRectMake(75, -5, 72, 50)];
    [replyButton setImage:[UIImage imageNamed:@"icon_reply_nor.png"] forState:UIControlStateNormal];
    [replyButton setImage:[UIImage imageNamed:@"icon_reply_sel.png"] forState:UIControlStateHighlighted];
    [replyButton addTarget:self action:@selector(doReply) forControlEvents:UIControlEventTouchUpInside];
    [selectedButtonBackgroundView addSubview:replyButton];
    [replyButton release];
    
    //转发
    UIButton *forwardButton = [[UIButton alloc] initWithFrame:CGRectMake(140, -5, 72, 50)];
    [forwardButton setImage:[UIImage imageNamed:@"icon_forward_nor.png"] forState:UIControlStateNormal];
    [forwardButton setImage:[UIImage imageNamed:@"icon_forward_sel.png"] forState:UIControlStateHighlighted];
    [forwardButton addTarget:self action:@selector(doForward) forControlEvents:UIControlEventTouchUpInside];
    [selectedButtonBackgroundView addSubview:forwardButton];

    [forwardButton release];
    
	UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:selectedButtonBackgroundView];//allocate rightBarButton
	//[self.navigationController.navigationBar  addSubview:listControl];
	if(!showTurePage)self.navigationItem.rightBarButtonItem = rightBarButton;
	[rightBarButton release];
    
    
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
    

}

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)gotomail
{
//    self.title = [NSString stringWithFormat:@"%i/%i",_currentindex+1,[_allmids count]];
    _mid=[[_allmids objectAtIndex:_currentindex] objectForKey:@"id"];
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice getMail:_mid];
}



//异步接收到返回的数据
- (void)requestDataFinished:(id)jsondata;//request
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
    NSLog(@"code   ==   %@",[jsondata objectForKey:@"code"]);
    if ([[jsondata objectForKey:@"code"] integerValue] == -1) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"提示" message:@"邮件不存在" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertView show];
        [alertView release];
    }
    tableData=[jsondata retain];
    [super requestDataFinished:jsondata];
    
    NSLog(@"_flag is %d",_flag);
    //更新标记未已读
    if (_flag == 0) {
        JsonService *jservice=[JsonService sharedManager];
        [jservice setDelegate:self];
        [jservice UpdateFlag:@"mail" oid:_mid flag:1  tipsid:self.tipsid];
    }
    if ([[tableData objectForKey:@"listAttach"] count] > 0) {
        [self loadAttachButton];
    }
    
}

- (void)loadAttachButton {
    if ([_attachView superview] == NO) {
        
        _attachView = [[UIView alloc] initWithFrame:CGRectMake(0, kDeviceHeight-94-49-40, 40, 30)];
        _attachView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"newconversation_bg.png"]];
        [self.view addSubview:_attachView];
        _attachView.layer.cornerRadius = 5.0f;
        
//        _attachView.userInteractionEnabled = YES;
//        
//        UIImageView *arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(280, 4.5, 21, 21)];
//        arrowView.image = [UIImage imageNamed:@"open_arrow.png"];
//        [_attachView addSubview:arrowView];
        
        _attachButton = [[UIButton alloc] initWithFrame:_attachView.bounds];
        
//        [_attachButton setTitle:[NSString stringWithFormat:@"附件数量:%d",[[tableData objectForKey:@"listAttach"] count]] forState:UIControlStateNormal];
        [_attachButton setTitle:@"附件" forState:UIControlStateNormal];
        [_attachButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
        [_attachButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_attachButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        [_attachButton addTarget:self action:@selector(onAttachButton:) forControlEvents:UIControlEventTouchUpInside];
        [_attachView addSubview:_attachButton];
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//删除
-(void)doDelete:(id)sender
{
    if([ModalAlert confirm:NSLocalizedString(@"Confirm Delete",@"Confirm Delete")])
    {
    if([_allmids count]>0)
    {
        JsonService *jservice=[JsonService sharedManager];
        [jservice setDelegate:self];
         [jservice UpdateFlag:@"mail" oid:[[_allmids objectAtIndex:_currentindex] objectForKey:@"id"] flag:2  tipsid:nil];
        
         [_allmids removeObjectAtIndex:_currentindex];
    }
    
    if (_currentindex==[_allmids count]) {
        _currentindex=_currentindex-1;
    }
    if (_currentindex<0) {
        _currentindex=0;
    }
    
    if([_allmids count]>0)
    {
        [self gotomail];
    }else{
        [self .navigationController popViewControllerAnimated:YES];
        return;
    }
    //suckEffect pageCurl
        [UIView beginAnimations:@"pageCurl" context:NULL];
        [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.parentViewController.view cache:YES];
        [UIView setAnimationDuration:1.5];
        //[UIView setAnimationPosition:CGPointMake(20, 460)];// 你要的位置！
        [UIView commitAnimations];
    }
}
//回复
-(void)doReply
{
    NewMailController *controller=[[NewMailController alloc] initWithNibName:nil bundle:nil];
    controller.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [controller.view setBackgroundColor:[UIColor whiteColor]];
    [controller setTo :[tableData objectForKey:@"from"]];
    controller.subject= [NSString stringWithFormat:@"Re:%@",[tableData objectForKey:@"subject"]];
    controller.from =  [tableData objectForKey:@"to"];
    controller.content= [tableData objectForKey:@"content"];
    controller.title = @"回复";
    [controller setAccounts:self.accounts];
    [controller setIsintranet:self.isintranet];
    BaseNavigationController * nav3=[[BaseNavigationController alloc] initWithRootViewController:controller];
    [self  presentModalViewController:nav3 animated:YES];
   
    [controller release];
    [nav3 release];
}
//转发
-(void)doForward
{
    NewMailController *controller=[[NewMailController alloc] initWithNibName:nil bundle:nil];
    controller.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [controller.view setBackgroundColor:[UIColor whiteColor]];
    //[controller setTo :[tableData objectForKey:@"from"]];
    controller.subject= [NSString stringWithFormat:@"Fwd:%@",[tableData objectForKey:@"subject"]];
    controller.from =  [tableData objectForKey:@"to"];
    //controller.to =  [tableData objectForKey:@"to"];
    controller.content= [tableData objectForKey:@"content"];
    controller.title = @"转发";
    [controller setAccounts:self.accounts];
    
    [controller setIsintranet:self.isintranet];
    BaseNavigationController * nav3=[[BaseNavigationController alloc] initWithRootViewController:controller];
    [self  presentModalViewController:nav3 animated:YES];
    
    [controller release];
    [nav3 release];

}
//转发到
-(void)doForwardTo
{
    NewMailController *controller=[[NewMailController alloc] initWithNibName:nil bundle:nil];
    controller.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [controller.view setBackgroundColor:[UIColor whiteColor]];
    //[controller setTo :[tableData objectForKey:@"from"]];
    controller.subject= [NSString stringWithFormat:@"FwdTo:%@",[tableData objectForKey:@"subject"]];
    controller.from =  [tableData objectForKey:@"to"];
    //controller.to =  [tableData objectForKey:@"to"];
    controller.content= [tableData objectForKey:@"content"];
    
    [controller setAccounts:self.accounts];
    
    [controller setIsintranet:!self.isintranet];
    BaseNavigationController * nav3=[[BaseNavigationController alloc] initWithRootViewController:controller];
    [self  presentModalViewController:nav3 animated:YES];
    
    
    [controller release];
    [nav3 release];
    
}

-(void)doselectC
{
    NSString *fstr;
    if (self.isintranet) {
        fstr=@"转发至外部";
    }else{
        fstr=@"转发至内部";
    }
    UIActionSheet *actionSheet = [[UIActionSheet alloc] 
                                  initWithTitle:nil
                                  delegate:self 
                                  cancelButtonTitle: NSLocalizedString(@"Cancel", @"Cancel") 
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"回复", @"转发", fstr, nil];
    
    [actionSheet showFromToolbar: self.navigationController.toolbar];
    //[actionSheet showInView:self.view];
    [actionSheet release];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //NSLog(@"%i", buttonIndex);
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    switch (buttonIndex) {
        case 0: {//回复
            //NSLog(@"Item 1 Selected");
            if (isReply) {
                [self doReply];
            }else {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"不能回复自己的邮件" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                [alert show];
                [alert release];
                return;
            }
            break;
        }
        case 1: {//转发
            //NSLog(@"Item 2 Selected");
            [self doForward];
            break;
        }
        case 2: {//转发到内部或外部
            //NSLog(@"Item 3 Selected");
            [self doForwardTo];
            break;
        }
    }
}

- (void)replyMail {
    
    if (!isReply) {
        [self doReply];
    }else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"不能回复自己的邮件" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alert show];
        [alert release];
        return;
    }

}

- (id)initWithMid:(NSString *)mid Currentindex:(int)currentindex Accounts:(NSString *)accounts AllMids:(NSMutableArray *)allMides Isintranet:(BOOL)isIntranet
{
    self = [super init];
    if (self) {
        self.mid = mid;
        self.currentindex = currentindex;
        self.accounts = accounts;
        self.allmids = allMides;
        isintranet = isIntranet;
    }
    return self;
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}


@end
