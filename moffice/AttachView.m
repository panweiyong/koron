//
//  AttachView.m
//  moffice
//
//  Created by Mac Mini on 13-10-29.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "AttachView.h"

@implementation AttachView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)dealloc
{
    self.attachImageView = nil;
    self.attachNameLabel = nil;
    self.backgroundImageView = nil;
    self.statusImageView = nil;
    self.actView = nil;
    [super dealloc];
}

- (void)initSubviews {
    
    UIImageView *imageBackgroundView = [[UIImageView alloc] initWithFrame:self.bounds];
    imageBackgroundView.image = [[UIImage imageNamed:@"btn_attachadd.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    imageBackgroundView.alpha = 0.5;
    [self addSubview:imageBackgroundView];
    [imageBackgroundView release];
    
    _backgroundImageView = [[UIView alloc] initWithFrame:CGRectZero];
    _backgroundImageView.clipsToBounds = YES;
    [self addSubview:_backgroundImageView];
    
    UIImageView *imageProcessView = [[UIImageView alloc] initWithFrame:self.bounds];
    imageProcessView.image = [[UIImage imageNamed:@"btn_attachadd_highlighted.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    imageProcessView.alpha = 0.2;
    [_backgroundImageView addSubview:imageProcessView];
    [imageProcessView release];
    
    _attachImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
//    _attachImageView.backgroundColor = [UIColor redColor];
    [self addSubview:_attachImageView];
    
    
    _attachNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//    _attachNameLabel.backgroundColor = [UIColor orangeColor];
    _attachNameLabel.backgroundColor = [UIColor clearColor];
    _attachNameLabel.numberOfLines = 2;
    [self addSubview:_attachNameLabel];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedAttachView:)];
    [self addGestureRecognizer:singleTap];
    [singleTap release];
    
    _statusImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
//    _statusImageView.backgroundColor = [UIColor redColor];
    [self addSubview:_statusImageView];
    
    _actView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectZero];
    _actView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
//    [_actView startAnimating];
    [self addSubview:_actView];

}

- (void)layoutSubviews {
    _attachImageView.frame = CGRectMake(5, 5, 50, 50);
    _attachNameLabel.frame = CGRectMake(_attachImageView.right+5, _attachImageView.top, self.width-_attachImageView.width-35, _attachImageView.height);
    _statusImageView.frame = CGRectMake(self.width-35, 15, 30, 30);
    _actView.frame = CGRectMake(self.width-35, 15, 30, 30);
}

- (void)selectedAttachView:(UITapGestureRecognizer *)tap {
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectedAttachView:withIndex:)]) {
        [self.delegate didSelectedAttachView:self withIndex:self.tag];
    }
    NSLog(@"%d",self.tag);
}

@end
