//
//  KoronItemListViewController.h
//  TodoListDemo
//
//  Created by Mac Mini on 13-11-22.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronToDoListCell.h"
#import "KoronAddItemViewController.h"
#import "ASIFormDataRequest.h"
#import "KoronManager.h"
#import "SVProgressHUD.h"
#import "EGORefreshTableHeaderView.h"

@interface KoronItemListViewController : UIViewController<KoronToDoListCellDelegate,KoronAddItemViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,ASIHTTPRequestDelegate, EGORefreshTableHeaderDelegate>
{
    EGORefreshTableHeaderView *_toDoRefreshTableView;
    EGORefreshTableHeaderView *_didFinishRefreshTableView;
    BOOL _reloading;
}

@property (nonatomic, copy) NSString *status;
@property (nonatomic, retain) KoronChecklistItem *checkedItem;


//开始重新加载时调用的方法
- (void)reloadTableViewDataSource;
//完成加载时调用的方法
- (void)doneLoadingTableViewData;

@end
