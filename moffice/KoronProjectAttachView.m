//
//  KoronProjectAttachView.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-28.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectAttachView.h"

@implementation KoronProjectAttachView
{
    UIImageView *_icon;
    UILabel *_attachLabel;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews
{
    self.layer.cornerRadius = 10.0f;
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1].CGColor;
    
    _attachLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _attachLabel.textColor = [UIColor colorWithRed:47/255.0 green:146/255.0 blue:1 alpha:1];
    _attachLabel.backgroundColor = [UIColor clearColor];
    _attachLabel.font = [UIFont boldSystemFontOfSize:15];
    [self addSubview:_attachLabel];
    
    _icon = [[UIImageView alloc] initWithFrame:CGRectZero];
//    _icon.backgroundColor = [UIColor orangeColor];
    [self addSubview:_icon];
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _icon.frame = CGRectMake(5, 5, self.height - 10, self.height - 10);
    _attachLabel.frame = CGRectMake(_icon.right + 10, self.height/2.0-10, 200, 20);
    
}

- (void)dealloc
{
    [_icon release], _icon = nil;
    [_attachLabel release], _attachLabel = nil;
    [super dealloc];
}


@end
