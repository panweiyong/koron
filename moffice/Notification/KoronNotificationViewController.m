//
//  KoronNotificationViewController.m
//  moffice
//
//  Created by Mac Mini on 13-10-9.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronNotificationViewController.h"
#import "MailDetailsController.h"
#import "BaseNavigationController.h"
#import "WorkFlowDetailsController.h"
#import "BbsViewController.h"
#import "TipsDetailsViewController.h"
#import "JsonService.h"

@interface KoronNotificationViewController ()

@end

@implementation KoronNotificationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _manager = [KoronManager sharedManager];
        
        _notificationArr = [[NSMutableArray alloc]init];
        
        [self sendNotificationRequest];
        
        isEmpty = YES;
        
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController.navigationBar setHidden:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showTabbarNotification" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeStatusBarBlack" object:nil];
    [_tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeStatusBarBlack" object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
//    UITapGestureRecognizer *tableViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeMenu)];
//    [_tableView addGestureRecognizer:tableViewTap];
    
    _endView = [[KoronEndRefreshView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    
    _topButton = [[UIButton alloc]initWithFrame:CGRectMake(90, 0, 120, _topView.bounds.size.height)];
    [_topButton setTitle:@"系统消息" forState:UIControlStateNormal];
    [_topButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_topButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_topButton];
    _topView.userInteractionEnabled = YES;
    
    _topImageView = [[UIImageView alloc]initWithFrame:CGRectMake(_topButton.frame.origin.x + _topButton.frame.size.width - 20, _topView.frame.size.height /2 - 11, 20, 20)];
    UITapGestureRecognizer *topImageViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showMenu)];
    [_topImageView addGestureRecognizer:topImageViewTap];
    [_topImageView setImage:[UIImage imageNamed:@"news_down.png"]];
    [_topView addSubview:_topImageView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//重写父类方法,用来下拉刷新数据
- (void)beginSendRequst {
    _lastmid = @"";
    isEmpty = YES;
    [self sendNotificationRequest];
}

//重写父类方法,用来上拉刷新数据
- (void)scrollToTheEnd {
    if (isShowEndRefresh) {
        [self sendNotificationRequest];
        [_endView refreshStartAnimation];
        [_endView setTitle:@"加载中.."];
    }
}

#pragma mark - HttpRequest


- (void)sendNotificationRequest {
    if (_notificationRequest) {
        [_notificationRequest clearDelegatesAndCancel];
        _notificationRequest = nil;
    }
    
    _notificationRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[_manager getObjectForKey:SERVERURL]]];
    _notificationRequest.delegate = self;
    [_notificationRequest setDidFinishSelector:@selector(notificationRequestFinish:)];
    [_notificationRequest setDidFailSelector:@selector(notificationRequestFail:)];
    [_notificationRequest setPostValue:[_manager getObjectForKey:SID]forKey:SID];
    [_notificationRequest setPostValue:@"listTips" forKey:@"op"];
    [_notificationRequest setPostValue:_status forKey:@"status"];
    [_notificationRequest setPostValue:_type forKey:@"type"];
    [_notificationRequest setPostValue:_lastmid forKey:@"lastmid"];
    [_notificationRequest startAsynchronous];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([_notificationArr count] > 0 && isShowEndRefresh) {
        return [_notificationArr count] + 1;
    }else {
        return [_notificationArr count];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
   return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [_notificationArr count] && isShowEndRefresh) {
        UITableViewCell *endCell = [_tableView dequeueReusableCellWithIdentifier:@"EndCell"];
        if (nil == endCell) {
            endCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EndCell"];
        }
        for (UIView *view in endCell.contentView.subviews) {
            if ([view isKindOfClass:[KoronEndRefreshView class]]) {
                [view removeFromSuperview];
            }
        }
        [endCell.contentView addSubview:_endView];
        return endCell;
        
    }else {
        KoronNotificationCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"NotificationCell"];
        if (nil == cell) {
            cell = [[KoronNotificationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NotificationCell"];
        }
        
        KoronNotificationModel *model = [_notificationArr objectAtIndex:indexPath.row];
        cell.titleLabel.text = model.content;
        cell.timeLabel.text = [self computationTimeResult:model.createTime];
        cell.imageView.hidden = NO;
        
        [self notificationCell:cell withType:model.type andIsRead:model.flag];
        
        return cell;
    }
}

- (void)notificationCell:(UITableViewCell *)notificationCell withType:(NSString *)type andIsRead:(NSInteger)flag {
    KoronNotificationCell *cell = (KoronNotificationCell *)notificationCell;
    
    if (flag == 1) {
        if ([type isEqualToString:@"bbs"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_bbs_gray.png"]];
        }else if ([type isEqualToString:@"mail"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_mail_gray.png"]];
        }else if ([type isEqualToString:@"plan"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_plan_gray.png"]];
        }else if ([type isEqualToString:@"clientHandOver"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_client_gray.png"]];
        }else if ([type isEqualToString:@"approval"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_approval_gray.png"]];
        }else if ([type isEqualToString:@"advice"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_advice_gray.png"]];
        }else {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_other_gray.png"]];
        }
    }else {
        if ([type isEqualToString:@"bbs"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_bbs.png"]];
        }else if ([type isEqualToString:@"mail"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_mail.png"]];
        }else if ([type isEqualToString:@"plan"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_plan.png"]];
        }else if ([type isEqualToString:@"clientHandOver"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_client.png"]];
        }else if ([type isEqualToString:@"approval"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_approval.png"]];
        }else if ([type isEqualToString:@"advice"]) {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_advice.png"]];
        }else {
            [cell.typeImageView setImage:[UIImage imageNamed:@"news_item_other.png"]];
        }
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == [_notificationArr count]) {
        return 50;
    }else {
       return 70.0;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [self removeMenu];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    KoronNotificationModel *model = [_notificationArr objectAtIndex:indexPath.row];
    
    if(model.flag==0)
    {
        //CCLOG(@"xxxxxx:%i",flag);
        NSLog(@"type    is   %@",model.type);
        NSLog(@"refid    is   %@",model.refid);
        NSLog(@"tipsid    is   %@",model.notificationID);
        
        [self UpdateFlag:model.type oid:model.refid flag:1 tipsid:model.notificationID];
        
        KoronNotificationModel *newModel = [[KoronNotificationModel alloc] init];
        newModel = [model retain];
        newModel.flag = 1;
        [_notificationArr replaceObjectAtIndex:indexPath.row withObject:newModel];
        [model release];
        
    }
    
    
    
    if ([model.type isEqualToString:@"mail"]) {
        [self.navigationController.navigationBar setHidden:NO];
        MailDetailsController *tdc=[[MailDetailsController alloc] initWithNibName:nil bundle:nil];
        [tdc setMid: model.refid];
        [tdc setTipsid:model.notificationID];
        [tdc setAccounts:[[NSUserDefaults standardUserDefaults] objectForKey:@"userName"]];
        tdc.isSystemMail = YES;
        tdc.flag = [[_notificationArr objectAtIndex:indexPath.row] flag];
        [self.navigationController pushViewController:tdc animated:YES];
        [tdc release];
    }else if([model.type isEqualToString:@"approval"]) {
        [self.navigationController.navigationBar setHidden:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"hiddenTabbarNotification" object:nil];
        WorkFlowDetailsController *tdc=[[WorkFlowDetailsController alloc] initWithNibName:nil bundle:nil];
        [tdc setOid: model.refid];
        [tdc setTipsid:model.notificationID];
        [self.navigationController pushViewController:tdc animated:YES];
        [tdc release];
    }else if ([model.type isEqualToString:@"bbs"]) {
        [self.navigationController.navigationBar setHidden:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BaseNavigationControllerBackgroundColorChangeWhite" object:nil];
        BbsViewController *bbsView=[[BbsViewController alloc]initWithNibName:nil bundle:nil];
        [bbsView setRefid:model.refid];
        NSLog(@"%@",model.refid);
        [bbsView setTipsid:model.notificationID];
        [self.navigationController pushViewController:bbsView animated:YES];
        [bbsView release];
    }else {
        [self.navigationController.navigationBar setHidden:NO];
        TipsDetailsViewController *tdc=[[TipsDetailsViewController alloc] initWithNibName:nil bundle:nil];
        [tdc setType: model.type];
        [tdc setRefid: model.notificationID];
        [self.navigationController pushViewController:tdc animated:YES];
        [tdc release];
    }
    
    [_tableView reloadData];
}




#pragma mark - HttpRequestDelegate


- (void)notificationRequestFinish:(ASIFormDataRequest *)response {
    [super doneLoadingTableViewData];
    if (isEmpty) {
        [_notificationArr removeAllObjects];
    }
    NSLog(@"%@",[response responseString]);
    
    SBJsonParser *parser = [[SBJsonParser alloc]init];
    NSArray *arr = [parser objectWithString:[response responseString]];
    if ([arr count] == 0) {
        NSLog(@"[arr count]  %d",[arr count]);
        [_endView setTitle:@"没有更多了.."];
        [_endView refreshStopAnimation];
        isShowEndRefresh = NO;
        [_tableView reloadData];
        return;
    }
    if ([arr count] < 20) {
        isShowEndRefresh = NO;
    }else {
        isShowEndRefresh = YES;
    }

    for (NSDictionary *dictionary in arr) {
        KoronNotificationModel *notificationModel = [[KoronNotificationModel alloc]init];
        notificationModel.notificationID = [dictionary objectForKey:@"id"];
        notificationModel.content = [dictionary objectForKey:@"content"];
        notificationModel.refid = [dictionary objectForKey:@"refid"];
        notificationModel.flag = [[dictionary objectForKey:@"flag"] integerValue];
        notificationModel.type = [dictionary objectForKey:@"type"];
        notificationModel.createTime = [dictionary objectForKey:@"createTime"];
        [_notificationArr addObject:notificationModel];
    }

    [_endView setTitle:@"加载更多.."];
    [_endView refreshStopAnimation];
    _lastmid = [[_notificationArr lastObject] notificationID];
    isEmpty = NO;
    [_tableView reloadData];
}

- (void)notificationRequestFail:(ASIFormDataRequest *)response {
    [super doneLoadingTableViewData];
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
    [_endView setTitle:@"加载更多.."];
    [_endView refreshStopAnimation];
}


#pragma mark - computTime

- (NSString *)computationTimeResult:(NSString *)timeString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:timeString];
    NSInteger result = [self computationTime:[formatter stringFromDate:date]];
    
    if (result == 0) {
        [formatter setDateFormat:@"今天 HH:mm"];
    }else if (result == 1) {
        [formatter setDateFormat:@"昨天 HH:mm"];
    }else if (result == 2) {
        [formatter setDateFormat:@"前天 HH:mm"];
    }else {
        [formatter setDateFormat:@"MM-dd HH:mm"];
    }
    return [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
}


- (NSInteger)computationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowadayTimeStr = [fromatter stringFromDate:[NSDate date]];
    
    if ([[nowadayTimeStr substringToIndex:3] integerValue] - [[timeStr substringToIndex:3] integerValue] == 0) {
        if ([[nowadayTimeStr substringWithRange:NSMakeRange(5, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(5, 2)] integerValue] == 0) {
            if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 0) {
                return 0;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
                return 1;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 2) {
                return 2;
            }else {
                return 4;
            }
        }else {
            return 4;
        }
        
    }else {
        return 4;
    }
    
}

#pragma mark - showMenu
- (void)showMenu {
    
    
    if (nil == _menu) {
        UIImage *image = [UIImage imageNamed:@"popover_background.png"];
        UIImage *backgroundImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)];
        _backgroundImageView = [[UIImageView alloc]initWithImage:backgroundImage];
        [_backgroundImageView setImage:backgroundImage];
        [_backgroundImageView setFrame:CGRectMake(self.view.frame.size.width/2 - 217/2, 50, 217, 265)];
        
        _menu = [[KoronMenuViewController alloc]initWithStyle:UITableViewStylePlain];
        _menu.delegate = self;
        [_menu setTableViewFrame:CGRectMake(self.view.frame.size.width/2 - 217/2 + 17/2, 60, 200, 250)];
        
        _maskView = [[UIView alloc] initWithFrame:self.view.bounds];
        _maskView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4];
        UITapGestureRecognizer *maskViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeMenu)];
        [_maskView addGestureRecognizer:maskViewTap];
        [maskViewTap release];
        
    }
    
    [self.view addSubview:_maskView];
    [self.view addSubview:_backgroundImageView];
    [self.view addSubview:_menu.view];
    [_topImageView setImage:[UIImage imageNamed:@"news_up.png"]];
}

- (void)removeMenu {
    
    [_maskView removeFromSuperview];
    [_backgroundImageView removeFromSuperview];
    [_menu.view removeFromSuperview];
    
    [_topImageView setImage:[UIImage imageNamed:@"news_down.png"]];
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    [self removeMenu];
//}

#pragma mark - KoronMenuViewControllerDelegate

- (void)changeTheType:(NSString *)type {
    if ([type isEqualToString:@""]) {
        _type = @"";
        _status = @"";
    }else if([type isEqualToString:@"mail"]){
        _type = @"mail";
        _status = @"";
    }else if([type isEqualToString:@"bbs"]){
        _type = @"bbs";
        _status = @"";
    }else if([type isEqualToString:@"advice"]){
        _type = @"advice";
        _status = @"";
    }else if([type isEqualToString:@"plan"]){
        _type = @"plan";
        _status = @"";
    }else if([type isEqualToString:@"approval"]){
        _type = @"approval";
        _status = @"";
    }else if([type isEqualToString:@"clientHandOver"]){
        _type = @"clientHandOver";
        _status = @"";
    }else if([type isEqualToString:@"clientShare"]){
        _type = @"clientShare";
        _status = @"";
    }else if([type isEqualToString:@"noread"]){
        _type = @"";
        _status = @"noread";
    }else if([type isEqualToString:@"read"]){
        _type = @"";
        _status = @"read";
    }
    [self removeMenu];
    isEmpty = YES;
    _lastmid = @"";
    
    
    [self sendNotificationRequest];
}

//发送请求标识已读
-(void)UpdateFlag:(NSString*)type oid:(NSString*)oid flag:(int)flag tipsid:(NSString*)tipsid
{
    if (nil != _notificationRequest) {
        [_notificationRequest cancel];
        _notificationRequest.delegate = nil;
        [_notificationRequest release];
        _notificationRequest = nil;
    }
    
    CCLOG(@"%@",type);
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    _notificationRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:hosturl]];
	[_notificationRequest setDelegate:self];
    
    [_notificationRequest setPostValue:[[JsonService sharedManager] getSessionID] forKey:@"sid"];
    [_notificationRequest setPostValue:tipsid forKey:@"tipsid"];
    [_notificationRequest setPostValue:type forKey:@"type"];
    [_notificationRequest setPostValue:[NSString stringWithFormat:@"%i",flag] forKey:@"flag"];
    [_notificationRequest setPostValue:@"updateFlag" forKey:@"op"];
    
    [_notificationRequest setPostValue:oid forKey:@"oid"];
    [_notificationRequest setPostValue:oid forKey:@"refid"];
    
    [_notificationRequest setDidFinishSelector:@selector(updateFinished:)];
    [_notificationRequest setDidFailSelector:@selector(updateFailed:)];
    
    [_notificationRequest startAsynchronous];
}

#pragma mark - UpdateFlagDelegate


- (void)updateFinished : (ASIFormDataRequest *)response {
//    NSLog(@"%@",[[response responseString] JSONValue]);
    
    NSLog(@"%@",[response responseString]);
}

- (void)updateFailed : (ASIFormDataRequest *)response {
//    NSLog(@"%@",[[response responseString] JSONValue]);
    
    NSLog(@"%@",[response responseString]);
}


- (void)dealloc
{
    
    [_endView release],_endView = nil;
    [_notificationRequest release],_notificationRequest = nil;
    [_notificationArr release],_notificationArr = nil;
    [_lastmid release],_lastmid = nil;
    [_topButton release],_topButton = nil;
    [_topImageView release],_topImageView = nil;
    [_menu release],_menu = nil;
    [_backgroundImageView release],_backgroundImageView = nil;
    [_status release],_status = nil;
    [_type release],_type = nil;
    [_maskView release],_maskView = nil;
    [super dealloc];
}


@end
