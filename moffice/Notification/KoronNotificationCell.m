//
//  KoronNotificationCell.m
//  moffice
//
//  Created by Mac Mini on 13-10-9.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronNotificationCell.h"

@implementation KoronNotificationCell
@synthesize typeImageView = _typeImageView;
@synthesize titleLabel = _titleLabel;
@synthesize timeLabel = _timeLabel;
@synthesize imageView = _imageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        _typeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 12.5, 40, 40)];
        [self.contentView addSubview:_typeImageView];
        
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, 0, 200, 50)];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        _titleLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        _titleLabel.numberOfLines = 2;
        _titleLabel.textColor = [UIColor grayColor];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        
        [self.contentView addSubview:_titleLabel];
        
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(70, _titleLabel.frame.origin.y + _titleLabel.frame.size.height, 200, 15)];
        _timeLabel.font = [UIFont systemFontOfSize:12];
        _timeLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:_timeLabel];
        
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(_titleLabel.frame.origin.x + _titleLabel.frame.size.width + 10, 20, 20, 20)];
        [_imageView setImage:[UIImage imageNamed:@"open_arrow.png"]];
        [self.contentView addSubview:_imageView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect {
    CGRect frame = self.frame;
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
    CGContextSetAlpha(ctx, 0.4);
    CGContextSetLineWidth(ctx, 2);
    CGContextMoveToPoint(ctx, 5, frame.size.height);
    CGContextAddLineToPoint(ctx, frame.size.width - 5, frame.size.height);
    CGContextStrokePath(ctx);
}

@end
