//
//  KoronNotificationViewController.h
//  moffice
//
//  Created by Mac Mini on 13-10-9.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "KoronDropDownRefreshViewController.h"
#import "KoronManager.h"
#import "SBJsonParser.h"
#import "KoronNotificationModel.h"
#import "SVProgressHUD.h"
#import "KoronNotificationCell.h"
#import "KoronEndRefreshView.h"
#import "KoronMenuViewController.h"


@interface KoronNotificationViewController : KoronDropDownRefreshViewController <UITableViewDelegate,UITableViewDataSource,ASIHTTPRequestDelegate,KoronMenuViewControllerDelegate> {
    //上拉刷新
    KoronEndRefreshView *_endView;
    
    KoronManager *_manager;
    
    ASIFormDataRequest *_notificationRequest;
    
    NSMutableArray *_notificationArr;
    
    NSString *_lastmid;
    //判断是否重需要清空数据源
    BOOL isEmpty;
    //点击弹出menu
    UIButton *_topButton;
    
    UIImageView *_topImageView;
    //选项视图
    KoronMenuViewController *_menu;
    //menu的背景图片
    UIImageView *_backgroundImageView;
    //请求服务器时发送的状态
    NSString *_status;
    //请求服务器时发送的类型
    NSString *_type;
    //是否显示上拉刷新
    BOOL isShowEndRefresh;
    
    //遮挡试图
    UIView *_maskView;
}

@end
