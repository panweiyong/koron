//
//  KoronEndRefreshView.m
//  moffice
//
//  Created by Mac Mini on 13-10-10.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronEndRefreshView.h"

@implementation KoronEndRefreshView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        _label = [[UILabel alloc]initWithFrame:CGRectMake(100, 10, 120, 30)];
        _label.backgroundColor = [UIColor clearColor];
        _label.font = [UIFont systemFontOfSize:14];
        _label.textColor = [UIColor grayColor];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.text = @"加载更多..";
        [self addSubview:_label];
        
        _animationImageView = [[UIImageView alloc]initWithFrame:CGRectMake(50, 10, 30, 30)];
        _animationImageView.hidden = YES;
        [_animationImageView setImage:[UIImage imageNamed:@"AlbumFlagMark@2x.png"]];
        [self addSubview:_animationImageView];
        
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setTitle:(NSString *)title {
    _label.text = title;
}


- (void)refreshStartAnimation {
    [self refreshStopAnimation];
    _animationImageView.hidden = NO;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.delegate = self;
    animation.toValue = [NSNumber numberWithFloat:M_PI * 2.0];
    animation.duration = 1.0;
    animation.cumulative = YES;
    animation.repeatCount = 99999;
    [_animationImageView.layer addAnimation:animation forKey:@"animation"];
}

- (void)refreshStopAnimation {
    _animationImageView.hidden = YES;
    [_animationImageView.layer removeAllAnimations];
}

@end
