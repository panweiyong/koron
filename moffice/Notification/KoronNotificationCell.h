//
//  KoronNotificationCell.h
//  moffice
//
//  Created by Mac Mini on 13-10-9.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KoronNotificationCell : UITableViewCell {
    UIImageView *_typeImageView;
    
    UILabel *_titleLabel;
    
    UILabel *_timeLabel;
    
    UIImageView *_imageView;
}

@property (nonatomic,strong)UIImageView *typeImageView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UIImageView *imageView;

@end
