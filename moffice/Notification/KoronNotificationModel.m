//
//  KoronNotificationModel.m
//  moffice
//
//  Created by Mac Mini on 13-10-9.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronNotificationModel.h"

@implementation KoronNotificationModel
@synthesize notificationID = _notificationID;
@synthesize content = _content;
@synthesize type = _type;
@synthesize refid = _refid;
@synthesize flag = _flag;
@synthesize createTime = _createTime;

@end
