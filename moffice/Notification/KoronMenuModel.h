//
//  KoronMenuModel.h
//  moffice
//
//  Created by Mac Mini on 13-10-11.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KoronMenuModel : NSObject  {
    NSString *_menuTitle;
    NSString *_menuType;
    BOOL isSelected;
}

@property (nonatomic,strong)NSString *menuTitle;
@property (nonatomic,strong)NSString *menuType;
@property (nonatomic,assign)BOOL isSelected;


- (id)initWithMenuTitle:(NSString *)title andMenuType:(NSString *)type;

@end
