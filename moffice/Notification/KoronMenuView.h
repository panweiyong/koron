//
//  KoronMenuView.h
//  moffice
//
//  Created by Mac Mini on 13-10-10.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KoronMenuView : UIView {
    UILabel *_titleLabel;
}

- (void)setTitle:(NSString *)title;

@end
