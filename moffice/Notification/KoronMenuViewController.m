//
//  KoronMenuViewController.m
//  moffice
//
//  Created by Mac Mini on 13-10-10.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronMenuViewController.h"

@interface KoronMenuViewController ()

@end

@implementation KoronMenuViewController
@synthesize delegate;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        _menuArr = [[NSMutableArray alloc]init];
        for (int i = 0; i < 10; i++) {
            KoronMenuModel *model = nil;
            if (i == 0) {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"全部" andMenuType:@""];
                model.isSelected = YES;
            }else if (i == 1) {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"邮箱" andMenuType:@"mail"];
            }else if (i == 2) {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"论坛" andMenuType:@"bbs"];
            }else if (i == 3) {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"通知通告" andMenuType:@"advice"];
            }else if (i == 4) {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"工作计划" andMenuType:@"plan"];
            }else if (i == 5) {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"工作审批" andMenuType:@"approval"];
            }else if (i == 6) {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"客户移交" andMenuType:@"clientHandOver"];
            }else if (i == 7) {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"客户共享" andMenuType:@"clientShare"];
            }else if (i == 8) {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"未读" andMenuType:@"noread"];
            }else {
                model = [[KoronMenuModel alloc]initWithMenuTitle:@"已读" andMenuType:@"read"];
            }
            [_menuArr addObject:model];
        }
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)setTableViewFrame:(CGRect)rect {
    [self.view setFrame:rect];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return 8;
    }else {
        return 2;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (nil == cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    
    KoronMenuModel *model = [_menuArr objectAtIndex:indexPath.row + (indexPath.section * 8)];
    cell.textLabel.text = [model menuTitle];
    if (model.isSelected) {
        cell.textLabel.textColor = [UIColor orangeColor];
    }else {
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 20.0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    KoronMenuView *menuView = [[KoronMenuView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 20)];
    if (section == 0) {
        menuView.backgroundColor = [UIColor colorWithRed:96/255.0 green:96/255.0 blue:96/255.0 alpha:1];
        [menuView setTitle:@"类型"];
    }else {
        [menuView setTitle:@"状态"];
    }
    
    return menuView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    for (KoronMenuModel *model in _menuArr) {
        model.isSelected = NO;
    }
    KoronMenuModel *model = [_menuArr objectAtIndex:indexPath.row + (indexPath.section * 8)];
    model.isSelected = YES;
    [tableView reloadData];
    if (delegate && [delegate respondsToSelector:@selector(changeTheType:)]) {
        [delegate performSelector:@selector(changeTheType:) withObject:model.menuType];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
