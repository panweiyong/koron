//
//  KoronMenuView.m
//  moffice
//
//  Created by Mac Mini on 13-10-10.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronMenuView.h"

@implementation KoronMenuView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.bounds.size.width/2 - 10, 0, 60, 20)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.textColor = [UIColor grayColor];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_titleLabel];
    }
    return self;
}

- (void)setTitle:(NSString *)title {
    _titleLabel.text = title;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect {
    CGRect frame = self.frame;
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
    CGContextSetAlpha(ctx, 1);
    CGContextSetLineWidth(ctx, 1);
    CGContextMoveToPoint(ctx, 5, frame.size.height / 2);
    CGContextAddLineToPoint(ctx, frame.size.width/2 - 15, frame.size.height /2);
    
    CGContextMoveToPoint(ctx, frame.size.width/2 + 25, frame.size.height / 2);
    CGContextAddLineToPoint(ctx, frame.size.width - 5, frame.size.height /2);
    
    CGContextStrokePath(ctx);
}

@end
