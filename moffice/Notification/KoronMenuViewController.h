//
//  KoronMenuViewController.h
//  moffice
//
//  Created by Mac Mini on 13-10-10.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronMenuView.h"
#import "KoronMenuModel.h"

@protocol KoronMenuViewControllerDelegate <NSObject>

- (void)changeTheType:(NSString *)type;

@end


@interface KoronMenuViewController : UITableViewController {
    UIImageView *_backgroundImageView;
    NSMutableArray *_menuArr;
    NSMutableArray *_menuTypeArr;
    __unsafe_unretained id<KoronMenuViewControllerDelegate> delegate;
}

@property (nonatomic,assign)id<KoronMenuViewControllerDelegate> delegate;


-(void)setTableViewFrame:(CGRect)rect;

@end
