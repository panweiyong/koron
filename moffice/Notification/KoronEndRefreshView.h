//
//  KoronEndRefreshView.h
//  moffice
//
//  Created by Mac Mini on 13-10-10.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface KoronEndRefreshView : UIView {
    UILabel *_label;
    
    UIImageView *_animationImageView;
}

- (void)refreshStartAnimation;
- (void)refreshStopAnimation;
- (void)setTitle:(NSString *)title;

@end
