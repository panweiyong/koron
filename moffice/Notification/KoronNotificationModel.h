//
//  KoronNotificationModel.h
//  moffice
//
//  Created by Mac Mini on 13-10-9.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KoronNotificationModel : NSObject {
    NSString *_notificationID;
    NSString *_content;
    NSString *_type;
    NSString *_refid;
    NSInteger _flag;
    NSString *_createTime;
}

@property (strong,nonatomic)NSString *notificationID;
@property (strong,nonatomic)NSString *content;
@property (strong,nonatomic)NSString *type;
@property (strong,nonatomic)NSString *refid;
@property (assign,nonatomic)NSInteger flag;
@property (strong,nonatomic)NSString *createTime;

@end
