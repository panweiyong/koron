//
//  KoronMenuModel.m
//  moffice
//
//  Created by Mac Mini on 13-10-11.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronMenuModel.h"

@implementation KoronMenuModel
@synthesize menuTitle = _menuTitle;
@synthesize menuType = _menuType;
@synthesize isSelected;

- (id)initWithMenuTitle:(NSString *)title andMenuType:(NSString *)type
{
    self = [super init];
    if (self) {
        _menuTitle = title;
        _menuType = type;
    }
    return self;
}

@end
