//
//  WorkFlowViewController.m
//  moffice
//
//  Created by yangxi zou on 11-12-29.
//  Copyright (c) 2011年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "WorkFlowViewController.h"

@implementation WorkFlowViewController
 

-(void)dealloc
{     
    [tableData release];
    [tableView release];
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title=NSLocalizedString(@"Workflow",@"Workflow") ;
        
    }
    return self;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableData count]%20==0&&[tableData count]>0) {
        return  [tableData count]+1;
    }
    return  [tableData count];
} 


- (UITableViewCell *)tableView:(UITableView *)tableViewz cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==[tableData count]){
        static NSString *cellid= @"mcid" ;
        UITableViewCellStyle style = UITableViewCellStyleDefault; 
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        if (!cell) 
        {
            cell = [[[ UITableViewCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
        }
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        //[button setBackgroundImage:image forState:UIControlStateNormal];
        [button setFrame:CGRectMake(20, 20, 280, 30)];	
        //set title, font size and font color
        [button setTitle:NSLocalizedString(@"display 20",@"display 20")  forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
        //[button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal]; 
        //[button setAutoresizingMask: UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth];
        //set action of the button
        [button addTarget:self action:@selector(displayMore) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:button];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
    
	UITableViewCellStyle style = UITableViewCellStyleDefault; 
	static NSString *cellid= @"Wid";
	Workflow *cell =(Workflow *) [tableViewz dequeueReusableCellWithIdentifier:cellid];
    //NSAssert([[tableData objectAtIndex:indexPath.row] objectForKey:@"title"] != nil,@"tableData objectAtIndex null"); 
    if (cell== nil) 
    {
        cell = [[[ WorkflowCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
    }
    cell.textLabel.text=  [[tableData objectAtIndex:indexPath.row] objectForKey:@"title"];
    cell.textLabel.font=  [UIFont boldSystemFontOfSize:18];
    cell.textLabel.backgroundColor  = [UIColor clearColor];  
    cell.oid = [[tableData objectAtIndex:indexPath.row] objectForKey:@"id"];
    cell.createTime =  [[[[tableData objectAtIndex:indexPath.row] objectForKey:@"createTime"]  dateFromISO8601]toShortString];
    cell.creator = [[tableData objectAtIndex:indexPath.row] objectForKey:@"creator"];
    cell.type = [[tableData objectAtIndex:indexPath.row] objectForKey:@"type"];
    cell.currentNode = [[tableData objectAtIndex:indexPath.row] objectForKey:@"currentNode"];
    cell.cancelEnabled = [[[tableData objectAtIndex:indexPath.row] objectForKey:@"cancelEnabled"] boolValue];
    
    cell.flag=[[[tableData objectAtIndex:indexPath.row] objectForKey:@"flag"] intValue];
    if (cell.flag==0){
       cell.imageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"unread.png" ]];
    }else if(cell.flag==1){
        cell.imageView.image=[UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"dshenpi.png"]];
    }else if(cell.flag==2){
        cell.imageView.image=[UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"wread.png"]];
    }else if(cell.flag==3){
        cell.imageView.image=[UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"wover.png"]];
    }else{
       cell.imageView.image=[UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"read.png"]];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleBlue ;
    return cell;
    }
    return nil;
}

/*
- (CGFloat)tableView:(UITableView *)tableViewz heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row%20==0&&indexPath.row>0)
        return 50;
    return 73;
}*/

- (void)tableView:(UITableView *)tableViewx didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
     [tableViewx deselectRowAtIndexPath:indexPath animated:YES];
	 
     WorkflowCell *cell = (WorkflowCell *)[self tableView:tableViewx cellForRowAtIndexPath:indexPath];
    if(cell.flag==0)[[tableData objectAtIndex:indexPath.row] setObject:[NSNumber numberWithInt:1] forKey:@"flag"] ;
     WorkFlowDetailsController *tdc=[[WorkFlowDetailsController alloc] initWithNibName:nil bundle:nil];
     
     [tdc setOid: cell.oid];
     [self.navigationController pushViewController:tdc animated:YES];
     [tdc release];
    
}

//发起请求到服务器
-(void)requestJsonData:(NSString* )wid
{
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice listWorkflows:wid];
}

-(void)displayMore
{
    if (tableData!=nil) {
        requestModel=1;
        [self requestJsonData: [[tableData objectAtIndex: [tableData count]-1 ] objectForKey:@"id" ] ];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [tableView reloadData];
    
    if (IOS_VERSIONS > 6.9) {
        
        [self setNeedsStatusBarAppearanceUpdate];
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [[JsonService sharedManager] cancelAllRequest];
}

- (void)tableView:(UITableView *)tableViewx willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   /*
     HybridSubviewBasedApplicationCell *ncell=(HybridSubviewBasedApplicationCell*)cell;
     if ([ncell.sgdate isToday]) {
     cell.backgroundColor =  TODAY_BACKGROUND;		
     return;
     } */
     cell.backgroundColor = WFCELL_BACKGROUND;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadNavigationItem];
    
    tableView = [[UITableView alloc]  initWithFrame:CGRectMake(0,0, self.view.bounds.size.width ,self.view.bounds.size.height  - self.tabBarController.tabBar.frame.size.height- self.navigationController.navigationBar.frame.size.height  ) style: UITableViewStylePlain];
    [tableView setDelegate:self]; 
    [tableView setDataSource:self]; 
    [tableView setRowHeight: 73];
    tableView.showsHorizontalScrollIndicator= NO;
    tableView.showsVerticalScrollIndicator=YES;
    tableView.scrollEnabled=YES;
    tableView.indicatorStyle =UIScrollViewIndicatorStyleDefault ;
    //[tableView setBackgroundColor:[UIColor clearColor]]; 
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; 
    [tableView setIndicatorStyle:UIScrollViewIndicatorStyleBlack];
    [tableView.layer setShadowOpacity:0.5];
    [tableView.layer setShadowOffset:CGSizeMake(1, 1)];
    [self.view addSubview:tableView];
    
    if (_refreshHeaderView == nil) {
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - tableView.bounds.size.height, self.view.frame.size.width, tableView.bounds.size.height)];
		view.delegate = self;
		[tableView addSubview:view];
		_refreshHeaderView = view;
		[view release];
	}
	//  update the last update date
	[_refreshHeaderView refreshLastUpdatedDate];
    requestModel=0;
    [self requestJsonData:nil];
        
}


-(int)countUnread:(id )data
{
    if(tableData==nil)return 0;
    int count=0;
    //int flag = [[[tableData objectAtIndex:indexPath.row] objectForKey:@"flag"] intValue];
    for (id item in tableData)
    {
        if ([[item objectForKey:@"flag" ]intValue]==1)count++;
    }
    return count;
}

- (void)requestDataFinished:(id)jsondata//request
{
    //tableData=[jsondata retain];
   // NSLog(@"desc=%@",[jsondata objectForKey:@"desc"]);
    if([jsondata isKindOfClass:[NSDictionary class]])
    {
        if([[jsondata objectForKey:@"code"] intValue]<0||[[jsondata objectForKey:@"code"] intValue]==0)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:[jsondata objectForKey:@"desc"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            return;
        }
    }
    if(requestModel==0){
        tableData=[jsondata retain];
        [super requestDataFinished:jsondata];
        
       // return;
    }else{
        NSMutableArray *td= [NSMutableArray arrayWithArray: tableData];
        [td addObjectsFromArray:[jsondata retain]];
        tableData = [td retain];
        [super requestDataFinished:jsondata];
        //return;
    }

    //更新提示数字
    /*
    if(tableData&&[tableData count]>0){
    int cc= [self countUnread:tableData];
    if(cc>0){
        self.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i",cc ] ;
    }else{
        self.tabBarItem.badgeValue = nil;
    }
    }else{
        self.tabBarItem.badgeValue = nil;
    }*/
    [super requestDataFinished:jsondata];
}

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    requestModel=0;
	[super egoRefreshTableHeaderDidTriggerRefresh:view];
	//[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    [self requestJsonData:nil];
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}

- (void)backButtonPressed {

    [self.navigationController dismissModalViewControllerAnimated:YES];
    
    
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:18.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}
  
- (void)viewDidUnload
{
    [super viewDidUnload];
}
 
@end
