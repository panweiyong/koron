//
//  Names.h
//  TokenFieldExample
//
//  Created by Tom on 06/03/2010.
//  Copyright 2010 Tom Irving. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMUtils.h"

@interface Names : NSObject
+ (NSArray *)listMailOfNamesIntranet;
//+ (NSArray *)listMailOfNamesInternet;
+ (NSArray *)listOfNames ;
@end
