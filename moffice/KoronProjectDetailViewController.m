//
//  KoronProjectDetailViewController.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-27.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectDetailViewController.h"
#import "KoronProjectDetailHeaderCell.h"
#import <QuartzCore/QuartzCore.h>
#import "KoronProjectParticipantCell.h"
#import "KoronParticipantView.h"
#import "KoronProjectAttachCell.h"
#import "KoronProjectDownloadCell.h"
#import "ASIFormDataRequest.h"
#import "SVProgressHUD.h"
#import "KoronProjectDynamicCell.h"
#import "KoronDynamicModel.h"
#import "KoronProjectAttachView.h"
#import "KoronProjectAddNewDynamicViewController.h"

#define URL @"http://192.168.0.37:8000/moffice"
#define SID @"9fb2b1dd_1210101546122310136"


@interface KoronProjectDetailViewController ()

@end

@implementation KoronProjectDetailViewController
{
    UIImageView *_bottomImageView;
    NSInteger _selectedIndex;
    NSArray *_attachs;
    NSArray *_participants;
    ASIFormDataRequest *_dynamicRequest;
    ASIFormDataRequest *_taskRequest;
    BOOL _isfinishFromNetwork;
    NSMutableArray *_dynamicArray;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"项目详情";
        
        
        _selectedIndex = 0;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *attachArray = [self.detailItem.affix componentsSeparatedByString:@";"];
    if ([attachArray count] != 0) {
        _attachs = [attachArray retain];
        NSLog(@"%@",_attachs);
    }
    
    NSArray *participantsArray = [self.detailItem.participantCount componentsSeparatedByString:@","];
    if ([attachArray count] != 0 ) {
        _participants = [participantsArray retain];
    }

    [self loadNavigationItem];
    
    [self sendItemListRequest];
    
    self.view.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
    self.tableView.separatorColor = [UIColor clearColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ASIRequest
- (void)sendItemListRequest
{
    if (_dynamicRequest) {
        [_dynamicRequest clearDelegatesAndCancel];
        _dynamicRequest = nil;
    }
    
    _dynamicRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:URL]];
    _dynamicRequest.delegate = self;
    [_dynamicRequest setDidFinishSelector:@selector(dynamicRequestFinish:)];
    [_dynamicRequest setDidFailSelector:@selector(dynamicRequestFail:)];
    [_dynamicRequest setPostValue:SID forKey:@"sid"];

    
    [_dynamicRequest setPostValue:self.detailItem.projectId forKey:@"itemId"];
    [_dynamicRequest setPostValue:@"listMyItemsLog" forKey:@"op"];
    [_dynamicRequest setPostValue:@"0" forKey:@"count"];
    
    [_dynamicRequest startAsynchronous];
    
}

- (void)dynamicRequestFinish:(ASIFormDataRequest *)response
{
    NSLog(@"%@",[response responseString]);
    NSArray *dynamicArray = [[NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:nil] objectForKey:@"list"];
    
    if (_dynamicArray == nil) {
        _dynamicArray = [[NSMutableArray alloc] initWithCapacity:20];
    } else {
        [_dynamicArray removeAllObjects];
    }
    
    for (NSDictionary *dic in dynamicArray) {
        KoronDynamicModel *dynamicItem = [[KoronDynamicModel alloc] initWithContent:dic];
        [_dynamicArray addObject:dynamicItem];
        [dynamicItem release];
    }
    _isfinishFromNetwork = YES;
    [self.tableView reloadData];
}

- (void)dynamicRequestFail:(ASIFormDataRequest *)response
{
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_selectedIndex == 0 && section == 1 && _isfinishFromNetwork) {
        return [_dynamicArray count];
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && indexPath.section == 0) {
        static NSString *CellIdentifier = @"Cell";
        KoronProjectDetailHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[[KoronProjectDetailHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self configureContentForHeaderCell:cell withItem:self.detailItem];
        
        return cell;
    } else if (indexPath.section == 1 && _selectedIndex == 3) {
        static NSString *CellIdentifier = @"ParticipantCell";
        KoronProjectParticipantCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[KoronProjectParticipantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
           
            }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    } else if (indexPath.section == 1 && _selectedIndex == 2) {
        static NSString *CellIdentifier = @"AttachCell";
        KoronProjectAttachCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[KoronProjectAttachCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
        }
        
        for (NSInteger index = 0; index < [_attachs count] - 1; index++) {
            KoronProjectAttachView *attachView = (KoronProjectAttachView *)[cell.contentView viewWithTag:index+100];
            attachView.attachLabel.text = _attachs[index];
            attachView.icon.image = [UIImage imageNamed:[self getAttachPicture:_attachs[index]]];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }else if (indexPath.section == 1 && _selectedIndex == 1) {
        static NSString *CellIdentifier = @"DownloadCell";
        KoronProjectDownloadCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[KoronProjectDownloadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
        }
        [cell startActivityIndicatorView];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    } else if (indexPath.section == 1 && _selectedIndex == 0) {
        
        if (_isfinishFromNetwork == NO) {
            
            static NSString *CellIdentifier = @"DownloadCell";
            KoronProjectDownloadCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[[KoronProjectDownloadCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                
            }
            [cell startActivityIndicatorView];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
            
        } else {
            static NSString *CellIdentifier = @"DynamicCell";
            KoronProjectDynamicCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[[KoronProjectDynamicCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if (indexPath.row == 0) {
                cell.dynamicButton.enabled = YES;
                [self configureForDynamicCell:cell isShow:YES];
                [cell.dynamicButton addTarget:self action:@selector(addNewDynamic:) forControlEvents:UIControlEventTouchUpInside];
            } else {
                cell.dynamicButton.enabled = NO;
                KoronDynamicModel *item = _dynamicArray[indexPath.row - 1];
                [self configureForDynamicCell:cell isShow:NO];
                [self configureForDynamicCellContent:cell withItem:item];
            }
            
            return cell;
        }
        
        
        
        
       
    }
    
    return nil;
}

- (void)addNewDynamic:(UIButton *)button
{
    NSLog(@"tapButton");
    KoronProjectAddNewDynamicViewController *controller = [[KoronProjectAddNewDynamicViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void)configureForDynamicCellContent:(KoronProjectDynamicCell *)cell withItem:(KoronDynamicModel *)item
{
    cell.timeLabel.text = [self loadFomatterDate:item.createTime];
    cell.replyContentLabel.text = [NSString stringWithFormat:@"            回复              %@",item.content];
    if ([item.replyId isEqualToString:@""]) {
        [cell changeDynamicFrame:NO];
        [cell setDottedLineHidden:NO];
    } else {
        [cell changeDynamicFrame:YES];
        [cell setDottedLineHidden:YES];
    }
}

- (void)configureForDynamicCell:(KoronProjectDynamicCell *)cell isShow:(BOOL)isShow
{
    if (isShow) {
        cell.dynamicButton.hidden = NO;
        cell.dynamicBackgroundView.hidden = YES;
    } else {
        cell.dynamicButton.hidden = YES;
        cell.dynamicBackgroundView.hidden = NO;
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (void)configureContentForHeaderCell:(KoronProjectDetailHeaderCell *)headerCell withItem:(KoronProjectModel *)item
{
    
    headerCell.detailTimeLabel.text = [NSString stringWithFormat:@"%@ - %@",[self.detailItem beginTime],[self.detailItem endTime]];
    
    headerCell.titleLabel.text = item.title;
    headerCell.executorLabel.text = item.executor;
    headerCell.detailContentLabel.text = [item remark];
    
    headerCell.remainingTimeLabel.text = [self configureTimeForLabelWithStatus:item.status andEndTime:item.endTime];
    headerCell.statusPicture.image = [UIImage imageNamed:[self configureImageForCell:headerCell.remainingTimeLabel.text]];
    
}

- (NSString *)configureTimeForLabelWithStatus:(NSString *)status andEndTime:(NSString *)endTime
{
    if ([status isEqualToString:@"2"]) {
        return @"已完成";
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd 23:59:59"];
    NSDate *endDate = [formatter dateFromString:endTime];
    NSDate *nowDate = [NSDate date];
    [formatter release];
    
    NSTimeInterval timeInterval = [endDate timeIntervalSinceDate:nowDate];
    
    CGFloat days = timeInterval/(60 * 60 * 24);
    CGFloat hours = timeInterval/(60 * 60);
    
    if (days > 1.0) {
        return [NSString stringWithFormat:@"剩余%ld天",lround(days)+1];
    }else if (hours > 1.0) {
        return [NSString stringWithFormat:@"剩下%ld小时",lround(hours)];
    }else if (hours > 0) {
        return @"即将到期";
    }else {
        return @"已超期";
    }
    
}

- (NSString *)configureImageForCell:(NSString *)remainingTimeString
{
    if ([remainingTimeString isEqualToString:@"已完成"]) {
        return @"items_b";
    } else if ([remainingTimeString isEqualToString:@"已超期"]) {
        return @"items_r";
    } else {
        return @"items_g";
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 10, kDeviceWidth, 45)];
        view.backgroundColor = [UIColor clearColor];
        
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 10)];
        backgroundView.backgroundColor = [UIColor colorWithRed:231/255.0 green:231/255.0 blue:231/255.0 alpha:1];
        [view addSubview:backgroundView];
        [backgroundView release];
        
        UIView *selectView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, 35)];
        selectView.layer.borderWidth = 1;
        selectView.layer.borderColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1].CGColor;
        selectView.backgroundColor = [UIColor whiteColor];
        selectView.userInteractionEnabled = YES;
        [view addSubview:selectView];
        [selectView release];
        
        NSArray *nameArray = @[@"动态", @"任务", @"附件", @"人员"];
        for (NSInteger index = 0; index < 4; index++) {
            
            UILabel *countLabel = [[UILabel alloc] initWithFrame:CGRectMake(5+75*index, 11, 25, 12)];
            countLabel.backgroundColor = [UIColor clearColor];
            countLabel.tag = 100+index;
            countLabel.font = [UIFont systemFontOfSize:10];
            countLabel.textAlignment = NSTextAlignmentRight;
            
            if (index == 0) {
                countLabel.text = self.detailItem.logCount;
            } else if (index == 1) {
                countLabel.text = self.detailItem.taskCount;
            } else if (index == 2) {
                NSArray *attachArray = [self.detailItem.affix componentsSeparatedByString:@";"];
                if ([attachArray count] != 0) {
                    countLabel.text = [NSString stringWithFormat:@"%d",attachArray.count - 1];
                }
            } else {
                
                NSArray *participantsArray = [self.detailItem.participantCount componentsSeparatedByString:@","];
                if ([participantsArray count] != 0) {
                    countLabel.text = [NSString stringWithFormat:@"%d",participantsArray.count - 1];
                }
            }
            
            if (_selectedIndex + 100 == 100 + index) {
                countLabel.textColor = [UIColor blackColor];
            } else {
                countLabel.textColor = [UIColor grayColor];
            }
            
            [selectView addSubview:countLabel];
            [countLabel release];
            
            UILabel *detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(countLabel.right+3, countLabel.top, 20, countLabel.height)];
            detailLabel.text = nameArray[index];
            detailLabel.tag = 110+index;
            detailLabel.font = [UIFont systemFontOfSize:10];
            
            if (_selectedIndex + 110 == 110 + index) {
                detailLabel.textColor = [UIColor blackColor];
            } else {
                detailLabel.textColor = [UIColor grayColor];
            }
            
            [selectView addSubview:detailLabel];
            [detailLabel release];
            
        }
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeDetailContent:)];
        [selectView addGestureRecognizer:tap];
        [tap release];
        
        UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(1, selectView.height, selectView.width-1, 1)];
        bottomLine.backgroundColor = [UIColor whiteColor];
        [selectView addSubview:bottomLine];
        [bottomLine release];
        
        
        for (NSInteger index = 0; index < 3; index++) {
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(75+75*index, 10, 1, 15)];
            line.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
            [selectView addSubview:line];
            [line release];
        }
        
        _bottomImageView = [[UIImageView alloc] initWithFrame:CGRectMake(75 * _selectedIndex, selectView.bottom - 22, 75, 12)];
        UIImage *image = [[UIImage imageNamed:@"myitems_cur"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
        _bottomImageView.image = image;
        [selectView addSubview:_bottomImageView];
        
        
        return [view autorelease];
    } else {
        UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
        view.hidden = YES;
        return [view autorelease];
        return nil;
    }
    
}

- (void)changeDetailContent:(UITapGestureRecognizer *)tap
{
    
    CGPoint point = [tap locationInView:tap.view];
    
    NSInteger xOffset = 0;
    if (point.x < 75) {
        NSLog(@"动态");
        xOffset = 0;
        _selectedIndex = 0;
    } else if (point.x < 150 && point.x > 75) {
        NSLog(@"任务");
        xOffset = 75;
        _selectedIndex = 1;

    } else if (point.x < 225 && point.x > 150) {
        NSLog(@"附件");
        xOffset = 150;
        _selectedIndex = 2;

    } else if (point.x < 300) {
        NSLog(@"参与人");
        xOffset = 225;
        _selectedIndex = 3;
    }
    _bottomImageView.frame = CGRectMake(0+xOffset, 35-12, 75, 12);
    
    [tap.view.subviews enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *label = obj;
            if (label.tag == 100 + _selectedIndex || label.tag == 110 + _selectedIndex) {
                label.textColor = [UIColor blackColor];
            } else {
                label.textColor = [UIColor grayColor];
            }
        }
    }];
    
    [self.tableView reloadData];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.hidden = YES;
    return [view autorelease];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 10;
    } else {
        return 45;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    if (_selectedIndex == 1) {
        return 200;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        NSString *detailContentString = [self.detailItem remark];
        CGSize size = [detailContentString sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(185, 10000)];
        
        return size.height+110;

    } else if (indexPath.section == 1 && _selectedIndex == 3) {
        return (31/2 + 1) * 60 + 20;
    } else if (indexPath.section == 1 && _selectedIndex == 2) {
        return (_attachs.count - 1) * 60;
    } else if (indexPath.section == 1 && _selectedIndex == 1) {
        return 44;
    } else if (indexPath.section == 1 && _selectedIndex == 0) {
        if (_isfinishFromNetwork) {
            return 60;
        }
        return 44;
    } else {
        return 44;
    }
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}

- (void)backButtonPressed
{
    [_dynamicRequest clearDelegatesAndCancel];
    [_taskRequest clearDelegatesAndCancel];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - getDate Method
- (NSString *)loadFomatterDate:(NSString *)dateString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger result = [self computationTime:[formatter stringFromDate:date]];
    if (result == 0) {
        [formatter setDateFormat:@"今天 HH:mm"];
    }else if (result == 1) {
        [formatter setDateFormat:@"昨天 HH:mm"];
    }
    else if (result == 2) {
        [formatter setDateFormat:@"前天 HH:mm"];
    }else if (result == 3) {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    }else {
        [formatter setDateFormat:@"MM-dd HH:mm"];
    }
    
    
    return [formatter stringFromDate:date];
    
}

- (NSInteger)computationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowadayTimeStr = [fromatter stringFromDate:[NSDate date]];
    
    if ([[nowadayTimeStr substringToIndex:4] integerValue]- [[timeStr substringToIndex:4] integerValue] != 0) {
        return 3;
    }
    
    if ([[nowadayTimeStr substringToIndex:4] integerValue] - [[timeStr substringToIndex:4] integerValue] == 0) {
        if ([[nowadayTimeStr substringWithRange:NSMakeRange(5, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(5, 2)] integerValue] == 0) {
            if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 0) {
                return 0;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
                return 1;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 2) {
                return 2;
            }else {
                return 4;
            }
        }else {
            return 4;
        }
        
    }else {
        return 4;
    }
    
}

- (NSString *)getAttachPicture:(NSString *)attachName {
    if ([attachName hasSuffix:@".zip"]) {
        return @"attach_file_icon_zip.png";
    }else if ([attachName hasSuffix:@".xls"]) {
        return @"attach_file_icon_xls.png";
    }else if ([attachName hasSuffix:@".wps"]) {
        return @"attach_file_icon_wps.png";
    }else if ([attachName hasSuffix:@".video"]) {
        return @"attach_file_icon_video.png";
    }else if ([attachName hasSuffix:@".txt"]) {
        return @"attach_file_icon_txt.png";
    }else if ([attachName hasSuffix:@".swf"]) {
        return @"attach_file_icon_swf.png";
    }else if ([attachName hasSuffix:@".psd"]) {
        return @"attach_file_icon_psd.png";
    }else if ([attachName hasSuffix:@".ppt"]) {
        return @"attach_file_icon_ppt.png";
    }else if ([attachName hasSuffix:@".pdf"]) {
        return @"attach_file_icon_pdf.png";
    }else if ([attachName hasSuffix:@".png"]) {
        return @"attach_file_icon_image.png";
    }else if ([attachName hasSuffix:@".html"]) {
        return @"attach_file_icon_html.png";
    }else if ([attachName hasSuffix:@".fla"]) {
        return @"attach_file_icon_fla.png";
    }else if ([attachName hasSuffix:@".doc"]||[attachName hasSuffix:@".docx"]) {
        return @"attach_file_icon_doc.png";
    }else if ([attachName hasSuffix:@".mp3"]) {
        return @"attach_file_icon_audio.png";
    }
    return @"attach_file_icon_default.png";
}




- (void)dealloc
{
    [_dynamicRequest release], _dynamicRequest = nil;
    [_taskRequest release], _taskRequest = nil;
    [_bottomImageView release], _bottomImageView = nil;
    [_participants release], _participants = nil;
    [_attachs release], _attachs = nil;
    [super dealloc];
}

@end
