//
//  KoronProjectTableViewController.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-25.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectTableViewController.h"
#import "KoronProjectModel.h"
#import "KoronProjectDetailViewController.h"
#import "KoronEndRefreshView.h"
#import "KoronRefrashCell.h"

#define URL @"http://192.168.0.37:8000/moffice"
#define SID @"9fb2b1dd_1210101546122310136"
/*
 2013-11-25 16:37:27.498 moffice[13959:a0b] url: http://192.168.0.37:8000/moffice
 2013-11-25 16:37:27.498 moffice[13959:a0b] sid: 3832bdf5_0912211631568490003
 */
@interface KoronProjectTableViewController ()

@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *count;

@end

@implementation KoronProjectTableViewController
{
    EGORefreshTableHeaderView *_refreshTableView;
    BOOL _reloading;
    ASIFormDataRequest *_projectListRequest;
    KoronEndRefreshView *_endView;
    //是否显示上拉刷新
    BOOL _isShowEndRefresh;
    BOOL _isEmpty;
    NSMutableArray *_projectLists;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"ProjectStatus"]) {
            self.status = [[NSUserDefaults standardUserDefaults] objectForKey:@"ProjectStatus"];
        } else {
            self.status = @"1";
        }
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"ProjectType"]) {
            self.type = [[NSUserDefaults standardUserDefaults] objectForKey:@"ProjectType"];
        } else {
            self.type = @"1";
        }
        self.title = @"我的项目";
        
        [self loadProjectListItems];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadNavigationItems];
    
//    [self sendProjectListRequest];
    
    NSLog(@"%@", [self docementsDirectory]);
    

    if ([self.tableView respondsToSelector:@selector(separatorInset)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    
    if (_refreshTableView == nil) {
        //初始化下拉刷新控件
        EGORefreshTableHeaderView *refreshView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height)];
        refreshView.delegate = self;
        //将下拉刷新控件作为子控件添加到UITableView中
        [self.tableView addSubview:refreshView];
        _refreshTableView = refreshView;
    }
    
    _endView = [[KoronEndRefreshView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 50)];
    
}

#pragma mark - Documents
- (NSString *)docementsDirectory
{
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [pathArray lastObject];
    return documentsDirectory;
}

- (NSString *)dataFilePath
{
    return [[self docementsDirectory] stringByAppendingPathComponent:@"ProjectList.plist"];
}

- (void)saveProjectListItems
{
    NSMutableData *data = [[NSMutableData alloc] initWithCapacity:20];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:_projectLists forKey:@"ProjectListItems"];
    [archiver finishEncoding];
    
    [data writeToFile:[self dataFilePath] atomically:YES];
}

- (void)loadProjectListItems
{
    NSString *path = [self dataFilePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSMutableData *data = [[NSMutableData alloc] initWithContentsOfFile:path];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        
        _projectLists = [unarchiver decodeObjectForKey:@"ProjectListItems"];
        [unarchiver finishDecoding];
        
        [SVProgressHUD showSuccessWithStatus:@"本地加载" duration:1];
    } else {
        _projectLists = [[NSMutableArray alloc] initWithCapacity:20];
        [self sendProjectListRequest];
    }
    
    
}

#pragma mark - ASIRequest
- (void)sendProjectListRequest
{

    if (_projectListRequest) {
        [_projectListRequest clearDelegatesAndCancel];
        _projectListRequest = nil;
    }
    
    _projectListRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:URL]];
    _projectListRequest.delegate = self;
    [_projectListRequest setDidFinishSelector:@selector(projectListRequestFinish:)];
    [_projectListRequest setDidFailSelector:@selector(projectListRequestFail:)];
    
    [_projectListRequest setPostValue:@"listMyItems" forKey:@"op"];
    [_projectListRequest setPostValue:_type forKey:@"type"];
    [_projectListRequest setPostValue:_status forKey:@"status"];
    [_projectListRequest setPostValue:_count forKey:@"count"];
    [_projectListRequest setPostValue:SID forKey:@"sid"];
    
    [_projectListRequest startAsynchronous];
}

- (void)projectListRequestFinish:(ASIFormDataRequest *)response
{
    NSArray *listArray = [[NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil] objectForKey:@"list"];
    
    if ([listArray count] == 0) {
        NSLog(@"没有更多了");
        [SVProgressHUD showErrorWithStatus:@"没有更多了" duration:1];
        
        [_endView setTitle:@"没有更多了.."];
        [_endView refreshStopAnimation];
        _isShowEndRefresh = NO;
        [self.tableView reloadData];
        return;
    }
    if ([listArray count] < 20) {
        _isShowEndRefresh = NO;
    }else {
        _isShowEndRefresh = YES;
    }
    
    if (_isEmpty) {
        [_projectLists removeAllObjects];
    }
    
    for (NSDictionary *dic in listArray) {
        KoronProjectModel *model = [[KoronProjectModel alloc] initWithContent:dic];
        [_projectLists addObject:model];
        [model release];
    }
    
    [self.tableView reloadData];
    
    [self saveProjectListItems];
    
    [_endView setTitle:@"加载更多.."];
    [_endView refreshStopAnimation];
    _isEmpty = NO;
    self.count = [NSString stringWithFormat:@"%d",_projectLists.count];
//    self.count = @"0";
    [self doneLoadingTableViewData];
    
    [SVProgressHUD showSuccessWithStatus:@"刷新成功" duration:1];
}

- (void)projectListRequestFail:(ASIFormDataRequest *)response
{
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
}

#pragma mark - Private Method
- (void)loadNavigationItems
{
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
    
}

#pragma mark - Target Action
- (void)backButtonPressed
{
    if (_projectListRequest) {
        [_projectListRequest clearDelegatesAndCancel];
        _projectListRequest = nil;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - KoronProjectCell Delegate
- (void)KoronProjectCell:(KoronProjectCell *)cell didSelectItem:(NSString *)itemstring
{
    NSLog(@"选择了");

    
}

- (void)KoronProjectCellDidSelectUpView:(KoronProjectCell *)cell
{
    NSLog(@"上边视图");
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    KoronProjectDetailViewController *controller = [[KoronProjectDetailViewController alloc] init];
    controller.detailItem = _projectLists[indexPath.section - 1];
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

#pragma mark - KoronProjectHeaderCell Delegate
- (void)KoronProjectHeaderCell:(KoronProjectHeaderCell *)cell DidChangeStatus:(NSString *)status
{
    NSLog(@"change status");
    self.status = status;
    
    [[NSUserDefaults standardUserDefaults] setObject:status forKey:@"ProjectStatus"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)KoronProjectHeaderCell:(KoronProjectHeaderCell *)cell DidChangeType:(NSString *)type
{
    NSLog(@"change type");
    self.type = type;
    [[NSUserDefaults standardUserDefaults] setObject:type forKey:@"ProjectType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_projectLists count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ((section == [_projectLists count]) && _isShowEndRefresh) {
        return 2;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        KoronProjectHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"typeCell"];
        if (cell == nil) {
            cell = [[[KoronProjectHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"typeCell" withStatusIndex:[_status integerValue] andTypeIndex:[_type integerValue]] autorelease];
            cell.delegate = self;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self configureFrameForHeaderCell:cell withType:_type andStatus:_status];
        
        return cell;
        
    } else if ((indexPath.section == [_projectLists count]) && indexPath.row == 1 && _isShowEndRefresh) {
        KoronRefrashCell *endCell = [tableView dequeueReusableCellWithIdentifier:@"EndCell"];
        if (nil == endCell) {
            endCell = [[KoronRefrashCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EndCell"];
        }
        for (UIView *view in endCell.contentView.subviews) {
            if ([view isKindOfClass:[KoronEndRefreshView class]]) {
                [view removeFromSuperview];
            }
        }
        [endCell.contentView addSubview:_endView];
        return endCell;

    } else {
        static NSString *CellIdentifier = @"Cell";
        KoronProjectCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[[KoronProjectCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.delegate = self;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        KoronProjectModel *item = _projectLists[indexPath.section - 1];
        
        [self configureContentForCell:cell withProjectItem:item];
        
        return cell;
    }
    
    
}

- (void)configureFrameForHeaderCell:(KoronProjectHeaderCell *)cell withType:(NSString *)type andStatus:(NSString *)status
{
 
    if ([type isEqualToString:@"0"]) {
        
       cell.typeSelectedView.frame = CGRectMake(17, 2.5, 60, 29);
    } else if ([type isEqualToString:@"1"]) {
        cell.typeSelectedView.frame = CGRectMake(17 + 75, 2.5, 90, 29);
    } else {
        cell.typeSelectedView.frame = CGRectMake(17 + 75 + 100, 2.5, 90, 29);
    }

    if ([status isEqualToString:@"0"]) {
        cell.statusSelectedView.frame = CGRectMake(17, 2.5 + cell.height/2.0, 60, 29);
    } else if ([status isEqualToString:@"1"]) {
        cell.statusSelectedView.frame = CGRectMake(17 + 75, 2.5 + cell.height/2.0, 90, 29);
    } else {
        cell.statusSelectedView.frame = CGRectMake(17 + 75 + 100, 2.5 + cell.height/2.0, 90, 29);
    }
 
}

- (void)configureContentForCell:(KoronProjectCell *)cell withProjectItem:(KoronProjectModel *)item
{
    cell.titleLabel.text = item.title;
    cell.executorLabel.text = item.executor;
    cell.detailContentLabel.text = item.remark;
    
    if (![item.logCount isEqualToString:@"0"]) {
        cell.dynamicLabel.text = item.logCount;
    } else {
        cell.dynamicLabel.text = @"动态";
    }
    
    if (![item.taskCount isEqualToString:@"0"]) {
        cell.assignmentLabel.text = item.taskCount;
    } else {
        cell.assignmentLabel.text = @"任务";
    }
    
    cell.remainingTimeLabel.text = [self configureTimeForLabelWithStatus:item.status andEndTime:item.endTime];
    cell.statusPicture.image = [UIImage imageNamed:[self configureImageForCell:cell.remainingTimeLabel.text]];
    NSArray *attachArray = [item.affix componentsSeparatedByString:@";"];
    if ([attachArray count] > 1) {
        cell.attachLabel.text = [NSString stringWithFormat:@"%d",attachArray.count - 1];
    } else {
        cell.attachLabel.text = @"附件";
    }
    
    NSArray *participantsArray = [item.participantCount componentsSeparatedByString:@","];
    if ([participantsArray count] > 1) {
        cell.peopleLabel.text = [NSString stringWithFormat:@"%d",participantsArray.count - 1];
    } else {
        cell.peopleLabel.text = @"人员";
    }
    
    
}

- (NSString *)configureTimeForLabelWithStatus:(NSString *)status andEndTime:(NSString *)endTime
{
    if ([status isEqualToString:@"2"]) {
        return @"已完成";
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd 23:59:59"];
    NSDate *endDate = [formatter dateFromString:endTime];
    NSDate *nowDate = [NSDate date];
    [formatter release];
    
    NSTimeInterval timeInterval = [endDate timeIntervalSinceDate:nowDate];
    
    CGFloat days = timeInterval/(60 * 60 * 24);
    CGFloat hours = timeInterval/(60 * 60);
   
    if (days > 1.0) {
        return [NSString stringWithFormat:@"剩余%ld天",lround(days)+1];
    }else if (hours > 1.0) {
        return [NSString stringWithFormat:@"剩下%ld小时",lround(hours)];
    }else if (hours > 0) {
        return @"即将到期";
    }else {
        return @"已超期";
    }
    
}

- (NSString *)configureImageForCell:(NSString *)remainingTimeString
{
    if ([remainingTimeString isEqualToString:@"已完成"]) {
        return @"items_b";
    } else if ([remainingTimeString isEqualToString:@"已超期"]) {
        return @"items_r";
    } else {
        return @"items_g";
    }
}


#pragma mark - TableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 60;
    } else if (indexPath.section == [_projectLists count] && indexPath.row == 1 && _isShowEndRefresh) {
        return 44;
    } else {

        NSString *detailContentString = [_projectLists[indexPath.section - 1] remark];
        CGSize size = [detailContentString sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(185, 10000)];
        
        return size.height+75 + 30;
    }
    
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods
//开始重新加载时调用的方法
- (void)reloadTableViewDataSource{
	_reloading = YES;
    _isEmpty = YES;
    
    self.count = @"0";
    [self sendProjectListRequest];
    NSLog(@"status : %@",_status);
    NSLog(@"type : %@",_type);
    
    [SVProgressHUD showWithStatus:@"刷新中"];
}

//完成加载时调用的方法
- (void)doneLoadingTableViewData{
    NSLog(@"doneLoadingTableViewData");
    
	_reloading = NO;
	[_refreshTableView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    //刷新表格内容
    [self.tableView reloadData];
}

#pragma mark -
#pragma mark Background operation
//这个方法运行于子线程中，完成获取刷新数据的操作
-(void)doInBackground
{
    NSLog(@"doInBackground");
    
    [NSThread sleepForTimeInterval:3];
    
    //后台操作线程执行完后，到主线程更新UI
    [self performSelectorOnMainThread:@selector(doneLoadingTableViewData) withObject:nil waitUntilDone:YES];
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods
//下拉被触发调用的委托方法
-(void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view
{
    [self reloadTableViewDataSource];
}

//返回当前是刷新还是无刷新状态
-(BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view
{
    return _reloading;
}

//返回刷新时间的回调方法
-(NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view
{
    return [NSDate date];
}

//重写父类方法,用来上拉刷新数据
- (void)scrollToTheEnd {
    if (_isShowEndRefresh) {
        [self sendProjectListRequest];
        [_endView refreshStartAnimation];
        [_endView setTitle:@"加载中.."];
    }
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
//滚动控件的委托方法
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_refreshTableView egoRefreshScrollViewDidScroll:scrollView];
    
    CGPoint contentOffsetPoint = self.tableView.contentOffset;
    CGRect frame = self.tableView.frame;
    //判断是否滚动到底部
    if (contentOffsetPoint.y >= self.tableView.contentSize.height - frame.size.height && _isShowEndRefresh)
    {
        [SVProgressHUD showWithStatus:@"加载中"];
        [self scrollToTheEnd];
    }

}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [_refreshTableView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark - Memory
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)dealloc
{
    [_endView release],_endView = nil;
    [_refreshTableView release], _refreshTableView = nil;
    [_projectListRequest release], _projectListRequest = nil;
    [super dealloc];
}

@end
