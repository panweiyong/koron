//
//  KoronChooseView.h
//  KoronMoffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronManager.h"

@interface KoronChooseView : UIView {
    //是否选择
    BOOL _isChoose;
    
    NSString *_type;
    
    KoronManager *_manager;
    
    UIImageView *_chooseView;
    
    UILabel *_label;
}
@property (nonatomic,assign)BOOL isChoose;


- (id)initWithFrame:(CGRect)frame type:(NSString *)type title:(NSString *)title;
-(void)setStatus:(BOOL) status;

@end
