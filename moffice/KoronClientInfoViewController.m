//
//  KoronClientInfoViewController.m
//  moffice
//
//  Created by Mac Mini on 13-11-12.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientInfoViewController.h"
#import "KoronManager.h"
#import "KoronClientInfoModel.h"
#import "KoronClientContactModel.h"
#import "KoronClientInfoCell.h"

@interface KoronClientInfoViewController ()

- (void)loadNavigationItem;

- (void)cellBackgroundImage:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath andArray:(NSArray *)array;

-(void)addTableView;

@end

@implementation KoronClientInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"客户详情";
        _infoArray = [[NSMutableArray alloc] init];
        _contactArray = [[NSMutableArray alloc] init];
        _isInfoSelected = YES;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
	
    [self loadNavigationItem];
    
    [self sendClientListRequest];
    
    
    UIImageView *topViewBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 35)];
    topViewBg.image = [UIImage imageNamed:@"top_tab_bg"];
    topViewBg.userInteractionEnabled = YES;
    [self.view addSubview:topViewBg];
    [topViewBg release];
    
    UITapGestureRecognizer *topTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(infoTap:)];
    [topViewBg addGestureRecognizer:topTap];
    [topTap release];
    
    _selectedView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth/2, 35)];
    _selectedView.image = [UIImage imageNamed:@"top_tab_item_bg2"];
    [topViewBg addSubview:_selectedView];
    
    UILabel *infoLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 10, 100, 15)];
    infoLabel.text = @"公司信息";
    infoLabel.textColor = [UIColor whiteColor];
    infoLabel.backgroundColor = [UIColor clearColor];
    infoLabel.textAlignment = NSTextAlignmentCenter;
    infoLabel.font = [UIFont systemFontOfSize:12];
    [topViewBg addSubview:infoLabel];
    [infoLabel release];
    
    UILabel *contactLabel = [[UILabel alloc] initWithFrame:CGRectMake(195, 10, 100, 15)];
    contactLabel.text = @"联系人信息";
    contactLabel.textColor = [UIColor whiteColor];
    contactLabel.backgroundColor = [UIColor clearColor];
    contactLabel.textAlignment = NSTextAlignmentCenter;
    contactLabel.font = [UIFont systemFontOfSize:12];
    [topViewBg addSubview:contactLabel];
    [contactLabel release];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, topViewBg.bottom, kDeviceWidth, kDeviceHeight-20-44-35) style:UITableViewStyleGrouped];
    _tableView.tag = 100;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if (IOS_VERSIONS >= 6.9) {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }

    
        
}

#pragma mark - Private Method
- (void)cellBackgroundImage:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath andArray:(NSArray *)array {
    if (IOS_VERSIONS < 7.0) {
        return;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    if (_isInfoSelected) {
        
        if ([array[indexPath.section] count] == 1) {
            UIImage *image =[[UIImage imageNamed:@"list_bg_group_single"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
            imageView.image = image;
            cell.backgroundView = imageView;
        }else {
            if ([array[indexPath.section] count] > 1) {
                if (indexPath.row == 0) {
                    UIImage *image =[[UIImage imageNamed:@"list_bg_group_top"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
                    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                    imageView.image = image;
                    cell.backgroundView = imageView;
                }else if (indexPath.row == [array[indexPath.section] count]-1) {
                    UIImage *image =[[UIImage imageNamed:@"list_bg_group_bottom"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
                    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                    imageView.image = image;
                    cell.backgroundView = imageView;
                }else {
                    UIImage *image =[[UIImage imageNamed:@"list_bg_group_middle"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
                    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                    imageView.image = image;
                    cell.backgroundView = imageView;
                }
            }
        }
    }else {
        
        if ([array count] == 1) {
            UIImage *image =[[UIImage imageNamed:@"list_bg_group_single"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
            imageView.image = image;
            cell.backgroundView = imageView;
        }else {
            if ([array count] > 1) {
                if (indexPath.row == 0) {
                    UIImage *image =[[UIImage imageNamed:@"list_bg_group_top"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
                    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                    imageView.image = image;
                    cell.backgroundView = imageView;
                }else if (indexPath.row == [array count]-1) {
                    UIImage *image =[[UIImage imageNamed:@"list_bg_group_bottom"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
                    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                    imageView.image = image;
                    cell.backgroundView = imageView;
                }else {
                    UIImage *image =[[UIImage imageNamed:@"list_bg_group_middle"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
                    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                    imageView.image = image;
                    cell.backgroundView = imageView;
                }
            }
        }

        
    }
}



- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}

-(void)addTableView
{
    UIView *mainScreen=[[UIView alloc]initWithFrame:self.view.bounds];
    [mainScreen setBackgroundColor:[UIColor colorWithRed:85.0/255.0 green:89.0/255.0 blue:92.0/255.0 alpha:0.5]];
    mainScreen.tag=20;
    [self.view addSubview:mainScreen];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeMaskViewAndTableView)];
    [mainScreen addGestureRecognizer:tap];
    [tap release];
    
    UITableView *outTable=[[UITableView alloc]initWithFrame:CGRectMake(10, self.view.frame.size.height /2 - 110, 300, 180) style:UITableViewStylePlain];
    [outTable setBackgroundColor:[UIColor whiteColor]];
    [outTable setBackgroundView:nil];
    outTable.layer.cornerRadius=5.0f;
    outTable.tag=11;
    outTable.delegate=self;
    outTable.dataSource=self;
    [self.view addSubview:outTable];
    
    if ([outTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [outTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
    
}

#pragma mark - TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView.tag == 100) {
        
        if (_isInfoSelected) {
            if (_infoArray.count != 0) {
                return _infoArray.count;
            }
        }else {
            if (_contactArray.count != 0) {
                return 1;
            }
        }
        
    }else {
        return 1;
    }
    
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (tableView.tag == 100) {
        
        if (_isInfoSelected) {
            if (_infoArray.count != 0) {
                return [_infoArray[section] count];
            }
        }else {
            if (_contactArray != 0) {
                return [_contactArray count];
            }
        }
        
    }else {
        return 3;
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100) {
        
        static NSString *cellIndentifier = @"infoCell";
        
        KoronClientInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
        
        if (cell == nil) {
            cell = [[[KoronClientInfoCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIndentifier] autorelease];
        }
        if (_isInfoSelected) {
            
            cell.keyLabel.text = [NSString stringWithFormat:@"%@:",[_infoArray[indexPath.section][indexPath.row] infoKey]];
            cell.valLabel.text = [NSString stringWithFormat:@"%@",[_infoArray[indexPath.section][indexPath.row] infoVal]];
            
            if ([@"<null>" isEqualToString:cell.valLabel.text] || [@"null" isEqualToString:cell.valLabel.text]) {
                cell.valLabel.text = @"";
            }
            
            [self cellBackgroundImage:cell withIndexPath:indexPath andArray:_infoArray];
            
            if (indexPath.section == 0 && [cell.keyLabel.text isEqualToString:@"客户电话:"]) {
                cell.valLabel.textColor = [UIColor blueColor];
            }else if (indexPath.section == 0 && [cell.keyLabel.text isEqualToString:@"客户传真:"]) {
                cell.valLabel.textColor = [UIColor blueColor];
            }
            else {
                cell.valLabel.textColor = [UIColor blackColor];
            }
            
        }else {
            
            cell.keyLabel.text = [NSString stringWithFormat:@"%@:",[_contactArray[indexPath.row] contactKey]];
            cell.valLabel.text = [_contactArray[indexPath.row] contactVal];
            
            [self cellBackgroundImage:cell withIndexPath:indexPath andArray:_contactArray];
            
            if ([[_contactArray[indexPath.row] contactKey] isEqualToString:@"电话"] || [[_contactArray[indexPath.row] contactKey] isEqualToString:@"手机"]) {
                cell.valLabel.textColor = [UIColor blueColor];
            }else {
                cell.valLabel.textColor = [UIColor blackColor];
            }
            
            if ([cell.valLabel.text isEqualToString:@"null"]) {
                cell.valLabel.text = @"";
            }
            
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }else {
    
        static NSString *CellIdentifier = @"nomalCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        switch (indexPath.row)
        {
            case 0:
                cell.textLabel.textColor=[UIColor whiteColor];
                cell.textLabel.text= @"选择";
                cell.textLabel.backgroundColor=[UIColor clearColor];
                UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
                backgrdView.backgroundColor = [UIColor blackColor];
                cell.backgroundView = backgrdView;
                break;
            case 1:
                cell.textLabel.text=@"拨打电话";
                break;
            case 2:
                cell.textLabel.text=@"发送短信";
                break;
            default:
                break;
        }
        return cell;
        
    }
    
    return nil;
}

#pragma mark - TableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 11) {
        return 60;
    }else {
        if (_isInfoSelected) {
            NSString *infoVal = [NSString stringWithFormat:@"%@",[_infoArray[indexPath.section][indexPath.row] infoVal] ];
            CGSize size = [infoVal sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(200, 10000)];
            
            return size.height+30;
        }else {
            NSString *contactVal = [NSString stringWithFormat:@"%@",[_contactArray[indexPath.row] contactVal]];
            CGSize size = [contactVal sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(200, 10000)];
            
            return size.height+30;
        }
        
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView.tag == 11) {
        return 0;
    }
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* customView = [[[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 30.0)] autorelease];
    
    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.opaque = NO;
    headerLabel.textColor = [UIColor colorWithRed:46/255.0 green:13/255.0 blue:110/255.0 alpha:1];
    headerLabel.highlightedTextColor = [UIColor whiteColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:18];
    headerLabel.frame = CGRectMake(15.0, 0.0, 300.0, 30.0);
    
    if (_infoArray.count != 0 && _isInfoSelected) {
        
        headerLabel.text = [[_infoArray[section] objectAtIndex:0] infoGroup];
    }else {
        headerLabel.text = @"基础信息";
    }
    
    [customView addSubview:headerLabel];
    [headerLabel release];
    
    return customView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *phoneNum = nil;
    if (tableView.tag == 100) {
     
        KoronClientInfoCell *cell = (KoronClientInfoCell *)[tableView cellForRowAtIndexPath:indexPath];
        if ([cell.keyLabel.text isEqualToString:@"客户电话:" ]|| [cell.keyLabel.text isEqualToString:@"电话:" ] || [cell.keyLabel.text isEqualToString:@"手机:" ] || [cell.keyLabel.text isEqualToString:@"客户传真:" ]) {
            if (![cell.valLabel.text isEqualToString:@""] ) {
                phoneNum = cell.valLabel.text;
                
                [self addTableView];
            }
           
            
        }
        
    }
    else if (tableView.tag == 11) {
        for(UIView *view in [self.view subviews])
        {
            if(view.tag==20||view.tag==11)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
        switch(indexPath.row)
        {
            case 1:
            {
                NSString *phoneNumUrl =[NSString stringWithFormat:@"tel://%@",phoneNum];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumUrl]];
                break;
            }
            case 2:
            {
                NSString *phoneNumUrl =[NSString stringWithFormat:@"sms://%@",phoneNum];
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:phoneNumUrl]];
                break;
            }
            
        }

    }
    
}

#pragma mark - Target Action
- (void)infoTap:(UITapGestureRecognizer *)tap
{
    CGPoint point = [tap locationInView:tap.view];
    if (point.x > kDeviceWidth/2 && point.x < kDeviceWidth) {
        _selectedView.frame = CGRectMake(kDeviceWidth/2, 0, kDeviceWidth/2, 35);
        _isInfoSelected = NO;
        
    }else {
        _selectedView.frame = CGRectMake(0, 0, kDeviceWidth/2, 35);
        _isInfoSelected = YES;
    }
    
    [_tableView reloadData];
}

- (void)removeMaskViewAndTableView
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==20||view.tag==11)
        {
            [view removeFromSuperview];
            [view release], view = nil;
        }
    }
}

- (void)backButtonPressed
{
    if (_infoRequest) {
        [_infoRequest clearDelegatesAndCancel];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - HttpRequest

//op=clientDet&lastUpdateTime=2013-11-05 17:13:40&sid=3832bdf5_0912211631568490003
- (void)sendClientListRequest {
    if (_infoRequest) {
        [_infoRequest clearDelegatesAndCancel];
        _infoRequest = nil;
    }
    
    _infoRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[[KoronManager sharedManager] getObjectForKey:SERVERURL]]];
    _infoRequest.delegate = self;
    [_infoRequest setDidFinishSelector:@selector(clientInfoRequestFinish:)];
    [_infoRequest setDidFailSelector:@selector(clientInfoRequestFail:)];
    
    [_infoRequest setPostValue:[[KoronManager sharedManager] getObjectForKey:SID] forKey:SID];
    [_infoRequest setPostValue:@"clientInfo" forKey:@"op"];
    [_infoRequest setPostValue:self.infoClientId forKey:@"clientId"];
    [_infoRequest setPostValue:self.infoModuleId forKey:@"moduleId"];
    [_infoRequest setPostValue:@"" forKey:@"viewId"];
    

    
    [_infoRequest startAsynchronous];
}

- (void)clientInfoRequestFinish:(ASIFormDataRequest *)response
{
    
    NSLog(@"%@",[response responseString]);
    NSData *data = [response responseData];
    
    NSDictionary *clientInfo = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
//    NSLog(@"%@",[clientInfo description]);
    
    //公司信息
    NSArray *infoArray = [clientInfo objectForKey:@"info"];
    NSString *categoryString = nil;
    NSMutableArray *categoryArray = nil;
    for (NSInteger index = 0; index < infoArray.count; index++) {
        NSDictionary *dic = infoArray[index];
        KoronClientInfoModel *infoModel = [[KoronClientInfoModel alloc] initWithContent:dic];
        
        if (index == 0) {
            categoryString =infoModel.infoGroup;
            categoryArray = [[NSMutableArray alloc] init];
            [categoryArray addObject:infoModel];
            
        }else {
            if ([categoryString isEqualToString:infoModel.infoGroup]) {
                [categoryArray addObject:infoModel];
//                NSLog(@"1-----%@",infoModel.infoGroup);
            }else {
                [_infoArray addObject:categoryArray];
                [categoryArray release];
                
                categoryString = infoModel.infoGroup;
                categoryArray = [[NSMutableArray alloc] init];
                [categoryArray addObject:infoModel];
//                NSLog(@"2-----%@",infoModel.infoGroup);
            }
            
            if (index == infoArray.count-1) {
                [_infoArray addObject:categoryArray];
                [categoryArray release];
            }
            
        }
        
        [infoModel release];
    }
    
//    NSLog(@"%d",[_infoArray count]);
    
    //联系人信息
    NSArray *contactArray = [clientInfo objectForKey:@"contact"];
    for (NSArray *clientContact in contactArray) {
        for (NSDictionary *cDic in clientContact) {
            KoronClientContactModel *contactModel = [[KoronClientContactModel alloc] initWithContent:cDic];
            [_contactArray addObject:contactModel];
            [contactModel release];
        }
    }
    
    [_tableView reloadData];
}

- (void)clientInfoRequestFail:(ASIFormDataRequest *)response
{
    NSLog(@"clientInfoRequest error");
}



#pragma mark - Memory
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.infoClientId = nil;
    self.infoModuleId = nil;
    [_infoArray release], _infoArray = nil;
    [_contactArray release], _contactArray = nil;
    [_tableView release], _tableView = nil;
    [_selectedView release], _selectedView = nil;
    [_infoRequest release], _infoRequest = nil;
    [super dealloc];
}

@end
