//
//  KoronNewClientCell.m
//  moffice
//
//  Created by Mac Mini on 13-11-13.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronNewClientCell.h"

@implementation KoronNewClientCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    
}

- (void)setFrame:(CGRect)frame
{
    if (IOS_VERSIONS > 6.9) {
        frame.origin.x += 10;
        frame.size.width -= 20;
        [super setFrame:frame];
    }
    
    [super setFrame:frame];
}


@end
