//
//  categoryListViewController.m
//  moffice
//
//  Created by lijinhua on 13-6-13.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "categoryListViewController.h"
#import "JsonService.h"

@interface categoryListViewController ()

@end

@implementation categoryListViewController
@synthesize listView,listArray,listDetailArray,listContentArr,listTopidArr;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    [[JsonService sharedManager] cancelAllRequest];
    [self setHidesBottomBarWhenPushed:NO];
    [super viewDidDisappear:animated];
}


//异步接收到返回的数据
- (void)requestDataFinished:(id)jsondata
{
    NSLog(@"%@",jsondata);
    if(!isFirst)
    {
        if([jsondata isKindOfClass:[NSArray class]])
        {
            listArray=[[NSArray arrayWithArray:jsondata] retain];
        }
        isFirst=YES;
    }
    else
    {
         listDetailArray=[[NSArray arrayWithArray:jsondata] retain];
    }
    [self removeactivityAndbackview];
    [listView reloadData];
}

-(void)removeactivityAndbackview
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==405||view.tag==406)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}


-(void)dealloc
{
    [listArray release];
    [listDetailArray release];
    [listContentArr release];
    [listTopidArr release];
    [listView release];
    [super dealloc];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"选择板块";
    listContentArr=[[NSMutableArray alloc]initWithCapacity:0];
    [listContentArr removeAllObjects];
    listTopidArr=[[NSMutableArray alloc]initWithCapacity:0];
    [listTopidArr removeAllObjects];
    isFirst=NO;
    listArray=[[NSArray alloc]init];
    listDetailArray=[[NSArray alloc]init];
    //NSLog(@"[listArray count]=%d",[listArray count]);
    //NSLog(@"[listDetailArray count]=%d",[listDetailArray count]);
    
    listView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-44)];
    listView.delegate=self;
    listView.dataSource=self;
    [self.view addSubview:listView];
    
    UIView *backview=[[UIView alloc]initWithFrame:CGRectMake(110, self.view.frame.size.height/2-40, 100, 80)];
    [backview setBackgroundColor:[UIColor grayColor]];
    backview.layer.cornerRadius=5.0f;
    backview.tag=405;
    [self.view addSubview:backview];
    
    UIActivityIndicatorView *activity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activity.center=CGPointMake(160, self.view.frame.size.height/2);
    activity.tag=406;
    [activity startAnimating];
    [self.view addSubview:activity];
  
    
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice getListviewData];
    [jservice getListviewDetailData];
    

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (NSComparisonResult)compare:(NSDictionary *)otherDictionary
{
    NSDictionary *tempDictionary = (NSDictionary *)self;
    NSString *number1 = [[tempDictionary allKeys] objectAtIndex:0];
    NSString *number2 = [[otherDictionary allKeys] objectAtIndex:0];
    NSComparisonResult result = [number1 compare:number2 options:NSLiteralSearch];
    [number1 compare:number2 options:(NSCaseInsensitiveSearch)];
    return result == NSOrderedDescending; // 升序
    //    return result == NSOrderedAscending;  // 降序
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    NSLog(@"000=%d",[listArray count]);
    return [listArray count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    //NSLog(@"title=%@",[[listArray objectAtIndex:section] objectForKey:@"name"]);
    if ([listArray count]==0)
    {
        return nil;
    }
    else
    {
      return [[listArray objectAtIndex:section] objectForKey:@"name"];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int Num=0;
    for(int i=0;i<[listDetailArray count];i++)
    {
        NSString *string1=[[listDetailArray objectAtIndex:i] objectAtIndex:7];
        NSString *string2=[[listArray objectAtIndex:section]objectForKey:@"value"];
        if ([string1 isEqualToString:string2])
        {
            Num++;
            NSString *string3=[[listDetailArray objectAtIndex:i] objectAtIndex:0];
            if(string3!=nil&&![listContentArr containsObject:string3])
            {
                [listContentArr addObject:string3];
            }
        }
    }
    return Num;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    [listContentArr removeAllObjects];
    for(int i=0;i<[listDetailArray count];i++)
    {
        NSString *string1=[[listDetailArray objectAtIndex:i] objectAtIndex:7];
        NSString *string2=[[listArray objectAtIndex:indexPath.section]objectForKey:@"value"];
        if ([string1 isEqualToString:string2])
        {
            NSString *string3=[[listDetailArray objectAtIndex:i] objectAtIndex:0];
            if(string3!=nil&&![listContentArr containsObject:string3])
            {
                [listContentArr addObject:string3];
            }
        }
    }
    if([listContentArr count]!=0)
    {
        cell.textLabel.text=[listContentArr objectAtIndex:indexPath.row];
    }
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self tableView:tableView cellForRowAtIndexPath:indexPath];
    NSString *titleStr=cell.textLabel.text;
    for(int i=0;i<[listDetailArray count];i++)
    {
        NSString *string1=[[listDetailArray objectAtIndex:i] objectAtIndex:0];
        if ([string1 isEqualToString:titleStr])
        {
           NSString *topidStr=[[listDetailArray objectAtIndex:i] objectAtIndex:5];
           [delegate CallBack:[NSString stringWithFormat:@"%@",topidStr] other:titleStr];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}
@end
