//
//  KoronManager.m
//  Koron-mofffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronManager.h"

@implementation KoronManager

@synthesize isFirst;

+ (KoronManager *)sharedManager {
    static KoronManager *manager = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        manager = [[KoronManager alloc]init];
        manager.isFirst = YES;
    });
    
    return manager;
}

//数据保存到本地(封装NSUserDefaults)
- (void)saveObject:(id)value forKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//取出本地保存的数据
- (id)getObjectForKey:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}


#pragma mark - MD5
//字符串转为MD5(密码)
- (NSString * )getMD5:(NSString *)string
{
	const char *cStr = [string UTF8String];
	unsigned char result [16];
	CC_MD5( cStr, strlen(cStr), result );
	
	return [NSString
			stringWithFormat: @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1],
			result[2], result[3],
			result[4], result[5],
			result[6], result[7],
			result[8], result[9],
			result[10], result[11],
			result[12], result[13],
			result[14], result[15]
			];
}

-(BOOL)initUserDictionary:(NSDictionary *)dic
{
    @try {
        
        self->mdUser = [NSMutableDictionary dictionaryWithCapacity:10];
        NSArray *arr = [NSArray arrayWithArray:[dic objectForKey:@"list"]];
        for (id obj in arr) {
            KoronUser *user = [KoronUser koronUserWithNSDictionary:(NSDictionary *)obj];
            [self->mdUser setObject:user forKey:user.id];
        }
    }
    @catch (NSException *exception) {
        return FALSE;
    }
    @finally {
        
    }
    return TRUE;
}

-(KoronUser *)getUserById:(NSString *)userId {
    return [self->mdUser objectForKey:userId];
}

- (NSString *)getNameById:(NSString *)userId {
    KoronUser *user = [self->mdUser objectForKey:userId];
    return user.name;
}


@end
