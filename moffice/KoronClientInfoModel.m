//
//  KoronClientInfoModel.m
//  moffice
//
//  Created by Mac Mini on 13-11-12.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientInfoModel.h"

@implementation KoronClientInfoModel

- (id)mapAttributes
{
    NSDictionary *mapDic = @{
                             @"infoKey"    : @"Key",
                             @"infoVal"    : @"Val",
                             @"infoGroup"  : @"Group"
                             };
    return mapDic;
}

- (void)dealloc
{
    self.infoKey = nil;
    self.infoVal = nil;
    self.infoGroup = nil;
    [super dealloc];
}

@end
