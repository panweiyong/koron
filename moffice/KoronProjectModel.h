//
//  KoronProjectModel.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-26.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "BaseModel.h"

@interface KoronProjectModel : BaseModel<NSCoding>

@property (nonatomic, copy) NSString *projectId;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *executor;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *beginTime;
@property (nonatomic, copy) NSString *endTime;
@property (nonatomic, copy) NSString *logCount;
@property (nonatomic, copy) NSString *taskCount;
@property (nonatomic, copy) NSString *affix;
@property (nonatomic, copy) NSString *participantCount;

@end
