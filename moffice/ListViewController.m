//
//  ListViewController.m
//  moffice
//
//  Created by szsm on 12-2-3.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "ListViewController.h"


@implementation CustomerCell

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGSize size = CGSizeMake(120,200); 
    CGSize labelsize = [self.detailTextLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:11.0] constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping];
    
    self.textLabel.frame = CGRectMake(10,5,self.textLabel.frame.size.width,self.textLabel.frame.size.height);
    self.detailTextLabel.frame = CGRectMake(300-labelsize.width,5,self.detailTextLabel.frame.size.width,self.detailTextLabel.frame.size.height);
}

@end

@implementation ListViewController

@synthesize layout,moduleid,value1name,value2name,value3name,parameterKey ,parameterKey2,tableData;

-(void)dealloc
{

    [moduleid release];
    [value1name release];
    [value2name release];
    [value3name release];
    [parameterKey release];
    [parameterKey2 release];
    //[target release];
    [tableData release];
    [super dealloc];
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //CCLOG(@"%i",[self.tableData count]);
    return [self.tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCellStyle style=UITableViewCellStyleDefault;
    if (self.layout==1) {
        style = UITableViewCellStyleValue1;
    }else if(self.layout==2){
        style = UITableViewCellStyleValue2;
    }else if(self.layout==3){
        style = UITableViewCellStyleSubtitle;
    }else if(self.layout==4){
        style = UITableViewCellStyleValue1;
    }
    if(self.layout==4){
        CustomerCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
           cell = [[[CustomerCell alloc] initWithStyle:style reuseIdentifier:CellIdentifier] autorelease];
        }
    
    NSDictionary *data=[tableData objectAtIndex:indexPath.row];
    cell.textLabel.text = [data objectForKey:self.value1name];
    cell.detailTextLabel.text = [data objectForKey:self.value2name];
    // Configure the cell...
    cell.textLabel.font=  [UIFont systemFontOfSize:18];
    cell.detailTextLabel.font=  [UIFont systemFontOfSize:18];
    cell.textLabel.backgroundColor= [UIColor clearColor];
    cell.detailTextLabel.backgroundColor= [UIColor clearColor];
        
        [[cell.contentView viewWithTag:22] removeFromSuperview];
        UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(10,20,280,30)] autorelease];  
        label.tag = 22;
        [label setNumberOfLines:0];   //设置自动行数与字符换行
        label.lineBreakMode = UILineBreakModeCharacterWrap;
        label.backgroundColor=[UIColor clearColor];
        // 测试字串  
        //NSString *s = @"这是一个测试！！！adsfsaf时发生发勿忘我勿忘我勿忘我勿忘我勿忘我阿阿阿阿阿阿阿阿阿阿阿阿阿啊00000000阿什顿。。。";  
        UIFont *font = [UIFont fontWithName:@"Arial" size:18]; 
        [label setText:[data objectForKey:self.value3name]];
        [label setFont:font];
        //[label sizeToFit];
        //设置一个行高上限  
        CGSize size = CGSizeMake(280,2000);  
        //计算实际frame大小，并将label的frame变成实际大小 
        CGSize labelsize = [[data objectForKey:self.value3name] sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping]; 
        CGRect fra=label.frame;
        //fra.size.width =300;
        fra.size.height= labelsize.height;
        label.frame=fra;
        [cell.contentView addSubview:label];
        
        if( [ [tableData objectAtIndex:indexPath.row]objectForKey:@"target"] !=nil )
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue ;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:style reuseIdentifier:CellIdentifier] autorelease];
        }
        
        NSDictionary *data=[tableData objectAtIndex:indexPath.row];
        cell.textLabel.text = [data objectForKey:self.value1name];
        cell.detailTextLabel.text = [data objectForKey:self.value2name];
        // Configure the cell...
        cell.textLabel.font=  [UIFont boldSystemFontOfSize:20];
        cell.detailTextLabel.font=  [UIFont boldSystemFontOfSize:18];
        
        if( [ [tableData objectAtIndex:indexPath.row]objectForKey:@"target"] !=nil )
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.selectionStyle = UITableViewCellSelectionStyleBlue ;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;
    }
         
    return nil;
}
- (CGFloat)tableView:(UITableView *)tableViewz heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.layout==4){
    UITableViewCell *cell = (UITableViewCell *)[self tableView:tableViewz cellForRowAtIndexPath:indexPath];
    
    if(cell!=nil&&[[cell.contentView subviews ] count]>2){
        
        UIView *c=(UIView *)[[cell.contentView subviews ] objectAtIndex:2];
        //CCLOG(@"count:%i",[[cell.contentView subviews ] count]);
        if(c)return c.frame.size.height+35.0f;
    }else{
        return 35;
    }
    }
    return 55;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
NSString *mytitle;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if( [ [tableData objectAtIndex:indexPath.row]objectForKey:@"target"] ==nil )return;
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice loadModule:[[ tableData objectAtIndex:indexPath.row]objectForKey:@"target"] parameterName:self.parameterKey parameterValue:[[tableData objectAtIndex:indexPath.row]objectForKey:self.parameterKey] parameterName2:self.parameterKey2 parameterValue2:[[tableData objectAtIndex:indexPath.row]objectForKey:self.parameterKey2] mid:nil];
    mytitle = [[tableData objectAtIndex:indexPath.row]objectForKey:self.value1name];
}

- (void)requestDataFinished:(id)jsondata//request
{ 
    id rData = [jsondata retain];
    //CCLOG(@"%@",  tableData  );//传旨
    
    int viewtype= [[rData valueForKey:@"type"] intValue];//布局
    NSString *keyname =[rData valueForKey:@"parameterKey"];
    NSString *keyname2 =[rData valueForKey:@"parameterKey2"];
    CCLOG(@"%@",  [rData objectForKey:@"data"]  );
    if (viewtype==1) {
        //列表视图显示
        
        ListViewController *listviewc=[[ListViewController alloc] initWithStyle:UITableViewStylePlain];
        [listviewc setLayout:[[rData valueForKey:@"layout"] intValue]];
        [listviewc setModuleid:self.moduleid];
        [listviewc setParameterKey:keyname];
        [listviewc setParameterKey2:keyname2];
        [listviewc setTableData:[[rData objectForKey:@"data"]retain]]; 
        [listviewc setTitle:mytitle];
        [listviewc setValue1name:[rData objectForKey:@"valuename1"]];
        [listviewc setValue2name:[rData objectForKey:@"valuename2"]];
        [listviewc setValue3name:[rData objectForKey:@"valuename3"]];
        [self.navigationController pushViewController:listviewc animated:YES];
        [listviewc release];
        
    }else if(viewtype==2){
        
        DetailsViewController *dviewc =[[DetailsViewController alloc]initWithStyle:UITableViewStyleGrouped];
        [dviewc setTableData:[[rData objectForKey:@"data"]retain] ]; 
        [dviewc setTitle:mytitle];
        [dviewc setModuleid:self.moduleid];
        [self.navigationController pushViewController:dviewc animated:YES];
        [dviewc release];
        
    }

}

@end
