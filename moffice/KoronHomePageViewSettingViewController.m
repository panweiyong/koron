//
//  KoronHomePageViewSettingViewController.m
//  moffice
//
//  Created by CA on 13-11-3.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronHomePageViewSettingViewController.h"
#import "HomePageOptionCell.h"

@interface KoronHomePageViewSettingViewController ()

@end

@implementation KoronHomePageViewSettingViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.title = @"编辑";
        
        NSArray *homePageArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"HOMEPAGEARRAY"];
        NSArray *homePageImageArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"HOMEPAGEIMAGEARRAY"];
        NSArray *isOptionSelectedArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"ISOPTIONSELECTEDARRAY"];

        if (homePageArray.count == 0 || homePageImageArray.count == 0 || isOptionSelectedArray.count == 0) {
            _homePageArray = [[NSMutableArray alloc] initWithObjects:@"通讯录",@"工作计划",@"客户列表",@"销售跟单",@"客户移交",@"销售出库",@"发帖",@"工作审批",@"产品知识库",@"行程轨迹",@"待办",@"我的项目", nil];
            _homePageImageArray = [[NSMutableArray alloc] initWithObjects:@"icon_contact.png",@"forum_test.png",@"icon_client.png",@"icon_sales_logistics.png",@"icon_transfer.png",@"icon_stock_removal.png",@"forum_test.png",@"icon_workflow.png",@"icon_information.png",@"icon_information.png",@"icon_information.png",@"icon_information.png",nil];
            _isOptionSelectedArray = [[NSMutableArray alloc] initWithCapacity:10];
            for (NSInteger index = 0; index < 12; index++) {
                NSNumber *selectedOption = [NSNumber numberWithBool:YES];
                [_isOptionSelectedArray addObject:selectedOption];
            }
        }else {
            _homePageArray = [[NSMutableArray alloc] initWithCapacity:10];
            for (NSString *homePage in homePageArray) {
                [_homePageArray addObject:homePage];
            }
            
            _homePageImageArray = [[NSMutableArray alloc] initWithCapacity:10];
            for (NSString *homePageImage in homePageImageArray) {
                [_homePageImageArray addObject:homePageImage];
            }
            
            _isOptionSelectedArray = [[NSMutableArray alloc] initWithCapacity:10];
            for (NSString *optionSelected in isOptionSelectedArray) {
                [_isOptionSelectedArray addObject:optionSelected];
            }
        }
        
        
        
       
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (IOS_VERSIONS > 6.9) {
        
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadNavigationItem];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    self.tableView.rowHeight = 60;
    self.tableView.showsVerticalScrollIndicator = NO;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return _homePageImageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    HomePageOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[HomePageOptionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        UILongPressGestureRecognizer *tap = [[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(beginMoveCell:)] autorelease];
        tap.minimumPressDuration = 1;
        [cell addGestureRecognizer:tap];
    }
    
//    cell.imageView.image = [UIImage imageNamed:_homePageImageArray[indexPath.row]];
//    cell.textLabel.text = _homePageArray[indexPath.row];
    
    cell.iconView.image = [UIImage imageNamed:_homePageImageArray[indexPath.row]];
    cell.iconLabel.text = _homePageArray[indexPath.row];
    if ([_isOptionSelectedArray[indexPath.row] boolValue]) {
        cell.iconSelectedView.image = [UIImage imageNamed:@"icon_check_tag_highlighted"];
    }else {
        cell.iconSelectedView.image = [UIImage imageNamed:@"btn_add"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    HomePageOptionCell *cell = (HomePageOptionCell *)[tableView cellForRowAtIndexPath:indexPath];
    if ([_isOptionSelectedArray[indexPath.row] boolValue]) {
        cell.iconSelectedView.image = [UIImage imageNamed:@"btn_add"];
        [_isOptionSelectedArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:NO]];
    }else {
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.iconSelectedView.image = [UIImage imageNamed:@"icon_check_tag_highlighted"];
        [_isOptionSelectedArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:YES]];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    return UITableViewCellEditingStyleNone;
    
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSString *optionName = [[_homePageArray objectAtIndex:fromIndexPath.row] retain]; // 2
    [_homePageArray removeObjectAtIndex:fromIndexPath.row];
    [_homePageArray insertObject:optionName atIndex:toIndexPath.row];
    [optionName release];
    NSLog(@"%@",[_homePageArray description]);
    
    NSString *optionImage = [[_homePageImageArray objectAtIndex:fromIndexPath.row] retain]; // 2
    [_homePageImageArray removeObjectAtIndex:fromIndexPath.row];
    [_homePageImageArray insertObject:optionImage atIndex:toIndexPath.row];
    [optionImage release];
    
    NSNumber *optionSelected = [[_isOptionSelectedArray objectAtIndex:fromIndexPath.row] retain]; // 2
    [_isOptionSelectedArray removeObjectAtIndex:fromIndexPath.row];
    [_isOptionSelectedArray insertObject:optionSelected atIndex:toIndexPath.row];
    [optionSelected release];
    
    [self setEditing:NO animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{

    return YES;
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
    
//    UIBarButtonItem *confirmItem = [[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleBordered target:self action:@selector(confirmOrder)];
//    [confirmItem setTintColor:[UIColor blackColor]];
//    self.navigationItem.rightBarButtonItem = confirmItem;
//    [confirmItem release];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    UIImage *image = [[UIImage imageNamed:@"btn_big_gray"] stretchableImageWithLeftCapWidth:5 topCapHeight:10];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTitle:@"确定" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [button addTarget:self action:@selector(confirmOrder) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [button release];
    self.navigationItem.rightBarButtonItem = rightItem;
    [rightItem release];
    
//    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)beginMoveCell:(UILongPressGestureRecognizer *)tap {
    
    NSLog(@"longTap");
    [self setEditing:YES animated:YES];
}

- (void)confirmOrder {
    [[NSUserDefaults standardUserDefaults] setObject:_homePageArray forKey:@"HOMEPAGEARRAY"];
    [[NSUserDefaults standardUserDefaults] setObject:_homePageImageArray forKey:@"HOMEPAGEIMAGEARRAY"];
    [[NSUserDefaults standardUserDefaults] setObject:_isOptionSelectedArray forKey:@"ISOPTIONSELECTEDARRAY"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"M-Office" message:@"保存成功" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
//    [alertView show];
//    [alertView release];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"HOMEPAGE_VIEW_CHANGE" object:nil];

    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)backButtonPressed {
   
    [self.navigationController dismissModalViewControllerAnimated:YES];
    
    
}

- (void)dealloc
{
    [_homePageArray release],_homePageArray = nil;
    [_homePageImageArray release],_homePageImageArray = nil;
    [super dealloc];
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:18.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

@end
