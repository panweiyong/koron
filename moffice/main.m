//
//  main.m
//  moffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        @try {
            return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
        }
        @catch (NSException *exception) {
            NSLog(@"callStackReturnAddresses    %@",[exception callStackReturnAddresses]);
            NSLog(@"callStackSymbols    %@",[exception callStackSymbols]);
        }
        @finally {
            
        }
    }
}
