//
//  SMAlertView.h
//  ChildrenCalendar
//
//  Created by yangxi zou on 11-1-23.
//  Copyright 2011 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface SMView : NSObject {

}

+(UIView *)showAlert:(NSString*)msg;

+(UIView *)showWaitingAlert;

+(UIActivityIndicatorView *)showProcessing;

@end
