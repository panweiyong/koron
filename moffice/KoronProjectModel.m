//
//  KoronProjectModel.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-26.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectModel.h"

@implementation KoronProjectModel

- (id)mapAttributes
{
    NSDictionary *mapDic = @{
                             @"projectId"           : @"id",
                             @"title"               : @"title",
                             @"executor"            : @"executor",
                             @"status"              : @"status",
                             @"remark"              : @"remark",
                             @"beginTime"           : @"beginTime",
                             @"endTime"             : @"endTime",
                             @"logCount"            : @"logCount",
                             @"taskCount"           : @"taskCount",
                             @"affix"               : @"affix",
                             @"participantCount"    : @"participant"
                             };
    return mapDic;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.projectId = [aDecoder decodeObjectForKey:@"ProjectId"];
        self.title = [aDecoder decodeObjectForKey:@"Title"];
        self.executor = [aDecoder decodeObjectForKey:@"Executor"];
        self.status = [aDecoder decodeObjectForKey:@"Remark"];
        self.beginTime = [aDecoder decodeObjectForKey:@"BeginTime"];
        self.endTime = [aDecoder decodeObjectForKey:@"EndTime"];
        self.logCount = [aDecoder decodeObjectForKey:@"LogCount"];
        self.taskCount = [aDecoder decodeObjectForKey:@"TaskCount"];
        self.affix = [aDecoder decodeObjectForKey:@"Affix"];
        self.participantCount = [aDecoder decodeObjectForKey:@"ParticipantCount"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.projectId forKey:@"ProjectId"];
    [aCoder encodeObject:self.title forKey:@"Title"];
    [aCoder encodeObject:self.executor forKey:@"Executor"];
    [aCoder encodeObject:self.status forKey:@"Remark"];
    [aCoder encodeObject:self.beginTime forKey:@"BeginTime"];
    [aCoder encodeObject:self.endTime forKey:@"EndTime"];
    [aCoder encodeObject:self.logCount forKey:@"LogCount"];
    [aCoder encodeObject:self.taskCount forKey:@"TaskCount"];
    [aCoder encodeObject:self.affix forKey:@"Affix"];
    [aCoder encodeObject:self.participantCount forKey:@"ParticipantCount"];
}

- (void)dealloc
{
    
    self.projectId = nil;
    self.title = nil;
    self.executor = nil;
    self.status = nil;
    self.remark = nil;
    self.beginTime = nil;
    self.endTime = nil;
    self.logCount = nil;
    self.taskCount = nil;
    self.affix = nil;
    self.participantCount = nil;
    [super dealloc];
}

@end
