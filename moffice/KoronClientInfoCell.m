//
//  KoronClientInfoCell.m
//  moffice
//
//  Created by Mac Mini on 13-11-12.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientInfoCell.h"

@implementation KoronClientInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews
{
    _keyLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _keyLabel.backgroundColor = [UIColor clearColor];
    _keyLabel.font = [UIFont boldSystemFontOfSize:16];
    [self.contentView addSubview:_keyLabel];
    
    _valLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _valLabel.backgroundColor = [UIColor clearColor];
    _valLabel.font = [UIFont systemFontOfSize:14];
//    _valLabel.adjustsFontSizeToFitWidth = YES;
    _valLabel.numberOfLines = 0;
    
    [self.contentView addSubview:_valLabel];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    _keyLabel.frame = CGRectMake(10, 5, 70, 40);
    [_keyLabel sizeToFit];
    [_keyLabel setCenter:CGPointMake(10+_keyLabel.width/2.0, self.height/2)];
    _valLabel.frame = CGRectMake(_keyLabel.right+5, 5, 200, 40);
    CGSize valSize = CGSizeMake(200, 1000);
    CGSize labelsize = [_valLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:14.0] constrainedToSize:valSize lineBreakMode:NSLineBreakByClipping];
    _valLabel.frame = CGRectMake(_keyLabel.right+5, 5, 200, labelsize.height);
    _valLabel.center = CGPointMake(_keyLabel.right+5+_valLabel.width/2.0, self.height/2.0);
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame {
    if (IOS_VERSIONS > 6.9) {
        frame.origin.x += 10;
        frame.size.width -= 20;
        [super setFrame:frame];
    }
    
    [super setFrame:frame];
}

- (void)dealloc
{

    [_keyLabel release], _keyLabel = nil;
    [_valLabel release], _valLabel = nil;
    [super dealloc];
}

@end
