//
//  WorkflowCell.h
//  moffice
//
//  Created by yangxi zou on 12-1-12.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SMUtils.h"
#import "CommonConst.h"

@interface Workflow : UITableViewCell
{
    NSString *oid;
    NSString *createTime;
    NSString *creator;
    NSString *title;
    NSString *type;
    NSString *currentNode;
    NSInteger flag;
    BOOL cancelEnabled; 
}

@property (nonatomic,retain)NSString *oid;
@property (nonatomic,retain)NSString *createTime;
@property (nonatomic,retain)NSString *creator;
@property (nonatomic,retain)NSString *title;
@property (nonatomic,retain)NSString *type;
@property (nonatomic,retain)NSString *currentNode;
@property (nonatomic)NSInteger flag;
@property (nonatomic)BOOL cancelEnabled;;
 
 

@end


@interface WorkflowCell : Workflow
{
    UIView *cellContentView;
}

@end