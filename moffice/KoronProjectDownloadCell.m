//
//  KoronProjectDownloadCell.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-28.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectDownloadCell.h"

@implementation KoronProjectDownloadCell
{
    UIView *_frameView;
    UIActivityIndicatorView *_indicatorView;
    UILabel *_statusLabel;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews
{
    _frameView = [[UIView alloc] initWithFrame:CGRectZero];
    _frameView.layer.borderWidth = 1;
    _frameView.layer.borderColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1].CGColor;
    [self.contentView addSubview:_frameView];

    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectZero];
    _indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [self.contentView addSubview:_indicatorView];
    
    _statusLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _statusLabel.text = @"加载数据中...";
    _statusLabel.textColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
    _statusLabel.font = [UIFont systemFontOfSize:12];
    [self.contentView addSubview:_statusLabel];
}

- (void)startActivityIndicatorView
{
    [_indicatorView startAnimating];
}

- (void)stopActivityIndicatorView
{
    [_indicatorView stopAnimating];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _frameView.frame = CGRectMake(0, 0, self.width, self.height);
    
    _indicatorView.frame = CGRectMake(self.width/3.0, self.height/2.0 - 10, 20, 20);
    _statusLabel.frame = CGRectMake(self.width/2.0 - self.width/20.0, self.height/2.0 - 10, 70, 20);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

- (void)setFrame:(CGRect)frame
{
    
    frame.origin.x += 10;
    frame.size.width -= 20;
    [super setFrame:frame];
    
}

@end
