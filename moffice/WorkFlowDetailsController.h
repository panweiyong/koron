//
//  WorkFlowDetailsController.h
//  moffice
//
//  Created by yangxi zou on 12-1-14.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CommonConst.h"
#import "JsonService.h"
#import "AbstractViewController.h"
#import "ImageViewController.h"
#import "WorkFlowActionController.h"
#import "SMUtils.h"
#import "ModalAlert.h"
#import "DTAttributedTextView.h"
#import "DTAttributedTextContentView.h"

#import "DTLazyImageView.h"

#import <QuartzCore/QuartzCore.h>

@interface TABLEHeader : UIView {

    NSString * creator;
    NSString * createDate;
    NSString * type;

}

-(CGFloat)getTotalHeight;

@property (nonatomic,retain)NSString *creator;
@property (nonatomic,retain)NSString *createDate;
@property (nonatomic,retain)NSString *type;
@end


//,DTAttributedTextContentViewDelegate 
@interface WorkFlowDetailsController : AbstractViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSString *oid;
    UIView *footerView;
    TABLEHeader *headerView;
    NSString *tipsid;
    NSMutableDictionary *contentViewCache;

 
} 
@property (nonatomic,retain)NSString *oid;
@property (nonatomic,retain)NSString *tipsid;
@end
