//
//  KoronLoginViewController.m
//  Koron-mofffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//   c79617691d49d23525c9b0578d89a17fe8b8d25c

#import "KoronLoginViewController.h"

@interface KoronLoginViewController ()

@end

@implementation KoronLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:@"UIKeyboardWillShowNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:@"UIKeyboardWillHideNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeAnimationImageView) name:@"removeAnimationImageView" object:nil];
        
        if (IOS_VERSIONS < 7.0) {
            CGRect rect = self.view.frame;
            rect.origin.y = rect.origin.y - 20;
            _rect = rect;
        }else {
            _rect = self.view.frame;
        }
        
        //自动登录
        if ([[_manager getObjectForKey:AUTOMATICLOGIN] isEqualToString:@"YES"]) {
            [self loginRequest];
        }
        isKeyBoardHide = YES;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _manager = [KoronManager sharedManager];
    
    CGRect rect = [[UIScreen mainScreen] bounds];

    _backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"login_bg.png"]];
    [_backgroundView setFrame:rect];
    _backgroundView.userInteractionEnabled = YES;
    [self.view addSubview:_backgroundView];
    
    
    UIImageView *logoImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"login_logo.png"]];
    [logoImageView setFrame:CGRectMake(0, 0, 100, 100)];
    [logoImageView setCenter:CGPointMake(_backgroundView.bounds.size.width/2, _backgroundView.bounds.size.height/2 - 140)];
    [self.view addSubview:logoImageView];
    
    
//    _loginView = [[KoronLoginView alloc]initWithFrame:CGRectMake(0, 0, 240, 60)];
    _loginView = [[KoronLoginView alloc]initWithFrame:CGRectMake(0, 5, 240, 80)];
    [_loginView setCenter:CGPointMake(_backgroundView.bounds.size.width/2, _backgroundView.bounds.size.height/2 - _loginView.bounds.size.height/2+10)];
    _loginView.backgroundColor = [UIColor whiteColor];
    _loginView.layer.masksToBounds = YES;
    [_loginView.layer setCornerRadius:8.0];
    [_loginView.layer setBorderWidth:1.0];
    [_loginView.layer setBorderColor:[UIColor colorWithRed:211/255.0 green:224/255.0 blue:231/255.0 alpha:1].CGColor];
    [self.view addSubview:_loginView];
    
    _loginButton = [[UIButton alloc]initWithFrame:CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y + _loginView.frame.size.height + 10+5, _loginView.frame.size.width, 30)];
    _loginButton.backgroundColor = [UIColor colorWithRed:104/255.0 green:203/255.0 blue:252/255.0 alpha:1];
    [_loginButton setTitle:@"登   录" forState:UIControlStateNormal];
    [_loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_loginButton addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
    [_loginButton addTarget:self action:@selector(loginTouchDown:) forControlEvents:UIControlEventTouchDown];
    [_loginButton addTarget:self action:@selector(loginTouchDragOutside:) forControlEvents:UIControlEventTouchDragOutside];
    [self.view addSubview:_loginButton];
    
    _automaticLoginView = [[KoronChooseView alloc]initWithFrame:CGRectMake(_loginButton.frame.origin.x, _loginButton.frame.origin.y + _loginButton.frame.size.height + 10, _loginButton.frame.size.width / 2, 20) type:AUTOMATICLOGIN title:@"自动登陆"];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onChangeAutomaticLoginView)];
    [_automaticLoginView addGestureRecognizer:gesture];
    [self.view addSubview:_automaticLoginView];
    
    _rememberPasswordView = [[KoronChooseView alloc]initWithFrame:CGRectMake(_loginButton.frame.origin.x + _automaticLoginView.bounds.size.width, _loginButton.frame.origin.y + _loginButton.frame.size.height + 10, _loginButton.frame.size.width / 2, 20) type:REMEMBERPASSWORD title:@"记住密码"];
    UITapGestureRecognizer *gesture2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onChangeRememberPasswordView)];
    [_rememberPasswordView addGestureRecognizer:gesture2];
    [self.view addSubview:_rememberPasswordView];
    
    _serverView = [[KoronServerView alloc]initWithFrame:CGRectMake(_loginView.frame.origin.x, _backgroundView.bounds.size.height - 60, _loginView.bounds.size.width, 60)];
    [self.view addSubview:_serverView];

    if (_manager.isFirst) {
        _startAnimationView = [[KoronStartAnimationViewController alloc]init];
        [self.view addSubview:_startAnimationView.view];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//动画效果结束 移除动画页面
- (void)removeAnimationImageView {
    [_startAnimationView.view removeFromSuperview];
    _startAnimationView = nil;
}


//登陆
- (void)login:(UIButton *)btn {
    btn.backgroundColor = [UIColor colorWithRed:104/255.0 green:203/255.0 blue:252/255.0 alpha:1];
    
    if ([@"" compare:[_serverView.textField text]] == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入服务器地址" duration:1];
        return;
    }
    if ([@"" compare:[_loginView.userNameTextField text]] == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入用户名" duration:1];
        return;
    }
    if ([@"" compare:[_loginView.passwordTextField text]] == 0) {
        [SVProgressHUD showErrorWithStatus:@"请输入密码" duration:1];
        return;
    }
    [self loginRequest];
}

//按下登陆按钮变色
- (void)loginTouchDown:(UIButton *)btn {
    btn.backgroundColor = [UIColor colorWithRed:72/255.0 green:165/255.0 blue:249/255.0 alpha:1];
}

//手指离开登陆按钮变色
- (void)loginTouchDragOutside:(UIButton *)btn {
    btn.backgroundColor = [UIColor colorWithRed:104/255.0 green:203/255.0 blue:252/255.0 alpha:1];
}



- (void)onChangeAutomaticLoginView {
    [_automaticLoginView setStatus:!_automaticLoginView.isChoose];
    if (_automaticLoginView.isChoose) {
        [_rememberPasswordView setStatus:YES];
    }
}

- (void)onChangeRememberPasswordView {
    [_rememberPasswordView setStatus:!_rememberPasswordView.isChoose];
    if (!_rememberPasswordView.isChoose) {
        [_automaticLoginView setStatus:NO];
    }
}

#pragma mark - touchesBegan

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    for (UIView *textFiled in _loginView.subviews) {
        if ([textFiled isKindOfClass:[UITextField class]]) {
            [textFiled resignFirstResponder];
        }
    }
    [_serverView.textField resignFirstResponder];
    
}

#pragma mark - UITextFieldDelegate


#pragma mark - keyboardWillShowNotification,keyboardWillHideNotification

- (void)keyboardWillShowNotification:(NSNotification *)note {
    if (isKeyBoardHide) {
        [UIView animateWithDuration:0.25 animations:^{
            CGRect rect = _rect;
            rect.origin.y = rect.origin.y - 200;
            [self.view setFrame:rect];
            if (IOS_VERSIONS < 7.0) {
                [_loginView setFrame:CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y + 50, _loginView.frame.size.width, _loginView.frame.size.height)];
                [_loginButton setFrame:CGRectMake(_loginButton.frame.origin.x, _loginButton.frame.origin.y + 50, _loginButton.frame.size.width, _loginButton.frame.size.height)];
                [_automaticLoginView setFrame:CGRectMake(_automaticLoginView.frame.origin.x, _automaticLoginView.frame.origin.y + 50, _automaticLoginView.frame.size.width, _automaticLoginView.frame.size.height)];
                [_rememberPasswordView setFrame:CGRectMake(_rememberPasswordView.frame.origin.x, _rememberPasswordView.frame.origin.y + 50, _rememberPasswordView.frame.size.width, _rememberPasswordView.frame.size.height)];
                [_serverView setFrame:CGRectMake(_serverView.frame.origin.x, _serverView.frame.origin.y + 10, _serverView.frame.size.width, _serverView.frame.size.height)];
            }else {
                [_loginView setFrame:CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y + 50, _loginView.frame.size.width, _loginView.frame.size.height)];
                [_loginButton setFrame:CGRectMake(_loginButton.frame.origin.x, _loginButton.frame.origin.y + 50, _loginButton.frame.size.width, _loginButton.frame.size.height)];
                [_automaticLoginView setFrame:CGRectMake(_automaticLoginView.frame.origin.x, _automaticLoginView.frame.origin.y + 50, _automaticLoginView.frame.size.width, _automaticLoginView.frame.size.height)];
                [_rememberPasswordView setFrame:CGRectMake(_rememberPasswordView.frame.origin.x, _rememberPasswordView.frame.origin.y + 50, _rememberPasswordView.frame.size.width, _rememberPasswordView.frame.size.height)];
            }
        }];
        isKeyBoardHide = NO;
    }
}

- (void)keyboardWillHideNotification:(NSNotification *)note {

    [UIView animateWithDuration:0.25 animations:^{
        [self.view setFrame:_rect];
        if (IOS_VERSIONS < 7.0) {
            [_loginView setFrame:CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y - 50, _loginView.frame.size.width, _loginView.frame.size.height)];
            [_loginButton setFrame:CGRectMake(_loginButton.frame.origin.x, _loginButton.frame.origin.y - 50, _loginButton.frame.size.width, _loginButton.frame.size.height)];
            [_automaticLoginView setFrame:CGRectMake(_automaticLoginView.frame.origin.x, _automaticLoginView.frame.origin.y - 50, _automaticLoginView.frame.size.width, _automaticLoginView.frame.size.height)];
            [_rememberPasswordView setFrame:CGRectMake(_rememberPasswordView.frame.origin.x, _rememberPasswordView.frame.origin.y - 50, _rememberPasswordView.frame.size.width, _rememberPasswordView.frame.size.height)];
            [_serverView setFrame:CGRectMake(_serverView.frame.origin.x, _serverView.frame.origin.y - 10, _serverView.frame.size.width, _serverView.frame.size.height)];
        }else {
            [_loginView setFrame:CGRectMake(_loginView.frame.origin.x, _loginView.frame.origin.y - 50, _loginView.frame.size.width, _loginView.frame.size.height)];
            [_loginButton setFrame:CGRectMake(_loginButton.frame.origin.x, _loginButton.frame.origin.y - 50, _loginButton.frame.size.width, _loginButton.frame.size.height)];
            [_automaticLoginView setFrame:CGRectMake(_automaticLoginView.frame.origin.x, _automaticLoginView.frame.origin.y - 50, _automaticLoginView.frame.size.width, _automaticLoginView.frame.size.height)];
            [_rememberPasswordView setFrame:CGRectMake(_rememberPasswordView.frame.origin.x, _rememberPasswordView.frame.origin.y - 50, _rememberPasswordView.frame.size.width, _rememberPasswordView.frame.size.height)];
        }
    }];
    isKeyBoardHide = YES;
    
}


#pragma mark - LoginRequestDelegate

- (void)loginRequest {
    
    [SVProgressHUD showWithStatus:@"登录中，请稍候..." maskType:SVProgressHUDMaskTypeBlack];
    
    [_loginRequest clearDelegatesAndCancel];
    _loginRequest = nil;
    _loginRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/moffice",[_serverView.textField text]]]];
    _loginRequest.delegate = self;
    [_loginRequest setDidFinishSelector:@selector(loginResponse:)];
    [_loginRequest setDidFailSelector:@selector(loginFail:)];
    [_loginRequest setPostValue:@"auth" forKey:@"op"];
    [_loginRequest setPostValue:_loginView.userNameTextField.text forKey:@"username"];
    [_loginRequest setPostValue:[_manager getMD5:_loginView.passwordTextField.text] forKey:@"password"];
    
    
    NSLog(@"username  %@   password  %@   url %@",_loginView.userNameTextField.text,[_manager getMD5:_loginView.passwordTextField.text],[_manager getObjectForKey:SERVERURL]);
    
    
    [_loginRequest startAsynchronous];
}

- (void)loginResponse:(ASIFormDataRequest *)respons {
    SBJsonParser *parser = [[SBJsonParser alloc]init];
    NSDictionary *dictionary = [parser objectWithString:[respons responseString]];
    NSLog(@"%@",[respons responseString]);
    if ([[dictionary objectForKey:@"userid"] isEqualToString:@""] || nil == [dictionary objectForKey:@"userid"]){
        NSLog(@"登录失败");
        [SVProgressHUD dismissWithError:[dictionary objectForKey:@"desc"]];
        return;
    }
    NSLog(@"---dictionary:%@",dictionary);
    [_manager saveObject:[dictionary objectForKey:@"userid"] forKey:SID];
    [_manager saveObject:[dictionary objectForKey:@"userName"] forKey:NAME];
    
    [_manager saveObject:[_serverView.textField text] forKey:SERVER];
    [_manager saveObject:[NSString stringWithFormat:@"http://%@/moffice",[_manager getObjectForKey:SERVER]] forKey:SERVERURL];
    
    [_manager saveObject:[_loginView.userNameTextField text] forKey:USERNAME];
    [_manager saveObject:[_loginView.passwordTextField text] forKey:PASSWORD];
    
    [_manager saveObject:_rememberPasswordView.isChoose ? @"YES" : @"NO" forKey:REMEMBERPASSWORD];
    [_manager saveObject:_automaticLoginView.isChoose ? @"YES" : @"NO" forKey:AUTOMATICLOGIN];
    
    [self loadUserRequest];
}

- (void)loginFail:(ASIFormDataRequest *)response {
    [SVProgressHUD dismissWithError:@"登录失败,请检查网络."];
}


- (void)loginSucceed {
}


- (void)loadUserRequest {
    
    [_loginRequest clearDelegatesAndCancel];
    _loginRequest = nil;
    _loginRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[_manager getObjectForKey:SERVERURL]]];
    _loginRequest.delegate = self;
    [_loginRequest setDidFinishSelector:@selector(loadUserResponse:)];
    [_loginRequest setDidFailSelector:@selector(loginFail:)];
    [_loginRequest setPostValue:@"chatUser" forKey:@"op"];
    [_loginRequest setPostValue:@"" forKey:@"lastUpdateTime"];
    [_loginRequest setPostValue:[_manager getObjectForKey:SID] forKey:@"sid"];
    
    
    [_loginRequest startAsynchronous];
}

- (void)loadUserResponse : (ASIFormDataRequest *)respons {
    
    SBJsonParser *parser = [[SBJsonParser alloc]init];
    NSDictionary *dictionary = [parser objectWithString:[respons responseString]];
    NSLog(@"%@",[respons responseString]);
    if ([[dictionary objectForKey:@"code"] integerValue] < 0){
        NSLog(@"获取职员信息失败");
        [SVProgressHUD dismissWithError:[dictionary objectForKey:@"desc"]];
        return;
    }
    [_manager initUserDictionary:dictionary];
    [SVProgressHUD dismissWithSuccess:@"登录成功"];
    NSLog(@"---dictionary:%@",dictionary);
    _manager.isFirst = NO;
        
    KoronRootViewController *root = [[KoronRootViewController alloc]init];
    [self presentViewController:root animated:YES completion:nil];

}

@end
