//
//  KoronHomePageTopView.h
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "KoronManager.h"
#import "UIImageView+WebCache.h"
#import "KoronManager.h"

@interface KoronHomePageTopView : UIView {
    UIImageView *_backgroundView;
    //头像
    UIImageView *_iconView;
    //用户名
    UILabel *_userNameLabel;
    //签名
    UILabel *_signatureLabel;
    //设置
    UIButton *_settingButton;
    
    KoronManager *_manager;
}

@end
