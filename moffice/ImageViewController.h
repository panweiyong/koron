//
//  ImageViewController.h
//  moffice
//
//  Created by yangxi zou on 12-1-11.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UINavigationBar+Helper.h"

@interface ImageViewController : UIViewController //<UIGestureRecognizerDelegate>
{
    UIImage * displayImage;
    UIImageView *iviewer;
    CGFloat lastScale ; 
    CGFloat _lastRotation;
    CGFloat _firstX;
    CGFloat _firstY;
}

@property (nonatomic,retain)UIImage * displayImage;

@end
