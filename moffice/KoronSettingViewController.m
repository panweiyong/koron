//
//  KoronSettingViewController.m
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronSettingViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface KoronSettingViewController ()


@end

@implementation KoronSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.view.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.1];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self loadNavigationItem];
    
    [self loadAccountView];
    
}

- (void)loadAccountView
{
    _manager = [KoronManager sharedManager];
    NSLog(@"--%@--",[_manager getObjectForKey:@"SettingUrl"]);
    if ([[_manager getObjectForKey:REMEMBERPASSWORD] isEqualToString:@"YES"]) {
        _isPasswordOn = YES;
    }else {
        _isPasswordOn = NO;
    }
    
    if ([[_manager getObjectForKey:AUTOMATICLOGIN] isEqualToString:@"YES"]) {
        _isAutoLoginOn = YES;
    }else {
        _isAutoLoginOn = NO;
    }
    
    UILabel *accountInfo = [[UILabel alloc] initWithFrame:CGRectMake(10, 15, 100, 20)];
    accountInfo.text = @"账号信息";
    accountInfo.backgroundColor = [UIColor clearColor];
    [self.view addSubview:accountInfo];
    [accountInfo release];
    
    UIView *accountView = [[UIView alloc] initWithFrame:CGRectMake(10, 50, 300, 200)];
    accountView.backgroundColor = [UIColor whiteColor];
    accountView.layer.cornerRadius = 15;
    [self.view addSubview:accountView];
    [accountView release];
    
    //头像
    UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];
    iconView.layer.cornerRadius = 5;
    iconView.clipsToBounds = YES;
    [accountView addSubview:iconView];
    
    NSString *headURL = [NSString stringWithFormat:@"http://%@/ReadFile?mobile=true&tempFile=&type=PIC&tableName=tblEmployee&fileName=mobile\\%@.jpg",[_manager getObjectForKey:SERVER],[_manager getObjectForKey:SID]];
    headURL = [headURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [iconView setImageWithURL:[NSURL URLWithString:headURL] placeholderImage:[UIImage imageNamed:@"default_head.jpg"]];
    [iconView release];
    
    //账号
    UILabel *accountLabel = [[UILabel alloc] initWithFrame:CGRectMake(iconView.right+5, iconView.top+10, 150, 30)];
    accountLabel.backgroundColor = [UIColor clearColor];
    accountLabel.textColor = [UIColor blackColor];
    accountLabel.text = [_manager getObjectForKey:NAME];
    [accountView addSubview:accountLabel];
    [accountLabel release];
    
    //网址
    UILabel *urlLabel = [[UILabel alloc] initWithFrame:CGRectMake(iconView.right+5, iconView.bottom-30, 200, 30)];
    urlLabel.textColor = [UIColor blackColor];
    urlLabel.backgroundColor = [UIColor clearColor];
    urlLabel.text = [_manager getObjectForKey:@"SettingUrl"];
    //    urlLabel.text = [NSString stringWithFormat:@"%@",[_manager getObjectForKey:SERVERURL]];
    [accountView addSubview:urlLabel];
    [urlLabel release];
    
    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, iconView.bottom+10, accountView.width, 1)];
    line1.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    [accountView addSubview:line1];
    [line1 release];
    
    //密码
    UILabel *passwordLabel = [[UILabel alloc] initWithFrame:CGRectMake(iconView.left, line1.bottom+10, 100, 30)];
    passwordLabel.backgroundColor = [UIColor clearColor];
    passwordLabel.text = @"记住密码";
    [accountView addSubview:passwordLabel];
    [passwordLabel release];
    
    UIImageView *passwordSwitch = [[UIImageView alloc] initWithFrame:CGRectMake(accountView.right-90, line1.bottom+50/2.0-20, 60, 41)];
    passwordSwitch.contentMode = UIViewContentModeScaleAspectFit;
    if (_isPasswordOn == YES) {
        passwordSwitch.image = [UIImage imageNamed:@"switcher_on"];
    }else {
        passwordSwitch.image = [UIImage imageNamed:@"switcher_off"];
    }
    passwordSwitch.userInteractionEnabled = YES;
    [accountView addSubview:passwordSwitch];
    [passwordSwitch release];
    
    UITapGestureRecognizer *passwordTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(passwordTap:)];
    [passwordSwitch addGestureRecognizer:passwordTap];
    [passwordTap release];
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, iconView.bottom+60, accountView.width, 1)];
    line2.backgroundColor = [UIColor colorWithWhite:0.8 alpha:0.5];
    [accountView addSubview:line2];
    [line2 release];
    
    //自动登录
    UILabel *autoLoginLabel = [[UILabel alloc] initWithFrame:CGRectMake(iconView.left, line2.bottom+10, 100, 30)];
    autoLoginLabel.backgroundColor = [UIColor clearColor];
    autoLoginLabel.text = @"自动登录";
    [accountView addSubview:autoLoginLabel];
    [autoLoginLabel release];
    
    
    UIImageView *autoLoginSwitch = [[UIImageView alloc] initWithFrame:CGRectMake(accountView.right-90, line2.bottom+50/2.0-20, 60, 41)];
    autoLoginSwitch.contentMode = UIViewContentModeScaleAspectFit;
    if (_isAutoLoginOn == YES) {
        autoLoginSwitch.image = [UIImage imageNamed:@"switcher_on"];
    }else {
        autoLoginSwitch.image = [UIImage imageNamed:@"switcher_off"];
    }
    autoLoginSwitch.userInteractionEnabled = YES;
    [accountView addSubview:autoLoginSwitch];
    [accountView addSubview:autoLoginSwitch];
    [autoLoginSwitch release];
    
    UITapGestureRecognizer *autoLoginTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(autoLoginTap:)];
    [autoLoginSwitch addGestureRecognizer:autoLoginTap];
    [autoLoginTap release];
    
//    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    UIImage *image = [[UIImage imageNamed:@"title_del_loc_nor9"] stretchableImageWithLeftCapWidth:15 topCapHeight:15];
//    cancelButton.frame = CGRectMake(accountView.left+20, accountView.bottom+20, accountView.width-40, 40);
//    [cancelButton addTarget:self action:@selector(cancelAndLogout) forControlEvents:UIControlEventTouchUpInside];
//    [cancelButton setBackgroundImage:image forState:UIControlStateNormal];
//    [cancelButton setTitle:@"注销" forState:UIControlStateNormal];
//    [self.view addSubview:cancelButton];
    
}

#pragma mark - Private Method

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    UIImage *image = [[UIImage imageNamed:@"btn_big_gray"] stretchableImageWithLeftCapWidth:5 topCapHeight:10];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTitle:@"保存" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [button addTarget:self action:@selector(doSaveConf) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [button release];
    self.navigationItem.rightBarButtonItem = rightItem;
    [rightItem release];
    
}

#pragma mark - Target Actions
- (void)passwordTap:(UITapGestureRecognizer *)tap
{

    UIImageView *passwordSwitch = (UIImageView *)tap.view;
    if (_isPasswordOn) {
        passwordSwitch.image = [UIImage imageNamed:@"switcher_off"];
        _isPasswordOn = NO;
    }else {
        passwordSwitch.image = [UIImage imageNamed:@"switcher_on"];
        _isPasswordOn = YES;
    }
    
}

- (void)autoLoginTap:(UITapGestureRecognizer *)tap
{
    
    UIImageView *autoLoginSwitch = (UIImageView *)tap.view;
    if (_isAutoLoginOn) {
        autoLoginSwitch.image = [UIImage imageNamed:@"switcher_off"];
        _isAutoLoginOn = NO;
    }else {
        autoLoginSwitch.image = [UIImage imageNamed:@"switcher_on"];
        _isAutoLoginOn = YES;
    }

}

- (void)doSaveConf {
    
    [_manager saveObject:_isPasswordOn ? @"YES" : @"NO" forKey:REMEMBERPASSWORD];
    [_manager saveObject:_isAutoLoginOn ? @"YES" : @"NO" forKey:AUTOMATICLOGIN];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"M-Office" message:@"保存成功" delegate:self cancelButtonTitle:@"是" otherButtonTitles:nil];
    [alertView show];
    
    [alertView release];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self backButtonPressed];
    }
}

- (void)backButtonPressed {
    
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

//- (void)cancelAndLogout
//{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"M-Office" message:@"注销后密码将不再保存到手机,是否继续？" delegate:self cancelButtonTitle:@"是" otherButtonTitles:@"否", nil];
//    [alertView show];
//    
//    [alertView release];
//}

//#pragma mark - AlertView Delegate
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == 0) {
//        NSLog(@"是");
//        if ([[_manager getObjectForKey:PASSWORD] isEqualToString:@""] == NO) {
//
//            [_manager saveObject: @"NO" forKey:REMEMBERPASSWORD];
//            [_manager saveObject: @"NO" forKey:AUTOMATICLOGIN];
//            [_manager saveObject:@"" forKey:PASSWORD];
//            [self.navigationController dismissModalViewControllerAnimated:YES];
//        }else {
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"M-Office" message:@"已注销" delegate:nil cancelButtonTitle:@"是" otherButtonTitles:nil];
//            [alertView show];
//            
//            [alertView release];
//        }
//        
//    }else {
//        NSLog(@"否");
//    }
//}

#pragma mark - Memory
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_tableView release],_tableView = nil;
    [super dealloc];
}

@end
