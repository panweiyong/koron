//
//  KoronClientContactModel.h
//  moffice
//
//  Created by Mac Mini on 13-11-12.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "BaseModel.h"

@interface KoronClientContactModel : BaseModel

@property (nonatomic, retain) NSString *contactKey;
@property (nonatomic, retain) NSString *contactVal;

@end
