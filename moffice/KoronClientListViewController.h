//
//  KoronClientListViewController.h
//  moffice
//
//  Created by Mac Mini on 13-11-6.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronDropDownRefreshViewController.h"
//#import "KoronMenuViewController.h"
#import "KoronClientListMenuViewController.h"
#import "ASIFormDataRequest.h"
#import "KoronEndRefreshView.h"
#import "KoronManager.h"
#import "SBJsonParser.h"
#import "SVProgressHUD.h"
#import "JsonService.h"
#import "KoronNotificationModel.h"
#import "KoronClientListCell.h"

@interface KoronClientListViewController : KoronDropDownRefreshViewController <UITableViewDelegate,UITableViewDataSource,ASIHTTPRequestDelegate,KoronClientListMenuViewControllerDelegate, UITextFieldDelegate, KoronClentListDelegate> {
    
    //上拉刷新
    KoronEndRefreshView *_endView;
    
    KoronManager *_manager;
    
    ASIFormDataRequest *_clientListRequest;
    
    ASIFormDataRequest *_moduleRequest;
    
    //对应模板下的请求
    ASIFormDataRequest *_moduleListRequest;
    
    NSMutableArray *_clientLists;
    
    NSString *_lastmid;
    //判断是否重需要清空数据源
    BOOL isEmpty;
    //点击弹出menu
    UIButton *_topButton;
    
    UIImageView *_topImageView;
    //选项视图
    KoronClientListMenuViewController *_menu;
    //menu的背景图片
    UIImageView *_backgroundImageView;
    //请求服务器时发送的状态
    NSString *_status;
    //请求服务器时发送的类型
    NSString *_type;
    //是否显示上拉刷新
    BOOL isShowEndRefresh;
    
    //遮挡试图
    UIView *_maskView;
    
    UITextField *_searchClientField;
    
    NSMutableArray *_moduleArray;
    
    NSString *_lastUpdateTime;
    
    NSMutableArray *_moduleListArray;
    
    BOOL _isModuleListShow;
    
    BOOL _isLoadFromDataBase;
    
    //是否第一次获取客户列表
    BOOL _isNotFirstGetClientList;
    
    UIButton *_synbutton;
}

@property (nonatomic, copy) NSString *phoneNum;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *lastUpdateTime;

@end
