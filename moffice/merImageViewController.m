//
//  merImageViewController.m
//  moffice
//
//  Created by koron on 13-5-29.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "merImageViewController.h"

@interface merImageViewController ()
@end

@implementation merImageViewController
@synthesize bigImageview,bigImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dealloc
{
    
    [bigImage release];
    [bigImageview release];
    
    [super dealloc];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadNavigationItem];
    
   // self.tabBarController.tabBar.hidden=YES;
    self.tabBarController.tabBar.backgroundColor=[UIColor clearColor];
    [self.view setBackgroundColor:[UIColor grayColor]];
    self.title=@"图片";
    [self.view setBackgroundColor:[UIColor grayColor]];
    //self.navigationController.navigationItem.backBarButtonItem.title=@"返回";
    self.navigationController.navigationBarHidden=NO;
    bigImageview=[[UIImageView alloc]initWithFrame:CGRectZero];
    if(bigImage.size.height<self.view.frame.size.height)
    {
        if(bigImage.size.width<320)
        {
            bigImageview.frame=CGRectMake((320-bigImage.size.width)/2, (self.view.frame.size.height-bigImage.size.height)/2, bigImage.size.width, bigImage.size.height);
        }
        else
        {
            bigImageview.frame=CGRectMake(0, (self.view.frame.size.height-bigImage.size.height)/2, 320, bigImage.size.height);
        }
    }
    else
    {
        if(bigImage.size.width<320)
        {
            bigImageview.frame=CGRectMake((320-bigImage.size.width)/2, (self.view.frame.size.height-bigImage.size.height)/2, bigImage.size.width, self.view.frame.size.height);
        }
        else
        {
             bigImageview.frame=CGRectMake(0, 0, 320,self.view.frame.size.height);
        }
    }
    [bigImageview setImage:bigImage];
    [self.view addSubview:bigImageview];
	// Do any additional setup after loading the view .
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}

- (void)backButtonPressed {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
