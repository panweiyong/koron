//
//  KoronRootViewController.m
//  moffice
//
//  Created by Mac Mini on 13-9-27.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//
//  Tag值 100-200

#import "KoronRootViewController.h"
#import "BaseNavigationController.h"
#import "WorkFlowViewController.h"
#import "merchandiserViewController.h"
#import "KoronHomePageViewSettingViewController.h"
#import "BbsViewController.h"
#import "QueryViewController.h"
#import "KoronClientListViewController.h"
#import "KoronItemListViewController.h"
#import "KoronProjectTableViewController.h"
#import "BadgeView.h"

@interface KoronRootViewController ()

@end

@implementation KoronRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _manager = [KoronManager sharedManager];
        //从 KoronStartAnimationViewController 收到的通知,动画效果完毕,移除动画页面
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeAnimationImageView) name:@"removeAnimationImageView" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentStting) name:@"presentStting" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentOption:) name:@"presentOption" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentChatView:) name:@"presentChatView" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hiddenTabbar) name:@"hiddenTabbarNotification" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showTabbar) name:@"showTabbarNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatusBarBlack) name:@"changeStatusBarBlack" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeStatusBarWhite) name:@"changeStatusBarWhite" object:nil];
        
        if (IOS_VERSIONS >= 7.0) {
            [self setNeedsStatusBarAppearanceUpdate];
        }
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (IOS_VERSIONS >= 7.0) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[_manager getObjectForKey:AUTOMATICLOGIN] isEqualToString:@"YES"] && _manager.isFirst) {
        [self loginRequest];
        _startAnimationView = [[KoronStartAnimationViewController alloc]init];
        [self.view addSubview:_startAnimationView.view];
        
    }else {
        [self initializeRootViewController];
    }
    
    
}

- (void)hiddenTabbar {
    [UIView animateWithDuration:0.3 animations:^{
        _tabBar.alpha = 0;
    }];
    
}

- (void)showTabbar {
    [UIView animateWithDuration:0.3 animations:^{
        _tabBar.alpha = 1;
    }];
}


- (void)viewDidAppear:(BOOL)animated {
    if (IOS_VERSIONS >= 7.0) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)initializeRootViewController {
    _homePageCtl = [[KoronHomePageViewController alloc]init];
    _privateMessageCtl = [[KoronPrivateMessageViewController alloc]init];

    _notificationCtl = [[KoronNotificationViewController alloc]init];
    _notificationNavigationController = [[BaseNavigationController alloc] initWithRootViewController:_notificationCtl];
    [_notificationNavigationController.navigationBar setHidden:YES];
    //邮箱
    _mailCtl = [[MailViewController alloc] init];
    _mailNavigationController = [[BaseNavigationController alloc] initWithRootViewController:_mailCtl];
    
    
    if (IOS_VERSIONS >= 7.0) {
        _customView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    }else {
        _customView = [[UIView alloc]initWithFrame:CGRectMake(0, -20, self.view.bounds.size.width, self.view.bounds.size.height)];
    }
    _customView.userInteractionEnabled = YES;
    [self.view addSubview:_customView];
    
    [_customView addSubview:_homePageCtl.view];
    
    _tabBar = [[KoronRootTabBarContller alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height - 44, self.view.bounds.size.width, 44)];
    _tabBar.userInteractionEnabled = YES;
    _tabBar.delegate = self;
    [self.view addSubview:_tabBar];

}



//动画效果结束 移除动画页面
- (void)removeAnimationImageView {
    if ([[_manager getObjectForKey:AUTOMATICLOGIN] isEqualToString:@"YES"] && _manager.isFirst) {
        [_startAnimationView.view removeFromSuperview];
        _startAnimationView = nil;
    }
    [self initializeRootViewController];
}


- (void)loginRequest {
    if (nil == [_manager getObjectForKey:SERVERURL]) {
        [_manager saveObject:[NSString stringWithFormat:@"http://%@/moffice",[_manager getObjectForKey:SERVER]] forKey:SERVERURL];
    }
    [_loginRequest clearDelegatesAndCancel];
    _loginRequest = nil;
    _loginRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[_manager getObjectForKey:SERVERURL]]];
    _loginRequest.delegate = self;
    [_loginRequest setDidFinishSelector:@selector(loginFinish:)];
    [_loginRequest setDidFailSelector:@selector(loginFail:)];
    [_loginRequest setPostValue:@"auth" forKey:@"op"];
    [_loginRequest setPostValue:[_manager getObjectForKey:USERNAME] forKey:@"username"];
    [_loginRequest setPostValue:[_manager getMD5:[_manager getObjectForKey:PASSWORD]] forKey:@"password"];
    
    
    [_loginRequest startAsynchronous];
}


- (void)loginFinish:(ASIFormDataRequest *)response {
    _manager.isFirst = NO;
}


- (void)loginFail:(ASIFormDataRequest *)response {
    
}


#pragma mark - KoronRootTabBarContllerDelegate

-(void)clickTabBarItem:(NSNumber *)item {
    NSLog(@"%d",[item integerValue]);
    NSInteger itemNumber = [item integerValue];
    for (UIView *view in _customView.subviews) {
        [view removeFromSuperview];
    }
    
    if (itemNumber == 0) {
        [_customView addSubview:_homePageCtl.view];
    }else if (itemNumber == 1) {
//        BadgeView *badgeView = (BadgeView *)[_tabBar viewWithTag:201];
//        badgeView.badgeLabel.text = @"95";
        [_customView addSubview:_notificationNavigationController.view];
        
    }else if (itemNumber == 2) {
        [_customView addSubview:_privateMessageCtl.view];
    }else if (itemNumber == 3){
        [_customView addSubview:_mailNavigationController.view];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    if (_isStatusBarBlack) {
        return UIStatusBarStyleDefault;
    }
    return UIStatusBarStyleLightContent;
}


#pragma mark - present
//切换至设置页面
- (void)presentStting {
    KoronSettingViewController *settingCtl = [[KoronSettingViewController alloc]init];
    BaseNavigationController *settingNav = [[BaseNavigationController alloc] initWithRootViewController:settingCtl];
    [self presentViewController:settingNav animated:YES completion:nil];
    [settingNav release];
}

//切换至对应的试图控制器
- (void)presentOption:(NSNotification *)note {
    NSString *string = [note object];
    if ([string isEqualToString:@"行程轨迹"]) {
        
        KoronLocusViewController *locusCtl = [[KoronLocusViewController alloc]init];
        [self presentViewController:locusCtl animated:YES completion:nil];
        [locusCtl release];
        
    }else if ([string isEqualToString:@"通讯录"]) {
        
        connectViewController *connectCtl = [[connectViewController alloc]init];
        [self presentViewController:connectCtl animated:YES completion:nil];
        [connectCtl release];
        
    }else if ([string isEqualToString:@"工作计划"]){
        
        WorkPlanViewController *workPlan = [[WorkPlanViewController alloc]init];
        BaseNavigationController *workPlanCtl = [[BaseNavigationController alloc]initWithRootViewController:workPlan];
        [self presentViewController:workPlanCtl animated:YES completion:nil];
        [workPlan release];
        [workPlanCtl release];
        
    }else if ([string isEqualToString:@"工作审批"]) {
        
        WorkFlowViewController *wfscreen = [[WorkFlowViewController alloc] initWithNibName:nil bundle:nil];
        BaseNavigationController *wfscreenNav =[[BaseNavigationController alloc] initWithRootViewController:wfscreen];
        [self presentModalViewController:wfscreenNav animated:YES];
        [wfscreen release];
        [wfscreenNav release];
        
    }else if ([string isEqualToString:@"销售跟单"]) {
        
        merchandiserViewController *merchandiserView=[[merchandiserViewController alloc]initWithNibName:nil bundle:nil];
        BaseNavigationController *merchandiserNav = [[BaseNavigationController alloc] initWithRootViewController:merchandiserView];
        [merchandiserView release];
        [self presentModalViewController:merchandiserNav animated:YES];
        [merchandiserNav release];
     
        
        
        
    }else if ([string isEqualToString:@"编辑"]) {
        
        KoronHomePageViewSettingViewController *homepageOptionSettingCtl = [[KoronHomePageViewSettingViewController alloc] init];
        BaseNavigationController *optionSettingNav = [[BaseNavigationController alloc] initWithRootViewController:homepageOptionSettingCtl];
        [self presentModalViewController:optionSettingNav animated:YES];
        [optionSettingNav release];
        
    }else if ([string isEqualToString:@"发帖"]) {
        
        BbsViewController *bbsViewController = [[BbsViewController alloc] init];
        BaseNavigationController *bbsViewNav = [[BaseNavigationController alloc] initWithRootViewController:bbsViewController];
        [bbsViewController release];
        [self presentModalViewController:bbsViewNav animated:YES];
        [bbsViewNav release];
        
    }else if ([string isEqualToString:@"产品知识库"]) {

        NSLog(@"产品知识库");
    }else if  ([string isEqualToString:@"客户列表"]) {
        
        KoronClientListViewController *clientViewCtl = [[KoronClientListViewController alloc] init];
        BaseNavigationController *clientViewNav = [[BaseNavigationController alloc] initWithRootViewController:clientViewCtl];
        
        [self presentModalViewController:clientViewNav animated:YES];
        [clientViewCtl release];
        [clientViewNav release];
    }else if ([string isEqualToString:@"待办"]) {
        
        KoronItemListViewController *toDoListViewController = [[KoronItemListViewController alloc] init];
        BaseNavigationController *addItemNavigationController = [[BaseNavigationController alloc] initWithRootViewController:toDoListViewController];
        [self presentViewController:addItemNavigationController animated:YES completion:nil];
        [addItemNavigationController release];
        [toDoListViewController release];
    } else if ([string isEqualToString:@"我的项目"]) {
        KoronProjectTableViewController *projectViewController = [[KoronProjectTableViewController alloc] init];
        BaseNavigationController *navigationController = [[BaseNavigationController alloc] initWithRootViewController:projectViewController];
        [self presentModalViewController:navigationController animated:YES];
        [projectViewController release];
        [navigationController release];
    }
}

//切换至聊天视图控制器
- (void)presentChatView:(NSNotification *)note {
    
    CATransition *myAnimation = [CATransition animation];
    myAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    myAnimation.duration = 0.4;
    myAnimation.type = kCATransitionMoveIn;
    myAnimation.subtype = kCATransitionFromRight;
    
    [[[[[UIApplication sharedApplication] delegate] window] layer] addAnimation:myAnimation forKey:nil];
    KoronChatViewController *chatViewCtl = [[KoronChatViewController alloc]initWithLinkMan:[[note object] objectForKey:@"id"] linkManName:[[note object] objectForKey:@"name"] type:[[note object] objectForKey:@"type"]];
    [self presentViewController:chatViewCtl animated:NO completion:nil];
    
}

- (void)changeStatusBarBlack {
    _isStatusBarBlack = YES;
    if (IOS_VERSIONS >= 7.0) {
        
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)changeStatusBarWhite {
    _isStatusBarBlack = NO;
    if (IOS_VERSIONS >= 7.0) {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

@end
