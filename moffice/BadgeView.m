//
//  BadgeView.m
//  moffice
//
//  Created by Mac Mini on 13-12-3.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "BadgeView.h"

@implementation BadgeView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews
{    
    self.layer.cornerRadius = 7.5;
    self.layer.borderWidth = 1.5;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    self.backgroundColor = [UIColor redColor];
    
    _badgeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _badgeLabel.backgroundColor = [UIColor clearColor];
    _badgeLabel.text = @"99";
    _badgeLabel.textColor = [UIColor whiteColor];
    _badgeLabel.textAlignment = NSTextAlignmentCenter;
    _badgeLabel.font = [UIFont boldSystemFontOfSize:8];
    [self addSubview:_badgeLabel];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
//    _badgeLabel.frame = CGRectMake(self.width/4.0, self.height/4.0, self.height/2.0, self.width/2.0);
    _badgeLabel.frame = self.bounds;
    
}

- (void)dealloc
{
    [_badgeLabel release], _badgeLabel = nil;
    [super dealloc];
}

@end
