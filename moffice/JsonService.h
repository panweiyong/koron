

#import <Foundation/Foundation.h>
#import "NSString+Helpers.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "SBJson.h"
#import "JsonUtils.h"
#import "SMFileUtils.h"
#import "ModalAlert.h"

//定义协议
@protocol pushDataDelegate
- (void)dataCallBack:(NSString *)str1 other:(NSString*)str2 third:(NSNumber*)str3; //回调传值
@end

@protocol pushArrayDataDelegate
-(void)arrayDataCallBack:(NSArray*)arr;
@end

@protocol pushmerArrayDataDelegate 
-(void) merarrayDataCallBack:(NSArray*)arr;
@end

@interface JsonService :  NSObject{
    
    
    ASIFormDataRequest *OARequest;//返回查询数据
    ASIFormDataRequest *POSTRequest;//返回操作结果
    ASIFormDataRequest *UPDATEFLAGRequest;//返回标记结果
    ASIFormDataRequest *CUSTOMERquest;//返回客户查询结果
    ASIFormDataRequest *MERRquest;//返回跟单查询结果
    ASIFormDataRequest *CLIENTRquest;//返回添加客户
    id <ASIHTTPRequestDelegate> delegate;
    id <pushDataDelegate> pushDelegate;
    id <pushArrayDataDelegate> ArrayDelegate;
    id <pushmerArrayDataDelegate> MerArrayDelegate;
    
    UILabel *_topLabel;
    
    NSString *_pushJson;
}
+ (JsonService*)sharedManager;

@property (nonatomic,retain) ASIFormDataRequest *OARequest;
@property (nonatomic,retain) ASIFormDataRequest *POSTRequest;
@property (nonatomic,retain) ASIFormDataRequest *UPDATEFLAGRequest;
@property (nonatomic,retain) ASIFormDataRequest *CUSTOMERquest;
@property (nonatomic,retain) ASIFormDataRequest *MERRquest;
@property (nonatomic,retain) ASIFormDataRequest *CLIENTRquest;

//@property (nonatomic,retain) id delegate;
//@property (nonatomic,retain) id pushDelegate;
//@property (nonatomic,retain) id ArrayDelegate;
//@property (nonatomic,retain) id MerArrayDelegate;

@property (nonatomic,assign) id delegate;
@property (nonatomic,assign) id pushDelegate;
@property (nonatomic,assign) id ArrayDelegate;
@property (nonatomic,assign) id MerArrayDelegate;

@property (nonatomic,retain)NSString *pushJson;



-(NSString*)getUserName;//用户名
-(NSString*)getPassword;//密码
-(NSString*)getHost;//服务地址
-(NSString*)getSessionID;//用户ID
-(NSString*)getMobile;//用户ID
-(BOOL)isFirstRun;
-(BOOL)isAutoLogin;
-(BOOL)enabledAutoLogin;

-(void)setMobile:(NSString*)m;
-(void)setUserName:(NSString*)u;
-(void)setPassword:(NSString*)p;
-(void)setHost:(NSString*)h;
-(void)setIsAutoLogin:(BOOL)v;

-(void)cancelAllRequest;

-(void)loadConf;

//登录
-(void)login:(NSString*)uname pwd:(NSString*)pwd;

//列出所有系统消息
-(void)listTips:(NSString*)lastid;
-(void)getTipDetails:(NSString*)OID t:(NSString*)t;

//私信
-(void)listMessageBox;
-(void)listMessages:(NSString*)from lastmid:(NSString*)lastmid;
-(void)sendMessage:(NSString*)to content:(NSString*)content;
-(void)listOnlineUsers;//列出在线用户
-(void)listAllUsers;//列出所有用户
-(NSString*)getUserIconURL:(NSString*)from;
-(NSString*)getOnlineURL;

//工作审批
-(void)listWorkflows:(NSString*)lastwid;
-(void)getWorkflowDetails:(NSString*)wid;
-(void)cancelWorkflow:(NSString*)wid;
-(void)approvalWorkflow:(NSString*)wid aid:(NSString*)aid to:(NSString*)to memo:(NSString*)memo iscancel:(BOOL)iscancel;

//论坛
-(void)getBbs:(NSString*)bid;
-(void)postReplay:(NSString*)refidStr second:(NSString*)topicIdStr third:(NSString*)contentStr;

//通知通告
-(void)getAdvice:(NSString*)Aid;

//获取发帖类型
-(void)getListviewData;
-(void)getListviewDetailData;
-(void)pushListData:(NSString*)jsonString topStr:(NSString*)str;

//工作计划
-(void)getWorkPlanList:(NSString*)type str:(NSString*)strTime end:(NSString*)endTime;//获取工作计划
-(void)getPersonList:(NSString*)type keyWord:(NSString*)keyWord lastId:(NSString*)lastId;//获取联系人等
-(void)getWorkPlanDetail:(NSString*)planID;//计划详情
-(void)addWorkPlan:(NSString*)Id type:(NSString*)Type title:(NSString *)title content:(NSString*)content start:(NSString*)start end:(NSString*)end ass:(NSString*)ass;//增加工作计划
-(void)deleteWorkPlan:(NSString *)planId;//删除工作计划
-(void)reviewWorkPlan:(NSString *)planId remarkType:(NSString *)remarkType commitId:(NSString *)commitId content:(NSString*)content;//点评工作计划
-(void)summaryWorkPlan:(NSString *)planId time:(NSString*)time summary:(NSString*)summary statusId:(NSString*)statusId;//总结工作计划

//通讯录
-(void)getContactList;


//邮件
-(void)listMailBox;
-(void)listMail:(NSString*)bid fid:(NSString*)fid lastmid:(NSString*)lastmid;
-(void)getMail:(NSString*)mid;
-(void)receiveMail:(NSString*)bid;
-(void)sendMail:(NSString*)to from:(NSString*)from subject:(NSString*)subject content:(NSString*)content;
-(void)listIntranetMailUsers;
-(void)listInternetMailUsers:(NSString*)skey;

//自定义
-(void)listMyModules;
-(void)listBusinessModules;
-(void)loadModule:(NSString*)target parameterName:(NSString*) parameterName parameterValue:(NSString*)parameterValue parameterName2:(NSString*) parameterName2 parameterValue2:(NSString*)parameterValue2 mid:(NSString *)mid;
-(void)loadModuleWithParameters:(NSString*)target parameters:(NSMutableDictionary*)parameters mid:(NSString*)mid;

//更新标记 0:未读 1:已读 2:删除
-(void)updataRead:(NSString*)tipsid;
-(void)UpdateFlag:(NSString*)type oid:(NSString*)oid flag:(int)flag tipsid:(NSString*)tipsid;

//查找客户是否存在
-(void)findCustomer:(NSString*)customerName;
-(void)findMoreCustomer:(NSString*)customerName lastId:(NSString*)lastId;//加载跟多

//发送销售跟单到后台
-(void)pushDate:(NSString*)jsonString;

//添加客户
- (void)pushAddClient:(NSString *)jsonString;
@end