//
//  KoronProjectCell.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-25.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation KoronProjectCell
{
    UIView *_leftLine;
    UIView *_rightLine;
    UIView *_middleLine;
    
    UIView *_upBackgroundView;
    UIView *_downBackgroundView;
    
    UILabel *_chargeLabel;
    UIImageView *_dynamicView;
    UIImageView *_assignmentView;
    UIImageView *_attachView;
    UIImageView *_peopleView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)initSubviews
{
    _leftLine = [[UIView alloc] initWithFrame:CGRectZero];
    _leftLine.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
    [self.contentView addSubview:_leftLine];
    
    _rightLine = [[UIView alloc] initWithFrame:CGRectZero];
    _rightLine.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
    [self.contentView addSubview:_rightLine];
    
    _upBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    _upBackgroundView.backgroundColor = [UIColor clearColor];
    _upBackgroundView.userInteractionEnabled = YES;
    [self.contentView addSubview:_upBackgroundView];
    
    UITapGestureRecognizer *upTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onUpViewTap:)];
    [_upBackgroundView addGestureRecognizer:upTap];
    [upTap release];
    
    _downBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    _downBackgroundView.backgroundColor = [UIColor clearColor];
    _downBackgroundView.userInteractionEnabled = YES;
    [self.contentView addSubview:_downBackgroundView];
    
    UITapGestureRecognizer *downTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDownViewTap:)];
    [_downBackgroundView addGestureRecognizer:downTap];
    [downTap release];
    
    //状态
    _statusPicture = [[UIImageView alloc] initWithFrame:CGRectZero];
    _statusPicture.backgroundColor = [UIColor grayColor];
    _statusPicture.image = [UIImage imageNamed:@"items_r.png"];
    [self.contentView addSubview:_statusPicture];
    
    //标题
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.textColor = [UIColor colorWithRed:47/255.0 green:146/255.0 blue:1 alpha:1];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.text = @"这个有的长长长长长长长长长长长长长长长长长长长长长";
    [self.contentView addSubview:_titleLabel];
    
    //负责人
    _chargeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _chargeLabel.font = [UIFont systemFontOfSize:10];
    _chargeLabel.text = @"负责人:";
    _chargeLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_chargeLabel];
    
    _executorLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _executorLabel.text = @"某某某";
    _executorLabel.font = [UIFont systemFontOfSize:10];
    _executorLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_executorLabel];
    
    //剩余时间
    _remainingTimeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _remainingTimeLabel.textAlignment = NSTextAlignmentRight;
    _remainingTimeLabel.text = @"剩余4天";
    _remainingTimeLabel.font = [UIFont boldSystemFontOfSize:12];
    [self.contentView addSubview:_remainingTimeLabel];
    
    //介绍
    _introduceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _introduceLabel.backgroundColor = [UIColor clearColor];
    _introduceLabel.font = [UIFont systemFontOfSize:10];
    _introduceLabel.text = @"介绍:";
    [self.contentView addSubview:_introduceLabel];
    
    //内容
    _detailContentLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _detailContentLabel.backgroundColor = [UIColor clearColor];
    _detailContentLabel.font = [UIFont systemFontOfSize:10];
    _detailContentLabel.numberOfLines = 0;
    [self.contentView addSubview:_detailContentLabel];
    
    _middleLine = [[UIView alloc] initWithFrame:CGRectZero];
    _middleLine.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
    [self.contentView addSubview:_middleLine];
    
    //动态
    _dynamicView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _dynamicView.contentMode = UIViewContentModeScaleAspectFit;
    _dynamicView.image = [UIImage imageNamed:@"ico_log"];
    [self.contentView addSubview:_dynamicView];
    
    _dynamicLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _dynamicLabel.backgroundColor = [UIColor clearColor];
    _dynamicLabel.text = @"动态";
    _dynamicLabel.font = [UIFont systemFontOfSize:10];
    _dynamicLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_dynamicLabel];
    
    //任务
    _assignmentView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _assignmentView.contentMode = UIViewContentModeScaleAspectFit;
    _assignmentView.image = [UIImage imageNamed:@"ico_task"];
    [self.contentView addSubview:_assignmentView];
    
    _assignmentLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _assignmentLabel.backgroundColor = [UIColor clearColor];
    _assignmentLabel.text = @"任务";
    _assignmentLabel.font = [UIFont systemFontOfSize:10];
    _assignmentLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_assignmentLabel];
    
    //附件
    _attachView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _attachView.contentMode = UIViewContentModeScaleAspectFit;
    _attachView.image = [UIImage imageNamed:@"ico_affix"];
    [self.contentView addSubview:_attachView];
    
    _attachLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _attachLabel.backgroundColor = [UIColor clearColor];
    _attachLabel.text = @"附件";
    _attachLabel.textAlignment = NSTextAlignmentCenter;
    _attachLabel.font = [UIFont systemFontOfSize:10];
    [self.contentView addSubview:_attachLabel];
    
    //参与人数
    _peopleView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _peopleView.contentMode = UIViewContentModeScaleAspectFit;
    _peopleView.image = [UIImage imageNamed:@"ico_man"];
    [self.contentView addSubview:_peopleView];
    
    _peopleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _peopleLabel.backgroundColor = [UIColor clearColor];
    _peopleLabel.text = @"人员";
    _peopleLabel.font = [UIFont systemFontOfSize:10];
    _peopleLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_peopleLabel];
    
    for (NSInteger index = 0; index < 3; index++) {
        
        UIView *breakline = [[UIView alloc] initWithFrame:CGRectZero];
        breakline.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:0.8];
        breakline.tag = 100 + index;
        [self.contentView addSubview:breakline];
    }
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _leftLine.frame = CGRectMake(0, 0, 0.5, self.height);
    _rightLine.frame = CGRectMake(self.width-0.5, 0, 0.5, self.height);
    
    _statusPicture.frame = CGRectMake(10, 10, 50, 50);
    _titleLabel.frame = CGRectMake(_statusPicture.right + 10, _statusPicture.top, 220, 20);
    _chargeLabel.frame = CGRectMake(_statusPicture.right + 10, _titleLabel.bottom + 10, 40, 10);
    _executorLabel.frame = CGRectMake(_chargeLabel.right, _chargeLabel.top, _chargeLabel.width, _chargeLabel.height);
    _remainingTimeLabel.frame = CGRectMake(self.width - 100, _chargeLabel.top, 90, 15);
    _introduceLabel.frame = CGRectMake(_statusPicture.right + 10, _chargeLabel.bottom + 10, 25, _chargeLabel.height);
    _detailContentLabel.frame = CGRectMake(_introduceLabel.right, _introduceLabel.top, 185, 1000);
    [_detailContentLabel sizeToFit];
    _detailContentLabel.center = CGPointMake(_introduceLabel.right+_detailContentLabel.width/2.0 + 10,  _introduceLabel.top + _detailContentLabel.height/2.0);
    
//    _middleLine.frame = CGRectMake(0, _detailContentLabel.bottom + 15, kDeviceWidth, 0.5);
    _middleLine.frame = CGRectMake(0, self.height - 30, kDeviceWidth, 0.5);
    _upBackgroundView.frame = CGRectMake(0, 0, self.width, _middleLine.top);
    _downBackgroundView.frame = CGRectMake(0, _middleLine.bottom, self.width, self.height - _middleLine.bottom);
    
    for (NSInteger index = 0; index < 3; index++) {
        UIView *breakline = [self.contentView viewWithTag:index + 100];
        breakline.frame = CGRectMake(self.width/4 + index * self.width/4, _middleLine.bottom + 8, 1, 12);
    
    }
    
    _dynamicView.frame = CGRectMake(18, _middleLine.bottom + 8, 13, 12);
    _dynamicLabel.frame = CGRectMake(_dynamicView.right + 5, _dynamicView.top, 20, _dynamicView.height);
    
    _assignmentView.frame = CGRectMake(18 + self.width/4, _middleLine.bottom + 8, 13, 12);
    _assignmentLabel.frame = CGRectMake(_assignmentView.right + 5, _assignmentView.top, 20, _assignmentView.height);
    
    _attachView.frame = CGRectMake(18 + 2 * self.width/4, _middleLine.bottom + 8, 13, 12);
    _attachLabel.frame = CGRectMake(_attachView.right + 5, _attachView.top, 20, _attachView.height);
    
    _peopleView.frame = CGRectMake(18 + 3 * self.width/4, _middleLine.bottom + 8, 13, 12);
    _peopleLabel.frame = CGRectMake(_peopleView.right + 5, _peopleView.top, 20, _peopleView.height);

}

- (void)onUpViewTap:(UITapGestureRecognizer *)tap
{
    NSLog(@"up Tap");
    
    if ([self.delegate respondsToSelector:@selector(KoronProjectCell:didSelectItem:)]) {
        [self.delegate KoronProjectCellDidSelectUpView:self];
    }
    
   
}

- (void)onDownViewTap:(UITapGestureRecognizer *)tap
{
    CGPoint point = [tap locationInView:_downBackgroundView];
    NSString *string = nil;
    if (point.x < self.width / 4) {
        NSLog(@"动态");

    } else if (point.x < self.width / 2) {
        NSLog(@"任务");
    } else if (point.x < self.width * 3 / 4.0) {
        NSLog(@"附件");
    } else {
        NSLog(@"参与人");
    }
    
    if ([self.delegate respondsToSelector:@selector(KoronProjectCell:didSelectItem:)]) {
        [self.delegate KoronProjectCell:self didSelectItem:string];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame
{
    if (IOS_VERSIONS >= 7.0) {
        frame.origin.x += 5;
        frame.size.width -= 10;
    }
    [super setFrame:frame];
    
}

- (void)dealloc
{
    [_leftLine release], _leftLine = nil;
    [_rightLine release], _rightLine = nil;
    [_middleLine release], _middleLine = nil;
    [_dynamicView release], _dynamicView = nil;
    [_attachView release], _attachView = nil;
    [_peopleView release], _peopleView = nil;
    [_upBackgroundView release], _upBackgroundView = nil;
    [_downBackgroundView release], _downBackgroundView = nil;
    self.statusPicture = nil;
    self.executorLabel = nil;
    self.titleLabel = nil;
    self.remainingTimeLabel = nil;
    self.introduceLabel = nil;
    self.detailContentLabel = nil;
    self.dynamicLabel = nil;
    self.assignmentLabel = nil;
    self.attachLabel = nil;
    self.peopleLabel = nil;
        
    [super dealloc];
}

@end
