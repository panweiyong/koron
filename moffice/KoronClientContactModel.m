//
//  KoronClientContactModel.m
//  moffice
//
//  Created by Mac Mini on 13-11-12.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientContactModel.h"

@implementation KoronClientContactModel

- (id)mapAttributes
{
    NSDictionary *mapDic = @{
                             @"contactKey"    : @"Key",
                             @"contactVal"    : @"Val",
                             };
    return mapDic;
}

- (void)dealloc
{
    self.contactKey = nil;
    self.contactVal = nil;
    [super dealloc];
}

@end
