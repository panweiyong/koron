//
//  KoronProjectDynamicCell.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-28.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface KoronProjectDynamicCell : UITableViewCell

@property (nonatomic, retain) UIButton *dynamicButton;

- (void)setDottedLineHidden:(BOOL)isHidden;
- (void)changeDynamicFrame:(BOOL)isChange;

@property (nonatomic, retain) UIImageView *iconView;
@property (nonatomic, retain) UILabel *timeLabel;
@property (nonatomic, retain) UILabel *nameLabel;
@property (nonatomic, retain) UILabel *replyLabel;
@property (nonatomic, retain) UILabel *replyContentLabel;
@property (nonatomic, retain) UIView  *dynamicBackgroundView;

@end
