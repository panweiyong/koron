//
//  KoronNewClientViewController.h
//  moffice
//
//  Created by Mac Mini on 13-11-13.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import "BMapKit.h"
#import <QuartzCore/QuartzCore.h>
#import "GMGridView.h"
#import "GMGridViewLayoutStrategies.h"
#import "JsonService.h"
#import "QBImagePickerController.h"
#import "DropDown.h"
#import "PassvalueDelegate.h"
#import <CoreLocation/CoreLocation.h>



@interface KoronNewClientViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,UITextViewDelegate,BMKGeneralDelegate,BMKMapViewDelegate,BMKSearchDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,QBImagePickerControllerDelegate,pushDataDelegate,pushmerArrayDataDelegate>
{
    UITableView *_tableView;
    UITextField *_clientNameField;
    UITextField *_contactField;
    UITextField *_phoneField;
    UITextView *_infoField;
    
    UITableView *_alertTabView;
    UIView *_backgroundView;
    NSMutableArray *_imageArray;
    UIScrollView *imageScrollview;
    int imageIndex;
    UIButton *_addPhotoButton;
    
    UIActivityIndicatorView *activity3;
    UIActivityIndicatorView *activity4;
    NSString *promptStr;
    BMKMapView *littleMap;
    UIButton *detailbutton;
    NSString *whereAmI;
    
    CLLocationCoordinate2D startPt;
    CLLocation *whereLocation;
}

@property(nonatomic,retain) BMKSearch *_search;

@end

