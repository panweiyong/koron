//
//  ImageViewController.m
//  moffice
//
//  Created by yangxi zou on 12-1-11.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "ImageViewController.h"

@implementation ImageViewController

@synthesize displayImage;

-(void)dealloc
{
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

BOOL ishidden;

- (void) hideNavBar:(BOOL) hidden{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0];
    
    for(UIView *view in self.navigationController.view.subviews)
    {
        if([view isKindOfClass:[UINavigationBar class]])
        {
            if (hidden) {
                [view setFrame:CGRectMake(view.frame.origin.x, - 69, view.frame.size.width, view.frame.size.height)];
            } else {
                [view setFrame:CGRectMake(view.frame.origin.x, 20, view.frame.size.width, view.frame.size.height)];
            }
        } 
    }
    
    [UIView commitAnimations];
}

-(void)handleTapFrom:(UITapGestureRecognizer *)recognizer{ 
    [self.navigationController setNavigationBarHidden:!self.navigationController.navigationBar.hidden animated:YES];
    //[self hideNavBar :NO];
}
- (void) hideTabBar:(BOOL) hidden{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0];
    
    for(UIView *view in self.tabBarController.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            if (hidden) {
                [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
            } else {
                [view setFrame:CGRectMake(view.frame.origin.x, 480-49, view.frame.size.width, view.frame.size.height)];
            }
        } 
        else 
        {
            if (hidden) {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
            } else {
                [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480-49)];
            }
        }
    }
    
    [UIView commitAnimations];
}

-(void)viewWillAppear:(BOOL)animated{
    //self.hidesBottomBarWhenPushed = YES;
    //[self.tabBarController.tabBar setHidden:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    //[self hideTabBar:YES]; 
    //[self.navigationController setToolbarHidden:NO animated:NO]; 
    //ishidden=YES;
    self.navigationController.navigationBar.translucent=YES;
    //self.navigationController.navigationBar.tintColor=[UIColor blackColor];
}
-(void)viewDidAppear:(BOOL)animated
{  
    self.navigationController.navigationBar.alpha = 0.7;
    //[self hideNavBar :YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
    //[self.tabBarController.tabBar setHidden:NO];
    //[self hideTabBar:NO];
    self.navigationController.navigationBar.alpha = 1;
    [self.navigationController.navigationBar setTranslucent:NO];
    //[self.navigationController setToolbarHidden:YES animated:YES]; 
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/
 
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

-(void)handlePinchEvent:(UIPinchGestureRecognizer *)sender
{
    if([sender state] == UIGestureRecognizerStateEnded) {  
        lastScale = 1.0;  
        return;  
    }  
    
    CGFloat scale = 1.0 - (lastScale - [(UIPinchGestureRecognizer*)sender scale]);  
    CGAffineTransform currentTransform =  iviewer.transform;  
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, scale, scale);  
    
    [ iviewer setTransform:newTransform];  
    lastScale = [sender scale];  
    
}

// 旋转
-(void)rotate:(id)sender {
    if([(UIRotationGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {
        _lastRotation = 0.0;
        return;
    }
    CGFloat rotation = 0.0 - (_lastRotation - [(UIRotationGestureRecognizer*)sender rotation]);
   CGAffineTransform currentTransform = iviewer.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(currentTransform,rotation);
    [iviewer setTransform:newTransform];
  _lastRotation = [(UIRotationGestureRecognizer*)sender rotation];
   // [self showOverlayWithFrame:iviewer.frame];

}  
// 移动
-(void)move:(id)sender {
    
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
   
    if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
        _firstX = [iviewer center].x;
        _firstY = [iviewer center].y;
    } 
    translatedPoint = CGPointMake(_firstX+translatedPoint.x, _firstY+translatedPoint.y);
    [iviewer setCenter:translatedPoint];
    //[self showOverlayWithFrame:photoImage.frame];
     
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    if(iviewer==nil){
        
        self.view.backgroundColor=[UIColor blackColor];
        
        CGRect  screensize=[[UIScreen mainScreen] bounds];
        iviewer =[[UIImageView alloc] initWithFrame: CGRectMake(0, -10,screensize.size.width,screensize.size.height) ];
        iviewer.userInteractionEnabled=YES;
        iviewer.backgroundColor=[UIColor blackColor];
        [iviewer setContentMode:UIViewContentModeScaleAspectFit];      
        iviewer.tag = 2;
        
        /*
        UIRotationGestureRecognizer *rotationRecognizer =  [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotate:)]  ;
        // [rotationRecognizer setDelegate:self];
        [iviewer addGestureRecognizer:rotationRecognizer];
        [rotationRecognizer release];
        */
        
        UIPanGestureRecognizer *panRecognizer =  [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)]  ;
        [panRecognizer setMinimumNumberOfTouches:1];
        [panRecognizer setMaximumNumberOfTouches:1];
        //[panRecognizer setDelegate:self];
        [iviewer addGestureRecognizer:panRecognizer];
        [panRecognizer release];
        
        
        UITapGestureRecognizer *recognizer=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)]; 
        [iviewer addGestureRecognizer:recognizer]; 
        [recognizer release];
        
        UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchEvent:)];
        //[pinch setDelegate:iviewer];
        [iviewer addGestureRecognizer:pinch];
        [pinch release];
        
        [self.view addSubview:iviewer];
        [iviewer release];
    }
        [iviewer setImage:nil];
        [iviewer setImage:displayImage];
    
    //self.view.backgroundColor =  [UIColor colorWithPatternImage: displayImage];
    /*
    NSMutableArray *buttons = [[NSMutableArray alloc] initWithCapacity: 4 ];  
    
    UIBarButtonItem *flexibleSpaceItem;  
    flexibleSpaceItem = [[[UIBarButtonItem alloc]   
                          initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace  
                          target:nil action:NULL] autorelease];   
    [buttons addObject:flexibleSpaceItem];  
    [flexibleSpaceItem release];  
    
    UIBarButtonItem *item;  
    item = [[UIBarButtonItem alloc]   
            initWithImage:[UIImage imageNamed:@"down.png" ]  
            style:UIBarButtonItemStylePlain   
            target:self   
            action:@selector (decrement:)];  
    [buttons addObject:item];  
    [item release];  
    
    item = [[UIBarButtonItem alloc]   
            initWithImage:[UIImage imageNamed:@"up.png" ]  
            style:UIBarButtonItemStylePlain target:self   
            action:@selector (increment:)];  
    [buttons addObject:item];  
    [item release];  
    
    item = [[[UIBarButtonItem alloc]   
             initWithBarButtonSystemItem:UIBarButtonSystemItemPlay  
             target:nil action:NULL] autorelease];    
    [buttons addObject:item];  
    [item release];  
    
    
    flexibleSpaceItem = [[[UIBarButtonItem alloc]   
                          initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace  
                          target:nil action:NULL] autorelease];   
    
    [buttons addObject:flexibleSpaceItem];  
    [flexibleSpaceItem release];  
    
    [self.navigationController setToolbarHidden:NO animated:YES];  
    self.navigationController.toolbar.barStyle = UIBarStyleBlack;
    [self.navigationController.toolbar setTranslucent:YES];
    
    [self setToolbarItems:buttons  animated:YES]
    */
    
    /*
     UIToolbar *toolbar = [[UIToolbar alloc] init];   
     toolbar.barStyle = UIBarStyleBlackOpaque;  
     [toolbar setItems:buttons animated:YES];  
     [toolbar sizeToFit];  
     */
    //[self.navigationController.toolbar setHidden:NO];
    
} 

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;//(interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
