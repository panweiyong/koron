//
//  bbsTabViewController.m
//  moffice
//
//  Created by lijinhua on 13-6-6.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "bbsTabViewController.h"
@interface bbsTabViewController ()

@end

@implementation bbsTabViewController
@synthesize refid,tipsid;
@synthesize bbsScrollview;
@synthesize commentsArr;
@synthesize replayTabview;
@synthesize dataArr;
@synthesize activity;
@synthesize replayContent,topicId;
@synthesize replayNum,replayResult;
@synthesize titleLabel,logoView,userLabel,timeLabel,comLabel,triImage,bbsWeb,lineView;
@synthesize replayLabel,ftriImage,replayTextview,replayBtn;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.title=NSLocalizedString(@"bbs",@"bbs") ;
    }
    return self;
}

-(void)dealloc
{
    [super dealloc];
    //[commentsArr release];
    //[bbsScrollview release];
}


- (void)viewWillDisappear:(BOOL)animated {
    [[JsonService sharedManager] cancelAllRequest];
    [self setHidesBottomBarWhenPushed:NO];
    [super viewDidDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewDidLoad];
    activity=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
    [activity setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
    [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:activity];
    [activity startAnimating];
    [self layOutView];
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    NSLog(@"refid=%@",refid);
    [jservice getBbs:refid];
    replayContent=nil;
    replayTabview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [replayTabview setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:replayTabview];
    replayTabview.delegate=self;
    replayTabview.dataSource=self;
    //[replayTabview reloadData];
    [replayTabview release];
}

//异步接收到返回的数据
- (void)requestDataFinished:(id)jsondata;//request
{
    BOOL  flag=NO;
    if(!flag)
    {
        flag=YES;
        if ([jsondata isKindOfClass:[NSDictionary class]])
        {
            NSLog(@"%@",jsondata);
            topicName=[[jsondata objectForKey:@"topicName"] retain];
            userName=[[[jsondata objectForKey:@"bbsUser"] objectForKey:@"fullName"] retain];
            creatTime=[[jsondata objectForKey:@"createTime"] retain];
            webContent=[[jsondata objectForKey:@"topicContent"] retain];
            //commentsArr=[[[NSArray alloc]init] retain];
            commentsArr=[[jsondata objectForKey:@"replayList"] retain];
            topicId=[[jsondata objectForKey:@"id"] retain];
            //NSLog(@"%@",creatTime);
        }
        dataArr=[[NSMutableArray alloc]initWithCapacity:0];
        [dataArr removeAllObjects];
    }
    else
    {
        flag=NO;
        NSLog(@"%@",jsondata);
        replayNum=[[jsondata objectForKey:@"code"] retain];
        replayResult=[[jsondata objectForKey:@"desc"] retain];
     //   [self displayResult:replayResult];
        if ([replayNum intValue]==1)
        {
            JsonService *jservice=[JsonService sharedManager];
            [jservice setDelegate:self];
            [jservice getBbs:refid];
            [replayNum release];
        }
    }
    [replayTabview reloadData];
    [activity stopAnimating];
    [activity release];
}

-(void)layOutView
{
   //table头部;
    titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.text=topicName;
    NSLog(@"%@",topicName);
    [titleLabel setNumberOfLines:0];
    titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    [titleLabel setTextColor:[UIColor blackColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    //UIFont *font = [UIFont systemFontOfSize:20];
    //设置一个行高上限
    CGSize size = CGSizeMake(320,2000);
    //计算实际frame大小，并将label的frame变成实际大小
    CGSize labelsize = [topicName sizeWithFont:[UIFont systemFontOfSize:24] constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
    [titleLabel setFrame:CGRectMake(30,0,self.view.frame.size.width-60, labelsize.height)];
    // [titleLabel sizeToFit];
    titleLabel.font=[UIFont systemFontOfSize:18];
    titleLabel.adjustsFontSizeToFitWidth=YES;
    titleLabel.backgroundColor=[UIColor clearColor];
    
    logoView=[[UIImageView alloc]initWithFrame:CGRectMake(10, titleLabel.frame.size.height+5, 40, 40)];
    [logoView setImage:[UIImage imageNamed:@"org.png"]];
    
    userLabel=[[UILabel alloc]initWithFrame:CGRectMake(55, titleLabel.frame.size.height+5, 100, 15)];
    [userLabel setBackgroundColor:[UIColor clearColor]];
    [userLabel setTextColor:[UIColor blueColor]];
    userLabel.text=userName;
    NSLog(@"%@",userName);
    userLabel.font=[UIFont systemFontOfSize:12.0];
    userLabel.textAlignment=UITextAlignmentLeft;
    
    timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(55, logoView.frame.origin.y+30, 100, 10)];
    [timeLabel setBackgroundColor:[UIColor clearColor]];
    [timeLabel setTextColor:[UIColor grayColor]];
    timeLabel.text=creatTime;
    NSLog(@"%@",creatTime);
    timeLabel.font=[UIFont systemFontOfSize:8.0];
    timeLabel.textAlignment=UITextAlignmentLeft;
    
    bbsWeb=[[UIWebView alloc]init];
    //    NSString *height_str= [bbsWeb stringByEvaluatingJavaScriptFromString:@"document.getElementById(\"foo\").offsetHeight"];
    //    int height = [height_str intValue];
    bbsWeb.tag=188;
    CGRect webframe=CGRectMake(0, timeLabel.frame.origin.y+15, 640, 0);
    bbsWeb.frame=webframe;
    //bbsWeb.scrollView.scrollEnabled=YES;
    // [bbsWeb setUserInteractionEnabled:NO];
    NSLog(@"webContent=%@",webContent);
    bbsWeb.delegate=self;
    [bbsWeb loadHTMLString:webContent baseURL:nil];

    
    if([commentsArr count]!=0)
    {
        comLabel=[[UILabel alloc]initWithFrame:CGRectMake(20,bbsWeb.frame.origin.y+bbsWebHeight+5, 100, 15)];
        [comLabel setBackgroundColor:[UIColor clearColor]];
        [comLabel setTextColor:[UIColor grayColor]];
        [comLabel setFont:[UIFont systemFontOfSize:10.0]];
        comLabel.text=[NSString stringWithFormat:@"评论:%d",[commentsArr count]];
        
        lineView=[[UIView alloc]initWithFrame:CGRectMake(0, bbsWeb.frame.origin.y+bbsWeb.frame.size.height+30, self.view.frame.size.width, 1.0f)];
        [lineView setBackgroundColor:[UIColor colorWithRed:193.0/255.0 green:196/255.0 blue:218/255.0 alpha:1.0]];
        
        triImage=[[UIImageView alloc]initWithFrame:CGRectMake(30, lineView.frame.origin.y-10, 16, 11)];
        [triImage setBackgroundColor:[UIColor clearColor]];
        [triImage setImage:[UIImage imageNamed:@"little_triangle.png"]];
    }

  //table尾部
    replayLabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 0, self.view.frame.size.width-10, 25)];
    [replayLabel setBackgroundColor:[UIColor colorWithRed:191.0/255.0 green:192.0/255.0 blue:191.0/255.0 alpha:1.0]];
    [replayLabel setTextColor:[UIColor blackColor]];
    [replayLabel setFont:[UIFont systemFontOfSize:10.0]];
    [replayLabel setText:@"  发表评论"];
    
    ftriImage=[[UIImageView alloc]initWithFrame:CGRectMake(25, 25-8, 12, 8)];
    [ftriImage setBackgroundColor:[UIColor clearColor]];
    [ftriImage setImage:[UIImage imageNamed:@"little_triangle.png"]];
    
    //加入评论框
    replayTextview=[[UITextView alloc]initWithFrame:CGRectMake(5, replayLabel.frame.origin.y+replayLabel.frame.size.height+5,self.view.frame.size.width-10,120)];
    CALayer *textViewlayer=replayTextview.layer;
    textViewlayer.borderWidth=1.0;
    textViewlayer.borderColor=[[UIColor blackColor] CGColor];
    textViewlayer.masksToBounds=YES;
    textViewlayer.cornerRadius=5.0;
    [replayTextview setEditable:YES];
    replayTextview.text=NSLocalizedString(@"write replay", @"write replay");
    replayTextview.textColor=[UIColor grayColor];
    replayTextview.font = [UIFont fontWithName:@"Arial" size:18.0];
    replayTextview.keyboardType=UIKeyboardTypeDefault;
    replayTextview.returnKeyType = UIReturnKeyDone;//返回键的类型
    replayTextview.autoresizingMask = UIViewAutoresizingFlexibleHeight;//自适应高度
    replayTextview.delegate=self;
    [replayTextview setBackgroundColor:[UIColor clearColor]];
    
    //发表评论btn
    replayBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, replayTextview.frame.origin.y+replayTextview.frame.size.height+25, self.view.frame.size.width-20, 23)];
    [replayBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
    CALayer *btnlayer=replayBtn.layer;
    //btnlayer.borderWidth=1.0;
    btnlayer.borderColor=[[UIColor blackColor] CGColor];
    btnlayer.masksToBounds=YES;
    btnlayer.cornerRadius=4.0;
    [replayBtn setBackgroundColor:[UIColor colorWithRed:76.0/255.0 green:150.0/255.2 blue:215.0/255.0 alpha:1.0]];
    [replayBtn setTitle:@"发表评论" forState:UIControlStateNormal];
    replayBtn.titleLabel.textColor=[UIColor blackColor];
    [replayBtn addTarget:self action:@selector(replayBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
}




- (void)viewDidLoad
{
    UIBarButtonItem *done =[[UIBarButtonItem alloc] initWithTitle:@"评论" style:UIBarButtonItemStyleBordered target:self action:@selector(replayShow:)];
    self.navigationItem.rightBarButtonItem = done;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)replayShow:(UIBarButtonItem *)item
{
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if([commentsArr count]!=0)
    {
        return [commentsArr count];
    }
    return 1;
}


////设置分区头标题
//-(NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return @"ios 43";
//}

//设置分区头部标题行高

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if ([commentsArr count]!=0)
    {
        NSLog(@"heightForHeader=%f",titleLabel.frame.size.height+5+logoView.frame.size.height+15+bbsWeb.frame.size.height+5+comLabel.frame.size.height+30+lineView.frame.size.height);
        return titleLabel.frame.size.height+5+logoView.frame.size.height+15+bbsWeb.frame.size.height+5+comLabel.frame.size.height+30+lineView.frame.size.height;
    }
    else
    {
        return titleLabel.frame.size.height+5+logoView.frame.size.height+15+bbsWeb.frame.size.height+5;
    }
}

////设置分区尾部标题
//-(NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
//{
//    return @"bu gao su ni !";
//}

//设置分区尾部标题行高
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    NSLog(@"heightForFooter=%f",replayLabel.frame.size.height+5+replayTextview.frame.size.height+25+replayBtn.frame.size.height);
    return replayLabel.frame.size.height+5+replayTextview.frame.size.height+25+replayBtn.frame.size.height;

}

//去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = replayLabel.frame.size.height+5+replayTextview.frame.size.height+25+replayBtn.frame.size.height;
    if([commentsArr count]!=0)
    {
      sectionHeaderHeight=titleLabel.frame.size.height+5+logoView.frame.size.height+15+bbsWeb.frame.size.height+5+comLabel.frame.size.height+30+lineView.frame.size.height;
    }
    else
    {
        sectionHeaderHeight=titleLabel.frame.size.height+5+logoView.frame.size.height+15+bbsWeb.frame.size.height+5;
    }
    CGFloat sectionFooterHeight = replayLabel.frame.size.height+5+replayTextview.frame.size.height+25+replayBtn.frame.size.height;
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0)
    {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight)
    {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
    else if(scrollView.contentOffset.y>=self.view.frame.size.height-sectionFooterHeight)
    {
        scrollView.contentInset = UIEdgeInsetsMake(self.view.frame.size.height-sectionFooterHeight, 0, 0, 0);
    }
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *HeaderView = [[UIView alloc]init];
    if ([commentsArr count]!=0)
    {
        HeaderView.frame=CGRectMake(0, 0, self.view.frame.size.width, titleLabel.frame.size.height+5+logoView.frame.size.height+15+bbsWeb.frame.size.height+5+comLabel.frame.size.height+30+lineView.frame.size.height);
    }
    else
    {
        HeaderView.frame=CGRectMake(0, 0, self.view.frame.size.width, titleLabel.frame.size.height+5+logoView.frame.size.height+15+bbsWeb.frame.size.height+5);
    }
    [HeaderView addSubview:titleLabel];
    [HeaderView addSubview:logoView];
    [HeaderView addSubview:userLabel];
    [HeaderView addSubview:timeLabel];
    [HeaderView addSubview:bbsWeb];
    if([commentsArr count]!=0)
    {
        [HeaderView addSubview:comLabel];
        [HeaderView addSubview:lineView];
        [HeaderView addSubview:triImage];
    }
    return HeaderView;
}


#pragma mark - webview
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
   if (webView.tag==188)
   {
     CGRect frame = webView.frame;
     frame.size.height = 1;
     webView.frame = frame;
     CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
     frame.size = fittingSize;
     webView.frame = frame;
     bbsWebHeight=frame.size.height;
     NSLog(@"bbsWeb.height=%f",webView.frame.size.height);
    //[self tableView:replayTabview heightForHeaderInSection:0];
   }
}


-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *FooterView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, replayLabel.frame.size.height+5+replayTextview.frame.size.height+25+replayBtn.frame.size.height)];
    [FooterView addSubview:replayLabel];
    [FooterView addSubview:ftriImage];
    [FooterView addSubview:replayTextview];
    [FooterView addSubview:replayBtn];
    return FooterView;
}


- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"bbsliCell";
    bbsCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[bbsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.logoImage setImage:[UIImage imageNamed:@"org.png"]];
    cell.nameLabel.text=[[[commentsArr objectAtIndex:indexPath.row] objectForKey:@"bbsUser"] objectForKey:@"fullName"];
    cell.nameLabel.textColor=[UIColor grayColor];
    cell.nameLabel.font=[UIFont systemFontOfSize:8.0];
    cell.timeLabel.text=[[commentsArr objectAtIndex:indexPath.row] objectForKey:@"createTime"];
    cell.timeLabel.textColor=[UIColor grayColor];
    cell.timeLabel.font=[UIFont systemFontOfSize:8.0];
    cell.contentWeb.delegate=self;
    [cell.contentWeb loadHTMLString:[[commentsArr objectAtIndex:indexPath.row] objectForKey:@"content"] baseURL:nil];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//     UITableViewCell *cell = [self tableView:tableView1 cellForRowAtIndexPath:indexPath];
//     return cell.frame.size.height;
    return 178;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

#pragma mark - textview delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    [textView becomeFirstResponder];
    textView.text=nil;
    [textView setTextColor:[UIColor blackColor]];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    if(textView.text==nil)
    {
        textView.text=@"写评论...";
    }
    replayContent=[[NSString stringWithFormat:@"%@",textView.text] retain];
    //NSLog(@"replayContent=%@",replayContent);
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)replayBtnPressed:(UIButton*)sender
{
    //NSString *startStr=[NSString stringWithFormat:@"写评论..."];
    if(replayContent==nil)
    {
        [self displayResult:@"请输入内容"];
    }
    else
    {
        NSLog(@"00000");
        NSLog(@"refid=%@",refid);
        NSLog(@"topicId=%@",topicId);
        NSLog(@"content=%@",replayContent);
        JsonService *jservicereplay=[JsonService sharedManager];
        [jservicereplay setDelegate:self];
        [jservicereplay postReplay:refid second:topicId third:replayContent];
        // [topicId release];
        // [replayContent release];
    }
}


-(void)displayResult:(NSString*)text
{
    UILabel *tellLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-35,replayBtn.frame.origin.y-80, 70, 30)];
    tellLabel.layer.masksToBounds=YES;
    tellLabel.layer.cornerRadius=4.0;
    [tellLabel setBackgroundColor:[UIColor colorWithRed:7.0/255.0 green:16.0/255.0 blue:33.0/255.0 alpha:1.0]];
    [tellLabel setText:text];
    [tellLabel setFont:[UIFont systemFontOfSize:10.0]];
    [tellLabel setTextAlignment:NSTextAlignmentCenter];
    [tellLabel setTextColor:[UIColor whiteColor]];
    [bbsScrollview addSubview:tellLabel];
    [replayResult release];
    [self performSelector:@selector(removetellLabel:) withObject:tellLabel afterDelay:1.0];
}

-(void)removetellLabel:(UILabel*)label
{
    [label removeFromSuperview];
    [label release];
}

@end
