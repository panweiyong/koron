//
//  KoronRootTabBarContller.m
//  moffice
//
//  Created by Mac Mini on 13-9-29.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronRootTabBarContller.h"
#import "BadgeView.h"

@implementation KoronRootTabBarContller
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor colorWithRed:62/255.0 green:67/255.0 blue:72/255.0 alpha:1];
//        selxf.backgroundColor = [UIColor redColor];
        
        //主页
        UIView *homePageView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width/4, self.bounds.size.height)];
        homePageView.tag = 100;
        
        UITapGestureRecognizer *homePageTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickOnView:)];
        [homePageView addGestureRecognizer:homePageTap];
        [self addSubview:homePageView];
        
        _homePageImageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 5, 32, 32)];
        [_homePageImageView setImage:[UIImage imageNamed:@"tab_home_nor.png"]];
        [homePageView addSubview:_homePageImageView];
        
        
        //通知
        UIView *noticeView = [[UIView alloc]initWithFrame:CGRectMake(self.bounds.size.width/4, 0, self.bounds.size.width/4, self.bounds.size.height)];
        noticeView.tag = 101;
        [self addSubview:noticeView];
        
        UITapGestureRecognizer *noticeTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickOnView:)];
        [noticeView addGestureRecognizer:noticeTap];
        
        _noticeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 5, 32, 32)];
        [_noticeImageView setImage:[UIImage imageNamed:@"tab_note_nor.png"]];
        [noticeView addSubview:_noticeImageView];
        
        
        //私信
        UIView *privateMessageView = [[UIView alloc]initWithFrame:CGRectMake(self.bounds.size.width/4 * 2, 0, self.bounds.size.width/4, self.bounds.size.height)];
        
        privateMessageView.tag = 102;
        [self addSubview:privateMessageView];
        
        UITapGestureRecognizer *msgTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickOnView:)];
        [privateMessageView addGestureRecognizer:msgTap];
        
        _privateMessageImageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 5, 32, 32)];
        [_privateMessageImageView setImage:[UIImage imageNamed:@"tab_msg_nor.png"]];
        [privateMessageView addSubview:_privateMessageImageView];
        
        
        
        
        //邮件
        UIView *mailView = [[UIView alloc]initWithFrame:CGRectMake(self.bounds.size.width/4 * 3, 0, self.bounds.size.width/4, self.bounds.size.height)];
        mailView.tag = 103;
        [self addSubview:mailView];
        
        UITapGestureRecognizer *mailTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(clickOnView:)];
        [mailView addGestureRecognizer:mailTap];
        
        _mailImageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 5, 32, 32)];
        [_mailImageView setImage:[UIImage imageNamed:@"tab_mail_nor.png"]];
        [mailView addSubview:_mailImageView];
        
        
        _selectedView = [[UIImageView alloc]initWithFrame:CGRectMake(2, -10, self.bounds.size.width/4 -4 , self.bounds.size.height + 10)];
        [_selectedView setImage:[UIImage imageNamed:@"tab_btn.png"]];
        [homePageView addSubview:_selectedView];
        
        _selectedImageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 15, 32, 32)];
        [_selectedImageView setImage:[UIImage imageNamed:@"tab_home_sel.png"]];
        [_selectedView addSubview:_selectedImageView];
        
//        for (NSInteger index = 0; index < 3; index++) {
//            BadgeView *badgeView = [[BadgeView alloc] initWithFrame:CGRectMake(80 + 40 + 80 * index, 2, 15, 15)];
//            badgeView.tag = 201 + index;
//            [self addSubview:badgeView];
//            
//            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickOnView:)];
//            [badgeView addGestureRecognizer:tap];
//        }
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)clickOnView:(UITapGestureRecognizer *)gesture {

        UIView *view = nil;
    
    if ([gesture.view isKindOfClass:[BadgeView class]]) {
        if (gesture.view.tag == 201) {
            view = [self viewWithTag:101];
        } else if (gesture.view.tag == 202) {
            view = [self viewWithTag:102];
        } else {
            view = [self viewWithTag:103];
        }
    } else {
        view = gesture.view;
    }
    
    [_selectedView removeFromSuperview];
    [_selectedView setFrame:CGRectMake(view.frame.origin.x + 2, -10, self.bounds.size.width / 4- 4, self.bounds.size.height + 10)];
    [self addSubview:_selectedView];
    if (view.tag == 100 ) {
        [_selectedImageView setImage:[UIImage imageNamed:@"tab_home_sel.png"]];
    }else if (view.tag == 101){
        [_selectedImageView setImage:[UIImage imageNamed:@"tab_note_sel.png"]];
        BadgeView *badgeView = (BadgeView *)[self viewWithTag:201];
        [self bringSubviewToFront:badgeView];
    }else if (view.tag == 102){
        [_selectedImageView setImage:[UIImage imageNamed:@"tab_msg_sel.png"]];
        BadgeView *badgeView = (BadgeView *)[self viewWithTag:202];
        [self bringSubviewToFront:badgeView];
    }else {
        [_selectedImageView setImage:[UIImage imageNamed:@"tab_mail_sel.png"]];
        BadgeView *badgeView = (BadgeView *)[self viewWithTag:203];
        [self bringSubviewToFront:badgeView];
    }
    
    if (delegate != nil && [delegate respondsToSelector:@selector(clickTabBarItem:)]) {
        [delegate performSelector:@selector(clickTabBarItem:) withObject:[NSNumber numberWithInteger:view.tag - 100]];
    }
}

@end
