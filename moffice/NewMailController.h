//
//  NewMailController.h
//  moffice
//
//  Created by yangxi zou on 12-1-18.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TITokenFieldView.h"
#import "JsonService.h"
#import "SMUtils.h"
#import "Names.h"
#import "ModalAlert.h"

@interface NewMailController :UIViewController <UIWebViewDelegate,TITokenFieldViewDelegate, UITextViewDelegate,UIPickerViewDelegate, UIPickerViewDataSource, UIActionSheetDelegate>

{
    TITokenFieldView * tokenFieldView;
    UITextField * subjectView;
    UIButton * fromView;
   // UITextView * bodyView;
     UIWebView * bodyView;
    CGFloat keyboardHeight;
    
    NSString *to;
    NSString *from;
    NSString *subject;
    NSString *content;
    BOOL isintranet;
    
    NSString *accounts;
    NSArray *accountarray;
    UIView *messageView;
    
    BOOL issending;
    
}
@property (nonatomic,retain)NSString *to;
@property (nonatomic,retain)NSArray *accountarray;
@property (nonatomic,retain)NSString *from;
@property (nonatomic,retain)NSString *subject;
@property (nonatomic,retain)NSString *content;
@property (nonatomic)BOOL isintranet;

@property (nonatomic,retain)NSString *accounts;

@end
