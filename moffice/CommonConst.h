//
//  CommonColor.h
//  newstock
//
//  Created by yangxi zou on 10-12-2.
//  Copyright 2010 shenzhen shuangmeng  computer co.,ltd. All rights reserved.

//[[[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"TIPSCELLBG.png"]]autorelease]

#define TIPSCELL_BACKGROUND [UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1]
#define MSGCELL_BACKGROUND [UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1]
#define MAILCELL_BACKGROUND [UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1]
#define WFCELL_BACKGROUND [UIColor colorWithRed:237.0f/255.0f green:237.0f/255.0f blue:237.0f/255.0f alpha:1]
#define MSGDETAILSCELL_BACKGROUND [UIColor colorWithRed:0.859f green:0.886f blue:0.929f alpha:1.0f]
#define TIPSCELLLINE_BACKGROUND [UIColor colorWithRed:203.0f/255.0f green:203.0f/255.0f blue:203.0f/255.0f alpha:1].CGColor
#define WFELLLINE_BACKGROUND [UIColor colorWithRed:203.0f/255.0f green:203.0f/255.0f blue:203.0f/255.0f alpha:1].CGColor
#define MAILLLINE_BACKGROUND [UIColor colorWithRed:203.0f/255.0f green:203.0f/255.0f blue:203.0f/255.0f alpha:1].CGColor
#define DETAILPAGE_BG [UIColor colorWithRed:221.0/255.0 green:239.0/255.0 blue:248.0/255.0 alpha:1.0]



#define ABOUTLINK @"http://www.koronsoft.com/"
#define SUBMITMOBILELINK @"http://www.koronsoft.com/?mobile="
                              
#define SYSBARBUTTON(ITEM, SELECTOR) [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:ITEM target:self action:SELECTOR] autorelease]
#define BARBUTTON(TITLE, SELECTOR) 	[[[UIBarButtonItem alloc] initWithTitle:TITLE style:UIBarButtonItemStylePlain target:self action:SELECTOR] autorelease]

struct ProcessResult {
    int code;
    NSString *desc;
};
typedef struct ProcessResult ProcessResult;

CG_INLINE ProcessResult ProcessResultMake(int code, NSString * desc);

CG_INLINE ProcessResult
ProcessResultMake(int code, NSString * desc)
{
    ProcessResult result;
    result.code = code;
    result.desc = desc;
    return result;
}

