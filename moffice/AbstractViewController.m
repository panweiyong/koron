//
//  AbstractViewController.m
//  moffice
//
//  Created by yangxi zou on 12-1-11.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "AbstractViewController.h"

@implementation AbstractViewController




- (void)requestFailed:(ASIHTTPRequest *)request
{
    //NSError *error = [request error];
    UIView *errAlert=[SMView showAlert: NSLocalizedString(@"Net Error",@"Net Error")];
    errAlert.center = self.view.center;
    [self.view addSubview:errAlert];
    [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         errAlert.alpha=0;
                     }
                     completion:^(BOOL finished){
                         if (finished){
                             [errAlert removeFromSuperview];
                         }
                     }
     ];
     [[self.view viewWithTag:10001] removeFromSuperview];
     [self doneLoadingTableViewData];
}

//完成请求隐藏View
- (void)doneLoadingTableViewData{	
	//  model should call this when its done loading
	_reloading = NO;
	[_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:tableView];
}
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	[self reloadTableViewDataSource];
	//[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    /*UIActivityIndicatorView *processAlert=[SMView showProcessing];
    processAlert.center = tableView.center;
    processAlert.tag=10001;
    [self.view addSubview:processAlert];
    [processAlert release];*/
}
  
#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{	
	[_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	[_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

- (void)viewDidUnload {
	_refreshHeaderView=nil;
}

//成功接收到返回的数据
- (void)requestDataFinished:(id)jsondata//request
{
    NSLog(@"jsonData=%@",jsondata);

    //CCLOG(@"loadding tips requestDataFinished.");
    [tableView reloadData];
    
    [self doneLoadingTableViewData];
    [[self.view viewWithTag:10001] removeFromSuperview];
    //[tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}


- (void)dealloc {
	_refreshHeaderView = nil;
    [super dealloc];
}

- (void)reloadTableViewDataSource{	
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
}

//超时处理函数
-(void)timeoutQuit
{
    // [self.tabBarController.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods


- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	return _reloading; // should return if data source model is reloading
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	return [NSDate date]; // should return date data source was last changed
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}



@end
