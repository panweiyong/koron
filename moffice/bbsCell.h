//
//  bbsCell.h
//  moffice
//
//  Created by koron on 13-6-5.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface bbsCell : UITableViewCell<UIWebViewDelegate>
{
    UIImageView *logoImage;
    UILabel  *nameLabel;
    UILabel  *timeLabel;
    UIWebView *contentWeb;
}
@property(nonatomic,retain) UIImageView *logoImage;
@property(nonatomic,retain) UILabel  *nameLabel;
@property(nonatomic,retain) UILabel  *timeLabel;
@property(nonatomic,retain) UIWebView *contentWeb;
@end
