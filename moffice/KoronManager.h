//
//  KoronManager.h
//  Koron-mofffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "KoronUser.h"




@interface KoronManager : NSObject {
    //第一次启动应用,判断是否使用启动动画
    BOOL isFirst;
    NSMutableDictionary *mdUser;
}

@property (nonatomic,assign)BOOL isFirst;


+ (KoronManager *)sharedManager;

//数据保存到本地(封装NSUserDefaults)
- (void)saveObject:(id)value forKey:(NSString *)key;
//通过key  获得本地保存的数据
- (id)getObjectForKey:(NSString *)key;

- (NSString * )getMD5:(NSString *)string;

- (BOOL) initUserDictionary:(NSDictionary *)dic;
- (KoronUser *) getUserById:(NSString *)userId;
- (NSString *) getNameById:(NSString *)userId;

@end
