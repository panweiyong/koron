//
//  KoronServerView.h
//  moffice
//
//  Created by Mac Mini on 13-9-26.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronManager.h"
#import <QuartzCore/QuartzCore.h>

@interface KoronServerView : UIView <UITextFieldDelegate>{
    UILabel *_Label;
    
    UIImageView *_ImageView;
    
    UITextField *_textField;
    
    CGRect _rect;
    
    UIView *_topView;
    
    UIView *_bottomView;
    
    KoronManager *_manager;
}

@property (nonatomic,strong)UITextField *textField;

@end
