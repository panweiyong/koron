//
//  personListCell.m
//  moffice
//
//  Created by lijinhua on 13-6-26.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "personListCell.h"

@implementation personListCell
@synthesize CellnumLabel,CellselectBtn,CellNameLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawCell];
    }
    return self;
}

-(void)drawCell
{
    CellnumLabel=[[UILabel alloc]initWithFrame:CGRectMake(25, 0,35, 35)];
    [CellnumLabel setBackgroundColor:[UIColor clearColor]];
    [self addSubview:CellnumLabel];
    
    CellselectBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    CellselectBtn.frame=CGRectMake(75, 5, 30, 30);
    CellselectBtn.backgroundColor=[UIColor clearColor];
    //[CellselectBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
    //[selectAllBtn setBackgroundImage:[UIImage imageNamed:@"wover.png"] forState:UIControlStateHighlighted];
    [self addSubview:CellselectBtn];
    
    CellNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(135, 0, self.frame.size.width-135, 40)];
    CellNameLabel.backgroundColor=[UIColor clearColor];
    CellNameLabel.adjustsFontSizeToFitWidth=YES;
    [self addSubview:CellNameLabel];
}

-(void)dealloc
{
   [CellnumLabel release];
   [CellNameLabel release];
   [super dealloc];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
