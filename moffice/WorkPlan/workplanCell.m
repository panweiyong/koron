//
//  workplanCell.m
//  moffice
//
//  Created by lijinhua on 13-7-3.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "workplanCell.h"

@implementation workplanCell
@synthesize typeLabel,NameLabel,nameOfplan,statusImage,dateOfcreate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawCell];
    }
    return self;
}

-(void)dealloc
{
    [typeLabel release];
    [NameLabel release];
    [nameOfplan release];
    [dateOfcreate release];
    [statusImage release];
    
    [super dealloc];
}

-(void)drawCell
{
    typeLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 2, 50, 15)];
    [typeLabel setBackgroundColor:[UIColor clearColor]];
    typeLabel.textAlignment=UITextAlignmentLeft;
    [typeLabel setFont:[UIFont systemFontOfSize:12.0]];
    [self addSubview:typeLabel];
    
    NameLabel=[[UILabel alloc]initWithFrame:CGRectMake(62, 2, 100, 15)];
    [NameLabel setBackgroundColor:[UIColor clearColor]];
    NameLabel.textAlignment=UITextAlignmentLeft;
    [NameLabel setFont:[UIFont systemFontOfSize:12.0]];
    NameLabel.textColor=[UIColor grayColor];
    [self addSubview:NameLabel];
    
    nameOfplan=[[UILabel alloc]initWithFrame:CGRectMake(10, 20, 152, 18)];
    [nameOfplan setBackgroundColor:[UIColor clearColor]];
    nameOfplan.textAlignment=UITextAlignmentLeft;
    [nameOfplan setFont:[UIFont systemFontOfSize:14.0]];
    [self addSubview:nameOfplan];
    
    statusImage=[[UIImageView alloc]initWithFrame:CGRectMake(165, 5, 43, 30)];
    [statusImage setBackgroundColor:[UIColor clearColor]];
    [self addSubview:statusImage];
    
    dateOfcreate=[[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-110, 5, 115, 20)];
    [dateOfcreate setBackgroundColor:[UIColor clearColor]];
    [dateOfcreate setFont:[UIFont systemFontOfSize:12.0]];
    dateOfcreate.textAlignment=UITextAlignmentLeft;
    [self addSubview:dateOfcreate];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
