//
//  headCell.h
//  moffice
//
//  Created by lijinhua on 13-7-8.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface headCell : UITableViewCell
{
    UILabel *titleLabel;
    UIView *lineView;
    UIWebView *contentWeb;
    UIImageView *statusImage;
    UILabel *timeLabel;
    UITextView *personView;
    UITextView *connectView;
    UITextView *customerView;
    UIButton *revButton;
    UIButton *sumButton;
}
@property(nonatomic,retain) UILabel *titleLabel;
@property(nonatomic,retain) UIView *lineView;
@property(nonatomic,retain) UIWebView *contentWeb;
@property(nonatomic,retain) UIImageView *statusImage;
@property(nonatomic,retain) UILabel *timeLabel;
@property(nonatomic,retain) UITextView *personView;
@property(nonatomic,retain) UITextView *connectView;
@property(nonatomic,retain) UITextView *customerView;
@property(nonatomic,retain) UIButton *revButton;
@property(nonatomic,retain) UIButton *sumButton;
@end
