//
//  WritePlanViewController.m
//  moffice
//
//  Created by lijinhua on 13-6-21.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "WritePlanViewController.h"

@interface WritePlanViewController ()

@end

@implementation WritePlanViewController
@synthesize planTitle,planType,planContent,startDate,endDate;
@synthesize weekStr,planId;
@synthesize perArr,connectArr,customerArr;
@synthesize names = _names;
//@synthesize knowDatalist,connectDatalist,customerDatalist;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _names = [[NSMutableArray alloc]init];
    }
    return self;
}

-(void)dealloc
{
   
    [planTitle release];
    [contentView release];
    [personList release];
    [knowDatalist release];
    [connectDatalist release];
    [customerDatalist release];
    [perArr release];
    [connectArr release];
    [customerArr release];
    [_names release];
    _names = nil;
     [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"写工作计划";
    UIBarButtonItem *done =[[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleBordered target:self action:@selector(saveWorkplan:)];
    self.navigationItem.rightBarButtonItem = done;
    
    UILabel *topicLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, 40, 40)];
    [topicLabel setBackgroundColor:[UIColor clearColor]];
    [topicLabel setText:@"主题:"];
    [self.view addSubview:topicLabel];
    [topicLabel release];
    
    knowDatalist=[[NSMutableArray alloc]initWithCapacity:0];
    [knowDatalist removeAllObjects];
    if([perArr count]!=0)
    {
      [knowDatalist addObjectsFromArray:perArr];
    }
    connectDatalist=[[NSMutableArray alloc]initWithCapacity:0];
    [connectDatalist removeAllObjects];
    if([connectArr count]!=0)
    {
        [connectDatalist addObjectsFromArray:connectArr];
    }
    //[connectDatalist addObjectsFromArray:[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",nil]];
    customerDatalist=[[NSMutableArray alloc]initWithCapacity:0];
    [customerDatalist removeAllObjects];
    if([customerArr count]!=0)
    {
        [customerDatalist addObjectsFromArray:customerArr];
    }

    //[customerDatalist addObjectsFromArray:[NSArray arrayWithObjects:@"1",@"2",@"3",@"4",nil]];
    
    planTitleField=[[UITextField alloc]initWithFrame:CGRectMake(55, 13, self.view.frame.size.width-65, 34)];
    [planTitleField setBackgroundColor:[UIColor clearColor]];
    [planTitleField setBorderStyle:UITextBorderStyleBezel];
    //planTitle.placeholder=NSLocalizedString(@"Customer name", @"Customer name");
    planTitleField.textAlignment=UITextAlignmentLeft;
    planTitleField.font = [UIFont fontWithName:@"helvetica" size:18.0];  //字体和大小设置
    planTitleField.adjustsFontSizeToFitWidth=YES;
//    planTitle.autoresizingMask=UIViewAutoresizingFlexibleHeight;
//    planTitle.enabled=YES;
    planTitleField.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    planTitleField.keyboardAppearance = UIKeyboardAppearanceAlert ;
    planTitleField.returnKeyType = UIReturnKeyDone;
    planTitleField.delegate=self;
    [self.view addSubview:planTitleField];
    [self makeSureplantitle];
    
    contentView=[[UITextView alloc]initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-20, 200)];
    contentView.tag=10021;
    [contentView setBackgroundColor:[UIColor whiteColor]];
    [contentView setEditable:YES];
    contentView.layer.backgroundColor = [[UIColor clearColor] CGColor];
    contentView.layer.borderColor = [[UIColor blackColor] CGColor];
    contentView.layer.borderWidth = 0.5f;
    //contentView.layer.cornerRadius = 8.0f;
    [contentView.layer setMasksToBounds:YES];
    //[saysomeThing setBorderStyle:UITextBorderStyleLine];
    contentView.font = [UIFont fontWithName:@"Arial" size:18.0];
    contentView.text=planContent;
    contentView.keyboardType=UIKeyboardTypeDefault;
    contentView.returnKeyType = UIReturnKeyDone;//返回键的类型
    contentView.autoresizingMask = UIViewAutoresizingFlexibleHeight;//自适应高度
    contentView.delegate = self;
    [self.view addSubview:contentView];
    
    personList=[[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    personList.backgroundColor=[UIColor clearColor];
    personList.backgroundView=nil;
    personList.bounces=NO;
    personList.sectionHeaderHeight=0.0f;
    personList.sectionFooterHeight=0.0f;
    personList.delegate=self;
    personList.dataSource=self;
    [self.view addSubview:personList];
    [self addTableview];
	// Do any additional setup after loading the view.
}

-(void)makeSureplantitle
{
    if(planTitle!=nil)
    {
        planTitleField.text=planTitle;
        return;
    }
    if([planType isEqualToString:@"day"])
    {
        if(startDate!=nil)
        {
            NSString *cutDateStr=[startDate substringToIndex:10];
            planTitleField.text=[NSString stringWithFormat:@"%@工作计划",cutDateStr];
            planTitle=planTitleField.text;
        }
    }
    else if([planType isEqualToString:@"week"])
    {
        if(startDate!=nil)
        {
            NSString *cutDateStr=[startDate substringToIndex:4];
            planTitleField.text=[NSString stringWithFormat:@"%@年第%@周工作计划",cutDateStr,weekStr];
            planTitle=planTitleField.text;
        }
    }
    else if([planType isEqualToString:@"month"])
    {
        if(startDate!=nil)
        {
            NSString *yearcutDateStr=[startDate substringToIndex:4];
            NSString *monthcutDateStr=[startDate substringWithRange:NSMakeRange(5, 2)];
            planTitleField.text=[NSString stringWithFormat:@"%@年%@月工作计划",yearcutDateStr,monthcutDateStr];
            planTitle=planTitleField.text;
        }
    }
    else
    {
        return;
    }
}

-(void)saveWorkplan:(id)sender
{
    
    [planTitleField resignFirstResponder];
    [contentView resignFirstResponder];
    if(planTitle==nil||[planTitle length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"主题不能为空!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    if(planContent==nil||[planContent length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"内容不能为空!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    [self showTellLabel];
    NSMutableArray *assArray=[[NSMutableArray alloc]initWithCapacity:0];
    [assArray removeAllObjects];
    if([knowDatalist count]!=0)
    {
        BOOL isEcho = YES;
        for(int i=[knowDatalist count] - 1 ;i >= 0;i--)
        {
            NSLog(@" _names  %@",_names);
            NSLog(@"%@",[knowDatalist objectAtIndex:i]);
            for (NSString *name in _names) {
                if ([name isEqualToString:[[knowDatalist objectAtIndex:i] objectForKey:@"assName"]]) {
                    isEcho = NO;
                    break;
                }
            }
            if (isEcho) {
                NSMutableDictionary *infoDic=[[NSMutableDictionary alloc]init];
                [infoDic setValue:@"知晓人" forKey:@"assName"];
                [infoDic setValue:[[knowDatalist objectAtIndex:i] objectForKey:@"id"] forKey:@"id"];
                [infoDic setValue:[[knowDatalist objectAtIndex:i] objectForKey:@"name"] forKey:@"name"];
                [assArray addObject:infoDic];
                [infoDic release];
            }
        }
    }
    if([connectDatalist count]!=0)
    {
        for(int i=0;i<[connectDatalist count];i++)
        {
            NSMutableDictionary *infoDic=[[NSMutableDictionary alloc]init];
            [infoDic setValue:@"联系人" forKey:@"assName"];
            [infoDic setValue:[[connectDatalist objectAtIndex:i] objectForKey:@"id"] forKey:@"id"];
            [infoDic setValue:[[connectDatalist objectAtIndex:i] objectForKey:@"name"] forKey:@"name"];
            [assArray addObject:infoDic];
            [infoDic release];
        }
    
    }
    if([customerDatalist count]!=0)
    {
        for(int i=0;i<[customerDatalist count];i++)
        {
            NSMutableDictionary *infoDic=[[NSMutableDictionary alloc]init];
            [infoDic setValue:@"关联客户" forKey:@"assName"];
            [infoDic setValue:[[customerDatalist objectAtIndex:i] objectForKey:@"id"] forKey:@"id"];
            [infoDic setValue:[[customerDatalist objectAtIndex:i] objectForKey:@"name"] forKey:@"name"];
            [assArray addObject:infoDic];
            [infoDic release];
        }
    }
    NSString *jsonStr=[assArray JSONRepresentation];
    
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    NSLog(@"planId=%@,planType=%@,planTitle=%@,planContent=%@,startDate=%@,endDate=%@,jsonStr=%@",planId,planType,planTitle,planContent,startDate,endDate,jsonStr);
    [jservice addWorkPlan:planId type:planType title:planTitle content:planContent start:startDate end:endDate ass:jsonStr];
    [assArray release];
}

-(void)showTellLabel
{
    UIView * backView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.tag=101;
    backView.layer.cornerRadius=5.0f;
    [backView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:backView];
    
    UILabel* Telllabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50,self.view.frame.size.height/2-40, 100,80)];
    Telllabel.tag=102;
    [Telllabel setBackgroundColor:[UIColor grayColor]];
    Telllabel.layer.cornerRadius=5.0f;
    [self.view addSubview:Telllabel];
    
    activity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activity setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
    [self.view addSubview:activity];
    [activity startAnimating];
}

-(void)removeTelllabel
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==101||view.tag==102)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
    [activity stopAnimating];
    [activity removeFromSuperview];
}

-(void)showResultLabel
{
    UILabel* resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-30, self.view.frame.size.height-80, 60, 40)];
    resultLabel.tag=100;
    resultLabel.text=descStr;
    resultLabel.textAlignment=UITextAlignmentCenter;
    resultLabel.adjustsFontSizeToFitWidth=YES;
    resultLabel.layer.cornerRadius=5.0f;
    resultLabel.backgroundColor=[UIColor grayColor];
    resultLabel.textColor=[UIColor whiteColor];
    [self.view addSubview:resultLabel];
}

-(void)removeResultLabel
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==100)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}

-(void)requestDataFinished:(id)jsondata
{
    NSLog(@"jsondata=%@",jsondata);
    [self removeTelllabel];
    descStr=[NSString stringWithFormat:@"%@",[jsondata objectForKey:@"desc"]];
    [self showResultLabel];
    [self performSelector:@selector(removeResultLabel) withObject:nil afterDelay:2.0f];
    if([[jsondata objectForKey:@"code"] intValue]==0)
    {
        planTitleField.text=nil;
        planTitle=nil;
        contentView.text=nil;
        [knowDatalist removeAllObjects];
        [connectDatalist removeAllObjects];
        [customerDatalist removeAllObjects];
        [personList reloadData];
        if(planId==nil)
        {
             planId=[jsondata objectForKey:@"planId"];
        }
        else
        {
             [self.navigationController popViewControllerAnimated:YES];
        }
//        UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
//        temporaryBarButtonItem.title = @"返回";
//        self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
//        PlandetailViewController *planDetail=[[PlandetailViewController alloc]initWithNibName:nil bundle:nil];
//        [planDetail setPlanId:planId];
//        [self.navigationController pushViewController:planDetail animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)addTableview
{
    NSLog(@"personArr=%d",[knowDatalist count]);
    NSLog(@"connectArr=%d",[connectDatalist count]);
    NSLog(@"customerArr=%d",[customerDatalist count]);
    float tableHeight=([knowDatalist count]+[connectDatalist count]+[customerDatalist count]+3)*40;
    NSLog(@"tableHeight=%f",tableHeight);
    if(tableHeight<=self.view.frame.size.height-245)
    {
        personList.frame=CGRectMake(10, 245, self.view.frame.size.width-20, tableHeight);
    }
    else
    {
       personList.frame=CGRectMake(10, 245, self.view.frame.size.width-20, self.view.frame.size.height-245);
    }
    [personList reloadData];
}

-(void)callbackPersonInfo:(NSMutableArray*)Arr other:(int)flag
{
   if(flag==0)
   {   
       for(int i=0;i<[Arr count];i++)
      {
        NSDictionary *dic=[Arr objectAtIndex:i];
        if(dic!=nil && ![knowDatalist containsObject:dic])
        {
            [knowDatalist addObject:dic];
        }
      }
   }
   else if(flag==1)
   {
       for(int i=0;i<[Arr count];i++)
       {
           NSDictionary *dic=[Arr objectAtIndex:i];
           if(dic!=nil && ![connectDatalist containsObject:dic])
           {
               [connectDatalist addObject:dic];
           }
       }
   }
   else if(flag==2)
   {
       for(int i=0;i<[Arr count];i++)
       {
           NSDictionary *dic=[Arr objectAtIndex:i];
           if(dic!=nil && ![customerDatalist containsObject:dic])
           {
               [customerDatalist addObject:dic];
           }
       }
   }
   [self addTableview];
}

//textview委托方法
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];
//    if(textviewIsfirstedite)
//    {
//        textView.text=nil;
//        textviewIsfirstedite=NO;
//    }
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    planContent=[textView.text retain];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

//委托方法
-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
    // self.tabBarController.tabBar.frame=CGRectMake(0, 215, 320, 49);
   // NSLog(@"%f",keyboardHeigh);
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
     planTitle=[textField.text retain];
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}


//按下Done按钮的调用方法，我们让键盘消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    //self.tabBarController.tabBar.frame=CGRectMake(0, 519, 320, 49);
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//table datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView1
{
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView1 numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        return [knowDatalist count]+1;
    }
    else if(section==1)
    {
        return [connectDatalist count]+1;
    }
    else
    {
        return [customerDatalist count]+1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        tableView1.SeparatorStyle=UITableViewCellSeparatorStyleSingleLine;
        if(indexPath.section==0)
        {
            cell.imageView.frame=CGRectMake(0, 0, 30, 30);
            [cell.imageView setImage:[UIImage imageNamed:@"message_add_background_highlight.png"]];
            cell.textLabel.text=@"知晓人";
            cell.contentView.backgroundColor=[UIColor colorWithRed:226.0/255.0 green:226.0/255.0 blue:226.0/255.0 alpha:1.0];
        }
        else if(indexPath.section==1)
        {
            cell.imageView.frame=CGRectMake(0, 0, 30, 30);
            [cell.imageView setImage:[UIImage imageNamed:@"message_add_background_highlight.png"]];
            cell.textLabel.text=@"联系人";
            cell.contentView.backgroundColor=[UIColor colorWithRed:226.0/255.0 green:226.0/255.0 blue:226.0/255.0 alpha:1.0];
        }
        else if(indexPath.section==2)
        {
            cell.imageView.frame=CGRectMake(0, 0, 30, 30);
            [cell.imageView setImage:[UIImage imageNamed:@"message_add_background_highlight.png"]];
            cell.textLabel.text=@"关联客户";
            cell.contentView.backgroundColor=[UIColor colorWithRed:226.0/255.0 green:226.0/255.0 blue:226.0/255.0 alpha:1.0];
        }
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"mCell";
        writeplanCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil){
            cell = [[[writeplanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        tableView1.SeparatorStyle=UITableViewCellSeparatorStyleSingleLine;
        cell.deleteBtn.tag=indexPath.section+indexPath.row*10;
        [cell.deleteBtn addTarget:self action:@selector(deletePerson:) forControlEvents:UIControlEventTouchUpInside];
        if(indexPath.section==0)
        {
           cell.nameLabel.text=[[knowDatalist objectAtIndex:indexPath.row-1] objectForKey:@"name"];
        }
        else if(indexPath.section==1)
        {
           cell.nameLabel.text=[[connectDatalist objectAtIndex:indexPath.row-1] objectForKey:@"name"];  
        }
        else if(indexPath.section==2)
        {
           cell.nameLabel.text=[[customerDatalist objectAtIndex:indexPath.row-1] objectForKey:@"name"];
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (void) tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPat
{
    UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
    temporaryBarButtonItem.title = @"返回";
    self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
    if(indexPat.section==0&&indexPat.row==0)
    {
        PersonListViewController *personView=[[PersonListViewController alloc]initWithNibName:nil bundle:nil];
        [personView setTitleStr:@"知晓人"];
        personView.addDelegate=self;
        [self.navigationController pushViewController:personView  animated:YES];
        [personView release];
    }
    else if(indexPat.section==1&&indexPat.row==0)
    {
        PersonListViewController *personView=[[PersonListViewController alloc]initWithNibName:nil bundle:nil];
        [personView setTitleStr:[NSString stringWithFormat:@"联系人"]];
        personView.addDelegate=self;
        [self.navigationController pushViewController:personView animated:YES];
       // [personView release];
    }
    else if(indexPat.section==2&&indexPat.row==0)
    {
        PersonListViewController *personView=[[PersonListViewController alloc]initWithNibName:nil bundle:nil];
        [personView setTitleStr:[NSString stringWithFormat:@"关联客户"]];
        personView.addDelegate=self;
        [self.navigationController pushViewController:personView animated:YES];
       // [personView release];
    }
    [temporaryBarButtonItem release];
}

-(void)deletePerson:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    int section = btn.tag%10;
    int row     = btn.tag/10;
    if(section==0)
    {
        [knowDatalist removeObjectAtIndex:row-1];
    }
    else if(section==1)
    {
        [connectDatalist removeObjectAtIndex:row-1];
    }
    else if(section==2)
    {
        [customerDatalist removeObjectAtIndex:row-1];
    }
    [personList reloadData];
}

@end
