//
//  writeplanCell.h
//  moffice
//
//  Created by lijinhua on 13-6-27.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface writeplanCell : UITableViewCell
{
    UIButton *deleteBtn;
    UILabel *nameLabel;
}
@property(nonatomic,retain) UIButton *deleteBtn;
@property(nonatomic,retain) UILabel *nameLabel;
@end
