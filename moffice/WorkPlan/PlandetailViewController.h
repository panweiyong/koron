//
//  PlandetailViewController.h
//  moffice
//
//  Created by lijinhua on 13-7-3.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JsonService.h"
#import "headCell.h"
#import "nomalCell.h"
#import "summaryCell.h"

@interface PlandetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate,UIWebViewDelegate>
{
    NSMutableArray *planDetailArray;
    NSString *PlanTitle;
    NSString *UpdateTime;
    NSString *planId;
    
    UITableView *mainTab;
    UILabel *detailLabel;
    UIWebView *planInfo;
    UIButton *summaryBtn;
    UIButton *reviewBtn;
    
    CGFloat webHeight;
    //计划内容详情
    NSMutableArray *reviewArray;//评论
    NSMutableArray *summaryArray;//总结
    BOOL textViewisPressed;
    BOOL connectTextViewisPressed;
    BOOL customerTextViewisPressed;

    
    NSString *reviewContent;//点评内容
    NSString *remarkType;
    NSString *commitId;
    NSString *summaryContent;//总结内容
    NSString *summaryTime;//时耗
    NSString *statusId;
    
    NSMutableArray *personArr;
    NSMutableArray *connectArr;
    NSMutableArray *customerArr;
    
    int btnFlag;//用于判断是点击哪个btn弹出的框。
    int sendFlag;//用于判断请求
//    NSArray *AssArray;
//    NSArray *commitArray;
    BOOL sysTohere;//判断是否从系统消息跳进来
//    NSString *
    int revFlag;
    int sumFlag;
    
    float revHeight;
    float revWidth;
    
    float sumHeight;
    float sumWidth;

}
@property(nonatomic,retain) NSString *PlanTitle;
@property(nonatomic,retain) NSString *UpdateTime;
@property(nonatomic,retain) NSString *planId;
@property(nonatomic,retain) NSMutableArray *reviewArray;
@property(nonatomic,retain) NSMutableArray *summaryArray;

@property(nonatomic,retain) NSString *reviewContent;
@property(nonatomic,retain) NSString *remarkType;
@property(nonatomic,retain) NSString *commitId;
@property(nonatomic,retain) NSString *summaryContent;
@property(nonatomic,retain) NSString *summaryTime;
@property(nonatomic,retain) NSString *statusId;
@property(nonatomic,readwrite) BOOL sysTohere;


@end
