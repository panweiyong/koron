//
//  PlandetailViewController.m
//  moffice
//
//  Created by lijinhua on 13-7-3.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "PlandetailViewController.h"
#import "WritePlanViewController.h"

@interface PlandetailViewController ()

@end

@implementation PlandetailViewController
@synthesize PlanTitle,UpdateTime,planId;
@synthesize reviewArray,summaryArray,reviewContent,summaryContent,summaryTime,remarkType,commitId,statusId;
@synthesize sysTohere;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dealloc
{
    [reviewArray release];
    [summaryArray release];
    [super dealloc];
}

-(void)viewWillAppear:(BOOL)animated
{
    textViewisPressed=NO;
    connectTextViewisPressed=NO;
    customerTextViewisPressed=NO;
    btnFlag=0;
    sendFlag=0;
    if (planDetailArray != nil) {
        [planDetailArray release];
        planDetailArray = nil;
    }
    planDetailArray=[[NSMutableArray alloc]initWithCapacity:0];
    [self getWorkplanDetail];

}

-(void)getWorkplanDetail
{
    UIView *backview=[[UIView alloc]initWithFrame:CGRectMake(110, self.view.frame.size.height/2-40, 100, 80)];
    [backview setBackgroundColor:[UIColor grayColor]];
    backview.layer.cornerRadius=5.0f;
    backview.tag=405;
    [self.view addSubview:backview];
    
    UIActivityIndicatorView *activity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activity.center=CGPointMake(160, self.view.frame.size.height/2);
    activity.tag=406;
    [self.view addSubview:activity];
    [activity startAnimating];
    
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice getWorkPlanDetail:planId];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStartAnimation" object:nil];
}

-(void)requestDataFinished:(id)jsondata
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
   // NSLog(@"WorkPlanList=%@",jsondata);
    switch (sendFlag){
        case 0:
            [self removeactivityAndbackviewS];
            [planDetailArray removeAllObjects];
            if([jsondata isKindOfClass:[NSDictionary class]])
            {
                NSArray *detailArr=[NSArray arrayWithArray:[jsondata objectForKey:@"data"]];
                if([detailArr count]!=0)
                {
                    for(int i=0;i<[detailArr count];i++)
                    {
                        if(![planDetailArray containsObject:[detailArr objectAtIndex:i]])
                        {
                            [planDetailArray addObject:[detailArr objectAtIndex:i]];
                        }
                    }
                    [self webViewLoadHtml];
                    NSArray *perArr=[[planDetailArray objectAtIndex:0] objectForKey:@"Ass"];
                    for(int i=0;i<[perArr count];i++)
                    {
                       NSDictionary *perDic=[perArr objectAtIndex:i];
                       if([[perDic objectForKey:@"assName"] isEqualToString:@"知晓人"])
                       {
                           [personArr addObject:perDic];
                       }
                       else if([[perDic objectForKey:@"assName"] isEqualToString:@"联系人"])
                       {
                           [connectArr addObject:perDic];
                       }
                       else
                       {
                           [customerArr addObject:perDic];
                       }
                    }
                }
                mainTab=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
                mainTab.backgroundColor=[UIColor clearColor];
                mainTab.backgroundView=nil;
                mainTab.separatorStyle=UITableViewCellSeparatorStyleSingleLine;
                mainTab.bounces=YES;
                mainTab.delegate=self;
                mainTab.dataSource=self;
                [self.view addSubview:mainTab];
               //[mainTab reloadData];
            }
            break;
        case 1:
             {
                 [self removeactivityAndbackviewF];
                 if([jsondata isKindOfClass:[NSDictionary class]])
                 {
                     NSString *result=[jsondata objectForKey:@"desc"];
                     UILabel *resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(120, 285, 80, 40)];
                     resultLabel.backgroundColor=[UIColor grayColor];
                     resultLabel.textColor=[UIColor whiteColor];
                     resultLabel.textAlignment=UITextAlignmentCenter;
                     resultLabel.tag=501;
                     resultLabel.layer.cornerRadius=4.0f;
                     resultLabel.layer.borderColor=[[UIColor grayColor] CGColor];
                     resultLabel.layer.borderWidth=1.0f;
                     resultLabel.text=result;
                     [resultLabel setFont:[UIFont fontWithName:@"Arial" size:12.0]];
                     [self.view addSubview:resultLabel];
                     [self performSelector:@selector(removeResultLabel) withObject:nil afterDelay:2.0f];
                     sendFlag=0;
                     if([[jsondata objectForKey:@"code"] intValue]==0)
                     {
                         [mainTab removeFromSuperview];
                         [self getWorkplanDetail];
                     }
                 }
             }
            break;
        case 2:
             {
                 [self removeactivityAndbackviewS];
                 if([jsondata isKindOfClass:[NSDictionary class]])
                 {
                     NSString *result=[jsondata objectForKey:@"desc"];
                     UILabel *resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(120, 285, 80, 40)];
                     resultLabel.backgroundColor=[UIColor grayColor];
                     resultLabel.textColor=[UIColor whiteColor];
                     resultLabel.textAlignment=UITextAlignmentCenter;
                     resultLabel.tag=502;
                     resultLabel.layer.cornerRadius=4.0f;
                     resultLabel.layer.borderColor=[[UIColor grayColor] CGColor];
                     resultLabel.layer.borderWidth=1.0f;
                     resultLabel.text=result;
                     [resultLabel setFont:[UIFont fontWithName:@"Arial" size:12.0]];
                     [self.view addSubview:resultLabel];
                     [self performSelector:@selector(removeResultLabel) withObject:nil afterDelay:2.0f];
                     sendFlag=0;
                     if([[jsondata objectForKey:@"code"] intValue]==0)
                     {
                         [mainTab removeFromSuperview];
                         [self getWorkplanDetail];
                     }
                 }
             }
            break;
        default:
            break;
    }
}

 
-(void)removeResultLabel
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==501||view.tag==502)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}



-(void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStopAnimation" object:nil];
    [[JsonService sharedManager] cancelAllRequest];
    [super viewDidDisappear:animated];
    [mainTab removeFromSuperview];
}

- (void)viewDidLoad
{
     [super viewDidLoad];
      self.title=@"计划详情";
      UIBarButtonItem *done =[[UIBarButtonItem alloc] initWithTitle:@"修改" style:UIBarButtonItemStyleBordered target:self action:@selector(ModifyWorkplan:)];
      self.navigationItem.rightBarButtonItem = done;
    
    if(sysTohere==YES)
    {
        self.navigationItem.rightBarButtonItem=nil;
    }
    revFlag=0;
    sumFlag=0;
    
    revHeight=0;
    revWidth =0;
    personArr=[[[NSMutableArray alloc]initWithCapacity:0] retain];
    [personArr removeAllObjects];
    connectArr=[[[NSMutableArray alloc]initWithCapacity:0] retain];
    [connectArr removeAllObjects];
    customerArr=[[[NSMutableArray alloc]initWithCapacity:0] retain];
    [customerArr removeAllObjects];
}

-(void)reviewPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    //NSLog(@"%d",btn.tag);
    if(btn.tag==131)
    {
      remarkType=[[NSString stringWithFormat:@"0"] retain];
    }
    else if(btn.tag==133)
    {
      remarkType=[[NSString stringWithFormat:@"1"] retain];
    }
    UIView *backView=[[UIView alloc]initWithFrame:self.view.bounds];
    backView.tag=10013;
    [self.view addSubview:backView];
    UITableView *reviewTab=[[UITableView alloc]initWithFrame:CGRectMake(40, 50, self.view.frame.size.width-80,240) style:UITableViewStylePlain];
    [reviewTab setBackgroundColor:[UIColor grayColor]];
    reviewTab.scrollEnabled=NO;
    reviewTab.tag=10011;
    reviewTab.delegate=self;
    reviewTab.dataSource=self;
    [self.view addSubview:reviewTab];
    [self.view bringSubviewToFront:reviewTab];
}

-(void)summaryPressed:(id)sender
{  
    UIButton *btn=(UIButton*)sender;
    //NSLog(@"%d",btn.tag);
    btnFlag=btn.tag;
    if ([[[planDetailArray objectAtIndex:0] objectForKey:@"summary"] hasPrefix:@"<"]||[[[planDetailArray objectAtIndex:0] objectForKey:@"summary"] hasSuffix:@">"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"内容包含HTML格式,是否修改?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=802;
        [alert show];
        [alert release];
    }
    else
    {
        [self addSummaryTab];
    }
}

-(void)ModifyWorkplan:(id)sender
{
    if ([[[planDetailArray objectAtIndex:0] objectForKey:@"content"] hasPrefix:@"<"]||[[[planDetailArray objectAtIndex:0] objectForKey:@"content"] hasSuffix:@">"])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"内容包含HTML格式,是否修改?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.tag=801;
        [alert show];
        [alert release];
    }
    else
    {
        [self pushViewTorewritePlan];
    }
    [self reviewcancelPressed];
    [self summarycancelPressed];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==801)
    {
       if(buttonIndex==0)
       {
        return;
       }
       else
       {
           [self pushViewTorewritePlan];
       }
    }
    else
    {
        if(buttonIndex==0)
        {
            return;
        }
        else
        {
            [self addSummaryTab];
        }
    }
}

-(void)pushViewTorewritePlan
{
    NSLog(@"%@",[planDetailArray objectAtIndex:0]);
    
    
    UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
    temporaryBarButtonItem.title = @"返回";
    self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
    WritePlanViewController *writeView=[[WritePlanViewController alloc]initWithNibName:nil bundle:nil];
    if([[planDetailArray objectAtIndex:0] objectForKey:@"beginDate"]!=nil&&[[planDetailArray objectAtIndex:0] objectForKey:@"endDate"]!=nil)
    {
        [writeView setStartDate:[[planDetailArray objectAtIndex:0] objectForKey:@"beginDate"]];
        [writeView setEndDate:[[planDetailArray objectAtIndex:0] objectForKey:@"endDate"]];
    }
    else
    {
        NSString *creatTime=[[planDetailArray objectAtIndex:0] objectForKey:@"createTime"];
        NSString *cutCreatTime=[creatTime substringToIndex:10];
        NSString *startTime=[NSString stringWithFormat:@"%@ 00:00:00",cutCreatTime];
        NSString *endTime=[NSString stringWithFormat:@"%@ 23:59:59",cutCreatTime];
        [writeView setStartDate:startTime];
        [writeView setEndDate:endTime];
    }
    [writeView setPlanType:[[planDetailArray objectAtIndex:0] objectForKey:@"planType"]];
    [writeView setPlanTitle:[[planDetailArray objectAtIndex:0] objectForKey:@"title"]];
    [writeView setPerArr:personArr];
    [writeView setConnectArr:connectArr];
    [writeView setCustomerArr:customerArr];
//    [writeView setStartDate:nil];
//    [writeView setEndDate:nil];
    [writeView setPlanId:[[planDetailArray objectAtIndex:0] objectForKey:@"planId"]];
    [writeView setPlanContent:[[planDetailArray objectAtIndex:0] objectForKey:@"content"]];
    
    NSArray *arr = [[planDetailArray objectAtIndex:0] objectForKey:@"Ass"];
    for (NSDictionary *dic in arr) {
        [writeView.names addObject:[dic objectForKey:@"assName"]];
    }
    
    
    [self.navigationController pushViewController:writeView animated:YES];
}

-(void)addSummaryTab
{
    UIView *backView=[[UIView alloc]initWithFrame:self.view.bounds];
    backView.tag=10014;
    [self.view addSubview:backView];
    UITableView *summaryTab=[[UITableView alloc]initWithFrame:CGRectMake(20, 20, self.view.frame.size.width-40, 330) style:UITableViewStylePlain];
    [summaryTab setBackgroundColor:[UIColor grayColor]];
    summaryTab.scrollEnabled=NO;
    summaryTab.tag=10012;
    summaryTab.delegate=self;
    summaryTab.dataSource=self;
    [self.view addSubview:summaryTab];
    [self.view bringSubviewToFront:summaryTab];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView.tag==10011||tableView.tag==10012)
    {
        return 1;
    }
    else
    {
        if([planDetailArray count]!=0)
        {
           if([[[planDetailArray objectAtIndex:0] objectForKey:@"summaryTime"] length]<3)
           {
              return 1;
           }
            return 2;
        }
        else
        {
            return 0;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(tableView.tag==10011||tableView.tag==10012)
    {
        return 3;
    }
    else
    {
        if([planDetailArray count]!=0)
        {
            NSArray *commitArr=[[planDetailArray objectAtIndex:0] objectForKey:@"commit"];
            reviewArray=[[NSMutableArray alloc]initWithCapacity:0];
            [reviewArray removeAllObjects];
            summaryArray=[[NSMutableArray alloc]initWithCapacity:0];
            [summaryArray removeAllObjects];
            for(int i=0;i<[commitArr count];i++)
            {
                if([[[commitArr objectAtIndex:i] objectForKey:@"remarkType"] intValue]==0)
                {
                    [reviewArray addObject:[commitArr objectAtIndex:i]];
                }
                else
                {
                    [summaryArray addObject:[commitArr objectAtIndex:i]];
                }
            }
            
            NSMutableArray *arr=[[NSMutableArray alloc]initWithCapacity:0];
            [arr removeAllObjects];
            for(int i=0;i<[summaryArray count];i++)
            {
                NSString *contentStr=[[summaryArray objectAtIndex:i] objectForKey:@"commitId"];
                NSString *IDStr=[[summaryArray objectAtIndex:i] objectForKey:@"id"];
                if([contentStr length]==0||contentStr==nil)
                {
                    [arr addObject:[summaryArray objectAtIndex:i]];
                    for(int j=0;j<[summaryArray count];j++)
                    {
                        NSString *reviewStr=[[summaryArray objectAtIndex:j] objectForKey:@"commitId"];
                        if([reviewStr isEqualToString:IDStr])
                        {
                            [arr addObject:[summaryArray objectAtIndex:j]];
                        }
                    }
                }
            }
            
            NSMutableArray *brr=[[NSMutableArray alloc]initWithCapacity:0];
            [brr removeAllObjects];
            for(int i=0;i<[reviewArray count];i++)
            {
                NSString *contentStr=[[reviewArray objectAtIndex:i] objectForKey:@"commitId"];
                NSString *IDStr=[[reviewArray objectAtIndex:i] objectForKey:@"id"];
                if([contentStr length]==0||contentStr==nil)
                {
                    [brr addObject:[reviewArray objectAtIndex:i]];
                    for(int j=0;j<[reviewArray count];j++)
                    {
                        NSString *reviewStr=[[reviewArray objectAtIndex:j] objectForKey:@"commitId"];
                        if([reviewStr isEqualToString:IDStr])
                        {
                            [brr addObject:[reviewArray objectAtIndex:j]];
                        }
                    }
                }
            }
            [reviewArray removeAllObjects];
            reviewArray=[[NSMutableArray arrayWithArray:brr] retain];
            
            [summaryArray removeAllObjects];
            summaryArray=[[NSMutableArray arrayWithArray:arr] retain];
            if(section==0)
            {
                return [reviewArray count]+1;
            }
            else
            {
                if([summaryArray count]==0&&[[planDetailArray objectAtIndex:0] objectForKey:@"summaryTime"]!=nil)
                {
                    return 1;
                }
                else
                {
                    return [summaryArray count]+1;
                }
            }
        }
        return 0;
    }
}

-(void)webViewLoadHtml
{
    UIWebView *cWeb=[[UIWebView alloc]initWithFrame:CGRectMake(0,0,320,10)];
    cWeb.tag=2001;
    cWeb.delegate=self;
    cWeb.scalesPageToFit=NO;
    [cWeb loadHTMLString:[[planDetailArray objectAtIndex:0] objectForKey:@"content"] baseURL:nil];
    [self.view addSubview:cWeb];
    
    UIWebView *sWeb=[[UIWebView alloc]initWithFrame:CGRectMake(0,0,320,10)];
    sWeb.tag=2002;
    sWeb.delegate=self;
    sWeb.scalesPageToFit=NO;
    [sWeb loadHTMLString:[[planDetailArray objectAtIndex:0] objectForKey:@"summary"] baseURL:nil];
    [self.view addSubview:sWeb];
}


#pragma mark - webview
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(webView.tag==2001)
    {
        revHeight=[webView.scrollView contentSize].height;
        NSLog(@"revHeight=%f",revHeight);
        revWidth=[webView.scrollView contentSize].width;
        NSLog(@"revWidth=%f",revWidth);
//        CGRect newFrame = webView.frame;
//        newFrame.size.height = webViewHeight;
//        webView.frame = newFrame;
//        webView.scrollView.scrollEnabled=YES;
//        webView.scrollView.contentSize=CGSizeMake(webViewWidth, newFrame.size.height);
        [webView removeFromSuperview];
        [mainTab reloadData];
    }
    else if(webView.tag==2002)
    {
        sumHeight=[webView.scrollView contentSize].height;
        sumWidth=[webView.scrollView contentSize].width;
        [webView removeFromSuperview];
        [mainTab reloadData];
    }
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
//    UIAlertView *alterview = [[UIAlertView alloc] initWithTitle:@"" message:[error localizedDescription]  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//    [alterview show];
//    [alterview release];
}

//-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView *FooterView = [[UIView alloc]init];
//    return FooterView;
//}


- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView1.tag==10011)
    {
        static NSString *CellIdentifier = @"reviewCell";
        UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        if (indexPath.row==0)
        {
            cell.textLabel.text=@"点评";
            cell.textLabel.textColor=[UIColor whiteColor];
            cell.textLabel.backgroundColor=[UIColor clearColor];
            UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
            backgrdView.backgroundColor = [UIColor blackColor];
            cell.backgroundView = backgrdView;
        }
        else if(indexPath.row==1)
        {
            cell.backgroundColor=[UIColor whiteColor];
            UITextView *reviewText=[[UITextView alloc]initWithFrame:CGRectMake(8, 5,224,150)];
            reviewText.tag=701;
            [reviewText setBackgroundColor:[UIColor whiteColor]];
            reviewText.delegate=self;
            reviewText.layer.borderColor=[[UIColor blueColor] CGColor];
            reviewText.layer.borderWidth=1.0f;
            reviewText.keyboardType=UIKeyboardTypeDefault;
            reviewText.returnKeyType = UIReturnKeyDone;//返回键的类型
            [cell.contentView addSubview:reviewText];
        }
        else
        {
            cell.backgroundColor=[UIColor grayColor];
            UIButton *reviewokBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            reviewokBtn.frame=CGRectMake(5, 5, 112, 30);
            [reviewokBtn setTitle:@"发送" forState:UIControlStateNormal];
            [reviewokBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [reviewokBtn setBackgroundColor:[UIColor whiteColor]];
            [reviewokBtn addTarget:self action:@selector(reviewokPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:reviewokBtn];
            
            UIButton *reviewcancelBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            reviewcancelBtn.frame=CGRectMake(123, 5, 112, 30);
            [reviewcancelBtn setTitle:@"取消" forState:UIControlStateNormal];
            [reviewcancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [reviewcancelBtn setBackgroundColor:[UIColor whiteColor]];
            [reviewcancelBtn addTarget:self action:@selector(reviewcancelPressed) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:reviewcancelBtn];
            
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(tableView1.tag==10012)
    {
        static NSString *CellIdentifier = @"summaryCell";
        UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        if (indexPath.row==0)
        {
            cell.textLabel.text=@"总结";
            cell.textLabel.textColor=[UIColor whiteColor];
            cell.textLabel.backgroundColor=[UIColor clearColor];
            UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
            backgrdView.backgroundColor = [UIColor blackColor];
            cell.backgroundView = backgrdView;
        }
        else if(indexPath.row==1)
        {
            UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 10, 40, 40)];
            [timeLabel setBackgroundColor:[UIColor clearColor]];
            timeLabel.text=@"耗时:";
            [cell.contentView addSubview:timeLabel];
            [timeLabel release];
            
            UITextField *timeField=[[UITextField alloc]initWithFrame:CGRectMake(50, 10, 220, 40)];
            timeField.delegate=self;
            timeField.layer.borderColor=[[UIColor blueColor] CGColor];
            timeField.backgroundColor=[UIColor whiteColor];
            timeField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            timeField.returnKeyType = UIReturnKeyDone;//返回键的类型
            //timeField.autoresizingMask=UIViewAutoresizingFlexibleHeight;
            summaryTime=[[planDetailArray objectAtIndex:0] objectForKey:@"time"];
            timeField.text=summaryTime;
            timeField.enabled=YES;
            timeField.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
            [cell.contentView addSubview:timeField];
            [timeField release];
            
            UILabel *statusLabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 50, 40, 40)];
            [statusLabel setBackgroundColor:[UIColor clearColor]];
            statusLabel.text=@"状态:";
            [cell.contentView addSubview:statusLabel];
            [statusLabel release];
            
            UIScrollView *statusScroll=[[UIScrollView alloc]initWithFrame:CGRectMake(50, 50, 220, 40)];
            statusScroll.showsHorizontalScrollIndicator=NO;
            statusScroll.showsVerticalScrollIndicator=NO;
            statusScroll.delegate=self;
            statusScroll.showsVerticalScrollIndicator=YES;
            statusScroll.scrollEnabled=YES;
            [cell.contentView addSubview:statusScroll];
            
             UIButton *completeBtn=[UIButton buttonWithType:UIButtonTypeCustom];
             completeBtn.frame=CGRectMake(5, 0, 40, 40);
             completeBtn.tag=201;
             [completeBtn setBackgroundColor:[UIColor clearColor]];
             [completeBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
             [completeBtn setTitle:@"完成" forState:UIControlStateNormal];
             [completeBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0,0.0,-65.0)];
             [completeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
             [completeBtn addTarget:self action:@selector(statusBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            // [completeBtn setImageEdgeInsets:UIEdgeInsetsMake(0.0,-30.0,0.0,0.0)];
             [statusScroll addSubview:completeBtn];
            
            UIButton *deferBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            deferBtn.frame=CGRectMake(80, 0, 40, 40);
            deferBtn.tag=202;
            [deferBtn setBackgroundColor:[UIColor clearColor]];
            [deferBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
            [deferBtn setTitle:@"暂缓" forState:UIControlStateNormal];
            [deferBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0,0.0,-65.0)];
            [deferBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [deferBtn addTarget:self action:@selector(statusBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            // [completeBtn setImageEdgeInsets:UIEdgeInsetsMake(0.0,-30.0,0.0,0.0)];
            [statusScroll addSubview:deferBtn];
            
            UIButton *continueBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            continueBtn.frame=CGRectMake(155, 0, 40, 40); 
            continueBtn.tag=203;
            [continueBtn setBackgroundColor:[UIColor clearColor]];
            [continueBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
            [continueBtn setTitle:@"继续" forState:UIControlStateNormal];
            [continueBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0,0.0,-65.0)];
            [continueBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [continueBtn addTarget:self action:@selector(statusBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            // [completeBtn setImageEdgeInsets:UIEdgeInsetsMake(0.0,-30.0,0.0,0.0)];
            [statusScroll addSubview:continueBtn];
            
            UIButton *stopBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            stopBtn.frame=CGRectMake(230, 0, 40, 40);
            stopBtn.tag=204;
            [stopBtn setBackgroundColor:[UIColor clearColor]];
            [stopBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
            [stopBtn setTitle:@"终止" forState:UIControlStateNormal];
            [stopBtn setTitleEdgeInsets:UIEdgeInsetsMake(0.0,0.0,0.0,-65.0)];
            [stopBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [stopBtn addTarget:self action:@selector(statusBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            // [completeBtn setImageEdgeInsets:UIEdgeInsetsMake(0.0,-30.0,0.0,0.0)];
            [statusScroll addSubview:stopBtn];
            statusId=[[planDetailArray objectAtIndex:0] objectForKey:@"statusId"];
            if (statusId==nil||[statusId intValue]==0)
            {
                statusId=@"1";
            }
            switch ([statusId intValue])
            {
                case 0:
                    //[completeBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
                    break;
                case 1:
                    [completeBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
                    break;
                case 2:
                    [stopBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
                    break;
                case 3:
                    [continueBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
                    break;
                case 4:
                    [deferBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
                    break;
                default:
                    break;
            }
            statusScroll.contentSize=CGSizeMake(300, 40);
            UITextView *summaryText=[[UITextView alloc]initWithFrame:CGRectMake(8,95,264,145)];
            [summaryText setBackgroundColor:[UIColor whiteColor]];
            summaryText.delegate=self;
            summaryText.layer.borderColor=[[UIColor blueColor] CGColor];
            summaryText.layer.borderWidth=1.0f;
            summaryText.font=[UIFont fontWithName:@"Arial" size:14.0f];
            summaryText.keyboardType=UIKeyboardTypeDefault;
            summaryText.returnKeyType = UIReturnKeyDone;//返回键的类型
            summaryContent=[[planDetailArray objectAtIndex:0] objectForKey:@"summary"];
            summaryText.text=summaryContent;
            [cell.contentView addSubview:summaryText];
        }
        else
        {
            UIButton *summaryokBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            summaryokBtn.frame=CGRectMake(10, 5, 125, 30);
            [summaryokBtn setTitle:@"发送" forState:UIControlStateNormal];
            [summaryokBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [summaryokBtn setBackgroundColor:[UIColor whiteColor]];
            [summaryokBtn addTarget:self action:@selector(summaryokPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:summaryokBtn];
            
            UIButton *summarycancelBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            summarycancelBtn.frame=CGRectMake(145, 5, 125, 30);
            [summarycancelBtn setTitle:@"取消" forState:UIControlStateNormal];
            [summarycancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [summarycancelBtn setBackgroundColor:[UIColor whiteColor]];
            [summarycancelBtn addTarget:self action:@selector(summarycancelPressed) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:summarycancelBtn];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
    }
    else
    {
        if(indexPath.section==0)
        {
            if(indexPath.row==0)
            {
                static NSString *CellIdentifier = @"headCell";
                headCell *cell = (headCell*)[tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    cell = [[[headCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                }
                cell.contentWeb.delegate=nil;
                if(revWidth<320)
                {
                    revWidth=320;
                }
                if(revHeight<51)
                {
                    revHeight=51;
                }
                cell.contentWeb.frame=CGRectMake(0,36, 320, revHeight);
                cell.contentWeb.tag=1100;
                cell.contentWeb.scalesPageToFit=NO;
                [cell.contentWeb loadHTMLString:[[planDetailArray objectAtIndex:0] objectForKey:@"content"] baseURL:nil];
                CGRect frame = [cell.contentWeb frame];
                [cell.contentWeb setBackgroundColor:[UIColor clearColor]];
                cell.contentWeb.scrollView.scrollEnabled=YES;
                cell.contentWeb.scrollView.contentSize=CGSizeMake(revWidth, revHeight);
                cell.titleLabel.frame=CGRectMake(0, 2, 320, 30);
                cell.lineView.frame=CGRectMake(0, 34, 320, 1);
                UIImageView *statusView=[[UIImageView alloc]init];
                [cell.contentWeb addSubview:statusView];
                [cell.contentWeb bringSubviewToFront:statusView];
                switch ([[[planDetailArray objectAtIndex:0] objectForKey:@"statusId"]intValue])
                {
                    case 0:
                        break;
                    case 1:
                        statusView.frame=CGRectMake(150, 0, 73, 51);
                        [statusView setImage:[UIImage imageNamed:@"back_1.png"]];
                        break;
                    case 2:
                        statusView.frame=CGRectMake(150, 0, 73, 51);
                        [statusView setImage:[UIImage imageNamed:@"back_2.png"]];
                        break;
                    case 3:
                        statusView.frame=CGRectMake(150, 0, 73, 51);
                        [statusView setImage:[UIImage imageNamed:@"back_3.png"]];
                        break;
                    case 4:
                        statusView.frame=CGRectMake(150, 0, 73, 51);
                        [statusView setImage:[UIImage imageNamed:@"back_4.png"]];
                        break;
                    default:
                        break;
                }
               // cell.contentWeb.frame=CGRectMake(0, 36, 320, 33);
                cell.timeLabel.frame=CGRectMake(5,frame.size.height+66, 150, 30);
                cell.revButton.frame=CGRectMake(165,frame.size.height+66, 70, 25);
                cell.revButton.tag=131;
                cell.sumButton.frame=CGRectMake(240, frame.size.height+66, 70, 25);
                cell.sumButton.tag=132;
                [cell.revButton addTarget:self action:@selector(reviewPressed:) forControlEvents:UIControlEventTouchUpInside];
                [cell.sumButton addTarget:self action:@selector(summaryPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                JsonService *je=[JsonService sharedManager];
                [je setDelegate:self];
                if(([[[planDetailArray objectAtIndex:0] objectForKey:@"summaryTime"] length]>3)||![[je getSessionID] isEqualToString:[[planDetailArray objectAtIndex:0] objectForKey:@"empId"]])
                {
                    cell.revButton.frame=CGRectMake(240, frame.size.height+66, 70, 25);
                    cell.sumButton.hidden=YES;
                }
                
                if([personArr count]!=0)
                {
                      if(!textViewisPressed)
                      {
                        cell.personView.frame=CGRectMake(2, frame.size.height+100, 308, 30);
                        NSString *pinString=@"";
                        for(int i=0;i<[personArr count];i++)
                        {
                            NSString *nameStr=[[personArr objectAtIndex:i] objectForKey:@"name"];
                            pinString=[pinString stringByAppendingFormat:@"%@ ",nameStr];
                        }
                        cell.personView.text=[NSString stringWithFormat:@"知晓人:\n%@",pinString];
                      }
                      else
                      {
                        cell.personView.frame=CGRectMake(2, frame.size.height+100, 308, 15*([personArr count]+1));
                        NSString *pinString=@"";
                        for(int i=0;i<[personArr count];i++)
                        {
                            NSString *nameStr=[[personArr objectAtIndex:i] objectForKey:@"name"];
                            pinString=[pinString stringByAppendingFormat:@"%@\n",nameStr];
                        }
                        cell.personView.text=[NSString stringWithFormat:@"知晓人:\n%@",pinString];
                      }
                      cell.personView.tag=125;
                      UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapPressPersonView)];
                      [cell.personView addGestureRecognizer:tapPress];
                }
                else
                {
                    cell.personView.frame=CGRectMake(2, frame.size.height+100, 308, 0);
                    cell.personView.text=nil;
                }
                
                if([connectArr count]!=0)
                {
                    if(!connectTextViewisPressed)
                    {
                        cell.connectView.frame=CGRectMake(2, cell.personView.frame.origin.y+cell.personView.frame.size.height, 308, 30);
                        NSString *pinString=@"";
                        for(int i=0;i<[connectArr count];i++)
                        {
                            NSString *nameStr=[[connectArr objectAtIndex:i] objectForKey:@"name"];
                            pinString=[pinString stringByAppendingFormat:@"%@ ",nameStr];
                        }
                        cell.connectView.text=[NSString stringWithFormat:@"联系人:\n%@",pinString];
                    }
                    else
                    {
                        cell.connectView.frame=CGRectMake(2, cell.personView.frame.origin.y+cell.personView.frame.size.height, 308, 15*([connectArr count]+1));
                        NSString *pinString=@"";
                        for(int i=0;i<[connectArr count];i++)
                        {
                            NSString *nameStr=[[connectArr objectAtIndex:i] objectForKey:@"name"];
                            pinString=[pinString stringByAppendingFormat:@"%@\n",nameStr];
                        }
                        cell.connectView.text=[NSString stringWithFormat:@"联系人:\n%@",pinString];
                    }
                    cell.connectView.tag=126;
                    UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(connectTapPressPersonView)];
                    [cell.connectView addGestureRecognizer:tapPress];
                }
                else
                {
                    cell.connectView.frame=CGRectMake(2, cell.personView.frame.origin.y+cell.personView.frame.size.height, 308, 0);
                    cell.connectView.text=nil;
                }
                
                if([customerArr count]!=0)
                {
                    if(!customerTextViewisPressed)
                    {
                        cell.customerView.frame=CGRectMake(2, cell.connectView.frame.origin.y+cell.connectView.frame.size.height, 308, 30);
                        NSString *pinString=@"";
                        for(int i=0;i<[customerArr count];i++)
                        {
                            NSString *nameStr=[[customerArr objectAtIndex:i] objectForKey:@"name"];
                            pinString=[pinString stringByAppendingFormat:@"%@ ",nameStr];
                        }
                        cell.customerView.text=[NSString stringWithFormat:@"关联客户:\n%@",pinString];
                    }
                    else
                    {
                        cell.customerView.frame=CGRectMake(2, cell.connectView.frame.origin.y+cell.connectView.frame.size.height, 308, 15*([customerArr count]+1));
                        NSString *pinString=@"";
                        for(int i=0;i<[customerArr count];i++)
                        {
                            NSString *nameStr=[[customerArr objectAtIndex:i] objectForKey:@"name"];
                            pinString=[pinString stringByAppendingFormat:@"%@\n",nameStr];
                        }
                        cell.customerView.text=[NSString stringWithFormat:@"关联客户:\n%@",pinString];
                    }
                    cell.customerView.tag=127;
                    UITapGestureRecognizer *tapPress = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(customerTapPressPersonView)];
                    [cell.customerView addGestureRecognizer:tapPress];
                }
                else
                {
                    cell.customerView.frame=CGRectMake(2, cell.connectView.frame.origin.y+cell.connectView.frame.size.height, 308, 0);
                    cell.customerView.text=nil;
                }
                
                cell.titleLabel.text=[[planDetailArray objectAtIndex:0] objectForKey:@"title"];
                cell.titleLabel.textAlignment=UITextAlignmentCenter;
                cell.timeLabel.text=[[planDetailArray objectAtIndex:0] objectForKey:@"createTime"];
                //[cell.revButton setTitle:@"点评" forState:UIControlStateNormal];
                // [cell.sumButton setTitle:@"总结" forState:UIControlStateNormal];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                return cell;
            }
            else
            {
                static NSString *CellIdentifier = @"rnomalCell";
                nomalCell *cell = (nomalCell*)[tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    cell = [[[nomalCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                }
                if(indexPath.row-1<[reviewArray count])
                {  
                    cell.nameLabel.text=[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"empFullName"];
                    cell.timeLabel.text=[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"createTime"];
                    cell.contentLabel.text=[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"content"];
                    UIFont *font = [UIFont fontWithName:@"Arial" size:12];
                    //设置一个行高上限
                    CGSize size = CGSizeMake(320,2000);
                    //计算实际frame大小，并将label的frame变成实际大小
                    CGSize labelsize = [[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"content"] sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
                    [cell.contentLabel setFrame:CGRectMake(10,25, labelsize.width, labelsize.height)];
                    if([[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"commitId"] length]>3)
                    {
                        cell.nameLabel.frame=CGRectMake(30, 2, 120, 15);
                        cell.contentLabel.frame=CGRectMake(35, 25, labelsize.width, labelsize.height);
                    }
                    else
                    {
                        cell.nameLabel.frame=CGRectMake(5, 2, 120, 15);
                        [cell.contentLabel setFrame:CGRectMake(10,25, labelsize.width, labelsize.height)];
                    }
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                }
                return cell;
            }

         }
        else if(indexPath.section==1)
        {
            if(indexPath.row==0)
            {
                static NSString *CellIdentifier = @"summaryCell";
                summaryCell *cell = (summaryCell*)[tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    cell = [[[summaryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                }
                cell.countTimeLabel.text=[NSString stringWithFormat:@"总结 %@小时",[[planDetailArray objectAtIndex:0] objectForKey:@"time"]];
                cell.contentWeb.delegate=nil;
                if(sumWidth<320)
                {
                    sumWidth=320;
                }
                if(sumHeight<51)
                {
                    sumHeight=51;
                }
                cell.contentWeb.frame=CGRectMake(0,36,320,sumHeight);
                cell.contentWeb.tag=1101;
                cell.contentWeb.scalesPageToFit=NO;
                [cell.contentWeb loadHTMLString:[[planDetailArray objectAtIndex:0] objectForKey:@"summary"] baseURL:nil];
                CGRect frame = [cell.contentWeb frame];
                [cell.contentWeb setBackgroundColor:[UIColor clearColor]];
                cell.contentWeb.scrollView.scrollEnabled=YES;
                cell.contentWeb.scrollView.contentSize=CGSizeMake(sumWidth, sumHeight);
                NSLog(@"cell.contentWeb.frame.size.width=%f",cell.contentWeb.frame.size.width);
                cell.timeLabel.frame=CGRectMake(5,frame.size.height+35, 150, 30);
                cell.timeLabel.text=[NSString stringWithFormat:@"%@",[[planDetailArray objectAtIndex:0] objectForKey:@"createTime"]];
                JsonService *js=[JsonService sharedManager];
                [js setDelegate:self];
                if(![[js getSessionID]isEqualToString:[[planDetailArray objectAtIndex:0] objectForKey:@"empId"]])
                {
                   cell.revButton.frame=CGRectMake(240, 2, 70, 25);
                   cell.sumButton.hidden=YES;
                }
                cell.revButton.tag=133;
                [cell.revButton addTarget:self action:@selector(reviewPressed:) forControlEvents:UIControlEventTouchUpInside];
                cell.sumButton.tag=134;
                [cell.sumButton addTarget:self action:@selector(summaryPressed:) forControlEvents:UIControlEventTouchUpInside];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                return cell;
            }
            else
            {
                static NSString *CellIdentifier = @"snomalCell";
                nomalCell *cell = (nomalCell*)[tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
                if (cell == nil) {
                    cell = [[[nomalCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                }
                if(indexPath.row-1<[summaryArray count])
                {
                   // NSLog(@"%@,%@,%@",[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"empFullName"],[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"createTime"],[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"content"]);
                    cell.nameLabel.text=[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"empFullName"];
                    cell.timeLabel.text=[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"createTime"];
                    cell.contentLabel.text=[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"content"];
                    UIFont *font = [UIFont fontWithName:@"Arial" size:12];
                    //设置一个行高上限
                    CGSize size = CGSizeMake(320,2000);
                    //计算实际frame大小，并将label的frame变成实际大小
                    CGSize labelsize = [[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"content"] sizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
                    
                    if([[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"commitId"] length]>3)
                    {
                        cell.nameLabel.frame=CGRectMake(30, 2, 120, 15);
                        cell.contentLabel.frame=CGRectMake(35, 25, labelsize.width, labelsize.height);
                    }
                    else
                    {
                        cell.nameLabel.frame=CGRectMake(5, 2, 120, 15);
                        [cell.contentLabel setFrame:CGRectMake(10,25, labelsize.width, labelsize.height)];
                    }
                    cell.selectionStyle=UITableViewCellSelectionStyleNone;
                }
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
        return nil;
     }
}

-(void)tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView1.tag==10011||tableView1.tag==10012)
    {
        return;
    }
    if(indexPath.section==0)
    {
        if(indexPath.row==0)
        {
            return;
        }
        else
        {
            //NSLog(@"remarkType＝%@",[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"remarkType"]);
            remarkType=[[NSString stringWithFormat:@"%@",[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"remarkType"]] retain];
            //NSLog(@"commitId＝%@",[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"commitId"]);
            if(([[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"commitId"]!=nil)&&([[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"commitId"]length]!=0))
            {
               commitId=[[NSString stringWithFormat:@"%@",[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"commitId"]] retain];
            }
            else
            {
                commitId=[[NSString stringWithFormat:@"%@",[[reviewArray objectAtIndex:indexPath.row-1] objectForKey:@"id"]] retain];
            }
            UIButton *btn=(UIButton*)[self.view viewWithTag:131];
            [self reviewPressed:btn];
        }
    }
    else if(indexPath.section==1)
    {
        if(indexPath.row==0)
        {
            return;
        }
        else
        {
            remarkType=[[NSString stringWithFormat:@"%@",[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"remarkType"]] retain];
            if(([[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"commitId"]!=nil)&&([[[summaryArray objectAtIndex:indexPath.row-1]objectForKey:@"commitId" ] length]!=0))
            {
                commitId=[[NSString stringWithFormat:@"%@",[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"commitId"]] retain];
            }
            else
            {
                commitId=[[NSString stringWithFormat:@"%@",[[summaryArray objectAtIndex:indexPath.row-1] objectForKey:@"id"]] retain];
            }
            UIButton *btn=(UIButton*)[self.view viewWithTag:133];
            [self reviewPressed:btn];
        }
    }
    else
    {
        return;
    }
}


- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView1.tag==10011)
    {
        if(indexPath.row==1)
        {
            return 160;
        }
        else 
        {
            return 40;
        }
    }
    else if(tableView1.tag==10012)
    {
        if(indexPath.row==1)
        {
            return 250;
        }
        else
        {
            return 40;
        }
    }
    else
    {
        if(indexPath.section==0)
        {
            if(indexPath.row==0)
            {
                headCell *cell=(headCell*)[self tableView:mainTab cellForRowAtIndexPath:indexPath];
                if([[[planDetailArray objectAtIndex:0] objectForKey:@"Ass"] count]==0)
                {
                    return cell.timeLabel.frame.origin.y+35;
                }
                else
                {
                    return cell.customerView.frame.origin.y+cell.customerView.frame.size.height+5;
                }
            }
            else
            {
                nomalCell *cell=(nomalCell*)[self tableView:mainTab cellForRowAtIndexPath:indexPath];
                CGFloat height=cell.contentLabel.frame.origin.y+cell.contentLabel.frame.size.height+5;
                return height;
            }
        }
        else
        {  
            if(indexPath.row==0)
            {
                summaryCell *cell=(summaryCell*)[self tableView:mainTab cellForRowAtIndexPath:indexPath];
                return cell.timeLabel.frame.origin.y+35;
            }
            else
            {
                nomalCell *cell=(nomalCell*)[self tableView:mainTab cellForRowAtIndexPath:indexPath];
                CGFloat height=cell.contentLabel.frame.origin.y+cell.contentLabel.frame.size.height+5;
                return height;
            }
        }
    }
}

-(void)statusBtnPressed:(id)sender
{
    UIButton *preBtn=(UIButton*)sender;
    UIButton *comBtn=(UIButton*)[self.view viewWithTag:201];
    UIButton *defBtn=(UIButton*)[self.view viewWithTag:202];
    UIButton *conBtn=(UIButton*)[self.view viewWithTag:203];
    UIButton *stoBtn=(UIButton*)[self.view viewWithTag:204];
    if(preBtn.tag==201)    //完成
    {
        statusId=[[NSString stringWithFormat:@"1"] retain];
        [comBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
        [defBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
        [conBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
        [stoBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
    }
    else if(preBtn.tag==202)//暂缓
    {
        statusId=[[NSString stringWithFormat:@"4"] retain];
        [defBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
        [comBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
        [conBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
        [stoBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
    }
    else if(preBtn.tag==203)//继续
    {
        statusId=[[NSString stringWithFormat:@"3"] retain];
        [conBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
        [comBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
        [defBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
        [stoBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
    }
    else                    //终止
    {
        statusId=[[NSString stringWithFormat:@"2"] retain];
        [stoBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
        [comBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
        [conBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
        [defBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
    }
}


-(void)tapPressPersonView
{
    if(textViewisPressed==NO)
    {
      textViewisPressed=YES;
    }
    else
    {
        textViewisPressed=NO;
    }
    [mainTab reloadData];
}

-(void)connectTapPressPersonView
{
    if(connectTextViewisPressed==NO)
    {
        connectTextViewisPressed=YES;
    }
    else
    {
        connectTextViewisPressed=NO;
    }
    [mainTab reloadData];
}


-(void)customerTapPressPersonView
{
    if(customerTextViewisPressed==NO)
    {
        customerTextViewisPressed=YES;
    }
    else
    {
        customerTextViewisPressed=NO;
    }
    [mainTab reloadData];
}


-(void)reviewokPressed:(id)sender
{
    if(reviewContent==nil||[reviewContent length]==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"内容不能为空!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        return;
    }
    
    UIView *backview=[[UIView alloc]initWithFrame:CGRectMake(110, self.view.frame.size.height/2-40, 80, 40)];
    [backview setBackgroundColor:[UIColor grayColor]];
    backview.layer.cornerRadius=5.0f;
    backview.tag=401;
    [self.view addSubview:backview];
    
    UIActivityIndicatorView *activity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activity.center=CGPointMake(160, self.view.frame.size.height/2);
    activity.tag=402;
    [backview addSubview:activity];
    [activity startAnimating];
    
    sendFlag=1;
    JsonService *js=[JsonService sharedManager];
    [js setDelegate:self];
    [js reviewWorkPlan:[[planDetailArray objectAtIndex:0] objectForKey:@"planId"] remarkType:remarkType commitId:commitId content:reviewContent];
    [self reviewcancelPressed];
    remarkType=nil;
    commitId=nil;
    reviewContent=nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStartAnimation" object:nil];
}

-(void)removeactivityAndbackviewF
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==401||view.tag==402)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}

-(void)reviewcancelPressed
{
    for(UIView *tab in [self.view subviews])
    {
        if(tab.tag==10011||tab.tag==10013)
        {
            [tab removeFromSuperview];
            [tab release];
        }
    }

}

-(void)summaryokPressed:(id)sender
{
    if(summaryTime==nil||[summaryTime length]==0)
    {
        UIAlertView *tAlert=[[UIAlertView alloc]initWithTitle:nil message:@"耗时不能为空!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [tAlert show];
        [tAlert release];
        return;
    }
    else if(statusId==nil||[statusId length]==0)
    {
        UIAlertView *sAlert=[[UIAlertView alloc]initWithTitle:nil message:@"请选择状态!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [sAlert show];
        [sAlert release];
        return;
    }
    else if(summaryContent==nil||[summaryContent length]==0)
    {
        UIAlertView *cAlert=[[UIAlertView alloc]initWithTitle:nil message:@"内容不能为空!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [cAlert show];
        [cAlert release];
        return;
    }
    else if ([summaryTime isEqualToString:@"0"]) {
        UIAlertView *cAlert=[[UIAlertView alloc]initWithTitle:nil message:@"耗时不能为'0'!" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [cAlert show];
        [cAlert release];
        return;
    }
    JsonService *js=[JsonService sharedManager];
    [js setDelegate:self];
//    NSLog(@"summaryTime=%@",summaryTime);
//    NSLog(@"summaryContent=%@",summaryContent);
    NSLog(@"statusId=%@",statusId);
    UIView *backview=[[UIView alloc]initWithFrame:CGRectMake(110, self.view.frame.size.height/2-40, 100, 80)];
    [backview setBackgroundColor:[UIColor grayColor]];
    backview.layer.cornerRadius=5.0f;
    backview.tag=403;
    [self.view addSubview:backview];
    
    UIActivityIndicatorView *activity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activity.center=CGPointMake(160, self.view.frame.size.height/2);
    activity.tag=404;
    [backview addSubview:activity];
    [activity startAnimating];
    
    sendFlag=2;
    [js summaryWorkPlan:[[planDetailArray objectAtIndex:0] objectForKey:@"planId"] time:summaryTime summary:summaryContent statusId:statusId];
    [self summarycancelPressed];
    summaryTime=nil;
    summaryContent=nil;
    statusId=nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshStartAnimation" object:nil];
}

-(void)removeactivityAndbackviewS
{  
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==403||view.tag==404||view.tag==405||view.tag==406)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}

-(void)summarycancelPressed
{
    for(UIView *tab in [self.view subviews])
    {
        if(tab.tag==10012||tab.tag==10014)
        {
            [tab removeFromSuperview];
            [tab release];
        }
    }
    
}

//委托方法
-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
    // self.tabBarController.tabBar.frame=CGRectMake(0, 215, 320, 49);
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
     summaryTime=[[NSString stringWithFormat:@"%@",textField.text] retain];
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

//按下Done按钮的调用方法，我们让键盘消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    //self.tabBarController.tabBar.frame=CGRectMake(0, 519, 320, 49);
    return YES;
}

//限制textField只能输入数字和一个小数点。
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHasRadixPoint = YES;
    NSString *existText = textField.text;
    if ([existText rangeOfString:@"."].location == NSNotFound) {
        isHasRadixPoint = NO;
    }
    if (string.length > 0) {
        unichar newChar = [string characterAtIndex:0];
        if ((newChar >= '0' && newChar <= '9') || newChar == '.' ) {
            if (newChar == '.') {
                if (isHasRadixPoint)
                    return NO;
                else
                    return YES;
            }else {
                if (isHasRadixPoint) {
                    NSRange ran = [existText rangeOfString:@"."];
                    int radixPointCount = range.location - ran.location;
                    if (radixPointCount <= 2) return YES;
                    else return NO;
                } else
                    return YES;
            }
            
        }else {
            if (newChar == '\n') return YES;       // 这句非常重要：不然将导致“Done”按钮失效
            return NO;
        }
        
    }else {
        return YES;
    }
}



//textview委托方法
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [textView becomeFirstResponder];

}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    if(textView.tag==701)
    {
        reviewContent=[[NSString stringWithFormat:@"%@",textView.text] retain];
    }
    else
    {
        summaryContent=[[NSString stringWithFormat:@"%@",textView.text] retain];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
