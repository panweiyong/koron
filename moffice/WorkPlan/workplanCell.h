//
//  workplanCell.h
//  moffice
//
//  Created by lijinhua on 13-7-3.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface workplanCell : UITableViewCell
{
    UILabel *typeLabel;
    UILabel *NameLabel;
    UILabel *nameOfplan;
    UIImageView *statusImage;
    UILabel *dateOfcreate;
}
@property(nonatomic,retain)UILabel *typeLabel;
@property(nonatomic,retain)UILabel *NameLabel;
@property(nonatomic,retain)UILabel *nameOfplan;
@property(nonatomic,retain)UIImageView *statusImage;
@property(nonatomic,retain)UILabel *dateOfcreate;
@end
