//
//  summaryCell.m
//  moffice
//
//  Created by lijinhua on 13-7-10.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "summaryCell.h"

@implementation summaryCell
@synthesize countTimeLabel,revButton,sumButton,timeLabel,contentWeb;


-(void)dealloc
{
    [countTimeLabel release];
    [timeLabel release];
    [contentWeb release];
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawCell];
    }
    return self;
}

-(void)drawCell
{
    countTimeLabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 2, 120, 15)];
    countTimeLabel.backgroundColor=[UIColor clearColor];
    countTimeLabel.textColor=[UIColor blackColor];
    countTimeLabel.font=[UIFont fontWithName:@"Arial" size:10.0];
    countTimeLabel.textAlignment=UITextAlignmentLeft;
    [self addSubview:countTimeLabel];
    
    revButton=[UIButton buttonWithType:UIButtonTypeCustom];
    revButton.frame=CGRectMake(165, 2, 70, 25);
    //revButton.frame=CGRectZero;
    [revButton setBackgroundColor:[UIColor clearColor]];
    // revButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    revButton.layer.borderWidth=1.0f;
    revButton.layer.borderColor=[[UIColor grayColor]CGColor];
    revButton.layer.cornerRadius=3.0f;
    [revButton setTitle:@"点评" forState:UIControlStateNormal];
    //[revButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 2.0)];
    [revButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [revButton setImage:[UIImage imageNamed:@"qzone_img_icon_comment.png"] forState:UIControlStateNormal];
    [revButton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-6.0,0.0,0.0)];
    //  [revButton addTarget:self action:@selector(reviewPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:revButton];
    
    
    sumButton=[UIButton buttonWithType:UIButtonTypeCustom];
    sumButton.frame=CGRectMake(240, 2, 70, 25);
    //  sumButton.frame=CGRectZero;
    [sumButton setBackgroundColor:[UIColor clearColor]];
    //sumButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    sumButton.layer.borderWidth=1.0f;
    sumButton.layer.borderColor=[[UIColor grayColor]CGColor];
    sumButton.layer.cornerRadius=3.0f;
    [sumButton setTitle:@"修改" forState:UIControlStateNormal];
    [sumButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [sumButton setImage:[UIImage imageNamed:@"qzone_img_icon_comment.png"] forState:UIControlStateNormal];
    [sumButton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-6.0,0.0,0.0)];
    //   [sumButton addTarget:self action:@selector(summaryPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sumButton];
    
    contentWeb=[[UIWebView alloc]initWithFrame:CGRectMake(0, 36, self.frame.size.width, 1)];
    contentWeb.backgroundColor=[UIColor clearColor];
    contentWeb.scrollView.scrollEnabled=YES;
    [self addSubview:contentWeb];
    
    
    timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 36+contentWeb.frame.size.height+5, 120, 20)];
    timeLabel.backgroundColor=[UIColor clearColor];
    timeLabel.textColor=[UIColor blackColor];
    timeLabel.font=[UIFont fontWithName:@"Arial" size:10.0];
    timeLabel.textAlignment=UITextAlignmentLeft;
    [self addSubview:timeLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
