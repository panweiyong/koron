//
//  PersonListViewController.m
//  moffice
//
//  Created by lijinhua on 13-6-24.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "PersonListViewController.h"

@interface PersonListViewController ()

@end

@implementation PersonListViewController
@synthesize titleStr,personkeyWord,personlastId,personType;
@synthesize addDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dealloc
{
    
    [personTexfield release];
//    [demandBtn release];
    [cellLabel release];
    [numLabel release];
//    [selectAllBtn release];
    [nameLabel release];
    
    [super dealloc];
    
}


-(void)viewWillAppear:(BOOL)animated
{
    personInfoArr=[[NSMutableArray alloc]initWithCapacity:0];
    [personInfoArr removeAllObjects];
    personList=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    personList.scrollsToTop=YES;
    //personList.scrollIndicatorInsets;
    personList.backgroundColor=[UIColor clearColor];
    personList.backgroundView=nil;
    personList.delegate=self;
    personList.dataSource=self;
    [self.view addSubview:personList];
    [self requestJsonData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [[JsonService sharedManager] cancelAllRequest];
    //[self setHidesBottomBarWhenPushed:NO];
    [super viewDidDisappear:animated];
}

//发起请求到服务器 
-(void)requestJsonData
{
    mainScreenView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [mainScreenView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:mainScreenView];
    backView=[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height/2-40, 100, 80)];
    backView.layer.cornerRadius=5.0f;
    [backView setBackgroundColor:[UIColor grayColor]];
    [self.view addSubview:backView];
    activity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [activity setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
    [self.view addSubview:activity];
    [activity startAnimating];
    if ([titleStr isEqualToString:@"知晓人"])
    {
        personType=@"selectKnowPerson";
    } 
    else if([titleStr isEqualToString:@"联系人"])
    {
        personType=@"selectLinkPerson";
    }
    else if([titleStr isEqualToString:@"关联客户"])
    {
        personType=@"selectClient";
    }
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice getPersonList:personType keyWord:personkeyWord lastId:personlastId];
}

//异步接收到返回的数据
- (void)requestDataFinished:(id)jsondata
{
    NSLog(@"%@",jsondata);
    if([jsondata isKindOfClass:[NSDictionary class]])
    {
        NSArray *arr=[NSArray arrayWithArray:[jsondata objectForKey:@"list"]];
        if([arr count]==0)
        {
//            [activity stopAnimating];
//            [activity release];
//            [backView removeFromSuperview];
//            [backView release];
//            [mainScreenView removeFromSuperview];
//            [mainScreenView release];
            UILabel *tellLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height-90, 100, 50)];
            tellLabel.text=@"数据加载已完成";
            tellLabel.adjustsFontSizeToFitWidth=YES;
            tellLabel.layer.cornerRadius=5.0f;
            tellLabel.backgroundColor=[UIColor grayColor];
            tellLabel.textColor=[UIColor whiteColor];
            [self.view addSubview:tellLabel];
            [self performSelector:@selector(removeLabel:) withObject:tellLabel afterDelay:1.0f];
            //return;
        }
        for(int i=0;i<[arr count];i++)
        {
            NSDictionary *dic=[arr objectAtIndex:i];
            if(dic!=nil && ![personInfoArr containsObject:dic])
            {
                [personInfoArr addObject:dic];
            }
        }
    }
    NSLog(@"[personInfoArr count]=%d",[personInfoArr count]);
    float height=([personInfoArr count]+1)*40;
    [self drawTable:height];
}

-(void)removeLabel:(UILabel*)label
{
    [label removeFromSuperview];
    [label release];
}

-(void)drawTable:(float)tableHeigth
{
    if(tableHeigth<self.view.frame.size.height-130)
    {
       personList.frame=CGRectMake(0, 86, self.view.frame.size.width,tableHeigth);
    }
    else
    {
       personList.frame=CGRectMake(0, 86, self.view.frame.size.width, self.view.frame.size.height-86);
    }
    [personList reloadData];
    [activity stopAnimating];
    [activity release];
    [backView removeFromSuperview];
    [backView release];
    [mainScreenView removeFromSuperview];
    [mainScreenView release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=titleStr;
    UIBarButtonItem *done =[[UIBarButtonItem alloc] initWithTitle:@"确定" style:UIBarButtonItemStyleBordered target:self action:@selector(addSure:)];
    self.navigationItem.rightBarButtonItem = done;
    
    personTexfield=[[UITextField alloc]initWithFrame:CGRectMake(10, 3, self.view.frame.size.width-60, 40)];
//    personTexfield.userInteractionEnabled=YES;
    [personTexfield setBackgroundColor:[UIColor clearColor]];
    [personTexfield setBorderStyle:UITextBorderStyleBezel];
    personTexfield.layer.cornerRadius=2.0;
    personTexfield.placeholder=@"关键字搜索";
    personTexfield.textAlignment=UITextAlignmentLeft;
    personTexfield.font = [UIFont fontWithName:@"helvetica" size:18.0];  //字体和大小设置
    personTexfield.adjustsFontSizeToFitWidth=YES;
    //personTexfield.autoresizingMask=UIViewAutoresizingFlexibleHeight;
    //personTexfield.enabled=YES;
    personTexfield.contentVerticalAlignment = UIControlContentHorizontalAlignmentCenter;
    personTexfield.keyboardAppearance = UIKeyboardAppearanceAlert ;
    personTexfield.returnKeyType = UIReturnKeyDone;
    personTexfield.delegate = self;
    [self.view addSubview:personTexfield];
    
    demandBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    demandBtn.frame=CGRectMake(self.view.frame.size.width-45, 3, 40, 40);
    demandBtn.layer.cornerRadius=3.0;
    [demandBtn setBackgroundImage:[UIImage imageNamed:@"customer_search_bt.png"] forState:UIControlStateNormal];
    [demandBtn setBackgroundImage:[UIImage imageNamed:@"customer_search_bt_pressed"] forState:UIControlStateHighlighted];
    [demandBtn setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:191.0/255.0 blue:241.0/255.0 alpha:1.0]];
    [demandBtn addTarget:self action:@selector(demandPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:demandBtn];
	// Do any additional setup after loading the view.
    
    cellLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 46, self.view.frame.size.width, 40)];
    cellLabel.userInteractionEnabled=YES;
    [cellLabel setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:214.0/255.0 blue:255.0/255.0 alpha:1.0]];
    [self.view addSubview:cellLabel];
    
    numLabel=[[UILabel alloc]initWithFrame:CGRectMake(20, 46, 40, 40)];
    [numLabel setBackgroundColor:[UIColor clearColor]];
    numLabel.text=@"No.";
    [self.view addSubview:numLabel];
    
    selectAllBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    selectAllBtn.frame=CGRectMake(75, 51, 30, 30);
    selectAllBtn.backgroundColor=[UIColor clearColor];
    selectAllBtn.selected=NO;
    [selectAllBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
    //[selectAllBtn setBackgroundImage:[UIImage imageNamed:@"wover.png"] forState:UIControlStateHighlighted];
    [selectAllBtn addTarget:self action:@selector(selectPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:selectAllBtn];
    
    nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(135, 46, 50, 40)];
    nameLabel.backgroundColor=[UIColor clearColor];
    nameLabel.text=@"姓名";
    [self.view addSubview:nameLabel];
    
    knowPersonList=[[NSMutableArray alloc]initWithCapacity:0];
    [knowPersonList removeAllObjects];
    connectPersonList=[[NSMutableArray alloc]initWithCapacity:0];
    [connectPersonList removeAllObjects];
    customerPersonList=[[NSMutableArray alloc]initWithCapacity:0];
    [customerPersonList removeAllObjects];
    
}

-(void)addSure:(id)sender
{
    if ([titleStr isEqualToString:@"知晓人"])
    {
        [addDelegate callbackPersonInfo:knowPersonList other:0];
    }
    else if([titleStr isEqualToString:@"联系人"])
    {
        [addDelegate callbackPersonInfo:connectPersonList other:1];
    }
    else if([titleStr isEqualToString:@"关联客户"])
    {
        [addDelegate callbackPersonInfo:customerPersonList other:2];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)demandPressed:(id)sender
{
    [personTexfield resignFirstResponder];
     personkeyWord=personTexfield.text;
    [personInfoArr removeAllObjects];
     personlastId=nil;
    [self requestJsonData];
}

-(void)selectPressed:(id)sender
{
    UIButton *Btn=(UIButton*)sender;
    [Btn setBackgroundColor:[UIColor colorWithRed:68.0/255.0 green:214.0/255.0 blue:255.0/255.0 alpha:1.0]];
    if(Btn.selected==NO)
    {
        Btn.selected=YES;
        [Btn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
        if ([titleStr isEqualToString:@"知晓人"])
        {
            [knowPersonList removeAllObjects];
            for(int i=0;i<[personInfoArr count];i++)
            {
                [knowPersonList addObject:[personInfoArr objectAtIndex:i]];
            }
        }
        else if([titleStr isEqualToString:@"联系人"])
        {
            [connectPersonList removeAllObjects];
            for(int i=0;i<[personInfoArr count];i++)
            {
                [connectPersonList addObject:[personInfoArr objectAtIndex:i]];
            }
        }
        else if([titleStr isEqualToString:@"关联客户"])
        {
            [customerPersonList removeAllObjects];
            for(int i=0;i<[personInfoArr count];i++)
            {
                [customerPersonList addObject:[personInfoArr objectAtIndex:i]];
            }
        }
    }
    else
    {
        NSLog(@"[personInfoArr count]=%d",[personInfoArr count]);
        Btn.selected=NO;
        [Btn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
        if ([titleStr isEqualToString:@"知晓人"])
        {
            [knowPersonList removeAllObjects];
        }
        else if([titleStr isEqualToString:@"联系人"])
        {
            [connectPersonList removeAllObjects];
        }
        else if([titleStr isEqualToString:@"关联客户"])
        {
            [customerPersonList removeAllObjects];
        }
    }
    NSLog(@"[personInfoArr count]=%d",[personInfoArr count]);
    [personList reloadData];
}

//委托方法
-(void) textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
    // self.tabBarController.tabBar.frame=CGRectMake(0, 215, 320, 49);
    // NSLog(@"%f",keyboardHeigh);
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    personkeyWord=textField.text;
    //customerStr=[textField.text retain];
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    //self.tabBarController.tabBar.frame=CGRectMake(0, 519, 320, 49);
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//tableview delegate/dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView1
{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView1 numberOfRowsInSection:(NSInteger)section
{
    return [personInfoArr count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==[personInfoArr count])
    {
        static NSString *CellIdentifier = @"mCell";
        UITableViewCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil){
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        for(UIButton *btn in [cell.contentView subviews])
        {
            [btn removeFromSuperview];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        tableView1.SeparatorStyle=UITableViewCellSeparatorStyleSingleLine;
        UIButton *moreBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        moreBtn.frame=CGRectMake(0, 0, cell.frame.size.width, cell.frame.size.height);
        [moreBtn setBackgroundColor:[UIColor clearColor]];
//        moreBtn.titleLabel.textColor=[UIColor blackColor];
//        moreBtn.titleLabel.text=@"点击加载更多";
        [moreBtn setTitle:@"点击加载更多" forState:UIControlStateNormal];
        [moreBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [moreBtn addTarget:self action:@selector(addmorePersonInfo:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:moreBtn];
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"Cell";
        personListCell *cell = [tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil){
            cell = [[[personListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        tableView1.SeparatorStyle=UITableViewCellSeparatorStyleSingleLine;
        cell.CellselectBtn.tag=indexPath.row;
        if([titleStr isEqualToString:@"知晓人"])
        {
            if([knowPersonList containsObject:[personInfoArr objectAtIndex:indexPath.row]])
            {
               cell.CellselectBtn.selected=YES;
               [cell.CellselectBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
            }
            else
            {
               cell.CellselectBtn.selected=NO;
               [cell.CellselectBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
            }        
        }
        if([titleStr isEqualToString:@"联系人"])
        {
            if([connectPersonList containsObject:[personInfoArr objectAtIndex:indexPath.row]])
            {
                cell.CellselectBtn.selected=YES;
                [cell.CellselectBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
            }
            else
            {
                cell.CellselectBtn.selected=NO;
                [cell.CellselectBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
            }
        }
        if([titleStr isEqualToString:@"关联客户"])
        {
            if([customerPersonList containsObject:[personInfoArr objectAtIndex:indexPath.row]])
            {
                cell.CellselectBtn.selected=YES;
                [cell.CellselectBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
            }
            else
            {
                cell.CellselectBtn.selected=NO;
                [cell.CellselectBtn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
            }
        }
        [cell.CellselectBtn addTarget:self action:@selector(cellBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.CellnumLabel.text=[NSString stringWithFormat:@"%d",indexPath.row+1];
        cell.CellNameLabel.text=[[personInfoArr objectAtIndex:indexPath.row] objectForKey:@"name"];
        return cell;
    }
}

-(void) cellBtnPressed:(id)sender
{
    UIButton *btn=(UIButton*)sender;
    int row = btn.tag;
    NSDictionary *dic=[personInfoArr objectAtIndex:row];
    if([titleStr isEqualToString:@"知晓人"])
    {
       if(btn.selected==YES)
       {
           btn.selected=NO;
          [btn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
           if([knowPersonList containsObject:dic])
           {
               [knowPersonList removeObject:dic];
           }
       }
       else
       {
           btn.selected=YES;
           [btn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
           if(dic!=nil && ![knowPersonList containsObject:dic])
           {
               [knowPersonList addObject:dic];
           }
       }
    }
    else if([titleStr isEqualToString:@"联系人"])
    {
        if(btn.selected==YES)
        {
            btn.selected=NO;
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
            if([connectPersonList containsObject:dic])
            {
                [connectPersonList removeObject:dic];
            }
        }
        else
        {
            btn.selected=YES;
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
            if(dic!=nil && ![connectPersonList containsObject:dic])
            {
                [connectPersonList addObject:dic];
            }
        }
    }
    else if([titleStr isEqualToString:@"关联客户"])
    {
        if(btn.selected==YES)
        {
            btn.selected=NO;
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_disable.png"] forState:UIControlStateNormal];
            if([customerPersonList containsObject:dic])
            {
                [customerPersonList removeObject:dic];
            }
        }
        else
        {
            btn.selected=YES;
            [btn setBackgroundImage:[UIImage imageNamed:@"btn_check_on_normal.png"] forState:UIControlStateNormal];
            if(dic!=nil && ![knowPersonList containsObject:dic])
            {
                [customerPersonList addObject:dic];
            }
        }
    }
}

-(void) addmorePersonInfo:(id)sender
{
    if([personInfoArr count]!=0)
    {
       personlastId=[[personInfoArr objectAtIndex:[personInfoArr count]-1] objectForKey:@"id"];
    }
    [self requestJsonData];
}

- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

- (void) tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPat
{
    personListCell *cell=(personListCell*)[tableView1 cellForRowAtIndexPath:indexPat];
    if(indexPat.row!=[personInfoArr count])
    {
        [self cellBtnPressed:cell.CellselectBtn];
    }
}


@end
