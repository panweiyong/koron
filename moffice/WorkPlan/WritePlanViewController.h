//
//  WritePlanViewController.h
//  moffice
//
//  Created by lijinhua on 13-6-21.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PersonListViewController.h"
#import "writeplanCell.h"
#import "JsonService.h"
#import "PlandetailViewController.h"

@interface WritePlanViewController : AbstractViewController<UITextFieldDelegate,UITextViewDelegate,UITableViewDelegate,UITableViewDataSource,addPersonDelegate>
{
    UITextField *planTitleField;
    UITextView  *contentView;
    UITableView *personList;
    NSMutableArray *knowDatalist;
    NSMutableArray *connectDatalist;
    NSMutableArray *customerDatalist;
    
    NSArray *perArr;
    NSArray *connectArr;
    NSArray *customerArr;
    
    NSString *startDate;
    NSString *endDate;
    NSString *planType;
    NSString *planTitle;
    NSString *planContent;
    NSString *planId;
    UIActivityIndicatorView * activity;
    NSString *weekStr;
    
    NSString *descStr;
    //保存知晓人名字
    NSMutableArray *_names;
}
//@property(nonatomic,retain) NSMutableArray *knowDatalist;
//@property(nonatomic,retain) NSMutableArray *connectDatalist;
//@property(nonatomic,retain) NSMutableArray *customerDatalist;
@property(nonatomic,retain) NSArray *perArr;
@property(nonatomic,retain) NSArray *connectArr;
@property(nonatomic,retain) NSArray *customerArr;
@property(nonatomic,retain) NSMutableArray *names;

@property(nonatomic,retain) NSString *startDate;
@property(nonatomic,retain) NSString *endDate;
@property(nonatomic,retain) NSString *planType;
@property(nonatomic,retain) NSString *planTitle;
@property(nonatomic,retain) NSString *planContent;
@property(nonatomic,retain) NSString *planId;
@property(nonatomic,retain) NSString *weekStr;
@end
