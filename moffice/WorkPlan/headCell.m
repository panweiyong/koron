//
//  headCell.m
//  moffice
//
//  Created by lijinhua on 13-7-8.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "headCell.h"

@implementation headCell
@synthesize timeLabel,lineView,contentWeb,statusImage,titleLabel,personView,connectView,customerView;
@synthesize revButton,sumButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawCell];
    }
    return self;
}

-(void)dealloc
{
    [titleLabel release];
    [lineView release];
    [contentWeb release];
    [statusImage release];
    [timeLabel release];
    [personView release];
    [connectView release];
    [customerView release];
    [super dealloc];
}

-(void)drawCell
{
     titleLabel=[[UILabel alloc]init];
    //titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 2, self.frame.size.width, 30)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.textAlignment=UITextAlignmentCenter;
    [self addSubview:titleLabel];
    
     lineView=[[UIView alloc]init];
    //lineView=[[UIView alloc]initWithFrame:CGRectMake(0, 34, self.frame.size.width, 1.0f)];
    [lineView setBackgroundColor:[UIColor colorWithRed:193.0/255.0 green:196/255.0 blue:218/255.0 alpha:1.0]];
    [self addSubview:lineView];
    
    statusImage=[[UIImageView alloc]initWithFrame:CGRectZero];
    [statusImage setBackgroundColor:[UIColor clearColor]];
    [self addSubview:statusImage];
    
     //contentWeb=[[UIWebView alloc]initWithFrame:CGRectZero];
    contentWeb=[[UIWebView alloc]initWithFrame:CGRectMake(0, 36, self.frame.size.width, 1)];
    contentWeb.backgroundColor=[UIColor clearColor];
    contentWeb.scrollView.scrollEnabled=YES;
    [self addSubview:contentWeb];
    
    //timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(2, contentWeb.frame.size.height+contentWeb.frame.origin.y+30, 150, 30)];
    timeLabel=[[UILabel alloc]init];
    timeLabel.textAlignment=UITextAlignmentLeft;
    timeLabel.textColor=[UIColor grayColor];
    timeLabel.font=[UIFont fontWithName:@"Arial" size:12.0];
    [self addSubview:timeLabel];
    

    revButton=[UIButton buttonWithType:UIButtonTypeCustom];
    //revButton.frame=CGRectMake(self.frame.size.width/2+5, timeLabel.frame.origin.y, 70, 25);
    //revButton.frame=CGRectZero;
    [revButton setBackgroundColor:[UIColor clearColor]];
   // revButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    revButton.layer.borderWidth=1.0f;
    revButton.layer.borderColor=[[UIColor grayColor]CGColor];
    revButton.layer.cornerRadius=3.0f;
    [revButton setTitle:@"点评" forState:UIControlStateNormal];
    //[revButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 2.0)];
    [revButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [revButton setImage:[UIImage imageNamed:@"qzone_img_icon_comment.png"] forState:UIControlStateNormal];
    [revButton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-6.0,0.0,0.0)];
  //  [revButton addTarget:self action:@selector(reviewPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:revButton];
    
    
    sumButton=[UIButton buttonWithType:UIButtonTypeCustom];
   // sumButton.frame=CGRectMake(self.frame.size.width/2+80, timeLabel.frame.origin.y, 70, 25);
  //  sumButton.frame=CGRectZero;
    [sumButton setBackgroundColor:[UIColor clearColor]];
    //sumButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    sumButton.layer.borderWidth=1.0f;
    sumButton.layer.borderColor=[[UIColor grayColor]CGColor];
    sumButton.layer.cornerRadius=3.0f;
    [sumButton setTitle:@"总结" forState:UIControlStateNormal];
    [sumButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [sumButton setImage:[UIImage imageNamed:@"qzone_img_icon_comment.png"] forState:UIControlStateNormal];
    [sumButton setImageEdgeInsets:UIEdgeInsetsMake(0.0,-6.0,0.0,0.0)];
 //   [sumButton addTarget:self action:@selector(summaryPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sumButton];
    
    personView=[[UITextView alloc]init];
    [personView setBackgroundColor:[UIColor clearColor]];
    personView.editable=NO;
    personView.scrollEnabled=NO;
    [self addSubview:personView];
    
    connectView=[[UITextView alloc]init];
    [connectView setBackgroundColor:[UIColor clearColor]];
    connectView.editable=NO;
    connectView.scrollEnabled=NO;
    [self addSubview:connectView];
    
    customerView=[[UITextView alloc]init];
    [customerView setBackgroundColor:[UIColor clearColor]];
    customerView.editable=NO;
    customerView.scrollEnabled=NO;
    [self addSubview:customerView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
