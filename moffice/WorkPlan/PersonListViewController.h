//
//  PersonListViewController.h
//  moffice
//
//  Created by lijinhua on 13-6-24.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "AbstractViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "personListCell.h"
#import "JsonService.h"

@protocol addPersonDelegate <NSObject>
-(void)callbackPersonInfo:(NSMutableArray*)Arr other:(int)flag;
@end

@interface PersonListViewController : AbstractViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSString *titleStr;
    UITextField *personTexfield;
    UIButton *demandBtn;
    UILabel *cellLabel;
    UILabel *numLabel;
    UIButton *selectAllBtn;
    UILabel *nameLabel;
    UITableView *personList;
    NSMutableArray *personInfoArr;
    //发送请求数据
    NSString *personType;
    NSString *personkeyWord;
    NSString *personlastId;
    //加载提示
    UIView *mainScreenView;
    UIView *backView;
    UIActivityIndicatorView *activity;
    //cell
    NSMutableArray *knowPersonList;
    NSMutableArray *connectPersonList;
    NSMutableArray *customerPersonList;
    id<addPersonDelegate> addDelegate;
  
}
@property(nonatomic,retain) NSString *titleStr;
@property(nonatomic,retain) NSString *personType;
@property(nonatomic,retain) NSString *personkeyWord;
@property(nonatomic,retain) NSString *personlastId;
@property(nonatomic,retain) id<addPersonDelegate> addDelegate;
@end
