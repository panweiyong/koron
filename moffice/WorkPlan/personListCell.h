//
//  personListCell.h
//  moffice
//
//  Created by lijinhua on 13-6-26.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface personListCell : UITableViewCell
{
    UILabel *CellnumLabel;
    UIButton *CellselectBtn;
    UILabel *CellNameLabel;
}
@property(nonatomic,retain) UILabel *CellnumLabel;
@property(nonatomic,retain) UIButton *CellselectBtn;
@property(nonatomic,retain) UILabel *CellNameLabel;

@end
