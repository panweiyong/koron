//
//  WorkPlanViewController.h
//  moffice
//
//  Created by lijinhua on 13-6-20.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CKCalendarView.h"
#import "JsonService.h"
#import "AbstractViewController.h"
#import "WritePlanViewController.h"
#import "PersonListViewController.h"
#import "workplanCell.h"
#import "PlandetailViewController.h"

@interface WorkPlanViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,CallbackDateDelegate>
{
    UIButton *titleButton;
    NSArray *statusArray;
    UIScrollView *mainScrollview;
    UIButton *dayButton;
    UIButton *weekButton;
    UIButton *monthButton;
    UIButton *calendarButton;
    CKCalendarView *ckCalendar;
    UITableView *planList;
    NSDate *selectDate;
    NSString *planType;
    NSString *startTime;
    NSString *endTime;
    
    NSString *lastplanType;
    NSString *laststartTime;
    NSString *lastendTime;
    
    NSString *weekStr;
    UIActivityIndicatorView * activity;
    
    NSMutableArray *planArray;
    NSMutableArray *copyPlanArray;//备份数据
    NSString* result;
    BOOL isDelete;
    
}
@property(nonatomic,retain) NSDate   *selectDate;
@property(nonatomic,retain) UITableView *planList;
@property(nonatomic,retain) CKCalendarView *ckCalendar;
@property(nonatomic,retain) NSString *planType;
@property(nonatomic,retain) NSString *startTime;
@property(nonatomic,retain) NSString *endTime;
@property(nonatomic,retain) NSString *weekStr;

@property(nonatomic,retain) NSString *lastplanType;
@property(nonatomic,retain) NSString *laststartTime;
@property(nonatomic,retain) NSString *lastendTime;
@end
