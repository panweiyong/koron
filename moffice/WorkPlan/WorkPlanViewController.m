//
//  WorkPlanViewController.m
//  moffice
//
//  Created by lijinhua on 13-6-20.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "WorkPlanViewController.h"

@interface WorkPlanViewController ()

@end

@implementation WorkPlanViewController
@synthesize selectDate;
@synthesize planType,startTime,endTime,lastplanType,lastendTime,laststartTime;
@synthesize weekStr,planList,ckCalendar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //self.title=@"工作计划";
    }
    return self;
}

-(void)dealloc
{
    [self.ckCalendar release],ckCalendar=nil;
    [planArray release];
    [self.planList release],planList=nil;
    self.startTime=nil;
    self.endTime=nil;
    self.laststartTime=nil;
    self.lastendTime=nil;
    self.planType=nil;
    self.lastplanType=nil;
    self.selectDate=nil;
    self.weekStr=nil;
    [super dealloc];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [[JsonService sharedManager] cancelAllRequest];
    NSLog(@"[ckCalendar retainCount]=%d",[ckCalendar retainCount]);
    NSLog(@"[planList retainCount]=%d",[planList retainCount]);
    NSLog(@"%d",[planArray retainCount]);
    NSLog(@"%d",[startTime retainCount]);
    NSLog(@"%d",[endTime retainCount]);
    NSLog(@"%d",[laststartTime retainCount]);
    NSLog(@"%d",[lastendTime retainCount]);
    NSLog(@"%d",[ckCalendar retainCount]);
    
    [super viewDidDisappear:animated];
    
}

-(void)viewWillAppear:(BOOL)animated
{
   // [super viewWillAppear:animated];
    if (IOS_VERSIONS > 6.9) {
        
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)showTellLabel
{
    UIView * backView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    backView.tag=101;
    backView.layer.cornerRadius=5.0f;
    [backView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:backView];
    
    UILabel* Telllabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50,self.view.frame.size.height/2-40, 100,80)];
    Telllabel.tag=102;
    [Telllabel setBackgroundColor:[UIColor grayColor]];
    Telllabel.layer.cornerRadius=5.0f;
    [self.view addSubview:Telllabel];
    
    UIActivityIndicatorView* getDateactivity=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [getDateactivity setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
     getDateactivity.tag=103;
    [getDateactivity startAnimating];
    [self.view addSubview:getDateactivity];

}

-(void)removeTelllabel
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==101||view.tag==102||view.tag==103)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}

-(void)showResultLabel
{
    UILabel* resultLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height-80, 100, 50)];
    resultLabel.tag=100;
    resultLabel.text=@"无工作计划";
    resultLabel.textAlignment=UITextAlignmentCenter;
    resultLabel.adjustsFontSizeToFitWidth=YES;
    resultLabel.layer.cornerRadius=5.0f;
    resultLabel.backgroundColor=[UIColor grayColor];
    resultLabel.textColor=[UIColor whiteColor];
    [self.view addSubview:resultLabel];
}

-(void)removeResultLabel
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==100)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}

-(void)requestDataFinished:(id)jsondata
{
    NSLog(@"WorkPlanList=%@",jsondata);
    if(!isDelete)
    {
        [planArray removeAllObjects];
        [copyPlanArray removeAllObjects];
        if([jsondata isKindOfClass:[NSDictionary class]])
        {
            NSArray *arr=[NSArray arrayWithArray:[jsondata objectForKey:@"list"]];
            if(arr==nil||[arr count]==0)
            {
                [self showResultLabel];
                [self performSelector:@selector(removeResultLabel) withObject:nil afterDelay:1.0f];
                [self removeTelllabel];
                [self drawTable:0];
                return;
            }
            for(int i=0;i<[arr count];i++)
            {
                NSDictionary *dic=[arr objectAtIndex:i];
                if(dic!=nil && ![planArray containsObject:dic]&&![copyPlanArray containsObject:dic])
                {
                    [planArray addObject:dic];
                    [copyPlanArray addObject:dic];
                }
            }
        }
       // NSLog(@"[planArray count]=%d",[planArray count]);
        float height=([planArray count])*40;
        [self drawTable:height];
        [self.planList reloadData];
        [self removeTelllabel];
    }
    else
    {
        NSLog(@"123");
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    [self removeTelllabel];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"加载失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
    [alert release];
    return;
}

-(void)Callback:(NSDate *)nowData select:(NSDate *)Data
{
    //NSLog(@"%@----%@",nowData,Data);
    self.selectDate=Data;
    NSLog(@"selectDate=%@",selectDate);
}

-(void)drawTable:(float)tableHeigth
{
   // NSLog(@"tableHeigth=%f",tableHeigth);
    NSLog(@"[ckCalendar retainCount]=%d",[ckCalendar retainCount]);
    NSLog(@"[planList retainCount]=%d",[planList retainCount]);
    if(tableHeigth<self.view.frame.size.height-self.ckCalendar.frame.size.height-self.ckCalendar.frame.origin.y)
    {
        if(self.ckCalendar.hidden==YES)
        {
            self.planList.frame=CGRectMake(0, 57, self.view.frame.size.width,tableHeigth);
        }
        else
        {
            self.planList.frame=CGRectMake(0, self.ckCalendar.frame.size.height+self.ckCalendar.frame.origin.y, self.view.frame.size.width,tableHeigth);
        }
    }
    else
    {
        if(self.ckCalendar.hidden==YES)
        {
            if(tableHeigth<self.view.frame.size.height-57)
            {
              self.planList.frame=CGRectMake(0, 57, self.view.frame.size.width, tableHeigth);
            }
            else
            {
              self.planList.frame=CGRectMake(0, 57, self.view.frame.size.width, self.view.frame.size.height-57);
            }
        }
        else
        {
            self.planList.frame=CGRectMake(0,self.ckCalendar.frame.size.height+self.ckCalendar.frame.origin.y, self.view.frame.size.width,self.view.frame.size.height-self.ckCalendar.frame.size.height-self.ckCalendar.frame.origin.y);
        }
    }
}

- (void)viewDidLoad
{ 
    [super viewDidLoad];
    
    [self loadNavigationItem];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(drawTable:) name:@"drawTable" object:nil];
    
    titleButton=[UIButton buttonWithType:UIButtonTypeCustom];
    titleButton.frame=CGRectMake(80, 0, 160, 55);
    [titleButton setBackgroundColor:[UIColor clearColor]];
    [titleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [titleButton setTitle:@"工作计划(全部)" forState:UIControlStateNormal];
    [titleButton addTarget:self action:@selector(planTypeSelect) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem setTitleView:titleButton];
    statusArray=[[NSArray alloc] initWithObjects:@"未完成",@"完成",@"终止",@"继续",@"暂缓",@"全部",nil];
    
//    UIBarButtonItem *done =[[UIBarButtonItem alloc] initWithTitle:@"写计划" style:UIBarButtonItemStyleBordered target:self action:@selector(writePlan:)];
//    self.navigationItem.rightBarButtonItem = done;
    
    dayButton=[UIButton buttonWithType:UIButtonTypeCustom];
    dayButton.frame=CGRectMake(20, 10, 61, 30);
    [dayButton.layer setMasksToBounds:YES];
    [dayButton.layer setCornerRadius:5.0]; //设置矩形四个圆角半径
    //[dayButton.layer setBorderWidth:1.0]; //边框宽度
    [dayButton setTitle:@"日" forState:UIControlStateNormal];
    [dayButton setBackgroundColor:[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0]];
    dayButton.selected=YES;
    self.planType=[NSString stringWithFormat:@"day"];
    [dayButton addTarget:self action:@selector(dayBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:dayButton];
    
    weekButton=[UIButton buttonWithType:UIButtonTypeCustom];
    weekButton.frame=CGRectMake(93, 10, 61, 30);
    [weekButton.layer setMasksToBounds:YES];
    [weekButton.layer setCornerRadius:5.0];
    [weekButton setTitle:@"周" forState:UIControlStateNormal];
    [weekButton setBackgroundColor:[UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0]];
    weekButton.selected=NO;
    [weekButton addTarget:self action:@selector(weekBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:weekButton];
    
    monthButton=[UIButton buttonWithType:UIButtonTypeCustom];
    monthButton.frame=CGRectMake(166, 10, 61, 30);
    [monthButton.layer setMasksToBounds:YES];
    [monthButton.layer setCornerRadius:5.0];
    [monthButton setTitle:@"月" forState:UIControlStateNormal];
    [monthButton setBackgroundColor:[UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0]];
    monthButton.selected=NO;
    [monthButton addTarget:self action:@selector(monthBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:monthButton];

	// Do any additional setup after loading the view.
    
    self.ckCalendar = [[CKCalendarView alloc] initWithStartDay:startSunday];
    self.ckCalendar.frame=CGRectMake(0, 50, 320, 0);
    self.ckCalendar.Calldelegate=self;
    [self.view addSubview:self.ckCalendar];
    
    calendarButton=[UIButton buttonWithType:UIButtonTypeCustom];
    calendarButton.frame=CGRectMake(239, 10, 61, 30);
    [calendarButton.layer setMasksToBounds:YES];
    [calendarButton.layer setCornerRadius:5.0];
    [calendarButton setTitle:@"日历" forState:UIControlStateNormal];
    [calendarButton setBackgroundColor:[UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0]];
    calendarButton.selected=NO;
    [calendarButton addTarget:self action:@selector(calendarBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:calendarButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getDatebtnDate) name:@"getDatebtnDate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTableFrame) name:@"changeTableFrame" object:nil];
    [self showTellLabel];
    
    planArray=[[NSMutableArray alloc]initWithCapacity:0];
    [planArray removeAllObjects];
    copyPlanArray=[[NSMutableArray alloc]initWithCapacity:0];
    [copyPlanArray removeAllObjects];
    isDelete=NO;
    
    self.planList=[[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.planList.scrollsToTop=YES;
    self.planList.tag=2200;
    //personList.scrollIndicatorInsets;
    self.planList.backgroundColor=[UIColor clearColor];
    self.planList.backgroundView=nil;
    self.planList.delegate=self;
    self.planList.dataSource=self;
    self.planList.bounces=NO;
    [self.view addSubview:self.planList];
    
    if(self.selectDate==nil)
    {
        self.selectDate=[NSDate date];
        NSTimeInterval strSec=8*60*60;
        NSString *nowStr=[NSString stringWithFormat:@"%@",self.selectDate];
        NSString *nowCutstr=[nowStr substringToIndex:10];
        NSString *addnowStr=[NSString stringWithFormat:@"%@ 00:00:00",nowCutstr];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:addnowStr];
        NSLog(@"date=%@", date);
        self.selectDate=[date dateByAddingTimeInterval:strSec];
        [dateFormatter release];
        self.startTime= [NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:0]];
        NSTimeInterval endSec=24*60*60-1;
        self.endTime = [NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:endSec]];
        self.startTime=[self.startTime substringToIndex:19];
        self.endTime=[self.endTime substringToIndex:19];
        
        JsonService *jservice=[JsonService sharedManager];
        [jservice setDelegate:self];
        [jservice getWorkPlanList:@"day" str:self.startTime end:self.endTime];
        self.laststartTime=self.startTime;
        self.lastendTime=self.endTime;
        self.lastplanType=self.planType;
    }
    else if(dayButton.selected==YES)
    {
        [self dayBtnPressed:dayButton];
    }
    else if(weekButton.selected==YES)
    {
        [self weekBtnPressed:weekButton];
    }
    else if(monthButton.selected==YES)
    {
        [self monthBtnPressed:monthButton];
    }
}

-(void)planTypeSelect
{
   // NSLog(@"12356");
    UIView *backgroundView=[[UIView alloc]initWithFrame:self.view.bounds];
    backgroundView.tag=110013;
    [backgroundView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:backgroundView];
    
    UIImageView *triImage=[[UIImageView alloc]initWithFrame:CGRectMake(148, 0, 24, 9)];
    [triImage setBackgroundColor:[UIColor clearColor]];
    triImage.tag=110014;
    [triImage setImage:[UIImage imageNamed:@"back_tri.png"]];
    [self.view addSubview:triImage];
    
    UITableView *statusTable=[[UITableView alloc]initWithFrame:CGRectMake(60, 9, 200, 210) style:UITableViewStylePlain];
    statusTable.layer.cornerRadius=5.0f;
    statusTable.tag=110015;
    statusTable.scrollEnabled=NO;
    [statusTable setBackgroundColor:[UIColor colorWithRed:57.0/255.0 green:69.0/255.0 blue:82.0/255.0 alpha:1.0]];
    //statusTable.backgroundView=nil;
    //statusTable.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"group_bg.png"]];
    statusTable.delegate=self;
    statusTable.dataSource=self;
    [self.view addSubview:statusTable];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    for(UITableView *table in [self.view subviews])
    {
        if(table.tag==110015)
        {
            UITouch *touch=[touches anyObject];
            CGPoint currentPoint=[touch locationInView:self.view];
            if(!CGRectContainsPoint(table.frame, currentPoint))
            {
                [table removeFromSuperview];
                [table release];
                table=nil;
                for (UIView *view in [self.view subviews])
                {
                    if(view.tag==110013||view.tag==110014)
                   {
                       [view removeFromSuperview];
                       [view release];
                   }
                }
            }
        }
    }
}

//table datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView1
{
    if(tableView1.tag==110015)
    {
        return 1;
    }
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView1 numberOfRowsInSection:(NSInteger)section
{
    if(tableView1.tag==110015)
    {
        return 6;
    } 
    else
    {
        if (planArray!=nil&&planArray.count!=0)
        {
            return [planArray count];
        }
        else
        {
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView1 cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView1.tag==110015)
    {
        static NSString *CellIdentifier = @"selNomalCell";
        UITableViewCell *cell =[tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil){
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.textLabel.textAlignment=UITextAlignmentCenter;
        cell.textLabel.textColor=[UIColor whiteColor];
        //cell.textLabel.text=[statusArray objectAtIndex:indexPath.row];
        switch (indexPath.row)
        {
            case 0:
                cell.textLabel.text=@"未完成";
                break;
            case 1:
                cell.textLabel.text=@"完成";
                break;
            case 2:
                cell.textLabel.text=@"终止";
                break;
            case 3:
                cell.textLabel.text=@"继续";
                break;
            case 4:
                cell.textLabel.text=@"暂缓";
                break;
            case 5:
                cell.textLabel.text=@"全部";
                break;
            default:
                break;
        }
        return cell;
    }
    else
    {
       static NSString *CellIdentifier = @"Cell";
       workplanCell *cell = (workplanCell*)[tableView1 dequeueReusableCellWithIdentifier:CellIdentifier];
       if (cell == nil){
        cell = [[[workplanCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
       }
       cell.selectionStyle=UITableViewCellSelectionStyleNone;
       if([[[planArray objectAtIndex:indexPath.row] objectForKey:@"planType"] isEqualToString:@"day"])
       {
           cell.typeLabel.text=[NSString stringWithFormat:@"[日计划]"];
       // cell.NameLabel.text=[NSString stringWithFormat:@"%@",[[planArray objectAtIndex:indexPath.row] objectForKey:@"empName"]];
       // cell.nameOfplan.text=[[planArray objectAtIndex:indexPath.row] objectForKey:@"title"];
       // cell.dateOfcreate.text=[[[planArray objectAtIndex:indexPath.row] objectForKey:@"createTime"] substringToIndex:10];
        }
        else if([[[planArray objectAtIndex:indexPath.row] objectForKey:@"planType"] isEqualToString:@"week"])
        {
           cell.typeLabel.text=[NSString stringWithFormat:@"[周计划]"];
//        cell.NameLabel.text=[NSString stringWithFormat:@"%@",[[planArray objectAtIndex:indexPath.row] objectForKey:@"empName"]];
        }
        else if([[[planArray objectAtIndex:indexPath.row] objectForKey:@"planType"] isEqualToString:@"month"])
        {
           cell.typeLabel.text=[NSString stringWithFormat:@"[月计划]"];
//        cell.NameLabel.text=[NSString stringWithFormat:@"%@",[[planArray objectAtIndex:indexPath.row] objectForKey:@"empName"]];
        }
        switch ([[[planArray objectAtIndex:indexPath.row] objectForKey:@"statusId"]intValue])
       {
        case 0:
            [cell.statusImage setImage:nil];
            break;
        case 1:
            [cell.statusImage setImage:[UIImage imageNamed:@"back_1.png"]];
            break;
        case 2:
            [cell.statusImage setImage:[UIImage imageNamed:@"back_2.png"]];
            break;
        case 3:
            [cell.statusImage setImage:[UIImage imageNamed:@"back_3.png"]];
            break;
        case 4:
            [cell.statusImage setImage:[UIImage imageNamed:@"back_4.png"]];
            break;
        default:
            break;
        }
        cell.NameLabel.text=[NSString stringWithFormat:@"%@",[[planArray objectAtIndex:indexPath.row] objectForKey:@"empName"]];
        cell.nameOfplan.text=[[planArray objectAtIndex:indexPath.row] objectForKey:@"title"];
        cell.dateOfcreate.text=[[[planArray objectAtIndex:indexPath.row] objectForKey:@"createTime"] substringToIndex:10];
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView1.tag==110015)
    {
        return 35;
    }
    else
    {
        return 40;
    }
}

- (void) tableView:(UITableView *)tableView1 didSelectRowAtIndexPath:(NSIndexPath *)indexPat
{
    if(tableView1.tag==110015)
    {
        [planArray removeAllObjects];
        [planArray addObjectsFromArray:copyPlanArray];
         //NSLog(@"[planArray count]=%d",[planArray count]);
        NSMutableArray *onceArray=[[NSMutableArray alloc]initWithCapacity:0];
        [onceArray removeAllObjects];
        switch (indexPat.row)
        {
            case 0:
                    for(int i=0;i<[planArray count];i++)
                    {
                        if([[[planArray objectAtIndex:i] objectForKey:@"statusId"] intValue]==0)
                        {
                            [onceArray addObject:[planArray objectAtIndex:i]];
                        }
                    }
                    [titleButton setTitle:@"工作计划(未完成)" forState:UIControlStateNormal];
                    break;
            case 1:
                    for(int i=0;i<[planArray count];i++)
                    {
                        if([[[planArray objectAtIndex:i] objectForKey:@"statusId"] intValue]==1)
                        {
                            [onceArray addObject:[planArray objectAtIndex:i]];
                        }
                    }
                    [titleButton setTitle:@"工作计划(完成)" forState:UIControlStateNormal];
                    break;
            case 2:
                    for(int i=0;i<[planArray count];i++)
                    {
                        if([[[planArray objectAtIndex:i] objectForKey:@"statusId"] intValue]==2)
                        {
                            [onceArray addObject:[planArray objectAtIndex:i]];
                        }
                    }
                    [titleButton setTitle:@"工作计划(终止)" forState:UIControlStateNormal];
                    break;
            case 3:
                    for(int i=0;i<[planArray count];i++)
                    {
                        if([[[planArray objectAtIndex:i] objectForKey:@"statusId"] intValue]==3)
                        {
                            [onceArray addObject:[planArray objectAtIndex:i]];
                        }
                    }
                    [titleButton setTitle:@"工作计划(继续)" forState:UIControlStateNormal];
                    break;
            case 4:
                    for(int i=0;i<[planArray count];i++)
                    {
                        if([[[planArray objectAtIndex:i] objectForKey:@"statusId"] intValue]==4)
                        {
                            [onceArray addObject:[planArray objectAtIndex:i]];
                        }
                    }
                    [titleButton setTitle:@"工作计划(暂缓)" forState:UIControlStateNormal];
                    break;
            case 5:
                    [onceArray addObjectsFromArray:planArray];
                    [titleButton setTitle:@"工作计划(全部)" forState:UIControlStateNormal];
                    break;
            default:
                    break;
            }
        
        for(UIView *view in [self.view subviews])
        {
            if(view.tag==110015||view.tag==110013||view.tag==110014)
            {
                [view removeFromSuperview];
                [view release];
            }
        }
        [planArray removeAllObjects];
        if([onceArray count]!=0)
        {
           [planArray addObjectsFromArray:onceArray];
        }        
        [onceArray release];
        if(planArray!=nil&&[planArray count]!=0)
        {
            [planList reloadData];
            float height=([planArray count])*40;
            [self drawTable:height];
        }
        else
        {
            [self drawTable:0];
        }
    }
    else
    {
        UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
        temporaryBarButtonItem.title = @"返回";
        self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
        JsonService *js=[JsonService sharedManager];
        [js setDelegate:self];
        NSString *userId=[NSString stringWithFormat:@"%@",[js getSessionID]];
        PlandetailViewController *planDetail=[[PlandetailViewController alloc]initWithNibName:nil bundle:nil];
        if(![[[planArray objectAtIndex:indexPat.row] objectForKey:@"empId"] isEqualToString:userId])
        {
            [planDetail setSysTohere:YES];
        }
        //[planDetail setPlanTitle:[[planArray objectAtIndex:indexPat.row] objectForKey:@"title"]];
        //[planDetail setUpdateTime:[[planArray objectAtIndex:indexPat.row] objectForKey:@"lastUpdateTime"]];
        [planDetail setPlanId:[[planArray objectAtIndex:indexPat.row] objectForKey:@"planId"]];
        [self.navigationController pushViewController:planDetail animated:YES];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView1 editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView1.tag==110015)
    {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView1 canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if(tableView1.tag==110015)
    {
        return NO;
    }
//    JsonService *js=[JsonService sharedManager];
//    [js setDelegate:self];
//    NSString *userId=[NSString stringWithFormat:@"%@",[js getSessionID]];
//    if(![[[planArray objectAtIndex:indexPath.row] objectForKey:@"empId"] isEqualToString:userId])
//    {
//        return NO;
//    }
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView1 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView1.tag==110015)
    {
        return;
    }
    JsonService *js=[JsonService sharedManager];
    [js setDelegate:self];
    NSString *userId=[NSString stringWithFormat:@"%@",[js getSessionID]];
    if([[[planArray objectAtIndex:indexPath.row] objectForKey:@"empId"] isEqualToString:userId])
    {
        [js deleteWorkPlan:[[planArray objectAtIndex:indexPath.row] objectForKey:@"planId"]];
        isDelete=YES;
        [planArray removeObjectAtIndex:indexPath.row];
        [tableView1 reloadData];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:@"不能删除他人计划" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil,nil];
        [alert show];
        [alert release];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}


-(void)writePlan:(id)sender
{
#warning notfinish
//    UIBarButtonItem *temporaryBarButtonItem = [[UIBarButtonItem alloc] init];
//    temporaryBarButtonItem.title = @"返回";
//    self.navigationItem.backBarButtonItem = temporaryBarButtonItem;
//    WritePlanViewController *WritePlanView=[[WritePlanViewController alloc]initWithNibName:nil bundle:nil];
//    WritePlanView.hidesBottomBarWhenPushed=YES;
//    [WritePlanView setStartDate:self.startTime];
//    [WritePlanView setEndDate:self.endTime];
//    [WritePlanView setPlanId:nil];
//    if(dayButton.selected==YES)
//    {
//        [WritePlanView setPlanType:@"day"];
//    }
//    else if(weekButton.selected==YES)
//    {
//        [WritePlanView setPlanType:@"week"];
//        [WritePlanView setWeekStr:self.weekStr];
//    }
//    else
//    {
//       [WritePlanView setPlanType:@"month"];
//    }
//    [self.navigationController pushViewController:WritePlanView animated:YES];
//    //[WritePlanView release];
//    [temporaryBarButtonItem release];
}

-(void)dayBtnPressed:(id)sender
{
    [[JsonService sharedManager] setDelegate:nil];
    [[JsonService sharedManager] cancelAllRequest];
    
    
    [[JsonService sharedManager] setDelegate: self];
    
    NSLog(@"self.selectDate=%@",self.selectDate);
    NSLog(@"NSDate=%@",[NSDate date]);
    dayButton.selected=YES;
    weekButton.selected=NO;
    monthButton.selected=NO;
    [dayButton setBackgroundColor:[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0]];
    [weekButton setBackgroundColor:[UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0]];
    [monthButton setBackgroundColor:[UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0]];
    self.planType=[NSString stringWithFormat:@"day"];
    if(self.selectDate==nil)
    {
        self.selectDate=[NSDate date];
        NSTimeInterval strSec=8*60*60;
        NSString *nowStr=[NSString stringWithFormat:@"%@",self.selectDate];
        NSString *nowCutstr=[nowStr substringToIndex:10];
        NSString *addnowStr=[NSString stringWithFormat:@"%@ 00:00:00",nowCutstr];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:addnowStr];
        self.selectDate=[date dateByAddingTimeInterval:strSec];
    }
    self.startTime=[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:0]];
    NSTimeInterval endSec=24*60*60-1;
    self.endTime =[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:endSec]];
    self.startTime=[self.startTime substringToIndex:19];
    self.endTime=[self.endTime substringToIndex:19];
    if([self.startTime isEqualToString:self.laststartTime]&&[self.endTime isEqualToString:self.lastendTime]&&[self.planType isEqualToString:self.lastplanType])
    {
        return;
    }
    [self showTellLabel];
    NSLog(@"self.startTime=%@,self.endTime=%@",self.startTime,self.endTime);
    [self getPlandata:self.planType str:self.startTime end:self.endTime];
    self.laststartTime=self.startTime;
    self.lastendTime=self.endTime;
    self.lastplanType=self.planType;
    
}

-(void)weekBtnPressed:(id)sender
{
    [[JsonService sharedManager] setDelegate:nil];
    [[JsonService sharedManager] cancelAllRequest];
    
    
    [[JsonService sharedManager] setDelegate: self];
    
   weekButton.selected=YES;
   dayButton.selected=NO;
   monthButton.selected=NO;
   [dayButton   setBackgroundColor:[UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0]];
   [weekButton  setBackgroundColor:[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0]];
   [monthButton setBackgroundColor:[UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0]];
   self.planType=[NSString stringWithFormat:@"week"];
   if(self.selectDate==nil)
   {    
        self.selectDate=[NSDate date];//当前时间
        NSTimeInterval strSec=8*60*60;
        NSString *nowStr=[NSString stringWithFormat:@"%@",self.selectDate];
        NSString *nowCutstr=[nowStr substringToIndex:10];
        NSString *addnowStr=[NSString stringWithFormat:@"%@ 00:00:00",nowCutstr];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:addnowStr];
        self.selectDate=[date dateByAddingTimeInterval:strSec];
        //NSLog(@"%@",selectDate);
        [dateFormatter release];
   }
   NSCalendar*calendar = [NSCalendar currentCalendar];
   NSDateComponents*comps;
   comps=[calendar components:(NSWeekCalendarUnit | NSWeekdayCalendarUnit |NSWeekdayOrdinalCalendarUnit) fromDate:self.selectDate];
   NSInteger week = [comps week];
   NSLog(@"week=%d",week);
   self.weekStr=[NSString stringWithFormat:@"%d",week];
   NSInteger weekday = [comps weekday];
   NSLog(@"weekday=%d",weekday);
    switch (weekday)
    {
        case 1:
            {
              self.startTime=[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:0]];
          //    NSLog(@"startTime=%@",startTime);
              NSTimeInterval endSec=7*24*60*60-1;
              self.endTime =[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:endSec]];
         //     NSLog(@"endTime=%@",endTime);
              break;
            }
        case 2:
            {
               NSTimeInterval strSec=24*60*60;
              self.startTime=[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:-strSec]];
           //    NSLog(@"startTime=%@",startTime);
               NSTimeInterval endSec=6*24*60*60-1;
               self.endTime =[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:endSec]];
          //     NSLog(@"endTime=%@",endTime);
               break;
            }
        case 3:
           {
               NSTimeInterval strSec=2*24*60*60;
               self.startTime=[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:-strSec]];
           //    NSLog(@"startTime=%@",startTime);
               NSTimeInterval endSec=5*24*60*60-1;
               self.endTime =[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:endSec]];
          //     NSLog(@"endTime=%@",endTime);
               break;
           }
        case 4:
           {
               NSTimeInterval strSec=3*24*60*60;
               self.startTime=[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:-strSec]];
           //    NSLog(@"startTime=%@",startTime);
               NSTimeInterval endSec=4*24*60*60-1;
               self.endTime =[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:endSec]];
            //   NSLog(@"endTime=%@",endTime);
               break;
           }
        case 5:
          {
              NSTimeInterval strSec=4*24*60*60;
              self.startTime=[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:-strSec]];
           //   NSLog(@"startTime=%@",startTime);
              NSTimeInterval endSec=3*24*60*60-1;
              self.endTime =[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:endSec]];
           //   NSLog(@"endTime=%@",endTime);
              break;
          }
        case 6:
         {
             NSTimeInterval strSec=5*24*60*60;
             self.startTime=[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:-strSec]];
           //  NSLog(@"startTime=%@",startTime);
             NSTimeInterval endSec=2*24*60*60-1;
             self.endTime =[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:endSec]];
          //   NSLog(@"endTime=%@",endTime);
             break;
         }
        case 7:
        {
            NSTimeInterval strSec=6*24*60*60;
            self.startTime=[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:-strSec]];
           // NSLog(@"startTime=%@",startTime);
            NSTimeInterval endSec=24*60*60-1;
            self.endTime =[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:endSec]];
          //  NSLog(@"endTime=%@",endTime);
            break;
        }
        default:
            break;
    }
    self.startTime=[self.startTime substringToIndex:19];
    self.endTime=[self.endTime substringToIndex:19];
    if([self.startTime isEqualToString:self.laststartTime]&&[self.endTime isEqualToString:self.lastendTime]&&[self.planType isEqualToString:self.lastplanType])
    {
        return;
    }
    [self showTellLabel];
    NSLog(@"self.startTime=%@,self.endTime=%@",self.startTime,self.endTime);
    [self getPlandata:self.planType str:self.startTime end:self.endTime];
    self.laststartTime=self.startTime;
    self.lastendTime=self.endTime;
    self.lastplanType=self.planType;
}

-(void)monthBtnPressed:(id)sender
{
    
    [[JsonService sharedManager] setDelegate:nil];
    [[JsonService sharedManager] cancelAllRequest];
    
    
    [[JsonService sharedManager] setDelegate: self];
    
    monthButton.selected=YES;
    dayButton.selected=NO;
    weekButton.selected=NO;
    [dayButton setBackgroundColor:[UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0]];
    [weekButton setBackgroundColor:[UIColor colorWithRed:168.0/255.0 green:168.0/255.0 blue:168.0/255.0 alpha:1.0]];
    [monthButton setBackgroundColor:[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1.0]];
    self.planType=[NSString stringWithFormat:@"month"];
    if(self.selectDate==nil)
    {
        self.selectDate=[NSDate date];
        NSTimeInterval strSec=8*60*60;
        NSString *nowStr=[NSString stringWithFormat:@"%@",[self.selectDate dateByAddingTimeInterval:0]];
        NSString *nowCutstr=[nowStr substringToIndex:10];
        NSString *addnowStr=[NSString stringWithFormat:@"%@ 00:00:00",nowCutstr];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormatter dateFromString:addnowStr];
        NSLog(@"date=%@", date);
        self.selectDate=[date dateByAddingTimeInterval:strSec];
        [dateFormatter release];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *selectDateStr= [dateFormatter stringFromDate:self.selectDate];
    [dateFormatter release];
    //NSLog(@"selectDateStr=%@",selectDateStr);
    int yearNum=[[selectDateStr substringToIndex:5] intValue];
    //NSLog(@"yearNum=%d",yearNum);
    int monthNum=[[selectDateStr substringWithRange:NSMakeRange(6,1)] intValue];
    //NSLog(@"monthNum=%d",monthNum);
    if(monthNum==1||monthNum==3||monthNum==5||monthNum==7||monthNum==8||monthNum==10||monthNum==12)
    {
        self.startTime=[NSString stringWithFormat:@"%d-0%d-01 00:00:00",yearNum,monthNum];
        self.endTime=[NSString stringWithFormat:@"%d-0%d-31 23:59:59",yearNum,monthNum];
    }
    else if(monthNum==4||monthNum==6||monthNum==9||monthNum==11)
    {
        self.startTime=[NSString stringWithFormat:@"%d-0%d-01 00:00:00",yearNum,monthNum];
        self.endTime=[NSString stringWithFormat:@"%d-0%d-30 23:59:59",yearNum,monthNum];
    }
    else if(monthNum==2)
    {
        if ((yearNum%4==0&&yearNum%100!=0)||(yearNum%400==0))
        {
            self.startTime=[NSString stringWithFormat:@"%d-0%d-01 00:00:00",yearNum,monthNum];
            self.endTime=[NSString stringWithFormat:@"%d-0%d-29 23:59:59",yearNum,monthNum];
        }
        else
        {
            self.startTime=[NSString stringWithFormat:@"%d-0%d-01 00:00:00",yearNum,monthNum];
            self.endTime=[NSString stringWithFormat:@"%d-0%d-28 23:59:59",yearNum,monthNum];
        }
    }
    else
    {
        NSLog(@"不存在的月份");
    }
    self.startTime=[self.startTime substringToIndex:19];
    self.endTime=[self.endTime substringToIndex:19];
    if([self.startTime isEqualToString:self.laststartTime]&&[self.endTime isEqualToString:self.lastendTime]&&[self.planType isEqualToString:self.lastplanType])
    {
        self.laststartTime=self.startTime;
        self.lastendTime=self.endTime;
        self.lastplanType=self.planType;
        return;
    }
    [self showTellLabel];
    NSLog(@"self.startTime=%@,self.endTime=%@",self.startTime,self.endTime);
    [self getPlandata:self.planType str:self.startTime end:self.endTime];
    self.laststartTime=self.startTime;
    self.lastendTime=self.endTime;
    self.lastplanType=self.planType;
}

-(void)getPlandata:(NSString*)type str:(NSString*)strTime end:(NSString*)eTime
{
    //NSLog(@"[startTime retainCount]=%d,[endTime retainCount]=%d",[startTime retainCount],[endTime retainCount]);
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice getWorkPlanList:type str:strTime end:eTime];
    isDelete=NO;
}

#pragma mark - 123
-(void)calendarBtnPressed:(id)sender
{
    NSLog(@"ckCalendar.frame.size.height=%f",ckCalendar.frame.size.height);
    UIButton *btn=(UIButton*)sender;
    if(btn.selected==NO)
    {
        btn.selected=YES;
        self.ckCalendar.hidden=YES;  
    }
    else
    {
        btn.selected=NO;
        self.ckCalendar.hidden=NO;
    }
    if(planArray!=nil&&[planArray count]!=0)
    {
       [self drawTable:[planArray count]*40];
    }
    else
    {
       [self drawTable:0];
    }
}
//时间btn按钮触发
-(void)getDatebtnDate
{
    if(dayButton.selected==YES)
    {
        [self dayBtnPressed:dayButton];
    }
    else if(weekButton.selected==YES)
    {  
        [self weekBtnPressed:weekButton];
    }
    else if(monthButton.selected==YES)
    {
        [self monthBtnPressed:monthButton];
    }
}

-(void)changeTableFrame
{
    NSLog(@"[ckCalendar retainCount]=%d",[ckCalendar retainCount]);
    NSLog(@"[planList retainCount]=%d",[planList retainCount]);
    float tableHeigth=0;
    if(planArray==nil&&planArray.count==0)
    {
        tableHeigth=0;
    }
    else
    {
       tableHeigth=[planArray count]*40;  
    } 
    if(tableHeigth<self.view.frame.size.height-self.ckCalendar.frame.size.height-self.ckCalendar.frame.origin.y)
    {
        if(self.ckCalendar.hidden==YES)
        {
            self.planList.frame=CGRectMake(0, 57, self.view.frame.size.width,tableHeigth);
        }
        else
        {
            self.planList.frame=CGRectMake(0, self.ckCalendar.frame.size.height+self.ckCalendar.frame.origin.y, self.view.frame.size.width,tableHeigth);
        }
    }
    else
    {
        if(self.ckCalendar.hidden==YES)
        {
            if(tableHeigth<self.view.frame.size.height-57)
            {
                self.planList.frame=CGRectMake(0, 57, self.view.frame.size.width, tableHeigth);
            }
            else
            {
                self.planList.frame=CGRectMake(0, 57, self.view.frame.size.width, self.view.frame.size.height-57);
            }
        }
        else
        {
            self.planList.frame=CGRectMake(0,self.ckCalendar.frame.size.height+self.ckCalendar.frame.origin.y, self.view.frame.size.width,self.view.frame.size.height-self.ckCalendar.frame.size.height-self.ckCalendar.frame.origin.y);
        }
    }
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    UIImage *image = [[UIImage imageNamed:@"btn_big_gray"] stretchableImageWithLeftCapWidth:5 topCapHeight:10];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTitle:@"写计划" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [button addTarget:self action:@selector(writePlan:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [button release];
    self.navigationItem.rightBarButtonItem = rightItem;
    [rightItem release];

}

- (void)backButtonPressed {
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"BaseNavigationControllerBackgroundColorChangeBlack" object:nil];
    [self.navigationController dismissModalViewControllerAnimated:YES];
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
