//
//  summaryCell.h
//  moffice
//
//  Created by lijinhua on 13-7-10.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface summaryCell : UITableViewCell
{
    UILabel *countTimeLabel;
    UIButton *revButton;
    UIButton *sumButton;
    UIWebView *contentWeb;
    UILabel *timeLabel;
}
@property(nonatomic,retain) UILabel *countTimeLabel;
@property(nonatomic,retain) UIButton *revButton;
@property(nonatomic,retain) UIButton *sumButton;
@property(nonatomic,retain) UIWebView *contentWeb;
@property(nonatomic,retain) UILabel *timeLabel;
@end
