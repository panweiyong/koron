//
//  nomalCell.m
//  moffice
//
//  Created by lijinhua on 13-7-8.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "nomalCell.h"

@implementation nomalCell
@synthesize nameLabel,timeLabel,contentLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawCell];
    }
    return self;
}

-(void)dealloc
{
    [timeLabel release];
    [nameLabel release];
    [contentLabel release];
    [super dealloc];
}

-(void)drawCell
{
    nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 2, 120, 15)];
    nameLabel.backgroundColor=[UIColor clearColor];
    nameLabel.textColor=[UIColor blackColor];
    nameLabel.font=[UIFont fontWithName:@"Arial" size:10.0];
    nameLabel.textAlignment=UITextAlignmentLeft;
    [self addSubview:nameLabel];
    
    timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(150, 5, 155, 10)];
    timeLabel.backgroundColor=[UIColor clearColor];
    timeLabel.textColor=[UIColor grayColor];
    timeLabel.font=[UIFont fontWithName:@"Arial" size:8.0];
    timeLabel.textAlignment=UITextAlignmentRight;
    [self addSubview:timeLabel];
    
    contentLabel=[[UILabel alloc]initWithFrame:CGRectMake(10, 25, 300, 0)];
    contentLabel.backgroundColor=[UIColor clearColor];
    contentLabel.textColor=[UIColor blackColor];
    contentLabel.font=[UIFont fontWithName:@"Arial" size:10.0];
    contentLabel.textAlignment=UITextAlignmentLeft;
    [contentLabel setNumberOfLines:0];
    contentLabel.lineBreakMode = UILineBreakModeWordWrap;
    [self addSubview:contentLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
