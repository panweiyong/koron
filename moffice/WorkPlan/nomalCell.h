//
//  nomalCell.h
//  moffice
//
//  Created by lijinhua on 13-7-8.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface nomalCell : UITableViewCell
{
    UILabel *nameLabel;
    UILabel *timeLabel;
    UILabel *contentLabel;
}
@property(nonatomic,retain) UILabel *nameLabel;
@property(nonatomic,retain) UILabel *timeLabel;
@property(nonatomic,retain) UILabel *contentLabel;
@end
