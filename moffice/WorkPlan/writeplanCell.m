//
//  writeplanCell.m
//  moffice
//
//  Created by lijinhua on 13-6-27.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "writeplanCell.h"

@implementation writeplanCell
@synthesize deleteBtn,nameLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self drawCell];
    }
    return self;
}

-(void)dealloc
{
    [nameLabel release];
    [super dealloc];
}

-(void)drawCell 
{
    deleteBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    deleteBtn.frame=CGRectMake(20, 5, 30, 30);
    [deleteBtn setBackgroundImage:[UIImage imageNamed:@"compose_card_delete_normal.png"] forState:UIControlStateNormal];
    [self addSubview:deleteBtn];
    
    
    nameLabel=[[UILabel alloc]initWithFrame:CGRectMake(55, 0, self.frame.size.width-55, 40)];
    [nameLabel setBackgroundColor:[UIColor clearColor]];
    nameLabel.textAlignment=UITextAlignmentLeft;
    [self addSubview:nameLabel];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
