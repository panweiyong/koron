//
//  MailCell.m
//  moffice
//
//  Created by yangxi zou on 12-1-17.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.


#import "MailCell.h"



@interface MailCellContentView : UIView
{
    Mail *_cell;
    BOOL _highlighted;
}

@end

@implementation MailCellContentView

- (id)initWithFrame:(CGRect)frame cell:(Mail *)cell
{
    if (self = [super initWithFrame:frame])
    {
        _cell = cell;
        self.opaque = YES;
        self.backgroundColor = [UIColor clearColor];//_cell.backgroundColor;
        
        
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    [[UIColor colorWithWhite:0.4 alpha:1] set];

    CGSize timesize = [_cell.receivedTime drawInRect:CGRectMake(230, 10, 90, 20) withFont:[UIFont systemFontOfSize:11] lineBreakMode:NSLineBreakByCharWrapping alignment:NSTextAlignmentCenter];
    //[@"time" drawAtPoint:CGPointMake(10, 5.0) withFont:[UIFont boldSystemFontOfSize:11.0]];
    
    if(_cell.hasattach)
    {
        UIImage *img= [UIImage imageNamed:@"attach.png"];

        if (_cell.receivedTime.length == 2) {
            [img drawInRect:CGRectMake(265-timesize.width, 12, 12, 12)];
            
            [[UIColor blackColor] set];
            [_cell.from drawInRect:CGRectMake(30.0, 10.0,240-timesize.width,20) withFont:[UIFont boldSystemFontOfSize:18]];
        }else if (_cell.receivedTime.length > 9) {
            [img drawInRect:CGRectMake(265-timesize.width+20, 12, 12, 12)];
            
            [[UIColor blackColor] set];
            [_cell.from drawInRect:CGRectMake(30.0, 10.0,240-timesize.width,20) withFont:[UIFont boldSystemFontOfSize:18]];
        }
        else
        {
            [img drawInRect:CGRectMake(255-timesize.width+20, 12, 12, 12)];
            
            [[UIColor blackColor] set];
            [_cell.from drawInRect:CGRectMake(30.0, 10.0,230-timesize.width,20) withFont:[UIFont boldSystemFontOfSize:18]];
        }
    } else {

        if (_cell.receivedTime.length > 9) {
            [[UIColor blackColor] set];
            [_cell.from drawInRect:CGRectMake(30.0, 10.0,245-timesize.width,20) withFont:[UIFont boldSystemFontOfSize:18]];
        } else {
            [[UIColor blackColor] set];
            [_cell.from drawInRect:CGRectMake(30.0, 10.0,240-timesize.width,20) withFont:[UIFont boldSystemFontOfSize:18]];
        }
        
    }
    
    
    
}

- (void)setHighlighted:(BOOL)highlighted
{
    _highlighted = highlighted;
    [self setNeedsDisplay];
}

- (BOOL)isHighlighted
{
    return _highlighted;
}

@end

@implementation Mail
 
@synthesize mid,bid,fid,from,intranet,hasattach,receivedTime;

-(void)dealloc 
{
    [mid release];
    [bid release];
    [fid release];
    [from release];
    [receivedTime release];
    [super dealloc];
}

- (void)setHighlighted:(BOOL)highlighted
{
    _highlighted = highlighted;
    [self setNeedsDisplay];
}

- (BOOL)isHighlighted
{
    return _highlighted;
}
-(void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    //开始一条线
    CGContextSaveGState(context);
    CGContextSetStrokeColorWithColor(context,MAILLLINE_BACKGROUND );
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0.0, 74.0);
    CGContextAddLineToPoint(context, 320.0, 74.0);
    CGContextStrokePath(context);
    
    CGContextSetRGBStrokeColor(context, 251.0f/255.0f, 251.0f/255.0f,252.0f/255.0f, 1.0);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0.0, 75.0);
    CGContextAddLineToPoint(context, 320.0, 75.0);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    //画渐变
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 1.0, 1.0, 1.0, 0.35,  // Start color
        1.0, 1.0, 1.0, 0.06 }; // End color
    
    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
    
    CGRect currentBounds = self.bounds;
    CGPoint topCenter = CGPointMake(CGRectGetMidX(currentBounds), 0.0f);
    CGPoint midCenter = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetMidY(currentBounds));
    CGContextDrawLinearGradient(context, glossGradient, topCenter, midCenter, 0);
    
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace); 
    
    /*
     CAGradientLayer *gradient = [CAGradientLayer layer];
     gradient.frame = self.bounds;
     gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor blackColor] CGColor], (id)[[UIColor whiteColor] CGColor], nil];
     [self.layer insertSublayer:gradient atIndex:0];
     */

}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.textLabel.frame = CGRectMake(30,50,self.textLabel.frame.size.width,self.textLabel.frame.size.height);
    self.detailTextLabel.frame = CGRectMake(30,40,self.detailTextLabel.frame.size.width,self.detailTextLabel.frame.size.height);
    self.imageView.frame = CGRectMake(self.imageView.frame.origin.x,26,12,12);
    
}

@end

@implementation MailCell

- (void)dealloc 
{
    [cellContentView release];
    [super dealloc];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        cellContentView = [[MailCellContentView alloc] initWithFrame:CGRectInset(self.contentView.bounds, 0.0, 1.0) cell:self];
        cellContentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        cellContentView.contentMode = UIViewContentModeLeft;
        [self.contentView addSubview:cellContentView];        
    }
    
    return self;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
    //  cellContentView.backgroundColor = backgroundColor;
}
@end

