//
//  KoronHomePageOptionView.h
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KoronHomePageOptionView : UIView {
    UIButton *_optionButton;
    
    UILabel *_optionLabel;
}

@property (nonatomic,strong)UIButton *optionButton;
@property (nonatomic,strong)UILabel *optionLabel;

@end
