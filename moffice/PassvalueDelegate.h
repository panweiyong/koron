//
//  PassvalueDelegate.h
//  moffice
//
//  Created by koron on 13-5-30.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PassValueDelegate <NSObject>

- (void)passValue:(NSArray *)value;


@end
