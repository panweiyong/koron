//
//  QueryViewController.m
//  moffice
//
//  Created by yangxi zou on 12-2-3.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "QueryViewController.h"


@implementation QueryViewController

@synthesize tableData;
@synthesize target,moduleid;

-(void)dealloc
{
    [tableData release];
    [target release];
    [moduleid release];
    [footerView release];
    [super dealloc];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView setBackgroundColor:DETAILPAGE_BG]; 
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{ 
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{ 
    // Return the number of rows in the section.
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }
    NSDictionary *dd=[tableData objectAtIndex:indexPath.row] ;
    cell.textLabel.text = [dd objectForKey:@"name"];//format
    cell.textLabel.font=  [UIFont boldSystemFontOfSize:15];
    // Configure the cell...
    [[cell.contentView viewWithTag:10+indexPath.row] removeFromSuperview];
   // cell.detailTextLabel.text = [dd objectForKey:@"id"];
   // cell.detailTextLabel.isOpaque = [[dd objectForKey:@"enabledNull"] boolValue];
   // cell.detailTextLabel.hidden=YES;
    if ([[dd objectForKey:@"format"] isEqualToString:@"TEXT"]) {
       
        UITextField* hostText = [[UITextField alloc] initWithFrame:CGRectMake(105, 5, 180, 20)];
        hostText.backgroundColor = [UIColor clearColor];
        hostText.borderStyle = UITextBorderStyleNone; 
        //hostText.placeholder =  NSLocalizedString(@"input Host",@"input Host");
        hostText.tag=10+indexPath.row; 
        //hostText.text = [[JsonService sharedManager] getHost];
        //hostText.returnKeyType = UIReturnKeyDefault;
        hostText.delegate=self; 
        //hostText.keyboardAppearance = UIKeyboardAppearanceAlert ;
        hostText.autocorrectionType = UITextAutocorrectionTypeNo;
        hostText.autocapitalizationType = UITextAutocapitalizationTypeNone;
        //hostText.returnKeyType = UIReturnKeyGo;
        hostText.clearButtonMode = UITextFieldViewModeWhileEditing; //编辑时会出现个修改X
        hostText.keyboardType = UIKeyboardTypeURL;
        //iamount.userInteractionEnabled=NO;
        //[iamount becomeFirstResponder ];
        [cell.contentView addSubview:hostText]; 
        [hostText release];		
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    }else if( ([[dd objectForKey:@"format"] isEqualToString:@"DATE"])){
        UITextField* dateText = [[UITextField alloc] initWithFrame:CGRectMake(105, 5, 180, 20)];
        dateText.backgroundColor = [UIColor clearColor];
        dateText.borderStyle = UITextBorderStyleNone; 
        //hostText.placeholder =  NSLocalizedString(@"input Host",@"input Host");
        dateText.tag=10+indexPath.row; 
        //hostText.text = [[JsonService sharedManager] getHost];
        dateText.enabled=NO;
        dateText.delegate=self; 
        //hostText.keyboardAppearance = UIKeyboardAppearanceAlert ;
        dateText.autocorrectionType = UITextAutocorrectionTypeNo;
        dateText.autocapitalizationType = UITextAutocapitalizationTypeNone;
        //hostText.returnKeyType = UIReturnKeyGo;
        dateText.clearButtonMode = UITextFieldViewModeWhileEditing; //编辑时会出现个修改X
        dateText.keyboardType = UIKeyboardTypeDefault;
        //iamount.userInteractionEnabled=NO;
        //[iamount becomeFirstResponder ];
        [cell.contentView addSubview:dateText]; 
        [dateText release];		
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue ;
    }else if( ([[dd objectForKey:@"format"] isEqualToString:@"NUMBER"])){
        UITextField* dateText = [[UITextField alloc] initWithFrame:CGRectMake(105, 5, 180, 20)];
        dateText.backgroundColor = [UIColor clearColor];
        dateText.borderStyle = UITextBorderStyleNone; 
        //hostText.placeholder =  NSLocalizedString(@"input Host",@"input Host");
        dateText.tag=10+indexPath.row; 
        //hostText.text = [[JsonService sharedManager] getHost];
        //dateText.enabled=NO;
        dateText.delegate=self; 
        //hostText.keyboardAppearance = UIKeyboardAppearanceAlert ;
        dateText.autocorrectionType = UITextAutocorrectionTypeNo;
        dateText.autocapitalizationType = UITextAutocapitalizationTypeNone;
        //hostText.returnKeyType = UIReturnKeyGo;
        dateText.clearButtonMode = UITextFieldViewModeWhileEditing; //编辑时会出现个修改X
        dateText.keyboardType = UIKeyboardTypeDecimalPad;
        //iamount.userInteractionEnabled=NO;
        //[iamount becomeFirstResponder ];
        [cell.contentView addSubview:dateText]; 
        [dateText release];		
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleBlue ;
    }
    
    return cell;
  
}

- (UIView *)tableView:(UITableView *)tableViewx viewForFooterInSection:(NSInteger)section {
	
	if(footerView == nil) {
        footerView  = [[UIView alloc] init];
        //UIImage *image = [[UIImage imageNamed:@"button_red.png"] stretchableImageWithLeftCapWidth:8 topCapHeight:8];
        //footerView.backgroundColor=[UIColor redColor];
        //[footerView setAutoresizingMask: UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth];
        //create the button
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        //[button setBackgroundImage:image forState:UIControlStateNormal];
        [button setFrame:CGRectMake(10, 3, 300, 35)];
        button.tag = 15895;
        //set title, font size and font color
        [button setTitle:NSLocalizedString(@"OK",@"OK")  forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
        //[button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal]; 
        //[button setAutoresizingMask: UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth];
        //set action of the button
        [button addTarget:self action:@selector(submitMe) forControlEvents:UIControlEventTouchUpInside];
        button.center =  CGPointMake(   self.tableView.center.x,button.frame.origin.y +20 );// self.tableView.center;
        [footerView addSubview:button];
	}
	return footerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 55;
}
- (CGFloat)tableView:(UITableView *)tableViewz heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}
-(void)submitMe
{
    UIButton *button = (UIButton*)[footerView viewWithTag:15895];
    [button setEnabled:NO];
    
    NSMutableDictionary *pas=[[NSMutableDictionary alloc] init];
    for (int i=0; i<[tableData count]; i++) {
        UITextField *t=(UITextField *) [self.view viewWithTag:10+i];
        
        if (![[[tableData objectAtIndex:i] objectForKey:@"enabledNull" ] boolValue ]) {
        if(t.text==nil||[@"" isEqualToString:t.text]) 
        {
            [ModalAlert prompt:[NSString stringWithFormat:@"%@%@" ,[[tableData objectAtIndex:i] objectForKey:@"name" ]  ,@"必须输入." ]];
            return;
        }  
        }
        if(t.text)[pas setObject:t.text forKey:[[tableData objectAtIndex:i] objectForKey:@"id"]   ];
    }
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice loadModuleWithParameters:self.target parameters:pas mid:self.moduleid];
    [pas release];
    
}
- (void)requestDataFinished:(id)jsondata//request
{ 
    
    UIButton *button = (UIButton*)[footerView viewWithTag:15895];
    [button setEnabled:YES];
    
    id rData = [jsondata retain];
    //CCLOG(@"%@",[tableData valueForKey:@"parameterKey"]);//传旨
    
    int viewtype= [[rData valueForKey:@"type"] intValue];//布局
    NSString *keyname =[rData valueForKey:@"parameterKey"];
    NSString *keyname2 =[rData valueForKey:@"parameterKey2"];
    
    
    if (viewtype==1) {
        //列表视图显示
        
        ListViewController *listviewc=[[ListViewController alloc] initWithStyle:UITableViewStylePlain];
        [listviewc setLayout:[[rData valueForKey:@"layout"] intValue]];
        [listviewc setModuleid:self.moduleid];
        [listviewc setParameterKey:keyname];
        [listviewc setParameterKey2:keyname2];
        [listviewc setTableData:[[rData objectForKey:@"data"]retain]]; 
        [listviewc setTitle:self.title];
        [listviewc setValue1name:[rData objectForKey:@"valuename1"]];
        [listviewc setValue2name:[rData objectForKey:@"valuename2"]];
        [listviewc setValue3name:[rData objectForKey:@"valuename3"]];
        [self.navigationController pushViewController:listviewc animated:YES];
        [listviewc release];
        
    }else if(viewtype==2){
        
        DetailsViewController *dviewc =[[DetailsViewController alloc]initWithStyle:UITableViewStyleGrouped];
        [dviewc setTableData:[[rData objectForKey:@"data"]retain] ]; 
        [dviewc setTitle:self.title];
        [dviewc setModuleid:self.moduleid];
        [self.navigationController pushViewController:dviewc animated:YES];
        [dviewc release];
    }
    
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
int selectedrow;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [(UITextField *)[self.view viewWithTag:10+indexPath.row] becomeFirstResponder];
    
    NSDictionary *dd=[tableData objectAtIndex:indexPath.row] ;
    if (![[dd objectForKey:@"format"] isEqualToString:@"DATE"])return;
    
    NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n\n\n\n\n\n\n\n" : @"\n\n\n\n\n\n\n\n\n\n\n\n" ;

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK",@"OK"), nil];
	[actionSheet showFromTabBar:self.tabBarController.tabBar ];
    
	
	UIDatePicker *datePicker = [[[UIDatePicker alloc] init] autorelease];
	datePicker.tag = 101;
	datePicker.datePickerMode = UIDatePickerModeDate;
	[actionSheet addSubview:datePicker];
    selectedrow = indexPath.row;
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	UIDatePicker *datePicker = (UIDatePicker *)[actionSheet viewWithTag:101];
	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	
    formatter.dateFormat = @"yyyy-MM-dd";
	
	NSString *timestamp = [formatter stringFromDate:datePicker.date];
	[(UITextField *)[self.view viewWithTag:10+selectedrow] setText:timestamp];
	[actionSheet release];
}
@end
