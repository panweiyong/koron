//
//  MailFolderController.m
//  moffice
//
//  Created by yangxi zou on 12-1-17.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "MailFolderController.h"
#import "BaseNavigationController.h"




@implementation MailFolderController

@synthesize boxid,folderid,isintranet,deleteDic,accounts;
@synthesize isReply;

BOOL ismuildelete;

-(void)dealloc
{
    [_endView release], _endView = nil;
    [accounts release];
    [boxid release];
    [folderid release];
    [deleteDic release];
     
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        ismuildelete = NO;
        deleteDic = [[NSMutableDictionary alloc] init];
    }
    return self;
}


//发起请求到服务器
-(void)requestJsonData:(NSString*)lastmid
{
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice listMail:self.boxid fid:self.folderid lastmid:lastmid];
    
    
}

-(void)displayMore
{
    if (tableData!=nil) {
        [self requestJsonData: [[tableData objectAtIndex: [tableData count]-1 ] objectForKey:@"id" ] ];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1; 
}

- (NSInteger)tableView:(UITableView *)tableViewx numberOfRowsInSection:(NSInteger)section {
    if (tableView.isEditing){
        return  [tableData count];
    }else{
    if ([tableData count]%20==0&&[tableData count]>0) {
        return  [tableData count]+1;
    }
        return  [tableData count];
    }
}
 
- (UITableViewCell *)tableView:(UITableView *)tableViewz cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	//dlog(@"%f",[[[UIDevice currentDevice] systemVersion] floatValue] );
    if(indexPath.row==[tableData count]){
        
//        static NSString *cellid= @"mcid" ;
//        UITableViewCellStyle style = UITableViewCellStyleDefault; 
//        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
//        if (!cell) 
//        {
//            cell = [[[ UITableViewCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
//        }
//        
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//        //[button setBackgroundImage:image forState:UIControlStateNormal];
//        [button setFrame:CGRectMake(20, 10, 280, 30)];	
//        //set title, font size and font color
//        [button setTitle:NSLocalizedString(@"display 20",@"display 20")  forState:UIControlStateNormal];
//        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
//    
//        [button addTarget:self action:@selector(displayMore) forControlEvents:UIControlEventTouchUpInside];
//        [cell.contentView addSubview:button];
//        cell.accessoryType = UITableViewCellAccessoryNone;
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
        
        
        UITableViewCell *endCell = [tableView dequeueReusableCellWithIdentifier:@"EndCell"];
        if (nil == endCell) {
            endCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EndCell"];
        }
        for (UIView *view in endCell.contentView.subviews) {
            if ([view isKindOfClass:[KoronEndRefreshView class]]) {
                [view removeFromSuperview];
            }
        }
        [endCell.contentView addSubview:_endView];
        return endCell;
        
        
    }else{
        UITableViewCellStyle style = UITableViewCellStyleSubtitle; 
        static NSString *cellid= @"mid" ;
        MailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        if (!cell) 
        {
            cell = [[[ MailCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
            
            UIImageView *arrowView = [[UIImageView alloc] initWithFrame:CGRectMake(285, 27, 21, 21)];
            arrowView.tag = 103;
            [cell.contentView addSubview:arrowView];
            [arrowView release];
        }
        
        UIImageView *arrowView = (UIImageView *)[cell.contentView viewWithTag:103];
        arrowView.image = [UIImage imageNamed:@"blue_arrow"];
        
        NSDictionary *data=[ tableData objectAtIndex:indexPath.row ];
        //cell.textLabel.numberOfLines = 3;
        //cell.textLabel.lineBreakMode = UILineBreakModeWordWrap; 
        [cell.textLabel setFont : [UIFont boldSystemFontOfSize:14]];
        
        cell.textLabel.text=  [data objectForKey:@"subject"];
        cell.textLabel.textColor = [UIColor grayColor];
//        NSLog(@"cell.textLabel.text:%@",cell.textLabel.text);
//        cell.detailTextLabel.text= [data objectForKey:@"content"];
        [cell.detailTextLabel setFont:[UIFont fontWithName:@"arial" size:13]];
        cell.detailTextLabel.numberOfLines = 2;
        cell.detailTextLabel.textColor = [UIColor blackColor];
        
//        cell.receivedTime= [[[data objectForKey:@"receivedTime"]  dateFromISO8601]toShortString];
//        cell.receivedTime = @"2012-11-12 19:40";
//        NSLog(@"%@",[data objectForKey:@"receivedTime"]);
        cell.receivedTime = [self loadFomatterDate:[data objectForKey:@"receivedTime"]];
        
        if ([self.accounts isEqualToString:[data objectForKey:@"from"]] && [self.accounts isEqualToString:[data objectForKey:@"to"]]) {
            cell.from = [self deleteEndOfRange:[data objectForKey:@"from"]];
        }
        else if ([self.accounts isEqualToString:[data objectForKey:@"from"]]) {
            cell.from = [self deleteEndOfRange:[data objectForKey:@"from"]];
        } else {
            cell.from = [self deleteEndOfRange:[data objectForKey:@"from"]];
        }
        
        cell.mid = [data objectForKey:@"id"];
        cell.fid = self.folderid;
        cell.bid = self.boxid;
        cell.intranet = self.isintranet;
        cell.hasattach = [[data objectForKey:@"attach"] boolValue];
        
        cell.textLabel.backgroundColor  = [UIColor clearColor];
        cell.detailTextLabel.backgroundColor  = [UIColor clearColor];
        
        if (![[data objectForKey:@"unread"] boolValue]) {
            cell.imageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"unread.png" ]];
        }else{
            cell.imageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"read.png" ]  ];
        }
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleGray ;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableViewx didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableViewx deselectRowAtIndexPath:indexPath animated:YES];
     if (tableView.isEditing){
         [deleteDic setObject:indexPath forKey:[tableData objectAtIndex:indexPath.row]];
         return;
     }
    
    BOOL flag = [[[tableData objectAtIndex:indexPath.row] objectForKey:@"unread"] boolValue];
    if(!flag)
    {
        //CCLOG(@"xxxxxx:%i",flag);
        int c=  [self.tabBarItem.badgeValue intValue] -1;

        if(c>0)
        {
            self.tabBarItem.badgeValue=[NSString stringWithFormat:@"%i",c];  
        }else{
            self.tabBarItem.badgeValue=nil;  
        }
    }
    
     [[tableData objectAtIndex:indexPath.row]  setValue:[NSNumber numberWithBool:1] forKey:@"unread"] ;
     [tableViewx deselectRowAtIndexPath:indexPath animated:NO];
     [self setHidesBottomBarWhenPushed:NO ];
      MailCell *cell = (MailCell *)[self tableView:tableViewx cellForRowAtIndexPath:indexPath];
//      MailDetailsController *tdc=[[MailDetailsController alloc] initWithNibName:nil bundle:nil];
    MailDetailsController *tdc = [[MailDetailsController alloc]initWithMid:cell.mid Currentindex:indexPath.row Accounts:self.accounts AllMids:tableData Isintranet:self.isintranet];
    
    //获取邮件id拼接路径
    tdc.pathId = [tableData[indexPath.row] objectForKey:@"id"];
    if (isReply) {
        tdc.isReply = YES;
    }else {
        tdc.isReply = NO;
    }
    NSLog(@"accounts =  %@",self.accounts);
    NSLog(@"cell.mid = %@",cell.mid);
    NSLog(@"allMids  =  %@",tableData);
    NSLog(@"%c",isintranet);
//      [tdc setMid:cell.mid];
//      [tdc setCurrentindex:indexPath.row];
//      [tdc setAccounts:self.accounts];
//      [tdc setAllmids:tableData];
//      [tdc setIsintranet:self.isintranet];
      [self.navigationController pushViewController:tdc animated:YES];
      [tdc release];
} 

/*
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 76;
}*/
/*
- (CGFloat)tableView:(UITableView *)tableViewz heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row%20==0&&indexPath.row>0)
        return 76;
    return 76;
}
*/
- (void)tableView:(UITableView *)tableViewx didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
	if ([tableView isEditing]) {
		[deleteDic removeObjectForKey:[tableData objectAtIndex:indexPath.row]];
	}
}

//滑动删除*****
- (void) updateItemAtIndexPath: (NSIndexPath *) indexPath withString: (NSString *) string
{
	// You cannot insert a nil item. Passing nil is a delete request.
	if (!string) {
        [deleteDic setObject:indexPath forKey:[tableData objectAtIndex:indexPath.row]];
        [self performSelector:@selector(doDelete)];
	}
   [tableView reloadData];
//	[self setBarButtonItems];
}

- (void)tableView:(UITableView *)aTableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath 
{
	// delete item
	[self updateItemAtIndexPath:indexPath withString:nil];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (ismuildelete) {
         return UITableViewCellEditingStyleDelete|UITableViewCellEditingStyleInsert ;
    }else{
        return UITableViewCellEditingStyleDelete;
    }
}

//滑动删除结束*****

- (void) setBarButtonItems
{
    UIView *editingButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    editingButtonBackgroundView.clipsToBounds = YES;
    UIButton *editingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS > 6.9) {
        editingButton.frame = CGRectMake(0, 0, 60, 40);
    }else {
        editingButton.frame = CGRectMake(-5, 0, 60, 40);
    }
    editingButton.imageView.contentMode = UIViewContentModeScaleToFill;
    [editingButton setImage:[UIImage imageNamed:@"icon_compose_nor.png"] forState:UIControlStateNormal];
    [editingButton setImage:[UIImage imageNamed:@"icon_compose_sel.png"] forState:UIControlStateHighlighted];
    [editingButton addTarget:self action:@selector(newmail:) forControlEvents:UIControlEventTouchUpInside];
    [editingButtonBackgroundView addSubview:editingButton];
    
    UIBarButtonItem *editingItem = [[UIBarButtonItem alloc] initWithCustomView:editingButtonBackgroundView];
    self.navigationItem.rightBarButtonItem = editingItem;
    [editingItem release];
    
    
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];

    
}

- (void)backButtonPressed {
    [self.navigationController popViewControllerAnimated:YES];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:  [SMFileUtils fullBundlePath:@"tipsbg.png" ]   ]];
    
    _endView = [[KoronEndRefreshView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];
    
    tableView = [[UITableView alloc]  initWithFrame:CGRectMake(0,0, self.view.bounds.size.width ,kDeviceHeight-44-40  ) style: UITableViewStylePlain];
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    //tableView.contentInset
    [tableView setDelegate:self]; 
    [tableView setDataSource:self]; 
    // This should be set to work with the image height
    if (kDeviceHeight > 480) {
        [tableView setRowHeight:76];
    }else {
        [tableView setRowHeight:73];
    }
    
    tableView.showsHorizontalScrollIndicator= NO;
    tableView.showsVerticalScrollIndicator=YES;
    tableView.scrollEnabled=YES;
    tableView.indicatorStyle =UIScrollViewIndicatorStyleDefault ;
//    [tableView setBackgroundColor:MAILCELL_BACKGROUND];
    [tableView setBackgroundColor:[UIColor whiteColor]];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    [tableView setIndicatorStyle:UIScrollViewIndicatorStyleBlack];
    //tableView.contentInset=UIEdgeInsetsMake (0,0,0,0);
    //tableView.contentOffset = CGPointZero;
    //tableView.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin);
    
    //[tableView.layer setShadowOpacity:0.5];
    //[tableView.layer setShadowOffset:CGSizeMake(1, 1)];
    [self.view addSubview:tableView];
    
    if (_refreshHeaderView == nil) {
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - tableView.bounds.size.height, self.view.frame.size.width, tableView.bounds.size.height)];
		view.delegate = self;
		[tableView addSubview:view];
		_refreshHeaderView = view;
		[view release];
	}
	//  update the last update date
	[_refreshHeaderView refreshLastUpdatedDate];
    /*
    UIBarButtonItem *rightBarButton = [[ UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(doedit:)]; 
    self.navigationItem.rightBarButtonItem = rightBarButton;
    [rightBarButton release];
    */
    [self setBarButtonItems];
    
    
}

//进行删除动作 调用json删除
-(void)doDelete
{
   if ([[deleteDic allKeys] count]==0) {
//       [self setBarButtonItems];
       [tableView reloadData];
       return;
   }
    
   NSString *refid=@"";
   for ( NSDictionary *record in [deleteDic allKeys])
   {
       refid = [refid stringByAppendingFormat:@"%@,", [record objectForKey:@"id"] ];
       CCLOG(@"%@",refid);
   }
   
   [tableData removeObjectsInArray:[deleteDic allKeys]];
  // [tableView deleteRowsAtIndexPaths:[NSArray arrayWithArray:[deleteDic allValues]] withRowAnimation:UITableViewRowAnimationFade];
   [deleteDic removeAllObjects];
   
   if(![refid isEqualToString:@""]){
        JsonService *jservice=[JsonService sharedManager];
        [jservice setDelegate:self];
        [jservice UpdateFlag:@"mail" oid:refid flag:2  tipsid:nil];
        //CCLOG(@"%@",refid);
   }
    
//    [self setBarButtonItems];
    [tableView reloadData];
}

-(void)enterEditMode
{
    ismuildelete = YES;
	[tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
	[tableView setEditing:YES animated:YES];
    [tableView reloadData];
//	[self setBarButtonItems];
}

-(void)leaveEditMode
{
   
    if([ModalAlert confirm:NSLocalizedString(@"Confirm Delete",@"Confirm Delete")])
    {
        ismuildelete = NO;
        [tableView setEditing:NO animated:YES];
        [self doDelete];
    }else{
        ismuildelete = NO;
        [tableView setEditing:NO animated:YES];
//        [self setBarButtonItems];
    }
    //[tableView reloadData];
}

-(void)newmail:(id)sender
{
    NewMailController *controller=[[NewMailController alloc] initWithNibName:nil bundle:nil];
    controller.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [controller.view setBackgroundColor:[UIColor whiteColor]];
    [controller setIsintranet:self.isintranet];
    [controller setAccounts:self.accounts];
    BaseNavigationController * nav3=[[BaseNavigationController alloc] initWithRootViewController:controller];
    [self  presentModalViewController:nav3 animated:YES];
    [controller release];
    [nav3 release];
}

-(void)recmail:(id)sender
{
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    [jservice receiveMail:boxid ];
    
    UILabel *lblTotCaratteri = (UILabel *)[self.navigationController.toolbar viewWithTag:199];
    lblTotCaratteri.text =  [[NSDate date] stringWithFormat:@"'更新于 ' yyyy-MM-dd HH:mm"];
    
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    [conf setValue:lblTotCaratteri.text forKey:[NSString stringWithFormat:@"%@-%@",@"lastmailupdate",boxid]];
    [conf synchronize];
}

- (void)requestDataFinished:(id)jsondata//request
{
    /*
    tableData=nil;
    tableData=[jsondata retain];
    [super requestDataFinished:jsondata];
    //更新提示数字
    if(tableData&&[tableData count]>0){
        self.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i", [tableData count] ] ;
    }else{
        self.tabBarItem.badgeValue =@"";
    }*/
    
    if ([jsondata count] < 20) {
        isShowEndRefresh = NO;
    }else {
        isShowEndRefresh = YES;
    }
    
    
    if(tableData==nil){
        tableData=[jsondata retain];
        [super requestDataFinished:jsondata];
        
    }else{
        NSMutableArray *td= [NSMutableArray arrayWithArray: tableData];
        [td addObjectsFromArray:[jsondata retain]];
        tableData = [td retain];
        [super requestDataFinished:jsondata];
    }
    
    [_endView setTitle:@"加载更多.."];
    [_endView refreshStopAnimation];
    
    return;
}

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
	[super egoRefreshTableHeaderDidTriggerRefresh:view];
	//[self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:3.0];
    tableData=nil;
    [self requestJsonData:nil];
    //刷新前复位编辑状态
    [deleteDic removeAllObjects];
    ismuildelete = NO;
	[tableView setEditing:NO animated:YES];
//	[self setBarButtonItems];
}

-(void)viewWillAppear:(BOOL)animated
{
    if(tableData==nil)
    {
        [self requestJsonData:nil];
    }
    [tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated { 
    [[JsonService sharedManager] cancelAllRequest];
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

#pragma getDate Method
- (NSString *)loadFomatterDate:(NSString *)dateString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger result = [self computationTime:[formatter stringFromDate:date]];
    if (result == 0) {
        [formatter setDateFormat:@"今天"];
    }else if (result == 1) {
        [formatter setDateFormat:@"昨天"];
    }
    else if (result == 2) {
        [formatter setDateFormat:@"前天"];
    }
    else {
        [formatter setDateFormat:@"MM-dd HH:mm"];
    }
    
    
    return [formatter stringFromDate:date];
    
}

- (NSInteger)computationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowadayTimeStr = [fromatter stringFromDate:[NSDate date]];
    
    
    if ([[nowadayTimeStr substringToIndex:3] integerValue] - [[timeStr substringToIndex:3] integerValue] == 0) {
        if ([[nowadayTimeStr substringWithRange:NSMakeRange(5, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(5, 2)] integerValue] == 0) {
            if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 0) {
                return 0;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
                return 1;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 2) {
                return 2;
            }else {
                return 4;
            }
        }else {
            return 4;
        }
        
    }else {
        return 4;
    }
    
}

- (NSString *)deleteEndOfRange:(NSString *)string {
    NSMutableString *newString = [[NSMutableString alloc] initWithString:string];
    NSInteger length = string.length;
    
    if ([newString hasSuffix:@">"]) {
        [newString deleteCharactersInRange:NSMakeRange(length-1, 1)];
        
        while (--length) {
            if ([newString hasSuffix:@"<"] == NO) {
                [newString deleteCharactersInRange:NSMakeRange(length-1, 1)];
            }else {
                [newString deleteCharactersInRange:NSMakeRange(length-1, 1)];
                break;
            }
        }
    }
    
    return newString;
}

#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
	[_refreshHeaderView egoRefreshScrollViewDidScroll:tableView];
    
    CGPoint contentOffsetPoint = tableView.contentOffset;
    CGRect frame = tableView.frame;
    //判断是否滚动到底部
    if (contentOffsetPoint.y == tableView.contentSize.height - frame.size.height || tableView.contentSize.height < frame.size.height)
    {
        [self scrollToTheEnd];
    }
    
}

- (void)scrollToTheEnd {
    if (isShowEndRefresh == YES) {
        [self displayMore];
        [_endView refreshStartAnimation];
        [_endView setTitle:@"加载中.."];
    }
}


@end
