//
//  AttachListViewController.m
//  moffice
//
//  Created by Mac Mini on 13-10-29.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "AttachListViewController.h"
#import "AttachView.h"


@interface AttachListViewController ()

- (void)loadCustomBackButton;

- (NSString *)getAttachPicture:(NSString *)attachName;

- (void)onDismissButtonPressed;

- (BOOL)isFileExistsAtPath:(NSString *)attachName;

@end

@implementation AttachListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"附件";
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showTabbarNotification" object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadCustomBackButton];
    
    // 缓存文件夹
    NSFileManager * manger = [NSFileManager defaultManager];
    NSString *cachePath = [NSHomeDirectory() stringByAppendingString:@"/tmp/MyCache"];
    NSString *path = [NSString stringWithFormat:@"%@/%@",cachePath,self.pathId];
    if (![manger fileExistsAtPath:path isDirectory:nil]) {
        [manger createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
	
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-20-44-49)];
    [self.view addSubview:_scrollView];

    
    for (NSInteger index = 0; index < self.attachArray.count; index++) {
        AttachView *view = [[AttachView alloc] initWithFrame:CGRectMake(20, 10+index*70, 280, 60)];
        view.tag = index+100;
        view.delegate = self;
        view.attachImageView.image = [UIImage imageNamed:[self getAttachPicture:[self.attachArray[index] objectForKey:@"name"]]];
        view.attachNameLabel.text = [self.attachArray[index] objectForKey:@"name"];

        view.statusImageView.image = [UIImage imageNamed:@"btn_download"];
        if ([self isFileExistsAtPath:view.attachNameLabel.text]) {
            view.backgroundImageView.frame = view.bounds;
            view.statusImageView.image = [UIImage imageNamed:@"blue_arrow"];
            
        }
        
        [_scrollView addSubview:view];
        [view release];
    }
    
    _scrollView.contentSize = CGSizeMake(kDeviceWidth, 20+self.attachArray.count*70);
    
    
    
    _isDownload = YES;
    _indexOfAttachView = 0;
    
    NSLog(@"%@",NSHomeDirectory());
    }

#pragma mark - Private Method
- (BOOL)isFileExistsAtPath:(NSString *)attachName {
    
    NSString *myCacheDirectory = [NSHomeDirectory() stringByAppendingString:@"/tmp/MyCache"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@/%@",myCacheDirectory,self.pathId,attachName];
    if ([fileManager fileExistsAtPath:filePath]) {
//        NSLog(@"YES");
        return YES;
    }
//    NSLog(@"NO");
    return NO;
                          
}

- (void)onDismissButtonPressed {
    CATransition *myAnimation = [CATransition animation];
    myAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    myAnimation.duration = 0.4;
    
    myAnimation.type = kCATransitionReveal;
    myAnimation.subtype = kCATransitionFromLeft;
    
//    [[[[[UIApplication sharedApplication] delegate] window] layer] addAnimation:myAnimation forKey:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BaseNavigationControllerBackgroundColorChangeBlack" object:nil];
    
    
    
    [self.navigationController.view.layer addAnimation:myAnimation forKey:nil];
    
    [self.navigationController popViewControllerAnimated:NO];
    
}

- (NSString *)getAttachPicture:(NSString *)attachName {
    if ([attachName hasSuffix:@".zip"]) {
        return @"attach_file_icon_zip.png";
    }else if ([attachName hasSuffix:@".xls"]) {
        return @"attach_file_icon_xls.png";
    }else if ([attachName hasSuffix:@".wps"]) {
        return @"attach_file_icon_wps.png";
    }else if ([attachName hasSuffix:@".video"]) {
        return @"attach_file_icon_video.png";
    }else if ([attachName hasSuffix:@".txt"]) {
        return @"attach_file_icon_txt.png";
    }else if ([attachName hasSuffix:@".swf"]) {
        return @"attach_file_icon_swf.png";
    }else if ([attachName hasSuffix:@".psd"]) {
        return @"attach_file_icon_psd.png";
    }else if ([attachName hasSuffix:@".ppt"]) {
        return @"attach_file_icon_ppt.png";
    }else if ([attachName hasSuffix:@".pdf"]) {
        return @"attach_file_icon_pdf.png";
    }else if ([attachName hasSuffix:@".png"]) {
        return @"attach_file_icon_image.png";
    }else if ([attachName hasSuffix:@".html"]) {
        return @"attach_file_icon_html.png";
    }else if ([attachName hasSuffix:@".fla"]) {
        return @"attach_file_icon_fla.png";
    }else if ([attachName hasSuffix:@".doc"]||[attachName hasSuffix:@".docx"]) {
        return @"attach_file_icon_doc.png";
    }else if ([attachName hasSuffix:@".mp3"]) {
        return @"attach_file_icon_audio.png";
    }
    return @"attach_file_icon_default.png";
}

- (void)loadCustomBackButton {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(onDismissButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}


#pragma mark - Getter Method
- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor blackColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}
#pragma mark - AttachView Delegate
- (void)didSelectedAttachView:(UIView *)attachView withIndex:(NSInteger)index {
    AttachView *view = (AttachView *)attachView;
    if (view.backgroundImageView.width == 280) {
        NSString * path = [NSHomeDirectory() stringByAppendingString:@"/tmp/MyCache"];
    
        NSString *filePath = [NSString stringWithFormat:@"%@/%@/%@",path,self.pathId,[self.attachArray[index-100] objectForKey:@"name"]];
        NSLog(@"%@",filePath);
        NSURL *URL = [NSURL fileURLWithPath:filePath];
        
//        NSURL *URL = [[NSBundle mainBundle] URLForResource:@"Blocks" withExtension:@"pdf"];
        if (URL) {
            
            self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
            self.documentInteractionController.delegate = self;
            BOOL isPreview = [self.documentInteractionController presentPreviewAnimated:YES];
            if (isPreview == NO) {
                [self.documentInteractionController presentOpenInMenuFromRect:attachView.frame inView:self.view animated:YES];
            }
        }
    }else if (_isDownload == YES) {
        _indexOfAttachView = index;
        [self click:index];
    }
}

- (NSString *) getFilePath
{
    if (_fileName.length == 0) {
        return nil;
    }
    return [NSHomeDirectory() stringByAppendingFormat:@"/tmp/MyCache/%@/%@",self.pathId,_fileName];
}


- (void)click:(NSInteger)index
{
        
    NSString *url = [[self.attachArray[index-100] objectForKey:@"path"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    NSLog(@"%@",[self.attachArray[_indexOfAttachView-100] objectForKey:@"path"]);
    //        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:PNGURL]];
    // 如果不是第一次 那么先读取本地文件大小 写Range字段
    if (_fileName.length != 0) {
        _fileHandle = [[NSFileHandle fileHandleForWritingAtPath:[self getFilePath]] retain];
        // 获取当前文件的下载的字节数
        _startDownLoadSize = [_fileHandle seekToEndOfFile];
        // 向请求头写Range字段
        [request addValue:[NSString stringWithFormat:@"bytes=%qu-",_downLoadSize] forHTTPHeaderField:@"RANGE"];
    }
    
    _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];

    
    
}

#pragma mark - UIDocumentInteractionController Delegate
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {

    [[NSNotificationCenter defaultCenter] postNotificationName:@"hiddenTabbarNotification" object:nil];
    
    return self;
}

#pragma mark - NSURLConnectionDataDelegate
- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [_connection cancel];
    [_connection release];
    
    [_fileHandle closeFile];
    [_fileHandle release];
}

- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
//    _isDownload == NO;
    _downLoadSize = 0;

        NSHTTPURLResponse * res = (NSHTTPURLResponse *)response;
        _fileName = [[self.attachArray[_indexOfAttachView-100] objectForKey:@"name"] retain];
        _totalFileSize = [res expectedContentLength];
        // 创建下载的缓存文件
        NSFileManager * manger = [NSFileManager defaultManager];
        if (![manger fileExistsAtPath:[self getFilePath]]) {
            [manger createFileAtPath:[self getFilePath] contents:nil attributes:nil];
        }
        
        _fileHandle = [[NSFileHandle fileHandleForWritingAtPath:[self getFilePath]] retain];

    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
}

- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // 写缓存文件
    [_fileHandle seekToEndOfFile];
    [_fileHandle writeData:data];
    _downLoadSize += data.length;
    CGFloat f = _downLoadSize / (CGFloat)_totalFileSize;
    AttachView *attachView = (AttachView *)[_scrollView viewWithTag:_indexOfAttachView];
    attachView.statusImageView.hidden = YES;
    [attachView.actView startAnimating];
    attachView.backgroundImageView.frame = CGRectMake(0, 0, 280*f, 60);
    NSLog(@"%llu %llu %f",_downLoadSize,_totalFileSize,f);
}

- (void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    AttachView *attachView = (AttachView *)[_scrollView viewWithTag:_indexOfAttachView];
    attachView.statusImageView.hidden = NO;
    attachView.statusImageView.image = [UIImage imageNamed:@"blue_arrow"];
    [attachView.actView stopAnimating];
    _isDownload = YES;
    [_connection cancel];
    [_connection release];
    
    [_fileHandle closeFile];
    [_fileHandle release];
}


#pragma mark - Memory
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_fileName release];
    [_scrollView release];

    
    [super dealloc];
}


@end
