//
//  merImageViewController.h
//  moffice
//
//  Created by koron on 13-5-29.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface merImageViewController : UIViewController

@property(nonatomic,retain) UIImage *bigImage;
@property(nonatomic,retain) UIImageView *bigImageview;
@end
