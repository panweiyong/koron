//
//  TipsDetailsViewController.h
//  moffice
//
//  Created by yangxi zou on 12-1-10.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "JsonService.h"
#import "AbstractViewController.h"
#import "ImageViewController.h"
#import "CommonConst.h"
#import "DTAttributedTextContentView.h"
@interface TipsDetailsViewController : AbstractViewController<UITableViewDelegate,UITableViewDataSource>
{
    //id tableData;
    NSString *refid;
    NSString *type;
    NSMutableDictionary *contentViewCache;
}

@property (nonatomic,retain)NSString *refid;
@property (nonatomic,retain)NSString *type;

@end
