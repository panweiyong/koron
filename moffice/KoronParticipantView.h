//
//  KoronParticipantView.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-28.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KoronParticipantView : UIView

@property (nonatomic, copy) NSString *participantID;

@end
