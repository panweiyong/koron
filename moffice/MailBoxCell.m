//
//  MailBoxCell.m
//  moffice
//
//  Created by yangxi zou on 12-1-15.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "MailBoxCell.h"

@implementation MailBoxCell

@synthesize accoutid,folderid,isintranet; 

-(void)dealloc
{
    [accoutid release], accoutid = nil;
    [folderid release], folderid = nil;
    self.contentLabel = nil;
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
