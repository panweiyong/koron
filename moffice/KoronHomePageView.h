//
//  KoronHomePageView.h
//  moffice
//
//  Created by Mac Mini on 13-10-8.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronHomePageOptionView.h"

@interface KoronHomePageView : UIView <UIScrollViewDelegate> {
    UIImageView *_backgroundImageView;
    
    UIScrollView *_scrollView;
    
    NSMutableArray *_homePageArray;
    
    NSMutableArray *_homePageImageArray;
    
    NSMutableArray *_isOptionSelectedArray;
    
}

@end
