//
//  bbsTabViewController.h
//  moffice
//
//  Created by lijinhua on 13-6-6.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JsonService.h"
#import "bbsCell.h"
#import "AbstractViewController.h"
@interface bbsTabViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate,UITextViewDelegate>
{
    NSString *refid;
    NSString *tipsid;
    UIScrollView *bbsScrollview;
    
    NSString *topicName;
    NSString *userName;
    NSString *creatTime;
    NSString *webContent;
    
    //评论需要的数据
    NSString *topicId;
    NSString *replayContent;
    
    NSArray *commentsArr;
    UITableView *replayTabview;
    
    NSMutableArray *dataArr;
    UIActivityIndicatorView *activity;
    
    NSString *replayNum;
    NSString *replayResult;
    float bbsWebHeight;
    
    //tableView 头部控件
    UILabel *titleLabel;
    UIImageView *logoView;
    UILabel *userLabel;
    UILabel *timeLabel;
    UILabel *comLabel;
    UIImageView *triImage;
    
    UIWebView *bbsWeb;
    UIView *lineView;
    //tableView 尾部控件
    UILabel *replayLabel;
    UIImageView *ftriImage;
    UITextView *replayTextview;
    UIButton *replayBtn;
}
@property(nonatomic,retain) NSString *refid;
@property(nonatomic,retain) NSString *tipsid;
@property(nonatomic,retain) UIScrollView *bbsScrollview;
@property(nonatomic,retain) NSArray *commentsArr;
@property(nonatomic,retain) UITableView *replayTabview;
@property(nonatomic,retain) NSMutableArray *dataArr;
@property(nonatomic,retain) UIActivityIndicatorView *activity;
@property(nonatomic,retain) NSString *topicId;
@property(nonatomic,retain) NSString *replayContent;
@property(nonatomic,retain) NSString *replayNum;
@property(nonatomic,retain) NSString *replayResult;
//头部控件
@property(nonatomic,retain) UILabel *titleLabel;
@property(nonatomic,retain) UIImageView *logoView;
@property(nonatomic,retain) UILabel *userLabel;
@property(nonatomic,retain) UILabel *timeLabel;
@property(nonatomic,retain) UILabel *comLabel;
@property(nonatomic,retain) UIImageView *triImage;
@property(nonatomic,retain) UIWebView *bbsWeb;
@property(nonatomic,retain) UIView *lineView;
//尾部控件
@property(nonatomic,retain) UILabel *replayLabel;
@property(nonatomic,retain) UIImageView *ftriImage;
@property(nonatomic,retain) UITextView *replayTextview;
@property(nonatomic,retain) UIButton *replayBtn;
@end
