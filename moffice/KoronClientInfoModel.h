//
//  KoronClientInfoModel.h
//  moffice
//
//  Created by Mac Mini on 13-11-12.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "BaseModel.h"

@interface KoronClientInfoModel : BaseModel

@property (nonatomic, retain) NSString *infoKey;
@property (nonatomic, retain) NSString *infoVal;
@property (nonatomic, retain) NSString *infoGroup;

@end
