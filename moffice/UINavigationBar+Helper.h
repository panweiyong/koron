//
//  UINavigationBar+Helper.h
//  origami3d
//
//  Created by yangxi zou on 11-3-7.
//  Copyright 2011 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
 
@interface UINavigationBar  (TENavigationBar) 
- (void) drawRect:(CGRect)rect;
@end
 
@interface UIToolbar (TENavigationBar) 
- (void) drawRect:(CGRect)rect;
- (void) setupIos5PlusNavBarImage;
@end

