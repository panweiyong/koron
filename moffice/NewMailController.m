//
//  NewMailController.m
//  moffice
//
//  Created by yangxi zou on 12-1-18.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "NewMailController.h"


@interface NewMailController (Private)
- (void)resizeViews;
@end

@implementation NewMailController


@synthesize from,to,subject,content,isintranet,accounts,accountarray;


-(void)dealloc 
{
    [accounts release];
    [accountarray release];
    [from release];
    [to release];
    [subject release];
    [content release];
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title=NSLocalizedString(@"New Mail",@"New Mail") ; 
    }
    return self;
}
 
-(void)viewWillDisappear:(BOOL)animated
{
   [[JsonService sharedManager] cancelAllRequest];
    [[NSNotificationCenter defaultCenter]  removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    // CCLOG(@"acc:%@",self.accounts);
    NSRange range = [self.accounts rangeOfString:@","];
    //int location = range.location;
    int leight = range.length;
    //CCLOG(@"%i %i",location,leight);
    if(leight>0)
    {
            self.accountarray = [ self.accounts componentsSeparatedByString:@","];
        }else{
             self.accountarray = [[NSArray alloc] initWithObjects:self.accounts, nil];
    }
    [fromView setTitle:[self.accountarray objectAtIndex:0] forState:UIControlStateNormal];
   
    
    if (self.to==nil){
        [tokenFieldView becomeFirstResponder]; 
    }else {
        [tokenFieldView.tokenField addToken:self.to];
    }
    
    
    
    if (self.subject!=nil){
        
        
       // CCLOG(ocontentx);
        
      //  NSBundle *bundle = [NSBundle mainBundle];
      // NSURL *indexFileURL = [bundle URLForResource:@"index" withExtension:@"html"];
      //  [bodyView loadRequest:[NSURLRequest requestWithURL:indexFileURL]];
        
        [subjectView setText:self.subject];
    }
    
    NSString *htmlPath = [[NSBundle mainBundle] pathForResource:  @"index" ofType:@"html"];  
    NSString *htmlString = [NSString stringWithContentsOfFile: htmlPath   encoding:NSUTF8StringEncoding error:NULL]; 
    //[tokenFieldView resignFirstResponder];
    if(self.content==nil)self.content=@"";
    NSString *ocontent = [NSString stringWithFormat:@"<br>%@<br>%@",NSLocalizedString(@"send from my iphone.",@"send from my iphone."),self.content];
    //[bodyView setContentToHTMLString:[NSString stringWithFormat:@"<br>%@<br>%@",NSLocalizedString(@"send from my iphone.",@"send from my iphone."),self.content]];//@"<html><head></head><body><img src = \"http://portrait5.sinaimg.cn/1712526484/blog/180\">这是文字<br/><br/></body></html>"];//self.content];
    NSString *ocontentx = [htmlString stringByReplacingOccurrencesOfString:@"MYHTMLCONTENT" withString:ocontent];
    [bodyView loadHTMLString:ocontentx baseURL:nil];
    
   // CCLOG(@"%@",ocontentx);
    
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    if(self.isintranet){ 
         if([SMFileUtils needDownload:@"mailaddress1"]) [jservice listIntranetMailUsers];
    }else{
        //if([SMFileUtils needDownload:@"mailaddress2"]) [jservice listInternetMailUsers];
    }  
    issending = NO;
    
     if (self.to!=nil){
         // [tokenFieldView resignFirstResponder]; 
     }
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView { 
    
    //[fromView resignFirstResponder];
    
    if (self.to!=nil){
        [webView becomeFirstResponder];
        //CCLOG(@"webViewDidFinishLoad");
        //  [bodyView becomeFirstResponder];
        [webView stringByEvaluatingJavaScriptFromString:@"gfocusme()"];
        //[webView stringByEvaluatingJavaScriptFromString:@"document.getElementById('content').click();"];
        
        // NSString *string = @"document.body.contentEditable=true;document.designMode='on';void(0)";
        // [webView stringByEvaluatingJavaScriptFromString:string];
        //bodyView.selectedRange = NSMakeRange(0,0);
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    //CCLOG(@"%@",self.to);
    
    //bodyView.text= self.content; 
    
    
    //CCLOG(@"intranet:%i",self.isintranet);
     
   
    //messageView.selectedRange = NSMakeRange(0, 0); 
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    //取消按钮
    [self loadCustomBackButton];
    
    //显示在线用户按钮
//    UIBarButtonItem *oButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Send",@"Send") style:UIBarButtonItemStylePlain target:self action:@selector(sendmail:)];          
//    self.navigationItem.rightBarButtonItem = oButton; 
//    [oButton release];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    UIImage *image = [[UIImage imageNamed:@"btn_big_gray"] stretchableImageWithLeftCapWidth:5 topCapHeight:10];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTitle:NSLocalizedString(@"Send",@"Send") forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:16]];
    [button addTarget:self action:@selector(sendmail:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [button release];
    self.navigationItem.rightBarButtonItem = rightItem;
    [rightItem release];
    
    // both are needed, otherwise hyperlink won't accept mousedown
    
    tokenFieldView = [[TITokenFieldView alloc] initWithFrame:self.view.bounds];
	[tokenFieldView setDelegate:self];
    [tokenFieldView setSourceArray:[[Names listMailOfNamesIntranet]retain]];
	[tokenFieldView.tokenField setAddButtonAction:@selector(showContactsPicker) target:self];
	[tokenFieldView.tokenField setPromptText:NSLocalizedString(@"MailTo:",@"MailTo:")];
    
    messageView = [[UIView alloc] initWithFrame:tokenFieldView.contentView.bounds];
    
    //发件人
    UILabel * flabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, 50 , 30)];
	//[label setTag:123];
	[flabel setText:NSLocalizedString(@"from:",@"from:")];
	[flabel setFont:[UIFont systemFontOfSize:15]];
	[flabel setTextColor:[UIColor colorWithWhite:0.5 alpha:1]];
	[flabel sizeToFit]; 
	[messageView addSubview:flabel];
	[flabel release];
    
    fromView = [UIButton buttonWithType:UIButtonTypeCustom] ;//[[ alloc] initWithFrame:CGRectMake(65,8, 320, 30)];
     
    [fromView setFrame:CGRectMake(65,5, 200, 30)];
    //[subjectView setScrollEnabled:NO];
    //[fromView setAutoresizingMask:UIViewAutoresizingFlexibleWidth];
    //[subjectView setDelegate:self];
    [fromView setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [fromView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    //[fromView setTitle: [ objectAtIndex:0] forState: UIControlStateNormal];
    [fromView addTarget:self action:@selector(selectaccount) forControlEvents:UIControlEventTouchUpInside];
    [fromView.titleLabel setFont:[UIFont systemFontOfSize:15] ];
    [messageView   addSubview:fromView];
    //[fromView release];
    
    
    UIView *separator2 = [[UIView alloc] initWithFrame:CGRectMake(0, 38, tokenFieldView.contentView.bounds.size.width, 1)];
    [separator2 setBackgroundColor:[UIColor colorWithWhite:0.7 alpha:1]];
    [messageView addSubview:separator2];
    [separator2 release];
    
    //主题
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(8, 46, 50 , 30)];
	//[label setTag:123];
	[label setText:NSLocalizedString(@"subject:",@"subject:")];
	[label setFont:[UIFont systemFontOfSize:15]];
	[label setTextColor:[UIColor colorWithWhite:0.5 alpha:1]];
	[label sizeToFit];
	[messageView addSubview:label];
	[label release];
    subjectView = [[UITextField alloc] initWithFrame:CGRectMake(65,46, 260, 30)];
    //[subjectView setScrollEnabled:NO];
    [subjectView setAutoresizingMask:UIViewAutoresizingNone];
    //[subjectView setDelegate:self];
    [subjectView setFont:[UIFont systemFontOfSize:15]];
    [messageView   addSubview:subjectView];
    [subjectView release];
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, 75, tokenFieldView.contentView.bounds.size.width, 1)];
    [separator setBackgroundColor:[UIColor colorWithWhite:0.7 alpha:1]];
    [messageView addSubview:separator];
    [separator release];
    
     bodyView = [[UIWebView alloc] initWithFrame:CGRectMake(5,76, 320, 320)];
    // [bodyView setScrollEnabled:NO];
     //[bodyView setAutoresizingMask:UIViewAutoresizingFlexibleHeight];
      [bodyView setDelegate:self];
  //   [bodyView setFont:[UIFont systemFontOfSize:15]];
   //  [bodyView setText:NSLocalizedString(@"send from my iphone.",@"send from my iphone.")];
     [messageView addSubview:bodyView];
    
     [bodyView release];
     
    [tokenFieldView.contentView addSubview:messageView];
    [messageView release];
    
    /*
     messageView = [[UITextView alloc] initWithFrame:tokenFieldView.contentView.bounds];
     [messageView setScrollEnabled:NO];
     [messageView setAutoresizingMask:UIViewAutoresizingNone];
     [messageView setDelegate:self];
     [messageView setFont:[UIFont systemFontOfSize:15]];
     [messageView setText:NSLocalizedString(@"send from my iphone.",@"send from my iphone.")];
     [tokenFieldView.contentView addSubview:messageView];
     [messageView release];
     */
    
	[self.view addSubview:tokenFieldView];
  //  [tokenFieldView release];
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        
    //[messageView becomeFirstResponder];
	// You can call this on either the view on the field.
	// They both do the same thing.
	
     
}


- (void)removeBar {
    // Locate non-UIWindow.
  //  CCLOG(@"removeBar");
    UIWindow *keyboardWindow = nil;
    for (UIWindow *testWindow in [[UIApplication sharedApplication] windows]) {
        if (![[testWindow class] isEqual:[UIWindow class]]) {
            keyboardWindow = testWindow;
            break;
        }
    }
  //  CCLOG(@"removeBar2");
    // Locate UIWebFormView.
    for (UIView *possibleFormView in [keyboardWindow subviews]) {       
        // iOS 5 sticks the UIWebFormView inside a UIPeripheralHostView.
        if ([[possibleFormView description] rangeOfString:@"UIPeripheralHostView"].location != NSNotFound) {
            for (UIView *subviewWhichIsPossibleFormView in [possibleFormView subviews]) {
                if ([[subviewWhichIsPossibleFormView description] rangeOfString:@"UIWebFormAccessory"].location != NSNotFound) {
                    [subviewWhichIsPossibleFormView removeFromSuperview];
                }
            }
        }
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
	return  [self.accountarray   count];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
	return 40.0f;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero] ;
    label.text= [self.accountarray objectAtIndex:row];
    label.backgroundColor =[UIColor clearColor];
    [label sizeToFit];
	return label;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	UIPickerView *pickerView = (UIPickerView *)[actionSheet viewWithTag:101];
	[fromView setTitle:[self.accountarray objectAtIndex:([pickerView selectedRowInComponent:0])] forState:UIControlStateNormal]; 
	[actionSheet release];
}

-(void)selectaccount
{
    NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n\n\n\n\n\n\n\n" : @"\n\n\n\n\n\n\n\n\n\n\n\n" ;
    
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK",@"OK"), nil];
	[actionSheet showInView:self.view];
    
	UIPickerView *pickerView = [[[UIPickerView alloc] init] autorelease];
	pickerView.tag = 101;
	pickerView.delegate = self;
	pickerView.dataSource = self;
	pickerView.showsSelectionIndicator = YES;
    
	[actionSheet addSubview:pickerView];
}

- (void)tokenFieldTextDidChange:(TITokenField *)tokenField
{
    if ([tokenField.text isEqualToString:@""] || tokenField.text.length == 0){
		return;
	}
    //CCLOG(@"%@",tokenField.text);
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    if(self.isintranet){ 
        //if([SMFileUtils needDownload:@"mailaddress1"]) [jservice listIntranetMailUsers];
    }else{
        if([SMFileUtils needDownload:@"mailaddress2"]) [jservice listInternetMailUsers:tokenField.text];
    }
    
}

- (void)requestDataFinished:(id)jsondata;//request
{
    if(self.isintranet){
        [SMFileUtils writePlistToCache:jsondata filename:@"mailaddress1"]  ;
        [tokenFieldView setSourceArray:[Names listMailOfNamesIntranet]];
    }else{
       // [SMFileUtils writePlistToCache:jsondata filename:@"mailaddress2"]  ;
        CFShow(jsondata);
        [tokenFieldView setSourceArray:jsondata];
        [tokenFieldView.resultsTable reloadData];
    }
}

- (void)postFinished:(id)data
{
    issending = NO;
    if([[data objectForKey:@"code"] intValue]==1){
        [ModalAlert prompt:NSLocalizedString(@"send Success",@"send Success")];
        [self dismissModalViewControllerAnimated:YES];
    }else{
        NSLog(@"desc ==  %@",[data objectForKey:@"desc"]);
        [ModalAlert prompt:[data objectForKey:@"desc"]];
        return;
    }
}


-(void)sendmail:(id)sender
{
    if(issending==YES)return;
    
    [subjectView becomeFirstResponder];
    if( [tokenFieldView.tokenTitles count]==0){
        [ModalAlert prompt:NSLocalizedString(@"Please Select To",@"Please Select To")];
        return;
    }
    
    if([[subjectView.text trim] isEqualToString:@""]){
        [ModalAlert prompt:NSLocalizedString(@"Please Input Subject",@"Please Input Subject")];
        return;
    }
    
    NSString *tostr=@"";
    for(int i=0;i<[tokenFieldView.tokenTitles count];i++)
    {
       tostr = [tostr stringByAppendingString:  [NSString stringWithFormat:@"%@," ,[tokenFieldView.tokenTitles objectAtIndex:i] ]   ] ;
    }
    if([tostr hasPrefix:@","] )tostr = [tostr substringToIndex: [tostr length]-1 ];
   
    
    
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    NSString *source = [bodyView stringByEvaluatingJavaScriptFromString:
                        
                        @"document.getElementsByTagName('html')[0].outerHTML"];     //[jservice sendMail:tostr from:[fromView titleForState:UIControlStateNormal] subject:subjectView.text content:bodyView.text];
    //CCLOG(source);
    [jservice sendMail:tostr from:[fromView titleForState:UIControlStateNormal] subject:subjectView.text content:source];
    issending = YES;
    //CCLOG(@"%@",tokenFieldView.tokenField.text);
    //NSLog(@"%@", [tokenFieldView.tokenTitles objectAtIndex:0]);
}
-(void)cancelMe:(id)sender
{
    [self dismissModalViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	return (toInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[UIView animateWithDuration:duration animations:^{[self resizeViews];}]; // Make it pretty.
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self resizeViews];
}

- (void)showContactsPicker {
	
	// Show some kind of contacts picker in here.
	// For now, it's a good chance to show how to add tokens.
	//[tokenFieldView.tokenField addToken:@"New Name"];
	
	// You can access token titles with 'tokenFieldView.tokenTitles'.
	//  NSLog(@"%@", tokenFieldView.tokenTitles);
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    if(self.isintranet){ 
         [jservice listIntranetMailUsers];
    }else{
        //if([SMFileUtils needDownload:@"mailaddress2"]) [jservice listInternetMailUsers];
    } 
}

- (void)keyboardWillShow:(NSNotification *)notification {
	// 
    [self performSelector:@selector(removeBar) withObject:nil afterDelay:0];
    CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	// Wouldn't it be fantastic if, when in landscape mode, width was actually width and not height?
	keyboardHeight = keyboardRect.size.height > keyboardRect.size.width ? keyboardRect.size.width : keyboardRect.size.height;
	[self resizeViews];
   
   
}

- (void)keyboardWillHide:(NSNotification *)notification {
	keyboardHeight = 0;
	[self resizeViews];
}

- (void)resizeViews {
	return;
	CGRect newFrame = tokenFieldView.frame;
	newFrame.size.width = self.view.bounds.size.width;
	newFrame.size.height = self.view.bounds.size.height - keyboardHeight;
	[tokenFieldView setFrame:newFrame];
    CGRect fra=tokenFieldView.contentView.bounds;
    fra.size.height=bodyView.frame.size.height+subjectView.frame.size.height+fromView.frame.size.height+20;//fra.size.height;//-subjectView.frame.size.height-fromView.frame.size.height;
    //  fra.size.height=300;//bodyView.contentSize.height+subjectView.frame.size.height+fromView.frame.size.height+20;
	[messageView setFrame:fra];
    //[messageView setBackgroundColor:[UIColor redColor]];
    // [self textViewDidChange:bodyView];
}

- (void)tokenField:(TITokenField *)tokenField didChangeToFrame:(CGRect)frame {
	//[self textViewDidChange:bodyView];
}

- (void)textViewDidChange:(UITextView *)textView {
	
    
	CGFloat fontHeight = (textView.font.ascender - textView.font.descender) + 1;
	CGFloat originHeight = tokenFieldView.frame.size.height - 30*2+35;
	CGFloat newHeight = textView.contentSize.height + fontHeight + 60;
	
	CGRect newTextFrame = textView.frame;
	newTextFrame.size = textView.contentSize;
	newTextFrame.size.height = newHeight;
	
	CGRect newFrame = tokenFieldView.contentView.frame;
	newFrame.size.height = newHeight;
	
	if (newHeight < originHeight){
		newTextFrame.size.height = originHeight;
		newFrame.size.height = originHeight;
	}
    
	[tokenFieldView.contentView setFrame:newFrame];
	[textView setFrame:newTextFrame];
	[tokenFieldView updateContentSize];
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:20.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)loadCustomBackButton {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(cancelMe:) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
    
   
}

@end
