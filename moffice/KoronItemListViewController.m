//
//  KoronItemListViewController.m
//  TodoListDemo
//
//  Created by Mac Mini on 13-11-22.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronItemListViewController.h"

@interface KoronItemListViewController ()

@end

@implementation KoronItemListViewController
{
    UIScrollView *_scrollBackgroundView;
    UITableView *_toDoTableView;
    UITableView *_didFinishTableView;
    NSMutableArray *_items;
    NSMutableArray *_todoItems;
    NSMutableArray *_didFinishItems;
    ASIFormDataRequest *_itemListRequest;
    ASIFormDataRequest *_checkItemListRequest;
    ASIFormDataRequest *_deleteItemListRequest;
    BOOL _isLeftTableView;
    NSString *_status;
    KoronChecklistItem *_checkedItem;
    
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _isLeftTableView = YES;
        self.status = @"0";
        [self loadChecklistItems];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    self.title = @"待办";
    
    [self loadNavigationItem];
    
    NSLog(@"document: %@",[self documentsDirectory]);
    
    NSLog(@"dataFile: %@",[self dataFilePath]);
    
    _scrollBackgroundView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-64)];
    _scrollBackgroundView.contentSize = CGSizeMake(kDeviceWidth * 2, kDeviceHeight-64);
    _scrollBackgroundView.pagingEnabled = YES;
    _scrollBackgroundView.backgroundColor = [UIColor clearColor];
    _scrollBackgroundView.bounces = NO;
    _scrollBackgroundView.delegate = self;
    
    [self.view addSubview:_scrollBackgroundView];
    
    _toDoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, _scrollBackgroundView.height) style:UITableViewStylePlain];
    _toDoTableView.rowHeight = 70;
    _toDoTableView.delegate = self;
    _toDoTableView.dataSource = self;
    [_scrollBackgroundView addSubview:_toDoTableView];
    
    if ([_toDoTableView respondsToSelector:@selector(separatorInset)]) {
        [_toDoTableView setSeparatorInset:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    
    if (_toDoRefreshTableView == nil) {
        //初始化下拉刷新控件
        EGORefreshTableHeaderView *refreshView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _toDoTableView.bounds.size.height, self.view.frame.size.width, _toDoTableView.bounds.size.height)];
        refreshView.delegate = self;
        //将下拉刷新控件作为子控件添加到UITableView中
        [_toDoTableView addSubview:refreshView];
        _toDoRefreshTableView = refreshView;
    }
    
    
    _didFinishTableView = [[UITableView alloc] initWithFrame:CGRectMake(kDeviceWidth, 0, kDeviceWidth, _scrollBackgroundView.height) style:UITableViewStylePlain];
    _didFinishTableView.rowHeight = 70;
    _didFinishTableView.delegate = self;
    _didFinishTableView.dataSource = self;
    [_scrollBackgroundView addSubview:_didFinishTableView];
    
    if (_didFinishRefreshTableView == nil) {
        //初始化下拉刷新控件
        EGORefreshTableHeaderView *refreshView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - _didFinishTableView.bounds.size.height, self.view.frame.size.width, _didFinishTableView.bounds.size.height)];
        refreshView.delegate = self;
        //将下拉刷新控件作为子控件添加到UITableView中
        [_didFinishTableView addSubview:refreshView];
        _didFinishRefreshTableView = refreshView;
    }
    
    if ([_didFinishTableView respondsToSelector:@selector(separatorInset)]) {
        [_didFinishTableView setSeparatorInset:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    
}

#pragma mark - ASIRequest
- (void)sendItemListRequest
{
    if (_itemListRequest) {
        [_itemListRequest clearDelegatesAndCancel];
        _itemListRequest = nil;
    }
    
    _itemListRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[[KoronManager sharedManager] getObjectForKey:SERVERURL]]];
    _itemListRequest.delegate = self;
    [_itemListRequest setDidFinishSelector:@selector(itemListRequestFinish:)];
    [_itemListRequest setDidFailSelector:@selector(itemListRequestFail:)];
    
    [_itemListRequest setPostValue:@"listMyTodo" forKey:@"op"];
    [_itemListRequest setPostValue:_status forKey:@"status"];
    [_itemListRequest setPostValue:[NSString stringWithFormat:@"%d",_todoItems.count] forKey:@"count"];
    [_itemListRequest setPostValue:[[KoronManager sharedManager] getObjectForKey:SID] forKey:SID];
    
    [_itemListRequest startAsynchronous];
}

- (void)itemListRequestFinish:(ASIFormDataRequest *)response
{
    NSLog(@"module: %@",[response responseString]);
    
    NSArray *listArray = [[NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil] objectForKey:@"list"];

    NSLog(@"item list: %@",listArray);
    
    if (_isLeftTableView) {
        
        for (NSDictionary *dic in listArray) {
            KoronChecklistItem *item = [[KoronChecklistItem alloc] initWithContent:dic];
            item.checked = NO;
            [_items addObject:item];
            [_todoItems addObject:item];
            [item release];
        }
        
        [_toDoTableView reloadData];
    } else {
        for (NSDictionary *dic in listArray) {
            KoronChecklistItem *item = [[KoronChecklistItem alloc] initWithContent:dic];
            item.checked = YES;
            [_items addObject:item];
            [_didFinishItems addObject:item];
            [item release];
        }
        [_didFinishTableView reloadData];
    }
    
    [self saveCheckListItems];
    
    [self doneLoadingTableViewData];

}

- (void)itemListRequestFail:(ASIFormDataRequest *)response
{
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
    
}

- (void)sendCheckedOrUncheckedItemListRequest
{
    if (_checkItemListRequest) {
        [_checkItemListRequest clearDelegatesAndCancel];
        _checkItemListRequest = nil;
    }
    
    _checkItemListRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[[KoronManager sharedManager] getObjectForKey:SERVERURL]]];
    _checkItemListRequest.delegate = self;
    [_checkItemListRequest setDidFinishSelector:@selector(checkedOrUncheckedItemListRequestFinish:)];
    [_checkItemListRequest setDidFailSelector:@selector(checkedOrUncheckedItemListRequestFail:)];
    
    [_checkItemListRequest setPostValue:@"addUpdateMyTodo" forKey:@"op"];
    
    if (_isLeftTableView) {
        self.status = @"1";
    } else {
        self.status = @"0";
    }
    
    [_checkItemListRequest setPostValue:_status forKey:@"status"];
    [_checkItemListRequest setPostValue:self.checkedItem.itemId forKey:@"id"];
    [_checkItemListRequest setPostValue:self.checkedItem.content forKey:@"title"];
    [_checkItemListRequest setPostValue:self.checkedItem.time forKey:@"time"];
    [_checkItemListRequest setPostValue:[[KoronManager sharedManager] getObjectForKey:SID] forKey:SID];
    
    [_checkItemListRequest startAsynchronous];
}

- (void)checkedOrUncheckedItemListRequestFinish:(ASIFormDataRequest *)response
{
    NSLog(@"checkOrunchecked : %@",[response responseString]);
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"%@",dic);
    if ([[dic objectForKey:@"code"] integerValue] >= 0 && [[dic objectForKey:@"desc"] isEqualToString:@"操作成功"] && _isLeftTableView) {
        
        [_didFinishItems addObject:self.checkedItem];
        
        NSInteger index = [_todoItems indexOfObject:self.checkedItem];
        
        [_todoItems removeObjectAtIndex:index];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        KoronToDoListCell *cell = (KoronToDoListCell *)[_didFinishTableView cellForRowAtIndexPath:indexPath];
        [self.checkedItem toggleChecked];
        
        cell.checkView.image = [UIImage imageNamed:@"backlog_finished.png"];
        cell.checkedLine.hidden = NO;
        
        [_toDoTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        
        [_didFinishTableView reloadData];
        
        [_items removeObject:self.checkedItem];
        [_items addObject:self.checkedItem];
        
        [SVProgressHUD showSuccessWithStatus:@"刷新成功" duration:1];
        
    } else if ([[dic objectForKey:@"code"] integerValue] >= 0 && [[dic objectForKey:@"desc"] isEqualToString:@"操作成功"] && _isLeftTableView == NO) {
        [_todoItems addObject:self.checkedItem];
        
        NSInteger index = [_didFinishItems indexOfObject:self.checkedItem];
        
        [_didFinishItems removeObjectAtIndex:index];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        
        KoronToDoListCell *cell = (KoronToDoListCell *)[_didFinishTableView cellForRowAtIndexPath:indexPath];
        [self.checkedItem toggleChecked];
        
        cell.checkView.image = [UIImage imageNamed:@"backlog_unfinished.png"];
        cell.checkedLine.hidden = YES;
        
        [_didFinishTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        
        [_toDoTableView reloadData];
        
        [_items removeObject:self.checkedItem];
        [_items addObject:self.checkedItem];
        
        [SVProgressHUD showSuccessWithStatus:@"刷新成功" duration:1];
    }else {
        [SVProgressHUD showErrorWithStatus:@"刷新失败" duration:1];
    }
    
    [self saveCheckListItems];
    
    
    
}

- (void)checkedOrUncheckedItemListRequestFail:(ASIFormDataRequest *)response
{
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
    
}

- (void)sendDeleteItemListRequest
{
    if (_deleteItemListRequest) {
        [_deleteItemListRequest clearDelegatesAndCancel];
        _deleteItemListRequest = nil;
    }
    
    _deleteItemListRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[[KoronManager sharedManager] getObjectForKey:SERVERURL]]];
    _deleteItemListRequest.delegate = self;
    [_deleteItemListRequest setDidFinishSelector:@selector(deleteItemListRequestFinish:)];
    [_deleteItemListRequest setDidFailSelector:@selector(deleteItemListRequestFail:)];
    
    [_deleteItemListRequest setPostValue:@"delMyTodo" forKey:@"op"];
    [_deleteItemListRequest setPostValue:self.checkedItem.itemId forKey:@"id"];
    [_deleteItemListRequest setPostValue:[[KoronManager sharedManager] getObjectForKey:SID] forKey:SID];
    
    [_deleteItemListRequest startAsynchronous];
}

- (void)deleteItemListRequestFinish:(ASIFormDataRequest *)response
{
    NSLog(@"delete: %@",[response responseString]);
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil];
    
    if ([[dic objectForKey:@"code"] integerValue] >= 0 && [[dic objectForKey:@"desc"] isEqualToString:@"操作成功"] && _isLeftTableView) {
        
        NSInteger index = [_todoItems indexOfObject:self.checkedItem];
        
        [_items removeObject:self.checkedItem];
        [_todoItems removeObjectAtIndex:index];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        
        [_toDoTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [SVProgressHUD showSuccessWithStatus:@"删除成功" duration:1];
        
        
    } else if ([[dic objectForKey:@"code"] integerValue] >= 0 && [[dic objectForKey:@"desc"] isEqualToString:@"操作成功"] && _isLeftTableView == NO) {
       
        NSInteger index = [_didFinishItems indexOfObject:self.checkedItem];
        
        [_items removeObject:self.checkedItem];
        [_didFinishItems removeObjectAtIndex:index];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        
        [_didFinishTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [SVProgressHUD showSuccessWithStatus:@"删除成功" duration:1];
        
    } else {
        [SVProgressHUD showErrorWithStatus:@"删除失败" duration:1];
    }
    [self saveCheckListItems];
    
    [self doneLoadingTableViewData];
    
}

- (void)deleteItemListRequestFail:(ASIFormDataRequest *)response
{
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
    
}


#pragma mark - ScrollView Delegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _scrollBackgroundView) {
        if (_scrollBackgroundView.contentOffset.x < kDeviceWidth) {
            self.title = @"待办";
            _isLeftTableView = YES;
            self.status = @"0";
        }else {
            self.title = @"完成";
            _isLeftTableView = NO;
            self.status = @"1";
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    if (decelerate == NO && scrollView == _scrollBackgroundView) {
        if (_scrollBackgroundView.contentOffset.x < kDeviceWidth) {
            self.title = @"待办";
            _isLeftTableView = YES;
            self.status = @"0";
        }else {
            self.title = @"完成";
            _isLeftTableView = NO;
            self.status = @"1";
        }
    }
    
    [_toDoRefreshTableView egoRefreshScrollViewDidEndDragging:scrollView];
}

#pragma mark - Documents
- (NSString *)documentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths firstObject];
    return documentsDirectory;
}

- (NSString *)dataFilePath
{
    return [[self documentsDirectory] stringByAppendingPathComponent:@"CheckLists.plist"];
}

- (void)saveCheckListItems
{
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    
    [archiver encodeObject:_items forKey:@"CheckListItems"];
    [archiver finishEncoding];
    
    [data writeToFile:[self dataFilePath] atomically:YES];
}


#pragma mark - KoronToDoListCell Delegate
- (void)KoronToDoListCellDidCheck:(KoronToDoListCell *)cell
{
    KoronChecklistItem *item = nil;
    if (_isLeftTableView) {
        NSIndexPath *indexPath = [_toDoTableView indexPathForCell:cell];
        
        item = _todoItems[indexPath.row];
        self.checkedItem = item;
        
        
    }else {
        NSIndexPath *indexPath = [_didFinishTableView indexPathForCell:cell];
        self.checkedItem = _didFinishItems[indexPath.row];
        
    }
    
    [self sendCheckedOrUncheckedItemListRequest];
    
    [SVProgressHUD showWithStatus:@"刷新中"];
    
    
    
    
    [self saveCheckListItems];
    
    
}

#pragma mark - KoronAddItemViewController Delegate
- (void)KoronAddItemViewController:(KoronAddItemViewController *)controller didFinishAddingItem:(KoronChecklistItem *)item
{
    NSInteger newRowIndex = [_todoItems count];
    [_todoItems addObject:item];
    [_items addObject:item];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:newRowIndex inSection:0];
    NSArray *indexPaths = @[indexPath];
    
    [self saveCheckListItems];
    
    [_toDoTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)KoronAddItemViewController:(KoronAddItemViewController *)controller didFinishEditingItem:(KoronChecklistItem *)item
{
    UITableViewCell *cell = nil;
    if (_scrollBackgroundView.contentOffset.x < kDeviceWidth) {
        NSInteger index = [_todoItems indexOfObject:item];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        
        cell = [_toDoTableView cellForRowAtIndexPath:indexPath];
        
        [_toDoTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }else {
        NSInteger index = [_didFinishItems indexOfObject:item];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        
        cell = [_didFinishTableView cellForRowAtIndexPath:indexPath];
        
        [_didFinishTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    
    [self saveCheckListItems];
    
    [self configureCheckmarkForCell:cell withChecklistItem:item];
    
}

#pragma mark - Private Method
- (void)loadChecklistItems
{
    NSString *path = [self dataFilePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        NSMutableData *data = [[NSMutableData alloc] initWithContentsOfFile:path];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        
        _items = [unarchiver decodeObjectForKey:@"CheckListItems"];
        [unarchiver finishDecoding];
        
        _todoItems = [[NSMutableArray alloc] initWithCapacity:20];
        _didFinishItems = [[NSMutableArray alloc] initWithCapacity:20];
        
        for (KoronChecklistItem *item in _items) {
            if (item.checked == NO) {
                [_todoItems addObject:item];
            }else {
                [_didFinishItems addObject:item];
            }
        }
        
    }else {
        _items = [[NSMutableArray alloc] initWithCapacity:20];
        _todoItems = [[NSMutableArray alloc] initWithCapacity:20];
        _didFinishItems = [[NSMutableArray alloc] initWithCapacity:20];
        
    }
    
    [self sendItemListRequest];
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    UIImage *image = [[UIImage imageNamed:@"btn_big_gray"] stretchableImageWithLeftCapWidth:5 topCapHeight:10];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setTitle:@"添加" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [button addTarget:self action:@selector(addItemView) forControlEvents:UIControlEventTouchUpInside];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    [button release];
    self.navigationItem.rightBarButtonItem = rightItem;
    [rightItem release];
    
}

- (void)configureCheckmarkForCell:(UITableViewCell *)cell withChecklistItem:(KoronChecklistItem *)item
{
    KoronToDoListCell *oneCell = (KoronToDoListCell *)cell;
    if (item.checked) {
        oneCell.checkView.image = [UIImage imageNamed:@"backlog_finished"];
        oneCell.checkedLine.hidden = NO;
        
    }else {
        oneCell.checkView.image = [UIImage imageNamed:@"backlog_unfinished"];
        oneCell.checkedLine.hidden = YES;
    }
    
    oneCell.timeLabel.text = [self loadFomatterDate:item.time];
    oneCell.toDoContentLabel.text = item.content;
    oneCell.colorLabel.text = item.type;
    oneCell.colorLabel.backgroundColor = [KoronItemListViewController hexStringToColor:item.typeColor];
}

+(UIColor *) hexStringToColor: (NSString *) stringToConvert
{
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    if ([cString hasPrefix:@"#"]) cString = [cString substringFromIndex:1];
    if ([cString length] != 6) return [UIColor blackColor];
    
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

#pragma mark - Target Action
- (void)addItemView
{
    if (_toDoTableView.editing) {
        [_toDoTableView setEditing:NO animated:YES];
        _scrollBackgroundView.scrollEnabled = YES;
        UIButton *rightItem = (UIButton *)[self.navigationItem.rightBarButtonItem customView];
        [rightItem setTitle:@"添加" forState:UIControlStateNormal];
    }else if (_didFinishTableView.editing) {
        [_didFinishTableView setEditing:NO animated:YES];
        _scrollBackgroundView.scrollEnabled = YES;
        UIButton *rightItem = (UIButton *)[self.navigationItem.rightBarButtonItem customView];
        [rightItem setTitle:@"添加" forState:UIControlStateNormal];
    }else {
        NSLog(@"添加");
        
        KoronAddItemViewController *addItemViewController = [[KoronAddItemViewController alloc] init];
        addItemViewController.delegate = self;
        addItemViewController.status = @"0";
        addItemViewController.itemId = @"";
        [self.navigationController pushViewController:addItemViewController animated:YES];
        [addItemViewController release];
    }
}

- (void)backButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getEditingMode:(UILongPressGestureRecognizer *)tap
{
    if (_scrollBackgroundView.contentOffset.x == 0) {
        [_toDoTableView setEditing:YES animated:YES];
    }else {
        [_didFinishTableView setEditing:YES animated:YES];
    }
    _scrollBackgroundView.scrollEnabled = NO;
    UIButton *rightItem = (UIButton *)[self.navigationItem.rightBarButtonItem customView];
    [rightItem setTitle:@"取消" forState:UIControlStateNormal];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == _toDoTableView) {
        return [_todoItems count];
    }else {
        return [_didFinishItems count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    KoronToDoListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[KoronToDoListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.delegate = self;
        
        UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(getEditingMode:)];
        longTap.minimumPressDuration = 1.5;
        [cell addGestureRecognizer:longTap];
        [longTap release];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    KoronChecklistItem *item = nil;
    
    if (tableView == _toDoTableView) {
        item = _todoItems[indexPath.row];
    }else {
        item = _didFinishItems[indexPath.row];
    }
    
    [self configureCheckmarkForCell:cell withChecklistItem:item];
    
    return cell;
}

#pragma mark - TableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    KoronChecklistItem *item = nil;
    if (tableView == _toDoTableView) {
        item = _todoItems[indexPath.row];
    }else {
        item = _didFinishItems[indexPath.row];
    }
    
    KoronAddItemViewController *addItemViewController = [[KoronAddItemViewController alloc] init];
    addItemViewController.itemToEdit = item;
    addItemViewController.itemId = item.itemId;
    addItemViewController.delegate = self;
    [self.navigationController pushViewController:addItemViewController animated:YES];
    [addItemViewController release];
    
}

#pragma mark - getDate Method
- (NSString *)loadFomatterDate:(NSString *)dateString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSInteger result = [self computationTime:[formatter stringFromDate:date]];
    if (result == 0) {
        [formatter setDateFormat:@"今天 HH:mm"];
    }else if (result == 1) {
        [formatter setDateFormat:@"昨天 HH:mm"];
    }
    else if (result == 2) {
        [formatter setDateFormat:@"前天 HH:mm"];
    }else if (result == 3) {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    }else {
        [formatter setDateFormat:@"MM-dd HH:mm"];
    }
    
    
    return [formatter stringFromDate:date];
    
}

- (NSInteger)computationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowadayTimeStr = [fromatter stringFromDate:[NSDate date]];
    
    if ([[nowadayTimeStr substringToIndex:4] integerValue]- [[timeStr substringToIndex:4] integerValue] != 0) {
        return 3;
    }
    
    if ([[nowadayTimeStr substringToIndex:4] integerValue] - [[timeStr substringToIndex:4] integerValue] == 0) {
        if ([[nowadayTimeStr substringWithRange:NSMakeRange(5, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(5, 2)] integerValue] == 0) {
            if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 0) {
                return 0;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
                return 1;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 2) {
                return 2;
            }else {
                return 4;
            }
        }else {
            return 4;
        }
        
    }else {
        return 4;
    }
    
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods
//开始重新加载时调用的方法
- (void)reloadTableViewDataSource{
	_reloading = YES;
    //开始刷新后执行后台线程，在此之前可以开启HUD或其他对UI进行阻塞
//    [NSThread detachNewThreadSelector:@selector(sendItemListRequest) toTarget:self withObject:nil];
    
    [self sendItemListRequest];
    
    [SVProgressHUD showWithStatus:@"加载中"];
}

//完成加载时调用的方法
- (void)doneLoadingTableViewData{
    NSLog(@"doneLoadingTableViewData");
    
	_reloading = NO;
    
    if (_isLeftTableView) {
        [_toDoRefreshTableView egoRefreshScrollViewDataSourceDidFinishedLoading:_toDoTableView];
        //刷新表格内容
        [_toDoTableView reloadData];
    }else {
        [_didFinishRefreshTableView egoRefreshScrollViewDataSourceDidFinishedLoading:_didFinishTableView];
        //刷新表格内容
        [_didFinishTableView reloadData];
    }
	   
    [SVProgressHUD showSuccessWithStatus:@"刷新完成" duration:1];
}

#pragma mark -
#pragma mark Background operation
//这个方法运行于子线程中，完成获取刷新数据的操作
-(void)doInBackground
{
    NSLog(@"doInBackground");
    
    
    //后台操作线程执行完后，到主线程更新UI
    [self performSelectorOnMainThread:@selector(doneLoadingTableViewData) withObject:nil waitUntilDone:YES];
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods
//下拉被触发调用的委托方法
-(void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view
{
    
    [self reloadTableViewDataSource];
}

//返回当前是刷新还是无刷新状态
-(BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view
{
    return _reloading;
}

//返回刷新时间的回调方法
-(NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view
{
    return [NSDate date];
}

#pragma mark -
#pragma mark UIScrollViewDelegate Methods
//滚动控件的委托方法
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (_isLeftTableView) {
        [_toDoRefreshTableView egoRefreshScrollViewDidScroll:scrollView];
    }else {
        [_didFinishRefreshTableView egoRefreshScrollViewDidScroll:scrollView];
    }
}




#pragma mark - Memory
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_deleteItemListRequest release], _deleteItemListRequest = nil;
    [_checkItemListRequest release], _checkItemListRequest = nil;
    [_itemListRequest release], _itemListRequest = nil;
    [_toDoTableView release], _toDoTableView = nil;
    [_didFinishTableView release], _didFinishTableView = nil;
    [_items release], _items = nil;
    [_scrollBackgroundView release], _scrollBackgroundView = nil;
    [super dealloc];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        if (tableView == _toDoTableView) {
            self.checkedItem = _todoItems[indexPath.row];
            
            [self sendDeleteItemListRequest];
            
//            [self sendCheckedOrUncheckedItemListRequest];
            
//            NSInteger index = [_todoItems indexOfObject:self.checkedItem];
//            
//            [_items removeObject:self.checkedItem];
//            [_todoItems removeObjectAtIndex:index];
//            
//            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
//            
//            [_toDoTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
        }else {
            self.checkedItem = _didFinishItems[indexPath.row];
            
            NSInteger index = [_didFinishItems indexOfObject:self.checkedItem];
            
            [_items removeObject:self.checkedItem];
            [_didFinishItems removeObjectAtIndex:index];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            
            [_didFinishTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
        
        
        
//        [SVProgressHUD showWithStatus:@"刷新中"];
        
    }
    
}


@end
