//
//  UINavigationBar+Helper.m
//  origami3d
//
//  Created by yangxi zou on 11-3-7.
//  Copyright 2011 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "UINavigationBar+Helper.h"
#import "CommonConst.h"

@interface SMNavigationBar : UINavigationBar

@end

@implementation SMNavigationBar

- (void)drawRect:(CGRect)rect {
   [super drawRect:rect];
}
@end

@implementation UINavigationBar  (TENavigationBar)

+ (Class)class {
    return NSClassFromString(@"SMNavigationBar");
}

- (void) drawRect:(CGRect)rect
{
	[super drawRect:rect];
	//CGContextRef context = UIGraphicsGetCurrentContext();
	//CGContextSetFillColor(context, CGColorGetComponents([UIColor colorWithRed:0.5 green:0.5 blue:0 alpha:1].CGColor)); // don't make color too saturated
	//CGContextFillRect(context, rect); // draw base
//    UIImage *image = [UIImage imageNamed:@"navbarbg.png"];
    UIImage *image = [UIImage imageNamed:@"home_top1.png"];
    [image drawAsPatternInRect:CGRectMake(0, 0, self.frame.size.width,  self.frame.size.height)];
    //image = [UIImage imageNamed:@"tableheadbg.png"];
    //[image drawInRect:CGRectMake(0, 40, self.frame.size.width,  self.frame.size.height+20)];
    //self.tintColor = COMMON_BACKGROUND;
        
    [self.layer setShadowOpacity:0.6];
    [self.layer setShadowOffset:CGSizeMake(0.5, 1)];
}

@end

 

@implementation UIToolbar (TENavigationBar)
 
- (void) setupIos5PlusNavBarImage
{
    if ([UINavigationBar respondsToSelector: @selector(appearance)])
    {
//        [[UINavigationBar appearance] setBackgroundImage: [UIImage imageNamed: @"navbarbg.png"] forBarMetrics: UIBarMetricsDefault];
        [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"home_top"] forBarMetrics:UIBarMetricsDefault];
    }
}
- (void) drawRect:(CGRect)rect
{
   [super drawRect:rect];
	//CGContextRef context = UIGraphicsGetCurrentContext();
	//CGContextSetFillColor(context, CGColorGetComponents([UIColor colorWithRed:0.5 green:0.5 blue:0 alpha:1].CGColor)); // don't make color too saturated
	//CGContextFillRect(context, rect); // draw base
	//self.superview.backgroundColor = [UIColor clearColor];
    //UIImage *image = [UIImage imageNamed:@"navbarbg.png"];	
    //[image drawInRect:CGRectMake(0, 0, self.frame.size.width,  self.frame.size.height)];//+7
	//[image drawInRect:CGRectMake(0, 0, self.frame.size.width,  self.frame.size.height) blendMode:kCGBlendModeOverlay   alpha:1.0];//+7
	//self.tintColor = COMMON_BACKGROUND;
	//self.tintColor = COMMON_BACKGROUND;
	//self.barStyle = UIBarStyleBlackTranslucent;
    
    //CGRect bounds=[self bounds];
    //NSLog(@"%@",[NSValue valueWithCGRect:bounds]);
    
    
    //NSLog(@"%@",[NSValue valueWithCGRect:bounds]);
    
    // [[UIColor redColor] set];
    
    //[[UIColor colorWithPatternImage:[UIImage imageNamed:@"navbarbg.png"]] set];
    
    //UIRectFill (bounds);
//    UIImage *image = [UIImage imageNamed:@"navbarbg.png"];
    UIImage *image = [UIImage imageNamed:@"home_top"];
    [image drawAsPatternInRect:CGRectMake(0, 0, self.frame.size.width,  self.frame.size.height)];
    
    
}

@end




