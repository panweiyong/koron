//
//  KoronToDoListModel.m
//  TodoListDemo
//
//  Created by Mac Mini on 13-11-20.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronChecklistItem.h"



@implementation KoronChecklistItem

//property jsonKey
- (id)mapAttributes
{
    NSDictionary *mapDic = @{
                             @"row"           : @"row_id",
                             @"itemId"        : @"id",
                             @"content"       : @"title",
                             @"time"          : @"time",
                             @"type"          : @"type",
                             @"typeColor"     : @"typeColor"
                             };
    return mapDic;
}

- (void)toggleChecked
{
    self.checked = !self.checked;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.row = [aDecoder decodeObjectForKey:@"Row"];
        self.itemId = [aDecoder decodeObjectForKey:@"ItemId"];
        self.checked = [aDecoder decodeBoolForKey:@"Checked"];
        self.content = [aDecoder decodeObjectForKey:@"Content"];
        self.time = [aDecoder decodeObjectForKey:@"Time"];
        self.type = [aDecoder decodeObjectForKey:@"Type"];
        self.typeColor = [aDecoder decodeObjectForKey:@"TypeColor"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.row forKey:@"Row"];
    [aCoder encodeObject:self.itemId forKey:@"ItemId"];
    [aCoder encodeBool:self.checked forKey:@"Checked"];
    [aCoder encodeObject:self.content forKey:@"Content"];
    [aCoder encodeObject:self.time forKey:@"Time"];
    [aCoder encodeObject:self.type forKey:@"Type"];
    [aCoder encodeObject:self.typeColor forKey:@"TypeColor"];
}

- (void)dealloc
{
    self.row = nil;
    self.itemId = nil;
    self.type = nil;
    self.typeColor = nil;
    self.content = nil;
    self.time = nil;
    [super dealloc];
}

@end
