//
//  KoronAddItemViewController.m
//  TodoListDemo
//
//  Created by Mac Mini on 13-11-20.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronAddItemViewController.h"


@interface KoronAddItemViewController ()

@end

@implementation KoronAddItemViewController
{
    UIView *_backgroundView;
    UITextField *_textField;
    UILabel *_specifiedTimeLabel;
    UIButton *_doneButton;
    ASIFormDataRequest *_addOrEditRequest;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"添加待办";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self loadNavigationItem];
    
    
    
    if (IOS_VERSIONS >= 7.0) {
        self.navigationController.navigationBar.translucent = NO;
        self.tabBarController.tabBar.translucent = NO;
    }
    
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight-20-44-49)];
//    _backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.1];
    [self.view addSubview:_backgroundView];
    
	
    UILabel *itemLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, 20, 40, 20)];
    itemLabel.text = @"事项";
    itemLabel.backgroundColor = [UIColor clearColor];
    itemLabel.font = [UIFont boldSystemFontOfSize:20];
    itemLabel.textColor = [UIColor colorWithRed:73/255.0 green:166/255.0 blue:236/255.0 alpha:1];
    [_backgroundView addSubview:itemLabel];
    [itemLabel release];
    
    UIImage *itemLineImage = [[UIImage imageNamed:@"contact_search_bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    UIImageView *itemLineView = [[UIImageView alloc] initWithFrame:CGRectMake(itemLabel.left, itemLabel.bottom+30, 270, 10)];
    itemLineView.image = itemLineImage;
    [_backgroundView addSubview:itemLineView];
    [itemLineView release];
    
    _textField = [[UITextField alloc] initWithFrame:CGRectMake(itemLineView.left+5, itemLabel.bottom+10, 260, 30)];
    _textField.borderStyle = UITextBorderStyleNone;
    _textField.backgroundColor = [UIColor clearColor];
    _textField.autoresizingMask = YES;
    _textField.returnKeyType = UIReturnKeyDone;
    _textField.delegate = self;
    [_backgroundView addSubview:_textField];
    
    
    UILabel *remindLabel = [[UILabel alloc] initWithFrame:CGRectMake(25, itemLineView.bottom+50, 40, 20)];
    remindLabel.text = @"提醒";
    remindLabel.backgroundColor = [UIColor clearColor];
    remindLabel.font = [UIFont boldSystemFontOfSize:20];
    remindLabel.textColor = [UIColor colorWithRed:73/255.0 green:166/255.0 blue:236/255.0 alpha:1];
    [_backgroundView addSubview:remindLabel];
    [remindLabel release];
    
    NSInteger gap = 0;
    for (NSInteger index = 0; index < 5; index++) {
        UIImageView *selectedTimeView = [[UIImageView alloc] initWithFrame:CGRectMake(remindLabel.right, remindLabel.bottom+gap, 30, 30)];
        selectedTimeView.image = [UIImage imageNamed:@"radiobutton_normal.png"];
        selectedTimeView.tag = 100+index;
        selectedTimeView.userInteractionEnabled = YES;
        [_backgroundView addSubview:selectedTimeView];
        [selectedTimeView release];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectedTime:)];
        [selectedTimeView addGestureRecognizer:tap];
        [tap release];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(selectedTimeView.right, selectedTimeView.top, 80, selectedTimeView.height)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor colorWithRed:114/255.0 green:114/255.0 blue:114/255.0 alpha:1];
        
        switch (index) {
            case 0:
                timeLabel.text = @"不提醒";
                if (self.itemToEdit == nil ||(self.itemToEdit != nil && self.itemToEdit.time.length == 0)) {
                    selectedTimeView.image = [UIImage imageNamed:@"radiobutton_check.png"];
                }
                
                break;
                
            case 1:
                timeLabel.text = @"半小时";
                break;
                
            case 2:
                timeLabel.text = @"一小时";
                break;
                
            case 3:
                timeLabel.text = @"明天此时";
                break;
                
            case 4:
                timeLabel.text = @"指定时间";
                if (self.itemToEdit != nil && self.itemToEdit.time.length != 0) {
                    selectedTimeView.image = [UIImage imageNamed:@"radiobutton_check.png"];
                }
                UIImageView *timeLineView = [[UIImageView alloc] initWithFrame:CGRectMake(timeLabel.right, timeLabel.bottom-15, 120, 10)];
                timeLineView.image = itemLineImage;
                [_backgroundView addSubview:timeLineView];
                [timeLineView release];
                
                _specifiedTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(timeLineView.left+5, timeLineView.bottom-20, timeLineView.width-10, 20)];
                _specifiedTimeLabel.textAlignment = NSTextAlignmentCenter;
//                _specifiedTimeLabel.text = @"10:30";
                _specifiedTimeLabel.backgroundColor = [UIColor clearColor];
                _specifiedTimeLabel.font = [UIFont systemFontOfSize:10];
                _specifiedTimeLabel.userInteractionEnabled = YES;
                [_backgroundView addSubview:_specifiedTimeLabel];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setTime:)];
                [_specifiedTimeLabel addGestureRecognizer:tap];
                [tap release];
                
                break;
                
            default:
                break;
        }
        
        [_backgroundView addSubview:timeLabel];
        [timeLabel release];
        
        gap += 40;
    }
    
    if (self.itemToEdit == nil) {
        _doneButton.enabled = NO;
        [_doneButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }else {
        _doneButton.enabled = YES;
        [_doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _textField.text = self.itemToEdit.content;
        _specifiedTimeLabel.text = [self resetDateString:self.itemToEdit.time];
    
    }
    
}

#pragma mark - ASIRequest
- (void)sendAddOrEditRequest
{
    if (_addOrEditRequest) {
        [_addOrEditRequest clearDelegatesAndCancel];
        _addOrEditRequest = nil;
    }
    
    _addOrEditRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[[KoronManager sharedManager] getObjectForKey:SERVERURL]]];
    _addOrEditRequest.delegate = self;
    [_addOrEditRequest setDidFinishSelector:@selector(addOrEditRequestFinish:)];
    [_addOrEditRequest setDidFailSelector:@selector(addOrEditRequestFail:)];
    
    [_addOrEditRequest setPostValue:@"addUpdateMyTodo" forKey:@"op"];
    [_addOrEditRequest setPostValue:self.itemId forKey:@"id"];
    [_addOrEditRequest setPostValue:_textField.text forKey:@"title"];
    [_addOrEditRequest setPostValue:self.status forKey:@"status"];
    [_addOrEditRequest setPostValue:self.selectedTime forKey:@"time"];
    [_addOrEditRequest setPostValue:[[KoronManager sharedManager] getObjectForKey:SID] forKey:SID];
    
    [_addOrEditRequest startAsynchronous];
}

- (void)addOrEditRequestFinish:(ASIFormDataRequest *)response
{
    NSLog(@"AddOrEditRequest: %@",[response responseString]);
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil];
    
    if ([[dic objectForKey:@"code"] integerValue] >= 0 && [[dic objectForKey:@"desc"] isEqualToString:@"操作成功"]) {
        
        [SVProgressHUD showSuccessWithStatus:@"更新成功" duration:1];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

- (void)addOrEditRequestFail:(ASIFormDataRequest *)response
{
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
    
}


- (NSString *)resetDateString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *labelDate = [dateFormatter dateFromString:dateString];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSString *string = [dateFormatter stringFromDate:labelDate];
    NSLog(@"%@",string);
    [dateFormatter release];
    
    return string;
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (newString.length > 0) {
        _doneButton.enabled = YES;
        [_doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }else {
        _doneButton.enabled = NO;
        [_doneButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    
    return YES;
}

#pragma mark - Private Method
- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
    
    _doneButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    UIImage *image = [[UIImage imageNamed:@"btn_big_gray"] stretchableImageWithLeftCapWidth:5 topCapHeight:10];
    [_doneButton setBackgroundImage:image forState:UIControlStateNormal];
    [_doneButton setTitle:@"确定" forState:UIControlStateNormal];
    [_doneButton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [_doneButton addTarget:self action:@selector(doConfirm) forControlEvents:UIControlEventTouchUpInside];
    [_doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:_doneButton];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    [rightItem release];
}

#pragma mark - Target Action
- (void)backButtonPressed
{
    [SVProgressHUD dismiss];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)doConfirm
{
    NSLog(@"确定");
    
    if (self.itemToEdit == nil) {
        KoronChecklistItem *item = [[KoronChecklistItem alloc] init];
        item.content = _textField.text;
        item.time = self.selectedTime;
        item.checked = NO;
        item.typeColor = @"#f0f0f0";
        item.type = @"未分类";
        if ([self.delegate respondsToSelector:@selector(KoronAddItemViewController:didFinishAddingItem:)]) {
            [self.delegate KoronAddItemViewController:self didFinishAddingItem:item];
        }
    }else {
        self.itemToEdit.content = _textField.text;
        self.itemToEdit.time = self.selectedTime;
        
        if ([self.delegate respondsToSelector:@selector(KoronAddItemViewController:didFinishEditingItem:)]) {
            [self.delegate KoronAddItemViewController:self didFinishEditingItem:self.itemToEdit];
        }
    }
    
    [self sendAddOrEditRequest];
    [SVProgressHUD showWithStatus:@"刷新中"];
    
    
}

- (void)selectedTime:(UITapGestureRecognizer *)tap
{
    for (id view in _backgroundView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            UIImageView *imageView = (UIImageView *)view;
            if (imageView.tag < 105 && imageView.tag >= 100) {
                imageView.image = [UIImage imageNamed:@"radiobutton_normal.png"];
            }
        }
    }
    
    UIImageView *selectedView = (UIImageView *)tap.view;
    selectedView.image = [UIImage imageNamed:@"radiobutton_check.png"];
    
    if (selectedView.tag == 100) {
        
        self.selectedTime = @"";
        
    }else if (selectedView.tag == 101) {
        
        NSDate *date = [NSDate dateWithTimeIntervalSinceNow:60*30];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString = [dateFormatter stringFromDate:date];
        self.selectedTime = dateString;
        [dateFormatter release];
        NSLog(@"%@",dateString);
        
    }else if (selectedView.tag == 102) {
        
        NSDate *date = [NSDate dateWithTimeIntervalSinceNow:60*60];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString = [dateFormatter stringFromDate:date];
        self.selectedTime = dateString;
        [dateFormatter release];
        NSLog(@"%@",dateString);
        
    }else if (selectedView.tag == 103) {
        
        NSDate *date = [NSDate dateWithTimeIntervalSinceNow:60*60*24];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString = [dateFormatter stringFromDate:date];
        self.selectedTime = dateString;
        [dateFormatter release];
        NSLog(@"%@",dateString);
        
    }
}

- (void)setTime:(UITapGestureRecognizer *)tap
{
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.tag = 100;
    datePicker.maximumDate = [NSDate dateWithTimeIntervalSinceNow:60*60*24*365*10];
    datePicker.minimumDate = [NSDate date];
    datePicker.date = [NSDate date];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"确定", nil];
    [actionSheet setBounds:CGRectMake(0,0,320, 500)];
    [actionSheet showInView:self.view];
    [actionSheet addSubview:datePicker];
    [actionSheet release];
    [datePicker release];
}

#pragma mark - ActionSheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIDatePicker *datePicker = (UIDatePicker *)[actionSheet viewWithTag:100];
    NSDate *date = datePicker.date;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    self.selectedTime = dateString;
    _specifiedTimeLabel.text = dateString;
    
    NSLog(@"%@",dateString);
    [dateFormatter release];
}



#pragma mark - Memory
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_addOrEditRequest release], _addOrEditRequest = nil;
    [_doneButton release], _doneButton = nil;
    [_specifiedTimeLabel release], _specifiedTimeLabel = nil;
    [_backgroundView release],_backgroundView = nil;
    [_textField release],_textField = nil;
    [super dealloc];
}

@end
