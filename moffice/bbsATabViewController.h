//
//  bbsATabViewController.h
//  moffice
//
//  Created by lijinhua on 13-6-7.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JsonService.h"
#import "bbsCell.h"
#import "AbstractViewController.h"
@interface bbsATabViewController : UITableViewController<UIScrollViewDelegate,UIWebViewDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    NSString *refid;
    NSString *tipsid;
    
    NSString *topicName;
    NSString *userName;
    NSString *creatTime;
    NSString *webContent;
    
    //评论需要的数据
    NSString *topicId;
    NSString *replayContent;
    
    NSArray *commentsArr;
    UITableView *replayTabview;
    
    NSMutableArray *dataArr;
    UIActivityIndicatorView *activity;
    UIButton *replayBtn;
    
    NSString *replayNum;
    NSString *replayResult;
    float bbsWebheight;
}
@property(nonatomic,retain) NSString *refid;
@property(nonatomic,retain) NSString *tipsid;
@property(nonatomic,retain) UIScrollView *bbsScrollview;
@property(nonatomic,retain) NSArray *commentsArr;
@property(nonatomic,retain) UITableView *replayTabview;
@property(nonatomic,retain) NSMutableArray *dataArr;
@property(nonatomic,retain) UIActivityIndicatorView *activity;
@property(nonatomic,retain) NSString *topicId;
@property(nonatomic,retain) NSString *replayContent;
@property(nonatomic,retain) UIButton *replayBtn;
@property(nonatomic,retain) NSString *replayNum;
@property(nonatomic,retain) NSString *replayResult;

@end
