//
//  BadgeView.h
//  moffice
//
//  Created by Mac Mini on 13-12-3.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BadgeView : UIView

@property (nonatomic, retain) UILabel *badgeLabel;

@end
