//
//  KoronUser.m
//  moffice
//
//  Created by 潘蔚勇 on 14-1-10.
//  Copyright (c) 2014年 Hsn. All rights reserved.
//

#import "KoronUser.h"

@implementation KoronUser

+(KoronUser *)koronUserWithNSDictionary:(NSDictionary *)dictionary {
    KoronUser *user = [[KoronUser alloc] init];
    
    user.id = [dictionary objectForKey:@"id"];
    user.empNumber = [dictionary objectForKey:@"empNumber"];
    user.name = [dictionary objectForKey:@"name"];
    user.gender = [dictionary objectForKey:@"gender"];
    user.title = [dictionary objectForKey:@"title"];
    user.sign = [dictionary objectForKey:@"sign"];
    user.department = [dictionary objectForKey:@"department"];
    user.telephone = [dictionary objectForKey:@"telephone"];
    user.mobile = [dictionary objectForKey:@"mobile"];
    user.icon = [dictionary objectForKey:@"icon"];
    user.lastUpdaeTime = [dictionary objectForKey:@"lastUpdaeTime"];
    user.statusId = [dictionary objectForKey:@"statusId"];
    user.alpha = @"";
    user.fullAlpha = @"";
    user.online = @"4";
    user.onlineType = @"0";
    
    
    
    return user;
}

@end
