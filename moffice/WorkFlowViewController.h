//
//  WorkFlowViewController.h
//  moffice
//
//  Created by yangxi zou on 11-12-29.
//  Copyright (c) 2011年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "AbstractViewController.h"
#import "CommonConst.h"
#import "SMUtils.h"
#import "JsonService.h"
#import "WorkflowCell.h"
#import "WorkFlowDetailsController.h"

@interface WorkFlowViewController : AbstractViewController<UITableViewDelegate,UITableViewDataSource >
{
    //id tableData;
    int requestModel;
}
@end
