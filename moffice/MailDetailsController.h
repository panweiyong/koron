//
//  MailDetailsController.h
//  moffice
//
//  Created by yangxi zou on 12-1-17.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "JsonService.h"
#import "AbstractViewController.h"
#import "ImageViewController.h"
#import "CommonConst.h"
#import "NewMailController.h"//UIWebViewDelegate

@interface MailDetailsController : AbstractViewController<UITableViewDelegate,UITableViewDataSource,UIWebViewDelegate,UIAlertViewDelegate,UIScrollViewDelegate>
{
    NSString *_mid;
    int _currentindex;
    NSMutableArray *_allmids;
    //保存flag判断是否已读邮件
    NSInteger _flag;
    
    NSString *_accounts;
    
    BOOL _isintranet;
    BOOL showTurePage;
    NSString *tipsid;
    
    NSMutableArray *buttons;
    
    BOOL isSystemMail;
    BOOL isReply;
    BOOL _isCollect;
    

//    UIView  *_attachBackfroundView;
    UIButton *_attachButton;
    UIView *_attachView;
    UIWebView *_webview;
    
} 
@property (nonatomic, retain) NSString *mid;
@property (nonatomic, retain) NSString *tipsid;
@property (nonatomic, retain) NSArray *allmids;
@property (nonatomic, copy) NSString *pathId;

@property (nonatomic, assign) NSInteger flag;
@property (nonatomic, assign) int currentindex;
@property (nonatomic, assign) BOOL isintranet;
@property (nonatomic, assign) BOOL showTurePage;
@property (nonatomic, retain) NSString *accounts;
@property (nonatomic, assign) BOOL isSystemMail;
@property (nonatomic, assign) BOOL isReply;;
//@property (nonatomic, copy) NSString *content;

- (id)initWithMid:(NSString *)mid Currentindex:(int)currentindex Accounts:(NSString *)accounts AllMids:(NSMutableArray *)allMides Isintranet:(BOOL)isIntranet;

@end
