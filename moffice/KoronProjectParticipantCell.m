//
//  KoronProjectContentCell.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-27.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectParticipantCell.h"
#import "KoronParticipantView.h"

@implementation KoronProjectParticipantCell
{
    UIView *_frameView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

- (void)initSubviews
{
    _frameView = [[UIView alloc] initWithFrame:CGRectZero];
    _frameView.layer.borderWidth = 1;
    _frameView.layer.borderColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1].CGColor;
    [self.contentView addSubview:_frameView];
    
    for (NSInteger index = 0; index < 31; index++) {
        KoronParticipantView *participantView = [[KoronParticipantView alloc] initWithFrame:CGRectZero];
        participantView.tag = index + 200;
        [self.contentView addSubview:participantView];
        [participantView release];
        
        UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseParticipant:)];
        [participantView addGestureRecognizer:gestureRecognizer];
        [gestureRecognizer release];
        
    }
    
}

- (void)chooseParticipant:(UITapGestureRecognizer *)gesture
{
    NSLog(@"%d",gesture.view.tag - 200);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _frameView.frame = CGRectMake(0, 0, self.width, self.height);
    
    for (NSInteger index = 0; index < 31; index++) {
        KoronParticipantView *view = (KoronParticipantView *)[self viewWithTag:index + 200];
        view.frame = CGRectMake(10 + 150 * (index % 2), 10 + 60 * (index / 2), 130, 50);
    }
}

- (void)setFrame:(CGRect)frame
{
    
    frame.origin.x += 10;
    frame.size.width -= 20;
    [super setFrame:frame];
    
}

@end
