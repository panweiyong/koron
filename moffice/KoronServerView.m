//
//  KoronServerView.m
//  moffice
//
//  Created by Mac Mini on 13-9-26.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronServerView.h"

@implementation KoronServerView
@synthesize textField = _textField;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _rect = frame;
        
        _manager = [KoronManager sharedManager];
        
        _topView = [[UIView alloc]initWithFrame:CGRectMake(5, 5, 80, 20)];
        _topView.backgroundColor = [UIColor clearColor];
        [self addSubview:_topView];
        
        _Label = [[UILabel alloc]initWithFrame:CGRectMake(0, 5, 50, 20)];
        _Label.backgroundColor = [UIColor clearColor];
        _Label.font = [UIFont systemFontOfSize:14];
        _Label.textColor = [UIColor grayColor];
        _Label.text = @"服务器";
        [_topView addSubview:_Label];
        
        _ImageView = [[UIImageView alloc]initWithFrame:CGRectMake(55, 5, 20, 20)];
        [_ImageView setImage:[UIImage imageNamed:@"login_server_up.png"]];
        [_topView addSubview:_ImageView];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
        [_topView addGestureRecognizer:tapGesture];
        
        
        _bottomView = [[UIView alloc]initWithFrame:CGRectZero];
        _bottomView.backgroundColor = [UIColor redColor];
        _bottomView.hidden = YES;
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.layer.masksToBounds = YES;
        [_bottomView.layer setCornerRadius:8.0];
        [_bottomView.layer setBorderWidth:1.0];
        [_bottomView.layer setBorderColor:[UIColor colorWithRed:211/255.0 green:224/255.0 blue:231/255.0 alpha:1].CGColor];
        [self addSubview:_bottomView];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 50, 40)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont systemFontOfSize:16];
        label.textColor = [UIColor grayColor];
        label.text = @"服务器";
        [_bottomView addSubview:label];
        
        _textField = [[UITextField alloc]initWithFrame:CGRectMake(60, 0, self.bounds.size.width - 60, 40)];
        _textField.text = [_manager getObjectForKey:SERVER];
        _textField.font = [UIFont systemFontOfSize:16];
        _textField.delegate = self;
        _textField.keyboardType = UIKeyboardTypeDefault;
        _textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [_bottomView addSubview:_textField];
        
        [_manager saveObject:_textField.text forKey:@"SettingUrl"];

        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)tapGesture {
    if (_bottomView.hidden) {
        [UIView animateWithDuration:0.2 animations:^{
            CGRect rect = self.frame;
            rect.origin.y = _rect.origin.y - 40;
            [self setFrame:rect];
            [_ImageView setImage:[UIImage imageNamed:@"login_server_down.png"]];
            
        }completion:^(BOOL finished){
            [_bottomView setFrame:CGRectMake(0, 35, self.frame.size.width, 40)];
            _bottomView.hidden = NO;
        }];
        
    }else {
        [UIView animateWithDuration:0.2 animations:^{
            [_bottomView setFrame:CGRectZero];
            _bottomView.hidden = YES;
            [_ImageView setImage:[UIImage imageNamed:@"login_server_up.png"]];
            [self setFrame:_rect];
        }];
    }
}

//
////textField失去焦点保存服务器URL到本地
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    [_manager saveObject:[NSString stringWithFormat:@"http://%@/moffice",textField.text] forKey:SERVERURL];
//    [_manager saveObject:_textField.text forKey:@"SettingUrl"];
//}

@end
