//
//  KoronProjectHeaderCell.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-26.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectHeaderCell.h"

@implementation KoronProjectHeaderCell
{
    UIView *_leftLine;
    UIView *_rightLine;
    UIView *_middleLine;
    
    UIImageView *_typeSelectedView;
    UIImageView *_statusSelectedView;
    
    UILabel *_upLabel;
    UILabel *_downLabel;
    
    NSInteger _statusIndex;
    NSInteger _typeIndex;

}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier withStatusIndex:(NSInteger)statusIndex andTypeIndex:(NSInteger)typeIndex;
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
        _statusIndex = statusIndex;
        _typeIndex = typeIndex;
    }
    return self;
}

- (void)initSubviews
{
    _leftLine = [[UIView alloc] initWithFrame:CGRectZero];
    _leftLine.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
    [self.contentView addSubview:_leftLine];
    
    _rightLine = [[UIView alloc] initWithFrame:CGRectZero];
    _rightLine.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
    [self.contentView addSubview:_rightLine];
    _middleLine = [[UIView alloc] initWithFrame:CGRectZero];
    _middleLine.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
    [self.contentView addSubview:_middleLine];
    
    UIImage *image = [[UIImage imageNamed:@"RadarSearchBlueBg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 15, 0, 15)];
    _typeSelectedView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _typeSelectedView.image = image;
    [self.contentView addSubview:_typeSelectedView];

    _statusSelectedView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _statusSelectedView.image = image;
    [self.contentView addSubview:_statusSelectedView];

    
    _upLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _upLabel.font = [UIFont systemFontOfSize:12];
    _upLabel.text = @"全部               我负责的               我参与的";
    _upLabel.textAlignment = NSTextAlignmentCenter;
    _upLabel.userInteractionEnabled = YES;
    _upLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_upLabel];
    
    UITapGestureRecognizer *typetap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeType:)];
    [_upLabel addGestureRecognizer:typetap];
    [typetap release];
    
    _downLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _downLabel.font = [UIFont systemFontOfSize:12];
    _downLabel.text = @"全部               进行中的               已完成的";
    _downLabel.textAlignment = NSTextAlignmentCenter;
    _downLabel.backgroundColor = [UIColor clearColor];
    _downLabel.userInteractionEnabled = YES;
    [self.contentView addSubview:_downLabel];
    
    UITapGestureRecognizer *statusTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeStatus:)];
    [_downLabel addGestureRecognizer:statusTap];
    [statusTap release];
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _leftLine.frame = CGRectMake(0, 0, 0.5, self.height);
    _rightLine.frame = CGRectMake(self.width-0.5, 0, 0.5, self.height);
    _middleLine.frame = CGRectMake(0, self.height/2.0, kDeviceWidth, 1);
    
    _upLabel.frame = CGRectMake(0, 0, self.width, self.height/2.0);
    _downLabel.frame = CGRectMake(0, self.height/2.0, self.width, self.height/2.0);
    
    [self typeSelectedViewSetFrame];
    [self stautsSelectedViewSetFrame];
}

- (void)typeSelectedViewSetFrame
{
    if (_typeIndex == 0) {
        _typeSelectedView.frame = CGRectMake(22, 2.5, 60, 29);
    } else if (_typeIndex == 1) {
        _typeSelectedView.frame = CGRectMake(22 + 75, 2.5, 90, 29);
    } else {
        _typeSelectedView.frame = CGRectMake(22 + 75 + 100, 2.5, 90, 29);
    }
}

- (void)stautsSelectedViewSetFrame
{
    if (_statusIndex == 0) {
        _statusSelectedView.frame = CGRectMake(22, 2.5 + self.height/2.0, 60, 29);
    } else if (_statusIndex == 1) {
        _statusSelectedView.frame = CGRectMake(22 + 75, 2.5 + self.height/2.0, 90, 29);
    } else {
        _statusSelectedView.frame = CGRectMake(22 + 75 + 100, 2.5 + self.height/2.0, 90, 29);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)changeType:(UITapGestureRecognizer *)tap
{
    CGPoint point = [tap locationInView:_upLabel];
    
    __block NSString *type = nil;
    
    [UIView animateWithDuration:0.3 animations:^{
        if (point.x < 100) {
            type = @"0";
            _typeSelectedView.frame = CGRectMake(22, 2.5, 60, 29);
            _typeIndex = 0;
        } else if (point.x < 200) {
            type = @"1";
            _typeSelectedView.frame = CGRectMake(22 + 75, 2.5, 90, 29);
            _typeIndex = 1;
        } else {
            type = @"2";
            _typeSelectedView.frame = CGRectMake(22 + 75 + 100, 2.5, 90, 29);
            _typeIndex = 2;
        }
    }];
    
    if ([self.delegate respondsToSelector:@selector(KoronProjectHeaderCell:DidChangeType:)]) {
        [self.delegate KoronProjectHeaderCell:self DidChangeType:type];
    }
    
}

- (void)changeStatus:(UITapGestureRecognizer *)tap
{
    CGPoint point = [tap locationInView:_upLabel];
    
    __block NSString *status = nil;
    
    [UIView animateWithDuration:0.3 animations:^{
        if (point.x < 100) {
            status = @"0";
            _statusSelectedView.frame = CGRectMake(22, 2.5 + self.height/2.0, 60, 29);
            _statusIndex = 0;
        } else if (point.x < 200) {
            status = @"1";
            _statusSelectedView.frame = CGRectMake(22 + 75, 2.5 + self.height/2.0, 90, 29);
            _statusIndex = 1;
        } else {
            status = @"2";
            _statusSelectedView.frame = CGRectMake(22 + 75 + 100, 2.5 + self.height/2.0, 90, 29);
            _statusIndex = 2;
        }
    }];
    
    if ([self.delegate respondsToSelector:@selector(KoronProjectHeaderCell:DidChangeStatus:)]) {
        [self.delegate KoronProjectHeaderCell:self DidChangeStatus:status];
    }
    
}


- (void)setFrame:(CGRect)frame
{
    if (IOS_VERSIONS >= 7.0) {
        frame.origin.x += 5;
        frame.size.width -= 10;
    }
    [super setFrame:frame];
    
}

- (void)dealloc
{
    [_leftLine release], _leftLine = nil;
    [_rightLine release], _rightLine = nil;
    [_middleLine release], _middleLine = nil;
    [_typeSelectedView release], _typeSelectedView = nil;
    [_statusSelectedView release], _statusSelectedView = nil;
    [_upLabel release], _upLabel = nil;
    [_downLabel release], _downLabel = nil;
    [super dealloc];
}

@end
