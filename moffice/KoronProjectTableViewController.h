//
//  KoronProjectTableViewController.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-25.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "ASIFormDataRequest.h"
#import "SVProgressHUD.h"
#import "KoronEndRefreshView.h"
#import "KoronProjectCell.h"
#import "KoronProjectHeaderCell.h"

@interface KoronProjectTableViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource,EGORefreshTableHeaderDelegate,ASIHTTPRequestDelegate,KoronProjectCellDelegate,KoronProjectHeaderCellDelegate>


//开始重新加载时调用的方法
- (void)reloadTableViewDataSource;
//完成加载时调用的方法
- (void)doneLoadingTableViewData;
@end
