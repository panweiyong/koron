//
//  MailCell.h
//  moffice
//
//  Created by yangxi zou on 12-1-17.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonConst.h"

@interface Mail : UITableViewCell
{
    NSString *mid;
    NSString *bid;
    NSString *fid;
    NSString *from;
    BOOL intranet;
    BOOL hasattach;
    NSString *receivedTime;
    
    BOOL _highlighted;
}

@property (nonatomic,retain)NSString *mid;
@property (nonatomic,retain)NSString *bid;
@property (nonatomic,retain)NSString *fid;
@property (nonatomic,retain)NSString *from;
@property (nonatomic,retain)NSString *receivedTime;
@property (nonatomic)BOOL intranet;
@property (nonatomic)BOOL hasattach;



@end


@interface MailCell : Mail
{
    UIView *cellContentView;
}
@end