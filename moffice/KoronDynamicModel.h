//
//  KoronDynamicModel.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-29.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "BaseModel.h"

@interface KoronDynamicModel : BaseModel

@property (nonatomic, copy) NSString *commentId;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *createBy;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *nameId;   
@property (nonatomic, copy) NSString *replyId;

@end
