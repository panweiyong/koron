//
//  SMAlertView.m
//  ChildrenCalendar
//
//  Created by yangxi zou on 11-1-23.
//  Copyright 2011 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "SMAlertView.h"


@implementation SMView

//显示提示信息自动消失
+(UIView *)showAlert:(NSString*)msg
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, 120, 26)];
    label.text = msg;
    label.textColor = [UIColor whiteColor];
    label.textAlignment=UITextAlignmentCenter;
    //[label sizeToFit];
    label.font = [UIFont systemFontOfSize:14];
    label.backgroundColor = [UIColor clearColor];
    
    UIView *theView =  [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 80)] autorelease];
	//theView.autoresizingMask= ( UIViewAutoresizingFlexibleWidth||UIViewAutoresizingFlexibleLeftMargin|| UIViewAutoresizingFlexibleRightMargin ||UIViewAutoresizingFlexibleHeight);
    //theView.autoresizesSubviews = YES;
	//theView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	theView.backgroundColor = [UIColor blackColor];
    theView.alpha = 0.7;
    [theView addSubview:label];
	[label release];
	theView.layer.cornerRadius = 10;
    theView.layer.masksToBounds = YES;
    return theView;

}

+(UIActivityIndicatorView *)showProcessing
{
    CGRect frame = CGRectMake(0, 0, 32, 32);
    UIActivityIndicatorView* progressInd =  [[UIActivityIndicatorView alloc] initWithFrame:frame] ;
    [progressInd startAnimating];
    progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    return progressInd;
}

+(UIView *)showWaitingAlert
{
    CGRect frame = CGRectMake(90, 0, 32, 32);
    UIActivityIndicatorView* progressInd = [[UIActivityIndicatorView alloc] initWithFrame:frame];
    [progressInd startAnimating];
    progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
	
    frame = CGRectMake(130, 3, 140, 30);
    UILabel *waitingLable = [[UILabel alloc] initWithFrame:frame];
    waitingLable.text = @"Processing...";
    waitingLable.textColor = [UIColor whiteColor];
    waitingLable.font = [UIFont systemFontOfSize:20];
    waitingLable.backgroundColor = [UIColor clearColor];
	
    frame = [[UIScreen mainScreen] applicationFrame];
    UIView *theView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 80)]autorelease] ;
	//theView.autoresizingMask= ( UIViewAutoresizingFlexibleWidth||UIViewAutoresizingFlexibleLeftMargin|| UIViewAutoresizingFlexibleRightMargin ||UIViewAutoresizingFlexibleHeight);
    //theView.autoresizesSubviews = YES;
	//theView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	theView.backgroundColor = [UIColor blackColor];
    theView.alpha = 0.7;
    [theView addSubview:progressInd];
    [theView addSubview:waitingLable];
	
    [progressInd release];
    [waitingLable release];
	
    return theView;
}

@end
