//
//  KoronAddItemViewController.h
//  TodoListDemo
//
//  Created by Mac Mini on 13-11-20.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronChecklistItem.h"
#import "ASIFormDataRequest.h"
#import "KoronManager.h"
#import "SVProgressHUD.h"

@class KoronAddItemViewController;

@protocol KoronAddItemViewControllerDelegate <NSObject>

- (void)KoronAddItemViewController:(KoronAddItemViewController *)controller didFinishAddingItem:(KoronChecklistItem *)item;

- (void)KoronAddItemViewController:(KoronAddItemViewController *)controller didFinishEditingItem:(KoronChecklistItem *)item;

@end

@interface KoronAddItemViewController : UIViewController<UITextFieldDelegate, UIActionSheetDelegate>

@property (nonatomic, assign) id <KoronAddItemViewControllerDelegate> delegate;
@property (nonatomic, retain) KoronChecklistItem *itemToEdit;
@property (nonatomic, copy) NSString *selectedTime;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *itemId;

@end
