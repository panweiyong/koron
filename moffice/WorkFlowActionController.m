//
//  WorkFlowActionController.m
//  moffice
//
//  Created by yangxi zou on 12-1-14.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "WorkFlowActionController.h"
#import "FlowDetailCell.h"

@implementation WorkFlowActionController

@synthesize  aid,wid,acts,fixed,isCancel,ideaRequired,mstr;

-(void)dealloc
{
    [aid release];
    [wid release];
    [acts release];
    [selectcache release];
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        mstr=@"";
        self.title=NSLocalizedString(@"Workflow",@"Workflow") ; 
        if(selectcache==nil)selectcache=[[NSMutableDictionary alloc] init];
    }
    return self;
} 

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	if(section==1) 
    {
        if (acts!=nil&&[acts count  ]>0) {
            return  NSLocalizedString(@"select goto",@"select goto"); 
        }else{
            return @"";
        }
    }
    if(section==0) return  NSLocalizedString(@"workflow mean",@"workflow mean"); 	 
	return @"";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerViewBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 30)];
    headerViewBg.backgroundColor = [UIColor clearColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, 280, 30)];
    headerLabel.backgroundColor = [UIColor clearColor];

    if (section == 0) {
        
        headerLabel.text = NSLocalizedString(@"workflow mean",@"workflow mean");;
    }else if (section == 1){
        headerLabel.text = NSLocalizedString(@"select goto",@"select goto");
    }
    headerLabel.font = [UIFont boldSystemFontOfSize:16];
    [headerViewBg addSubview:headerLabel];
    [headerLabel release];
    return headerViewBg ;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section==1)return [acts count];
	return  1;
} 

-(NSString*)parseName:(NSString*)vv
{
    if(vv==nil)return @"";
    NSRange range = [vv rangeOfString:@"("];
    if (range.length<1) return @"";
    return [vv substringToIndex:range.location];
}
-(NSString*)parseUID:(NSString*)vv
{
    if(vv==nil)return @"";
    NSRange range = [vv rangeOfString:@"("];
    if (range.length<1) return @"";
    NSString  *tstr= [vv substringFromIndex:range.location+1];
    return [tstr substringToIndex: [tstr length]-1 ];
}

-(void)setcheckedStatus:(NSString*)s b:(BOOL)b
{
    [selectcache setObject:[NSNumber numberWithBool:b ]  forKey:s] ;
}
-(BOOL)getcheckedStatus:(NSString*)s
{
    NSNumber *n=[selectcache objectForKey: s ];
    return [n boolValue]  ;
}


- (UITableViewCell *)tableView:(UITableView *)tableViewz cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if (indexPath.section==1) {
         
	UITableViewCellStyle style = UITableViewCellStyleValue1; 
	NSString *cellid= @"tid";
	FlowDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
    if (!cell) 
    {
        cell = [[[FlowDetailCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
        cell.accessoryType = cell.accessoryType;//UITableViewCellAccessoryNone; 
    }
        cell.textLabel.text= [self parseName:[acts objectAtIndex:indexPath.row]];
        cell.detailTextLabel.text =  [self parseUID:[acts objectAtIndex:indexPath.row]];
        cell.detailTextLabel.hidden=YES;
    //cell.textLabel.backgroundColor  = [UIColor clearColor];
    //cell.detailTextLabel.backgroundColor  = [UIColor clearColor];
    //cell.imageView.image= [UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"tips_mail.png" ]  ];
        if (self.fixed){
           cell.accessoryType = UITableViewCellAccessoryCheckmark; 
           [self setcheckedStatus:[NSString stringWithFormat:@"%i",indexPath.row] b:YES];
        }else{
            if([self getcheckedStatus:[NSString stringWithFormat:@"%i",indexPath.row] ]){
                cell.accessoryType = UITableViewCellAccessoryCheckmark; 
            }else{
                cell.accessoryType = UITableViewCellAccessoryNone; 
            }
           
        }
        cell.selectionStyle =  UITableViewCellSelectionStyleBlue;
        [self cellBackgroundImage:cell withIndexPath:indexPath];
        return cell;
        
    }
    
    if (indexPath.section==0) {
        UITableViewCellStyle style = UITableViewCellStyleValue1; 
        NSString *cellid= @"tid1";
        FlowDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellid];
        if (!cell) 
        {
            cell = [[[FlowDetailCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
        }
        //cell = [[[UITableViewCell alloc] initWithStyle:style reuseIdentifier:cellid] autorelease];
        //[cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
         wfmean = [[UITextView alloc] initWithFrame:CGRectMake(5, 5, 280, 60)];
        wfmean.backgroundColor = [UIColor clearColor]; 
        wfmean.returnKeyType = UIReturnKeyGo;
        wfmean.delegate=self;  
        wfmean.tag=100;
        wfmean.autocorrectionType = UITextAutocorrectionTypeNo;
        wfmean.autocapitalizationType = UITextAutocapitalizationTypeNone;
        wfmean.returnKeyType = UIReturnKeyDone; 
        wfmean.text = self.mstr;
        //wfmean.keyboardType = UIKeyboardTypeURL; 
        [cell.contentView addSubview:wfmean]; 
         //[wfmean release];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone ;
        [self cellBackgroundImage:cell withIndexPath:indexPath];
        return cell;
    }
    return nil;
    
}
- (void)tableView:(UITableView *)tableViewz didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{   
    [tableViewz deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section!=1)return;
    if (self.fixed) {
        return;
    }
   
    UITableViewCell *oldCell = [tableView cellForRowAtIndexPath:indexPath]; 
    if (oldCell.accessoryType == UITableViewCellAccessoryCheckmark)
    {   
        oldCell.accessoryType = UITableViewCellAccessoryNone;   
        [self setcheckedStatus:[NSString stringWithFormat:@"%i",indexPath.row] b:NO];
        //
    } else{
        /*
        UITableViewCell *newCell = [tableView cellForRowAtIndexPath:indexPath];     
        
        if (newCell.accessoryType == UITableViewCellAccessoryNone) 
        {   
             newCell.accessoryType = UITableViewCellAccessoryCheckmark; 
        }  */
        [self setcheckedStatus:[NSString stringWithFormat:@"%i",indexPath.row] b:YES];
        oldCell.accessoryType = UITableViewCellAccessoryCheckmark; 
    } 
    //[self updatenumber];
}


- (CGFloat)tableView:(UITableView *)tableViewz heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ 	
    if(indexPath.section==0)return 70;
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if(section==1)return 70;
    return 0;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    mstr = [textView.text retain];
}

- (UIView *)tableView:(UITableView *)tableViewx viewForFooterInSection:(NSInteger)section {
	
    if(section==0)return nil;
	if(footerView == nil) {
        footerView  = [[UIView alloc] init];
        //UIImage *image = [[UIImage imageNamed:@"button_red.png"] stretchableImageWithLeftCapWidth:8 topCapHeight:8];
        //footerView.backgroundColor=[UIColor redColor];
        //[footerView setAutoresizingMask: UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth];
        //create the button
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        //[button setBackgroundImage:image forState:UIControlStateNormal];
        [button setFrame:CGRectMake(30, 3, 260, 40)];
        //set title, font size and font color
        UIImage *image =[[UIImage imageNamed:@"list_bg_group_single"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        [button setTitle:NSLocalizedString(@"approvalok",@"approvalok")  forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //[button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal]; 
        //[button setAutoresizingMask: UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth];
        //set action of the button
        [button addTarget:self action:@selector(doApproval) forControlEvents:UIControlEventTouchUpInside];
        button.center =  CGPointMake(   tableView.center.x,button.frame.origin.y + 30 );// self.tableView.center;
        [footerView addSubview:button];
	}
	return footerView;
}

-(NSString*)getTO
{
    NSString *rt=@"";
    //for (int i = 0; i < [acts count]; i++) {
        //NSIndexPath *tempIndex = [NSIndexPath indexPathForRow:i inSection:1];
        //UITableViewCell *tempCell = [tableView cellForRowAtIndexPath:tempIndex];
        //if (tempCell.accessoryType == UITableViewCellAccessoryCheckmark) {
        
            //CCLOG(@"%@",  tempCell.textLabel.text );
         //   rt= [rt stringByAppendingFormat:@"%@,", tempCell.detailTextLabel.text];
        //}
    //}
    for(NSString * sel in selectcache.allKeys )
    {
        NSNumber *m= [selectcache objectForKey:sel ] ;
        if(m.boolValue==YES){
            rt= [rt stringByAppendingFormat:@"%@,",  [self parseUID: [acts objectAtIndex:[sel intValue] ] ] ];
        }
    }
    //CCLOG(@"%@",  rt );
    return rt;
}

-(void)doApproval
{
    if([ModalAlert confirm:NSLocalizedString(@"Confirm Workflow",@"Confirm Workflow")])
    {
      //  UITextView* wfmean =(UITextView*) [tableView viewWithTag:100];
        //[ModalAlert prompt:[NSString stringWithFormat:@"ddd%i",wfmean.text.length]];
        if(self.ideaRequired)
        {
            if( wfmean.text.length == 0 )
            {
                [ModalAlert prompt:NSLocalizedString(@"Please Input Mean",@"Please Input Mean")];
                return;
            }
        }
        
        //CCLOG(@"%@",wfmean.text);
        NSString *to=[self getTO]; 
        if([acts count]>0){
        if(to==nil||[to isEqualToString:@""]){
            [ModalAlert prompt:NSLocalizedString(@"Please Select Action",@"Please Select Action")];
            return;
        }
        }
        JsonService *jservice=[JsonService sharedManager];
        [jservice setDelegate:self];
        [jservice approvalWorkflow:wid aid:aid to:to memo:wfmean.text iscancel:self.isCancel];
        
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text 
{
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

/*
 - (void)tableView:(UITableView *)tableViewx willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 }*/
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [self loadNavigationItem];
    CCLOG(@"%@,%@,%i,%i",wid,aid,[acts count],fixed); 
    mstr=@"";
    
    self.view.backgroundColor =  [UIColor colorWithPatternImage:[UIImage imageWithContentsOfFile:  [SMFileUtils fullBundlePath:@"tipsbg.png" ] ]];
    tableView = [[UITableView alloc]  initWithFrame:CGRectMake(0,0, self.view.bounds.size.width ,self.view.bounds.size.height  - self.tabBarController.tabBar.frame.size.height- self.navigationController.navigationBar.frame.size.height  ) style: UITableViewStyleGrouped];
    [tableView setDelegate:self]; 
    [tableView setDataSource:self]; 
    // This should be set to work with the image height 
    [tableView setRowHeight:45];
    tableView.showsHorizontalScrollIndicator= NO;
    tableView.showsVerticalScrollIndicator=YES;
    tableView.scrollEnabled=YES;
    tableView.indicatorStyle =UIScrollViewIndicatorStyleDefault ;
    [tableView setBackgroundColor:[UIColor colorWithRed:221.0/255.0 green:239.0/255.0 blue:248.0/255.0 alpha:1.0]]; 
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone]; 
    [tableView setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    //[tableView setAllowsMultipleSelection:YES];
    [self.view addSubview:tableView];
    [super viewDidLoad];
        
}
//异步接收到返回的数据
- (void)requestDataFinished:(id)jsondata;//request
{
    //tableData=[jsondata retain];
    //[super requestDataFinished:jsondata];
    
    //更新标记未已读
    //JsonService *jservice=[JsonService sharedManager];
    //[jservice setDelegate:self];
    //[jservice UpdateFlag:type oid:refid flag:1];
    
}

-(void)updatenumber
{
    
    int c=  [self.tabBarItem.badgeValue intValue] -1;
    if(c>0)
    {
        self.tabBarItem.badgeValue=[NSString stringWithFormat:@"%i",c];  
    }else{
        self.tabBarItem.badgeValue=nil;  
    }
}

- (void)postFinished:(id)jsondata
{
    //CCLOG(@"postFinished");
    ProcessResult r = ProcessResultMake([[jsondata objectForKey:@"code"] intValue],[jsondata objectForKey:@"desc"] );
    if (r.code==0) {
        /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",@"Error")
         message:r.desc
         delegate:nil
         cancelButtonTitle:NSLocalizedString(@"Op Error",@"Op Error")
         otherButtonTitles:nil];
         [alertView show];
         [alertView release];*/
        mstr =  @""  ;
        [ModalAlert prompt:[NSString stringWithFormat:@"%@\r%@",NSLocalizedString(@"Op Error",@"Op Error") ,r.desc]   ];
    }else{
        //[self performSelectorOnMainThread:@selector(updatenumber) withObject:self waitUntilDone:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateBadgeNumber" object:nil userInfo:nil];
        /**/
        //[LoginViewController refershTipsNumber];
        [ModalAlert prompt:NSLocalizedString(@"Op Success",@"Op Success")];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

- (void)cellBackgroundImage:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath {
    if (IOS_VERSIONS > 6.9) {
        
        cell.backgroundColor = [UIColor clearColor];
    }
    if (indexPath.section == 0 &&IOS_VERSIONS > 6.9 ) {
        UIImage *image =[[UIImage imageNamed:@"list_bg_group_single"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
        imageView.image = image;
        cell.backgroundView = imageView;
    }else {
        if (indexPath.section == 1 &&IOS_VERSIONS > 6.9) {
            if ([acts count] == 1) {
                UIImage *image =[[UIImage imageNamed:@"list_bg_group_single"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                imageView.image = image;
                cell.backgroundView = imageView;
            }else if (indexPath.row == 0) {
                UIImage *image =[[UIImage imageNamed:@"list_bg_group_top"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                imageView.image = image;
                cell.backgroundView = imageView;
            }else if (indexPath.row == [acts count] - 1) {
                UIImage *image =[[UIImage imageNamed:@"list_bg_group_bottom"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                imageView.image = image;
                cell.backgroundView = imageView;
            }else {
                UIImage *image =[[UIImage imageNamed:@"list_bg_group_middle"] stretchableImageWithLeftCapWidth:5 topCapHeight:5];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
                imageView.image = image;
                cell.backgroundView = imageView;
            }
        }
    }
}

- (void)loadNavigationItem {
    UIView *dismissButtonBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if (IOS_VERSIONS >6.9) {
        dismissButton.frame = CGRectMake(-10, 0, 40, 40);
    } else {
        dismissButton.frame = CGRectMake(0, 0, 40, 40);
    }
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [dismissButtonBackgroundView addSubview:dismissButton];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:dismissButtonBackgroundView];
    [dismissButtonBackgroundView release];
    self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
}

- (void)setTitle:(NSString *)title
{
    [super setTitle:title];
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont systemFontOfSize:18.0];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        titleView.textColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        [titleView release];
    }
    titleView.text = title;
    [titleView sizeToFit];
}

- (void)backButtonPressed {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

@end
