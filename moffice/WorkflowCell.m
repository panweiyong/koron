//
//  WorkflowCell.m
//  moffice
//
//  Created by yangxi zou on 12-1-12.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "WorkflowCell.h"

@interface HybridSubviewBasedApplicationCellContentView : UIView
{
    Workflow *_cell;
    BOOL _highlighted;
}

@end

@implementation HybridSubviewBasedApplicationCellContentView

- (id)initWithFrame:(CGRect)frame cell:(Workflow *)cell
{
    if (self = [super initWithFrame:frame])
    {
        _cell = cell;
        self.opaque = YES;
        self.backgroundColor = [UIColor clearColor];//_cell.backgroundColor;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
	 _highlighted ? [[UIColor whiteColor] set] : [[UIColor colorWithWhite:0.6 alpha:1] set];    
    
    [_cell.type drawAtPoint:CGPointMake(35.0, 3.0) withFont:[UIFont boldSystemFontOfSize:12.0]];
    
    CGSize size = CGSizeMake(120,200);  
    
    //[[NSString stringWithFormat:@"%@-%@",_cell.type,_cell.currentNode] drawAtPoint:CGPointMake(35.0, 3.0) withFont:[UIFont boldSystemFontOfSize:11.0]];
    
    //CCLOG(@"flag:%i %@",_cell.flag,_cell.createTime );
    
    //[_cell.type drawAtPoint:CGPointMake(60.0, 3.0) withFont:[UIFont boldSystemFontOfSize:11.0]];
    
    [_cell.creator drawAtPoint:CGPointMake(35.0, 50.0) withFont:[UIFont boldSystemFontOfSize:12.0]];
    
    
    CGSize labelsize = [_cell.createTime sizeWithFont:[UIFont boldSystemFontOfSize:12.0] constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping];
    
    [_cell.createTime drawAtPoint:CGPointMake(300.0f-labelsize.width, 50.0) withFont:[UIFont boldSystemFontOfSize:12.0]];
    
    
    _highlighted ? [[UIColor whiteColor] set] : [[UIColor colorWithRed:0.01f green:0.42f blue:0.02 alpha:1] set];    
    
    CGSize labelsize0 = [_cell.currentNode sizeWithFont:[UIFont boldSystemFontOfSize:12.0] constrainedToSize:size lineBreakMode:NSLineBreakByCharWrapping];
    [_cell.currentNode drawAtPoint:CGPointMake(300.0f-labelsize0.width, 3.0) withFont:[UIFont boldSystemFontOfSize:12.0]];
    
    //[[NSString stringWithFormat:@"%@(%@)",_cell.creator,_cell.createTime] drawAtPoint:CGPointMake(60.0, 50.0) withFont:[UIFont boldSystemFontOfSize:11.0]];
    
	/*
     CGPoint ratingImageOrigin = CGPointMake(81.0, 45.0);
     UIImage *ratingBackgroundImage = [UIImage imageNamed:@"StarsBackground.png"];
     [ratingBackgroundImage drawAtPoint:ratingImageOrigin];
     UIImage *ratingForegroundImage = [UIImage imageNamed:@"StarsForeground.png"];
     UIRectClip(CGRectMake(ratingImageOrigin.x, ratingImageOrigin.y, ratingForegroundImage.size.width * (_cell.rating / MAX_RATING), ratingForegroundImage.size.height));
     [ratingForegroundImage drawAtPoint:ratingImageOrigin];
	 */
}

- (void)setHighlighted:(BOOL)highlighted
{
    _highlighted = highlighted;
    [self setNeedsDisplay];
}

- (BOOL)isHighlighted
{
    return _highlighted;
}

@end

@implementation Workflow
@synthesize oid,createTime,creator,title,type,currentNode,flag,cancelEnabled ;

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //画渐变
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 1.0, 1.0, 1.0, 0.35,  // Start color
        1.0, 1.0, 1.0, 0.06 }; // End color
    
    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
    
    CGRect currentBounds = self.bounds;
    CGPoint topCenter = CGPointMake(CGRectGetMidX(currentBounds), 0.0f);
    CGPoint midCenter = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetMidY(currentBounds));
    CGContextDrawLinearGradient(context, glossGradient, topCenter, midCenter, 0);
    
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace); 
    
    //开始一条线
    CGContextSaveGState(context);
    CGContextSetStrokeColorWithColor(context,WFELLLINE_BACKGROUND );
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0.0, 71.0);
    CGContextAddLineToPoint(context, 320.0, 71.0);
    CGContextStrokePath(context);

    CGContextSetRGBStrokeColor(context, 251.0f/255.0f, 251.0f/255.0f,252.0f/255.0f, 1.0);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0.0, 72.0);
    CGContextAddLineToPoint(context, 320.0, 72.0);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    /*
    if (flag==0) {
        UIImage *img=[UIImage imageWithContentsOfFile:[SMFileUtils fullBundlePath:@"unread.png" ]];
        [img  drawAtPoint:CGPointMake(81.0, 45.0)];    
    } */
}
- (void)layoutSubviews {
    [super layoutSubviews];
    //self.imageView.frame = CGRectMake(5,5,32,32);
    //  self.imageView.layer.cornerRadius = 5;
    //  self.imageView.layer.masksToBounds = YES;
    //  [self.imageView.layer setShadowOpacity:0.6];
    // [self.imageView.layer setShadowOffset:CGSizeMake(1, 1)];
    //  [self.imageView.layer setShadowOffset:CGSizeMake(0.5f, 0.5f)];
    //  [self.imageView.layer setShadowPath:[[UIBezierPath bezierPathWithRoundedRect:[self bounds]cornerRadius:12.0f] CGPath]];
    //self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}
- (void)dealloc
{
    [oid release];
    [createTime release];
    [creator release];
    [title release];
    [type release];
    [currentNode release]; 
    [super dealloc];
}
 
@end
 

@implementation WorkflowCell

- (void)dealloc
{
    [cellContentView release];
    [super dealloc];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])
    {
        cellContentView = [[HybridSubviewBasedApplicationCellContentView alloc] initWithFrame:CGRectInset(self.contentView.bounds, 0.0, 1.0) cell:self];
        cellContentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        cellContentView.contentMode = UIViewContentModeLeft;
        [self.contentView addSubview:cellContentView];        
    }
    
    return self;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
  //  cellContentView.backgroundColor = backgroundColor;
}



@end
