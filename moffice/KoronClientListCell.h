//
//  KoronClientListCell.h
//  moffice
//
//  Created by Mac Mini on 13-11-11.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronClientListModel.h"

@protocol KoronClentListDelegate <NSObject>

- (void)didSelectedPhoneNum:(NSString *)phoneNum;

- (void)merchandiserPushWithName:(NSString *)clientName andCustomerID:(NSString *)customerID;

@end


@interface KoronClientListCell : UITableViewCell
{
@private
    UILabel *_clientLabel;
    UILabel *_contactLabel;
    UILabel *_phoneLabel;
    UIImageView *_merchandiserView;
    id <KoronClentListDelegate> _delegate;
}

@property (nonatomic, retain, readonly) UILabel *clientLabel;
@property (nonatomic, retain, readonly) UILabel *contactLabel;
@property (nonatomic, retain, readonly) UILabel *phoneLabel;
@property (nonatomic, retain) KoronClientListModel *model;
@property (nonatomic, assign) id <KoronClentListDelegate> delegate;
@property (nonatomic, copy) NSString *customerID;

@end
