//
//  KoronToDoListModel.h
//  TodoListDemo
//
//  Created by Mac Mini on 13-11-20.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface KoronChecklistItem : BaseModel<NSCoding>

@property (nonatomic, copy) NSString *row;
@property (nonatomic, copy) NSString *itemId;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *time;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *typeColor;
@property (nonatomic, assign) BOOL checked;


- (void)toggleChecked;

@end
