//
//  WorkFlowActionController.h
//  moffice
//
//  Created by yangxi zou on 12-1-14.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CommonConst.h"
#import "SMUtils.h"
#import "ModalAlert.h"
#import "JsonService.h"
#import "SMAlertView.h"

@interface WorkFlowActionController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
     NSString *aid;
     NSString *wid;
     NSArray *acts;
     UITableView *tableView;
     BOOL isCancel;
     //int select; 
     BOOL fixed;
     UIView *footerView;
     NSMutableDictionary *selectcache;
     BOOL ideaRequired ;
     UITextView* wfmean;
    NSString *mstr;
}
@property (nonatomic,retain)NSString *aid;
@property (nonatomic,retain)NSString *wid;
@property (nonatomic,retain)NSArray *acts;
@property (nonatomic,retain)NSString *mstr;
@property (nonatomic)BOOL fixed;
@property (nonatomic)BOOL isCancel;
@property (nonatomic)BOOL ideaRequired;
@end
