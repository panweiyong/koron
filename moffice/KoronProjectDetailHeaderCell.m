//
//  KoronProjectDetailHeaderCell.m
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-27.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronProjectDetailHeaderCell.h"
#import <QuartzCore/QuartzCore.h>

@interface KoronProjectDetailHeaderCell ()

@end

@implementation KoronProjectDetailHeaderCell
{
    UIView *_frameView;
    UILabel *_chargeLabel;
    UILabel *_timeLabel;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}

- (void)initSubviews
{
    _frameView = [[UIView alloc] initWithFrame:CGRectZero];
    _frameView.layer.borderWidth = 1;
    _frameView.layer.borderColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1].CGColor;
    [self.contentView addSubview:_frameView];
    
    //状态
    _statusPicture = [[UIImageView alloc] initWithFrame:CGRectZero];
    _statusPicture.backgroundColor = [UIColor grayColor];
    _statusPicture.image = [UIImage imageNamed:@"items_r.png"];
    [self.contentView addSubview:_statusPicture];
    
    //标题
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _titleLabel.textColor = [UIColor colorWithRed:47/255.0 green:146/255.0 blue:1 alpha:1];
    _titleLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_titleLabel];
    
    //负责人
    _chargeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _chargeLabel.font = [UIFont systemFontOfSize:10];
    _chargeLabel.text = @"负责人:";
    _chargeLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_chargeLabel];
    
    _executorLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _executorLabel.font = [UIFont systemFontOfSize:10];
    _executorLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_executorLabel];
    
    //剩余时间
    _remainingTimeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _remainingTimeLabel.textAlignment = NSTextAlignmentRight;
    _remainingTimeLabel.font = [UIFont boldSystemFontOfSize:12];
    _remainingTimeLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_remainingTimeLabel];
    
    _timeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _timeLabel.text = @"时间:";
    _timeLabel.font = [UIFont systemFontOfSize:10];
    _timeLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_timeLabel];
    
    _detailTimeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _detailTimeLabel.font = [UIFont systemFontOfSize:10];
    _detailTimeLabel.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_detailTimeLabel];
    
    //介绍
    _introduceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _introduceLabel.backgroundColor = [UIColor clearColor];
    _introduceLabel.font = [UIFont systemFontOfSize:12];
    _introduceLabel.text = @"介绍:";
    [self.contentView addSubview:_introduceLabel];
    
    //内容
    _detailContentLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _detailContentLabel.backgroundColor = [UIColor clearColor];
    _detailContentLabel.font = [UIFont systemFontOfSize:12];
    _detailContentLabel.numberOfLines = 0;
    [self.contentView addSubview:_detailContentLabel];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _frameView.frame = CGRectMake(0, 0, self.width, self.height);
    
    _statusPicture.frame = CGRectMake(10, 10, 50, 50);
    _titleLabel.frame = CGRectMake(_statusPicture.right + 10, _statusPicture.top, 220, 20);
    _chargeLabel.frame = CGRectMake(_statusPicture.right + 10, _titleLabel.bottom + 10, 40, 10);
    _executorLabel.frame = CGRectMake(_chargeLabel.right, _chargeLabel.top, 100, _chargeLabel.height);
    _timeLabel.frame = CGRectMake(_chargeLabel.left, _chargeLabel.bottom+10, _chargeLabel.width, _chargeLabel.height);
    _detailTimeLabel.frame = CGRectMake(_timeLabel.right, _timeLabel.top, 150, _timeLabel.height);
    
    _remainingTimeLabel.frame = CGRectMake(self.width - 100, _chargeLabel.top, 90, 15);
    
    _introduceLabel.frame = CGRectMake(_statusPicture.left, _statusPicture.bottom + 20, 30, _chargeLabel.height);
    _detailContentLabel.frame = CGRectMake(_introduceLabel.right, _introduceLabel.top, 240, 1000);
    [_detailContentLabel sizeToFit];
    _detailContentLabel.center = CGPointMake(_introduceLabel.right+_detailContentLabel.width/2.0 + 10,  _introduceLabel.top + _detailContentLabel.height/2.0);

}

- (void)setFrame:(CGRect)frame
{
    frame.origin.x += 10;
    frame.size.width -= 20;
    [super setFrame:frame];
    
}

- (void)dealloc
{
    self.statusPicture = nil;
    self.executorLabel = nil;
    self.titleLabel = nil;
    self.remainingTimeLabel = nil;
    self.introduceLabel = nil;
    self.detailContentLabel = nil;
    
    [super dealloc];
}

@end
