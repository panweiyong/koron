//
//  KoronLoginView.m
//  KoronMoffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronLoginView.h"

@implementation KoronLoginView
@synthesize userNameTextField = _userNameTextField;
@synthesize passwordTextField = _passwordTextField;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _manager = [KoronManager sharedManager];
        
        UILabel *userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 5, 40, 30)];
        userNameLabel.font = [UIFont systemFontOfSize:16];
        userNameLabel.text = @"账号:";
        userNameLabel.textColor = [UIColor grayColor];
        userNameLabel.textAlignment = NSTextAlignmentLeft;
        userNameLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:userNameLabel];
        
        UILabel *passWordLabel = [[UILabel alloc]initWithFrame:CGRectMake(14, 45, 40, 30)];
        passWordLabel.font = [UIFont systemFontOfSize:16];
        passWordLabel.text = @"密码:";
        passWordLabel.textColor = [UIColor grayColor];
        passWordLabel.textAlignment = NSTextAlignmentLeft;
        passWordLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:passWordLabel];
        
        _userNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(60, 5, 180, 30)];
        _userNameTextField.keyboardType = UIKeyboardTypeEmailAddress;
        _userNameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _userNameTextField.returnKeyType = UIReturnKeyNext;
        _userNameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _userNameTextField.delegate = self;
//        _userNameTextField.backgroundColor = [UIColor redColor];
        _userNameTextField.font = [UIFont systemFontOfSize:20];
        _userNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self addSubview:_userNameTextField];
        
        _passwordTextField = [[UITextField alloc]initWithFrame:CGRectMake(60, 45, 180, 30)];
        _passwordTextField.delegate = self;
        _passwordTextField.secureTextEntry = YES;
        _passwordTextField.keyboardType = UIKeyboardTypeEmailAddress;
        _passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//        _passwordTextField.backgroundColor = [UIColor orangeColor];
        _passwordTextField.font = [UIFont systemFontOfSize:20];
        _passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self addSubview:_passwordTextField];
        
        if ([_manager getObjectForKey:USERNAME]) {
            _userNameTextField.text = [_manager getObjectForKey:USERNAME];
        }
        
        if ([[_manager getObjectForKey:REMEMBERPASSWORD]isEqualToString:@"YES"]) {
            _passwordTextField.text = [_manager getObjectForKey:PASSWORD];
        }
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)drawRect:(CGRect)rect {
    CGRect frame = self.frame;
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
    CGContextSetAlpha(ctx, 0.2);
    CGContextSetLineWidth(ctx, 0.5);
    
    CGContextMoveToPoint(ctx, 3, frame.size.height / 2 );
    CGContextAddLineToPoint(ctx, frame.size.width - 3 , frame.size.height / 2 );
    
    CGContextStrokePath(ctx);
}



#pragma UITextFiledDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.returnKeyType == UIReturnKeyNext) {
        [_passwordTextField becomeFirstResponder];
    }else {
        
    }
    return YES;
}



@end
