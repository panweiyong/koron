//
//  KoronStartAnimationViewController.m
//  moffice
//
//  Created by Mac Mini on 13-9-26.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronStartAnimationViewController.h"

@interface KoronStartAnimationViewController ()

@end

@implementation KoronStartAnimationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.frame = self.view.superview.frame;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.userInteractionEnabled = NO;
    
    UIImageView *backgroundView = [[UIImageView alloc]initWithFrame:self.view.frame];
    [backgroundView setImage:[UIImage imageNamed:@"splash_bg.png"]];
    [self.view addSubview:backgroundView];
    
    
    
    UIImageView *koronView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width / 2 + 30, 150)];
    koronView.center = CGPointMake(self.view.frame.size.width / 2 , self.view.frame.size.height / 2);
    [koronView setImage:[UIImage imageNamed:@"all_in_one.png"]];
    [self.view addSubview:koronView];
    
    _animationImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 50, 0, 100, 100)];
//        _animationImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 50, 100, 100, 100)];
    [_animationImageView setImage:[UIImage imageNamed:@"logo.png"]];
    [self.view addSubview:_animationImageView];
    
    //ios7以下操作系统UI有问题,需要调整
    if (IOS_VERSIONS < 7.0) {
        [backgroundView setFrame:CGRectMake(0, -20, self.view.bounds.size.width, [[UIScreen mainScreen] bounds].size.height)];
        [_animationImageView setFrame:CGRectMake(self.view.frame.size.width / 2 - 50, -20, 100, 100)];
    }
    
    [self performSelector:@selector(moveAnimationImageView) withObject:self afterDelay:0.5];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)moveAnimationImageView {
	SKBounceAnimation *bounceAnimation = [SKBounceAnimation animationWithKeyPath:@"position.y"];
	bounceAnimation.fromValue = [NSNumber numberWithFloat:_animationImageView.center.y];
    bounceAnimation.toValue = [NSNumber numberWithFloat:130];
	bounceAnimation.duration = 2.0f;
	bounceAnimation.delegate = self;
	bounceAnimation.numberOfBounces = 1;
	bounceAnimation.shouldOvershoot = NO;
	bounceAnimation.removedOnCompletion = YES;
	bounceAnimation.fillMode = kCAFillModeForwards;
    
	[_animationImageView.layer addAnimation:bounceAnimation forKey:@"someKey"];
    
    [self performSelector:@selector(removeAnimationImageView) withObject:self afterDelay:1];
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:1.0f];
//    [UIView setAnimationRepeatCount:60];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(removeAnimationImageView)];
//    
//    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:_animationImageView cache:YES];
//    
//    [UIView commitAnimations];
}

//动画执行结束,发送通知移除动画
- (void)removeAnimationImageView {
    [UIView animateWithDuration:1 animations:^{
        [self.view setAlpha:0];
    } completion:^(BOOL finished) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeAnimationImageView" object:nil];
    }];
}


@end
