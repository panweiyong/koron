//
//  KoronLoginViewController.h
//  Koron-mofffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//



#import "KoronSuperclassViewController.h"
#import "KoronLoginView.h"
#import "KoronChooseView.h"
#import "KoronServerView.h"
#import "ASIFormDataRequest.h"
#import "KoronManager.h"
#import "SBJsonParser.h"
#import "SVProgressHUD.h"
#import "KoronStartAnimationViewController.h"
#import "KoronRootViewController.h"


@interface KoronLoginViewController : UIViewController <ASIHTTPRequestDelegate ,UITextFieldDelegate> {
    UIImageView *_backgroundView;
    //账号,密码框
    KoronLoginView *_loginView;
    
    KoronServerView *_serverView;
    
    CGRect _rect;
    
    UIButton *_loginButton;
    
    KoronChooseView *_automaticLoginView;
    
    KoronChooseView *_rememberPasswordView;
    
    //登陆请求
    ASIFormDataRequest *_loginRequest;

    KoronManager *_manager;
    
    KoronStartAnimationViewController *_startAnimationView;
    
    BOOL isKeyBoardHide;
    
}

@end
