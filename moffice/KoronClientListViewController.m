//
//  KoronClientListViewController.m
//  moffice
//
//  Created by Mac Mini on 13-11-6.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientListViewController.h"
#import "KoronClientListModel.h"
#import "KoronClientListCell.h"
#import "KoronClientInfoViewController.h"
#import "KoronNewClientViewController.h"
#import "KoronClientModuleModel.h"
#import "merchandiserViewController.h"
#import "FMDatabase.h"

@interface KoronClientListViewController ()

-(void)addTableView;

@end

@implementation KoronClientListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _manager = [KoronManager sharedManager];
        
        NSString *sidString = [NSString stringWithFormat:@"IsNotFirstGetClientList_%@",[_manager getObjectForKey:SID]];
        _isNotFirstGetClientList = [[[NSUserDefaults standardUserDefaults] objectForKey:sidString] boolValue];

    }
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
    
    if (IOS_VERSIONS > 6.9) {
        
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self loadNavigationItems];

    if ([_synbutton.titleLabel.text isEqualToString:@"添加"]) {
        if (_isNotFirstGetClientList) {
            
            _isLoadFromDataBase = YES;
//            [self beginSendRequst];
            [self loadFromDataBase];
            
        }else {
            
            _lastUpdateTime = @"";
            [self sendClientListRequest];
            
        }
        
    }
    
    
    [self sendModuleRequest];

    _tableView.delegate = self;
    _tableView.dataSource = self;

    _endView = [[KoronEndRefreshView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50)];

    _topButton = [[UIButton alloc]initWithFrame:CGRectMake(90, 0, 120, _topView.bounds.size.height)];
    [_topButton setTitle:@"默认模版" forState:UIControlStateNormal];
    [_topButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [_topButton addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:_topButton];
    _topView.userInteractionEnabled = YES;

    _topImageView = [[UIImageView alloc]initWithFrame:CGRectMake(_topButton.frame.origin.x + _topButton.frame.size.width - 20, _topView.frame.size.height /2 - 11, 20, 20)];
    UITapGestureRecognizer *topImageViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showMenu)];
    [_topImageView addGestureRecognizer:topImageViewTap];
    [_topImageView setImage:[UIImage imageNamed:@"news_down.png"]];
    [_topView addSubview:_topImageView];

    UIImageView *headerView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, _topView.bottom, kDeviceWidth, 40)] autorelease];
    headerView.userInteractionEnabled = YES;
    UIImage *image = [[UIImage imageNamed:@"customer_bg"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5,5)];
    headerView.image = image;
    [self.view addSubview:headerView];
    
    UIImageView *searchClientBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 240, 30)];
    searchClientBackgroundView.userInteractionEnabled = YES;
    UIImage *fieldImage = [[UIImage imageNamed:@"search_edit_bg.9.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    searchClientBackgroundView.image = fieldImage;

    _searchClientField = [[UITextField alloc] initWithFrame:CGRectMake(5, 0, 230, 30)];
    _searchClientField.delegate = self;
    _searchClientField.placeholder = @"搜你所关注的客户";
    _searchClientField.backgroundColor = [UIColor clearColor];
    _searchClientField.font = [UIFont systemFontOfSize:16];
    _searchClientField.returnKeyType = UIReturnKeyDone;
    [searchClientBackgroundView addSubview:_searchClientField];
    
    [headerView addSubview:searchClientBackgroundView];
    [searchClientBackgroundView release];

    UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    searchButton.frame = CGRectMake(searchClientBackgroundView.right+10, searchClientBackgroundView.top, 50, searchClientBackgroundView.height);
    [searchButton setBackgroundImage:[UIImage imageNamed:@"client_saerch_blue_normal.9.png"] forState:UIControlStateNormal];
    searchButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [searchButton setImage:[UIImage imageNamed:@"customer_search_bt"] forState:UIControlStateHighlighted];
    [searchButton setImage:[UIImage imageNamed:@"customer_search_bt_pressed"] forState:UIControlStateNormal];
    [searchButton addTarget:self action:@selector(searchClient) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:searchButton];

    _tableView.frame = CGRectMake(0, headerView.bottom, kDeviceWidth, kDeviceHeight-20-44-40);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    _tableView.tag = 100;
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
}

//重写父类方法,用来下拉刷新数据
- (void)beginSendRequst {
    _lastmid = @"";
    isEmpty = YES;
    
    if (_isNotFirstGetClientList) {
        [self loadFromDataBase];
    }
    
    if (_isModuleListShow == NO) {
        
        if (_clientLists != nil &&_clientLists.count != 0) {
            NSMutableArray *lastTimeArray = [[NSMutableArray alloc] initWithArray:_clientLists];
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"lastUpdateTime" ascending:NO];
            [lastTimeArray sortUsingDescriptors:[NSArray arrayWithObject:sort]];
            self.lastUpdateTime = [lastTimeArray[0] lastUpdateTime];
            [lastTimeArray release];
        }
        
        [self sendClientListRequest];

    }else {
        
        [self sendModuleListRequestWithModuleId:_type];
    }
}

- (NSString *)getNowDate
{
    //2013-11-05 17:13:40
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *nowDate = [NSDate date];
    NSString *day = [dateFormatter stringFromDate:nowDate];
    [dateFormatter release];
    return day;
}

//重写父类方法,用来上拉刷新数据
- (void)scrollToTheEnd {
    if (isShowEndRefresh && _isModuleListShow) {
        [self sendModuleListRequestWithModuleId:_type];
        [_endView refreshStartAnimation];
        [_endView setTitle:@"加载中.."];
    }
}

#pragma mark - Private Method
- (void)loadNavigationItems
{
    UIButton *dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.frame = CGRectMake(10, 2, 40, 40);
    [dismissButton setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:dismissButton];
    
    _synbutton = [[UIButton alloc] initWithFrame:CGRectMake(270, 7, 40, 30)];
    UIImage *image = [[UIImage imageNamed:@"btn_big_gray"] stretchableImageWithLeftCapWidth:5 topCapHeight:10];
    [_synbutton setBackgroundImage:image forState:UIControlStateNormal];
    if (_isNotFirstGetClientList) {
        [_synbutton setTitle:@"添加" forState:UIControlStateNormal];
    }else {
        [_synbutton setTitle:@"同步" forState:UIControlStateNormal];
    }
    [_synbutton.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [_synbutton addTarget:self action:@selector(addMoreClient) forControlEvents:UIControlEventTouchUpInside];
    [_synbutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [_topView addSubview:_synbutton];
}

-(void)addTableView
{
    UIView *mainScreen=[[UIView alloc]initWithFrame:CGRectMake(0, 0, kDeviceWidth, kDeviceHeight)];
    [mainScreen setBackgroundColor:[UIColor colorWithRed:85.0/255.0 green:89.0/255.0 blue:92.0/255.0 alpha:0.5]];
    mainScreen.tag=20;
    [self.view addSubview:mainScreen];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeMaskViewAndTableView)];
    [mainScreen addGestureRecognizer:tap];
    [tap release];
    
    UITableView *outTable=[[UITableView alloc]initWithFrame:CGRectMake(10, self.view.frame.size.height /2 - 110, 300, 180) style:UITableViewStylePlain];
    [outTable setBackgroundColor:[UIColor whiteColor]];
    [outTable setBackgroundView:nil];
    outTable.layer.cornerRadius=5.0f;
    outTable.tag=11;
    outTable.delegate=self;
    outTable.dataSource=self;
    [self.view addSubview:outTable];
    
    if ([outTable respondsToSelector:@selector(setSeparatorInset:)]) {
        [outTable setSeparatorInset:UIEdgeInsetsZero];
    }
    
}

#pragma mark - Target Action
- (void)addMoreClient
{
    
    if (_isNotFirstGetClientList) {
        KoronNewClientViewController *newClientViewController = [[KoronNewClientViewController alloc] init];
        [self.navigationController pushViewController:newClientViewController animated:NO];
        [newClientViewController release];
    }else {
        [SVProgressHUD showWithStatus:@"加载中"];
        [_synbutton setTitle:@"添加" forState:UIControlStateNormal];
        _lastUpdateTime = @"";
        [self sendClientListRequest];
    }
    
}

- (void)searchClient
{
    NSLog(@"搜索客户");
    
    [_searchClientField resignFirstResponder];
    [self searchClientFromDataBaseWithModuleId:_type];
}

- (void)backButtonPressed {
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

- (void)removeMaskViewAndTableView
{
    for(UIView *view in [self.view subviews])
    {
        if(view.tag==20||view.tag==11)
        {
            [view removeFromSuperview];
            [view release];
        }
    }
}

#pragma mark - HttpRequest

//op=clientDet&lastUpdateTime=2013-11-05 17:13:40&sid=3832bdf5_0912211631568490003
- (void)sendClientListRequest {
    if (_clientListRequest) {
        [_clientListRequest clearDelegatesAndCancel];
        _clientListRequest = nil;
    }
    
    _clientListRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[_manager getObjectForKey:SERVERURL]]];

    _clientListRequest.delegate = self;
    [_clientListRequest setDidFinishSelector:@selector(clientListRequestFinish:)];
    [_clientListRequest setDidFailSelector:@selector(clientListRequestFail:)];

    [_clientListRequest setPostValue:@"clientDet" forKey:@"op"];
    [_clientListRequest setPostValue:_lastUpdateTime forKey:@"lastUpdateTime"];
    [_clientListRequest setPostValue:[_manager getObjectForKey:SID] forKey:SID];
    
//    [_clientListRequest setPostValue:@"listTips" forKey:@"op"];
//    [_clientListRequest setPostValue:_status forKey:@"status"];
//    [_clientListRequest setPostValue:_type forKey:@"type"];
//    [_clientListRequest setPostValue:_lastmid forKey:@"lastmid"];
    
    [_clientListRequest startAsynchronous];
}

- (void)sendModuleRequest
{
    if (_moduleRequest) {
        [_moduleRequest clearDelegatesAndCancel];
        _moduleRequest = nil;
    }
    
    _moduleRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[_manager getObjectForKey:SERVERURL]]];
    _moduleRequest.delegate = self;
    [_moduleRequest setDidFinishSelector:@selector(moduleRequestFinish:)];
    [_moduleRequest setDidFailSelector:@selector(moduleRequestFail:)];
    
    [_moduleRequest setPostValue:@"clientModule" forKey:@"op"];
    [_moduleRequest setPostValue:[_manager getObjectForKey:SID] forKey:SID];
    
    [_moduleRequest startAsynchronous];
}

- (void)moduleRequestFinish:(ASIFormDataRequest *)response
{
    NSLog(@"module: %@",[response responseString]);
    
    NSData *data = [response responseData];
    
    NSArray *moduleList = [[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil] objectForKey:@"moduleList"];
    
    if (_moduleArray == nil) {
        _moduleArray = [[NSMutableArray alloc] initWithCapacity:moduleList.count];
    }else if (_moduleArray.count != 0) {
        [_moduleArray removeAllObjects];
    }
    
    for (NSDictionary *dic in moduleList) {
        KoronClientModuleModel *moduleModel = [[KoronClientModuleModel alloc] initWithContent:dic];
        [_moduleArray addObject:moduleModel];
        [moduleModel release];
    }
}

- (void)moduleRequestFail:(ASIFormDataRequest *)response
{
    [super doneLoadingTableViewData];
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
    [_endView setTitle:@"加载更多.."];
    [_endView refreshStopAnimation];
}

- (void)sendModuleListRequestWithModuleId:(NSString *)moduleId
{
    if (_moduleListRequest) {
        [_moduleListRequest clearDelegatesAndCancel];
        _moduleListRequest = nil;
    }
    
    _moduleListRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:[_manager getObjectForKey:SERVERURL]]];
    _moduleListRequest.delegate = self;
    [_moduleListRequest setDidFinishSelector:@selector(moduleListRequestFinish:)];
    [_moduleListRequest setDidFailSelector:@selector(moduleListRequestFail:)];
    
    [_moduleListRequest setPostValue:@"ModuleClient" forKey:@"op"];
    [_moduleListRequest setPostValue:moduleId forKey:@"moduleId"];
    [_moduleListRequest setPostValue:_lastmid forKey:@"lastId"];
    [_moduleListRequest setPostValue:[_manager getObjectForKey:SID] forKey:SID];
    
    [_moduleListRequest startAsynchronous];
}

- (void)moduleListRequestFinish:(ASIFormDataRequest *)response
{
//    NSLog(@"%@",[response responseString]);
    
    [super doneLoadingTableViewData];
    //    if (isEmpty) {
    //        [_notificationArr removeAllObjects];
    //    }
    
//    NSData *data = [response responseData];
//    
//    NSArray *moduleListArray = [[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil] objectForKey:@"listClient"];
    
    SBJsonParser *parser = [[SBJsonParser alloc]init];
    NSArray *moduleArr = [[parser objectWithString:[response responseString]] objectForKey:@"listClient"];
    if ([moduleArr count] == 0) {
        NSLog(@"没有更多了");
        [SVProgressHUD showErrorWithStatus:@"没有更多了" duration:1];

        [_endView setTitle:@"没有更多了.."];
        [_endView refreshStopAnimation];
        isShowEndRefresh = NO;
        [_tableView reloadData];
        return;
    }
    if ([moduleArr count] < 20) {
        isShowEndRefresh = NO;
    }else {
        isShowEndRefresh = YES;
    }
    
    
    
    NSLog(@"[arr count]  %d",[moduleArr count]);
    
    if (_moduleListArray == nil) {
        
        _moduleListArray = [[NSMutableArray alloc] initWithArray:_clientLists];
    }
    
    if (isEmpty) {
        [_clientLists removeAllObjects];
    }
    
    for (NSDictionary *dic in moduleArr) {
        
        NSString *moduleId = [dic objectForKey:@"id"];
        
        for (KoronClientListModel *listModel in _moduleListArray) {
            if ([[listModel clientId] isEqualToString: moduleId]) {
                [_clientLists addObject:listModel];
                break;
            }
        }
        
    }
    
    [_endView setTitle:@"加载更多.."];
    [_endView refreshStopAnimation];
    _lastmid = [[moduleArr lastObject] retain];
    isEmpty = NO;
    
    [_tableView reloadData];
}

- (void)moduleListRequestFail:(ASIFormDataRequest *)response
{
    [super doneLoadingTableViewData];
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
    [_endView setTitle:@"加载更多.."];
    [_endView refreshStopAnimation];
}

#pragma mark - UITableViewDelegate,UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 100) {
        if ([_clientLists count] > 0 && isShowEndRefresh) {
            return [_clientLists count] + 1;
        }
        return _clientLists.count;
    }
    
    return 3;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (tableView.tag == 100) {
        
        if (indexPath.row == [_clientLists count] && isShowEndRefresh) {
            UITableViewCell *endCell = [_tableView dequeueReusableCellWithIdentifier:@"EndCell"];
            if (nil == endCell) {
                endCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EndCell"];
            }
            for (UIView *view in endCell.contentView.subviews) {
                if ([view isKindOfClass:[KoronEndRefreshView class]]) {
                    [view removeFromSuperview];
                }
            }
            [endCell.contentView addSubview:_endView];
            return endCell;
            
        }else {
        
            static NSString *cellIdentifier = @"clientCell";
            
            KoronClientListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            if (cell == nil) {
                cell = [[[KoronClientListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
                cell.delegate = self;
            }
            
            cell.clientLabel.text = [_clientLists[indexPath.row] clientName];
            cell.contactLabel.text = [_clientLists[indexPath.row] userName];
            cell.customerID = [_clientLists[indexPath.row] clientId];
            
            NSString *mobile = [NSString stringWithFormat:@"%@",[_clientLists[indexPath.row] mobile]];
            NSString *phone = [NSString stringWithFormat:@"%@",[_clientLists[indexPath.row] telephone]];
            if ([mobile isEqualToString:@""] == NO ) {
                cell.phoneLabel.text = mobile;
                cell.phoneLabel.hidden = NO;
            }else if ([mobile isEqualToString:@""] && [phone isEqualToString:@""] == NO) {
                cell.phoneLabel.text = phone;
                cell.phoneLabel.hidden = NO;
            }else {
                cell.phoneLabel.text = @"";
                cell.phoneLabel.hidden = YES;
            }
            
            if ([@"<null>" isEqualToString:cell.phoneLabel.text] || [@"(null)" isEqualToString:cell.phoneLabel.text]) {
                cell.phoneLabel.text = @"";
                cell.phoneLabel.hidden = YES;
            }
            
            return cell;

        
        }
        
    }else if (tableView.tag == 11) {
        
        static NSString *CellIdentifier = @"nomalCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        switch (indexPath.row)
        {
            case 0:
                cell.textLabel.textColor=[UIColor whiteColor];
                cell.textLabel.text= @"选择";
                cell.textLabel.backgroundColor=[UIColor clearColor];
                UIView *backgrdView = [[UIView alloc] initWithFrame:cell.frame];
                backgrdView.backgroundColor = [UIColor blackColor];
                cell.backgroundView = backgrdView;
                break;
            case 1:
                cell.textLabel.text=@"拨打电话";
                break;
            case 2:
                cell.textLabel.text=@"发送短信";
                break;
            default:
                break;
        }
        return cell;

    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    [self removeMenu];
    
    if (tableView.tag == 100) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        KoronClientInfoViewController *infoViewController = [[KoronClientInfoViewController alloc] init];
        infoViewController.infoModuleId = [_clientLists[indexPath.row] moduleId];
        infoViewController.infoClientId = [_clientLists[indexPath.row] clientId];
        
        
        
        [self.navigationController pushViewController:infoViewController animated:YES];
        
        [infoViewController release];
        
    }else {
    
        switch(indexPath.row)
        {
            case 1:
            {
                NSString *phoneNumUrl =[NSString stringWithFormat:@"tel://%@",_phoneNum];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumUrl]];
                break;
            }
            case 2:
            {
                NSString *phoneNumUrl =[NSString stringWithFormat:@"sms://%@",_phoneNum];
                [[UIApplication sharedApplication]openURL:[NSURL URLWithString:phoneNumUrl]];
                break;
            }
            default:
                break;
                
        }
        
        [self removeMaskViewAndTableView];
    }
    
}


#pragma mark - KoronClentListDelegate
- (void)didSelectedPhoneNum:(NSString *)phoneNum
{
    if ([phoneNum isEqualToString:@""] == NO) {
        [self addTableView];
        self.phoneNum = phoneNum;
    }
    NSLog(@"打电话");
}

- (void)merchandiserPushWithName:(NSString *)clientName andCustomerID:(NSString *)customerID
{
    merchandiserViewController *merchandiserView=[[merchandiserViewController alloc]initWithNibName:nil bundle:nil];
    merchandiserView.isNavigationPush = YES;
    merchandiserView.customName = clientName;
    merchandiserView.customerID = customerID;
    NSLog(@"%@",customerID);
    [self.navigationController pushViewController:merchandiserView animated:YES];
    [merchandiserView release];
    
    NSLog(@"销售跟单");
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



#pragma mark - HttpRequestDelegate


- (void)clientListRequestFinish:(ASIFormDataRequest *)response {
    [super doneLoadingTableViewData];
    
    NSLog(@"%@",[response responseString]);
    
    NSData *data = [response responseData];
    
    NSArray *clientListArray = [[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil] objectForKey:@"telList"];
    
    if (_isNotFirstGetClientList == NO) {
        
        if (_clientLists != nil && _clientLists.count != 0) {
            [_clientLists removeAllObjects];
        }
        
        if (_clientLists == nil) {
            
            _clientLists = [[NSMutableArray alloc] initWithCapacity:clientListArray.count];
        }
        
        for (NSDictionary *dic in clientListArray) {
            
            KoronClientListModel *clientModel = [[KoronClientListModel alloc] initWithContent:dic];
            [_clientLists addObject:clientModel];
            [clientModel release];
        }


    }else {
        if ([clientListArray count] != 0) {

            for (NSDictionary *dic in clientListArray) {
                KoronClientListModel *clientModel = [[KoronClientListModel alloc] initWithContent:dic];

                [self replaceOrAddDataBaseWithClientId:clientModel];
                [self loadFromDataBase];
                [clientModel release];
            }
            
        }
    }
    
    //按时间排序
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"lastContractTime" ascending:NO];
    [_clientLists sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    //数据库储存数据
    if (_isLoadFromDataBase == NO) {
        
        [self saveInDatabase];
        
        NSString *sidString = [NSString stringWithFormat:@"IsNotFirstGetClientList_%@",[_manager getObjectForKey:SID]];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:sidString];
        [[NSUserDefaults standardUserDefaults] synchronize];
        _isLoadFromDataBase = YES;
        
        [self loadFromDataBase];
    }
    
//    [self loadFromDataBase];
    
    
    [_tableView reloadData];
    if (_isNotFirstGetClientList == NO) {
        [SVProgressHUD showSuccessWithStatus:@"同步完成" duration:1];
        _isNotFirstGetClientList = YES;
    }
    

    isEmpty = YES;
}


- (void)clientListRequestFail:(ASIFormDataRequest *)response {
    [super doneLoadingTableViewData];
    [SVProgressHUD showErrorWithStatus:@"服务器没有响应" duration:1];
    [_endView refreshStopAnimation];
}

#pragma mark - FMDB DataBase
//读取数据库
- (void)loadFromDataBase
{
    
    // 取沙盒Document目录绝对路径
    NSArray * arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * path = [arr objectAtIndex:0];
    
    // 数据库以xxx.db .db结尾
    path = [path stringByAppendingPathComponent:@"KoronDataBase.db"];
    NSLog(@"path is %@...",path);
    
    // 创建并且打开数据库
    FMDatabase * dataBase = [FMDatabase databaseWithPath:path];
    if (![dataBase open]) {
        NSLog(@"can not open dataBase!");
        return;
    }
    
    [dataBase executeUpdate:@"CREATE TABLE if not exists KoronClientList ( id text, userName text,mobile text, telephone text, clientId text, clientName text, lastUpdateTime text, lastContractTime text, moduleId text, sid text)"];
    
    if (_clientLists != nil && _clientLists.count != 0) {
        [_clientLists removeAllObjects];
    }
    
    if (_clientLists == nil) {
        _clientLists = [[NSMutableArray alloc] initWithCapacity:1000];
    }
    
    NSString *sid = [_manager getObjectForKey:SID];
    
    FMResultSet * rs = [dataBase executeQuery:@"select * from KoronClientList WHERE sid = ?",sid];
    
    while ([rs next]) {
        
        KoronClientListModel *clientModel = [[KoronClientListModel alloc] init];
        clientModel.contactId = [rs stringForColumn:@"id"];
        clientModel.userName = [rs stringForColumn:@"userName"];
        clientModel.mobile = [rs stringForColumn:@"mobile"];
        clientModel.telephone = [rs stringForColumn:@"telephone"];
        clientModel.clientId = [rs stringForColumn:@"clientId"];
        clientModel.clientName = [rs stringForColumn:@"clientName"];
        clientModel.lastUpdateTime = [rs stringForColumn:@"lastUpdateTime"];
        clientModel.lastContractTime = [rs stringForColumn:@"lastContractTime"];
        clientModel.moduleId = [rs stringForColumn:@"moduleId"];

        [_clientLists addObject:clientModel];

        [clientModel release];
        
        
        
    }
    
    [rs close];
    
    [dataBase close];
    
    //按时间排序
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"lastContractTime" ascending:NO];
    [_clientLists sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    [_tableView reloadData];
    
}

//搜索客户
- (void)searchClientFromDataBaseWithModuleId:(NSString *)moduleId
{
    if (_searchClientField.text.length == 0) {
        if (_isModuleListShow == NO) {
            [self loadFromDataBase];
        }else {
            [self sendModuleListRequestWithModuleId:_type];
        }
        return;
    }
    
    if (_isModuleListShow == NO) {
        moduleId = @"1";
    }
    
    isShowEndRefresh = NO;
    
    
    // 取沙盒Document目录绝对路径
    NSArray * arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * path = [arr objectAtIndex:0];
    
    // 数据库以xxx.db .db结尾
    path = [path stringByAppendingPathComponent:@"KoronDataBase.db"];
    NSLog(@"path is %@...",path);
    
    // 创建并且打开数据库
    FMDatabase * dataBase = [FMDatabase databaseWithPath:path];
    if (![dataBase open]) {
        NSLog(@"can not open dataBase!");
        return;
    }
    
    [dataBase executeUpdate:@"CREATE TABLE if not exists KoronClientList (id text, userName text,mobile text, telephone text, clientId text, clientName text, lastUpdateTime text, lastContractTime text, moduleId text, sid text)"];
    
    if (_clientLists != nil && _clientLists.count != 0) {
        [_clientLists removeAllObjects];
    }
    
    if (_clientLists == nil) {
        _clientLists = [[NSMutableArray alloc] initWithCapacity:1000];
    }
    
    NSString *sid = [_manager getObjectForKey:SID];
    
    
    FMResultSet * rs = [dataBase executeQuery:@"select * from KoronClientList WHERE sid = ? AND clientName LIKE ? AND moduleId = ?" ,sid,[NSString stringWithFormat:@"%%%@%%", _searchClientField.text],moduleId];
    
    while ([rs next]) {
        
        KoronClientListModel *clientModel = [[KoronClientListModel alloc] init];
        clientModel.contactId = [rs stringForColumn:@"id"];
        clientModel.userName = [rs stringForColumn:@"userName"];
        clientModel.mobile = [rs stringForColumn:@"mobile"];
        clientModel.telephone = [rs stringForColumn:@"telephone"];
        clientModel.clientId = [rs stringForColumn:@"clientId"];
        clientModel.clientName = [rs stringForColumn:@"clientName"];
        clientModel.lastUpdateTime = [rs stringForColumn:@"lastUpdateTime"];
        clientModel.lastContractTime = [rs stringForColumn:@"lastContractTime"];
        clientModel.moduleId = [rs stringForColumn:@"moduleId"];
        
//        BOOL isMore = NO;
//        for (KoronClientListModel *model in _clientLists) {
//            if ([clientModel.clientId isEqualToString:model.clientId]) {
//                isMore = YES;
//                break;
//            }
//        }
//        if (isMore == NO) {
            [_clientLists addObject:clientModel];
//        }
        
//        [_clientLists addObject:clientModel];
        [clientModel release];
        
        
    }
    
    [rs close];
    
    [dataBase close];
    
    //按时间排序
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"lastContractTime" ascending:NO];
    [_clientLists sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    
    [_tableView reloadData];
}

//替换或者添加
- (void)replaceOrAddDataBaseWithClientId:(KoronClientListModel *)clientModel
{
    // 取沙盒Document目录绝对路径
    NSArray * arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * path = [arr objectAtIndex:0];
    
    // 数据库以xxx.db .db结尾
    path = [path stringByAppendingPathComponent:@"KoronDataBase.db"];
    NSLog(@"-------%@",path);
    // 创建并且打开数据库
    FMDatabase * dataBase = [FMDatabase databaseWithPath:path];
    if (![dataBase open]) {
        NSLog(@"can not open dataBase!");
        return;
    }
    
    [dataBase executeUpdate:@"CREATE TABLE if not exists KoronClientList ( id text, userName text,mobile text, telephone text, clientId text, clientName text, lastUpdateTime text, lastContractTime text, moduleId text, sid text)"];
    
    NSString *sid = [_manager getObjectForKey:SID];
    
    FMResultSet * rs = [dataBase executeQuery:@"select * from KoronClientList WHERE clientId = ?",clientModel.clientId];
    if ([rs next] == NO) {
        [dataBase executeUpdate:@"INSERT INTO KoronClientList (id,userName,mobile,telephone,clientId,clientName,lastUpdateTime,lastContractTime,moduleId,sid) VALUES (?,?,?,?,?,?,?,?,?,?)",clientModel.contactId,clientModel.userName,clientModel.mobile,clientModel.telephone,clientModel.clientId,clientModel.clientName,clientModel.lastUpdateTime,clientModel.lastContractTime,clientModel.moduleId,sid];
    }else {
    
            [dataBase executeUpdate:@"DELETE  FROM KoronClientList WHERE clientId = ?",clientModel.clientId];
            [dataBase executeUpdate:@"INSERT INTO KoronClientList (id,userName,mobile,telephone,clientId,clientName,lastUpdateTime,lastContractTime,moduleId,sid) VALUES (?,?,?,?,?,?,?,?,?,?)",clientModel.contactId,clientModel.userName,clientModel.mobile,clientModel.telephone,clientModel.clientId,clientModel.clientName,clientModel.lastUpdateTime,clientModel.lastContractTime,clientModel.moduleId,sid];
    }
    
    
    
    
    [dataBase close];
    
    
}

//储存数据
- (void)saveInDatabase
{
    // 取沙盒Document目录绝对路径
    NSArray * arr = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * path = [arr objectAtIndex:0];
    
    // 数据库以xxx.db .db结尾
    path = [path stringByAppendingPathComponent:@"KoronDataBase.db"];
    NSLog(@"path is %@...",path);
    
    // 创建并且打开数据库
    FMDatabase * dataBase = [FMDatabase databaseWithPath:path];
    if (![dataBase open]) {
        NSLog(@"can not open dataBase!");
        return;
    }
    
    [dataBase executeUpdate:@"CREATE TABLE if not exists KoronClientList (id text, userName text,mobile text, telephone text, clientId text, clientName text, lastUpdateTime text, lastContractTime text, moduleId text, sid text)"];
    
    NSString *sid = [_manager getObjectForKey:SID];
    for (NSInteger index = 0; index < [_clientLists count]; index++) {
        
        KoronClientListModel *clientModel = _clientLists[index];
        
        FMResultSet * rs = [dataBase executeQuery:@"select * from KoronClientList WHERE sid = ? AND clientId = ?" ,sid, clientModel.clientId];
        if ([rs next]) {
            continue;
        }else {
            [dataBase executeUpdate:@"INSERT INTO KoronClientList (id,userName,mobile,telephone,clientId,clientName,lastUpdateTime,lastContractTime,moduleId,sid) VALUES (?,?,?,?,?,?,?,?,?,?)",clientModel.contactId,clientModel.userName,clientModel.mobile,clientModel.telephone,clientModel.clientId,clientModel.clientName,clientModel.lastUpdateTime,clientModel.lastContractTime,clientModel.moduleId,sid];
        }
        
    }
    
    [dataBase close];
}


#pragma mark - computTime

- (NSString *)computationTimeResult:(NSString *)timeString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [formatter dateFromString:timeString];
    NSInteger result = [self computationTime:[formatter stringFromDate:date]];
    
    if (result == 0) {
        [formatter setDateFormat:@"今天 HH:mm"];
    }else if (result == 1) {
        [formatter setDateFormat:@"昨天 HH:mm"];
    }else if (result == 2) {
        [formatter setDateFormat:@"前天 HH:mm"];
    }else {
        [formatter setDateFormat:@"MM-dd HH:mm"];
    }
    return [NSString stringWithFormat:@"%@",[formatter stringFromDate:date]];
}


- (NSInteger)computationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowadayTimeStr = [fromatter stringFromDate:[NSDate date]];
    
    if ([[nowadayTimeStr substringToIndex:3] integerValue] - [[timeStr substringToIndex:3] integerValue] == 0) {
        if ([[nowadayTimeStr substringWithRange:NSMakeRange(5, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(5, 2)] integerValue] == 0) {
            if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 0) {
                return 0;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
                return 1;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 2) {
                return 2;
            }else {
                return 4;
            }
        }else {
            return 4;
        }
        
    }else {
        return 4;
    }
    
}

#pragma mark - showMenu
- (void)showMenu {
    
    if (_isNotFirstGetClientList == NO) {
        UIAlertView *synAlertView = [[UIAlertView alloc] initWithTitle:@"M-office" message:@"请先同步数据" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles: nil];
        [synAlertView show];
        [synAlertView release];
        return;
    }
    
    if (nil == _menu && _moduleArray != nil) {
        UIImage *image = [UIImage imageNamed:@"popover_background.png"];
        UIImage *backgroundImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)];
        _backgroundImageView = [[UIImageView alloc]initWithImage:backgroundImage];
        [_backgroundImageView setImage:backgroundImage];
        [_backgroundImageView setFrame:CGRectMake(self.view.frame.size.width/2 - 217/2, 50, 217, 115)];
        
        _menu = [[KoronClientListMenuViewController alloc]initWithStyle:UITableViewStylePlain withModuleArray:_moduleArray];
        _menu.delegate = self;
        [_menu setTableViewFrame:CGRectMake(self.view.frame.size.width/2 - 217/2 + 17/2, 60, 200, 100)];
        
        _maskView = [[UIView alloc] initWithFrame:self.view.bounds];
        _maskView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        UITapGestureRecognizer *maskViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeMenu)];
        [_maskView addGestureRecognizer:maskViewTap];
        [maskViewTap release];
        
    }
    if (_moduleArray != nil) {
        [self.view addSubview:_maskView];
        [self.view addSubview:_backgroundImageView];
        [self.view addSubview:_menu.view];
        [_topImageView setImage:[UIImage imageNamed:@"news_up.png"]];
    }
    
}

- (void)removeMenu {
    
    [_maskView removeFromSuperview];
    [_backgroundImageView removeFromSuperview];
    [_menu.view removeFromSuperview];
    
    [_topImageView setImage:[UIImage imageNamed:@"news_down.png"]];
}

#pragma mark - KoronMenuViewControllerDelegate
- (void)changeTheType:(NSString *)type withTitle:(NSString *)title {
    
    [_topButton setTitle:title forState:UIControlStateNormal];
    
    if ([title isEqualToString: @"默认模版"]) {
        _lastUpdateTime = @"";
        _isModuleListShow = NO;
        if (_isLoadFromDataBase) {
            [self loadFromDataBase];
        }else {
            [self sendClientListRequest];
        }
        
    }else {
        self.type = type;
        isEmpty = YES;
        _lastmid = @"";
        _isModuleListShow = YES;
        [self sendModuleListRequestWithModuleId:type];
    }
    
    [self removeMenu];

//
//    
    
}

#pragma mark - UpdateFlagDelegate
- (void)updateFinished : (ASIFormDataRequest *)response {
//    NSLog(@"%@",[[response responseString] JSONValue]);
    
    NSLog(@"%@",[response responseString]);
}

- (void)updateFailed : (ASIFormDataRequest *)response {
//    NSLog(@"%@",[[response responseString] JSONValue]);
    NSLog(@"%@",[response responseString]);
}


- (void)dealloc
{
    
    [_endView release],_endView = nil;
    [_clientListRequest release],_clientListRequest = nil;
    [_lastmid release],_lastmid = nil;
    [_topButton release],_topButton = nil;
    [_topImageView release],_topImageView = nil;
    [_menu release],_menu = nil;
    [_backgroundImageView release],_backgroundImageView = nil;
//    [_status release],_status = nil;
    [_type release],_type = nil;
    [_searchClientField release], _searchClientField = nil;
    [_maskView release],_maskView = nil;
    [_clientLists release], _clientLists = nil;
    [_moduleListArray release], _moduleListArray = nil;
    [_phoneNum release], _phoneNum = nil;
    [super dealloc];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
