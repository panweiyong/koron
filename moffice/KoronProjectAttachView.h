//
//  KoronProjectAttachView.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-28.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KoronProjectAttachView : UIView

@property (nonatomic, retain) UIImageView *icon;
@property (nonatomic, retain) UILabel *attachLabel;

@end
