//
//  KoronDropDownRefreshViewController.h
//  moffice
//
//  Created by Mac Mini on 13-10-9.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronSuperclassViewController.h"
#import "EGORefreshTableHeaderView.h"

@interface KoronDropDownRefreshViewController : KoronSuperclassViewController <EGORefreshTableHeaderDelegate> {
    UITableView *_tableView;
    EGORefreshTableHeaderView * _refreshHeaderView;
    BOOL _reloading;
}

- (void)doneLoadingTableViewData;

@end
