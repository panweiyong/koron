//
//  KoronClientListModel.m
//  moffice
//
//  Created by Mac Mini on 13-11-11.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "KoronClientListModel.h"

@implementation KoronClientListModel

//property jsonKey
- (id)mapAttributes
{
    NSDictionary *mapDic = @{
                             @"contactId"           : @"id",
                             @"userName"            : @"userName",
                             @"mobile"              : @"mobile",
                             @"clientId"            : @"clientId",
                             @"clientName"          : @"clientName",
                             @"lastUpdateTime"      : @"lastUpdateTime",
                             @"lastContractTime"    : @"lastContractTime",
                             @"moduleId"            : @"moduleId",
                             @"telephone"           : @"telephone"
                             };
    return mapDic;
}

- (void)dealloc
{
    self.contactId = nil;
    self.userName = nil;
    self.mobile = nil;
    self.clientId = nil;
    self.clientName = nil;
    self.lastContractTime = nil;
    self.lastContractTime = nil;
    self.moduleId = nil;
    
    [super dealloc];
}

@end
