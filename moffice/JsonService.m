//
//  Copyright 2010 All rights reserved.
//

#import "JsonService.h"

@implementation JsonService
 
@synthesize OARequest,POSTRequest,UPDATEFLAGRequest,CUSTOMERquest,MERRquest,CLIENTRquest;
@synthesize delegate,pushDelegate,ArrayDelegate,MerArrayDelegate;
@synthesize pushJson = _pushJson;
/*
@synthesize username;
@synthesize password;
@synthesize sid;
@synthesize host;
@synthesize hosturl;
*/
static JsonService *sharedService = nil;
/*
NSString *username;
NSString *sid;
NSString *password;
NSString *host;
NSString *hosturl;
BOOL firstRun;
BOOL autologin;
*/
 
+ (JsonService *)sharedManager{
    @synchronized(self) {
        if (sharedService == nil) {
            [[self alloc] init];  
        }
    }    
    return sharedService;    
} 

+(id)alloc{
    @synchronized ([sharedService class])
    {
        NSAssert(sharedService == nil,@"Attempted to allocated a second instance of the sharedService singleton"); // 6
        sharedService = [super alloc]; 
        [sharedService loadConf];
        return sharedService;
    }
    return nil;  
}

-(void)saveConf:(NSString *)key value:(id)value
{
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    [conf setObject:value  forKey:key];
    [conf synchronize];
}

-(void)loadConf
{
    //获得配置信息
    /*NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    username=[conf stringForKey:@"username"];
    password=[conf stringForKey:@"password"];
    host=[conf stringForKey:@"host"];
    firstRun = [[conf objectForKey:@"firstRun"] boolValue]; 
    sid=[conf stringForKey:@"sid"];
    hosturl= [[NSString stringWithFormat:@"http://%@/moffice",host ] retain];
    autologin = [[conf objectForKey:@"autologin"] boolValue]; 
     */
    //CCLOG(@"firstRun:%i",firstRun);
}

-(NSString*)getUserName {
    
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];return [conf stringForKey:@"username"];
}
-(NSString*)getPassword{NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];return [conf stringForKey:@"password"];}
-(NSString*)getHost{NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];return [conf stringForKey:@"host"];}
-(NSString*)getSessionID{NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];return [conf stringForKey:@"sid"];}
-(BOOL)isFirstRun{NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];return [[conf objectForKey:@"firstRun"] boolValue]; }

//断开所有网络请求
-(void)cancelAllRequest{
    if(OARequest)
    {
           [OARequest clearDelegatesAndCancel];
           [OARequest cancel];
    }
    if(POSTRequest)
    {
        [POSTRequest clearDelegatesAndCancel];
        [POSTRequest cancel];
    }
    if(UPDATEFLAGRequest)
    {
        [UPDATEFLAGRequest clearDelegatesAndCancel];
        [UPDATEFLAGRequest cancel];
    }
    if(CUSTOMERquest)
    {
        [CUSTOMERquest clearDelegatesAndCancel];
        [CUSTOMERquest cancel];
    }
    if(MERRquest)
    {
        [MERRquest clearDelegatesAndCancel];
        [MERRquest cancel];
    }
}

-(void)setUserName:(NSString*)u{
    [self saveConf:@"username" value:u];
}
-(void)setSid:(NSString*)uid{
    [self saveConf:@"sid" value:uid];
}
-(void)setPassword:(NSString*)p{
    [self saveConf:@"password" value:p];
}
-(void)setHost:(NSString*)h{
    [self saveConf:@"host" value:h];
    [self saveConf:@"hosturl" value:[NSString stringWithFormat:@"http://%@/moffice",h ]];
}
-(void)setIsAutoLogin:(BOOL)v
{
     [self saveConf:@"autologin" value: [NSString stringWithFormat:@"%i" ,v ] ];
     //autologin= v;
}

-(NSString*)getUserIconURL:(NSString*)from
{
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];//[NSString stringWithFormat:@"http://%@/moffice",h ]   ;
    return [NSString stringWithFormat:@"%@?op=%@&sid=%@&userid=%@",hosturl,@"getUserInfo", [self getSessionID] ,from];
}

-(NSString*)getOnlineURL
{
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];//[NSString stringWithFormat:@"http://%@/moffice",h ]   ;
    return [NSString stringWithFormat:@"%@?op=%@&sid=%@",hosturl,@"online",[self getSessionID] ];
}

-(BOOL)isAutoLogin
{
    //return (username!=NULL&&password!=NULL&&host!=NULL);
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    return  [[conf objectForKey:@"autologin"] boolValue]; 
}

-(BOOL)enabledAutoLogin
{
    return ([self getUserName]!=NULL&&[self getPassword]!=NULL&&[self getHost]!=NULL&&[self isAutoLogin]);
}


-(NSString*)getMobile
{
     NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    return [conf stringForKey:@"mobile"];
}

-(void)setMobile:(NSString*)m
{
    [self saveConf:@"mobile" value:m ];
    
}

-(void)initRequest
{
    //if(OARequest)[OARequest clearDelegatesAndCancel];
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    [self setOARequest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:hosturl]]];
	[OARequest setDelegate:self];
    [OARequest setDidFinishSelector:@selector(requestFinished:)];
    [OARequest setDidFailSelector:@selector(requestFailed:)];
    [OARequest setPostValue:[self getSessionID] forKey:@"sid"];
}

-(void)initPostRequest
{
    //if(OARequest)[OARequest clearDelegatesAndCancel];
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    [self setPOSTRequest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:hosturl]]];
	[POSTRequest setDelegate:self];
    [POSTRequest setDidFinishSelector:@selector(postFinished:)];
    [POSTRequest setDidFailSelector:@selector(requestFailed:)];
    [POSTRequest setPostValue:[self getSessionID] forKey:@"sid"];
}

//系统消息
-(void)listTips:(NSString*)lastid
{
    [self initRequest];
	[OARequest setPostValue:@"listTips" forKey:@"op"];
    if(lastid)[OARequest setPostValue:lastid forKey:@"lastmid"];
    [OARequest startAsynchronous];
}

-(void)getTipDetails:(NSString*)OID t:(NSString*)t
{
    [self initRequest];
    //CCLOG(@"refid:%@ type:%@",OID ,t);
	[OARequest setPostValue:@"getInfo" forKey:@"op"];
    [OARequest setPostValue:OID forKey:@"refid"];
    [OARequest setPostValue:t forKey:@"type"];
    [OARequest startAsynchronous];
}

-(void)listMessageBox
{
    [self initRequest];
	[OARequest setPostValue:@"listMessageBox" forKey:@"op"];
    [OARequest startAsynchronous];
}
-(void)listMessages:(NSString*)from lastmid:(NSString*)lastmid
{
    NSLog(@"lastmid is %@",lastmid);
    [self initRequest];
	[OARequest setPostValue:@"listMessages" forKey:@"op"];
    [OARequest setPostValue:from forKey:@"from"];
    [OARequest setPostValue:lastmid forKey:@"lastmid"];
    [OARequest startAsynchronous];
}
-(void)sendMessage:(NSString*)to content:(NSString*)content
{
    [self initPostRequest];
    [POSTRequest setPostValue:@"sendMessage" forKey:@"op"];
    [POSTRequest setPostValue:content forKey:@"content"];
    [POSTRequest setPostValue:to forKey:@"to"];
    [POSTRequest startAsynchronous];
}
//列出在线用户
-(void)listOnlineUsers
{
    [self initRequest];
	[OARequest setPostValue:@"listOnlineUsers" forKey:@"op"];
    [OARequest startAsynchronous];
}
//列出所有用户
-(void)listAllUsers
{
    [self initRequest];
	[OARequest setPostValue:@"listAllUsers" forKey:@"op"];
    [OARequest startAsynchronous];
}

-(void)listWorkflows:(NSString*)lastwid
{
    [self initRequest];
	[OARequest setPostValue:@"listWorkflows" forKey:@"op"];
    if(lastwid!=nil)[OARequest setPostValue:lastwid forKey:@"lastwid"];
    [OARequest startAsynchronous];
}
-(void)getWorkflowDetails:(NSString*)wid
{
    [self initRequest];
	[OARequest setPostValue:@"getWorkflow" forKey:@"op"];
    [OARequest setPostValue:wid forKey:@"wid"];
    [OARequest startAsynchronous];
}
-(void)cancelWorkflow:(NSString*)wid
{
    [self initPostRequest];
    [POSTRequest setPostValue:@"cancelWorkflow" forKey:@"op"];
    [POSTRequest setPostValue:wid forKey:@"wid"];
    [POSTRequest startAsynchronous];
}
-(void)approvalWorkflow:(NSString*)wid aid:(NSString*)aid to:(NSString*)to memo:(NSString*)memo iscancel:(BOOL)iscancel
{
    [self initPostRequest];
    [POSTRequest setPostValue:@"approvalWorkflow" forKey:@"op"];
    [POSTRequest setPostValue:wid forKey:@"wid"];
    [POSTRequest setPostValue:aid forKey:@"actid"];
    [POSTRequest setPostValue:memo forKey:@"memo"];
    [POSTRequest setPostValue:to forKey:@"to"];
    if(iscancel==YES)[POSTRequest setPostValue:@"1" forKey:@"cancel"];
    [POSTRequest startAsynchronous];
}

//工作计划
-(void)getWorkPlanList:(NSString*)type str:(NSString*)strTime end:(NSString*)endTime
{
//    UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:nil message:[NSString stringWithFormat:@"%@,%@,%@",type,strTime,endTime] delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//    [alert1 show];
//    [alert1 release];
//    if([type isEqualToString:@"week"])
//    {
//        return;
//    }
    //UIAlertView *alert2=[[UIAlertView alloc]initWithTitle:nil message:@"jiance0" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    //[alert2 show];
    //[alert2 release];
    [self initRequest];
    [OARequest setPostValue:@"showWorkPlanList" forKey:@"op"];
    [OARequest setPostValue:type forKey:@"type"];
    [OARequest setPostValue:strTime forKey:@"beginTime"];
    [OARequest setPostValue:endTime forKey:@"endTime"];
    [OARequest startAsynchronous];
}

-(void)getPersonList:(NSString*)type keyWord:(NSString*)keyWord lastId:(NSString*)lastId
{
    [self initRequest];
    [OARequest setPostValue:type forKey:@"op"];
    [OARequest setPostValue:keyWord forKey:@"keyword"];
    [OARequest setPostValue:lastId forKey:@"lastId"];
    [OARequest startAsynchronous];
}

-(void)getWorkPlanDetail:(NSString*)planID
{
    [self initRequest];
    [OARequest setPostValue:@"showWorkPlan" forKey:@"op"];
    [OARequest setPostValue:planID forKey:@"planId"];
    [OARequest startAsynchronous];
}

-(void)addWorkPlan:(NSString*)Id type:(NSString*)Type title:(NSString *)title content:(NSString*)content start:(NSString*)start end:(NSString*)end ass:(NSString*)ass
{
   // NSLog(@"planId=%@,Type=%@,title=%@,content=%@,start=%@,end=%@,ass=%@",Id,Type,title,content,start,end,ass);
    [self initRequest];
    [OARequest setPostValue:@"addUpdateWorkPlan" forKey:@"op"];
    if(Id!=nil&&[Id length]!=0)
    {
      [OARequest setPostValue:Id forKey:@"planId"];
    }
    else
    {
      [OARequest setPostValue:nil forKey:@"planId"];
    }
    [OARequest setPostValue:Type forKey:@"planType"];
    [OARequest setPostValue:title forKey:@"title"];
    [OARequest setPostValue:content forKey:@"content"];
    [OARequest setPostValue:start forKey:@"beginDate"];
    [OARequest setPostValue:end forKey:@"endDate"];
    [OARequest setPostValue:ass forKey:@"Ass"];
    [OARequest startAsynchronous];
}

-(void)deleteWorkPlan:(NSString *)planId
{
    [self initRequest];
    [OARequest setPostValue:@"delWorkPlan" forKey:@"op"];
    [OARequest setPostValue:planId forKey:@"planId"];
    [OARequest startAsynchronous];
}
          //点评工作计划
-(void)reviewWorkPlan:(NSString *)planId remarkType:(NSString *)remarkType commitId:(NSString *)commitId content:(NSString*)content
{
    [self initRequest];
    [OARequest setPostValue:@"commitWorkPlan" forKey:@"op"];
    [OARequest setPostValue:planId forKey:@"planId"];
    [OARequest setPostValue:remarkType forKey:@"remarkType"];
    [OARequest setPostValue:commitId forKey:@"commitId"];
    [OARequest setPostValue:content forKey:@"content"];
    [OARequest startAsynchronous];
}
          //总结工作计划
-(void)summaryWorkPlan:(NSString *)planId time:(NSString*)time summary:(NSString*)summary statusId:(NSString*)statusId
{
    [self initRequest];
    [OARequest setPostValue:@"summaryWorkPlan" forKey:@"op"];
    [OARequest setPostValue:planId forKey:@"planId"];
    [OARequest setPostValue:time forKey:@"time"];
    [OARequest setPostValue:summary forKey:@"summary"];
    [OARequest setPostValue:statusId forKey:@"statusId"];
    [OARequest startAsynchronous];
}

//论坛
-(void)getBbs:(NSString*)bid
{
    [self initRequest];
    [OARequest setPostValue:@"forum" forKey:@"op"];
    [OARequest setPostValue:bid forKey:@"refid"];
    [OARequest startAsynchronous];
}

//论坛发表评论
-(void)postReplay:(NSString*)refidStr second:(NSString*)topicIdStr third:(NSString*)contentStr;
{
    //NSLog(@"refid=%@,topicId=%@,content=%@",refidStr,topicIdStr,contentStr);
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    [self setOARequest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:hosturl]]];
	[OARequest setDelegate:self];
    [OARequest setDidFinishSelector:@selector(requestFinished:)];
    [OARequest setDidFailSelector:@selector(requestFailed:)];
    [OARequest setPostValue:[self getSessionID] forKey:@"sid"];
    [OARequest setPostValue:@"replayForum" forKey:@"op"];
    [OARequest setPostValue:refidStr forKey:@"refid"];
    [OARequest setPostValue:topicIdStr forKey:@"topicId"];
    [OARequest  setPostValue:contentStr forKey:@"content"];
    [OARequest startAsynchronous];
}

//通知通告
-(void)getAdvice:(NSString*)Aid
{
    [self initRequest];
    [OARequest setPostValue:@"detail" forKey:@"op"];
    [OARequest setPostValue:@"advice" forKey:@"type"];
    [OARequest setPostValue:Aid forKey:@"refid"];
    [OARequest startAsynchronous];
}

//发帖类型
-(void)getListviewData
{
    [self initRequest];
    [OARequest setPostValue:@"bbsClass" forKey:@"op"];
    [OARequest startAsynchronous];
}

-(void)getListviewDetailData
{
    [self initRequest];
    [OARequest setPostValue:@"bbsType" forKey:@"op"];
    [OARequest startAsynchronous];
}

-(void)pushListData:(NSString*)jsonString topStr:(NSString*)str
{
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    [self setMERRquest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:hosturl]]];
	[MERRquest setDelegate:self];
    [MERRquest setDidFinishSelector:@selector(pushDateFinished:)];
    [MERRquest setDidFailSelector:@selector(pushDateFailed:)];
    [MERRquest setPostValue:[self getSessionID] forKey:@"sid"];
    [MERRquest setPostValue:@"addForum" forKey:@"op"];
    [MERRquest setPostValue:str forKey:@"topicId"];
    [MERRquest setPostValue:jsonString forKey:@"content"];
    [MERRquest startAsynchronous];
}

//通讯录
-(void)getContactList
{
    [self initRequest];
    [OARequest setPostValue:@"syncContact" forKey:@"op"];
    [OARequest startAsynchronous];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//邮件
-(void)listMailBox
{
    [self initRequest];
	[OARequest setPostValue:@"listMailBox" forKey:@"op"];
    [OARequest startAsynchronous];
    
}
-(void)listMail:(NSString*)bid fid:(NSString*)fid lastmid:(NSString*)lastmid
{
    [self initRequest];
	[OARequest setPostValue:@"listMail" forKey:@"op"];
    [OARequest setPostValue:bid forKey:@"bid"];
    [OARequest setPostValue:fid forKey:@"fid"];
    if(lastmid!=nil)[OARequest setPostValue:lastmid forKey:@"lastmid"];
    [OARequest startAsynchronous];
}
-(void)getMail:(NSString*)mid
{
    [self initRequest];
	[OARequest setPostValue:@"getMail" forKey:@"op"];
    [OARequest setPostValue:mid forKey:@"mid"];
    [OARequest startAsynchronous];
}
//触发接收邮件
-(void)receiveMail:(NSString*)bid
{
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    [self setUPDATEFLAGRequest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:hosturl]]];
	[UPDATEFLAGRequest setDelegate:self];
    [UPDATEFLAGRequest setDidFinishSelector:@selector(updateFinished:)];
    [UPDATEFLAGRequest setDidFailSelector:@selector(requestFailed:)];
    [UPDATEFLAGRequest setPostValue:bid forKey:@"bid"];
    [UPDATEFLAGRequest setPostValue:@"receiveMail" forKey:@"op"];
    [UPDATEFLAGRequest startAsynchronous];
}
//发送邮件
-(void)sendMail:(NSString*)to from:(NSString*)from subject:(NSString*)subject content:(NSString*)content;
{
    [self initPostRequest];
    [POSTRequest setPostValue:@"sendMail" forKey:@"op"];
    [POSTRequest setPostValue:content forKey:@"content"];
    [POSTRequest setPostValue:to forKey:@"to"];
    [POSTRequest setPostValue:from forKey:@"from"];
    [POSTRequest setPostValue:subject forKey:@"subject"];
    [POSTRequest startAsynchronous];
}
//列出内部邮件用户
 -(void)listIntranetMailUsers
 {
     [self initRequest];
     [OARequest setPostValue:@"listAddress" forKey:@"op"];
     [OARequest setPostValue:@"1" forKey:@"intranet"];
     [OARequest startAsynchronous];
 }
 //列出公用邮件地址
 -(void)listInternetMailUsers:(NSString*)skey
 {
     [self initRequest];
     [OARequest setPostValue:@"listAddress" forKey:@"op"];
     [OARequest setPostValue:@"0" forKey:@"intranet"];
     [OARequest setPostValue:skey forKey:@"skey"];
     [OARequest startAsynchronous];
 }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//其他
-(void)listMyModules
{
    [self initRequest];
    [OARequest setPostValue:@"listMyModules" forKey:@"op"];
    [OARequest startAsynchronous];
}
-(void)listBusinessModules
{
    [self initRequest];
    [OARequest setPostValue:@"listBusinessModules" forKey:@"op"];
    [OARequest startAsynchronous];
}

-(void)loadModule:(NSString*)target parameterName:(NSString*)parameterName parameterValue:(NSString*)parameterValue parameterName2:(NSString*) parameterName2  parameterValue2:(NSString*)parameterValue2 mid:(NSString *)mid;
{
    [self initRequest];
    [OARequest setPostValue:target forKey:@"op"];
    if(parameterName!=nil)[OARequest setPostValue:parameterValue forKey:parameterName];
    if(parameterName2!=nil)[OARequest setPostValue:parameterValue2 forKey:parameterName2];
    if(mid!=nil)[OARequest setPostValue:mid forKey:@"mid"];
    [OARequest startAsynchronous];
}
-(void)loadModuleWithParameters:(NSString*)target parameters:(NSMutableDictionary*)parameters mid:(NSString*)mid;
{
    [self initRequest];
    [OARequest setPostValue:target forKey:@"op"];
    if(parameters!=nil&&[[parameters allKeys] count]>0)
    {
        for(NSString *k in [parameters allKeys])
        {
            [OARequest setPostValue:[parameters objectForKey:k] forKey:k];
        }
        if(mid!=nil)[OARequest setPostValue:mid forKey:@"mid"];

    }
    [OARequest startAsynchronous];
}

-(void)updataRead:(NSString*)tipsid
{
    NSLog(@"tipsid=%@",tipsid);
    [self initRequest];
    [OARequest setPostValue:tipsid forKey:@"tipsid"];
    [OARequest setPostValue:@"updateFlag" forKey:@"op"];
    [OARequest startAsynchronous];
}

//修改信息为已读状态
-(void)UpdateFlag:(NSString*)type oid:(NSString*)oid flag:(int)flag tipsid:(NSString*)tipsid
{
    CCLOG(@"%@",type);
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    [self setUPDATEFLAGRequest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:hosturl]]];
	[UPDATEFLAGRequest setDelegate:self];
    
    [UPDATEFLAGRequest setPostValue:[self getSessionID] forKey:@"sid"];
    [UPDATEFLAGRequest setPostValue:tipsid forKey:@"tipsid"];
    [UPDATEFLAGRequest setPostValue:type forKey:@"type"];
    [UPDATEFLAGRequest setPostValue:[NSString stringWithFormat:@"%i",flag] forKey:@"flag"];
    [UPDATEFLAGRequest setPostValue:@"updateFlag" forKey:@"op"];
    
    [UPDATEFLAGRequest setPostValue:oid forKey:@"oid"];
    [UPDATEFLAGRequest setPostValue:oid forKey:@"refid"];
    
    [UPDATEFLAGRequest setDidFinishSelector:@selector(updateFinished:)];
    [UPDATEFLAGRequest setDidFailSelector:@selector(updateFailed:)];
    
    NSLog(@"requestMethod  %@",[UPDATEFLAGRequest requestMethod]);
    NSLog(@"url  is %@",[UPDATEFLAGRequest url]);
    
    
    [UPDATEFLAGRequest startAsynchronous];
    
    
}

-(void)login:(NSString*)uname pwd:(NSString*)pwd 
{   
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    //username = uname;
    //password = pwd;
    [self setUserName:uname];
    [self setPassword:pwd];
    //CCLOG(@"username:%@ password:%@ host:%@ md5:%@ url:%@",username,password,host,[password md5],hosturl);
    [self setOARequest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:hosturl]]];
	[OARequest setDelegate:self];
    [OARequest setDidFinishSelector:@selector(authFinished:)];
    [OARequest setDidFailSelector:@selector(authFailed:)];
    [OARequest setPostValue:@"auth" forKey:@"op"];
	[OARequest setPostValue:uname forKey:@"username"];
    [OARequest setPostValue:[pwd md5] forKey:@"password"];
    if([self getMobile]!=nil)[OARequest setPostValue:[self getMobile] forKey:@"mobile"];
    
    NSString *dt=[conf objectForKey:@"dt"];
    if(dt!=nil)[OARequest setPostValue:dt forKey:@"dt"];
    //CCLOG(@"%@",dt);
    //[OARequest setPostValue:oahost forKey:@"mobile"]; 
	[OARequest startAsynchronous]; 
}

-(void)findCustomer:(NSString*)customerName
{
    
    if (nil != CUSTOMERquest) {
        [CUSTOMERquest clearDelegatesAndCancel];
        [CUSTOMERquest release];
        CUSTOMERquest = nil;
    }
     NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
     NSString *hosturl =  [conf objectForKey:@"hosturl"];
     NSLog(@"%@",hosturl);
     CUSTOMERquest=[[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:hosturl]];
     CUSTOMERquest.delegate=self;
	//[CUSTOMERquest setDelegate:self];
     [CUSTOMERquest setPostValue:@"selectClient" forKey:@"op"];
	 [CUSTOMERquest setPostValue:[self getSessionID] forKey:@"sid"];
     [CUSTOMERquest setPostValue:customerName forKey:@"keyword"];
     [CUSTOMERquest setPostValue:nil forKey:@"lastId"];
     [CUSTOMERquest setDidFinishSelector:@selector(customerRequestFinished:)];
     [CUSTOMERquest setDidFailSelector:@selector(customerRequestFailed:)];
     [CUSTOMERquest startAsynchronous];
//    [self initPostRequest];
//    [POSTRequest setDidFinishSelector:@selector(customerRequestFinished:)];
//    [POSTRequest setDidFailSelector:@selector(customerRequestFailed:)];
//    [POSTRequest setPostValue:@"selectClient" forKey:@"op"];
//    [POSTRequest setPostValue:customerName forKey:@"keyword"];
//    [POSTRequest startAsynchronous];
}

-(void)findMoreCustomer:(NSString*)customerName lastId:(NSString*)lastId
{
    [self initRequest];
    [OARequest setPostValue:@"selectClient" forKey:@"op"];
    [OARequest setPostValue:customerName forKey:@"keyword"];
    [OARequest setPostValue:lastId forKey:@"lastId"];
    [OARequest startAsynchronous];
}

//请求完成
- (void)customerRequestFinished:(ASIHTTPRequest *)request
{
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"netIsok"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSString *responseString = [request responseString];
    NSData *data = [request responseData];
    NSLog(@"responseString=%@",responseString);
    //CCLOG(@"authFinished:%@",responseString);
//    id getData = [responseString JSONValue];
    
    id getData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
    if ([getData isKindOfClass:[NSDictionary class]])
    {
        NSArray *dataArray=[getData objectForKey:@"list"];
        if([dataArray count]==0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"existCustomer"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //return;
        }
        else
        {
            [MerArrayDelegate merarrayDataCallBack:dataArray];
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"existCustomer"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopActivity2" object:nil];
}

-(void)customerRequestFailed:(ASIHTTPRequest*)request
{
    if (delegate && [delegate respondsToSelector:@selector(requestFailed:) ]) {
		[delegate performSelector:@selector(requestFailed:) withObject:request];
	}
    [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"netIsok"];
   [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopActivity2" object:nil];
    
}

-(void)pushDate:(NSString*)jsonString
{  
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    //NSString *cusUrl=[NSString stringWithFormat:@"%@?op=%@&sid=%@&json=%@",hosturl,@"addSaleBill",[self getSessionID],jsonString];
    [self setMERRquest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:hosturl]]];
	[MERRquest setDelegate:self];
    [MERRquest setDidFinishSelector:@selector(pushDateFinished:)];
    [MERRquest setDidFailSelector:@selector(pushDateFailed:)];
    [MERRquest setPostValue:[self getSessionID] forKey:@"sid"];
    [MERRquest setPostValue:@"addSaleBill" forKey:@"op"];
    [MERRquest setPostValue:jsonString forKey:@"json"];
    [MERRquest startAsynchronous];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowTopView" object:nil];
}  

-(void)pushDateFinished:(ASIHTTPRequest*)request
{
    //服务器返回错误信息时,发送请求失败的通知
    NSString *response = [request responseString];
    NSLog(@"%@",response);
    
//    NSDictionary *responseDic = [response JSONValue];
    
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:nil];
    
    if ([[responseDic objectForKey:@"code"] integerValue] == -1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FailedRemoveTopView" object:nil];
        return;
    }
    
    NSString *responseString = [request responseString];
    NSLog(@"%@",responseString);
//    id getData=[responseString JSONValue];
    
    id getData = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:nil];
    
    if ([getData isKindOfClass:[NSDictionary class]])
    {
        NSString *desc=[getData objectForKey:@"desc"];
        NSNumber *code=[getData objectForKey:@"code"];
       // NSLog(@"desc=%@",desc);
        [pushDelegate dataCallBack:@"YES" other:desc third:code];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopActivity3" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveTopView" object:nil];
    //提交完成,清空json数据
    [_pushJson release];
    _pushJson = nil;
}

//添加客户
-(void)pushAddClient:(NSString *)jsonString
{
    NSUserDefaults *conf = [NSUserDefaults standardUserDefaults];
    NSString *hosturl =  [conf objectForKey:@"hosturl"];
    //NSString *cusUrl=[NSString stringWithFormat:@"%@?op=%@&sid=%@&json=%@",hosturl,@"addSaleBill",[self getSessionID],jsonString];
    [self setCLIENTRquest:[ASIFormDataRequest requestWithURL:[NSURL URLWithString:hosturl]]];
	[CLIENTRquest setDelegate:self];
    [CLIENTRquest setDidFinishSelector:@selector(pushAddClientFinished:)];
    [CLIENTRquest setDidFailSelector:@selector(pushAddClientFailed:)];
    [CLIENTRquest setPostValue:[self getSessionID] forKey:@"sid"];
    [CLIENTRquest setPostValue:@"addClient" forKey:@"op"];
    [CLIENTRquest setPostValue:jsonString forKey:@"json"];
    [CLIENTRquest startAsynchronous];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowTopView" object:nil];
}

-(void)pushAddClientFinished:(ASIHTTPRequest*)request
{
    //服务器返回错误信息时,发送请求失败的通知
//    NSString *response = [request responseString];
//    NSDictionary *responseDic = [response JSONValue];
    
    NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:nil];
    
    if ([[responseDic objectForKey:@"code"] integerValue] == -1) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"FailedRemoveTopView" object:nil];
        return;
    }
    
    NSString *responseString = [request responseString];
    NSLog(@"%@",responseString);
//    id getData=[responseString JSONValue];
    id getData = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:nil];
    
    if ([getData isKindOfClass:[NSDictionary class]])
    {
        NSString *desc=[getData objectForKey:@"desc"];
        NSNumber *code=[getData objectForKey:@"code"];
        // NSLog(@"desc=%@",desc);
        [pushDelegate dataCallBack:@"YES" other:desc third:code];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopActivity3" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RemoveTopView" object:nil];
    //提交完成,清空json数据
    [_pushJson release];
    _pushJson = nil;
}


-(void)pushAddClientFailed:(ASIHTTPRequest*)request
{
    //    if (delegate && [delegate respondsToSelector:@selector(requestFailed:) ]) {
    //		[delegate performSelector:@selector(requestFailed:) withObject:request];
    //	}
    //    [pushDelegate dataCallBack:@"NO" other:@"网络连接失败" third:-1];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopActivity3" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FailedRemoveTopView" object:nil];
}


-(void)pushDateFailed:(ASIHTTPRequest*)request
{
//    if (delegate && [delegate respondsToSelector:@selector(requestFailed:) ]) {
//		[delegate performSelector:@selector(requestFailed:) withObject:request];
//	}
//    [pushDelegate dataCallBack:@"NO" other:@"网络连接失败" third:-1];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopActivity3" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FailedRemoveTopView" object:nil];
}

//请求完成requestFinished
- (void)authFinished:(ASIHTTPRequest *)request
{
//    NSString *responseString = [request responseString];
//    NSDictionary *dic = [responseString JSONValue];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:nil];
    
    //把用户名保存到沙盒
    [[NSUserDefaults standardUserDefaults] setObject:[dic objectForKey:@"userName"] forKey:@"userName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
   // CCLOG(@"authFinished:%@",responseString);
//    id myData = [responseString JSONValue];
    id myData = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:nil];
    NSString * userid=[myData  objectForKey:@"userid"];
    if(userid){ 
    [self setSid:userid];
    [self saveConf:@"firstRun" value:NO ];
     
    }
    if (delegate && [delegate respondsToSelector:@selector(requestDataFinished:) ]) {
		[delegate performSelector:@selector(requestDataFinished:) withObject:myData];
	} 
    //[OARequest clearDelegatesAndCancel];
}

//请求失败
-(void)authFailed:(ASIHTTPRequest *)request
{
    if (delegate && [delegate respondsToSelector:@selector(requestFailed:) ]) {
		[delegate performSelector:@selector(requestFailed:) withObject:request];
	}
    //NSError *error = [request error];
    //CCLOG(@"requestFailed: %@" ,[error localizedDescription]); 
    /*
    NSArray* detailedErrors = [[error userInfo] objectForKey:NSDetailedErrorsKey];
	if(detailedErrors != nil && [detailedErrors count] > 0) { 
		for(NSError* detailedError in detailedErrors) {
			NSLog(@" DetailedError: %@", [detailedError userInfo]);
		}
        
	}else {
		NSlog(@"%@",[error userInfo]);
	}*/
}

//请求完成requestFinished
- (void)requestFinished:(ASIHTTPRequest *)request
{
//    UIAlertView *alert2=[[UIAlertView alloc]initWithTitle:nil message:@"jiance1" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//    [alert2 show];
//    [alert2 release];
    NSString *responseString = [request responseString];
    NSLog(@"responseString=%@",responseString);
//    id myData = [responseString JSONValue];
    
    id myData = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:nil];
    
//    NSLog(@"myData=%@",myData);
//    if ([myData isKindOfClass:[NSDictionary class]])
//    {
//        if([[myData objectForKey:@"code"] intValue]==-1)
//        {
//            NSLog(@"%@",[myData objectForKey:@"desc"]);
//            [ModalAlert prompt:NSLocalizedString(@"Auth Timeout",@"Auth Timeout")];    
//            if (delegate && [delegate respondsToSelector:@selector(timeoutQuit) ]) {
//                [delegate performSelector:@selector(timeoutQuit) withObject:nil];
//            } 
//            return;
//        }
//    }
    if (delegate && [delegate respondsToSelector:@selector(requestDataFinished:) ]) {
		[delegate performSelector:@selector(requestDataFinished:) withObject:myData];
	} 
}



//-(void)postReplayFinished:(ASIHTTPRequest *)request
//{
//    NSString *responseString = [request responseString];
//    // CCLOG(@"requestFinished:%@",responseString);
//    // [ModalAlert prompt:NSLocalizedString(@"Please Input Mean",@"Please Input Mean")];
//    id myData = [responseString JSONValue];
//    if ([myData isKindOfClass:[NSDictionary class]]) {
//        if([[myData objectForKey:@"code"] intValue]==-1)
//        {
//            [ModalAlert prompt:NSLocalizedString(@"Auth Timeout",@"Auth Timeout")];
//            if (delegate && [delegate respondsToSelector:@selector(timeoutQuit) ]) {
//                [delegate performSelector:@selector(timeoutQuit) withObject:nil];
//            }
//            return;
//        }
//    }
//    if (delegate && [delegate respondsToSelector:@selector(requestReplayDataFinished:) ]) {
//		[delegate performSelector:@selector(requestDataFinished:) withObject:myData];
//	}
//}


- (void)requestFailed:(ASIHTTPRequest *)request
{
//    UIAlertView *alert2=[[UIAlertView alloc]initWithTitle:nil message:@"jiance2" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//    [alert2 show];
//    [alert2 release];
    if (delegate && [delegate respondsToSelector:@selector(requestFailed:)]) {
		[delegate performSelector:@selector(requestFailed:) withObject:request];
	}
    //NSError *error = [request error];
    //CCLOG(@"requestFailed: %@" ,[error localizedDescription]);
}

//操作完成
- (void)postFinished:(ASIHTTPRequest *)request
{
//    NSString *responseString = [request responseString];
   
    //CCLOG(@"postFinished:%@",responseString);  
//    id myData = [responseString JSONValue];
    id myData = [NSJSONSerialization JSONObjectWithData:[request responseData] options:NSJSONReadingMutableContainers error:nil];
    
    if (delegate && [delegate respondsToSelector:@selector(postFinished:) ]) {
		[delegate performSelector:@selector(postFinished:) withObject:myData];
	} 
}
- (void)postFailed:(ASIHTTPRequest *)request
{
    if (delegate && [delegate respondsToSelector:@selector(postFailed:) ]) {
		[delegate performSelector:@selector(postFailed:) withObject:request];
	}
    //NSError *error = [request error];
    //CCLOG(@"requestFailed: %@" ,[error localizedDescription]); 
}

- (void)updateFinished:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSLog(@"responseString=%@",responseString);
    //CCLOG(@"updateFlagFinished.");  
}

- (void)updateFailed:(ASIHTTPRequest *)request
{
    NSString *responseString = [request responseString];
    NSLog(@"responseString=%@",responseString);
    //CCLOG(@"updateFlagFinished.");
}
/*
+ (NSDictionary *)fetchLibraryInformation:(NSString *)urlstr
{
	//
    //NSString *urlString = [NSString stringWithFormat:@"http://192.168.0.54:8080/test/index.jsp?q="];
   NSURL *url = [NSURL URLWithString:urlstr];
	//NSLog(@"fetching library data");
    return [self fetchJSONValueForURL:url];
}

+ (id)fetchJSONValueForURL:(NSURL *)url
{
	//NSError *parseError;
    NSString *jsonString = [[NSString alloc] initWithContentsOfURL:url
                                                          encoding:NSUTF8StringEncoding error:nil ];
	
	//if (parseError) {
	 	//[WebService handleError: parseError];
		//return @"";
	//}  
    id jsonValue = [jsonString JSONValue];
    [jsonString release];
    return jsonValue;
}

+ (void)handleError:(NSError *)error
{
    NSString *errorMessage = [error localizedDescription];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cannot Load Data"
														message:errorMessage
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
    [alertView show];
    [alertView release];
}



+(id)getWebServiceData:(NSString *)urlstr
{	
	NSURL *url = [NSURL URLWithString:urlstr];
	ASIHTTPRequest *request = [ ASIHTTPRequest requestWithURL:url]  ;
	[request addRequestHeader:@"Content-Type" value:@"text/x-gwt-rpc; charset=utf-8"];
	[request setDefaultResponseEncoding:NSUTF8StringEncoding];
	[request setResponseEncoding:NSUTF8StringEncoding];
	[request startSynchronous];
	NSError *error = [ request error]  ;
	if (!error) {
		int statusCode = [request responseStatusCode];
			//dlog(@"status code:%d",statusCode);
			if (statusCode==200) {
				NSString *responsestr = [[request responseString] retain];
				//dlog(responsestr);
				id jsonValue = [  responsestr JSONValue]  ;
				[responsestr release];
				return jsonValue;
			}else {
				NSDictionary * errorInfo=[NSDictionary dictionaryWithObjectsAndKeys:@"not found url" ,@"errorKey", nil];
				error=[NSError  errorWithDomain:@"newstocks" code:404 userInfo:  errorInfo]; 
				[self handleError:error];
			}
	
	}else{
		[self handleError:error];
		return nil;
	}
	
	//NSString *responsestr = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil ];
	return nil;	
}
*/
/*

//【1】http  GET请求


//注意url是经过URL编码之后的

- (NSString*) sendXMLRequest: (NSString*)url;
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //NOTE with this way, cookie is send automatically, so it can be ignored
	int TIME_OUT=10000;
	//创建NSURLRequest
	NSString* urlEncoding = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	NSURLRequest* urlrequest = [NSURLRequest requestWithURL:[NSURL URLWithString:urlEncoding] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:TIME_OUT];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	
	//通过NSURLConnection 发送NSURLRequest，这里是同步的，因此会又等待的过程，TIME_OUT为超时时间。
	
	//error可以获取失败的原因。
    NSError* error = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:urlrequest returningResponse:NULL error:&error];
    if(!error){
        NSString *stringData = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
        return stringData;
    }
    NSString* errorString = [NSString stringWithFormat:@"<error string=\"%@\"/>", [error localizedDescription]];
    return errorString;
}

//【2】http POST请求

//url是请求的目的URL地址，body是要发送的数据

- (NSString*) postURLRequest: (NSString*)url body:(NSString*)body
{
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString: url] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSData* data = [NSURLConnection sendSynchronousRequest:request returningResponse:NULL error:NULL];
    
    NSString *stringData = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    return stringData;
}

 
 
 
 ASIHTTPRequest，是一个直接在CFNetwork上做的开源项目，提供了一个比官方更方便更强大的HTTP网络传输的封装。
 特色功能如下：
 1，下载的数据直接保存到内存或文件系统里
 2，提供直接提交(HTTP POST)文件的API
 3，可以直接访问与修改HTTP请求与响应HEADER
 4，轻松获取上传与下载的进度信息
 5，异步请求与队列，自动管理上传与下载队列管理机
 6，认证与授权的支持
 7，Cookie
 8，请求与响应的GZIP
 9，代理请求
 
 
 下面来两个小例子：
 NSURL *url = [NSURL URLWithString:@"http://www.baidu.com"];
 ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
 [request start];
 NSError *error = [request error];
 if (!error) {
 NSString *response = [request responseString];
 }
 
 当你需要添加更多的请求信息时，如，添加个请求Header：
 [request addRequestHeader:@"name" value:@"Jory lee"];
 
 添加Post请求时的健值:
 [request setPostValue:@"Ben" forKey:@"first_name"];
 [request setPostValue:@"Copsey" forKey:@"last_name"];
 [request setFile:@"/Users/ben/Desktop/ben.jpg" forKey:@"photo"];
 
 设置HTTP的授权帐号：
 [request setUsername:@"username"];
 [request setPassword:@"password"];
 
 一个异步请求：
 - (IBAction)grabURLInBackground:(id)sender
 {
 NSURL *url = [NSURL URLWithString:@"http://allseeing-i.com"];
 ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
 [request setDelegate:self];
 [request startAsynchronous];
 }
 
 - (void)requestFinished:(ASIHTTPRequest *)request
 {
 // Use when fetching text data
 NSString *responseString = [request responseString];
 
 // Use when fetching binary data
 NSData *responseData = [request responseData];
 }
 
 - (void)requestFailed:(ASIHTTPRequest *)request
 {
 NSError *error = [request error];
 }   
 
 在我们数据获取的过程中，如果数据源复杂，一个请求队列是必不可少的：
 - (IBAction)grabURLInTheBackground:(id)sender
 {
 if (![self queue]) {
 [self setQueue:[[[NSOperationQueue alloc] init] autorelease]];
 }
 
 NSURL *url = [NSURL URLWithString:@"http://allseeing-i.com"];
 ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
 [request setDelegate:self];
 [request setDidFinishSelector:@selector(requestDone:)];
 [request setDidFailSelector:@selector(requestWentWrong:)];
 [[self queue] addOperation:request]; //queue is an NSOperationQueue
 }
 
 - (void)requestDone:(ASIHTTPRequest *)request
 {
 NSString *response = [request responseString];
 }
 
 - (void)requestWentWrong:(ASIHTTPRequest *)request
 {
 NSError *error = [request error];
 }   
 
 
 
 ASIHTTP
 
 NSURL *url = [NSURL URLWithString:@"请求地址"];
 //ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
 ASIFormDataRequest *request=[ASIFormDataRequest requestWithURL:url];
 [request setPostValue:@"值" forKey:@"参数1"];
 [request setPostValue:@"值" forKey:@"参数2"];
 
 [request start];
 NSError *error = [request error];
 if (!error) {
 NSString *response = [request responseString];
 NSLog(response);
 }
 
 添加允许从已存在文件下载：
 [request setAllowResumeForFileDownloads:YES];
 
 当然还要添加下载文件目录的设置：
 [request setTemporaryFileDownloadPath:[[paths objectAtIndex:0] stringByAppendingPathComponent:@"test2.tmp"]];
 [request setDownloadDestinationPath:[[paths objectAtIndex:0] stringByAppendingPathComponent:@"test2.mp3"]];
 

- (void)loadServerData:(NSString *)paramter {
	// Add the following line if you want the list to be editable
	// self.navigationItem.leftBarButtonItem = self.editButtonItem;
	// init the url.
	NSString *host = @"http://192.168.0.52:8080/test/index.jsp?q=";
	//NSString *queryString = @"dd" ;//querySearchBar.text;
	//NSString *jsonURL = [NSString stringWithFormat:@"http://192.168.0.52:8080/test/index.jsp?q=%@", queryString];
	NSString *jsonURL = [host stringByAppendingString: @"vvv"];
	//NSLog(jsonURL);
	//NSString *jsonURL =  @"http://localhost:8080/test/index.jsp" ;
	
	//NSURL *jsonURL = [NSURL URLWithString:@"http://localhost:8080/test/index.jsp"];
	
	//NSLog(jsonURL);
	
	 
	 loading the contents of the URL into a string. At this point we should hit
	 the json endpoint and put the contents of it in jsonData.
	 
	//NSString *jsonData = [[NSString alloc] initWithContentsOfURL:jsonURL];
	
	
	// 初始化請求
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];         
	// 設置URL
	[request setURL:[NSURL URLWithString:jsonURL]];
	//[request setURL:jsonURL];
	
	
	// 設置HTTP方法
	[request setHTTPMethod:@"GET"];
	// 發送同步請求, 這裡得returnData就是返回得數據楽
	NSURLResponse *response = nil;
	NSError *error = nil;
	
	//NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil]; 
	NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	// 釋放對象
	NSString *jsonData = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
	
	//[self handleError:error];
	[request release];	
	
	 
	// we can verify the contents by looking at a NSLog if we open the console
	  
	//NSLog(jsonData);
	
	// converting the json data into an array
	//self.jsonArray = [jsonData JSONValue]; 
	
	// this will return the array.. in the console it will still look a bit like json
	// but that is a array object.
	//NSLog(@"%@", jsonArray);
	
	// this will give the current count
	//NSLog(@"count is: %i", [self.jsonArray count]);
	// releasing the vars now
	
	
	[jsonURL release];
	[host release];
	[jsonData release];
}

*/
@end
