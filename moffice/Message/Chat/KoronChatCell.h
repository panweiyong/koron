//
//  KoronChatCell.h
//  moffice
//
//  Created by Koron on 13-9-6.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KoronChatCell : UITableViewCell {
    UIImageView *_headImageView;
    UIImageView *_chatImageView;
    UILabel *_chatLabel;
    UILabel *_nameLabel;
    BOOL isWho;
}

@end
