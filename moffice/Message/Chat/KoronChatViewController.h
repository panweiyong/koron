//
//  KoronChatViewController.h
//  moffice
//
//  Created by Koron on 13-9-6.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBJson.h"
#import "ASIFormDataRequest.h"
#import "KoronMessageViewController.h"
#import "KoronChatModel.h"
#import "KoronPrivateMessageDB.h"
#import "KoronCopyLabel.h"
#import "SVProgressHUD.h"


@interface KoronChatViewController : KoronMessageViewController <ASIHTTPRequestDelegate,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate> {
    ASIFormDataRequest *_receveChatMsgRequest;
    ASIFormDataRequest *_sendChatMsgRequest;
    ASIFormDataRequest *_receveNewMsgRequest;
    
    //记录最后一条消息的ID,初始化为空串
    NSString *_lasstId;
    
    UIImageView *_topView;
    //联系人ID
    NSString *_linkManID;
    
    NSMutableArray *_chatModelArr;
    //记录时间标题出现的时间
    NSDate *_date;
    
    //联系人名字
    NSString *_linkManName;
    //标题
    UILabel *_topLabel;
    
    UIImageView *_underView;
    //输入框
    UITextView *_textView;
    //输入框上的label
    UILabel *_textViewLabel;
    
    //记录键盘的高度
    CGFloat _y;
    
    BOOL isImplementbserver;
    //是否第一次加载
    BOOL isFirst;
    //每次加载完滚动到指定的cell
    NSInteger _scrollToRow;
    //聊天的类型(群聊/私聊)
    NSString *_type;
    //保存输入框的内容
    NSString *_content;
    
    UIButton *_sendBtn;
    
    BOOL isSend;
    
    BOOL isChange;

}

@property (nonatomic,retain)NSString *linkManID;

- (id)initWithLinkMan:(NSString *)linkManID linkManName:(NSString *)linkManname type:(NSString *)type;

@end
