//
//  KoronChatCell.m
//  moffice
//
//  Created by Koron on 13-9-6.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronChatCell.h"

@implementation KoronChatCell

- (void)dealloc
{
    RELEASE_SAFELY(_headImageView);
    RELEASE_SAFELY(_chatImageView);
    RELEASE_SAFELY(_chatLabel);
    
    [super dealloc];
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
//        _headImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
//        _chatImageView = [[UIImageView alloc]initWithFrame:CGRectZero];
//        _chatLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)sendMsg:(CGSize)size {
    if (isWho) {
        [_headImageView setFrame:CGRectMake(5, 5, 40, 40)];
//        UIImage *image = [UIImage imageNamed:@"chat_send_press.png"];
//        UIImage *newImage = [image resizableImageWithCapInsets:UIEdgeInsetsMake(51, 40, 30, 40)];
        [_chatImageView setFrame:CGRectMake(0, 0, size.width, size.height)];
    }
}



@end
