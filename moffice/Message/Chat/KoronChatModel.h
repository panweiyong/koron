//
//  KoronChatModel.h
//  moffice
//
//  Created by Koron on 13-9-6.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "KoronCopyLabel.h"
#import "KoronPrivateMessageDB.h"
#import "UIImage+Extension.h"
#import "KoronPrivateMessageDB.h"
#import "KoronManager.h"

@interface KoronChatModel : NSObject {
    BOOL _isWho;
    NSString *_chatContent;
    UIView *_chatView;
    CGFloat _height;
    NSString *_chatID;
    
    NSString *_hostURL;
    NSString *_timeText;
    BOOL isHiden;
    UILabel *_timeLabel;
    NSInteger _y;
    
    NSString *_name;
    NSString *_gender;
    BOOL isGoup;
    
    KoronManager *_manager;
}

@property (nonatomic,assign)BOOL isWho;
@property (nonatomic,retain)NSString *chatContent;
@property (nonatomic,retain)UIView *chatView;
@property (nonatomic,assign)CGFloat height;
@property (nonatomic,retain)NSString *chatID;
@property (nonatomic,retain)UILabel *timeLabel;
@property (nonatomic,assign)BOOL isHiden;
@property (nonatomic,retain)NSString *timeText;
@property (nonatomic,retain)NSString *gender;
@property (nonatomic,assign)BOOL isGoup;
@property (nonatomic,retain)NSString *name;

- (void)setChatViewFrame:(CGRect)frame;
- (void)sendMsg;


@end
