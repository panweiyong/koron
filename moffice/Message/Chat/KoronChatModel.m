//
//  KoronChatModel.m
//  moffice
//
//  Created by Koron on 13-9-6.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronChatModel.h"

@implementation KoronChatModel
@synthesize isWho = _isWho;
@synthesize chatContent = _chatContent;
@synthesize chatView = _chatView;
@synthesize height = _height;
@synthesize chatID = _chatID;
@synthesize timeLabel = _timeLabel;
@synthesize isHiden;
@synthesize timeText = _timeText;
@synthesize gender = _gender;
@synthesize isGoup;
@synthesize name = _name;

- (void)dealloc
{
    RELEASE_SAFELY(_name);
    RELEASE_SAFELY(_timeLabel);
    RELEASE_SAFELY(_chatContent);
    RELEASE_SAFELY(_chatID);
    RELEASE_SAFELY(_chatView);
    RELEASE_SAFELY(_hostURL);
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        
        _manager = [KoronManager sharedManager];
    }
    return self;
}

- (void)setChatViewFrame:(CGRect)frame {
    _chatView = [[UIView alloc]initWithFrame:frame];
    _chatView.userInteractionEnabled = YES;
    _hostURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"hosturl"];
    NSRange range = [_hostURL rangeOfString:@"/moffice"];
    _hostURL = [[_hostURL substringToIndex:range.location] copy];
}


- (void)sendMsg {
    NSLog(@"_chatID  == %@",_chatID);
    UIFont *font = [UIFont systemFontOfSize:16];
    CGSize maxSize = CGSizeMake(220, 20000);
    //动态计算字符串的高度和宽度
    //第一个参数  指定一个字体大小
    //第二个参数  指定最大的宽度和高度
    //第三个参数  折行方式   NSLineBreakByCharWrapping(以单词的方式折行)
    CGSize realSize = [_chatContent sizeWithFont:font constrainedToSize:maxSize lineBreakMode:NSLineBreakByCharWrapping];
    if (!isHiden) {
        _y = 20;
    }else {
        _y = 50;
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(120, 5, 80, 20)];
        _timeLabel.backgroundColor = [UIColor grayColor];
        _timeLabel.alpha = 0.3;
        _timeLabel.textAlignment = UITextAlignmentCenter;
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.font = [UIFont systemFontOfSize:10];
        _timeLabel.layer.masksToBounds = YES;
        _timeLabel.layer.cornerRadius = 8;

        if ([self computationTime:_timeText] >= 1 && [self computationTime:_timeText] < 2) {
            _timeLabel.text = [NSString stringWithFormat:@"昨天 %@",[_timeText substringWithRange:NSMakeRange(11, 5)]];
        }else if ([self computationTime:_timeText] >= 2 && [self computationTime:_timeText] < 3) {
            _timeLabel.text = [NSString stringWithFormat:@"前天 %@",[_timeText substringWithRange:NSMakeRange(11, 5)]];
        }else if ([self computationTime:_timeText] < 1) {
            _timeLabel.text = [NSString stringWithFormat:@"今天 %@",[_timeText substringWithRange:NSMakeRange(11, 5)]];
        }else {
            _timeLabel.text = [_timeText substringWithRange:NSMakeRange(5, 11)];
        }
        [_chatView addSubview:_timeLabel];
    }
    
    
    if (_isWho) {
        
        //根据realSize拉伸气泡
        UIImage *img = [UIImage imageNamed:@"chat_send_press.png"];
        //拉伸方法
        UIImage *newImg = [img resizableImageWithCapInsets:UIEdgeInsetsMake(30, 20, 20, 25)];
        UIImageView *imgView = [[UIImageView alloc]initWithImage:newImg];
        CGSize imgViewSize = realSize;
        if ([_chatContent isEqualToString:@""] || imgViewSize.width == 0) {
            imgViewSize.width = img.size.width;
            imgViewSize.height = img.size.height-5;
        }else {
            imgViewSize.height += 2*15;
            imgViewSize.width += 2*20;
        }
        
        [imgView setFrame:CGRectMake(_chatView.bounds.size.width - imgViewSize.width -50, _y, imgViewSize.width, imgViewSize.height)];
        imgView.userInteractionEnabled = YES;
        [_chatView addSubview:imgView];
        _height = imgView.bounds.size.height + _y;
        [imgView release];
        
        KoronCopyLabel *label = [[KoronCopyLabel alloc]initWithFrame:CGRectMake(10, 15, realSize.width, realSize.height)];

        label.text = _chatContent;
        label.backgroundColor = [UIColor clearColor];
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeCharacterWrap;
        label.font = font;
        [imgView addSubview:label];
        [label release];
        
        UIImageView *headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(320 - 45, 5 + _y - 20, 40, 40)];
        UIImage *headImg = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:_chatID];
        KoronPrivateMessageDB *db = [[KoronPrivateMessageDB alloc]init];
        FMResultSet *rs = [db selectName:_chatID];
        if (rs.next) {
            _gender = [[rs stringForColumn:@"gender"] retain];
        }
        if (headImg) {
            [headImageView setImage:[UIImage createRoundedRectImage:headImg]];
        }else {
            if ([_gender isEqualToString:@"男"]) {
                [headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"default_head.jpg"]]];
            }else {
                [headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"default_head_woman.jpg"]]];
            }
        }
        
        [db.dataBase close];
        RELEASE_SAFELY(db);
        
        [_chatView addSubview:headImageView];
        [headImageView release];
        
        [_chatView setFrame:CGRectMake(_chatView.frame.origin.x, _chatView.frame.origin.y, _chatView.frame.size.width, _height)];
        
    } else {
        //根据realSize拉伸气泡
        UIImage *img = [UIImage imageNamed:@"chat_recive_press.png"];
        //拉伸方法
        UIImage *newImg = [img resizableImageWithCapInsets:UIEdgeInsetsMake(30, 25, 20, 20)];
        UIImageView *imgView = [[UIImageView alloc]initWithImage:newImg];
        imgView.userInteractionEnabled = YES;
        CGSize imgViewSize = realSize;
        //计算支持气泡和拉伸边距的真实尺寸
        if ([_chatContent isEqualToString:@""] || imgViewSize.width == 0) {
            imgViewSize.width = img.size.width;
            imgViewSize.height = img.size.height-5;
        }else {
            imgViewSize.height += 2*15;
            imgViewSize.width += 2*20;
        }
        
        [imgView setFrame:CGRectMake(50,_y, imgViewSize.width, imgViewSize.height)];
        _height = imgView.bounds.size.height + _y;
        [_chatView addSubview:imgView];
        [imgView release];
        
        KoronCopyLabel *label = [[KoronCopyLabel alloc]initWithFrame:CGRectMake(15, 15, realSize.width, realSize.height)];
        label.text = _chatContent;
        label.backgroundColor = [UIColor clearColor];
        label.numberOfLines = 0;
        label.lineBreakMode = UILineBreakModeCharacterWrap;
        label.font = font;
        [imgView addSubview:label];
        
        [label release];
        
        
        KoronPrivateMessageDB *db = [[KoronPrivateMessageDB alloc]init];
        UIImageView *headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5 + _y - 20, 40, 40)];
        UIImage *headImg = [[SDImageCache sharedImageCache] imageFromMemoryCacheForKey:_chatID];
       
        FMResultSet *result = [db selectName:_chatID];
        if (result.next) {
            _gender = [[result stringForColumn:@"gender"] retain];
        }
        
        if (headImg) {
            [headImageView setImage:headImg];
        }else {
            if ([_gender isEqualToString:@"男"]) {
                [headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"default_head.jpg"]]];
            }else {
                [headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"default_head_woman.jpg"]]];
            }
        }
        
        [_chatView addSubview:headImageView];
        [headImageView release];
        
        [_chatView setFrame:CGRectMake(_chatView.frame.origin.x, _chatView.frame.origin.y, _chatView.frame.size.width, _height)];
        
        if (isGoup) {
            
            FMResultSet *rs = [db selectName:_chatID];
            
            UILabel *namelabel = [[UILabel alloc]initWithFrame:CGRectMake(60,5 + _y - 20  , 200, 12)];
            namelabel.backgroundColor = [UIColor clearColor];
            namelabel.textColor = [UIColor grayColor];
            namelabel.font = [UIFont systemFontOfSize:12];
            namelabel.textAlignment = NSTextAlignmentLeft;
            if (rs.next) {
                namelabel.text = [rs stringForColumn:@"name"];
            }
            [_chatView addSubview:namelabel];
            RELEASE_SAFELY(namelabel);
        }
        [db.dataBase close];
        RELEASE_SAFELY(db);
    }
}


- (NSInteger)computationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowadayTimeStr = [fromatter stringFromDate:[NSDate date]];
    [fromatter release];
    
    if ([[nowadayTimeStr substringToIndex:3] integerValue] - [[timeStr substringToIndex:3] integerValue] == 0) {
        if ([[nowadayTimeStr substringWithRange:NSMakeRange(5, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(5, 2)] integerValue] == 0) {
            if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 0) {
                return 0;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
                return 1;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 2) {
                return 2;
            }else {
                return 4;
            }
        }else {
            return 4;
        }
        
    }else {
        return 4;
    }
    
}


@end
