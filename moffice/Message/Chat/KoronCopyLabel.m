//
//  KoronCopyLabel.m
//  moffice
//
//  Created by Hsn on 13-9-7.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronCopyLabel.h"

@implementation KoronCopyLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self attachTapHandler];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

//能成为第一响应者
- (BOOL)canBecomeFirstResponder {
    return YES;
}


//UILabel默认是不接收事件的，需要自己添加touch事件
-(void)attachTapHandler
{
    self.userInteractionEnabled = YES;
    UILongPressGestureRecognizer *touch = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    touch.minimumPressDuration = 1.0;
    
    [self addGestureRecognizer:touch];
    [touch release];
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self attachTapHandler];
}


-(void)handleTap:(UILongPressGestureRecognizer *) recognizer
{
    [self becomeFirstResponder];
    UIMenuController *menu = [UIMenuController sharedMenuController];
    [menu setTargetRect:self.frame inView:self.superview];
    [menu setMenuVisible:YES animated:YES];
}

// 可以响应的方法
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return (action == @selector(copy:));
}

//针对于响应方法的实现
- (void)copy:(id)sender {
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.text;
}


@end
