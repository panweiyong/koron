//
//  KoronChatViewController.m
//  moffice
//
//  Created by Koron on 13-9-6.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronChatViewController.h"


@interface KoronChatViewController ()

@end

@implementation KoronChatViewController

@synthesize linkManID = _linkManID;




- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_receveNewMsgRequest clearDelegatesAndCancel];
    RELEASE_SAFELY(_receveNewMsgRequest);
    [_receveChatMsgRequest clearDelegatesAndCancel];
    RELEASE_SAFELY(_receveChatMsgRequest);
    [_sendChatMsgRequest clearDelegatesAndCancel];
    RELEASE_SAFELY(_sendChatMsgRequest);
    
    
    RELEASE_SAFELY(_chatModelArr);
    RELEASE_SAFELY(_linkManName);
    RELEASE_SAFELY(_type);
    
    
    RELEASE_SAFELY(_topView);
    RELEASE_SAFELY(_topLabel);
    RELEASE_SAFELY(_textView);
    RELEASE_SAFELY(_underView);
    RELEASE_SAFELY(_sendBtn);
    RELEASE_SAFELY(_textViewLabel);
    
    
    [super dealloc];
}


- (id)initWithLinkMan:(NSString *)linkManID linkManName:(NSString *)linkManname type:(NSString *)type
{
    self = [super init];
    if (self) {
        
        if (IOS_VERSIONS >= 7.0) {
            [self setNeedsStatusBarAppearanceUpdate];
        }
        
        isFirst = YES;
        
        self.linkManID = linkManID;
        [self.view setFrame:CGRectMake(0, 0, self.view.bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
        
        _linkManName = [linkManname copy];
        _type = [type copy];
        _lasstId = @"";

        [self receveChatMessage];
        
        
        
        UIImage *backgroundImage = [UIImage imageNamed:@"chat_background.png"];
        
        UIImage *backgroundNewImage = [backgroundImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        UIImageView *backgroundImageView = [[UIImageView alloc]initWithImage:backgroundNewImage];
        [backgroundImageView setFrame:self.view.frame];
        
        UIImage *topImage = [UIImage imageNamed:@"home_top.png"];
        UIImage *newTopImage = [topImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 1, 0)];
        if (IOS_VERSIONS < 7.0) {
            _topView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,45)];
        }else {
            _topView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20, self.view.bounds.size.width,45)];
        }
        
        _topView.userInteractionEnabled = YES;
        [_topView setImage:newTopImage];
        [self.view addSubview:_topView];
        _topLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 200, 34)];
        _topLabel.textAlignment = NSTextAlignmentCenter;
        _topLabel.textColor = [UIColor grayColor];
        _topLabel.backgroundColor = [UIColor clearColor];
        _topLabel.text = _linkManName;
        [_topView addSubview:_topLabel];
        
        
        UIButton *dismissBtn = [[UIButton alloc]init];
        [dismissBtn setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
        [dismissBtn setFrame:CGRectMake(5, 2, 40, 40)];
        [dismissBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
        [_topView addSubview:dismissBtn];
        
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor clearColor];
        [_tableView setBackgroundView:backgroundImageView];
        RELEASE_SAFELY(backgroundImageView);
        
        
        [_tableView setFrame:CGRectMake(_tableView.bounds.origin.x, _topView.frame.origin.y + _topView.bounds.size.height, _tableView.bounds.size.width, [UIScreen mainScreen].bounds.size.height-45-70)];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tableviewTapGestureRecognizer)];
        [_tableView addGestureRecognizer:tap];
        RELEASE_SAFELY(tap);
        
        _underView = [[UIImageView alloc]initWithFrame:CGRectMake(0, _tableView.frame.origin.y + _tableView.bounds.size.height, self.view.bounds.size.width, 50)];
        _underView.backgroundColor = [UIColor colorWithRed:56/255.0 green:57/255.0 blue:66/255.0 alpha:1];
        _underView.userInteractionEnabled = YES;
        
        _textView = [[UITextView alloc]initWithFrame:CGRectMake(10, 10, 260, 30)];
        _textView.delegate = self;
        _textView.font = [UIFont systemFontOfSize:16];
        _textView.layer.masksToBounds = YES;
        _textView.layer.cornerRadius = 5;
        _textView.text = @"";
        _textView.scrollEnabled = NO;
        
        
        _textViewLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 30)];
        _textViewLabel.font = [UIFont systemFontOfSize:16];
        _textViewLabel.backgroundColor = [UIColor clearColor];
        _textViewLabel.textColor = [UIColor grayColor];
        _textViewLabel.text = @"输入消息";
        [_textView addSubview:_textViewLabel];
        

        _sendBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.view.bounds.size.width - 45, _underView.bounds.size.height - 45, 40, 40)];
        [_sendBtn setImage:[UIImage imageNamed:@"all_icon_send_simple.png"] forState:UIControlStateNormal];
        [_sendBtn addTarget:self action:@selector(sendChatMessage) forControlEvents:UIControlEventTouchUpInside];
        [_underView addSubview:_sendBtn];
        RELEASE_SAFELY(_sendBtn);
        [_underView addSubview:_textView];
        [self.view addSubview:_underView];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShowNotification:) name:@"UIKeyboardWillShowNotification" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHideNotification:) name:@"UIKeyboardWillHideNotification" object:nil];
        //收到私信时,接收通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privateLetterNotice) name:@"Privateletternotice" object:nil];
        
        
        isChange = YES;
        
        if (IOS_VERSIONS >= 7.0) {
            UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
            topView.backgroundColor = [UIColor blackColor];
            [self.view addSubview:topView];
            [self.view bringSubviewToFront:topView];
        }
    }
    return self;
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView
{
    
    UIFont * font = [UIFont systemFontOfSize:16];
    CGSize maxSize = CGSizeMake(250, 60);
    
    CGSize realSize = [textView.text sizeWithFont:font constrainedToSize:maxSize lineBreakMode:NSLineBreakByCharWrapping];
    
    CGRect frame = textView.frame;
    frame.size.height = realSize.height+10;

//    NSLog(@"%f",realSize.height);
//    if (realSize.height >= 80) {
//        textView.scrollEnabled = YES;
//    }else {
//        textView.scrollEnabled = NO;
//    }
    textView.scrollEnabled = YES;
    if (realSize.height == 0) {
        [_textView addSubview:_textViewLabel];
    }else {
        [_textViewLabel removeFromSuperview];
    }
    
    
    if (realSize.height < 30) {
        [_textView setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 30)];
        if (IOS_VERSIONS < 7.0) {
            [_underView setFrame:CGRectMake(0, _y - (20 + 50), self.view.bounds.size.width, 50)];
        }else {
            [_underView setFrame:CGRectMake(0, _y - 50, self.view.bounds.size.width, 50)];
        }
        
    }else {
        [_textView setFrame:frame];
        if (IOS_VERSIONS < 7.0) {
            [_underView setFrame:CGRectMake(0, _y - (realSize.height + 50), self.view.bounds.size.width, frame.size.height + 20)];
        }else {
            [_underView setFrame:CGRectMake(0, _y - (realSize.height + 30), self.view.bounds.size.width, frame.size.height + 20)];
        }
        
    }
    if (_y == [[UIScreen mainScreen] bounds].size.height) {
        if (IOS_VERSIONS < 7.0) {
            [_tableView setFrame:CGRectMake(0, 46, _tableView.bounds.size.width, [UIScreen mainScreen].bounds.size.height-45-70)];
        }else {
            [_tableView setFrame:CGRectMake(0, _topView.frame.origin.y + _topView.bounds.size.height, _tableView.bounds.size.width, [UIScreen mainScreen].bounds.size.height-45-50)];
        }
        
    }else {
        if (IOS_VERSIONS < 7.0) {
            [_tableView setFrame:CGRectMake(0, _y - _tableView.bounds.size.height - _underView.bounds.size.height - 20, _tableView.bounds.size.width, _tableView.bounds.size.height)];
        }else {
            [_tableView setFrame:CGRectMake(0, _y - _tableView.bounds.size.height - _underView.bounds.size.height, _tableView.bounds.size.width, _tableView.bounds.size.height)];
        }
        
    }
    
    [_sendBtn setFrame:CGRectMake(_sendBtn.frame.origin.x, (_underView.bounds.size.height + _underView.frame.origin.y) - 45 , _sendBtn.bounds.size.width, _sendBtn.bounds.size.height)];
    
}


- (void)textViewDidChangeSelection:(UITextView *)textView {
    NSLog(@"textViewDidChangeSelection");
}




- (void)tableviewTapGestureRecognizer {
    [_textView resignFirstResponder];
}


#pragma mark - dismissViewController

- (void)dismissViewController {
    CATransition *myAnimation = [CATransition animation];
    myAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    myAnimation.duration = 0.4;

    myAnimation.type = kCATransitionPush;
    myAnimation.subtype = kCATransitionFromLeft;
    
    [[[[[UIApplication sharedApplication] delegate] window] layer] addAnimation:myAnimation forKey:nil];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_chatModelArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"ChatCell"];
    if (nil == cell) {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatCell"] autorelease];
    }
    cell.backgroundColor = [UIColor clearColor];
//    cell.contentView.userInteractionEnabled = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    KoronChatModel *chatModel = [_chatModelArr objectAtIndex:indexPath.row];
    [cell.contentView addSubview:chatModel.chatView];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    KoronChatModel *chatModel = [_chatModelArr objectAtIndex:indexPath.row];
    return chatModel.height;
}




#pragma mark - keyboardWillHideNotification,keyboardWillShowNotification

- (void)keyboardWillShowNotification:(NSNotification *)note {
    NSDictionary *dictionary = [note userInfo];
    CGRect rect = [[dictionary objectForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    NSLog(@"userInfo  sa%@",[note userInfo]);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0.035];
    if (IOS_VERSIONS < 7.0) {
        [_underView setFrame:CGRectMake(0, rect.origin.y - _underView.bounds.size.height - 20,_underView.bounds.size.width ,_underView.bounds.size.height )];
        [_tableView setFrame:CGRectMake(0, rect.origin.y - _tableView.bounds.size.height - _underView.bounds.size.height - 20, _tableView.bounds.size.width, _tableView.bounds.size.height)];
    }else {
        [_underView setFrame:CGRectMake(0, rect.origin.y - _underView.bounds.size.height,_underView.bounds.size.width ,_underView.bounds.size.height )];
        [_tableView setFrame:CGRectMake(0, rect.origin.y - _tableView.bounds.size.height - _underView.bounds.size.height, _tableView.bounds.size.width, _tableView.bounds.size.height)];
    }
    
    

    
    [UIView commitAnimations];
    
    _y = rect.origin.y;
}


- (void)keyboardWillHideNotification:(NSNotification *)note {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0.015];
    if (IOS_VERSIONS < 7.0) {
        [_underView setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - _underView.bounds.size.height - 20 , self.view.bounds.size.width, _underView.bounds.size.height)];
        [_tableView setFrame:CGRectMake(_tableView.bounds.origin.x, 45, _tableView.bounds.size.width, [UIScreen mainScreen].bounds.size.height - 45 - _underView.bounds.size.height - 20)];
    }else {
        [_underView setFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height - _underView.bounds.size.height , self.view.bounds.size.width, _underView.bounds.size.height)];
        [_tableView setFrame:CGRectMake(_tableView.bounds.origin.x, _topView.frame.origin.y + _topView.bounds.size.height, _tableView.bounds.size.width, [UIScreen mainScreen].bounds.size.height - 45 - _underView.bounds.size.height - 20)];
    }
    

    [UIView commitAnimations];
    
    NSDictionary *dictionary = [note userInfo];
    CGRect rect = [[dictionary objectForKey:@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    NSLog(@"userInfo  sa%@",[note userInfo]);
    _y = rect.origin.y;
}




#pragma mark - Request,RequestDelegate

/*
 网络请求:
 第一次加载消息和下拉加载更多消息
 relationId  对方的ID
 type  person    group    dept
 lasstId  最后一条消息的id   重新加载lasstId为空
 */

- (void)receveChatMessage {
    if ( nil != _receveChatMsgRequest) {
        [_receveChatMsgRequest clearDelegatesAndCancel];
        RELEASE_SAFELY(_receveChatMsgRequest);
    }
    NSString *hostURL = [[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL];
    _receveChatMsgRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:hostURL]];
    _receveChatMsgRequest.delegate = self;
    [_receveChatMsgRequest setDidFinishSelector:@selector(receveChatMsgRequestDidFinish:)];
    [_receveChatMsgRequest setDidFailSelector:@selector(receveChatMsgRequestDidFail:)];
    [_receveChatMsgRequest setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"sid"] forKey:@"sid"];
    [_receveChatMsgRequest setPostValue:@"receveChatMsg" forKey:@"op"];
    [_receveChatMsgRequest setPostValue:_linkManID forKey:@"relationId"];
    [_receveChatMsgRequest setPostValue:_type forKey:@"type"];
    
    [_receveChatMsgRequest setPostValue:_lasstId forKey:@"lasstId"];
    
    [_receveChatMsgRequest startAsynchronous];
    
}

/*
 下拉刷新
 */
- (void)beginSendRequst {
    [self receveChatMessage];
}


//加载消息成功
- (void)receveChatMsgRequestDidFinish:(ASIFormDataRequest *)response {
    NSLog(@"receveChatMsgRequestDidFinish  %@",[response responseString]);
//    NSDictionary *dictionary = [[response responseString] JSONValue];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil];
    
    if ([[dictionary objectForKey:@"code"] integerValue] == -1) {
        [super doneLoadingTableViewData];
        return;
    }
    
    if (nil == _chatModelArr) {
        _chatModelArr = [[NSMutableArray alloc]init];
    }
    
    NSLog(@"receveChatMsgRequestDidFinish  %@",[response responseString]);
    NSArray *arr = [dictionary objectForKey:@"list"];
    _scrollToRow = [arr count];
    
    if(_date) {
        RELEASE_SAFELY(_date);
    }
    NSString *dateStr = [[arr objectAtIndex:0] objectForKey:@"time"];
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    _date = [[fromatter dateFromString:dateStr] retain];
    [fromatter release];
    
    
    if (isFirst) {
        int i = 0;
        for (NSDictionary *chatDictionary in arr) {
            KoronChatModel *chatView = [[KoronChatModel alloc]init];
            if (i == 0) {
                chatView.timeText = [chatDictionary objectForKey:@"time"];
                if (_lasstId) {
                    RELEASE_SAFELY(_lasstId);
                }
                _lasstId = [[chatDictionary objectForKey:@"id"] copy];
                chatView.isHiden = YES;
            }
            if ([self computationTime:[chatDictionary objectForKey:@"time"]]) {
                chatView.isHiden = YES;
            }
            
            if ([[chatDictionary objectForKey:@"send"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"sid"]]) {
                chatView.isWho = YES;
                [chatView setChatViewFrame:CGRectMake(0, 0, 320, 0)];
            }else {
                [chatView setChatViewFrame:CGRectMake(0, 0, 320, 0)];
            }
            chatView.chatID = [chatDictionary objectForKey:@"send"];
            chatView.chatContent = [chatDictionary objectForKey:@"msg"];
            chatView.timeText = [chatDictionary objectForKey:@"time"];
            if ([_type isEqualToString:@"group"] || [_type isEqualToString:@"dept"]) {
                chatView.isGoup = YES;
            }
            [chatView sendMsg];
            [_chatModelArr addObject:chatView];
            
            [chatView release];
            i++;
        }
        
    } else {
        for (int i = 0 ;i < [arr count]  ; i++ ) {
            KoronChatModel *chatView = [[KoronChatModel alloc]init];
            if (i == 0) {
                if (_lasstId) {
                    RELEASE_SAFELY(_lasstId);
                }
                _lasstId = [[[arr objectAtIndex:i] objectForKey:@"id"] copy];
            }
            if ([self computationTime:[[arr objectAtIndex:i] objectForKey:@"time"]]) {
                chatView.isHiden = YES;
            }
            if ([[[arr objectAtIndex:i] objectForKey:@"send"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"sid"]]) {
                chatView.isWho = YES;
                [chatView setChatViewFrame:CGRectMake(0, 0, 320, 0)];
            }else {
                [chatView setChatViewFrame:CGRectMake(0, 0, 320, 0)];
            }
            chatView.chatID = [[arr objectAtIndex:i] objectForKey:@"send"];
            chatView.chatContent = [[arr objectAtIndex:i] objectForKey:@"msg"];
            chatView.timeText = [[arr objectAtIndex:i] objectForKey:@"time"];
            if ([_type isEqualToString:@"group"] || [_type isEqualToString:@"dept"]) {
                chatView.isGoup = YES;
            }
            [chatView sendMsg];
            [_chatModelArr insertObject:chatView atIndex:i];
            
            [chatView release];
        }
    }
    
    [_tableView reloadData];
    if (isFirst) {
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_chatModelArr count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        isFirst = NO;
    }else {
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:_scrollToRow inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }
    
    [super doneLoadingTableViewData];
}

//加载消息失败
- (void)receveChatMsgRequestDidFail:(ASIFormDataRequest *)response {
    [super doneLoadingTableViewData];
    [SVProgressHUD showErrorWithStatus:@"加载失败" duration:1];
}




/*
 网络请求:
 发送消息
 */
- (void)sendChatMessage {
    if ([_textView.text isEqualToString:@""]) {
        return;
    }
    
    if (nil != _sendChatMsgRequest) {
        [_sendChatMsgRequest clearDelegatesAndCancel];
        RELEASE_SAFELY(_sendChatMsgRequest);
    }
    
    RELEASE_SAFELY(_content);
    _content = [_textView.text copy];
    
    NSString *hostURL = [[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL];
    _sendChatMsgRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL  URLWithString:hostURL]];
    _sendChatMsgRequest.delegate = self;
    [_sendChatMsgRequest setDidFinishSelector:@selector(sendChatMsgRequestDidFinish:)];
    [_sendChatMsgRequest setDidFailSelector:@selector(sendChatMsgRequestDidFail:)];
    [_sendChatMsgRequest setPostValue:[[NSUserDefaults standardUserDefaults]objectForKey:@"sid"] forKey:@"sid"];
    [_sendChatMsgRequest setPostValue:@"sendChatMsg" forKey:@"op"];
    [_sendChatMsgRequest setPostValue:_linkManID forKey:@"relationId"];
    [_sendChatMsgRequest setPostValue:_content forKey:@"msg"];
    [_sendChatMsgRequest setPostValue:_type forKey:@"type"];
    
    [_sendChatMsgRequest startAsynchronous];
    
    
    
    
    KoronChatModel *model = [_chatModelArr objectAtIndex:[_chatModelArr count] - 1];
    
    KoronChatModel *chatView = [[KoronChatModel alloc]init];
    [chatView setChatViewFrame:CGRectMake(0, 0, 320, 0)];
    chatView.isWho = YES;
    chatView.chatID = [[NSUserDefaults standardUserDefaults] objectForKey:@"sid"];
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    chatView.timeText = [fromatter stringFromDate:[NSDate date]];
    RELEASE_SAFELY(fromatter);
    chatView.chatContent = _content;
    if ([self sendChatMessageComputationTime:model.timeText]) {
        chatView.isHiden = YES;
    }
    [chatView sendMsg];
    [_chatModelArr addObject:chatView];
    [chatView release];
    [_tableView reloadData];
    if ([_chatModelArr count] > 0) {
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_chatModelArr count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:nil];
    }
    _textView.text = @"";
    isSend = YES;
    [self textViewDidChange:_textView];

    
}

//发送新消息成功
- (void)sendChatMsgRequestDidFinish:(ASIFormDataRequest *)response {
    NSLog(@"responseString  ====  %@",[response responseString]);
    NSDictionary *dictionary = [[response responseString] JSONValue];
    if ([[dictionary objectForKey:@"desc"]isEqualToString:@"发送成功"]) {
        return;
    }else {
        
    }
}

//发送新消息失败
- (void)sendChatMsgRequestDidFail:(ASIFormDataRequest *)response {
    [SVProgressHUD showErrorWithStatus:@"消息发送失败" duration:1];
}


/*
 网络请求:
 接收新消息
 */
- (void)receveNewMsgRequest {
    if ( nil != _receveNewMsgRequest) {
        [_receveNewMsgRequest clearDelegatesAndCancel];
        RELEASE_SAFELY(_receveNewMsgRequest);
    }
    NSString *hostURL = [[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL];
    _receveNewMsgRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:hostURL]];
    _receveNewMsgRequest.delegate = self;
    [_receveNewMsgRequest setDidFinishSelector:@selector(receveNewMsgRequestDidFinish:)];
    [_receveNewMsgRequest setDidFailSelector:@selector(receveNewMsgRequestDidFail:)];
    [_receveNewMsgRequest setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"sid"] forKey:@"sid"];
    [_receveNewMsgRequest setPostValue:@"receveChatMsg" forKey:@"op"];
    [_receveNewMsgRequest setPostValue:_linkManID forKey:@"relationId"];
    [_receveNewMsgRequest setPostValue:_type forKey:@"type"];
    [_receveNewMsgRequest setPostValue:@"" forKey:@"lasstId"];
    
    [_receveNewMsgRequest startAsynchronous];
}

//接收新消息成功
- (void)receveNewMsgRequestDidFinish:(ASIFormDataRequest *)response {
//    NSDictionary *dictionary = [[response responseString] JSONValue];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[response responseData] options:kNilOptions error:nil];
    
    if ([[dictionary objectForKey:@"code"] integerValue] == -1) {
        [super doneLoadingTableViewData];
        return;
    }
//    NSLog(@"receveChatMsgRequestDidFinish  %@",[response responseString]);
    NSArray *arr = [dictionary objectForKey:@"list"];
    
    if ([[[_chatModelArr lastObject] chatID] isEqualToString:[[arr lastObject] objectForKey:@"id"]]) {
        NSLog(@"没有新消息");
        return;
    }
    if (nil != _chatModelArr) {
        RELEASE_SAFELY(_chatModelArr);
    }
    _chatModelArr = [[NSMutableArray alloc]init];
    
    _scrollToRow = [arr count];
    
    NSString *dateStr = [[arr objectAtIndex:0] objectForKey:@"time"];
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    if (_date) {
        RELEASE_SAFELY(_date);
    }
    _date = [[fromatter dateFromString:dateStr] retain];
    [fromatter release];
    
    
    int i = 0;
    for (NSDictionary *chatDictionary in arr) {
        KoronChatModel *chatView = [[KoronChatModel alloc]init];
        if (i == 0) {
            chatView.timeText = [chatDictionary objectForKey:@"time"];
            if (_lasstId) {
                RELEASE_SAFELY(_lasstId);
            }
            _lasstId = [[chatDictionary objectForKey:@"id"] copy];
            chatView.isHiden = YES;
        }
        if ([self computationTime:[chatDictionary objectForKey:@"time"]]) {
            chatView.isHiden = YES;
        }
        
        if ([[chatDictionary objectForKey:@"send"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"sid"]]) {
            chatView.isWho = YES;
            [chatView setChatViewFrame:CGRectMake(0, 0, 320, 0)];
        }else {
            [chatView setChatViewFrame:CGRectMake(0, 0, 320, 0)];
        }
        chatView.chatID = [chatDictionary objectForKey:@"send"];
        chatView.chatContent = [chatDictionary objectForKey:@"msg"];
        chatView.timeText = [chatDictionary objectForKey:@"time"];
        if ([_type isEqualToString:@"group"] || [_type isEqualToString:@"dept"]) {
            chatView.isGoup = YES;
        }
        [chatView sendMsg];
        [_chatModelArr addObject:chatView];
        
        [chatView release];
        i++;
    
    }
    [_tableView reloadData];
    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_chatModelArr count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refurbishPrivateMessage" object:nil];
}


- (void)receveNewMsgRequestDidFail:(ASIFormDataRequest *)response {
    
}





#pragma mark - computationTime
//计算时间判断是否显示时间

- (BOOL)sendChatMessageComputationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [fromatter dateFromString:timeStr];
    [fromatter release];
    long intervalTime = [date timeIntervalSinceReferenceDate] - [[NSDate date] timeIntervalSinceReferenceDate];
    
    if (intervalTime < -180) {
        return YES;
    }else {
        return NO;
    }
}




- (BOOL)computationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [fromatter dateFromString:timeStr];
    [fromatter release];
    long intervalTime = [date timeIntervalSinceReferenceDate] - [_date timeIntervalSinceReferenceDate];
    NSLog(@"intervalTime  %ld",intervalTime);
    
    if (intervalTime > 180) {
        RELEASE_SAFELY(_date);
        _date = [date copy];
        return YES;
    }else {
        return NO; 
    }
}


#pragma mark - privateLetterNotice
/*
 接收到新消息
 LoginViewController的通知
 */
- (void)privateLetterNotice {
    [self receveNewMsgRequest];
}

#pragma mark - UIStatusBarStyle

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


@end
