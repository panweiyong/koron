//
//  KoronOnlineCell.m
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronOnlineCell.h"

@implementation KoronOnlineCell
@synthesize onlineTypeView = _onlineTypeView;

- (void)dealloc
{
    RELEASE_SAFELY(_onlineTypeView);
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _onlineTypeView = [[UIImageView alloc]initWithFrame:CGRectMake(320 - 50, 5, 20, 20)];
        _onlineTypeView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_onlineTypeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
