//
//  KoronOnlineViewController.h
//  PrivateMessage
//
//  Created by Koron on 13-9-3.
//  Copyright (c) 2013年 Koron-Hsn. All rights reserved.
//

#import "KoronMessageViewController.h"
#import "ASIFormDataRequest.h"
#import "KoronOnlineInfo.h"
#import "KoronOnlineCell.h"
#import "UIImageView+WebCache.h"
#import "koronPrivateMessageDB.h"
#import "SBJson.h"
#import "UIImage+Extension.h"
#import "SVProgressHUD.h"


@interface KoronOnlineViewController : KoronMessageViewController <UITableViewDataSource,UITableViewDelegate,ASIHTTPRequestDelegate> {
    NSMutableArray *_onlineArr;
    NSMutableArray *_linkmanArr;
    NSString *_hostURL;
    ASIFormDataRequest *_onlineRequest;
}

@end
