//
//  KoronOnlineInfo.m
//  moffice
//
//  Created by Koron on 13-9-3.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronOnlineInfo.h"

@implementation KoronOnlineInfo
@synthesize empNumber = _empNumber;
@synthesize gender = _gender;
@synthesize icon = _icon;
@synthesize onlineId = _onlineId;
@synthesize lastUpdateTime = _lastUpdateTime;
@synthesize name = _name;
@synthesize sign = _sign;
@synthesize title = _title;
@synthesize onlineType = _onlineType;
@synthesize status = _status;


- (void)dealloc
{
    RELEASE_SAFELY(_empNumber);
    RELEASE_SAFELY(_gender);
    RELEASE_SAFELY(_onlineId);
    RELEASE_SAFELY(_lastUpdateTime);
    RELEASE_SAFELY(_name);
    RELEASE_SAFELY(_sign);
    RELEASE_SAFELY(_title);
    
    [super dealloc];
}

@end
