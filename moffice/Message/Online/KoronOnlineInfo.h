//
//  KoronOnlineInfo.h
//  moffice
//
//  Created by Koron on 13-9-3.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KoronOnlineInfo : NSObject {
    NSString *_empNumber;
    NSString *_gender;
    NSInteger _icon;
    NSString *_onlineId;
    NSString *_lastUpdateTime;
    NSString *_name;
    NSString *_sign;
    NSString *_title;
    NSInteger _onlineType;
    NSInteger _status;
}

@property (nonatomic,retain)NSString *empNumber;
@property (nonatomic,retain)NSString *gender;
@property (nonatomic,assign)NSInteger icon;
@property (nonatomic,retain)NSString *onlineId;
@property (nonatomic,retain)NSString *lastUpdateTime;
@property (nonatomic,retain)NSString *name;
@property (nonatomic,retain)NSString *sign;
@property (nonatomic,retain)NSString *title;
@property (nonatomic,assign)NSInteger onlineType;
@property (nonatomic,assign)NSInteger status;


@end
