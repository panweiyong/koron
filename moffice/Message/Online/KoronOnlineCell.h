//
//  KoronOnlineCell.h
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronMessageCell.h"

@interface KoronOnlineCell : KoronMessageCell {
    UIImageView *_onlineTypeView;
}

@property (nonatomic,retain)UIImageView *onlineTypeView;

@end
