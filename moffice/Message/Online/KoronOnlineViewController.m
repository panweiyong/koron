//
//  KoronOnlineViewController.m
//  PrivateMessage
//
//  Created by Koron on 13-9-3.
//  Copyright (c) 2013年 Koron-Hsn. All rights reserved.
//

/*
 所有联系人
 */

#import "KoronOnlineViewController.h"

@interface KoronOnlineViewController ()

@end

@implementation KoronOnlineViewController

- (void)dealloc
{
    [_onlineRequest clearDelegatesAndCancel];
    RELEASE_SAFELY(_onlineRequest);
    RELEASE_SAFELY(_onlineArr);
    RELEASE_SAFELY(_hostURL);
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _hostURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"hosturl"];
    NSRange range = [_hostURL rangeOfString:@"/moffice"];
    _hostURL = [[_hostURL substringToIndex:range.location] copy];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;

    [self sendOnlineRequest];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - FMDatabase

//从数据库里获得好友列表
- (void)getAllContacts {
    if (_onlineArr != nil) {
        RELEASE_SAFELY(_onlineArr);
    }
    _onlineArr = [[NSMutableArray alloc]init];
    
    KoronPrivateMessageDB *database = [[KoronPrivateMessageDB alloc] init];
    FMResultSet *resultSet = [database selectLinkman];
    while (resultSet.next) {
        KoronOnlineInfo *info = [[KoronOnlineInfo alloc]init];
        info.empNumber = [resultSet stringForColumn:@"empNumber"];
        info.gender = [resultSet stringForColumn:@"gender"];
        info.icon = [resultSet intForColumn:@"icon"];
        info.onlineId = [resultSet stringForColumn:@"id"];
        info.lastUpdateTime = [resultSet stringForColumn:@"lastUpdateTime"];
        info.name = [resultSet stringForColumn:@"name"];
        info.sign = [resultSet stringForColumn:@"sign"];
        info.title = [resultSet stringForColumn:@"title"];
        info.onlineType = [resultSet intForColumn:@"onlineType"];
        info.status = [resultSet intForColumn:@"status"];
        
        [_onlineArr addObject:info];
        [info release];
    }
    [_tableView reloadData];
    [database.dataBase close];
    [database release];
}



#pragma mark - UITableViewDataSource,UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_onlineArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KoronOnlineCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"OnlineCell"];
    if (nil == cell) {
        cell = [[[KoronOnlineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OnlineCell"] autorelease];
    }
    KoronOnlineInfo *info = [_onlineArr objectAtIndex:indexPath.row];
    
    
    UIImage *img = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:info.onlineId];
    //判断是否在线
    if (info.status == 1 || info.status == 2) {
        if (img) {
            [cell.headImageView setImage:[UIImage createRoundedRectImage:img]];
        }else if ([info.gender isEqualToString:@"男"]){
            [cell.headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"default_head.jpg"]]];
        }else {
            [cell.headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"default_head_woman.jpg"]]];
        }
    }else {
        if (img) {
            UIImage *grayImg = [KoronOnlineViewController getGrayImage:img];
            [cell.headImageView setImage:[UIImage createRoundedRectImage:grayImg]];
        }else if ([info.gender isEqualToString:@"男"]){
            UIImage *grayImg = [KoronOnlineViewController getGrayImage:[UIImage imageNamed:@"default_head.jpg"]];
            [cell.headImageView setImage:[UIImage createRoundedRectImage:grayImg]]; 
        }else {
            UIImage *grayImg = [KoronOnlineViewController getGrayImage:[UIImage imageNamed:@"default_head_woman.jpg"]];
            [cell.headImageView setImage:[UIImage createRoundedRectImage:grayImg]];
        }
    }
    //判断是什么设备登陆
    if (info.onlineType == 1 || info.onlineType == 2) {
        [cell.onlineTypeView setImage:[UIImage imageNamed:@"user_icon_pc.png"]];
        [cell.onlineTypeView setBounds:CGRectMake(0, 0, 18, 18)];
    }else if (info.onlineType == 3) {
        [cell.onlineTypeView setImage:[UIImage imageNamed:@"user_icon_phone_ip.png"]];
        [cell.onlineTypeView setBounds:CGRectMake(0, 0, 10.5, 17)];
    }else {
        [cell.onlineTypeView setImage:[UIImage imageNamed:@""]];
    }

    cell.nameLabel.text = info.name;
    if ([info.sign isEqualToString:@""]) {
        cell.underLabel.text = @"这个家伙很懒,什么都没有写!";
    }else {
        cell.underLabel.text = info.sign;
    }
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    KoronOnlineInfo *info = [_onlineArr objectAtIndex:indexPath.row];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    [dictionary setValue:info.onlineId forKey:@"id"];
    [dictionary setValue:info.name forKey:@"name"];
    [dictionary setValue:@"person" forKey:@"type"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"presentChatView" object:dictionary];
    [dictionary release];
}

#pragma mark - request



/*
 发送网络请求获得在线联系人
 */
- (void)sendOnlineRequest {
    if (nil != _onlineRequest) {
        [_onlineRequest clearDelegatesAndCancel];
        _onlineRequest.delegate = nil;
        RELEASE_SAFELY(_onlineRequest);
    }
    
    NSString *hosturl = [[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL];
    
    _onlineRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:hosturl]];
    _onlineRequest.delegate = self;
    [_onlineRequest setDidFinishSelector:@selector(onlineRequestDidFinish:)];
    [_onlineRequest setDidFailSelector:@selector(onlineRequestDidFail:)];
    [_onlineRequest setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"sid"] forKey:@"sid"];
    [_onlineRequest setPostValue:@"chatOnline" forKey:@"op"];
    [_onlineRequest startAsynchronous];
    
}

#pragma mark - KoronPrivateMessageRequestDelegate


/*
 网络请求完成返回数据
 */

//onlinetype 0.不显示  1.PC   2.网页  3.手机
//status   1.在线  2.离线  3.离开
- (void)onlineRequestDidFinish:(ASIFormDataRequest *)response {
//    NSDictionary *dictionary = [[response responseString] JSONValue];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil];

    NSArray *arr = [[dictionary objectForKey:@"list"] copy];

    KoronPrivateMessageDB *dataBase = [[KoronPrivateMessageDB alloc] init];
    if ([dataBase updateLinkman]) {
        if (![dataBase updateLinkman:arr]) {
            NSLog(@"updateLinkman is error");
        }else {
            
            [self getAllContacts];
            [super doneLoadingTableViewData];
        }
    }
    [dataBase.dataBase close];
    [dataBase release];
    RELEASE_SAFELY(arr);
}

- (void)onlineRequestDidFail:(ASIFormDataRequest *)response {
    [super doneLoadingTableViewData];
    [SVProgressHUD showErrorWithStatus:@"加载失败" duration:1];
}

#pragma mark - egoRefreshScrollView


/*
 重写父类的方法,下拉刷新开始发送网络请求
 */

- (void)beginSendRequst {
    [self sendOnlineRequest];
}


/*
 改变UIImage的颜色(image变成灰色)
 */

+ (UIImage*)getGrayImage:(UIImage*)sourceImage
{
    int width = sourceImage.size.width;
    int height = sourceImage.size.height;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate (nil,width,height,8,0,colorSpace,kCGImageAlphaNone);
    CGColorSpaceRelease(colorSpace);
    
    if (context == NULL) {
        return nil;
    }
    
    CGContextDrawImage(context,CGRectMake(0, 0, width, height), sourceImage.CGImage);
    CGImageRef grayImageRef = CGBitmapContextCreateImage(context);
    UIImage *grayImage = [UIImage imageWithCGImage:grayImageRef];
    CGContextRelease(context);
    CGImageRelease(grayImageRef);
    
    return grayImage;
}




@end
