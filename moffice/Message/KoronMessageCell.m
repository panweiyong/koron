//
//  KoronMessageCell.m
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//  


/*
 
 自定义UITableViewCell,用来给私信模块继承
 
 */

#import "KoronMessageCell.h"

@implementation KoronMessageCell
@synthesize headImageView = _headImageView;
@synthesize nameLabel = _nameLabel;
@synthesize underLabel = _underLabel;

- (void)dealloc
{
    RELEASE_SAFELY(_headImageView);
    RELEASE_SAFELY(_nameLabel);
    RELEASE_SAFELY(_underLabel);
    [super dealloc];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        _headImageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 45, 45)];
//        [_headImageView.layer setCornerRadius:8];
//        [_headImageView.layer setMasksToBounds:YES];
        [self.contentView addSubview:_headImageView];
        
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(55, 5, 200, 20)];
        _nameLabel.font = [UIFont systemFontOfSize:16];
        _nameLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:_nameLabel];
        
        _underLabel = [[UILabel alloc]initWithFrame:CGRectMake(55, 35, 250, 15)];
        _underLabel.font = [UIFont systemFontOfSize:12];
        _underLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:_underLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect {
    CGRect frame = self.frame;
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
    CGContextSetAlpha(ctx, 0.4);
    CGContextSetLineWidth(ctx, 2);
    CGContextMoveToPoint(ctx, 5, frame.size.height);
    CGContextAddLineToPoint(ctx, frame.size.width - 5, frame.size.height);
    CGContextStrokePath(ctx);
}

@end
