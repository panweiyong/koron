//
//  koronPrivateMessageDB.m
//  moffice
//
//  Created by Hsn on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronPrivateMessageDB.h"

//static KoronPrivateMessageDB *fmDataBase = nil;

@implementation KoronPrivateMessageDB
@synthesize dataBase = _dataBase;

//+ (KoronPrivateMessageDB *)sharedManager {
//    if (nil == fmDataBase) {
//        fmDataBase = [[KoronPrivateMessageDB alloc]init];
//    }
//    return fmDataBase;
//}

- (void)dealloc
{
    RELEASE_SAFELY(_dataBase);
    [super dealloc];
}

- (id)init
{
    self = [super init];
    if (self) {
        NSString *path = NSHomeDirectory();
        path = [path stringByAppendingPathComponent:@"Library/PrivateMessage.db"];
//        NSLog(@"path == %@",path);
        _dataBase = [[FMDatabase alloc]initWithPath:path];
        if (![_dataBase open]) {
            NSLog(@"创建失败!!");
            return nil;
        }
        
        [_dataBase executeUpdate:@"create table if not exists Linkman(empNumber text(1024) default null,gender text(1024) default null,icon integer,id text(1024) default null,lastUpdateTime text(1024) default null,name text(1024) default null,sign text(1024) default null,title text(1024) default null,onlineType integer default 0,status integer default 3)"];
        
        
    }
    return self;
}

- (void)deleteLinkman {
    [_dataBase executeUpdate:@"delete from Linkman"];
}

- (BOOL)insertLinkman:(KoronOnlineInfo *)info {
    return [_dataBase executeUpdate:@"insert into Linkman (empNumber,gender,icon,id,lastUpdateTime,name,sign,title) values (?,?,?,?,?,?,?,?)",info.empNumber,info.gender,[NSNumber numberWithInteger:info.icon],info.onlineId,info.lastUpdateTime,info.name,info.sign,info.title];
}

- (FMResultSet *)selectLinkman {
    return [_dataBase executeQuery:@"select * from Linkman ORDER BY status,empNumber"];
}

- (FMResultSet *)selectName:(NSString *)linkmanID {
    return [_dataBase executeQuery:@"select * from Linkman where id = ?",linkmanID];
}

- (BOOL)updateLinkman:(NSArray *)arr {
    BOOL boolean = YES;
    for (NSDictionary *dictionary in arr) {
        NSNumber *onlineType = [NSNumber numberWithInteger:[[dictionary objectForKey:@"onlineType"] integerValue]];
        NSNumber *status = nil;
        if ([[dictionary objectForKey:@"status"] integerValue] == 2) {
            status = [NSNumber numberWithInteger:[[dictionary objectForKey:@"status"] integerValue]+1];
        }else if ([[dictionary objectForKey:@"status"] integerValue] == 3) {
            status = [NSNumber numberWithInteger:[[dictionary objectForKey:@"status"] integerValue]-1];
        }else {
            status = [NSNumber numberWithInteger:[[dictionary objectForKey:@"status"] integerValue]];
        }
        
        NSString *onlineID = [dictionary objectForKey:@"id"];
        if (![_dataBase executeUpdate:@"UPDATE Linkman SET onlineType=?,status=? WHERE id=?",onlineType,status,onlineID]) {
            boolean = NO;
        }
    }
    return boolean;
}

- (BOOL)updateLinkman {
    return [_dataBase executeUpdate:@"UPDATE Linkman SET onlineType = 0, status = 3 "];
}


- (BOOL)updatePersonalData:(KoronOnlineInfo *)info {
    return [_dataBase executeUpdate:@"UPDATE Linkman SET id = ?, empNumber = ?, name = ?,title = ?,sign = ?,icon = ?,gender = ?,lastUpdateTime = ? where id = ?",info.onlineId,info.empNumber,info.name,info.title,info.sign,[NSNumber numberWithInteger:info.icon],info.gender,info.lastUpdateTime,info.onlineId];
}



@end
