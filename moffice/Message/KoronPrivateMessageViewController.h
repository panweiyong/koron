//
//  KoronPrivateMessageViewController.h
//  PrivateMessage
//
//  Created by Koron on 13-9-3.
//  Copyright (c) 2013年 Koron-Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "KoronOnlineViewController.h"
#import "KoronRecentlyViewController.h"
#import "KoronGroupViewController.h"
#import "ASIFormDataRequest.h"
#import "koronPrivateMessageDB.h"
#import "KoronChatViewController.h"
#import "SDImageCache.h"

@interface KoronPrivateMessageViewController : UIViewController <UIScrollViewDelegate,ASIHTTPRequestDelegate> {
    //顶部
    UIImageView *_topImageView;
    //顶部滑动控件
    UIView *_moveView;
    
    UIScrollView *_scrollView;
    //联系人页面
    KoronOnlineViewController *_onlineViewCtl;
    //最近联系人页面
    KoronRecentlyViewController *_recentlyViewCtl;
    //群组页面
    KoronGroupViewController *_groupViewCtl;
    //第一次运行加载所有联系人
    ASIFormDataRequest *_chatUserRequest;
    
    NSString *_hostURL;
    
    BOOL isFirstOnlineView;
    BOOL isFirstGroupView;
    BOOL isFirstMakeUseOf;
}

- (id)initWithCGRect:(CGRect)rect;

@end
