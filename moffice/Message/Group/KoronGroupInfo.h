//
//  KoronGroupInfo.h
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KoronGroupInfo : NSObject {
}

@property (nonatomic,retain)NSString *name;
@property (nonatomic,retain)NSString *type;
@property (nonatomic,retain)NSString *remark;
@property (nonatomic,retain)NSString *groupID;

@end
