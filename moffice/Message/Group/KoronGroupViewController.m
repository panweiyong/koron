//
//  KoronGroupViewController.m
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronGroupViewController.h"

@interface KoronGroupViewController ()

@end

@implementation KoronGroupViewController

- (void)dealloc
{
    [_groupRequest clearDelegatesAndCancel];
    _groupRequest.delegate = nil;
    RELEASE_SAFELY(_groupRequest);
    RELEASE_SAFELY(_groupArr);
    [super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self sendGroupRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource,UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_groupArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KoronMessageCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"GroupCell"];
    if (nil == cell) {
        cell = [[[KoronMessageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GroupCell"] autorelease];
    }
    KoronGroupInfo *info = [_groupArr objectAtIndex:indexPath.row];
    cell.nameLabel.text = info.name;
    if ([@"dept" isEqualToString:info.type]) {
        [cell.headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"dept.jpg"]]];
    } else {
        [cell.headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"group.jpg"]]];
    }
    cell.underLabel.text = info.remark;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    KoronGroupInfo *info = [_groupArr objectAtIndex:indexPath.row];
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    [dictionary setValue:info.groupID forKey:@"id"];
    [dictionary setValue:info.name forKey:@"name"];
    [dictionary setValue:info.type forKey:@"type"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"presentChatView" object:dictionary];
    [dictionary release];
}


#pragma mark - groupRequest
- (void)sendGroupRequest {
    if (_groupRequest != nil) {
        [_groupRequest clearDelegatesAndCancel];
        _groupRequest.delegate = nil;
        RELEASE_SAFELY(_groupRequest);
    }

    NSString *hosturl = [[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL];
    
    _groupRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:hosturl]];
    _groupRequest.delegate = self;
    [_groupRequest setDidFinishSelector:@selector(groupRequestDidFinish:)];
    [_groupRequest setDidFailSelector:@selector(groupRequestDidFail:)];
    [_groupRequest setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"sid"] forKey:@"sid"];
    [_groupRequest setPostValue:@"chatGroup" forKey:@"op"];
    [_groupRequest startAsynchronous];
}

//重写父类下拉刷新方法
- (void)beginSendRequst {
    [self sendGroupRequest];
}


#pragma mark - requestDelegate 
- (void)groupRequestDidFinish:(ASIFormDataRequest *)response {
    if (_groupArr != nil) {
        RELEASE_SAFELY(_groupArr);
    }
    _groupArr = [[NSMutableArray alloc]init];
    
//    NSDictionary *dictionary = [[response responseString]JSONValue];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil];
    
    NSArray *arr = [dictionary objectForKey:@"list"];
    for (NSDictionary *groupDictionary in arr) {
        KoronGroupInfo *info = [[KoronGroupInfo alloc]init];
        info.name = [groupDictionary objectForKey:@"name"];
        info.type = [groupDictionary objectForKey:@"type"];
        info.remark = [groupDictionary objectForKey:@"remark"];
        info.groupID = [groupDictionary objectForKey:@"id"];
        [_groupArr addObject:info];
        [info release];
    }
    
    [super doneLoadingTableViewData];
    [_tableView reloadData];
}

- (void)groupRequestDidFail:(ASIFormDataRequest *)response {
    [super doneLoadingTableViewData];
    [SVProgressHUD showErrorWithStatus:@"加载失败" duration:1];
}

@end
