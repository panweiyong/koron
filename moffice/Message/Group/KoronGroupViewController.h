//
//  KoronGroupViewController.h
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronMessageViewController.h"
#import "ASIFormDataRequest.h"
#import "KoronGroupInfo.h"
#import "KoronMessageCell.h"
#import "SBJson.h"
#import "SVProgressHUD.h"
#import "UIImage+Extension.h"

@interface KoronGroupViewController : KoronMessageViewController <UITableViewDataSource,UITableViewDelegate,ASIHTTPRequestDelegate> {
    ASIFormDataRequest *_groupRequest;
    NSMutableArray *_groupArr;
}

@end
