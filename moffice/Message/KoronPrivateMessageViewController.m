//
//  KoronPrivateMessageViewController.m
//  PrivateMessage
//
//  Created by Koron on 13-9-3.
//  Copyright (c) 2013年 Koron-Hsn. All rights reserved.
//

#import "KoronPrivateMessageViewController.h"
#import "FMDatabase.h"

@interface KoronPrivateMessageViewController ()

@end

@implementation KoronPrivateMessageViewController

- (void)dealloc
{
    [_chatUserRequest clearDelegatesAndCancel];
    _chatUserRequest.delegate = nil;
    RELEASE_SAFELY(_chatUserRequest);
    
    RELEASE_SAFELY(_topImageView);
    RELEASE_SAFELY(_moveView);
    RELEASE_SAFELY(_scrollView);
    RELEASE_SAFELY(_onlineViewCtl);
    RELEASE_SAFELY(_recentlyViewCtl);
    RELEASE_SAFELY(_groupViewCtl);
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

- (id)initWithCGRect:(CGRect)rect
{
    self = [super init];
    if (self) {
        [self.view setFrame:rect];
        self.title=NSLocalizedString(@"Message",@"Message") ;
        self.tabBarItem.image = [UIImage imageNamed:@"3"];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isFirstOnlineView = YES;
        isFirstGroupView = YES;

        [self sendChatUserRequest];
        
        
        _hostURL = [[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL];
        NSRange range = [_hostURL rangeOfString:@"/moffice"];
        _hostURL = [[_hostURL substringToIndex:range.location] copy];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeStatusBarWhite" object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //顶部
    [self.view setBackgroundColor:[UIColor whiteColor]];
    _topImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"message_top_bg.png"]];
    [_topImageView setUserInteractionEnabled:YES];
    if (IOS_VERSIONS < 7.0) {
        [_topImageView setFrame:CGRectMake(0, 0, self.view.bounds.size.width,54)];
    }else {
        [_topImageView setFrame:CGRectMake(0, 20, self.view.bounds.size.width,54)];
    }
    
    [_topImageView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:_topImageView];
    
    //联系人列表 
    UIView *onlineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 106.33333, 60)];
    onlineView.backgroundColor = [UIColor clearColor];
    onlineView.userInteractionEnabled = YES;
    UITapGestureRecognizer *onlineTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onlineViewTouchUpInside)];
    [onlineView addGestureRecognizer:onlineTap];
    [onlineTap release];
    [_topImageView addSubview:onlineView];
    [onlineView release];
    
    UIImageView *onlineImgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"message_online.png"]];
    [onlineImgView setFrame:CGRectMake(38.33333, 7, 30, 30)];
    [onlineView addSubview:onlineImgView];
    [onlineImgView release];

    UILabel *onlineLabel = [[UILabel alloc]initWithFrame:CGRectMake(33, 38, 40, 12)];
    onlineLabel.backgroundColor = [UIColor clearColor];
    onlineLabel.font = [UIFont systemFontOfSize:10];
    onlineLabel.textColor = [UIColor colorWithRed:42/255.0 green:135/255.0 blue:192/255.0 alpha:1];
    onlineLabel.text = @"联系人";
    onlineLabel.textAlignment = NSTextAlignmentCenter;
    [_topImageView addSubview:onlineLabel];
    [onlineLabel release];
    
    
    //最近联系人列表
    UIView *sessionView = [[UIView alloc]initWithFrame:CGRectMake(106.33333, 0, 106.33333, 60)];
    sessionView.backgroundColor = [UIColor clearColor];
    sessionView.userInteractionEnabled = YES;
    UITapGestureRecognizer *sessionTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sessionViewTouchUpInside)];
    [sessionView addGestureRecognizer:sessionTap];
    [sessionTap release];
    [_topImageView addSubview:sessionView];
    [sessionView release];
    
    UIImageView *sessionImgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"message_session.png"]];
    [sessionImgView setFrame:CGRectMake(38.33333, 7, 30, 30)];
    [sessionView addSubview:sessionImgView];
    [sessionImgView release];
    
    UILabel *sessionLabel = [[UILabel alloc]initWithFrame:CGRectMake(33, 38, 40, 12)];
    sessionLabel.backgroundColor = [UIColor clearColor];
    sessionLabel.font = [UIFont systemFontOfSize:10];
    sessionLabel.textColor = [UIColor colorWithRed:42/255.0 green:135/255.0 blue:192/255.0 alpha:1];
    sessionLabel.text = @"最近";
    sessionLabel.textAlignment = NSTextAlignmentCenter;
    [sessionView addSubview:sessionLabel];
    [sessionLabel release];
    
    
    //群组
    UIView *groupView = [[UIView alloc]initWithFrame:CGRectMake(106.33333 * 2, 0, 106.33333, 60)];
    groupView.backgroundColor = [UIColor clearColor];
    groupView.userInteractionEnabled = YES;
    UITapGestureRecognizer *groupTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(groupViewTouchUpInside)];
    [groupView addGestureRecognizer:groupTap];
    [groupTap release];
    [_topImageView addSubview:groupView];
    [groupView release];
    
    UIImageView *groupImgView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"message_group.png"]];
    [groupImgView setFrame:CGRectMake(38.33333, 7, 30, 30)];
    [groupView addSubview:groupImgView];
    [groupImgView release];
    
    UILabel *groupLabel = [[UILabel alloc]initWithFrame:CGRectMake(25, 38, 50, 12)];
    groupLabel.backgroundColor = [UIColor clearColor];
    groupLabel.font = [UIFont systemFontOfSize:10];
    groupLabel.textColor = [UIColor colorWithRed:42/255.0 green:135/255.0 blue:192/255.0 alpha:1];
    groupLabel.text = @"部门/群组";
    groupLabel.textAlignment = NSTextAlignmentCenter;
    [groupView addSubview:groupLabel];
    [groupLabel release];
    
    //滑块
    _moveView = [[UIView alloc]initWithFrame:CGRectMake(106.33333, 52, 106.33333, 2)];
    _moveView.backgroundColor = [UIColor colorWithRed:42/255.0 green:135/255.0 blue:192/255.0 alpha:1];
    [_topImageView addSubview:_moveView];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, _topImageView.frame.origin.y+_topImageView.bounds.size.height, self.view.bounds.size.width, [UIScreen mainScreen].bounds.size.height - 60 - 44 - 15)];
    [_scrollView setContentSize:CGSizeMake(320*3, _scrollView.bounds.size.height)];
    _scrollView.delegate = self;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.alwaysBounceVertical = NO;
    _scrollView.pagingEnabled = YES;
    [self.view addSubview:_scrollView];
    
    [_scrollView setContentOffset:CGPointMake(320, _scrollView.bounds.origin.y)];
    
    
    _recentlyViewCtl = [[KoronRecentlyViewController alloc]initWithCGRect:CGRectMake(320, _scrollView.bounds.origin.y, self.view.bounds.size.width, _scrollView.bounds.size.height)];
    [_scrollView addSubview:_recentlyViewCtl.view];
    
    if (IOS_VERSIONS >= 7.0) {
        UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 20)];
        topView.backgroundColor = [UIColor blackColor];
        [self.view addSubview:topView];
        [self.view bringSubviewToFront:topView];
        [topView release];
    }
    
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)onlineViewTouchUpInside {
    if (isFirstOnlineView) {
        [self addOnlineViewController];
    }
    [self moveView:0];
    [_scrollView setContentOffset:CGPointMake(0, _scrollView.bounds.origin.y)];
}

- (void)sessionViewTouchUpInside {
    [self moveView:106.33333];
    [_scrollView setContentOffset:CGPointMake(320, _scrollView.bounds.origin.y)];
}

- (void)groupViewTouchUpInside {
    if (isFirstGroupView) {
        [self addGroupViewController];
    }
    [self moveView:106.33333*2];
    [_scrollView setContentOffset:CGPointMake(320*2, _scrollView.bounds.origin.y)];
}


- (void)addOnlineViewController {
    _onlineViewCtl = [[KoronOnlineViewController alloc]initWithCGRect:CGRectMake(0,_scrollView.bounds.origin.y, self.view.bounds.size.width, _scrollView.bounds.size.height)];
    [_scrollView addSubview:_onlineViewCtl.view];
    isFirstOnlineView = NO;
}

- (void)addGroupViewController {
    _groupViewCtl = [[KoronGroupViewController alloc]initWithCGRect:CGRectMake(320*2, _scrollView.bounds.origin.y, self.view.bounds.size.width, _scrollView.bounds.size.height)];
    [_scrollView addSubview:_groupViewCtl.view];
    isFirstGroupView = NO;
}

//改变_moveView的x坐标
- (void)moveView :(CGFloat)x {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [_moveView setFrame:CGRectMake(x, 52, _moveView.bounds.size.width, _moveView.bounds.size.height)];
    [UIView commitAnimations];
}






#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.x <320) {
        if (isFirstOnlineView) {
            [self addOnlineViewController];
        }
        [self moveView:0];
    }else if(scrollView.contentOffset.x >= 320 && scrollView.contentOffset.x < 320*2) {
        [self moveView:106.33333];
    }else {
        if (isFirstGroupView) {
            [self addGroupViewController];
        }
        [self moveView:106.33333*2];
    }
}

#pragma mark - sendRequest

/*
 发送网络请求获取所有联系人
 */
- (void)sendChatUserRequest {
    if (_chatUserRequest != nil) {
        [_chatUserRequest clearDelegatesAndCancel];
        _chatUserRequest.delegate = nil;
        RELEASE_SAFELY(_chatUserRequest);
    }
    
    NSString *hosturl = [[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL];
    
    _chatUserRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:hosturl]];
    _chatUserRequest.delegate = self;
    [_chatUserRequest setDidFinishSelector:@selector(chatUserRequestDidFinish:)];
    [_chatUserRequest setDidFailSelector:@selector(chatUserRequestDidFail:)];
    [_chatUserRequest setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"sid"] forKey:@"sid"];
    [_chatUserRequest setPostValue:@"chatUser" forKey:@"op"];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdateTime"]) {
        [_chatUserRequest setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"lastUpdateTime"] forKey:@"lastUpdateTime"];
        isFirstMakeUseOf = NO;
    }else {
        [_chatUserRequest setPostValue:@"" forKey:@"lastUpdateTime"];
        isFirstMakeUseOf = YES;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateStr = [formatter stringFromDate:[NSDate date]];
    RELEASE_SAFELY(formatter);
    
    //保存最后一次获取联系人信息的时间
    [[NSUserDefaults standardUserDefaults] setValue:dateStr forKey:@"lastUpdateTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [_chatUserRequest startAsynchronous];
    
    
    
    
}

#pragma mark - ChatUserRequestDelegate

- (void)chatUserRequestDidFinish:(ASIFormDataRequest *)response {
    
    NSLog(@"response responseString %@",[response responseString]);

//    NSDictionary *dictionary = [[response responseString] JSONValue];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil];
    
    if ([[dictionary objectForKey:@"desc"]isEqualToString:@"无查询结果"]) {
        return;
    }
    
    
    NSArray *arr = [dictionary objectForKey:@"list"];
    
    
    KoronPrivateMessageDB *dataBase = [[KoronPrivateMessageDB alloc]init];
    if (dataBase == nil) {
        NSLog(@"dataBase  is nil");
    }
    if (isFirstMakeUseOf) {
        [dataBase deleteLinkman];
        for (NSDictionary *dic in arr) {
            KoronOnlineInfo *info = [[KoronOnlineInfo alloc]init];
            info.empNumber = [dic objectForKey:@"empNumber"];
            if ([[dic objectForKey:@"gender"]isEqualToString:@"1"]) {
                info.gender = @"男";
            }else {
                info.gender = @"女";
            }
            info.icon = [[dic objectForKey:@"icon"] integerValue];
            info.onlineId = [dic objectForKey:@"id"];
            info.lastUpdateTime = [dic objectForKey:@"lastUpdateTime"];
            info.name = [dic objectForKey:@"name"];
            info.sign = [dic objectForKey:@"sign"];
            info.title = [dic objectForKey:@"title"];
            if (info.icon == 1) {
                NSString *headURL = [NSString stringWithFormat:@"%@/ReadFile?mobile=true&tempFile=&type=PIC&tableName=tblEmployee&fileName=mobile\\%@.jpg",_hostURL,[dic objectForKey:@"id"]];
                headURL = [headURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:headURL] options:SDWebImageDownloaderProgressiveDownload progress:^(NSUInteger receivedSize, long long expectedSize) {
                    //            NSLog(@"%u %lld",receivedSize,expectedSize);
                } completed:^(UIImage *aImage, NSData *data, NSError *error, BOOL finished) {
                    //                NSLog(@"成功了:%d",UIImageJPEGRepresentation(aImage, 1).length);
                    [[SDImageCache sharedImageCache] storeImage:aImage forKey:info.onlineId toDisk:YES];
                }];
            }
            
            if ([dataBase insertLinkman:info]) {
                NSLog(@"insert OK");
            }else {
                NSLog(@"insert error");
            }
            [info release];
        }
        
    }else {
        for (NSDictionary *dic in arr) {
            KoronOnlineInfo *info = [[KoronOnlineInfo alloc]init];
            info.empNumber = [dic objectForKey:@"empNumber"];
            if ([[dic objectForKey:@"gender"]isEqualToString:@"1"]) {
                info.gender = @"男";
            }else {
                info.gender = @"女";
            }
            info.icon = [[dic objectForKey:@"icon"] integerValue];
            info.onlineId = [dic objectForKey:@"id"];
            info.lastUpdateTime = [dic objectForKey:@"lastUpdateTime"];
            info.name = [dic objectForKey:@"name"];
            info.sign = [dic objectForKey:@"sign"];
            info.title = [dic objectForKey:@"title"];
            //删除头像等待更新
            [[SDImageCache sharedImageCache] removeImageForKey:info.onlineId fromDisk:YES];
            if (info.icon == 1) {
                NSString *headURL = [NSString stringWithFormat:@"%@/ReadFile?mobile=true&tempFile=&type=PIC&tableName=tblEmployee&fileName=mobile\\%@.jpg",_hostURL,[dic objectForKey:@"id"]];
                headURL = [headURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:headURL] options:SDWebImageDownloaderProgressiveDownload progress:^(NSUInteger receivedSize, long long expectedSize) {

                } completed:^(UIImage *aImage, NSData *data, NSError *error, BOOL finished) {
                    [[SDImageCache sharedImageCache] storeImage:aImage forKey:info.onlineId toDisk:YES];
                }];
            }
            if ([dataBase updatePersonalData:info]) {
                NSLog(@"update OK");
            }else {
                NSLog(@"update error");
            }
            [info release];
        }
    }
    [dataBase.dataBase close];
    [dataBase release];
}


- (void)chatUserRequestDidFail:(ASIFormDataRequest *)response {
    
}


@end
