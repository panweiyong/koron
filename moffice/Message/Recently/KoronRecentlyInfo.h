//
//  KoronRecentlyInfo.h
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KoronRecentlyInfo : NSObject {
    NSInteger _count;
    NSString *_recentID;
    NSString *_msg;
    NSString *_name;
    NSString *_time;
    NSString *_type;
    NSString *_icon;
    NSString *_gender;
}

@property (nonatomic,assign)NSInteger count;
@property (nonatomic,retain)NSString *recentID;
@property (nonatomic,retain)NSString *msg;
@property (nonatomic,retain)NSString *name;
@property (nonatomic,retain)NSString *time;
@property (nonatomic,retain)NSString *type;
@property (nonatomic,retain)NSString *icon;
@property (nonatomic,retain)NSString *gender;

@end
