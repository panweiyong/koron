//
//  KoronRecentlyCell.m
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronRecentlyCell.h"

@implementation KoronRecentlyCell
@synthesize lastUpdateTimeLabel = _lastUpdateTimeLabel;
@synthesize messageCountLabel = _messageCountLabel;
@synthesize messageCountView = _messageCountView;

- (void)dealloc
{
    RELEASE_SAFELY(_lastUpdateTimeLabel);
    RELEASE_SAFELY(_messageCountLabel);
    RELEASE_SAFELY(_messageCountView);
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _lastUpdateTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(320 - 180, 5, 150, 20)];
        _lastUpdateTimeLabel.backgroundColor = [UIColor clearColor];
        _lastUpdateTimeLabel.textAlignment = NSTextAlignmentRight;
        _lastUpdateTimeLabel.textColor = [UIColor blackColor];
        _lastUpdateTimeLabel.font = [UIFont systemFontOfSize:10];
        [self.contentView addSubview:_lastUpdateTimeLabel];
        
        _messageCountView = [[UIImageView alloc]initWithFrame:CGRectMake(30, -5, 20, 20)];
        [_messageCountView setImage:[UIImage imageNamed:@"com_tencent_open_qz_bg_tabbar_indicator1"]];
        _messageCountView.backgroundColor = [UIColor clearColor];
        [_headImageView addSubview:_messageCountView];
        
        _messageCountLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
        _messageCountLabel.backgroundColor = [UIColor clearColor];
        _messageCountLabel.textColor = [UIColor whiteColor];
        _messageCountLabel.textAlignment = NSTextAlignmentCenter;
        _messageCountLabel.font = [UIFont systemFontOfSize:8];
        [_messageCountView addSubview:_messageCountLabel];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
