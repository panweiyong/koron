//
//  KoronRecentlyCell.h
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronMessageCell.h"

@interface KoronRecentlyCell : KoronMessageCell {
    UILabel *_lastUpdateTimeLabel;
    UILabel *_messageCountLabel;
    UIImageView *_messageCountView;
}

@property (nonatomic,retain)UILabel *lastUpdateTimeLabel;
@property (nonatomic,retain)UILabel *messageCountLabel;
@property (nonatomic,retain)UIImageView *messageCountView;

@end
