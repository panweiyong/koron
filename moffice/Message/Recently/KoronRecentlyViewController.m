//
//  KoronRecentlyViewController.m
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronRecentlyViewController.h"
#import "KoronManager.h"

@interface KoronRecentlyViewController ()

@end

@implementation KoronRecentlyViewController

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_recentlyRequest clearDelegatesAndCancel];
    RELEASE_SAFELY(_recentlyRequest);
    [super dealloc];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //收到私信时,接收通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(privateLetterNotice) name:@"Privateletternotice" object:nil];
        
        //刷新列表
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refurbishPrivateMessage) name:@"refurbishPrivateMessage" object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.backgroundColor = [UIColor clearColor];
    
    _hostURL = [[NSUserDefaults standardUserDefaults] objectForKey:@"hosturl"];
    NSRange range = [_hostURL rangeOfString:@"/moffice"];
    _hostURL = [[_hostURL substringToIndex:range.location] copy];
    
    [self sendRecentlyRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource,UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_recentlyArr count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KoronRecentlyCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"RecentlyCell"];
    if (nil == cell) {
        cell = [[[KoronRecentlyCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RecentlyCell"] autorelease];
    }
    KoronRecentlyInfo *info = [_recentlyArr objectAtIndex:indexPath.row];
    
    UIImage *img = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:info.recentID];
    if (img) {
        [cell.headImageView setImage:[UIImage createRoundedRectImage:[[SDImageCache sharedImageCache] imageFromDiskCacheForKey:info.recentID]]];
    } else if([info.type isEqualToString:@"dept"]) {
        [cell.headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"dept.jpg"]]];
    } else if([info.type isEqualToString:@"group"]) {
        [cell.headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"group.jpg"]]];
    } else if([info.gender isEqualToString:@"男"]) {
        [cell.headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"default_head.jpg"]]];
    } else {
        [cell.headImageView setImage:[UIImage createRoundedRectImage:[UIImage imageNamed:@"default_head_woman.jpg"]]];
    }
        
    

    if (info.count > 0) {
        cell.messageCountView.hidden = NO;
        cell.messageCountLabel.hidden = NO;
        cell.messageCountLabel.text = [NSString stringWithFormat:@"%d",info.count];
    }else {
        cell.messageCountView.hidden = YES;
        cell.messageCountLabel.hidden = YES;
    }
    NSString *timeText = info.time;
    cell.lastUpdateTimeLabel.text = timeText;
    cell.nameLabel.text = info.name;
    cell.underLabel.text = info.msg;
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    KoronRecentlyCell *cell = (KoronRecentlyCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.messageCountLabel.text = nil;
    cell.messageCountLabel.hidden = YES;
    cell.messageCountView.hidden = YES;
    
    
    KoronRecentlyInfo *info = [_recentlyArr objectAtIndex:indexPath.row];
    NSNumber *number = [NSNumber numberWithInteger:info.count];
    info.count = 0;
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc]init];
    [dictionary setValue:info.recentID forKey:@"id"];
    [dictionary setValue:info.name forKey:@"name"];
    [dictionary setValue:info.type forKey:@"type"];

    [[NSNotificationCenter defaultCenter] postNotificationName:@"presentChatView" object:dictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"alreadyReadPrivateMessage" object:number];
    
    [dictionary release];
}




#pragma mark - sendReuqest
/*
 重写父类方法发送网络请求
 */
- (void)beginSendRequst {
    [self sendRecentlyRequest];
}


- (void)sendRecentlyRequest {
    if (_recentlyRequest != nil) {
        [_recentlyRequest clearDelegatesAndCancel];
        _recentlyRequest.delegate = nil;
        RELEASE_SAFELY(_recentlyRequest);
    }
    
    NSString *hosturl = [[NSUserDefaults standardUserDefaults] objectForKey:SERVERURL];
    
    _recentlyRequest = [[ASIFormDataRequest alloc]initWithURL:[NSURL URLWithString:hosturl]];
    _recentlyRequest.delegate = self;
    [_recentlyRequest setDidFinishSelector:@selector(RecentlyRequestDidFinish:)];
    [_recentlyRequest setDidFailSelector:@selector(RecentlyRequestDidFail:)];
    [_recentlyRequest setPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"sid"] forKey:@"sid"];
    [_recentlyRequest setPostValue:@"chatHis" forKey:@"op"];
    [_recentlyRequest startAsynchronous];
}

#pragma mark - requestDelegate

- (void)RecentlyRequestDidFinish:(ASIFormDataRequest *)response {
    if (_recentlyArr != nil) {
        RELEASE_SAFELY(_recentlyArr);
    }
    _recentlyArr = [[NSMutableArray alloc]init];
//    NSDictionary *dictionary = [[response responseString] JSONValue];
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:[response responseData] options:NSJSONReadingMutableContainers error:nil];
    
    NSArray *arr = [dictionary objectForKey:@"list"];
    KoronPrivateMessageDB *db = [[KoronPrivateMessageDB alloc]init];
    for (NSDictionary *recentlyDictionary in arr) {
        KoronRecentlyInfo *info = [[KoronRecentlyInfo alloc]init];
        info.count = [[recentlyDictionary objectForKey:@"count"] integerValue];
        info.recentID = [recentlyDictionary objectForKey:@"id"];
        info.msg = [recentlyDictionary objectForKey:@"msg"];
        info.name = [recentlyDictionary objectForKey:@"name"];
        NSInteger number = [self computationTime:[recentlyDictionary objectForKey:@"time"]];
        if (number == 1 ) {
            info.time = [NSString stringWithFormat:@"昨天 %@",[[recentlyDictionary objectForKey:@"time"] substringWithRange:NSMakeRange(11, 5)]];
        }else if (number == 2) {
            info.time = [NSString stringWithFormat:@"前天 %@",[[recentlyDictionary objectForKey:@"time"] substringWithRange:NSMakeRange(11, 5)]];
        }else if (number < 1) {
            info.time = [NSString stringWithFormat:@"今天 %@",[[recentlyDictionary objectForKey:@"time"] substringWithRange:NSMakeRange(11, 5)]];
        }else {
            info.time = [[recentlyDictionary objectForKey:@"time"] substringWithRange:NSMakeRange(5, 11)];
        }
        info.type = [recentlyDictionary objectForKey:@"type"];
        NSString *iconStr = [NSString stringWithFormat:@"%@/ReadFile?mobile=true&tempFile=&type=PIC&tableName=tblEmployee&fileName=mobile\\%@.jpg",_hostURL,[recentlyDictionary objectForKey:@"id"]];
        iconStr = [iconStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        info.icon = iconStr;
        
        FMResultSet *rs = [db selectName:info.recentID];
        if (rs.next) {
            info.gender = [rs stringForColumn:@"gender"];
        }
        
        
        [_recentlyArr addObject:info];
        [info release];
    }
    [db.dataBase close];
    [db release];
    [super doneLoadingTableViewData];
    [_tableView reloadData];
}

- (void)RecentlyRequestDidFail:(ASIFormDataRequest *)response {
    [super doneLoadingTableViewData];
    [SVProgressHUD showErrorWithStatus:@"加载失败" duration:1];
}


#pragma mark - privateLetterNotice

//心跳包刷新新消息
- (void)privateLetterNotice {
    [self sendRecentlyRequest];
}

//刷新未读消息
- (void)refurbishPrivateMessage {
    [self sendRecentlyRequest];
}


- (NSInteger)computationTime:(NSString *)timeStr {
    NSDateFormatter *fromatter = [[NSDateFormatter alloc]init];
    [fromatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *nowadayTimeStr = [fromatter stringFromDate:[NSDate date]];
    [fromatter release];
    
    if ([[nowadayTimeStr substringToIndex:3] integerValue] - [[timeStr substringToIndex:3] integerValue] == 0) {
        if ([[nowadayTimeStr substringWithRange:NSMakeRange(5, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(5, 2)] integerValue] == 0) {
            if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 0) {
                return 0;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 1) {
                return 1;
            }else if ([[nowadayTimeStr substringWithRange:NSMakeRange(8, 2)] integerValue] - [[timeStr substringWithRange:NSMakeRange(8, 2)] integerValue] == 2) {
                return 2;
            }else {
                return 4;
            }
        }else {
            return 4;
        }
        
    }else {
        return 4;
    }
}



@end
