//
//  KoronRecentlyInfo.m
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronRecentlyInfo.h"

@implementation KoronRecentlyInfo
@synthesize count = _count;
@synthesize recentID = _recentID;
@synthesize msg = _msg;
@synthesize name = _name;
@synthesize time = _time;
@synthesize type = _type;
@synthesize icon = _icon;
@synthesize gender = _gender;


- (void)dealloc
{
    RELEASE_SAFELY(_gender);
    RELEASE_SAFELY(_recentID);
    RELEASE_SAFELY(_msg);
    RELEASE_SAFELY(_name);
    RELEASE_SAFELY(_time);
    RELEASE_SAFELY(_type);
    RELEASE_SAFELY(_icon);
    [super dealloc];
}

@end
