//
//  KoronRecentlyViewController.h
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronMessageViewController.h"
#import "ASIFormDataRequest.h"
#import "KoronRecentlyInfo.h"
#import "KoronRecentlyCell.h"
#import "UIImageView+WebCache.h"
#import "KoronChatViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "KoronPrivateMessageDB.h"
#import "UIImage+Extension.h"

@interface KoronRecentlyViewController : KoronMessageViewController <UITableViewDataSource,UITableViewDelegate,ASIHTTPRequestDelegate> {
    ASIFormDataRequest *_recentlyRequest;
    NSMutableArray *_recentlyArr;
    NSString *_hostURL;
}

@end
