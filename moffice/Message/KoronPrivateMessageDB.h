//
//  koronPrivateMessageDB.h
//  moffice
//
//  Created by Hsn on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "KoronOnlineInfo.h"

@interface KoronPrivateMessageDB : NSObject {
    FMDatabase *_dataBase;
}

@property (nonatomic,retain)FMDatabase *dataBase;

//+ (KoronPrivateMessageDB *)sharedManager;
- (void)deleteLinkman;
- (BOOL)insertLinkman:(KoronOnlineInfo *)info;
- (FMResultSet *)selectLinkman;
- (BOOL)updateLinkman;
- (BOOL)updateLinkman:(NSArray *)arr;
- (FMResultSet *)selectName:(NSString *)linkmanID;
- (BOOL)updatePersonalData:(KoronOnlineInfo *)info;

@end
