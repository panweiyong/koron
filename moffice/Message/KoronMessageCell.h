//
//  KoronMessageCell.h
//  moffice
//
//  Created by Koron on 13-9-4.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface KoronMessageCell : UITableViewCell {
    //头像
    UIImageView *_headImageView;
    //联系人名字
    UILabel *_nameLabel;
    //个性签名或者上一条消息的记录
    UILabel *_underLabel;
}

@property (nonatomic,retain)UIImageView *headImageView;
@property (nonatomic,retain)UILabel *nameLabel;
@property (nonatomic,retain)UILabel *underLabel;

@end
