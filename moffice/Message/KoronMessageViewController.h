//
//  KoronMessageViewController.h
//  PrivateMessage
//
//  Created by Koron on 13-9-3.
//  Copyright (c) 2013年 Koron-Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"

@interface KoronMessageViewController : UIViewController <UIScrollViewDelegate,EGORefreshTableHeaderDelegate>{
    UITableView *_tableView;
    EGORefreshTableHeaderView * _refreshHeaderView;
    BOOL _reloading;
}

- (id)initWithCGRect :(CGRect) rect;
- (void)doneLoadingTableViewData;

@end
