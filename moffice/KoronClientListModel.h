//
//  KoronClientListModel.h
//  moffice
//
//  Created by Mac Mini on 13-11-11.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import "BaseModel.h"

@interface KoronClientListModel : BaseModel
/*
 
 "id": "0cac6bdd_1311051713408900111",
 "userName": "",
 "mobile": "138",
 "telephone": "",
 "clientId": "2f1972dd_0909261629321740304",
 "clientName": "深圳市美创科技有限公司",
 "lastUpdateTime": "2013-11-05 17:13:40",
 "lastContractTime": "2013-11-05 17:13:40",
 "moduleId": "1"
 
 */

@property (nonatomic, copy) NSString *contactId;    //联系人id
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *telephone;
@property (nonatomic, copy) NSString *clientId;     //客户id
@property (nonatomic, copy) NSString *clientName;
@property (nonatomic, copy) NSString *lastUpdateTime;
@property (nonatomic, copy) NSString *lastContractTime;
@property (nonatomic, copy) NSString *moduleId;

@end
