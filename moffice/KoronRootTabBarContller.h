//
//  KoronRootTabBarContller.h
//  moffice
//
//  Created by Mac Mini on 13-9-29.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol KoronRootTabBarContllerDelegate <NSObject>
//选中第几个tabbarIitem
- (void)clickTabBarItem:(NSNumber *) item;

@end


@interface KoronRootTabBarContller : UIView {
    
   __unsafe_unretained id<KoronRootTabBarContllerDelegate> delegate;
    
    UIImageView *_homePageImageView;
    
    UIImageView *_privateMessageImageView;
    
    UIImageView *_mailImageView;
    
    UIImageView *_noticeImageView;
    
    UIImageView *_selectedView;
    
    UIImageView *_selectedImageView;
    
}

@property (nonatomic,assign)id<KoronRootTabBarContllerDelegate> delegate;

@end
