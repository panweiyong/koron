//
//  TipsCell.m
//  moffice
//
//  Created by yangxi zou on 12-1-11.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.

#import "TipsCell.h"

@implementation TipsCell

@synthesize type;
@synthesize refid;
@synthesize tipsid;

-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    //开始一条线
    CGContextSaveGState(context);
    CGContextSetStrokeColorWithColor(context,TIPSCELLLINE_BACKGROUND );
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0.0, self.frame.size.height-2);
    CGContextAddLineToPoint(context, 320.0, self.frame.size.height-2);
    CGContextStrokePath(context);
    
    CGContextSetRGBStrokeColor(context, 251.0f/255.0f, 251.0f/255.0f,252.0f/255.0f, 1.0);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0.0, self.frame.size.height-1);
    CGContextAddLineToPoint(context, 320.0, self.frame.size.height-1);
    CGContextStrokePath(context);
    CGContextRestoreGState(context);
    
    //画渐变
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 1.0, 1.0, 1.0, 0.35,  // Start color
        1.0, 1.0, 1.0, 0.06 }; // End color
    
    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
    
    CGRect currentBounds = self.bounds;
    CGPoint topCenter = CGPointMake(CGRectGetMidX(currentBounds), 0.0f);
    CGPoint midCenter = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetMidY(currentBounds));
    CGContextDrawLinearGradient(context, glossGradient, topCenter, midCenter, 0);
    
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace);
    
    /*
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor blackColor] CGColor], (id)[[UIColor whiteColor] CGColor], nil];
    [self.layer insertSublayer:gradient atIndex:0];
    */
}


- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(self.imageView.frame.origin.x,12,12,12);
    self.textLabel.frame = CGRectMake(self.textLabel.frame.origin.x,self.textLabel.frame.origin.y-3,self.textLabel.frame.size.width,self.textLabel.frame.size.height );
    self.detailTextLabel.frame = CGRectMake(self.detailTextLabel.frame.origin.x,self.detailTextLabel.frame.origin.y+3,self.detailTextLabel.frame.size.width,self.detailTextLabel.frame.size.height ); 
}
@end
