//
//  TipsCell.h
//  moffice
//
//  Created by yangxi zou on 12-1-11.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "CommonConst.h"

@interface TipsCell : UITableViewCell
{
    NSString *refid;
    NSString *type;
    NSString *tipsid;
}

@property (nonatomic,retain)NSString *refid;
@property (nonatomic,retain)NSString *type;
@property (nonatomic,retain)NSString *tipsid;


@end
