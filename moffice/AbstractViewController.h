//
//  AbstractViewController.h
//  moffice
//
//  Created by yangxi zou on 12-1-11.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "EGORefreshTableHeaderView.h"
#import "SMAlertView.h"

@interface AbstractViewController : UIViewController <EGORefreshTableHeaderDelegate>
{
    EGORefreshTableHeaderView *_refreshHeaderView;
	BOOL _reloading;
    UITableView *tableView;
    id tableData;
}

//通用请求失败处理
- (void)requestFailed:(ASIHTTPRequest *)request;
- (void)requestDataFinished:(id)jsondata;

- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

-(void)timeoutQuit;

@end
