//
//  KoronLoginView.h
//  KoronMoffice
//
//  Created by Mac Mini on 13-9-24.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KoronManager.h"

@interface KoronLoginView : UIView <UITextFieldDelegate> {
    UITextField *_userNameTextField;
    UITextField *_passwordTextField;
    KoronManager *_manager;
}

@property (nonatomic,strong)UITextField *userNameTextField;
@property (nonatomic,strong)UITextField *passwordTextField;

@end
