//
//  PostingViewController.h
//  moffice
//
//  Created by lijinhua on 13-6-8.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "AbstractViewController.h"
#import "BMapKit.h"
#import "QBImagePickerController.h"
#import "JsonService.h"
#import "categoryListViewController.h"

@interface PostingViewController :UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UITextViewDelegate,UITextFieldDelegate,UITextViewDelegate,BMKGeneralDelegate,BMKMapViewDelegate,BMKSearchDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,QBImagePickerControllerDelegate,UIAlertViewDelegate,ContactCtrlDelegate,pushDataDelegate> //AbstractViewController
{
    //UITableView *listView;
    UIView * backgroundView;
    UITableView *alertTabView;
    NSMutableArray *imageArray;
    NSInteger imageIndex;
    
    BMKMapView *littleMap;
    BMKSearch* _search;
    UIActivityIndicatorView *activity;
    
    CLLocationCoordinate2D startPt;
    CLLocation *whereLocation;
    UIButton *detailbutton;
    BOOL isRetina1;
    BOOL textviewIsfirstEdite;
    UILabel *caDetailLabel;
    
    categoryListViewController *cateView;
    NSString *titleStr;
    NSString *topId;
    NSString *contentStr;
    NSString *whereAmI;
    NSString *netIsok;
    NSString *promptStr;
    NSNumber *codeStr;
    
    UITextField *titleField;
    UITextView *contentView;
}
@property(nonatomic,retain) UIView * backgroundView;
@property(nonatomic,retain) UITableView *alertTabView;
@property(nonatomic,retain) BMKMapView *littleMap;
@property(nonatomic,retain) BMKSearch *_search;
@property(nonatomic,retain) UIActivityIndicatorView *activity;
@property(nonatomic,retain) UIButton *detailbutton;
@property(nonatomic,retain) UILabel *caDetailLabel;
@property(nonatomic,retain) categoryListViewController *cateView;
@property(nonatomic,retain) NSString *titleStr;
@property(nonatomic,retain) NSString *topId;
@property(nonatomic,retain) NSString *contentStr;
@property(nonatomic,retain) NSString *whereAmI;
@property(nonatomic,retain) NSString *netIsok;
@property(nonatomic,retain) NSString *promptStr;
@property(nonatomic,retain) NSNumber *codeStr;
@property(nonatomic,retain) UITextField *titleField;
@property(nonatomic,retain) UITextView *contentView;
@end
