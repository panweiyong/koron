//
//  KoronProjectDetailHeaderCell.h
//  ProjectDemo
//
//  Created by Mac Mini on 13-11-27.
//  Copyright (c) 2013年 Hsn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KoronProjectDetailHeaderCell : UITableViewCell

@property (nonatomic, retain) UIImageView *statusPicture;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *executorLabel;
@property (nonatomic, retain) UILabel *remainingTimeLabel;
@property (nonatomic, retain) UILabel *introduceLabel;
@property (nonatomic, retain) UILabel *detailContentLabel;
@property (nonatomic, retain) UILabel *detailTimeLabel;
@end
