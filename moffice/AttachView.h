//
//  AttachView.h
//  moffice
//
//  Created by Mac Mini on 13-10-29.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol didSelectedItem <NSObject>

- (void)didSelectedAttachView:(UIView *)attachView withIndex:(NSInteger)index;

@end

@interface AttachView : UIView {
    id <didSelectedItem> _delegate;
}

@property (nonatomic, retain) UIImageView *attachImageView;
@property (nonatomic, retain) UILabel     *attachNameLabel;
@property (nonatomic, retain) UIView      *backgroundImageView;
@property (nonatomic, retain) UIImageView *statusImageView;
@property (nonatomic, retain) UIActivityIndicatorView *actView;

@property (nonatomic, assign) id <didSelectedItem> delegate;
@end
