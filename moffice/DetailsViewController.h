//
//  DetailsViewController.h
//  moffice
//
//  Created by szsm on 12-2-3.
//  Copyright (c) 2012年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageViewController.h"
#import "JsonUtils.h"
#import "CommonConst.h"

@interface DetailsViewController : UITableViewController
{
    NSArray *tableData;
    NSString *moduleid;
}
@property (nonatomic,retain)NSArray *tableData;
@property (nonatomic,retain)NSString *moduleid;
@end
