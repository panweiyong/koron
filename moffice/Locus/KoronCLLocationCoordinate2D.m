//
//  KoronCLLocationCoordinate2D.m
//  moffice
//
//  Created by Koron on 13-9-16.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronCLLocationCoordinate2D.h"

@implementation KoronCLLocationCoordinate2D
@synthesize latitude = _latitude;
@synthesize longitude = _longitude;


- (id)initWithLatitude:(float)latitude longitude:(float)longitude
{
    self = [super init];
    if (self) {
        _latitude = latitude;
        _longitude = longitude;
    }
    return self;
}

@end
