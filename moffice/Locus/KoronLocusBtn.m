//
//  KoronLocusBtn.m
//  moffice
//
//  Created by Koron on 13-9-18.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "KoronLocusBtn.h"

@implementation KoronLocusBtn

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)drawRect:(CGRect)rect {
    CGRect frame = self.frame;
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(ctx, [UIColor grayColor].CGColor);
    CGContextSetAlpha(ctx, 0.2);
    CGContextSetLineWidth(ctx, 1);
    
    CGContextMoveToPoint(ctx, 0, 5);
    CGContextAddLineToPoint(ctx, 0 , frame.size.height - 5);
    
    CGContextMoveToPoint(ctx, frame.size.width, 5);
    CGContextAddLineToPoint(ctx, frame.size.width , frame.size.height - 5);
    
    CGContextStrokePath(ctx);
}

@end
