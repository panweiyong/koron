//
//  KoronLocusViewController.m
//  moffice
//
//  Created by Koron on 13-9-16.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//  UIImageWriteToSavedPhotosAlbum(img, self, @selector(image:didFinishSavingWithError:contextInfo:), nil)




#import "KoronLocusViewController.h"


#define MYBUNDLE [NSBundle bundleWithPath: MYBUNDLE_PATH]
#define MYBUNDLE_NAME @ "mapapi.bundle"
#define MYBUNDLE_PATH [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: MYBUNDLE_NAME]


@interface KoronLocusViewController ()

@end

@implementation KoronLocusViewController

- (void)dealloc
{
    [_dataBase close];
    [_dataBase release];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    _mapView.delegate = nil;
    RELEASE_SAFELY(_mapView);
    
    RELEASE_SAFELY(_coors);
    RELEASE_SAFELY(_polylineView);

    RELEASE_SAFELY(_startAnnotation);
    RELEASE_SAFELY(_endAnnotation);
    RELEASE_SAFELY(_topView);
    RELEASE_SAFELY(_topLabel);
    
    RELEASE_SAFELY(_subTopView);
    RELEASE_SAFELY(_rangeLabel);
    RELEASE_SAFELY(_timeLabel);
    
    RELEASE_SAFELY(_startCoor);
    RELEASE_SAFELY(_endCoor);
    RELEASE_SAFELY(_suspendBtn);
    
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [_mapView viewWillAppear];
    _mapView.delegate = self; // 此处记得不用的时候需要置nil，否则影响内存的释放
}
-(void)viewWillDisappear:(BOOL)animated
{
    [_mapView viewWillDisappear];
    _mapView.delegate = nil; // 不用时，置nil
}



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        isFirst = YES;
        
        _coors = [[NSMutableArray alloc]init];
        _startAnnotation = [[RouteAnnotation alloc]init];
        _startAnnotation.title = @"起点";
        _startAnnotation.type = 0;
        
        _endAnnotation = [[RouteAnnotation alloc]init];
        _endAnnotation.title = @"终点";
        _endAnnotation.type = 1;
        
        _range = 0;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground) name:@"applicationDidEnterBackground" object:nil];
        
        
     
        [NSThread detachNewThreadSelector:@selector(getLocations) toTarget:self withObject:nil];
        
    }
    return self;
}


- (void)getLocations {
    
    @synchronized(_coors)
    
    {
        [self createDataBase];
        FMResultSet *rs = [self selectLucus];
        while (rs.next) {
            KoronCLLocationCoordinate2D *coor = [[KoronCLLocationCoordinate2D alloc]initWithLatitude:[rs doubleForColumn:@"latitude"] longitude:[rs doubleForColumn:@"longitude"]];
            [_coors addObject:coor];
            [coor release];
        }
    }
    
    [self performSelectorOnMainThread:@selector(drawPolyline) withObject:nil waitUntilDone:YES];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    UIImage *topImage = [UIImage imageNamed:@"home_top.png"];
    UIImage *newTopImage = [topImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 1, 0)];
    _topView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width,45)];
    _topView.userInteractionEnabled = YES;
    [_topView setImage:newTopImage];
    [self.view addSubview:_topView];
    _topLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 200, 34)];
    _topLabel.textAlignment = NSTextAlignmentCenter;
    _topLabel.textColor = [UIColor grayColor];
    _topLabel.backgroundColor = [UIColor clearColor];
    _topLabel.text = @"行程轨迹";
    [_topView addSubview:_topLabel];
    
    UIButton *topRightBtn = [[UIButton alloc] init];
    [topRightBtn setFrame:CGRectMake(270, 5, 34, 34)];
    [topRightBtn addTarget:self action:@selector(screenshot) forControlEvents:UIControlEventTouchUpInside];
    [topRightBtn setImage:[UIImage imageNamed:@"icon_cut_picture.png"] forState:UIControlStateNormal];
    [_topView addSubview:topRightBtn];
    RELEASE_SAFELY(topRightBtn);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        [_topView setFrame:CGRectMake(0, 20, self.view.bounds.size.width, 45)];
    }
    
    
    UIButton *dismissBtn = [[UIButton alloc]init];
    [dismissBtn setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [dismissBtn setFrame:CGRectMake(5, 2, 40, 40)];
    [dismissBtn addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:dismissBtn];
    RELEASE_SAFELY(dismissBtn);
    
    
    
    _mapView = [[BMKMapView alloc]initWithFrame:CGRectMake(0,_topView.frame.origin.y + _topView.bounds.size.height, 320, [[UIScreen mainScreen] bounds].size.height -110)];
    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    _mapView.zoomLevel = 19;

    [self.view addSubview:_mapView];
    
    
    
    if (_coors.count >= 1) {
        isFirst = NO;
        CLLocationCoordinate2D coor2D;
        coor2D.latitude = [[_coors objectAtIndex:0] latitude];
        coor2D.longitude = [[_coors objectAtIndex:0] longitude];
        
        _startCoor = [[KoronCLLocationCoordinate2D alloc]init];
        _startCoor.latitude = coor2D.latitude;
        _startCoor.longitude = coor2D.longitude;
        
        _startAnnotation.coordinate = coor2D;
        [_mapView addAnnotation:_startAnnotation];
        
        [self drawPolyline];
    }
    
    
    
    
    _subTopView = [[UIView alloc]initWithFrame:CGRectMake(0, _topView.frame.origin.y + _topView.frame.size.height, 320, 20)];
    _subTopView.backgroundColor = [UIColor grayColor];
    _subTopView.alpha = 0.7;
    [self.view addSubview:_subTopView];
    
    _rangeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 160, 20)];
    _rangeLabel.textAlignment = NSTextAlignmentCenter;
    _rangeLabel.text = @"行走距离(km) 0.00";
    _rangeLabel.font = [UIFont systemFontOfSize:14];
    _rangeLabel.textColor = [UIColor whiteColor];
    _rangeLabel.backgroundColor = [UIColor clearColor];
    [_subTopView addSubview:_rangeLabel];
    
    _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(160, 0, 160, 20)];
    _timeLabel.textAlignment = NSTextAlignmentCenter;
    _timeLabel.text = @"记录时间 00:00:00";
    _timeLabel.font = [UIFont systemFontOfSize:14];
    _timeLabel.textColor = [UIColor whiteColor];
    _timeLabel.backgroundColor = [UIColor clearColor];
    [_subTopView addSubview:_timeLabel];
    
    UIButton *startBtn = [[UIButton alloc] init];
    [startBtn setFrame:CGRectMake(0, _mapView.frame.origin.y + _mapView.frame.size.height, 320/3-1, 45)];
    [startBtn setTitle:@"开始" forState:UIControlStateNormal];
    [startBtn setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    startBtn.backgroundColor = [UIColor clearColor];
    [startBtn addTarget:self action:@selector(startLocation:) forControlEvents:UIControlEventTouchUpInside];
    [startBtn addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
    [startBtn addTarget:self action:@selector(changeButtonBackgroundColor:) forControlEvents:UIControlEventTouchDragOutside];
    
    [self.view addSubview:startBtn];
    RELEASE_SAFELY(startBtn);
    
    _suspendBtn = [[KoronLocusBtn alloc] init];
    [_suspendBtn setFrame:CGRectMake(320/3, _mapView.frame.origin.y + _mapView.frame.size.height, 320/3-1, 45)];
    [_suspendBtn setTitle:isFirst?@"暂停":@"继续" forState:UIControlStateNormal];
    [_suspendBtn setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    _suspendBtn.backgroundColor = [UIColor clearColor];
    [_suspendBtn addTarget:self action:@selector(suspendLocation:) forControlEvents:UIControlEventTouchUpInside];
    [_suspendBtn addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
    [_suspendBtn addTarget:self action:@selector(changeButtonBackgroundColor:) forControlEvents:UIControlEventTouchDragOutside];
    [self.view addSubview:_suspendBtn];
    
    
    
    UIButton *endBtn = [[UIButton alloc] init];
    [endBtn setFrame:CGRectMake(320/3 * 2, _mapView.frame.origin.y + _mapView.frame.size.height, 320/3+1, 45)];
    [endBtn setTitle:@"结束" forState:UIControlStateNormal];
    [endBtn setTitleColor:[UIColor blackColor]forState:UIControlStateNormal];
    endBtn.backgroundColor = [UIColor clearColor];
    [endBtn addTarget:self action:@selector(endLocation:) forControlEvents:UIControlEventTouchUpInside];
    [endBtn addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown];
    
    [endBtn addTarget:self action:@selector(changeButtonBackgroundColor:) forControlEvents:UIControlEventTouchDragOutside];
    [self.view addSubview:endBtn];
    RELEASE_SAFELY(endBtn);
    
    
    UIButton *locationBtn = [[UIButton alloc]init];
    [locationBtn setFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 60, [UIScreen mainScreen].bounds.size.height - 50 - 75 , 45, 45)];
    [locationBtn addTarget:self action:@selector(currentLocation) forControlEvents:UIControlEventTouchUpInside];
    [locationBtn setImage:[UIImage imageNamed:@"regionImage.png"] forState:UIControlStateNormal];
    
    [self.view addSubview:locationBtn];
    RELEASE_SAFELY(locationBtn);
    
}

- (void)buttonTouchDown:(UIButton *)btn {
    btn.alpha = 0.5;
    [UIView animateWithDuration:0.5 animations:^{
        btn.backgroundColor = [UIColor grayColor];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btn.alpha = 0.2;
    }];
}

- (void)changeButtonBackgroundColor:(UIButton *)btn {
    
    [UIView animateWithDuration:0.5 animations:^{
        btn.backgroundColor = [UIColor whiteColor];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.alpha = 1;
        
    }];
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//开始记录行走路线
- (void)startLocation:(UIButton *)btn {
    [self changeButtonBackgroundColor:btn];
    
    if (isStart || isSuspend) {
        [SVProgressHUD showErrorWithStatus:@"行程轨迹已运行" duration:1];
        return;
    }else {
        if (isDidUpdateUserLocation) {
            //第一次运行,记录时间清零
            if (isFirst) {
                _timeConsuming = 0;
            }
            
            //开始记录时间
            _colokTime = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(colokTime) userInfo:nil repeats:YES];
            _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(getCurrentLocation) userInfo:nil repeats:YES];
            isStart = YES;
        }else {
            [SVProgressHUD showErrorWithStatus:@"正在更新地理位置" duration:1];
        }
    }
    
}


//记录时间
- (void)colokTime {
    _timeConsuming ++;
    _timeLabel.text = [NSString stringWithFormat:@"记录时间 %02d:%02d:%02d",_timeConsuming/3600,_timeConsuming/60,_timeConsuming%60];
}

//暂停记录行走路线
- (void)suspendLocation:(UIButton *)btn {
    [self changeButtonBackgroundColor:btn];
    if (isFirst) {
        return;
    }
    if (isStart) {
        [_timer invalidate];
        [_colokTime invalidate];
        isSuspend = YES;
        isStart = NO;
        [btn setTitle:@"继续" forState:UIControlStateNormal];
        [SVProgressHUD showSuccessWithStatus:@"暂停记录行程轨迹" duration:1];
    }else {
        if (isDidUpdateUserLocation) {
            _colokTime = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(colokTime) userInfo:nil repeats:YES];
            _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(getCurrentLocation) userInfo:nil repeats:YES];
            isStart = YES;
            isSuspend = NO;
            [btn setTitle:@"暂停" forState:UIControlStateNormal];
            [SVProgressHUD showSuccessWithStatus:@"继续记录行程轨迹" duration:1];
        }else {
            [SVProgressHUD showErrorWithStatus:@"正在更新地理位置" duration:1];
        }
    }
}


//停止记录行走路线
- (void)endLocation:(UIButton *)btn {
    [self changeButtonBackgroundColor:btn];
    if (isStart) {
        [_timer invalidate];
        [_colokTime invalidate];
        isStart = NO;
        
        if ([_coors count] >= 1) {
            CLLocationCoordinate2D coor;
            coor.latitude = [[_coors lastObject] latitude];
            coor.longitude = [[_coors lastObject] longitude];
            
            _endAnnotation.coordinate = coor;
            [_mapView addAnnotation:_endAnnotation];
        }else {
            _endAnnotation.coordinate = _mapView.userLocation.coordinate;
            [_mapView addAnnotation:_endAnnotation];
        }
        _endCoor = [[KoronCLLocationCoordinate2D alloc]initWithLatitude:_mapView.userLocation.coordinate.latitude longitude:_mapView.userLocation.coordinate.longitude];
    }
    isFirst = YES;
    isStart = NO;
    isSuspend = NO;
    RELEASE_SAFELY(_startCoor);
    RELEASE_SAFELY(_endCoor);
    [_suspendBtn setTitle:@"暂停" forState:UIControlStateNormal];
    [_coors removeAllObjects];
    [self deleteLucus];
    [SVProgressHUD showSuccessWithStatus:@"已停止记录行程轨迹,重新开始或退出将清除地图上的轨迹." duration:1];
}


//获得地理位置
- (void)getCurrentLocation {
    //判断一下是否定位错误
    if ((_mapView.userLocation.coordinate.latitude == 0.0 && _mapView.userLocation.coordinate.longitude == 0.0) || (_mapView.userLocation.coordinate.latitude == -180 && _mapView.userLocation.coordinate.longitude == -180)) {
        return;
    }
    
    //如果第一次运行,直接把地理位置添加到数组里
    if (isFirst) {
        
        KoronCLLocationCoordinate2D *coor = [[KoronCLLocationCoordinate2D alloc]initWithLatitude:_mapView.userLocation.coordinate.latitude longitude:_mapView.userLocation.coordinate.longitude];
        
        _startCoor = [[KoronCLLocationCoordinate2D alloc] initWithLatitude:coor.latitude longitude:coor.longitude];
        
        _startAnnotation.coordinate = _mapView.userLocation.coordinate;
        [_mapView addAnnotation:_startAnnotation];
        
        [_coors addObject:coor];
        [coor release];
        
        
        isFirst = NO;
    }else {
        //判断每次定位距离上次相差是否大于50米
        if ([self judgeRange:[_coors lastObject] newCoordinate2D:_mapView.userLocation.coordinate]) {
            
            //如果大于50,保存地理位置信息
            KoronCLLocationCoordinate2D *coor = [[KoronCLLocationCoordinate2D alloc]initWithLatitude:_mapView.userLocation.coordinate.latitude longitude:_mapView.userLocation.coordinate.longitude];
            [_coors addObject:coor];
            [coor release];
            
            //画图
            [self drawPolyline];
        }
    }
    
}


//回到当前地图上蓝点显示的经纬度
- (void)currentLocation {
    CLLocationCoordinate2D coor;
    coor.longitude = _mapView.userLocation.coordinate.longitude;
    coor.latitude = _mapView.userLocation.coordinate.latitude;
    [_mapView setCenterCoordinate:coor animated:YES];
}


/*
 计算两点之间的距离(米)
 
 lastCoordinate2D   传入地理位置数组里的最后一条地理位置记录
 newCoordinate2D    传入当前地理位置
 */
- (BOOL)judgeRange:(KoronCLLocationCoordinate2D *)lastCoordinate2D newCoordinate2D:(CLLocationCoordinate2D)newCoordinate2D {
    
    CLLocationCoordinate2D lastCoor;
    lastCoor.latitude = [lastCoordinate2D latitude];
    lastCoor.longitude = [lastCoordinate2D longitude];
    
    
    BMKMapPoint lastPoint = BMKMapPointForCoordinate(lastCoor);
    
    CLLocationCoordinate2D newCoor;
    newCoor.latitude = newCoordinate2D.latitude;
    newCoor.longitude = newCoordinate2D.longitude;
    
    
    BMKMapPoint newPoint = BMKMapPointForCoordinate(newCoor);
    
    float walkRange = BMKMetersBetweenMapPoints(lastPoint, newPoint);
    
    
    if (walkRange >= 30) {
        
        _range += walkRange;
        
        _rangeLabel.text = [NSString stringWithFormat:@"行走距离(km) %0.2f",_range / 1000];
        
        
        return YES;
    }else {
        return NO;
    }
}



//画线
- (void)drawPolyline {
    
    //每次画图之前先删除百度地图上的覆盖图,否则导致不能画图
    if (nil != _polyline) {
        [_mapView removeOverlay:_polyline];
    }
    //地理位置信息小于2的时候不用画图
    if ([_coors count] <= 1) {
        return;
    }
    
    
    //创建 CLLocationCoordinate2D 数组 (C++语法)
    CLLocationCoordinate2D *coor2Ds = new CLLocationCoordinate2D[_coors.count];
    
    for (int i = 0;i < [_coors count] ;i++ ) {
        
        coor2Ds[i].latitude = [[_coors objectAtIndex:i] latitude] ;
        coor2Ds[i].longitude = [[_coors objectAtIndex:i] longitude];
        
    }
    //每次画图完成之后保存 polyline 用于下次画图之前删除地图上的覆盖物 
    _polyline = [BMKPolyline polylineWithCoordinates:coor2Ds count:_coors.count];
    
    //释放内存,C语法
    delete(coor2Ds);
    [_mapView addOverlay:_polyline];
    
}


#pragma mark - BMKMapViewDelegate

- (BMKOverlayView *)mapView:(BMKMapView *)mapView viewForOverlay:(id <BMKOverlay>)overlay{
    if ([overlay isKindOfClass:[BMKPolyline class]]){
        BMKPolylineView* polylineView = [[[BMKPolylineView alloc] initWithOverlay:overlay] autorelease];
        polylineView.strokeColor = [[UIColor purpleColor] colorWithAlphaComponent:1];
        polylineView.lineWidth = 2.0;
        return polylineView;
    }
    return nil;
}


/**
 *用户位置更新后，会调用此函数
 *@param mapView 地图View
 *@param userLocation 新的用户位置
 */

- (void)mapView:(BMKMapView *)mapView didUpdateUserLocation:(BMKUserLocation *)userLocation
{
    if (!isDidUpdateUserLocation) {
        [self currentLocation];
    }
    isDidUpdateUserLocation = YES;

}


- (BMKAnnotationView*)getRouteAnnotationView:(BMKMapView *)mapview viewForAnnotation:(RouteAnnotation*)routeAnnotation
{
	BMKAnnotationView* view = nil;
	switch (routeAnnotation.type) {
		case 0:
		{
			view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"start_node"];
			if (view == nil) {
				view = [[[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"start_node"] autorelease];
				view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_start.png"]];
				view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
				view.canShowCallout = TRUE;
			}
			view.annotation = routeAnnotation;
		}
			break;
		case 1:
		{
			view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"end_node"];
			if (view == nil) {
				view = [[[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"end_node"] autorelease];
				view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_end.png"]];
				view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
				view.canShowCallout = TRUE;
			}
			view.annotation = routeAnnotation;
		}
			break;
		default:
			break;
	}
	
	return view;
}



- (BMKAnnotationView *)mapView:(BMKMapView *)view viewForAnnotation:(id <BMKAnnotation>)annotation
{
	if ([annotation isKindOfClass:[RouteAnnotation class]]) {
		return [self getRouteAnnotationView:view viewForAnnotation:(RouteAnnotation*)annotation];
	}
	return nil;
}


- (NSString*)getMyBundlePath1:(NSString *)filename
{
	
	NSBundle * libBundle = MYBUNDLE ;
	if ( libBundle && filename ){
		NSString * s=[[libBundle resourcePath ] stringByAppendingPathComponent : filename];
		return s;
	}
	return nil ;
}

//根据经纬度设置中心点
//- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate animated:(BOOL)animated;

#pragma mark - FMDataBase

//创建数据库
- (BOOL) createDataBase {
    NSString *path = NSHomeDirectory();
    path = [path stringByAppendingPathComponent:@"Library/Lucus.db"];
    _dataBase = [[FMDatabase alloc]initWithPath:path];
    
    
    if (![_dataBase open]) {
        NSLog(@"创建失败!!");
        return NO;
    }
    //type   1,起点   2,途径点   3,终点
    return  [_dataBase executeUpdate:@"create table if not exists Lucus(latitude real,longitude real,type integer)"];
}

- (FMResultSet *)selectLucus {
    return [_dataBase executeQuery:@"select * from Lucus"];
}


- (BOOL)insertLucus {
    BOOL isInsertSucceed;
    
    isInsertSucceed = [_dataBase executeUpdate:@"insert into Lucus (latitude,longitude,type) values (?,?,?)",[NSNumber numberWithFloat:_startCoor.latitude],[NSNumber numberWithFloat:_startCoor.longitude],[NSNumber numberWithInteger:1]];
    
    if (_coors.count <= 1) {
        return isInsertSucceed;
    }
    
    for (KoronCLLocationCoordinate2D *coor in _coors) {
        if ([_dataBase executeUpdate:@"insert into Lucus (latitude,longitude,type) values (?,?,?)",[NSNumber numberWithFloat:coor.latitude],[NSNumber numberWithFloat:coor.longitude],[NSNumber numberWithInteger:2]]) {
            isInsertSucceed = YES;
        }else {
            isInsertSucceed = NO;
        }
    }
    if (_endCoor) {
        isInsertSucceed = [_dataBase executeUpdate:@"insert into Lucus (latitude,longitude,type) values (?,?,?)",[NSNumber numberWithFloat:_startCoor.latitude],[NSNumber numberWithFloat:_endCoor.longitude],[NSNumber numberWithInteger:3]];
    }
    return isInsertSucceed;
}


- (BOOL)deleteLucus {
    return [_dataBase executeUpdate:@"delete from Lucus"];
}



//应用程序切换至后台接收通知
- (void)applicationDidEnterBackground {
    [self insertLucus];
}






#pragma mark - dismissViewController

- (void)dismissLocation {
    if (isStart) {
        [_timer invalidate];
        [_colokTime invalidate];
        
        if (_coors.count > 0) {
            [self deleteLucus];
            [self insertLucus];
        }
        
        isStart = NO;
    }
}

- (void)dismissViewController {
    [self dismissLocation];
    
//    CATransition *myAnimation = [CATransition animation];
//    myAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    myAnimation.duration = 0.4;
//    
//    myAnimation.type = kCATransitionReveal;
//    myAnimation.subtype = kCATransitionFromLeft;
    
//    [[self.view.superview layer] addAnimation:myAnimation forKey:nil];

    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - screenshot

//截图保存到相册
- (void)screenshot {
    [SVProgressHUD showSuccessWithStatus:@"图片已保存至相册" duration:1];
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //图片保存到相册
    UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
}





@end
