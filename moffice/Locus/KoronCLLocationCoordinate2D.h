//
//  KoronCLLocationCoordinate2D.h
//  moffice
//
//  Created by Koron on 13-9-16.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KoronCLLocationCoordinate2D : NSObject {
    float _latitude;
    float _longitude;
}

@property (nonatomic,assign)float latitude;
@property (nonatomic,assign)float longitude;

- (id)initWithLatitude:(float)latitude longitude:(float)longitude;

@end
