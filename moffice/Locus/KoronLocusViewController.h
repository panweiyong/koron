//
//  KoronLocusViewController.h
//  moffice
//
//  Created by Koron on 13-9-16.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMKMapView.h"
#import "BMKPolyline.h"
#import "BMKPolylineView.h"
#import "KoronCLLocationCoordinate2D.h"
#import "RouteAnnotation.h"
#import "KoronLocusBtn.h"
#import "FMDatabase.h"
#import "SVProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

@interface KoronLocusViewController : UIViewController <BMKMapViewDelegate> {
    UIImageView *_topView;
    UILabel *_topLabel;
    UIView *_subTopView;
    UILabel *_rangeLabel;
    UILabel *_timeLabel;
    
    BMKMapView *_mapView;
    //保存所有的经纬度(绘图)
    NSMutableArray *_coors;
    
    BMKPolylineView *_polylineView;
    //开始记录行走轨迹的起始经纬度
    KoronCLLocationCoordinate2D *_startCoor;
    //结束记录行走轨迹的经纬度
    KoronCLLocationCoordinate2D *_endCoor;
    //定时器(定时绘图)
    NSTimer *_timer;
    //绘图
    BMKPolyline *_polyline;
    
    
    RouteAnnotation *_startAnnotation;
    RouteAnnotation *_endAnnotation;
    //判断是否重新开始记录轨迹
    BOOL isFirst;
    //判断时候开始记录轨迹
    BOOL isStart;
    //判断是否暂停
    BOOL isSuspend;
    //行走距离
    CGFloat _range;
    //总耗时
    NSInteger _timeConsuming;
    //定时器(记录时间)
    NSTimer *_colokTime;
    
    KoronLocusBtn *_suspendBtn;
    
    FMDatabase *_dataBase;
    //用户地理位置是否更新，用来判断第一次是否定位错误
    BOOL isDidUpdateUserLocation;
}

@end
