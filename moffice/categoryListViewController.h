//
//  categoryListViewController.h
//  moffice
//
//  Created by lijinhua on 13-6-13.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
//定义协议
@protocol ContactCtrlDelegate
- (void)CallBack:(NSString *)str1 other:(NSString*)str2; //回调传值
@end

@interface categoryListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *listView;
    NSArray *listArray;
    NSArray *listDetailArray;
    BOOL isFirst;
    NSMutableArray *listContentArr;
    NSMutableArray *listTopidArr;
    id <ContactCtrlDelegate> delegate;
}
@property(nonatomic,retain) UITableView *listView;
@property(nonatomic,retain) NSArray *listArray;
@property(nonatomic,retain) NSArray *listDetailArray;
@property(nonatomic,retain) NSMutableArray *listContentArr;
@property(nonatomic,retain) NSMutableArray *listTopidArr;
@property(nonatomic,retain)id <ContactCtrlDelegate> delegate;  
@end
