//
//  bbsATabViewController.m
//  moffice
//
//  Created by lijinhua on 13-6-7.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "bbsATabViewController.h"

@interface bbsATabViewController ()

@end

@implementation bbsATabViewController
@synthesize refid,tipsid;
@synthesize bbsScrollview;
@synthesize commentsArr;
@synthesize replayTabview;
@synthesize dataArr;
@synthesize activity;
@synthesize replayContent,topicId;
@synthesize replayBtn;
@synthesize replayNum,replayResult;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
         self.title=NSLocalizedString(@"bbs",@"bbs");
    }
    return self;
}

-(void)dealloc
{
    [super dealloc];
    //[commentsArr release];
    //[bbsScrollview release];
}


- (void)viewWillDisappear:(BOOL)animated {
    [[JsonService sharedManager] cancelAllRequest];
    [self setHidesBottomBarWhenPushed:NO];
    [super viewDidDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    for(UIView *view in [self.view subviews])
    {
        [view removeFromSuperview];
    }
    activity=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
    [activity setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
    [activity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [self.view addSubview:activity];
    [activity startAnimating];
    JsonService *jservice=[JsonService sharedManager];
    [jservice setDelegate:self];
    NSLog(@"refid=%@",refid);
    [jservice getBbs:refid];
    replayContent=nil;
}

//异步接收到返回的数据
- (void)requestDataFinished:(id)jsondata;//request
{
    BOOL flag=NO;
    if(!flag)
    {
        flag=YES;
        if ([jsondata isKindOfClass:[NSDictionary class]])
        {
            NSLog(@"%@",jsondata);
            topicName=[[jsondata objectForKey:@"topicName"] retain];
            userName=[[[jsondata objectForKey:@"bbsUser"] objectForKey:@"fullName"] retain];
            creatTime=[[jsondata objectForKey:@"createTime"] retain];
            webContent=[[jsondata objectForKey:@"topicContent"] retain];
            //commentsArr=[[[NSArray alloc]init] retain];
            commentsArr=[[jsondata objectForKey:@"replayList"] retain];
            topicId=[[jsondata objectForKey:@"id"] retain];
        }
        dataArr=[[NSMutableArray alloc]initWithCapacity:0];
        [dataArr removeAllObjects];
    }
    else
    {
        flag=NO;
        NSLog(@"%@",jsondata);
        replayNum=[[jsondata objectForKey:@"code"] retain];
        replayResult=[[jsondata objectForKey:@"desc"] retain];
        if ([replayNum intValue]==1)
        {
            JsonService *jservice=[JsonService sharedManager];
            [jservice setDelegate:self];
            [jservice getBbs:refid];
            [replayNum release];
        }
    }
    [self layOutView];
}

-(void)layOutView
{
    replayTabview = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [replayTabview setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:replayTabview];
    replayTabview.delegate=self;
    replayTabview.dataSource=self;
    // [replayTabview reloadData];
    [replayTabview release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *done =[[UIBarButtonItem alloc] initWithTitle:@"评论" style:UIBarButtonItemStyleBordered target:self action:@selector(replayShow:)];
    self.navigationItem.rightBarButtonItem = done;
    [activity stopAnimating];
    [activity release];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)replayShow:(UIButton *)button
{
    NSLog(@"replayShow");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if([commentsArr count]!=0)
    {
        return [commentsArr count]+4;
    }
    else
    {
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    for(UIView *subview in [cell.contentView subviews])
    {
		[subview removeFromSuperview];
    }
    if(indexPath.row==0)
    {
        UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 0, 0)];
        titleLabel.text=topicName;
        NSLog(@"%@",topicName);
        [titleLabel setNumberOfLines:0];
        titleLabel.lineBreakMode = UILineBreakModeWordWrap;
        [titleLabel setTextColor:[UIColor blackColor]];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        //UIFont *font = [UIFont systemFontOfSize:20];
        //设置一个行高上限
        CGSize size = CGSizeMake(320,2000);
        //计算实际frame大小，并将label的frame变成实际大小
        CGSize labelsize = [topicName sizeWithFont:[UIFont systemFontOfSize:24] constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
        [titleLabel setFrame:CGRectMake(30,0,self.view.frame.size.width-60, labelsize.height)];
        // [titleLabel sizeToFit];
        titleLabel.font=[UIFont systemFontOfSize:18];
        titleLabel.adjustsFontSizeToFitWidth=YES;
        titleLabel.backgroundColor=[UIColor clearColor];
        [cell.contentView addSubview:titleLabel];
        [titleLabel release];
        
        UIImageView *logoView=[[UIImageView alloc]initWithFrame:CGRectMake(10, titleLabel.frame.size.height+5, 40, 40)];
        [logoView setImage:[UIImage imageNamed:@"org.png"]];
        [cell.contentView addSubview:logoView];
        [logoView release];
        
        UILabel *userLabel=[[UILabel alloc]initWithFrame:CGRectMake(55, titleLabel.frame.size.height+5, 100, 15)];
        [userLabel setBackgroundColor:[UIColor clearColor]];
        [userLabel setTextColor:[UIColor blueColor]];
        userLabel.text=userName;
        userLabel.font=[UIFont systemFontOfSize:12.0];
        userLabel.textAlignment=UITextAlignmentLeft;
        [cell.contentView addSubview:userLabel];
        [userLabel release];
        
        UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(55, logoView.frame.origin.y+30, 100, 10)];
        [timeLabel setBackgroundColor:[UIColor clearColor]];
        [timeLabel setTextColor:[UIColor grayColor]];
        timeLabel.text=creatTime;
        timeLabel.font=[UIFont systemFontOfSize:8.0];
        timeLabel.textAlignment=UITextAlignmentLeft;
        [cell.contentView addSubview:timeLabel];
        [timeLabel release];
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width, titleLabel.frame.size.height+5+logoView.frame.size.height+5);
    }
    else if(indexPath.row==1)
    {
        UIWebView *bbsWeb=[[UIWebView alloc]init];
        bbsWeb.tag=188;
        CGRect webframe=CGRectMake(0,0,640,0);
        bbsWeb.frame=webframe;
        bbsWeb.delegate=self;
        [bbsWeb loadHTMLString:webContent baseURL:nil];
        [cell.contentView addSubview:bbsWeb];
        CGRect frame = bbsWeb.frame;
        CGSize fittingSize = [bbsWeb sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        bbsWeb.frame = frame;
        NSLog(@"bbsWeb.height=%f",frame.size.height);
        cell.frame=CGRectMake(0, 0, self.view.frame.size.width,frame.size.height);
        [bbsWeb release];
    }
    if([commentsArr count]!=0)
    {
        if(indexPath.row==2)
        {
            UILabel *comLabel=[[UILabel alloc]initWithFrame:CGRectMake(20,0, 100, 15)];
            [comLabel setBackgroundColor:[UIColor clearColor]];
            [comLabel setTextColor:[UIColor grayColor]];
            [comLabel setFont:[UIFont systemFontOfSize:10.0]];
            comLabel.text=[NSString stringWithFormat:@"评论:%d",[commentsArr count]];
            [cell.contentView addSubview:comLabel];
            [comLabel release];
            
            UIView *lineView=[[UIView alloc]initWithFrame:CGRectMake(0, 25, self.view.frame.size.width, 1.0f)];
            [lineView setBackgroundColor:[UIColor colorWithRed:193.0/255.0 green:196/255.0 blue:218/255.0 alpha:1.0]];
            [cell.contentView addSubview:lineView];
            [lineView release];
            
            NSLog(@"lineView.frame.origin.y=%f",lineView.frame.origin.y);
            UIImageView *triImage=[[UIImageView alloc]initWithFrame:CGRectMake(30, lineView.frame.origin.y-10, 16, 11)];
            [triImage setBackgroundColor:[UIColor clearColor]];
            [triImage setImage:[UIImage imageNamed:@"little_triangle.png"]];
            [cell.contentView addSubview:triImage];
            [triImage release];
            cell.frame=CGRectMake(0, 0, self.view.frame.size.width, comLabel.frame.size.height+11);
        }
        else if((indexPath.row>2)&&(indexPath.row!=([commentsArr count]+3)))
        {
            UIImageView* logoImage=[[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 30, 30)];
            [logoImage setBackgroundColor:[UIColor clearColor]];
            [logoImage setImage:[UIImage imageNamed:@"org.png"]];
            [cell.contentView addSubview:logoImage];
            
            UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(42, 15, 100, 10)];
            nameLabel.backgroundColor = [UIColor clearColor];
            nameLabel.textColor = [UIColor grayColor];
            nameLabel.font=[UIFont systemFontOfSize:8.0];
            nameLabel.textAlignment = UITextAlignmentLeft;
            nameLabel.text=[[[commentsArr objectAtIndex:indexPath.row-3] objectForKey:@"bbsUser"] objectForKey:@"fullName"];
            [cell.contentView addSubview:nameLabel];
            
            UILabel* timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-125, 15, 120, 10)];
            timeLabel.backgroundColor = [UIColor clearColor];
            timeLabel.textColor = [UIColor grayColor];
            timeLabel.textAlignment = UITextAlignmentLeft;
            timeLabel.text=[[commentsArr objectAtIndex:indexPath.row-3] objectForKey:@"createTime"];
            timeLabel.font=[UIFont systemFontOfSize:8.0];
            [cell.contentView addSubview:timeLabel];
            
            UIWebView* contentWeb=[[UIWebView alloc]init];
            CGRect webframe=CGRectMake(0,logoImage.frame.origin.y+45, 320, 0);
            contentWeb.frame=webframe;
            contentWeb.delegate=self;
            [contentWeb loadHTMLString:[[commentsArr objectAtIndex:indexPath.row-3] objectForKey:@"content"] baseURL:nil];
            [cell.contentView addSubview:contentWeb];
            CGRect frame = contentWeb.frame;
            frame.size.height = 1;
            contentWeb.frame = frame;
            CGSize fittingSize = [contentWeb sizeThatFits:CGSizeZero];
            frame.size = fittingSize;
            contentWeb.frame = frame;
            NSLog(@"contentWeb.height=%f",frame.size.height);
            cell.frame=CGRectMake(0, 0, self.view.frame.size.width, logoImage.frame.size.height+15+contentWeb.frame.size.height);
        }
        else if(indexPath.row==[commentsArr count]+3)
        {
            UILabel *replayLabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 0, self.view.frame.size.width-10, 25)];
            [replayLabel setBackgroundColor:[UIColor colorWithRed:191.0/255.0 green:192.0/255.0 blue:191.0/255.0 alpha:1.0]];
            [replayLabel setTextColor:[UIColor blackColor]];
            [replayLabel setFont:[UIFont systemFontOfSize:10.0]];
            [replayLabel setText:@"  发表评论"];
            [cell.contentView addSubview:replayLabel];
            [replayLabel release];
            
            UIImageView *triImage=[[UIImageView alloc]initWithFrame:CGRectMake(25, 25-8, 12, 8)];
            [triImage setBackgroundColor:[UIColor clearColor]];
            [triImage setImage:[UIImage imageNamed:@"little_triangle.png"]];
            [cell.contentView addSubview:triImage];
            [triImage release];
        
            //加入评论框
            UITextView *replayTextview=[[UITextView alloc]initWithFrame:CGRectMake(5,30,self.view.frame.size.width-10,120)];
            CALayer *textViewlayer=replayTextview.layer;
            textViewlayer.borderWidth=1.0;
            textViewlayer.borderColor=[[UIColor blackColor] CGColor];
            textViewlayer.masksToBounds=YES;
            textViewlayer.cornerRadius=5.0;
            [replayTextview setEditable:YES];
            replayTextview.text=NSLocalizedString(@"write replay", @"write replay");
            replayTextview.textColor=[UIColor grayColor];
            replayTextview.font = [UIFont fontWithName:@"Arial" size:18.0];
            replayTextview.keyboardType=UIKeyboardTypeDefault;
            replayTextview.returnKeyType = UIReturnKeyDone;//返回键的类型
            replayTextview.autoresizingMask = UIViewAutoresizingFlexibleHeight;//自适应高度
            replayTextview.delegate=self;
            [replayTextview setBackgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:replayTextview];
            [replayTextview release];
            
            //发表评论btn
            replayBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, replayTextview.frame.origin.y+replayTextview.frame.size.height+5, self.view.frame.size.width-20, 23)];
            [replayBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
            CALayer *btnlayer=replayBtn.layer;
            //btnlayer.borderWidth=1.0;
            btnlayer.borderColor=[[UIColor blackColor] CGColor];
            btnlayer.masksToBounds=YES;
            btnlayer.cornerRadius=4.0;
            [replayBtn setBackgroundColor:[UIColor colorWithRed:76.0/255.0 green:150.0/255.2 blue:215.0/255.0 alpha:1.0]];
            [replayBtn setTitle:@"发表评论" forState:UIControlStateNormal];
            replayBtn.titleLabel.textColor=[UIColor blackColor];
            [replayBtn addTarget:self action:@selector(replayBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:replayBtn];
            [replayBtn release];
            cell.frame=CGRectMake(0, 0, self.view.frame.size.width,replayTabview.frame.size.height+5+replayTextview.frame.size.height+5+replayBtn.frame.size.height);
        }
        
    }
    else
    {
        if (indexPath.row==2)
        {
            UILabel *replayLabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 0, self.view.frame.size.width-10, 25)];
            [replayLabel setBackgroundColor:[UIColor colorWithRed:191.0/255.0 green:192.0/255.0 blue:191.0/255.0 alpha:1.0]];
            [replayLabel setTextColor:[UIColor blackColor]];
            [replayLabel setFont:[UIFont systemFontOfSize:10.0]];
            [replayLabel setText:@"  发表评论"];
            [cell.contentView addSubview:replayLabel];
            [replayLabel release];
            
            UIImageView *triImage=[[UIImageView alloc]initWithFrame:CGRectMake(25, 25-8, 12, 8)];
            [triImage setBackgroundColor:[UIColor clearColor]];
            [triImage setImage:[UIImage imageNamed:@"little_triangle.png"]];
            [cell.contentView addSubview:triImage];
            [triImage release];
            
            //加入评论框
            UITextView *replayTextview=[[UITextView alloc]initWithFrame:CGRectMake(5, 30,self.view.frame.size.width-10,120)];
            CALayer *textViewlayer=replayTextview.layer;
            textViewlayer.borderWidth=1.0;
            textViewlayer.borderColor=[[UIColor blackColor] CGColor];
            textViewlayer.masksToBounds=YES;
            textViewlayer.cornerRadius=5.0;
            [replayTextview setEditable:YES];
            replayTextview.text=NSLocalizedString(@"write replay", @"write replay");
            replayTextview.textColor=[UIColor grayColor];
            replayTextview.font = [UIFont fontWithName:@"Arial" size:18.0];
            replayTextview.keyboardType=UIKeyboardTypeDefault;
            replayTextview.returnKeyType = UIReturnKeyDone;//返回键的类型
            replayTextview.autoresizingMask = UIViewAutoresizingFlexibleHeight;//自适应高度
            replayTextview.delegate=self;
            [replayTextview setBackgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:replayTextview];
            [replayTextview release];
            
            //发表评论btn
            replayBtn=[[UIButton alloc]initWithFrame:CGRectMake(10, replayTextview.frame.origin.y+replayTextview.frame.size.height+5, self.view.frame.size.width-20, 23)];
            [replayBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0]];
            CALayer *btnlayer=replayBtn.layer;
            //btnlayer.borderWidth=1.0;
            btnlayer.borderColor=[[UIColor blackColor] CGColor];
            btnlayer.masksToBounds=YES;
            btnlayer.cornerRadius=4.0;
            [replayBtn setBackgroundColor:[UIColor colorWithRed:76.0/255.0 green:150.0/255.2 blue:215.0/255.0 alpha:1.0]];
            [replayBtn setTitle:@"发表评论" forState:UIControlStateNormal];
            replayBtn.titleLabel.textColor=[UIColor blackColor];
            [replayBtn addTarget:self action:@selector(replayBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:replayBtn];
            [replayBtn release];
            cell.frame=CGRectMake(0, 0, self.view.frame.size.width,replayTabview.frame.size.height+5+replayTextview.frame.size.height+5+replayBtn.frame.size.height);
        }
    }
   // [self tableView:tableView heightForRowAtIndexPath:indexPath];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView1 heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
         UITableViewCell *cell = [self tableView:tableView1 cellForRowAtIndexPath:indexPath];
        return cell.frame.size.height;
        //return 300;
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if(webView.tag==188)
    {
        CGRect frame = webView.frame;
        frame.size.height = 1;
        webView.frame = frame;
        CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
        frame.size = fittingSize;
        webView.frame = frame;
        bbsWebheight=frame.size.height;
        NSLog(@"bbsWebheight=%f",bbsWebheight);
      // [replayTabview reloadData];
        //[self tableView:replayTabview heightForRowAtIndexPath:2];
    }
    else
    {
    
    }

}

- (void)replayBtnPressed:(UIButton *)btn
{
    NSLog(@"replayBtnPressed");
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
