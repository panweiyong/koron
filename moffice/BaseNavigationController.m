//
//  BaseNavigationController.m
//  moffice
//
//  Created by CA on 13-10-26.
//  Copyright (c) 2013年 shenzhen shuangmeng  computer co.,ltd. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        if ([self.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
            [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"navigationbg1.png"] forBarMetrics:UIBarMetricsDefault];
        }
        self.navigationBar.clipsToBounds = YES;
        self.view.backgroundColor = [UIColor whiteColor];
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBGWhite) name:@"BaseNavigationControllerBackgroundColorChangeWhite" object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBGBlack) name:@"BaseNavigationControllerBackgroundColorChangeBlack" object:nil];
//    
}

//- (void)changeBGBlack {
//    self.view.backgroundColor = [UIColor blackColor];
//}
//
//- (void)changeBGWhite {
//    self.view.backgroundColor = [UIColor whiteColor];
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BaseNavigationControllerBackgroundColorChangeWhite" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BaseNavigationControllerBackgroundColorChangeBlack" object:nil];
    [super dealloc];
}


@end

@implementation UINavigationBar (CustomBackground)

- (void)drawRect:(CGRect)rect {
    UIImage *image = [UIImage imageNamed:@"home_top1.png"];
    [image drawInRect:rect];
}

@end


